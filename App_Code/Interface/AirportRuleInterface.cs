﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for AirportRuleInterface
/// </summary>

namespace DataLayer
{
    public interface AirportRuleInterface
    {
        int Save(AirportRuleBase objAirportRule);
        DataTable Edit(string strRowId);
        DataTable Select();
        string DeletAirportRule(string strRowId);
    }
}