﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for BookedReportInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface BookedReportInterface
    {
        DataTable Select(string strFromDate, string strToDate, string strTail, string strTrip);

        DataSet Select_PageHit(string strFromDate, string strRepToDate);

        DataTable PageHit_TopBookings(string strFromDate, string strRepToDate);

        DataTable PageHit_GetPopup_Details(string country, string regionName, string city, string strRepFromDate, string strRepToDate);

        DataTable AdminUser_Log_Report(string strRepFromDate, string strRepToDate, string strUserName);
    }
}