﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for FixedPriceInterface
/// </summary>

namespace DataLayer
{
    public interface FixedPriceInterface
    {
        int Save(AbstractLayer.FixedPriceBase objUser);
        DataTable Edit(string strRowId);
        string DeletUser(string strEmailid);
        DataTable ActiveSelect();
        DataTable ExpiredSelect();
        DataTable AlreadyExist(string strStartDate, string strEndDate, string strSpecialPriceDesc, string strStartAirport, string strEndAirport);
    }
}