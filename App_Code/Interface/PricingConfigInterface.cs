﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for PricingConfigInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface PricingConfigInterface
    {
        DataTable Select();
        DataTable FilterGrid(string strTailFilter);
        DataTable Edit(string strRowID);
        DataTable TailDetails(string strRowID);
        DataTable PopDetails(string strTripNo, string strTailNo, string strAvailableFrom, string strStartTime);

        string Insert_TailImages(string strTailNo, string FilePath, string InteriorImage, string ExteriorImage, string UploadedBy);
        DataTable Select_TailImages(string strTailNo);
    }
}