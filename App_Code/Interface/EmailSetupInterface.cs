﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;
/// <summary>
/// Summary description for EmailSetupInterface
/// </summary>
namespace DataLayer
{
    public interface EmailSetupInterface
    {
        int Save(AbstractLayer.EmailSetupBase objUser);
        
        DataTable Select();

        DataTable DuplicateUserMasterEmail(string strEmail);

        DataTable DuplicateEmailSetupEmail(string strEmail);
    }
}