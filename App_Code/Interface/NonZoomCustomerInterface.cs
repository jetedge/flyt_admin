﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for NonZoomCustomerInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface NonZoomCustomerInterface
    {
        int Save(AbstractLayer.NonZoomCustomerBase objUser);
        DataTable Edit(string strUserId);
        DataTable Select();
        DataTable AlreadyExist(string strEmailid);
        string DeletUser(string strEmailid);
    }
}