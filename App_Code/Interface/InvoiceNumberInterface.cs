﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for InvoiceNumberInterface
/// </summary>

namespace DataLayer
{
    public interface InvoiceNumberInterface
    {
        int Save(AbstractLayer.InvoiceNumberBase objUser);

        DataTable Select();

        DataTable InvoiceNumber();

        DataTable BookingInNumberExist(string strInvNumber);

        DataTable BookingDetails(string strInvNumber);

        DataTable InvoiceExist(string strInvNumber, string strProject, string strProcess, string strCategory);
    }
}