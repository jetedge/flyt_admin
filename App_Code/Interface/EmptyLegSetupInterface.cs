﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for EmptyLegSetupInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface EmptyLegSetupInterface
    {
        string InsertLivePricingCalculation(AbstractLayer.EmptyLegSetupBase objUser);

        DataTable BindEmptyLegs_SP(string strManaeType, string strFromDate, string strToDate, string strPageINdex, string strPageSize);

        DataTable GET_LIVEDATA(string strManaeType, string strFromDate, string strToDate, string strTail, string strTrip, string strTriplag);

        DataTable BindGroupData();

        DataTable BindGroupChildData(string strRowId);

        DataTable CountryList();

        DataSet LivePricing_SEL(string strTailNo, string strTripNo, string strStartAirport, string strEndAirport, string strGroupId, string strFlyingDate);

        DataTable Get_FutureData();

        string SplitDelete(string strGroupId);

        string ManualDelete(string strGroupId, string strTailNo, string strTripNo, string strAvailableFrom, string strStartTime, string strStartAirport, string strEndAirport);

        string SaveDepartureGroup(string strManageType, string strRowId, string strGroupName, string strFlag, string strCreatedBy);

        string DeleteAirportChild(string strGroupId);

        string DeleteAirport(string strRowID);

        string SaveAirportChild(string strManageType, string strGroupId, string strModifiedFlag, string strAirportFor, string strSetupBy, string strAirport,
            string strCreatedBy, string strStateCode, string strCountryCode, string strAirportCode);

        DataTable GroupExist(string strGroupName);

        string saveCategory(string[] arrCategory);

        string saveCategoryGroup(string[] arrCategory);

         
    }
}