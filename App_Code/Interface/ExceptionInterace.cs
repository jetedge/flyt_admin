﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for ExceptionInterace
/// </summary>

namespace DataLayer
{
    public interface ExceptionInterace
    {
        DataTable Select();
    }
}