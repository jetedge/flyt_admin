﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for UserPreferenceInterface
/// </summary>

namespace DataLayer
{
    public interface UserPreferenceInterface
    {
        int Update(AbstractLayer.UserPreferenceBase objUser);
        DataTable Select();
        DataTable Child(string strUserId);
        DataTable UserNameFilter(string strUserName);
        DataTable AirportFilter(string strAirport);
        DataTable DateFilter(string strDate);
        DataTable Edit(string strRowId);
        DataTable Preference_Report(string MansgeType, string UserName, string Airport, string FilterDate);

    }
}