﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for PrivacyEmailInterface
/// </summary>
namespace DataLayer
{
    public interface PrivacyEmailInterface
    {
        int Save(AbstractLayer.PrivacyEmailbase objPrivacy);
        DataTable Select();
        DataTable Edit(string strUserId);
        string DeletUser(string strUserId);
        DataTable SelectPR();
    }
}