﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for AirportMasterInterface
/// </summary>

namespace DataLayer
{
    public interface AirportMasterInterface
    {
        int Save(AbstractLayer.AirportMasterBase objUser);
        DataTable Edit(string strRowId);
        DataTable Select();
        DataTable AlreadyExist(string strICAO);
        string DeletUser(string strRowId);
    }
}