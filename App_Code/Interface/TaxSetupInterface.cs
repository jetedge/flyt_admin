﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

namespace DataLayer
{
    public interface TaxSetupInterface
    {
        int Save(AbstractLayer.TaxSetupBase objUser);
        DataTable Edit(string strRowId);
        DataTable History(string strTaxFlag, string strCode);
        DataSet Select();
    }
}