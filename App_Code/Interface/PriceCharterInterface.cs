﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
/// <summary>
/// Summary description for PriceCharterInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface PriceCharterInterface
    {
        DataTable Select();

        DataTable BindCharterPrice(string strTailNo, string strAvailableFrom, string strStart, string strEnd);
    }
}