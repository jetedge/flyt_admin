﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for CustomerInterface
/// </summary>
/// 
/// 
/// 
namespace DataLayer
{
    public interface  CustomerInterface
    {
        string AddNewCustomer(AbstractLayer.CreateCustomerBase objUser);

        string AddPreference(AbstractLayer.CreateCustomerBase objUser);
        DataTable AlreadyExist(string strEmailid, string RowId);
        DataTable ListAllCutomers(string strUserName);
        DataTable CouponDiscount();
        DataTable AirportMaster();
        DataSet Edit(string strUserId);
        DataTable GetEmptyTrips(string strPageINdex, string strRowID, string strFromDate, string strToDate, string strTail);

        DataTable GetLeg(string strTailNo, string strTripNo, string strAvailableFrom, string strStartTime, string strEndTime, string strStartAirport, string strEndAirport);
        string UpdateCoupon(string strUsers, string strCouponCode);
        string DeletePreference(string strEmailid);

        string Sync_CouponDiscount(string strUserId);
        string Sync_CouponDiscount_Delete(string strUserId);
    }
}