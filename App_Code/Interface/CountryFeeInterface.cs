﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for CountryFeeInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface CountryFeeInterface
    {
        string Save(AbstractLayer.CountryFeeBase objUser);
        DataTable Edit(string strUserId);
        DataTable Child(string strRowId);
        DataTable Select();
        string DeletUser(string strEmailid);
      
    }
}