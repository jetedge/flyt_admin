﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for ExceptionReportInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface ExceptionReportInterface
    {
         DataTable Select(string strManageType, string strFromDate, string strToDate);

         DataTable Booking(string strInvNumber);

         DataTable Child(string strManageType, string strFromDate, string strToDate, string strTailNo, string strTripNo);
    }
}