﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for TermsAndConditionInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface TermsAndConditionInterface
    {
        string Save(AbstractLayer.TermsAndConditionBase objUser);
        DataTable Edit(string strRowId);
        DataTable History(string strTaxFlag, string strCode);
        DataSet Select();
    }
}