﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for TaxSetupBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class TaxSetupBLL : TaxSetupBase
    {
        public int Save(AbstractLayer.TaxSetupBase objUser)
        {
            TaxSetupInterface MemberSqlDataAccess = new TaxSetupDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataSet Select()
        {
            DataSet dtSt;
            TaxSetupInterface MemberListSqlDataAccess = new TaxSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            TaxSetupInterface MemberListSqlDataAccess = new TaxSetupDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }

        public DataTable History(string strTaxFlag, string strCode)
        {
            DataTable dtEdit;
            TaxSetupInterface MemberListSqlDataAccess = new TaxSetupDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.History(strTaxFlag, strCode);
            return dtEdit;
        }
    }
}