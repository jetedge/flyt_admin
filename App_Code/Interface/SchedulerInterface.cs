﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for SchedulerInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface SchedulerInterface
    {
        int Save(AbstractLayer.SchedulerBase objUser);
        DataTable Select();
        DataTable BindEMAIl(string strSCHID);
        DataTable BindExternalUser(string strSCHID);
        DataTable BindSchedulePerticular(string strSchID);
        DataTable BindSchedulerStatus(string strSCHID);
        DataTable getToAddress(string strManageType);
        string UpdateRunFlag(string strSchID, string strFlag);
        string DeleteEMAIL(string strSchID);
        string SaveEMAIL(string strScHID, string strUserID);
        string SaveScheduleStatus(string strManageType, string strSchID, string strRunFlag, string strUserID);
        string SaveExternalUser(string strSchid, string strName, string strEmailid, string strUserRole, string strCreatedby);

        #region Customer Signup Scheduler
        DataTable Get_ReceiveEmailAlerts(string strManageType);
        DataTable Select_CustomerSignup_Sch();
        #endregion
    }
}