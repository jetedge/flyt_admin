﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;
/// <summary>
/// Last Modified By Prem to check format for function
/// </summary>
namespace DataLayer
{
    public interface LandingGalleryInterface
    {
        //save / update the admin user details
        int SaveGallery(AbstractLayer.LandingGalleryBase objGallery);

        //Edit the admin user details
        DataTable EditGallery(string strRowId);

        //Get the saved user list
        DataTable ListGallery(string strFlag);

        //Delete the user
        string DeleteGallery(string strRowId);
    }
}