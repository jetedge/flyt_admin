﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for DiscountandCouponInterface
/// </summary>
namespace DataLayer
{
    public interface DiscountandCouponInterface
    {
        int SaveDiscountandCoupon(DiscountandCouponBase objDiscount);
        DataTable ListDiscountandCoupon(string Status);
        DataTable EditDiscountandCoupon(string strRowId);
        DataTable AlreadyExist(string strCouponCode);
        DataTable List_RegisteredUser(string ManageType, string UserName, string Email, string DiscountType);
        int SaveCoupon_Users(DiscountandCouponBase objDiscount);
        int DeleteCoupon_Users(DiscountandCouponBase objDiscount);
        DataTable SelectCoupon_Users(string strRowId);
    }
}