﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;

/// <summary>
/// Summary description for FAQInterface
/// </summary>
/// 
namespace DataLayer
{
    public interface FAQInterface
    {
        string Save(AbstractLayer.FAQBase objUser);

        int SaveTopic(AbstractLayer.FAQBase objUser);

        DataTable Select();

        DataTable SelectTopic();
    }
}