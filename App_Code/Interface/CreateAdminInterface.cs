﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using System.Data;
/// <summary>
/// Last Modified By Prem to check format for function
/// </summary>
namespace DataLayer
{
    public interface CreateAdminInterface
    {
        //save / update the admin user details
        string Save(AbstractLayer.CreateAdminBase objUser);

        //Edit the admin user details
        DataTable Edit(string strUserId);

        //Get the saved user list
        DataTable List();

        //Check the email id already exist
        DataTable DuplicateCheck(string strEmailid);

        //Delete the user
        string Delete(string strEmailid);

        //Insert the admin user details into email setup
        string InsertEmailSetup(string strUserid, string strEmail, string strFirstName, string strLastName);

        DataTable Select_User_Login(string EmailId, string ManageType);
        
    }
}