﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace AbstractLayer
{
    /// <summary>
    /// Summary description for ExternalUserGlobalInterface
    /// </summary>
    public interface ExternalUserGlobalInterface
    {
        //save / update the admin user details
        string Save(AbstractLayer.ExternalUserGlobalBase objGlobal);

        //Get the saved user list
        DataTable Select();

        //Edit the admin user details
        DataTable Edit(string strUserId);

        //Delete the user
        string Delete(string strEmailid);

        string Save_DayDutyFilter(ExternalUserGlobalBase objGlobal);

        DataTable List_CrewDutyFilter();


        DataTable Bind_CustomFLYT();

        DataTable Bind_ForFlight();
    }
}