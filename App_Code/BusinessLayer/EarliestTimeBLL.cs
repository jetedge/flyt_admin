﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer;
using System.Data;
using System.Globalization;
using DataLayer;

/// <summary>
/// Summary description for BookedReportBLL
/// </summary>
/// 
namespace BusinessLayer
{

    public class EarliestTimeBLL
    {
        EarliestTimeDAL objEarliestTimeDAL = new EarliestTimeDAL();

        public void getTripsLeg(string strTripNo, string strLeg, string strTailNo, string flighthours,
            ref string MINDepartTime, ref string MAXDepartTime)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);

            double Dmini = Convert.ToDouble(flighthours) * 60;

            TimeSpan DspWorkMin = TimeSpan.FromMinutes(Dmini);
            string DworkHours = DspWorkMin.ToString(@"hh\.mm");

            int CurrentHours = Convert.ToInt32(DworkHours.Split('.')[0]);
            int CurrentMinutes = Convert.ToInt32(DworkHours.Split('.')[1]);


            string strStartTime = string.Empty;
            string strEndTime = string.Empty;
            string strAStartTime = string.Empty;
            string strAEndTime = string.Empty;
            string strTstartDate = string.Empty;
            string strTStartTime = string.Empty;
            string srAVALFlag = "Y";


            int PeviousLegTimeZone = 0;
            int NextlegTimeZone = 0;
            int offsetDiff = 0;


            DataSet ds = objEarliestTimeDAL.getTripsNextLeg(strTripNo, strLeg, strTailNo);
            if (ds.Tables[2].Rows.Count > 0)
            {
                strTstartDate = ds.Tables[2].Rows[0]["TDATE"].ToString();
                strTStartTime = ds.Tables[2].Rows[0]["EST"].ToString();
                PeviousLegTimeZone = Convert.ToInt32(ds.Tables[2].Rows[0]["STimeZone"]);
                NextlegTimeZone = Convert.ToInt32(ds.Tables[2].Rows[0]["ETimeZone"]);
                strEndTime = Convert.ToDateTime(ds.Tables[2].Rows[0]["StartTime"]).ToString();
                #region Find OffSet
                offsetDiff = PeviousLegTimeZone - NextlegTimeZone;
                #endregion



                //Start Date time
                if (ds.Tables[0].Rows.Count > 0)
                {
                    strStartTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndTime"].ToString()).AddHours(14).ToString();
                    strAStartTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndTime"].ToString()).ToString();
                    #region  //departure Time validation
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        if (ds.Tables[2].Rows[0]["Notes"].ToString().Contains("@"))
                        {
                            string[] str = ds.Tables[2].Rows[0]["Notes"].ToString().Split(' ')[0].Split('@');
                            if (str.Length == 2)
                            {
                                try
                                {
                                    string strDate = string.Empty;
                                    if (str[0].Length == 6)
                                        strDate = DateTime.ParseExact(str[0], "MMddyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");
                                    else if (str[0].Length == 8)
                                        strDate = DateTime.ParseExact(str[0], "MMddyyyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");

                                    string strTime = DateTime.ParseExact(str[1], "HHmm", DateTimeFormatInfo.InvariantInfo).ToString(@"HH:mm");

                                    if (Convert.ToDateTime(strDate).Date < System.DateTime.Now.Date)
                                    {
                                        strDate = System.DateTime.Now.ToString("MM/dd/yyyy");
                                        strTime = "00:00";
                                    }

                                    strStartTime = Convert.ToDateTime(strDate + " " + strTime).ToString();
                                    strAStartTime = Convert.ToDateTime(strDate + " " + strTime).ToString();


                                    // Convert.ToDateTime(str[0]).ToString("MM/dd/yyyy") + " " + str[1];
                                }
                                catch (Exception)
                                {
                                }
                            }
                            else if (str.Length == 3)
                            {
                                try
                                {
                                    string strDate = string.Empty;
                                    if (str[1].Length == 6)
                                        strDate = DateTime.ParseExact(str[1], "MMddyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");
                                    else if (str[1].Length == 8)
                                        strDate = DateTime.ParseExact(str[1], "MMddyyyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");


                                    string strTime = DateTime.ParseExact(str[2], "HHmm", DateTimeFormatInfo.InvariantInfo).ToString(@"HH:mm");

                                    if (Convert.ToDateTime(strDate).Date < System.DateTime.Now.Date)
                                    {
                                        strDate = System.DateTime.Now.ToString("MM/dd/yyyy");
                                        strTime = "00:00";
                                    }


                                    strStartTime = Convert.ToDateTime(strDate + " " + strTime).ToString();
                                    strAStartTime = Convert.ToDateTime(strDate + " " + strTime).ToString();

                                    // Convert.ToDateTime(str[0]).ToString("MM/dd/yyyy") + " " + str[1];
                                }
                                catch (Exception)
                                {
                                }

                            }

                        }
                    }

                    #endregion

                }
                else
                {
                    strStartTime = Convert.ToDateTime(strTstartDate).ToString("MM/dd/yyyy") + " 00:00";
                    strAStartTime = strStartTime;
                }

                //if (ds.Tables[1].Rows.Count > 0)
                //{
                //    strEndTime = Convert.ToDateTime(ds.Tables[1].Rows[0]["StartTime"]).AddHours(-1).ToString();
                //}
                //else
                //{
                //    strEndTime = Convert.ToDateTime(strTstartDate).ToString("MM/dd/yyyy") + " 23:30";
                //}

                //strEndTime = Convert.ToDateTime(strEndTime).AddHours(offsetDiff).AddHours(-CurrentHours).AddMinutes(-CurrentMinutes).ToString();

                if (Convert.ToDateTime(strEndTime) < Convert.ToDateTime(strStartTime))
                {
                    MINDepartTime = Convert.ToDateTime(strTstartDate).ToString("MMMM dd, yyyy") + " " + Convert.ToDateTime(strTStartTime).ToString("hh:mm tt");
                    MAXDepartTime = Convert.ToDateTime(strTstartDate).ToString("MMMM dd, yyyy") + " " + Convert.ToDateTime(strTStartTime).ToString("hh:mm tt");

                    //MINDepartTime = Convert.ToDateTime(strAStartTime).ToString("MMMM dd, yyyy hh:mm tt");
                    //MAXDepartTime = Convert.ToDateTime(strTstartDate).ToString("MMMM dd, yyyy") + " " + Convert.ToDateTime(strTStartTime).ToString("hh:mm tt");
                }
                else
                {
                    MINDepartTime = Convert.ToDateTime(strStartTime).ToString("MMMM dd, yyyy hh:mm tt");
                    MAXDepartTime = Convert.ToDateTime(strEndTime).ToString("MMMM dd, yyyy hh:mm tt");
                }
            }
        }



        //public void distanse(string strStartAirport, string strEndAirport)
        //{


        //    DataTable dtStart = genclass.GetDataTable("select Latitude,Longitude from JETEDGE_FUEL..[ST_AIRPORT] where ICAO='" + strStartAirport + "'");
        //    DataTable dtEnd = genclass.GetDataTable("select Latitude,Longitude from JETEDGE_FUEL..[ST_AIRPORT] where ICAO='" + strEndAirport + "'");

        //    double LAT1 = Convert.ToDouble(dtStart.Rows[0]["Latitude"].ToString());
        //    double LAT2 = Convert.ToDouble(dtEnd.Rows[0]["Latitude"].ToString());

        //    double LONG1 = Convert.ToDouble(dtStart.Rows[0]["Longitude"].ToString());
        //    double LONG2 = Convert.ToDouble(dtEnd.Rows[0]["Longitude"].ToString());





        //    double ds = ((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))) * 6378206 +
        //                              5405.6 * (3 * Math.Sin((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) - (Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) /
        //                              (1 + Math.Cos((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))))) * Math.Pow((Math.Sin(LAT1) * Math.Sin(LAT2)), 2) - 5405.6 * (3 * Math.Sin((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) *
        //                              (Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) / (1 - Math.Cos((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))))) * Math.Pow((Math.Sin(LAT1) - Math.Sin(LAT2)), 2)) * 0.0006213699 + 0.5;




        //}
    }
}