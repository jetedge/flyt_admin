﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for SchedulerBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class SchedulerBLL : SchedulerBase
    {
        public int Save(AbstractLayer.SchedulerBase objUser)
        {
            SchedulerInterface MemberSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }
        public DataTable BindEmail(string strSchId)
        {
            DataTable dtSt;
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindEMAIl(strSchId);
            return dtSt;
        }
        public DataTable BindExternalUser(string strSchId)
        {
            DataTable dtSt;
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindExternalUser(strSchId);
            return dtSt;
        }
        public DataTable BindSchedulePerticular(string strSchId)
        {
            DataTable dtSt;
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindSchedulePerticular(strSchId);
            return dtSt;
        }
        public DataTable BindSchedulerStatus(string strSchId)
        {
            DataTable dtSt;
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindSchedulerStatus(strSchId);
            return dtSt;
        }
        public DataTable getToAddress(string strmanageType)
        {
            DataTable dtSt;
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.getToAddress(strmanageType);
            return dtSt;
        }
        public string UpdateRunFlag(string strSchId,string strFlag)
        {
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.UpdateRunFlag(strSchId, strFlag);
        }
        public string DeleteEMAIL(string strSchId)
        {
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeleteEMAIL(strSchId);
        }
        public string SaveEmail(string strScHID, string strUserID)
        {
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.SaveEMAIL(strScHID, strUserID);
        }

        public string Save_ScheduleStatus(string strManageType, string strSchID, string strRunFlag, string strUserID)
        {
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.SaveScheduleStatus(strManageType, strSchID, strRunFlag, strUserID);
        }

        public string SaveExternalUser(string strSchid, string strName, string strEmailid, string strUserRole, string strCreatedby)
        {
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.SaveExternalUser(strSchid, strName, strEmailid, strUserRole, strCreatedby);
        }

        #region Customer Signup Scheduler
        public DataTable Get_ReceiveEmailAlerts(string strmanageType)
        {
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.Get_ReceiveEmailAlerts(strmanageType);
            return dtSt;
        }
        public DataTable Select_CustomerSignup_Sch()
        {
            SchedulerInterface MemberListSqlDataAccess = new SchedulerDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.Select_CustomerSignup_Sch();
            return dtSt;
        }
        #endregion
    }
}