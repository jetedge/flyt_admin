﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

/// <summary>
/// Summary description for EmptyLegPriceCal
/// </summary>
public class EmptyLegDataUpdateCal
{
    ZoomCalculatinDAL objZoomCalculatinDAL = new ZoomCalculatinDAL();
    int linenum = 0;
    string MethodName = string.Empty;

    public void FunEmptyPriceCal(DataTable dtOneWayTrip1)
    {
        string trquery = string.Empty;

        //string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
        //string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

        string strStartDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
        string strEndDate = DateTime.Now.AddDays(7).ToString("MM/dd/yyyy HH:mm:ss");

        DataTable dtOneWayTrip = objZoomCalculatinDAL.Get_EmptyTripSetup(strStartDate, strEndDate);



        //Update Flag is OLD
        objZoomCalculatinDAL.UpdateFLAG();

        #region Live Price Calculation

        DataTable dtMerge = dtOneWayTrip.Clone();
        if (dtOneWayTrip.Rows.Count > 0)
        {
            for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
            {
                decimal daystoflight = 0;
                decimal L10_minPrice = 0;
                decimal M10_maxPrice = 0;
                decimal price = 0;
                string strEDepartDate = string.Empty;
                string strEDepartTime = string.Empty;

                string strADepartDate = string.Empty;
                string strADepartTime = string.Empty;

                string strEraliestDepart = string.Empty;
                string MINDepartTime = string.Empty;
                string MAXDepartTime = string.Empty;
                int strDecreseAmount = 0;
                int stroldAmount = 0;

                string DeprtDate = dtOneWayTrip.Rows[i]["DeprtDate"].ToString();
                string DeprtTime = dtOneWayTrip.Rows[i]["DepartTime"].ToString();
                string ArrTime = dtOneWayTrip.Rows[i]["ArrTime"].ToString();

                //51704
                if (dtOneWayTrip.Rows[i]["TripNo"].ToString() == "12457")
                {

                }
                string tailNo = dtOneWayTrip.Rows[i]["TailNo"].ToString();
                string noOfSeats = dtOneWayTrip.Rows[i]["NoOFSeats"].ToString();

                var flyingDate = dtOneWayTrip.Rows[i]["DDate"].ToString();
                var strtTime = dtOneWayTrip.Rows[i]["DTime"].ToString();

                strEDepartDate = dtOneWayTrip.Rows[i]["DDate"].ToString();
                strEDepartTime = dtOneWayTrip.Rows[i]["DTime"].ToString();


                DataRow[] row = dtOneWayTrip1.Select("TripNo='" + dtOneWayTrip.Rows[i]["TripNo"].ToString() + "' and Leg='" + dtOneWayTrip.Rows[i]["Leg"].ToString() + "'");
                if (row.Length > 0)
                {
                    strDecreseAmount = Convert.ToInt32(row.CopyToDataTable().Rows[0]["DecreaseAmount"].ToString());
                    stroldAmount = Convert.ToInt32(row.CopyToDataTable().Rows[0]["OldAmount"].ToString());

                }




                var flyingtoDate = dtOneWayTrip.Rows[i]["ADate"].ToString();
                var endTime = dtOneWayTrip.Rows[i]["ATime"].ToString();


                strADepartDate = dtOneWayTrip.Rows[i]["ADate"].ToString();
                strADepartTime = dtOneWayTrip.Rows[i]["ATime"].ToString();


                var flyingHours = dtOneWayTrip.Rows[i]["FH"].ToString();
                string startCity = dtOneWayTrip.Rows[i]["StartCity"].ToString();
                string endCity = dtOneWayTrip.Rows[i]["EndCity"].ToString();
                string startAirport = dtOneWayTrip.Rows[i]["Start"].ToString();
                string endAirport = dtOneWayTrip.Rows[i]["End"].ToString();

                var HourlyRate = dtOneWayTrip.Rows[i]["HourlyRate"].ToString();
                var fixedCeiling = dtOneWayTrip.Rows[i]["FixedCeiling"].ToString();
                var fixedFloor = dtOneWayTrip.Rows[i]["FixedFloor"].ToString();
                var D4_MinTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MinTimeBD"].ToString());
                var D5_MaxTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MaxTimeBD"].ToString());
                var MinFLHSellable = dtOneWayTrip.Rows[i]["MinFLHSellable"].ToString();
                var applytax = dtOneWayTrip.Rows[i]["applytax"].ToString();
                var tripno = dtOneWayTrip.Rows[i]["tripno"].ToString();
                var enabletrip = dtOneWayTrip.Rows[i]["enabletrip"].ToString();
                var showtrip = dtOneWayTrip.Rows[i]["Active"].ToString();
                var TailFlag = dtOneWayTrip.Rows[i]["TailFlag"].ToString();
                var createdby = dtOneWayTrip.Rows[i]["createdby"].ToString();
                var createdon = dtOneWayTrip.Rows[i]["createdon"].ToString();
                var tripflag = dtOneWayTrip.Rows[i]["tripflag"].ToString();
                var groupid = dtOneWayTrip.Rows[i]["groupid"].ToString();
                var modifyflag = dtOneWayTrip.Rows[i]["ModifyFlag"].ToString();
                var leg = dtOneWayTrip.Rows[i]["leg"].ToString();
                var splittype = dtOneWayTrip.Rows[i]["Splittype"].ToString();

                var NewStartAirport = dtOneWayTrip.Rows[i]["NewStart"].ToString();
                var NewEndAirport = dtOneWayTrip.Rows[i]["NewEnd"].ToString();
                var LandingFlag = dtOneWayTrip.Rows[i]["LandingFlag"].ToString();

                #region DepartTime

                SqlParameter[] getTripsLegParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", false);
                getTripsLegParm[0].Value = "SEL";
                getTripsLegParm[1].Value = tripno;
                getTripsLegParm[2].Value = leg;
                getTripsLegParm[3].Value = tailNo;
                DataSet dsDate = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", getTripsLegParm);

                if (dsDate.Tables[2].Rows.Count > 0)
                {
                    if (dsDate.Tables[2].Rows[0]["Notes"].ToString().Contains("@"))
                    {
                        string[] str = dsDate.Tables[2].Rows[0]["Notes"].ToString().Split(' ')[0].Split('@');
                        if (str.Length == 2)
                        {
                            try
                            {
                                if (str[0].Length == 6)
                                    strEDepartDate = DateTime.ParseExact(str[0], "MMddyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");
                                else if (str[0].Length == 8)
                                    strEDepartDate = DateTime.ParseExact(str[0], "MMddyyyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");

                                strEDepartTime = DateTime.ParseExact(str[1], "HHmm", DateTimeFormatInfo.InvariantInfo).ToString(@"HH:mm");

                                strEraliestDepart = Convert.ToDateTime(strEDepartDate + " " + strEDepartTime).ToString();

                                if (modifyflag == "ET" || modifyflag == "MF" || modifyflag == "SP")
                                {
                                    strEDepartDate = flyingDate;
                                    strEDepartTime = strtTime;
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                        else if (str.Length == 3)
                        {
                            try
                            {
                                if (str[1].Length == 6)
                                    strEDepartDate = DateTime.ParseExact(str[1], "MMddyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");
                                else if (str[1].Length == 8)
                                    strEDepartDate = DateTime.ParseExact(str[1], "MMddyyyy", DateTimeFormatInfo.InvariantInfo).ToString("MM/dd/yyyy");

                                strEDepartTime = DateTime.ParseExact(str[2], "HHmm", DateTimeFormatInfo.InvariantInfo).ToString(@"HH:mm");

                                strEraliestDepart = Convert.ToDateTime(strEDepartDate + " " + strEDepartTime).ToString();
                                if (modifyflag == "ET" || modifyflag == "MF" || modifyflag == "SP")
                                {
                                    strEDepartDate = flyingDate;
                                    strEDepartTime = strtTime;
                                }
                            }
                            catch (Exception)
                            {
                            }

                        }

                    }
                }

                DateTime strETAvailableFrom = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00");
                DateTime strETAvailableTo = Convert.ToDateTime(Convert.ToDateTime(flyingtoDate).Date.ToString("yyyy-MM-dd") + ' ' + endTime + ":00");

                getTripsLeg(tripno, leg, tailNo,
                            ref   MINDepartTime, ref MAXDepartTime, dtOneWayTrip.Rows[i]["ModifyFlag"].ToString(),
                strETAvailableFrom.ToString(), strETAvailableTo.ToString(), strEraliestDepart, flyingHours);

                if (strEraliestDepart == string.Empty)
                {
                    strEDepartDate = Convert.ToDateTime(MINDepartTime).Date.ToString();
                    strEDepartTime = Convert.ToDateTime(MINDepartTime).ToString("HH:mm");

                    if (Convert.ToDateTime(Convert.ToDateTime(strADepartDate).Date.ToString("yyyy-MM-dd") + ' ' + strADepartTime + ":00") < Convert.ToDateTime(MINDepartTime))
                    {
                        strEDepartDate = Convert.ToDateTime(strADepartDate).Date.ToString("yyyy-MM-dd");
                        strEDepartTime = strADepartTime;
                    }

                    //if (Convert.ToDateTime(strETAvailableFrom).Date > Convert.ToDateTime(MINDepartTime).Date)
                    //{
                    //    MINDepartTime = Convert.ToDateTime(strETAvailableFrom).ToString("MM/dd/yyyy") + " " + "00:00";
                    //}
                }


                DateTime dtLocalTime = GETLOCALDATETIME(dtOneWayTrip.Rows[i]["Start"].ToString(), dtOneWayTrip.Rows[i]["End"].ToString(), dtOneWayTrip.Rows[i]["TailNo"].ToString());
                //dtLocalTime = dtLocalTime.AddDays(1);

                if (dtLocalTime.Date > Convert.ToDateTime(strEDepartDate).Date)
                {
                    strEDepartDate = dtLocalTime.Date.ToString();
                    strEDepartTime = dtLocalTime.ToString("HH:mm");
                }


                if (modifyflag != "ET" && modifyflag != "MF" && modifyflag != "SP")
                {
                    strADepartDate = Convert.ToDateTime(flyingDate).ToString("MM/dd/yyyy");
                    strADepartTime = strtTime;
                }


                //if (modifyflag != "ET" && modifyflag != "MF" && modifyflag != "SP")
                //{
                //    if (MAXDepartTime != string.Empty)
                //    {
                //        strADepartDate = Convert.ToDateTime(MAXDepartTime).ToString("MM/dd/yyyy");
                //        strADepartTime = Convert.ToDateTime(MAXDepartTime).ToString("HH:mm");
                //    }
                //}
                //if (Convert.ToDateTime(strETAvailableTo).Date < Convert.ToDateTime(MAXDepartTime).Date)
                //{
                //    MAXDepartTime = Convert.ToDateTime(strETAvailableFrom).ToString("MM/dd/yyyy") + " " + "23:59";
                //}

                #endregion

                if (dtOneWayTrip.Rows[i]["HourlyRate"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedCeiling"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedFloor"].ToString() != "0.00")
                {

                    if (Convert.ToDateTime(Convert.ToDateTime(strADepartDate).Date.ToString("yyyy-MM-dd") + ' ' + strADepartTime + ":00") > Convert.ToDateTime(strEndDate))
                    {
                        strADepartDate = Convert.ToDateTime(strEndDate).ToString("MM/dd/yyyy");
                        strADepartTime = Convert.ToDateTime(strEndDate).ToString("HH:mm");
                    }

                    var finalPrice = 0;
                    string CBPF = string.Empty;
                    var finalPricevalidation2 = (decimal)0;
                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(strADepartDate).Date.ToString("yyyy-MM-dd") + ' ' + strADepartTime + ":00").Subtract(DateTime.Now);
                    var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                    decimal totalhrs = Convert.ToDecimal(flyingHours);

                    L10_minPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs));

                    if (L10_minPrice < Convert.ToDecimal(fixedFloor))
                    {
                        L10_minPrice = Convert.ToDecimal(fixedFloor);
                        CBPF = "FIXEDCEILING";
                    }

                    M10_maxPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs)) + Convert.ToDecimal(fixedCeiling);

                    if (stroldAmount != 0)
                    {
                        finalPrice = stroldAmount;
                    }
                    else
                    {
                        finalPrice = Convert.ToInt32(((1 - (D5_MaxTimeBD - Convert.ToDecimal(_totalDaysK10)) / (D5_MaxTimeBD - Convert.ToDecimal(D4_MinTimeBD))) * (M10_maxPrice - L10_minPrice) + L10_minPrice) / 25) * 25;
                        strDecreseAmount = Convert.ToInt32((M10_maxPrice - L10_minPrice) / (96 * 7));
                    }





                    int mul1 = 0;
                    if (Convert.ToDecimal(_totalDaysK10) < D4_MinTimeBD)
                    {
                        mul1 = 0;
                    }
                    else
                        mul1 = 1;

                    string mul2 = "0";
                    if (Convert.ToDecimal(Convert.ToDecimal(totalhrs)) >= Convert.ToDecimal(MinFLHSellable))
                    {
                        mul2 = "1";
                    }
                    else
                        mul2 = "ERROR";


                    if (Convert.ToDateTime(MAXDepartTime) < dtLocalTime)
                    {
                        mul1 = 0;
                    }

                    if (mul2 != "ERROR")
                    {
                        finalPricevalidation2 = Convert.ToDecimal(finalPrice) * Convert.ToDecimal(mul2) * Convert.ToDecimal(mul1);
                    }

                    DataTable dtspecialpricing = objZoomCalculatinDAL.SpeicalPricing();
                    DataRow[] drspecial = dtspecialpricing.Select("#" + DeprtDate + "# >= StartDate AND EndDate >= #" + DeprtDate + "#" + "and startAirport='" + startAirport + "' and endairport ='" + endAirport + "'");
                    DataTable dtselection = new DataTable();
                    string Specialflag = string.Empty;
                    if (drspecial.Length > 0)
                    {
                        dtselection = drspecial.CopyToDataTable();
                        if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "H")
                        {
                            finalPricevalidation2 = Convert.ToDecimal(flyingHours) * Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                            Specialflag = "(Special Price)";
                        }
                        else if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "D")
                        {
                            finalPricevalidation2 = Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                            Specialflag = "(Special Price)";
                        }
                    }


                    string s = DateTime.Now.ToString("HH:mm:ss.fff");
                    TimeSpan ts = TimeSpan.Parse(s);
                    string time = ts.ToString();
                    string Date = DateTime.Now.Date.ToString();
                    if (Specialflag == "(Special Price)")
                    {
                        DataTable dtspecialCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, " select * from LivePricingCaculation Where flag='FINAL' and Priceflag='(Special Price)' and TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' ").Tables[0];
                        if (dtspecialCheck.Rows.Count == 0)
                        {
                            string[] srr = new string[50];
                            srr[0] = tailNo;
                            srr[1] = strEDepartDate;
                            srr[2] = strADepartDate;
                            srr[3] = strEDepartTime;
                            srr[4] = strADepartTime;
                            srr[5] = startAirport;
                            srr[6] = startCity.Replace("'", "''");
                            srr[7] = endAirport;
                            srr[8] = endCity.Replace("'", "''");
                            srr[9] = noOfSeats;
                            srr[10] = totalhrs.ToString();
                            srr[11] = totalhrs.ToString();
                            srr[12] = HourlyRate;
                            srr[13] = fixedFloor;
                            srr[14] = fixedCeiling;
                            srr[15] = D4_MinTimeBD.ToString();
                            srr[16] = D5_MaxTimeBD.ToString();
                            srr[17] = MinFLHSellable;
                            srr[18] = _totalDaysK10;
                            srr[19] = L10_minPrice.ToString();
                            srr[20] = M10_maxPrice.ToString();
                            srr[21] = finalPricevalidation2.ToString();
                            srr[22] = time;
                            srr[23] = Date;
                            srr[24] = "FINAL";
                            srr[25] = CBPF;
                            srr[26] = "0";
                            srr[27] = applytax;
                            srr[28] = tripno;
                            srr[29] = enabletrip;
                            srr[30] = showtrip;
                            srr[31] = Specialflag;
                            srr[32] = TailFlag;
                            srr[33] = createdby;
                            if (createdon == string.Empty)
                                srr[34] = null;
                            else
                                srr[34] = createdon;
                            srr[35] = tripflag;
                            srr[36] = groupid;
                            srr[37] = modifyflag;
                            srr[38] = leg;
                            srr[39] = splittype;
                            srr[40] = NewStartAirport.Replace("'", "''");
                            srr[41] = NewEndAirport.Replace("'", "''");
                            if (strEraliestDepart == string.Empty)
                                srr[42] = null;
                            else
                                srr[42] = strEraliestDepart;

                            if (MINDepartTime == string.Empty)
                                srr[43] = null;
                            else
                                srr[43] = MINDepartTime;

                            if (MAXDepartTime == string.Empty)
                                srr[44] = null;
                            else
                                srr[44] = MAXDepartTime;

                            srr[45] = LandingFlag;
                            srr[46] = strDecreseAmount.ToString();


                            objZoomCalculatinDAL.InsertLive(srr);

                            if (finalPricevalidation2 > 0)
                            {
                                objZoomCalculatinDAL.UpdateShowPrice("NEW", tailNo, tripno, flyingDate);
                            }
                            else
                            {
                                objZoomCalculatinDAL.UpdateShowPrice("EXP", tailNo, tripno, flyingDate);
                            }
                        }
                    }
                    else
                    {
                        string[] srr = new string[50];
                        srr[0] = tailNo;
                        srr[1] = strEDepartDate;
                        srr[2] = strADepartDate;
                        srr[3] = strEDepartTime;
                        srr[4] = strADepartTime;
                        srr[5] = startAirport;
                        srr[6] = startCity.Replace("'", "''");
                        srr[7] = endAirport;
                        srr[8] = endCity.Replace("'", "''");
                        srr[9] = noOfSeats;
                        srr[10] = totalhrs.ToString();
                        srr[11] = totalhrs.ToString();
                        srr[12] = HourlyRate;
                        srr[13] = fixedFloor;
                        srr[14] = fixedCeiling;
                        srr[15] = D4_MinTimeBD.ToString();
                        srr[16] = D5_MaxTimeBD.ToString();
                        srr[17] = MinFLHSellable;
                        srr[18] = _totalDaysK10;
                        srr[19] = L10_minPrice.ToString();
                        srr[20] = M10_maxPrice.ToString();
                        srr[21] = finalPricevalidation2.ToString();
                        srr[22] = time;
                        srr[23] = Date;
                        srr[24] = "FINAL";
                        srr[25] = CBPF;
                        srr[26] = "0";
                        srr[27] = applytax;
                        srr[28] = tripno;
                        srr[29] = enabletrip;
                        srr[30] = showtrip;
                        srr[31] = Specialflag;
                        srr[32] = TailFlag;
                        srr[33] = createdby;
                        if (createdon == string.Empty)
                            srr[34] = null;
                        else
                            srr[34] = createdon;
                        srr[35] = tripflag;
                        srr[36] = groupid;
                        srr[37] = modifyflag;
                        srr[38] = leg;
                        srr[39] = splittype;
                        srr[40] = NewStartAirport.Replace("'", "''");
                        srr[41] = NewEndAirport.Replace("'", "''");
                        if (strEraliestDepart == string.Empty)
                            srr[42] = null;
                        else
                            srr[42] = strEraliestDepart;

                        if (MINDepartTime == string.Empty)
                            srr[43] = null;
                        else
                            srr[43] = MINDepartTime;

                        if (MAXDepartTime == string.Empty)
                            srr[44] = null;
                        else
                            srr[44] = MAXDepartTime;

                        srr[45] = LandingFlag;
                        srr[46] = strDecreseAmount.ToString();
                        objZoomCalculatinDAL.InsertLive(srr);

                        if (finalPricevalidation2 > 0)
                        {
                            objZoomCalculatinDAL.UpdateShowPrice("NEW", tailNo, tripno, flyingDate);

                        }
                        else
                        {
                            objZoomCalculatinDAL.UpdateShowPrice("EXP", tailNo, tripno, flyingDate);

                        }
                    }
                }
            }
        }
        #endregion

        #region CD Flag
        ZoomCalculatinDAL.UpdateCDFlag();
        #endregion
    }


    public void getTripsLeg(string strTripNo, string strLeg, string strTailNo,
   ref string MINDepartTime, ref string MAXDepartTime, string modifyflag,
          string strETAvailableFrom, string strETAvailableTo, string strEarliestTime, string strFlightHours)
    {
        string strStartTime = string.Empty;
        string strEndTime = string.Empty;
        string strAStartTime = string.Empty;
        string strAEndTime = string.Empty;
        string strTstartDate = string.Empty;
        string strTStartTime = string.Empty;
        int CurrentHours = 0;
        int CurrentMinutes = 0;
        string[] flyhours = strFlightHours.Split('.');
        if (flyhours.Length > 1)
        {
            if (Convert.ToInt32(flyhours[0]) > 1)
            {
                CurrentHours = Convert.ToInt32(flyhours[0]);
            }

            if (Convert.ToInt32(flyhours[1]) > 1)
            {
                CurrentMinutes = Convert.ToInt32(flyhours[1]);
            }

        }

        if (modifyflag == "ET" || modifyflag == "SP" || modifyflag == "MF")
        {
            strStartTime = Convert.ToDateTime(strETAvailableFrom).ToString("MM/dd/yyyy HH:mm");
            strEndTime = Convert.ToDateTime(strETAvailableTo).ToString("MM/dd/yyyy HH:mm"); ;
            MINDepartTime = Convert.ToDateTime(strStartTime).ToString("MMMM dd, yyyy hh:mm tt");
            MAXDepartTime = Convert.ToDateTime(strEndTime).ToString("MMMM dd, yyyy hh:mm tt");
        }
        else if (modifyflag == "SP")
        {
            strStartTime = Convert.ToDateTime(strETAvailableFrom).ToString("MM/dd/yyyy HH:mm");
            strEndTime = Convert.ToDateTime(strETAvailableTo).ToString("MM/dd/yyyy HH:mm"); ;

            MINDepartTime = Convert.ToDateTime(strStartTime).ToString("MMMM dd, yyyy hh:mm tt");
            MAXDepartTime = Convert.ToDateTime(strEndTime).ToString("MMMM dd, yyyy hh:mm tt");
        }
        else if (modifyflag == "MF")
        {
            strStartTime = Convert.ToDateTime(strETAvailableFrom).ToString("MM/dd/yyyy HH:mm");
            strEndTime = Convert.ToDateTime(strETAvailableTo).ToString("MM/dd/yyyy HH:mm"); ;

            MINDepartTime = Convert.ToDateTime(strStartTime).ToString("MMMM dd, yyyy hh:mm tt");
            MAXDepartTime = Convert.ToDateTime(strEndTime).ToString("MMMM dd, yyyy hh:mm tt");
        }
        else
        {
            int PeviousLegTimeZone = 0;
            int NextlegTimeZone = 0;
            int offsetDiff = 0;

            DataSet ds = objZoomCalculatinDAL.getTripsNextLeg(strTripNo, strLeg, strTailNo);
            if (ds.Tables[2].Rows.Count > 0)
            {
                strTstartDate = ds.Tables[2].Rows[0]["TDATE"].ToString();
                strTStartTime = ds.Tables[2].Rows[0]["EST"].ToString();
                PeviousLegTimeZone = Convert.ToInt32(ds.Tables[2].Rows[0]["STimeZone"]);
                NextlegTimeZone = Convert.ToInt32(ds.Tables[2].Rows[0]["ETimeZone"]);
                strEndTime = Convert.ToDateTime(ds.Tables[2].Rows[0]["StartTime"]).ToString();
                #region Find OffSet
                offsetDiff = PeviousLegTimeZone - NextlegTimeZone;
                #endregion


                //Start Date time
                if (ds.Tables[0].Rows.Count > 0)
                {
                    strStartTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndTime"].ToString()).AddHours(14).ToString();
                    strAStartTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndTime"].ToString()).ToString();

                    if (strEarliestTime != string.Empty)
                    {
                        string strDate = Convert.ToDateTime(strEarliestTime).ToString("MM/dd/yyyy");
                        string strTime = Convert.ToDateTime(strEarliestTime).ToString(@"HH:mm");

                        if (Convert.ToDateTime(strDate).Date < System.DateTime.Now.Date)
                        {
                            strDate = System.DateTime.Now.ToString("MM/dd/yyyy");
                            strTime = "00:00";
                        }

                        strStartTime = Convert.ToDateTime(strDate + " " + strTime).ToString();
                        strAStartTime = Convert.ToDateTime(strDate + " " + strTime).ToString();
                    }
                }
                else
                {
                    strStartTime = Convert.ToDateTime(strTstartDate).ToString("MM/dd/yyyy") + " 00:00";
                    strAStartTime = strStartTime;
                }

                //if (ds.Tables[1].Rows.Count > 0)
                //{
                //    strEndTime = Convert.ToDateTime(ds.Tables[1].Rows[0]["StartTime"]).AddHours(-1).ToString();
                //}
                //else
                //{
                //    strEndTime = Convert.ToDateTime(strTstartDate).ToString("MM/dd/yyyy") + " 23:30";
                //}

                //strEndTime = Convert.ToDateTime(strEndTime).AddHours(offsetDiff).AddHours(-CurrentHours).AddMinutes(-CurrentMinutes).ToString();

            }
            else
            {

                strStartTime = Convert.ToDateTime(strETAvailableFrom).ToString("MM/dd/yyyy HH:mm");
                strEndTime = Convert.ToDateTime(strETAvailableTo).ToString("MM/dd/yyyy HH:mm"); ;

                MINDepartTime = Convert.ToDateTime(strStartTime).ToString("MMMM dd, yyyy hh:mm tt");
                MAXDepartTime = Convert.ToDateTime(strEndTime).ToString("MMMM dd, yyyy hh:mm tt");
            }
            if (Convert.ToDateTime(strEndTime) < Convert.ToDateTime(strStartTime))
            {
                MINDepartTime = Convert.ToDateTime(strTstartDate).ToString("MMMM dd, yyyy") + " " + Convert.ToDateTime(strTStartTime).ToString("hh:mm tt");
                MAXDepartTime = Convert.ToDateTime(strTstartDate).ToString("MMMM dd, yyyy") + " " + Convert.ToDateTime(strTStartTime).ToString("hh:mm tt");

                //MINDepartTime = Convert.ToDateTime(strAStartTime).ToString("MMMM dd, yyyy hh:mm tt");
                //MAXDepartTime = Convert.ToDateTime(strTstartDate).ToString("MMMM dd, yyyy") + " " + Convert.ToDateTime(strTStartTime).ToString("hh:mm tt");
            }
            else
            {
                MINDepartTime = Convert.ToDateTime(strStartTime).ToString("MMMM dd, yyyy hh:mm tt");
                MAXDepartTime = Convert.ToDateTime(strEndTime).ToString("MMMM dd, yyyy hh:mm tt");
            }



        }


    }




    public void PriceUpdate(string strTrip, string strLeg, int strRevisedAmount, string oldAmount, string userID, string strNotes)
    {

        DataTable dtOneWayTrip = objZoomCalculatinDAL.Get_EmptyTripSetupTrip(strTrip, strLeg).Tables[0];

        #region Live Price Calculation


        if (dtOneWayTrip.Rows.Count > 0)
        {

            decimal L10_minPrice = 0;
            decimal M10_maxPrice = 0;
            int strDecreseAmount = 0;
            string strADepartDate = string.Empty;
            string strADepartTime = string.Empty;


            strADepartDate = dtOneWayTrip.Rows[0]["ADate"].ToString();
            strADepartTime = dtOneWayTrip.Rows[0]["ATime"].ToString();
            var flyingHours = dtOneWayTrip.Rows[0]["FH"].ToString();
            var HourlyRate = dtOneWayTrip.Rows[0]["HourlyRate"].ToString();
            var fixedCeiling = dtOneWayTrip.Rows[0]["FixedCeiling"].ToString();
            var fixedFloor = dtOneWayTrip.Rows[0]["FixedFloor"].ToString();
            var D4_MinTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[0]["MinTimeBD"].ToString());
            var D5_MaxTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[0]["MaxTimeBD"].ToString());
            var MinFLHSellable = dtOneWayTrip.Rows[0]["MinFLHSellable"].ToString();

            var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(strADepartDate).Date.ToString("yyyy-MM-dd") + ' ' + strADepartTime + ":00").Subtract(DateTime.Now);
            var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

            decimal totalhrs = Convert.ToDecimal(flyingHours);

            L10_minPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs));

            if (L10_minPrice < Convert.ToDecimal(fixedFloor))
            {
                L10_minPrice = Convert.ToDecimal(fixedFloor);
            }

            M10_maxPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs)) + Convert.ToDecimal(fixedCeiling);

            strDecreseAmount = Convert.ToInt32((strRevisedAmount - L10_minPrice) / (96 * Convert.ToDecimal(_totalDaysK10)));

            objZoomCalculatinDAL.UpdateRevisedAmount(strTrip, strLeg, strRevisedAmount.ToString(), strDecreseAmount.ToString(), oldAmount, userID, strNotes);



        }

        #endregion


    }
    public static DateTime GETLOCALDATETIME(string startAirport, string strEndAirprot, string strTail)
    {
        string strEndTimeZone = string.Empty;
        string strStartTimeZone = string.Empty;

        TimeZone curTimeZone = TimeZone.CurrentTimeZone;
        TimeSpan currentOffset = curTimeZone.GetUtcOffset(DateTime.Now);

        DataSet dsTimeZoom = ZoomCalculatinDAL.getTimeZone(startAirport.Trim(), strEndAirprot.Trim());

        if (dsTimeZoom.Tables[0].Rows.Count > 0)
        {
            strEndTimeZone = dsTimeZoom.Tables[0].Rows[0]["TimeZone"].ToString();
        }
        strStartTimeZone = currentOffset.Hours.ToString();

        int offsetDiff = 0;
        if (strEndTimeZone.Length > 0)
        {
            offsetDiff = Convert.ToInt32(strEndTimeZone) - Convert.ToInt32(strStartTimeZone);
        }


        string days = "0";

        DataTable dtDays = ZoomCalculatinDAL.getLocalTime(strTail);

        if (dtDays.Rows.Count > 0)
        {
            days = Convert.ToDecimal(dtDays.Rows[0][0]).ToString("00");
        }

        return System.DateTime.Now.AddHours(offsetDiff).AddHours(Convert.ToInt32(days));




    }
}