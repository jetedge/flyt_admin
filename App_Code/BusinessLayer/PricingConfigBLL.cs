﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for PricingConfigBLL
/// </summary>
namespace BusinessLayer
{
    public class PricingConfigBLL : PricingConfigBase
    {
        public DataTable Select()
        {
            DataTable dtSt;
            PricingConfigInterface MemberListSqlDataAccess = new PricingConfigDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }
        public DataTable FilterGrid(string strTail)
        {
            DataTable dtExist;
            PricingConfigInterface MemberListSqlDataAccess = new PricingConfigDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.FilterGrid(strTail);
            return dtExist;
        }
        public DataTable Edit(string strRowID)
        {
            DataTable dtExist;
            PricingConfigInterface MemberListSqlDataAccess = new PricingConfigDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.Edit(strRowID);
            return dtExist;
        }
        public DataTable TailDetails(string strRowID)
        {
            DataTable dtExist;
            PricingConfigInterface MemberListSqlDataAccess = new PricingConfigDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.TailDetails(strRowID);
            return dtExist;
        }
        public DataTable PopDetails(string strTripNo, string strTailNo, string strAvailableFrom, string strStartTime)
        {
            DataTable dtExist;
            PricingConfigInterface MemberListSqlDataAccess = new PricingConfigDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.PopDetails(strTripNo, strTailNo, strAvailableFrom, strStartTime);
            return dtExist;
        }

        public string Insert_TailImages(string strTailNo, string FilePath, string InteriorImage, string ExteriorImage, string UploadedBy)
        {
            PricingConfigInterface MemberListSqlDataAccess = new PricingConfigDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.Insert_TailImages(strTailNo, FilePath, InteriorImage, ExteriorImage, UploadedBy);
        }
        public DataTable Select_TailImages(string strTripNo)
        {
            DataTable dtExist;
            PricingConfigInterface MemberListSqlDataAccess = new PricingConfigDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.Select_TailImages(strTripNo);
            return dtExist;
        }
    }
}