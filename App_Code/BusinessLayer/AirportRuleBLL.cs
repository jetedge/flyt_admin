﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for AirportRuleBLL
/// </summary>
namespace BusinessLayer
{
    public class AirportRuleBLL : AirportRuleBase
    {
        public int Save(AirportRuleBase objAirportRule)
        {
            AirportRuleInterface MemberSqlDataAccess = new AirportRuleDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objAirportRule);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            AirportRuleInterface MemberListSqlDataAccess = new AirportRuleDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }
        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            AirportRuleInterface MemberListSqlDataAccess = new AirportRuleDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }
        public string DeletAirportRule(string strUserId)
        {
            AirportRuleInterface MemberListSqlDataAccess = new AirportRuleDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeletAirportRule(strUserId);
        }
    }
}