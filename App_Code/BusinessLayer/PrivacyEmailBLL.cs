﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;


/// <summary>
/// Summary description for PrivacyEmailBLL
/// </summary>
namespace BusinessLayer
{
    public class PrivacyEmailBLL : PrivacyEmailbase
    {
        public int Save(AbstractLayer.PrivacyEmailbase objPrivacy)
        {
            PrivacyEmailInterface MemberSqlDataAccess = new PrivacyEmailDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objPrivacy);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            PrivacyEmailInterface MemberListSqlDataAccess = new PrivacyEmailDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            PrivacyEmailInterface MemberListSqlDataAccess = new PrivacyEmailDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }


        public string DeletUser(string strUserId)
        {
            PrivacyEmailInterface MemberListSqlDataAccess = new PrivacyEmailDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeletUser(strUserId);
        }

        public DataTable SelectPR()
        {
            DataTable dtSt;
            PrivacyEmailInterface MemberListSqlDataAccess = new PrivacyEmailDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.SelectPR();
            return dtSt;
        }

    }
}