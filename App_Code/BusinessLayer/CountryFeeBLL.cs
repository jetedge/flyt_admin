﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for CountryFeeBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class CountryFeeBLL : CountryFeeBase
    {
        public string Save(AbstractLayer.CountryFeeBase objUser)
        {
            CountryFeeInterface MemberSqlDataAccess = new CountryFeeDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            CountryFeeInterface MemberListSqlDataAccess = new CountryFeeDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            CountryFeeInterface MemberListSqlDataAccess = new CountryFeeDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }

        public DataTable Child(string strRowId)
        {
            DataTable dtEdit;
            CountryFeeInterface MemberListSqlDataAccess = new CountryFeeDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Child(strRowId);
            return dtEdit;
        }

        public string DeletUser(string strUserId)
        {
            CountryFeeInterface MemberListSqlDataAccess = new CountryFeeDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeletUser(strUserId);
        }
    }
}