﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


/// <summary>
/// Summary description for UserPreferenceBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class UserPreferenceBLL : UserPreferenceBase
    {

        public int Update(AbstractLayer.UserPreferenceBase objUser)
        {
            UserPreferenceInterface MemberSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Update(objUser);
        }

        public DataTable Preference_Report(string MansgeType, string UserName, string Airport, string FilterDate)
        {
            UserPreferenceInterface MemberListSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.Preference_Report(MansgeType, UserName, Airport, FilterDate);
            return dtSt;
        }

        public DataTable Select()
        {
            DataTable dtSt;
            UserPreferenceInterface MemberListSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable Child(string strUserID)
        {
            DataTable dtSt;
            UserPreferenceInterface MemberListSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Child(strUserID);
            return dtSt;
        }

        public DataTable UserNameFilter(string strUserName)
        {
            DataTable dtSt;
            UserPreferenceInterface MemberListSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.UserNameFilter(strUserName);
            return dtSt;
        }

        public DataTable AirportFilter(string strAirport)
        {
            DataTable dtSt;
            UserPreferenceInterface MemberListSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.AirportFilter(strAirport);
            return dtSt;
        }

        public DataTable DateFilter(string strDate)
        {
            DataTable dtSt;
            UserPreferenceInterface MemberListSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.DateFilter(strDate);
            return dtSt;
        }

        public DataTable Edit(string strRowId)
        {
            DataTable dtSt;
            UserPreferenceInterface MemberListSqlDataAccess = new UserPreferenceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Edit(strRowId);
            return dtSt;
        }

        #region Export Excel

        public static object GetUrl(string domain)
        {
            if (domain == "localhost")
                return "J:\\jetedgeMgnt";
            else
                return "D:\\jetedgefiles";
        }

        public void Exportexcel(GridView gvPreference, string strFileName, string[] arrlblPageHits, string strFromDate, string strUserName, string SheetName)
        {
            string fileName = strFileName + ".xlsx";
            object fileUrl = GetUrl(HttpContext.Current.Request.Url.Host);

            if (fileUrl != null && fileUrl != string.Empty)
            {
                bool exists = Directory.Exists(fileUrl.ToString());
                if (!exists)
                {
                    Directory.CreateDirectory(fileUrl.ToString());
                }

                bool vendor = Directory.Exists(fileUrl.ToString() + @"\Temporary");
                if (!vendor)
                {
                    Directory.CreateDirectory(fileUrl.ToString() + @"\Temporary");
                }
            }

            string targetPath = fileUrl.ToString() + @"\Temporary";
            string destFile = System.IO.Path.Combine(targetPath, fileName);
            var existingFile = new FileInfo(destFile);

            if (existingFile.Exists)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                existingFile.Delete();
            }

            using (ExcelPackage xlPackage = new ExcelPackage(existingFile))
            {
                funexcel(xlPackage, gvPreference, arrlblPageHits, strFileName, strUserName, strFromDate, SheetName);

                xlPackage.Save();
                Download_Click(destFile);
            }
        }

        private void Download_Click(string destFile)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(destFile);
            if (file.Exists)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.WriteFile(file.FullName);
                HttpContext.Current.Response.End();
            }
        }

        private void funexcel(ExcelPackage xlPackage, GridView gvSummary, string[] labelSummary, string strFileName,
                string strUserName, string strFromDate, string SheetName)
        {
            try
            {



                int rowExcel = 1;
                var ws = xlPackage.Workbook.Worksheets.Add(SheetName);

                ws.Cells[1, 1].Value = "Jet Edge - FLYT";
                ws.Cells[1, 1].Style.Font.Bold = true;
                ws.Cells[1, 1].Style.Font.Size = 18;
                ws.Cells[1, 1, 1, labelSummary.Length].Merge = true;
                ws.Cells[1, 1, 1, labelSummary.Length].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#BFBFBF");
                ws.Cells[1, 1, 1, labelSummary.Length].Style.Fill.BackgroundColor.SetColor(colFromHex);

                ws.Cells[2, 1].Value = "Report generated by : " + strUserName + " On " + DateTime.Now.ToString();
                ws.Cells[2, 1, 2, labelSummary.Length].Merge = true;
                ws.Cells[2, 1, 2, labelSummary.Length].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#D8D8D8");
                ws.Cells[2, 1, 2, labelSummary.Length].Style.Fill.BackgroundColor.SetColor(colFromHex1);

                ws.Cells[3, 1].Value = "User Preferences Report";
                ws.Cells[3, 1, 3, labelSummary.Length].Merge = true;
                ws.Cells[3, 1, 3, labelSummary.Length].Style.Fill.PatternType = ExcelFillStyle.Solid;
                Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#F2F2F2");
                ws.Cells[3, 1, 3, labelSummary.Length].Style.Fill.BackgroundColor.SetColor(colFromHex2);

                rowExcel = 5;
                if (gvSummary.Rows.Count > 0)
                {
                    #region write Excel grid row greater then 0
                    int Count = 0;
                    for (int j = 0; j < gvSummary.HeaderRow.Cells.Count; j++)
                    {
                        if (gvSummary.Columns[j].Visible == true)
                        {
                            ws.Cells[rowExcel, Count + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells[rowExcel, Count + 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                            ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ws.Cells[rowExcel, Count + 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            ws.Cells[rowExcel, Count + 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            ws.Cells[rowExcel, Count + 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            ws.Cells[rowExcel, Count + 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            ws.Cells[rowExcel, Count + 1].Style.Font.Bold = true;

                            ws.Cells[rowExcel, Count + 1].Value = gvSummary.HeaderRow.Cells[j].Text.Replace("&nbsp;", "");
                            Count++;
                        }
                    }
                    rowExcel++;

                    for (int i = 0; i < gvSummary.Rows.Count; i++)
                    {
                        if (gvSummary.Rows[i].Visible == true)
                        {
                            Count = 0;
                            for (int j = 0; j < gvSummary.HeaderRow.Cells.Count; j++)
                            {
                                if (gvSummary.Columns[j].Visible == true)
                                {
                                    ws.Cells[rowExcel, Count + 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    ws.Cells[rowExcel, Count + 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    ws.Cells[rowExcel, Count + 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    ws.Cells[rowExcel, Count + 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                    ws.Cells[rowExcel, Count + 1].Value = ((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Trim();
                                    if (gvSummary.HeaderRow.Cells[j].Text == "#")
                                    {
                                        ws.Cells[rowExcel, Count + 1].Value = Convert.ToInt32(((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Replace("&nbsp;", " ").Trim());
                                        ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else if (gvSummary.HeaderRow.Cells[j].Text == "Start Date" || gvSummary.HeaderRow.Cells[j].Text == "End Date" ||
                                             gvSummary.HeaderRow.Cells[j].Text == "Email" || gvSummary.HeaderRow.Cells[j].Text == "SMS")
                                    {
                                        ws.Cells[rowExcel, Count + 1].Value = ((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Replace("&nbsp;", " ").Trim();
                                        ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        ws.Cells[rowExcel, Count + 1].Value = ((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Replace("&nbsp;", " ").Trim();
                                        ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    }

                                    Count++;
                                }
                            }
                            rowExcel++;
                        }
                    }

                    #endregion
                }

                ws.Cells[ws.Dimension.Address].AutoFitColumns();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}