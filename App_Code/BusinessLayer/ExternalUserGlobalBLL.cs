﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

namespace BusinessLayer
{
    /// <summary>
    /// Summary description for ExternalUserGlobalBLL
    /// </summary>
    public class ExternalUserGlobalBLL : ExternalUserGlobalBase
    {
        public string Save(AbstractLayer.ExternalUserGlobalBase objGlobal)
        {
            ExternalUserGlobalInterface MemberSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objGlobal);
        }

        public DataTable UserList()
        {
            DataTable dtSt;
            ExternalUserGlobalInterface MemberListSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            ExternalUserGlobalInterface MemberListSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }

        public string Delete(string strUserId)
        {
            ExternalUserGlobalInterface MemberListSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.Delete(strUserId);
        }

        public string Save_DayDutyFilter(AbstractLayer.ExternalUserGlobalBase objGlobal)
        {
            ExternalUserGlobalInterface MemberSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save_DayDutyFilter(objGlobal);
        }

        public DataTable List_CrewDutyFilter()
        {
            DataTable dtSt;
            ExternalUserGlobalInterface MemberListSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.List_CrewDutyFilter();
            return dtSt;
        }

        public DataTable Bind_CustomFLYT()
        {
            ExternalUserGlobalInterface MemberListSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.Bind_CustomFLYT();
            return dtSt;
        }

        public DataTable Bind_ForFlight()
        {
            ExternalUserGlobalInterface MemberListSqlDataAccess = new ExternalUserGlobalDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.Bind_ForFlight();
            return dtSt;
        }
    }
}