﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;


/// <summary>
/// Summary description for AirportMasterBLL
/// </summary>

namespace BusinessLayer
{
    public class AirportMasterBLL : AirportMasterBase
    {
        public int Save(AbstractLayer.AirportMasterBase objUser)
        {
            AirportMasterInterface MemberSqlDataAccess = new AirportMasterDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            AirportMasterInterface MemberListSqlDataAccess = new AirportMasterDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }
        public DataTable AlreadyExist(string strEmail)
        {
            DataTable dtExist;
            AirportMasterInterface MemberListSqlDataAccess = new AirportMasterDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.AlreadyExist(strEmail);
            return dtExist;
        }
        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            AirportMasterInterface MemberListSqlDataAccess = new AirportMasterDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }
        public string DeletUser(string strUserId)
        {
            AirportMasterInterface MemberListSqlDataAccess = new AirportMasterDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeletUser(strUserId);
        }

    }
}