﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for TermsAndConditionBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class TermsAndConditionBLL : TermsAndConditionBase
    {
        public string Save(AbstractLayer.TermsAndConditionBase objUser)
        {
            TermsAndConditionInterface MemberSqlDataAccess = new TermsAndConditionDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataSet Select()
        {
            DataSet dtSt;
            TermsAndConditionInterface MemberListSqlDataAccess = new TermsAndConditionDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            TermsAndConditionInterface MemberListSqlDataAccess = new TermsAndConditionDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }

        public DataTable History(string strTaxFlag, string strCode)
        {
            DataTable dtEdit;
            TermsAndConditionInterface MemberListSqlDataAccess = new TermsAndConditionDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.History(strTaxFlag, strCode);
            return dtEdit;
        }
    }
}