﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for CreateAdminBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class CreateAdminBLL : CreateAdminBase
    {
        public string AddnewMember(AbstractLayer.CreateAdminBase objUser)
        {
            CreateAdminInterface MemberSqlDataAccess = new CreateAdminDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataTable UserList()
        {
            DataTable dtSt;
            CreateAdminInterface MemberListSqlDataAccess = new CreateAdminDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.List();
            return dtSt;
        }
        public DataTable AlreadyExist(string strEmail)
        {
            DataTable dtExist;
            CreateAdminInterface MemberListSqlDataAccess = new CreateAdminDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.DuplicateCheck(strEmail);
            return dtExist;
        }
        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            CreateAdminInterface MemberListSqlDataAccess = new CreateAdminDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }
        public string DeletUser(string strUserId)
        {
            CreateAdminInterface MemberListSqlDataAccess = new CreateAdminDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.Delete(strUserId);
        }
        public string InsertEmailSetup(string strUserId,string strEmail,string strFirstName,string strLastName)
        {
            CreateAdminInterface MemberListSqlDataAccess = new CreateAdminDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.InsertEmailSetup(strUserId,strEmail,strFirstName,strLastName);
        }

        public DataTable Select_User_Login(string EmailId, string ManageType)
        {
            CreateAdminInterface MemberListSqlDataAccess = new CreateAdminDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.Select_User_Login(EmailId, ManageType);
        }
    }
}