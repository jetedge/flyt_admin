﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for FixedPriceBLL
/// </summary>

namespace BusinessLayer
{
    public class FixedPriceBLL : FixedPriceBase
    {
        public int Save(AbstractLayer.FixedPriceBase objUser)
        {
            FixedPriceInterface MemberSqlDataAccess = new FixedPriceDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataTable ActiveSelect()
        {
            DataTable dtSt;
            FixedPriceInterface MemberListSqlDataAccess = new FixedPriceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.ActiveSelect();
            return dtSt;
        }

        public DataTable ExpiredSelect()
        {
            DataTable dtSt;
            FixedPriceInterface MemberListSqlDataAccess = new FixedPriceDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.ActiveSelect();
            return dtSt;
        }
        public DataTable AlreadyExist(string strStartDate, string strEndDate, string strSpecialPriceDesc, string strStartAirport, string strEndAirport)
        {
            DataTable dtExist;
            FixedPriceInterface MemberListSqlDataAccess = new FixedPriceDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.AlreadyExist(strStartDate, strEndDate, strSpecialPriceDesc, strStartAirport, strEndAirport);
            return dtExist;
        }
        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            FixedPriceInterface MemberListSqlDataAccess = new FixedPriceDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }
        public string DeletUser(string strUserId)
        {
            FixedPriceInterface MemberListSqlDataAccess = new FixedPriceDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeletUser(strUserId);
        }
    }
}