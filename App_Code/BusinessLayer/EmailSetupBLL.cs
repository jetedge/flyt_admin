﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for EmailSetupBLL
/// </summary>

namespace BusinessLayer
{
    public class EmailSetupBLL : EmailSetupBase
    {
        public int Save(AbstractLayer.EmailSetupBase objUser)
        {
            EmailSetupInterface MemberSqlDataAccess = new EmailSetupDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            EmailSetupInterface MemberListSqlDataAccess = new EmailSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable DuplicateUserMasterEmail(string strEmail)
        {
            DataTable dtSt;
            EmailSetupInterface MemberListSqlDataAccess = new EmailSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.DuplicateUserMasterEmail(strEmail);
            return dtSt;
        }

        public DataTable DuplicateEmailSetupEmail(string strEmail)
        {
            DataTable dtSt;
            EmailSetupInterface MemberListSqlDataAccess = new EmailSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.DuplicateEmailSetupEmail(strEmail);
            return dtSt;
        }
    }
}