﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for InvoiceNumberBLL
/// </summary>

namespace BusinessLayer
{
    public class InvoiceNumberBLL : InvoiceNumberBase
    {
        public int Save(AbstractLayer.InvoiceNumberBase objUser)
        {
            InvoiceNumberInterface MemberSqlDataAccess = new InvoiceNumberDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }

        public DataTable Select()
        {
            DataTable dtSt;
            InvoiceNumberInterface MemberListSqlDataAccess = new InvoiceNumberDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable InvoiceNumber()
        {
            DataTable dtSt;
            InvoiceNumberInterface MemberListSqlDataAccess = new InvoiceNumberDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.InvoiceNumber();
            return dtSt;
        }

        public DataTable BookingInNumberExist(string strInvoiceNumber)
        {
            DataTable dtSt;
            InvoiceNumberInterface MemberListSqlDataAccess = new InvoiceNumberDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BookingInNumberExist(strInvoiceNumber);
            return dtSt;
        }

        public DataTable BookingDetails(string strInvoiceNumber)
        {
            DataTable dtSt;
            InvoiceNumberInterface MemberListSqlDataAccess = new InvoiceNumberDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BookingDetails(strInvoiceNumber);
            return dtSt;
        }

        public DataTable InvoiceExist(string strInvoiceNumber, string strProject, string strProcess, string strCategory)
        {
            DataTable dtSt;
            InvoiceNumberInterface MemberListSqlDataAccess = new InvoiceNumberDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BookingDetails(strInvoiceNumber);
            return dtSt;
        }
    }
}