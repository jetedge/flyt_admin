﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for LandingGalleryBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class LandingGalleryBLL : LandingGalleryBase
    {
        public int SaveGallery(AbstractLayer.LandingGalleryBase objGallery)
        {
            LandingGalleryInterface MemberSqlDataAccess = new LandingGalleryDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.SaveGallery(objGallery);
        }

        public DataTable ListGallery(string strReportFlag)
        {
            DataTable dtSt;
            LandingGalleryInterface MemberListSqlDataAccess = new LandingGalleryDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.ListGallery(strReportFlag);
            return dtSt;
        }

        public DataTable EditGallery(string strUserId)
        {
            DataTable dtEdit;
            LandingGalleryInterface MemberListSqlDataAccess = new LandingGalleryDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.EditGallery(strUserId);
            return dtEdit;
        }

        public string DeleteGallery(string strUserId)
        {
            LandingGalleryInterface MemberListSqlDataAccess = new LandingGalleryDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeleteGallery(strUserId);
        }
    }
}