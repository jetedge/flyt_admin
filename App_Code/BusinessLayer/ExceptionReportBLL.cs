﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for ExceptionReportBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class ExceptionReportBLL
    {
        public DataTable Select(string strManageType, string strFromDate, string strToDate)
        {
            DataTable dtSt;
            ExceptionReportInterface MemberListSqlDataAccess = new ExceptionReportDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select(strManageType, strFromDate, strToDate);
            return dtSt;
        }

        public DataTable Booking( string strInvNumber)
        {
            DataTable dtSt;
            ExceptionReportInterface MemberListSqlDataAccess = new ExceptionReportDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Booking(strInvNumber);
            return dtSt;
        }

        public DataTable Child(string strManageType, string strFromDate, string strToDate, string strTailNo, string strTripNo)
        {
            DataTable dtSt;
            ExceptionReportInterface MemberListSqlDataAccess = new ExceptionReportDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Child(strManageType, strFromDate, strToDate, strTailNo, strTripNo);
            return dtSt;
        }
    }
}