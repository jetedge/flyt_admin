﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;
using System.Globalization;

/// <summary>
/// Summary description for EmptyLegSetupBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class EmptyLegSetupBLL : EmptyLegSetupBase
    {
        public string InsertLivePricingCalculation(AbstractLayer.EmptyLegSetupBase objUser)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.InsertLivePricingCalculation(objUser);
        }

        public DataTable BindEmptyLegs_SP(string strManaeType, string strFromDate, string strToDate, string strPageINdex, string strPageSize)
        {
            DataTable dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindEmptyLegs_SP(strManaeType, strFromDate, strToDate, strPageINdex, strPageSize);
            return dtSt;
        }

        public DataTable GET_LIVEDATA(string strManaeType, string strFromDate, string strToDate, string strTail, string strTrip, string strTriplag)
        {
            DataTable dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.GET_LIVEDATA(strManaeType, strFromDate, strToDate, strTail, strTrip, strTriplag);
            return dtSt;
        }

        public DataTable BindGroupData()
        {
            DataTable dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindGroupData();
            return dtSt;
        }
        public DataTable BindGroupChildData(string strRowId)
        {
            DataTable dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindGroupChildData(strRowId);
            return dtSt;
        }
        public DataTable CountryList()
        {
            DataTable dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.CountryList();
            return dtSt;
        }
        public DataSet LivePricing_SEL(string strTailNo, string strTripNo, string strStartAirport, string strEndAirport, string strGroupId, string strFlyingDate)
        {
            DataSet dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.LivePricing_SEL(strTailNo, strTripNo, strStartAirport, strEndAirport, strGroupId, strFlyingDate);
            return dtSt;
        }

        public DataTable Get_FutureData()
        {
            DataTable dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Get_FutureData();
            return dtSt;
        }

        public string SplitDelete(string strGroupId)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.SplitDelete(strGroupId);
        }

        public string ManualDelete(string strGroupId, string strTailNo, string strTripNo, string strAvailableFrom, string strStartTime, string strStartAirport, string strEndAirport)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);

            return MemberSqlDataAccess.ManualDelete(strGroupId, strTailNo, strTripNo, strAvailableFrom, strStartTime, strStartAirport, strEndAirport);
        }

        public string SaveDepartureGroup(string strManageType, string strRowId, string strGroupName, string strFlag, string strCreatedBy)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);

            return MemberSqlDataAccess.SaveDepartureGroup(strManageType, strRowId, strGroupName, strFlag, strCreatedBy);
        }

        public string DeleteAirportChild(string strGroupId)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.DeleteAirportChild(strGroupId);
        }

        public string DeleteAirport(string strRowId)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.DeleteAirport(strRowId);
        }

        public string SaveAirportChild(string strManageType, string strGroupId, string strModifiedFlag, string strAirportFor, string strSetupBy, string strAirport,
            string strCreatedBy, string strStateCode, string strCountryCode, string strAirportCode)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);

            return MemberSqlDataAccess.SaveAirportChild(strManageType, strGroupId, strModifiedFlag, strAirportFor, strSetupBy, strAirport, strCreatedBy, strStateCode, strCountryCode, strAirportCode);
        }

        public DataTable GroupExist(string strGroupName)
        {
            DataTable dtSt;
            EmptyLegSetupInterface MemberListSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.GroupExist(strGroupName);
            return dtSt;
        }

        public string saveCategory(string[] arrCategory)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);

            return MemberSqlDataAccess.saveCategory(arrCategory);
        }

        public string saveCategoryGroup(string[] arrCategory)
        {
            EmptyLegSetupInterface MemberSqlDataAccess = new EmptyLegSetupDAL(SqlHelper.FLYTConnectionString);

            return MemberSqlDataAccess.saveCategoryGroup(arrCategory);
        }

    
    }
}