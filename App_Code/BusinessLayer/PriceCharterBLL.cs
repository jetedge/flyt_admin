﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for PriceCharterBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class PriceCharterBLL
    {
        public DataTable Select()
        {
            DataTable dtSt;
            PriceCharterInterface MemberListSqlDataAccess = new PriceCharterDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }

        public DataTable BindCharterPrice(string strTailNo, string strAvailableFrom, string strStart, string strEnd)
        {
            DataTable dtSt;
            PriceCharterInterface MemberListSqlDataAccess = new PriceCharterDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.BindCharterPrice(strTailNo, strAvailableFrom, strStart, strEnd);
            return dtSt;
        }
    }
}