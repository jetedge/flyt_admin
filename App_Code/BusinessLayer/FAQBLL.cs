﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for FAQBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class FAQBLL : FAQBase
    {
        public string Save(AbstractLayer.FAQBase objUser)
        {
            FAQInterface MemberSqlDataAccess = new FAQDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public int SaveTopic(AbstractLayer.FAQBase objUser)
        {
            FAQInterface MemberSqlDataAccess = new FAQDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.SaveTopic(objUser);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            FAQInterface MemberListSqlDataAccess = new FAQDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }
        public DataTable SelectTopic()
        {
            DataTable dtSt;
            FAQInterface MemberListSqlDataAccess = new FAQDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.SelectTopic();
            return dtSt;
        }
    }
}