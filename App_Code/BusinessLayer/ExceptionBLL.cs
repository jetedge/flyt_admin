﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for ExceptionBLL
/// </summary>

namespace BusinessLayer
{
    public class ExceptionBLL 
    {
        public DataTable Select()
        {
            DataTable dtSt;
            ExceptionInterace MemberListSqlDataAccess = new ExceptionDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }
    }
}