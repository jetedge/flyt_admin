﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Drawing;
using System.IO;

/// <summary>
/// Summary description for CreateCustomerBLL
/// </summary>
/// 

namespace BusinessLayer
{
    public class CreateCustomerBLL : CreateCustomerBase
    {
        public string AddnewMember(AbstractLayer.CreateCustomerBase objUser)
        {
            CustomerInterface MemberSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.AddNewCustomer(objUser);
        }

        public string AddPreference(AbstractLayer.CreateCustomerBase objUser)
        {
            CustomerInterface MemberSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.AddPreference(objUser);
        }

        public DataTable UserList(string strUserName)
        {
            DataTable dtSt;
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.ListAllCutomers(strUserName);
            return dtSt;
        }
        public DataTable CouponDiscount()
        {
            DataTable dtSt;
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.CouponDiscount();
            return dtSt;
        }
        public DataTable AirportMaster()
        {
            DataTable dtSt;
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.AirportMaster();
            return dtSt;
        }
        public DataTable AlreadyExist(string strEmail, string RowId)
        {
            DataTable dtExist;
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.AlreadyExist(strEmail, RowId);
            return dtExist;
        }
        public DataSet Edit(string strUserId)
        {
            DataSet dtEdit;
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }

        public DataTable GetEmptyTrips(string strPageINdex, string strRowID, string strFromDate, string strToDate, string strTail)
        {
            DataTable dtEdit;
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.GetEmptyTrips(strPageINdex, strRowID, strFromDate, strToDate, strTail);
            return dtEdit;
        }

        public DataTable GetLeg(string strTailNo, string strTripNo, string strAvailableFrom, string strStartTime, string strEndTime, string strStartAirport, string strEndAirport)
        {
            DataTable dtEdit;
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.GetLeg(strTailNo, strTripNo, strAvailableFrom, strStartTime, strEndTime, strStartAirport, strEndAirport);
            return dtEdit;
        }

        public string UpdateCoupon(string strUsers, string strCouponCode)
        {
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.UpdateCoupon(strUsers, strCouponCode);
        }

        public string DeletePreference(string strUserId)
        {
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeletePreference(strUserId);
        }

        public static object GetUrl(string domain)
        {
            if (domain == "localhost")
                return "J:\\jetedgeMgnt";
            else
                return "D:\\jetedgefiles";
        }

        public void Exportexcel(DataTable dtCustomer, string strFileName, string strFromDate, string strUserName, string SheetName)
        {
            string fileName = strFileName + ".xlsx";
            object fileUrl = GetUrl(HttpContext.Current.Request.Url.Host);

            if (fileUrl != null && fileUrl != string.Empty)
            {
                bool exists = Directory.Exists(fileUrl.ToString());
                if (!exists)
                {
                    Directory.CreateDirectory(fileUrl.ToString());
                }

                bool vendor = Directory.Exists(fileUrl.ToString() + @"\Temporary");
                if (!vendor)
                {
                    Directory.CreateDirectory(fileUrl.ToString() + @"\Temporary");
                }
            }

            string targetPath = fileUrl.ToString() + @"\Temporary";
            string destFile = System.IO.Path.Combine(targetPath, fileName);
            var existingFile = new FileInfo(destFile);

            if (existingFile.Exists)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                existingFile.Delete();
            }

            using (ExcelPackage xlPackage = new ExcelPackage(existingFile))
            {
                funexcel(xlPackage, dtCustomer, strFileName, strUserName, strFromDate, SheetName);

                xlPackage.Save();
                Download_Click(destFile);
            }

        }

        private void Download_Click(string destFile)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(destFile);
            if (file.Exists)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.WriteFile(file.FullName);
                HttpContext.Current.Response.End();
            }
        }

        private void funexcel(ExcelPackage xlPackage, DataTable dtCustomer, string strFileName,
                string strUserName, string strFromDate, string SheetName)
        {
            int rowExcel = 1;
            var ws = xlPackage.Workbook.Worksheets.Add(SheetName);
            Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#D8D8D8");

            ws.Cells[1, 1].Value = "JET EDGE FLYT | FOUR SEASONS";
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 1].Style.Font.Size = 16;
            ws.Cells[1, 1, 1, dtCustomer.Columns.Count].Merge = true;
            ws.Cells[1, 1, 1, dtCustomer.Columns.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 1, 1, dtCustomer.Columns.Count].Style.Fill.BackgroundColor.SetColor(colFromHex1);

            ws.Cells[2, 1].Value = "Report generated by : " + strUserName + " On " + DateTime.Now.ToString("MMMM d, yyyy HH:mm");
            ws.Cells[2, 1, 2, dtCustomer.Columns.Count].Merge = true;
            ws.Cells[2, 1, 2, dtCustomer.Columns.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[2, 1, 2, dtCustomer.Columns.Count].Style.Fill.BackgroundColor.SetColor(colFromHex1);

            ws.Cells[3, 1].Value = SheetName;
            ws.Cells[3, 1, 3, dtCustomer.Columns.Count].Merge = true;
            ws.Cells[3, 1, 3, dtCustomer.Columns.Count].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[3, 1, 3, dtCustomer.Columns.Count].Style.Fill.BackgroundColor.SetColor(colFromHex1);

            rowExcel = 5;
            if (dtCustomer.Rows.Count > 0)
            {
                #region write Excel grid row greater then 0

                int Count = 0;
                for (int j = 0; j < dtCustomer.Columns.Count; j++)
                {
                    ws.Cells[rowExcel, Count + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowExcel, Count + 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                    ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowExcel, Count + 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowExcel, Count + 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowExcel, Count + 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowExcel, Count + 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowExcel, Count + 1].Style.Font.Bold = true;

                    ws.Cells[rowExcel, Count + 1].Value = dtCustomer.Columns[j].ColumnName.ToString().Replace("&nbsp;", "");
                    Count++;
                }
                rowExcel++;

                for (int i = 0; i < dtCustomer.Rows.Count; i++)
                {
                    Count = 0;
                    for (int j = 0; j < dtCustomer.Columns.Count; j++)
                    {
                        ws.Cells[rowExcel, Count + 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        ws.Cells[rowExcel, Count + 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        ws.Cells[rowExcel, Count + 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        ws.Cells[rowExcel, Count + 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                        //ws.Cells[rowExcel, Count + 1].Value = ((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Replace("&nbsp;", " ").Trim();
                        ws.Cells[rowExcel, Count + 1].Value = dtCustomer.Rows[i][j].ToString().Replace("&nbsp;", " ").Trim();
                        ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        Count++;
                    }
                    rowExcel++;
                }

                #endregion
            }

            ws.Cells[ws.Dimension.Address].AutoFitColumns();
        }

        public string Sync_CouponDiscount(string strUserId)
        {
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.Sync_CouponDiscount(strUserId);
        }

        public string Sync_CouponDiscount_Delete(string strUserId)
        {
            CustomerInterface MemberListSqlDataAccess = new CreateCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.Sync_CouponDiscount_Delete(strUserId);
        }
    }
}