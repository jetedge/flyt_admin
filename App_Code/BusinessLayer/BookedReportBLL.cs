﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Style;

/// <summary>
/// Summary description for BookedReportBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class BookedReportBLL
    {
        public DataTable Select(string strFromDate, string strToDate, string strTail, string strTrip)
        {
            DataTable dtSt;
            BookedReportInterface MemberListSqlDataAccess = new BookedReportDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select(strFromDate, strToDate, strTail, strTrip);
            return dtSt;
        }

        public DataSet Select_PageHit(string strFromDate, string strRepToDate)
        {
            DataSet dtSt;
            BookedReportInterface MemberListSqlDataAccess = new BookedReportDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select_PageHit(strFromDate, strRepToDate);
            return dtSt;
        }

        public DataTable PageHit_TopBookings(string strFromDate, string strRepToDate)
        {
            BookedReportInterface MemberListSqlDataAccess = new BookedReportDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.PageHit_TopBookings(strFromDate, strRepToDate);
            return dtSt;
        }

        public DataTable PageHit_GetPopup_Details(string country, string regionName, string city, string strRepFromDate, string strRepToDate)
        {
            BookedReportInterface MemberListSqlDataAccess = new BookedReportDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.PageHit_GetPopup_Details(country, regionName, city, strRepFromDate, strRepToDate);
            return dtSt;
        }

        public static object GetUrl(string domain)
        {
            if (domain == "localhost")
                return "J:\\jetedgeMgnt";
            else
                return "D:\\jetedgefiles";
        }

        public void Exportexcel(GridView gvPageHits, string strFileName, string[] arrlblPageHits,
            string strFromDate, string strUserName, string SheetName)
        {
            string fileName = strFileName + ".xlsx";
            object fileUrl = GetUrl(HttpContext.Current.Request.Url.Host);

            if (fileUrl != null && fileUrl != string.Empty)
            {
                bool exists = Directory.Exists(fileUrl.ToString());
                if (!exists)
                {
                    Directory.CreateDirectory(fileUrl.ToString());
                }

                bool vendor = Directory.Exists(fileUrl.ToString() + @"\Temporary");
                if (!vendor)
                {
                    Directory.CreateDirectory(fileUrl.ToString() + @"\Temporary");
                }
            }

            string targetPath = fileUrl.ToString() + @"\Temporary";
            string destFile = System.IO.Path.Combine(targetPath, fileName);
            var existingFile = new FileInfo(destFile);

            if (existingFile.Exists)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                existingFile.Delete();
            }

            using (ExcelPackage xlPackage = new ExcelPackage(existingFile))
            {
                funexcel(xlPackage, gvPageHits, SheetName, arrlblPageHits, strFileName, strUserName, strFromDate, SheetName);

                xlPackage.Save();
                Download_Click(destFile);
            }

        }

        private void Download_Click(string destFile)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(destFile);
            if (file.Exists)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.WriteFile(file.FullName);
                HttpContext.Current.Response.End();
            }
        }

        private void funexcel(ExcelPackage xlPackage, GridView gvSummary, string p, string[] labelSummary, string strFileName,
                string strUserName, string strFromDate, string SheetName)
        {
            int rowExcel = 1;
            var ws = xlPackage.Workbook.Worksheets.Add(SheetName);

            ws.Cells[1, 1].Value = "Jet Edge - FLYT";
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 1].Style.Font.Size = 18;
            ws.Cells[1, 1, 1, labelSummary.Length].Merge = true;
            ws.Cells[1, 1, 1, labelSummary.Length].Style.Fill.PatternType = ExcelFillStyle.Solid;
            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#BFBFBF");
            ws.Cells[1, 1, 1, labelSummary.Length].Style.Fill.BackgroundColor.SetColor(colFromHex);

            ws.Cells[2, 1].Value = "Report generated by : " + strUserName + " On " + DateTime.Now.ToString();
            ws.Cells[2, 1, 2, labelSummary.Length].Merge = true;
            ws.Cells[2, 1, 2, labelSummary.Length].Style.Fill.PatternType = ExcelFillStyle.Solid;
            Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#D8D8D8");
            ws.Cells[2, 1, 2, labelSummary.Length].Style.Fill.BackgroundColor.SetColor(colFromHex1);

            ws.Cells[3, 1].Value = "Page Hits Report for [ " + strFromDate + " ]";
            ws.Cells[3, 1, 3, labelSummary.Length].Merge = true;
            ws.Cells[3, 1, 3, labelSummary.Length].Style.Fill.PatternType = ExcelFillStyle.Solid;
            Color colFromHex2 = System.Drawing.ColorTranslator.FromHtml("#F2F2F2");
            ws.Cells[3, 1, 3, labelSummary.Length].Style.Fill.BackgroundColor.SetColor(colFromHex2);

            rowExcel = 5;
            if (gvSummary.Rows.Count > 0)
            {
                #region write Excel grid row greater then 0
                int Count = 0;
                for (int j = 0; j < gvSummary.HeaderRow.Cells.Count; j++)
                {
                    if (gvSummary.Columns[j].Visible == true)
                    {
                        ws.Cells[rowExcel, Count + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[rowExcel, Count + 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                        ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ws.Cells[rowExcel, Count + 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        ws.Cells[rowExcel, Count + 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        ws.Cells[rowExcel, Count + 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        ws.Cells[rowExcel, Count + 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        ws.Cells[rowExcel, Count + 1].Style.Font.Bold = true;

                        ws.Cells[rowExcel, Count + 1].Value = gvSummary.HeaderRow.Cells[j].Text.Replace("&nbsp;", "");
                        Count++;
                    }
                }
                rowExcel++;

                for (int i = 0; i < gvSummary.Rows.Count; i++)
                {
                    if (gvSummary.Rows[i].Visible == true)
                    {
                        Count = 0;
                        for (int j = 0; j < gvSummary.HeaderRow.Cells.Count; j++)
                        {
                            if (gvSummary.Columns[j].Visible == true)
                            {
                                ws.Cells[rowExcel, Count + 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                ws.Cells[rowExcel, Count + 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                ws.Cells[rowExcel, Count + 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                ws.Cells[rowExcel, Count + 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                ws.Cells[rowExcel, Count + 1].Value = ((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Trim();
                                if (gvSummary.HeaderRow.Cells[j].Text == "#")
                                {
                                    ws.Cells[rowExcel, Count + 1].Value = Convert.ToInt32(((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Replace("&nbsp;", " ").Trim());
                                    ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    ws.Cells[rowExcel, Count + 1].Value = ((Label)gvSummary.Rows[i].FindControl(labelSummary[j])).Text.Replace("&nbsp;", " ").Trim();
                                    ws.Cells[rowExcel, Count + 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }

                                Count++;
                            }
                        }
                        rowExcel++;
                    }
                }

                #endregion
            }

            ws.Cells[ws.Dimension.Address].AutoFitColumns();
        }

        public DataTable AdminUser_Log_Report(string strRepFromDate, string strRepToDate, string strUserName)
        {
            BookedReportInterface MemberListSqlDataAccess = new BookedReportDAL(SqlHelper.FLYTConnectionString);
            DataTable dtSt = MemberListSqlDataAccess.AdminUser_Log_Report(strRepFromDate, strRepToDate, strUserName);
            return dtSt;
        }
    }
}