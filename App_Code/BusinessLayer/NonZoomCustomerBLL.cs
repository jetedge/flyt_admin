﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;
/// <summary>
/// Summary description for NonZoomCustomerBLL
/// </summary>
/// 
namespace BusinessLayer
{
    public class NonZoomCustomerBLL : NonZoomCustomerBase
    {
        public int Save(AbstractLayer.NonZoomCustomerBase objUser)
        {
            NonZoomCustomerInterface MemberSqlDataAccess = new NonZoomCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.Save(objUser);
        }
        public DataTable Select()
        {
            DataTable dtSt;
            NonZoomCustomerInterface MemberListSqlDataAccess = new NonZoomCustomerDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.Select();
            return dtSt;
        }
        public DataTable AlreadyExist(string strEmail)
        {
            DataTable dtExist;
            NonZoomCustomerInterface MemberListSqlDataAccess = new NonZoomCustomerDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.AlreadyExist(strEmail);
            return dtExist;
        }
        public DataTable Edit(string strUserId)
        {
            DataTable dtEdit;
            NonZoomCustomerInterface MemberListSqlDataAccess = new NonZoomCustomerDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.Edit(strUserId);
            return dtEdit;
        }
        public string DeletUser(string strUserId)
        {
            NonZoomCustomerInterface MemberListSqlDataAccess = new NonZoomCustomerDAL(SqlHelper.FLYTConnectionString);
            return MemberListSqlDataAccess.DeletUser(strUserId);
        }
       
    }
}