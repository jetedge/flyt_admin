﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbstractLayer;
using DataLayer;
using System.Data;

/// <summary>
/// Summary description for DiscountandCouponBLL
/// </summary>
namespace BusinessLayer
{
    public class DiscountandCouponBLL : DiscountandCouponBase
    {
        public int SaveDiscountandCoupon(AbstractLayer.DiscountandCouponBase objUser)
        {
            DiscountandCouponInterface MemberSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.SaveDiscountandCoupon(objUser);
        }
        public DataTable ListDiscountandCoupon(string Status)
        {
            DataTable dtSt;
            DiscountandCouponInterface MemberListSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.ListDiscountandCoupon(Status);
            return dtSt;
        }
        public DataTable AlreadyExist(string strCouponCode)
        {
            DataTable dtExist;
            DiscountandCouponInterface MemberListSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            dtExist = MemberListSqlDataAccess.AlreadyExist(strCouponCode);
            return dtExist;
        }
        public DataTable EditDiscountandCoupon(string strRowId)
        {
            DataTable dtEdit;
            DiscountandCouponInterface MemberListSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.EditDiscountandCoupon(strRowId);
            return dtEdit;
        }

        public int SaveCoupon_Users(AbstractLayer.DiscountandCouponBase objUser)
        {
            DiscountandCouponInterface MemberSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.SaveCoupon_Users(objUser);
        }
        public int DeleteCoupon_Users(AbstractLayer.DiscountandCouponBase objUser)
        {
            DiscountandCouponInterface MemberSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            return MemberSqlDataAccess.DeleteCoupon_Users(objUser);
        }
        public DataTable SelectCoupon_Users(string strRowId)
        {
            DataTable dtEdit;
            DiscountandCouponInterface MemberListSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            dtEdit = MemberListSqlDataAccess.SelectCoupon_Users(strRowId);
            return dtEdit;
        }

        public DataTable List_RegisteredUser(string ManageType, string UserName, string Email, string DiscountType)
        {
            DataTable dtSt;
            DiscountandCouponInterface MemberListSqlDataAccess = new DiscountandCouponDAL(SqlHelper.FLYTConnectionString);
            dtSt = MemberListSqlDataAccess.List_RegisteredUser(ManageType, UserName, Email, DiscountType);
            return dtSt;
        }
    }
}