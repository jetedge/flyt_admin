﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for TaxSetupDAL
/// </summary>
/// 
namespace DataLayer
{
    public class TaxSetupDAL : TaxSetupInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public TaxSetupDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.TaxSetupBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowId);
                arrParam[2] = new SqlParameter("@Code", objMember.Code);
                arrParam[3] = new SqlParameter("@Name", objMember.Name);
                arrParam[4] = new SqlParameter("@Description", objMember.Description);
                arrParam[5] = new SqlParameter("@Notes", objMember.Notes);
                arrParam[6] = new SqlParameter("@Effectivefrom", objMember.EffectiveFrom);
                arrParam[7] = new SqlParameter("@Tax", objMember.Tax);
                arrParam[8] = new SqlParameter("@Active", objMember.Active);
                arrParam[9] = new SqlParameter("@Fees", "0.00");
                arrParam[10] = new SqlParameter("@Pflag", objMember.PercentageSymbol);
                arrParam[11] = new SqlParameter("@DisconDate", objMember.EffectiveFrom);
                arrParam[12] = new SqlParameter("@TaxFLag", objMember.TaxFlag);

                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_domestictax_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }
        public DataSet Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[TaxSetup_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[TaxSetup_SEL]", getAllTripParm);
        }
        public DataTable Edit(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[TaxSetup_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[TaxSetup_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable History(string strTaxFlag,string strCode)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[TaxSetup_SEL]", false);
            getAllTripParm[0].Value = "H";
            getAllTripParm[3].Value = strTaxFlag;
            getAllTripParm[2].Value = strCode;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[TaxSetup_SEL]", getAllTripParm).Tables[0];
        }
    }
}