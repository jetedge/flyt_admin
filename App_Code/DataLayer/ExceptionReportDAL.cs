﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for ExceptionReportDAL
/// </summary>
/// 
namespace DataLayer
{
    public class ExceptionReportDAL : ExceptionReportInterface
    {
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public ExceptionReportDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public DataTable Select(string strManageType, string strFromDate, string strToDate)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ExceptionReport_SEL]", false);
            saveCategoryParam[0].Value = strManageType;
            saveCategoryParam[1].Value = strFromDate;
            saveCategoryParam[2].Value = strToDate;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[ExceptionReport_SEL]", saveCategoryParam).Tables[0];
        }

        public DataTable Child(string strManageType, string strFromDate, string strToDate, string strTailNo, string strTripNo)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ExceptionReport_SEL]", false);
            saveCategoryParam[0].Value = strManageType;
            saveCategoryParam[1].Value = strFromDate;
            saveCategoryParam[2].Value = strToDate;
            saveCategoryParam[3].Value = strTailNo;
            saveCategoryParam[4].Value = strTripNo;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[ExceptionReport_SEL]", saveCategoryParam).Tables[0];
        }

        public DataTable Booking(string strInvNumber)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ExceptionReport_SEL]", false);
            saveCategoryParam[0].Value = "IN";
            saveCategoryParam[5].Value = strInvNumber;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[ExceptionReport_SEL]", saveCategoryParam).Tables[0];
        }
    }
}