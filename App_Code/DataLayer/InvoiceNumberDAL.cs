﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for InvoiceNumberDAL
/// </summary>

namespace DataLayer
{
    public class InvoiceNumberDAL : InvoiceNumberInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public InvoiceNumberDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.InvoiceNumberBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[15];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowId);
                arrParam[2] = new SqlParameter("@InvoiceNumber", objMember.InvoiceNumbert);
                arrParam[3] = new SqlParameter("@Project", objMember.ProjectShortCode);
                arrParam[4] = new SqlParameter("@Process", objMember.ProcessShortCode);
                arrParam[5] = new SqlParameter("@Category", objMember.Category);
                arrParam[7] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[8] = new SqlParameter("@InitialNumber", objMember.AutoIncrement);
                arrParam[8] = new SqlParameter("@Flag", "FINAL");

                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_InvoiceNumber_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }

        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[InvoiceNumber_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[InvoiceNumber_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable InvoiceNumber()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[InvoiceNumber_SEL]", false);
            getAllTripParm[0].Value = "IN";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[InvoiceNumber_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable BookingInNumberExist(string strInvoiceNumber)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_InvoiceNumber_INS]", false);
            getAllTripParm[0].Value = "NE";
            getAllTripParm[2].Value = strInvoiceNumber;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_InvoiceNumber_INS]", getAllTripParm).Tables[0];
        }

        public DataTable BookingDetails(string strInvoiceNumber)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_InvoiceNumber_INS]", false);
            getAllTripParm[0].Value = "BD";
            getAllTripParm[2].Value = strInvoiceNumber;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_InvoiceNumber_INS]", getAllTripParm).Tables[0];
        }

        public DataTable InvoiceExist(string strInvoiceNumber, string strProject, string strProcess, string strCategory)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_InvoiceNumber_INS]", false);
            getAllTripParm[0].Value = "INE";
            getAllTripParm[2].Value = strInvoiceNumber;
            getAllTripParm[3].Value = strProject;
            getAllTripParm[4].Value = strProcess;
            getAllTripParm[5].Value = strCategory;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_InvoiceNumber_INS]", getAllTripParm).Tables[0];
        }

    }
}