﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;


/// <summary>
/// Summary description for LandingGalleryDAL
/// </summary>
/// 
namespace DataLayer
{
    public class LandingGalleryDAL : LandingGalleryInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public LandingGalleryDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public int SaveGallery(AbstractLayer.LandingGalleryBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowID);
                arrParam[2] = new SqlParameter("@ImagePath", objMember.ImagePath);
                arrParam[3] = new SqlParameter("@Image1", objMember.Image1);
                arrParam[4] = new SqlParameter("@Image2", objMember.Image2);
                arrParam[5] = new SqlParameter("@Image3", objMember.Image3);
                arrParam[6] = new SqlParameter("@Image4", objMember.Image4);
                arrParam[7] = new SqlParameter("@Notes", objMember.Notes);
                arrParam[8] = new SqlParameter("@Status", objMember.Status);
                arrParam[9] = new SqlParameter("@UploadedBy", objMember.UploadedBy);
                arrParam[10] = new SqlParameter("@SliderPosition", objMember.SliderPosition);
                arrParam[11] = new SqlParameter("@ReportFlag", objMember.ReportFlag);


                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_LANDINGGALLERY_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }

        public DataTable ListGallery(string strFlag)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_LANDINGGALLERY_INS]", false);
            getAllTripParm[0].Value = "SEL";
            getAllTripParm[11].Value = strFlag;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_LANDINGGALLERY_INS]", getAllTripParm).Tables[0];
        }

        public DataTable EditGallery(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_LANDINGGALLERY_INS]", false);
            getAllTripParm[0].Value = "EDT";
            getAllTripParm[1].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_LANDINGGALLERY_INS]", getAllTripParm).Tables[0];
        }

        public string DeleteGallery(string strRowId)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_LANDINGGALLERY_INS", false);
            SaveNewTicketParams[0].Value = "DEL";
            SaveNewTicketParams[1].Value = strRowId;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_LANDINGGALLERY_INS", SaveNewTicketParams);
            return obj.ToString();
        }
    }
}