﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for ExceptionDAL
/// </summary>

namespace DataLayer
{
    public class ExceptionDAL : ExceptionInterace
    {
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public ExceptionDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[Exception_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[Exception_SEL]", getAllTripParm).Tables[0];
        }

    }
}