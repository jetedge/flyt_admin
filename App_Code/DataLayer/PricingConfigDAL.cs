﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for PricingConfigDAL
/// </summary>

namespace DataLayer
{
    public class PricingConfigDAL : PricingConfigInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public PricingConfigDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PricingSetup_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PricingSetup_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable FilterGrid(string strTailFilter)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PricingSetup_SEL]", false);
            getAllTripParm[0].Value = "FIL";
            getAllTripParm[1].Value = strTailFilter;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PricingSetup_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable Edit(string strRowID)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PricingSetup_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[2].Value = strRowID;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PricingSetup_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable TailDetails(string strRowID)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PricingSetup_SEL]", false);
            getAllTripParm[0].Value = "TD";
            getAllTripParm[2].Value = strRowID;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PricingSetup_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable PopDetails(string strTripNo, string strTailNo, string strAvailableFrom, string strStartTime)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PricingSetup_SEL]", false);
            getAllTripParm[0].Value = "PD";
            getAllTripParm[3].Value = strTripNo;
            getAllTripParm[4].Value = strTailNo;
            getAllTripParm[5].Value = strAvailableFrom;
            getAllTripParm[6].Value = strStartTime;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PricingSetup_SEL]", getAllTripParm).Tables[0];
        }

        public string Insert_TailImages(string strTailNo, string FilePath, string InteriorImage, string ExteriorImage, string UploadedBy)
        {
            SqlParameter[] TailImagesParms = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_TAILSETUP_IMAGES_INS]", false);

            TailImagesParms[0].Value = "INS";
            TailImagesParms[1].Value = strTailNo;
            TailImagesParms[2].Value = FilePath;
            TailImagesParms[3].Value = InteriorImage;
            TailImagesParms[4].Value = ExteriorImage;
            TailImagesParms[5].Value = UploadedBy;

            object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "[SP_TAILSETUP_IMAGES_INS]", TailImagesParms);
            return obj.ToString();
        }
        public DataTable Select_TailImages(string strTailNo)
        {
            SqlParameter[] getTailImages_Parm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_TAILSETUP_IMAGES_INS]", false);
            getTailImages_Parm[0].Value = "SEL";
            getTailImages_Parm[1].Value = strTailNo;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_TAILSETUP_IMAGES_INS]", getTailImages_Parm).Tables[0];
        }

    }
}