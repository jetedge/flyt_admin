﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for AirportMasterDAL
/// </summary>

namespace DataLayer
{
    public class AirportMasterDAL : AirportMasterInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public AirportMasterDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.AirportMasterBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowId);
                arrParam[2] = new SqlParameter("@Country", objMember.Country);
                arrParam[3] = new SqlParameter("@ICAO", objMember.ICAO);
                arrParam[4] = new SqlParameter("@City", objMember.City);
                arrParam[5] = new SqlParameter("@RunWayLength", objMember.RunWayLength);
                arrParam[6] = new SqlParameter("@Active", objMember.Active);
                arrParam[7] = new SqlParameter("@LandingFlag", objMember.LandingFlag);
                arrParam[8] = new SqlParameter("@CreatedBy", objMember.CreatedBy);

                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_AirportMaster_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }
        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[AirportMaster_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[AirportMaster_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable Edit(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[AirportMaster_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[AirportMaster_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable AlreadyExist(string strICAO)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_AirportMaster_INS]", false);
            getAllTripParm[0].Value = "AE";
            getAllTripParm[3].Value = strICAO;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_AirportMaster_INS]", getAllTripParm).Tables[0];
        }
        public string DeletUser(string strRowId)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "AirportMaster_SEL", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strRowId;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "AirportMaster_SEL", SaveNewTicketParams);
            return obj.ToString();
        }
    }
}