﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for CreateCustomerDAL
/// </summary>

namespace DataLayer
{
    public class CreateCustomerDAL : CustomerInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public CreateCustomerDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public string AddNewCustomer(AbstractLayer.CreateCustomerBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[40];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowID);
                arrParam[2] = new SqlParameter("@Username", "");
                arrParam[3] = new SqlParameter("@FirstName", objMember.firstName);
                arrParam[4] = new SqlParameter("@LastName", objMember.lastName);
                arrParam[5] = new SqlParameter("@Title", objMember.Title);
                arrParam[6] = new SqlParameter("@Email", objMember.Email);
                arrParam[7] = new SqlParameter("@PhoneNumber", objMember.Phone);
                arrParam[8] = new SqlParameter("@Code_Phone", objMember.PhoneCode);
                arrParam[9] = new SqlParameter("@Address", objMember.Address1);
                arrParam[10] = new SqlParameter("@Address2", objMember.Address2);
                arrParam[11] = new SqlParameter("@Country", objMember.Country);
                arrParam[12] = new SqlParameter("@City", objMember.City);
                arrParam[13] = new SqlParameter("@ZipCode", objMember.ZipCode);
                arrParam[14] = new SqlParameter("@Role", objMember.UserType);
                arrParam[15] = new SqlParameter("@Password", objMember.Password);
                arrParam[16] = new SqlParameter("@Gender", "");
                arrParam[17] = new SqlParameter("@state", objMember.State);
                arrParam[18] = new SqlParameter("@MustchangePassword", "1");
                arrParam[20] = new SqlParameter("@Usecookies", "1");
                arrParam[21] = new SqlParameter("@Active", objMember.Active);
                arrParam[22] = new SqlParameter("@Userflag", "ADMINUSER");
                arrParam[23] = new SqlParameter("@BrokerFlag", objMember.BrokerFlag);
                arrParam[24] = new SqlParameter("@BrokerName", objMember.BrokerFirstName);
                arrParam[25] = new SqlParameter("@BroKerAddress2", objMember.BrokerLastName);
                arrParam[26] = new SqlParameter("@Manager", objMember.Manger);
                arrParam[27] = new SqlParameter("@BroKerAddress1", objMember.BrokerAddress);
                arrParam[28] = new SqlParameter("@BroKerCity", objMember.BrokerCity);
                arrParam[29] = new SqlParameter("@BroKerState", objMember.BrokerState);
                arrParam[30] = new SqlParameter("@BroKerCountry", objMember.BrokerCountry);
                arrParam[31] = new SqlParameter("@BroKerZipCode", objMember.BrokerZipCode);
                arrParam[32] = new SqlParameter("@Days", objMember.NumberDays);
                arrParam[33] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[34] = new SqlParameter("@MustMoveFlag", objMember.MustMoveMailFlag);
                arrParam[35] = new SqlParameter("@CountryId", objMember.Countryid);
                arrParam[36] = new SqlParameter("@PayMethod", objMember.PayMehod);


                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "SP_UserMaster_ADMININS", arrParam);
                return obj.ToString();
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }

        }

        public DataTable ListAllCutomers(string strUserName)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[CREATECUSTOMER_SEL]", false);
            getAllTripParm[0].Value = "S";
            getAllTripParm[2].Value = strUserName;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[CREATECUSTOMER_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable CouponDiscount()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[CREATECUSTOMER_SEL]", false);
            getAllTripParm[0].Value = "CD";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[CREATECUSTOMER_SEL]", getAllTripParm).Tables[0];
        }
        public DataSet Edit(string strUserId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[CREATECUSTOMER_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strUserId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[CREATECUSTOMER_SEL]", getAllTripParm);
        }
        public DataTable AlreadyExist(string strEmail, string RowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_UserMaster_ADMININS]", false);
            getAllTripParm[0].Value = "AE";
            getAllTripParm[1].Value = RowId;
            getAllTripParm[6].Value = strEmail;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_UserMaster_ADMININS]", getAllTripParm).Tables[0];
        }

        public string UpdateCoupon(string strUsers, string strCouponCode)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_UserMaster_ADMININS", false);
            SaveNewTicketParams[0].Value = "UC";
            SaveNewTicketParams[44].Value = strCouponCode;
            SaveNewTicketParams[45].Value = strUsers;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_UserMaster_ADMININS", SaveNewTicketParams);
            return obj.ToString();
        }

        public string DeletePreference(string strUserID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CREATECUSTOMER_SEL]", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[CREATECUSTOMER_SEL]", SaveNewTicketParams);
            return obj.ToString();
        }

        public string AddPreference(AbstractLayer.CreateCustomerBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[20];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.PreManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.PreRowID);
                arrParam[2] = new SqlParameter("@startAirport", objMember.StartAirport);
                arrParam[3] = new SqlParameter("@EndAirport", objMember.EndAirport);
                arrParam[4] = new SqlParameter("@UserId", objMember.RowID);
                arrParam[5] = new SqlParameter("@AvailableDate", objMember.StartDate);
                arrParam[6] = new SqlParameter("@AvailableTime", objMember.Time);
                arrParam[7] = new SqlParameter("@EmailFlag", objMember.EmailFlag);
                arrParam[8] = new SqlParameter("@preferenceflag", "1");
                arrParam[9] = new SqlParameter("@StartCity", objMember.StartCity);
                arrParam[10] = new SqlParameter("@Endcity", objMember.EndCity);
                arrParam[11] = new SqlParameter("@AvailableToDate", objMember.EndDate);
                arrParam[12] = new SqlParameter("@Timeflag", objMember.TimeFlag);
                arrParam[13] = new SqlParameter("@SMSFlag", objMember.SMSFlag);
                arrParam[14] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[15] = new SqlParameter("@StartICAODisp", objMember.StartICAODisp);
                arrParam[16] = new SqlParameter("@EndICAODisp", objMember.EndICAODisp);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "SP_Preference_INS", arrParam);
                return obj.ToString();
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }

        }

        public DataTable AirportMaster()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[CREATECUSTOMER_SEL]", false);
            getAllTripParm[0].Value = "AM";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[CREATECUSTOMER_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable GetEmptyTrips(string strPageIndex, string strRowId, string strFromDate, string strToDate, string strTail)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[GetEmptytrips]", false);
            getAllTripParm[0].Value = strPageIndex;
            getAllTripParm[1].Value = strRowId;
            getAllTripParm[2].Value = strFromDate;
            getAllTripParm[3].Value = strToDate;
            getAllTripParm[4].Value = strTail;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[GetEmptytrips]", getAllTripParm).Tables[0];
        }

        public DataTable GetLeg(string strTailNo, string strTripNo, string strAvailableFrom, string strStartTime, string strEndTime, string strStartAirport, string strEndAirport)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[GetLeg]", false);
            getAllTripParm[0].Value = strTailNo;
            getAllTripParm[1].Value = strTripNo;
            getAllTripParm[2].Value = strAvailableFrom;
            getAllTripParm[3].Value = strStartTime;
            getAllTripParm[4].Value = strEndTime;
            getAllTripParm[5].Value = strStartAirport;
            getAllTripParm[6].Value = strEndAirport;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[GetLeg]", getAllTripParm).Tables[0];
        }

        public string Sync_CouponDiscount(string strUserID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_UserMaster_ADMININS]", false);
            SaveNewTicketParams[0].Value = "SYNC";
            SaveNewTicketParams[1].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[SP_UserMaster_ADMININS]", SaveNewTicketParams);
            return obj.ToString();
        }

        public string Sync_CouponDiscount_Delete(string strUserID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_UserMaster_ADMININS]", false);
            SaveNewTicketParams[0].Value = "SYNCDEL";
            SaveNewTicketParams[1].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[SP_UserMaster_ADMININS]", SaveNewTicketParams);
            return obj.ToString();
        }
    }
}