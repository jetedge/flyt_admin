﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for ZoomCalculatinDAL
/// </summary>
public class ZoomCalculatinDAL
{
    #region PricingCalculation

    public void UpdateFLAG()
    {
        SqlParameter[] UpdateFLAGParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS]", false);
        UpdateFLAGParm[0].Value = "UPIN";
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS]", UpdateFLAGParm);
    }

   



    public void UpdateShowPrice(string strPriceFlag, string strTailNo, string strTrip, string strAvailableFrom)
    {
        SqlParameter[] UpdateShowPriceParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS]", false);
        UpdateShowPriceParm[0].Value = "UPDTR";
        UpdateShowPriceParm[1].Value = strPriceFlag;
        UpdateShowPriceParm[2].Value = strTailNo;
        UpdateShowPriceParm[3].Value = strTrip;
        UpdateShowPriceParm[4].Value = strAvailableFrom;
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS]", UpdateShowPriceParm);
    }
    public DataTable SpeicalPricing()
    {
        SqlParameter[] UpdateShowPriceParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS]", false);
        UpdateShowPriceParm[0].Value = "SPPR";
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS]", UpdateShowPriceParm).Tables[0];
    }



    public void InsertLive(string[] strarray)
    {
        SqlParameter[] Get_EmptySetupParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Price_02_INS]", false);
        Get_EmptySetupParm[0].Value = strarray[0];
        Get_EmptySetupParm[1].Value = strarray[1];
        Get_EmptySetupParm[2].Value = strarray[2];
        Get_EmptySetupParm[3].Value = strarray[3];
        Get_EmptySetupParm[4].Value = strarray[4];
        Get_EmptySetupParm[5].Value = strarray[5];
        Get_EmptySetupParm[6].Value = strarray[6];
        Get_EmptySetupParm[7].Value = strarray[7];
        Get_EmptySetupParm[8].Value = strarray[8];
        Get_EmptySetupParm[9].Value = strarray[9];
        Get_EmptySetupParm[10].Value = strarray[10];
        Get_EmptySetupParm[11].Value = strarray[11];
        Get_EmptySetupParm[12].Value = strarray[12];
        Get_EmptySetupParm[13].Value = strarray[13];
        Get_EmptySetupParm[14].Value = strarray[14];
        Get_EmptySetupParm[15].Value = strarray[15];
        Get_EmptySetupParm[16].Value = strarray[16];
        Get_EmptySetupParm[17].Value = strarray[17];
        Get_EmptySetupParm[18].Value = strarray[18];
        Get_EmptySetupParm[19].Value = strarray[19];
        Get_EmptySetupParm[20].Value = strarray[20];
        Get_EmptySetupParm[21].Value = strarray[21];
        Get_EmptySetupParm[22].Value = strarray[22];
        Get_EmptySetupParm[23].Value = strarray[23];
        Get_EmptySetupParm[24].Value = strarray[24];
        Get_EmptySetupParm[25].Value = strarray[25];
        Get_EmptySetupParm[26].Value = strarray[26];
        Get_EmptySetupParm[27].Value = strarray[27];
        Get_EmptySetupParm[28].Value = strarray[28];
        Get_EmptySetupParm[29].Value = strarray[29];
        Get_EmptySetupParm[30].Value = strarray[30];
        Get_EmptySetupParm[31].Value = strarray[31];
        Get_EmptySetupParm[32].Value = strarray[32];
        Get_EmptySetupParm[33].Value = strarray[33];
        Get_EmptySetupParm[34].Value = strarray[34];
        Get_EmptySetupParm[35].Value = strarray[35];
        Get_EmptySetupParm[36].Value = strarray[36];
        Get_EmptySetupParm[37].Value = strarray[37];
        Get_EmptySetupParm[38].Value = strarray[38];
        Get_EmptySetupParm[39].Value = strarray[39];
        Get_EmptySetupParm[40].Value = strarray[40];
        Get_EmptySetupParm[41].Value = strarray[41];
        Get_EmptySetupParm[42].Value = strarray[42];
        Get_EmptySetupParm[43].Value = strarray[43];
        Get_EmptySetupParm[44].Value = strarray[44];
        Get_EmptySetupParm[45].Value = strarray[45];
        Get_EmptySetupParm[46].Value = strarray[46];

        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[CON_Price_02_INS]", Get_EmptySetupParm);
    }

    //update the customer Dashboard Flag
    public static void UpdateCDFlag()
    {
        SqlParameter[] UpdateCDFlagParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Setup_05_CDFLAG]", false);
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[CON_Setup_05_CDFLAG]", UpdateCDFlagParm);
    }

    public DataSet getTripsNextLeg(string strTrip, string strLeg, string strTail)
    {
        SqlParameter[] getTripsLegParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", false);
        getTripsLegParm[0].Value = "SEL";
        getTripsLegParm[1].Value = strTrip;
        getTripsLegParm[2].Value = strLeg;
        getTripsLegParm[3].Value = strTail;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", getTripsLegParm);
    }

    public static DataSet getTimeZone(string startAirport, string strEndAiport)
    {
        SqlParameter[] getTripsLegParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", false);
        getTripsLegParm[0].Value = "TIMEZONE";
        getTripsLegParm[4].Value = startAirport;
        getTripsLegParm[5].Value = strEndAiport;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", getTripsLegParm);
    }

    public static DataTable getLocalTime(string strTail)
    {
        SqlParameter[] getLocalTimeParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_LOCAL_TIME", false);
        getLocalTimeParm[0].Value = "DAY";
        getLocalTimeParm[1].Value = strTail;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_LOCAL_TIME", getLocalTimeParm).Tables[0];
    }

    #endregion

    #region Price Change

    public DataTable Get_EmptyTripSetup(string strStartDate, string strEndDate)
    {
        SqlParameter[] Get_EmptySetupParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Price_01_SEL]", false);
        Get_EmptySetupParm[0].Value = strStartDate;
        Get_EmptySetupParm[1].Value = strEndDate;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[CON_Price_01_SEL]", Get_EmptySetupParm).Tables[0];
    }


    public DataSet Get_EmptyTripSetupTrip(string strTrip, string strLeg)
    {
        SqlParameter[] Get_EmptySetupParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Price_01_SEL_TRIP]", false);
        Get_EmptySetupParm[0].Value = strTrip;
        Get_EmptySetupParm[1].Value = strLeg;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[CON_Price_01_SEL_TRIP]", Get_EmptySetupParm);
    }
    public void UpdateRevisedAmount(string strTrip, string strLeg, string strRevisedAmount, string strDecreseAmount, string oldAmount, string userID, string strNotes)
    {
        SqlParameter[] Get_EmptySetupParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS_TRIP]", false);
        Get_EmptySetupParm[0].Value = "UPAM";
        Get_EmptySetupParm[1].Value = strTrip;
        Get_EmptySetupParm[2].Value = strLeg;
        Get_EmptySetupParm[3].Value = strRevisedAmount;
        Get_EmptySetupParm[4].Value = strDecreseAmount;

        Get_EmptySetupParm[5].Value = oldAmount;
        Get_EmptySetupParm[6].Value = userID;
        Get_EmptySetupParm[7].Value = strNotes;
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[CON_Price_03_INS_TRIP]", Get_EmptySetupParm);
    }


     

    #endregion
}