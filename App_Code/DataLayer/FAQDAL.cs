﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for FAQDAL
/// </summary>
/// 
namespace DataLayer
{
    public class FAQDAL : FAQInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public FAQDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public string Save(AbstractLayer.FAQBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowId);
                arrParam[2] = new SqlParameter("@Topic", objMember.Topic);
                arrParam[3] = new SqlParameter("@Question", objMember.Question);
                arrParam[4] = new SqlParameter("@Answer", objMember.Answer);
                arrParam[5] = new SqlParameter("@DisSeq", objMember.DisSeq);
                arrParam[6] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[7] = new SqlParameter("@Active", objMember.Active);


                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "SP_FAQ_INS", arrParam);
                return obj.ToString();

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }
         
        }
        public int SaveTopic(AbstractLayer.FAQBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[10];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowId);
                arrParam[2] = new SqlParameter("@TopicName", objMember.Topic);
                arrParam[3] = new SqlParameter("@Active", objMember.Active);
                arrParam[4] = new SqlParameter("@CreatedBy", objMember.CreatedBy);


                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_FAQ_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }
        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_FAQ_INS]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_FAQ_INS]", getAllTripParm).Tables[0];
        }
        public DataTable SelectTopic()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_FAQ_INS]", false);
            getAllTripParm[0].Value = "ST";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_FAQ_INS]", getAllTripParm).Tables[0];
        }
    }
}