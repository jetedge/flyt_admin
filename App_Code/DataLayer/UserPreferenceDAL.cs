﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;


/// <summary>
/// Summary description for UserPreferenceDAL
/// </summary>

namespace DataLayer
{
    public class UserPreferenceDAL : UserPreferenceInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public UserPreferenceDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Update(AbstractLayer.UserPreferenceBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@RowID", objMember.RowID);
                arrParam[1] = new SqlParameter("@StartAirport", objMember.StartAirport);
                arrParam[2] = new SqlParameter("@EndAirport", objMember.EndAirport);
                arrParam[3] = new SqlParameter("@StartCity", objMember.StatCity);
                arrParam[4] = new SqlParameter("@EndCity", objMember.EndCity);
                arrParam[5] = new SqlParameter("@AvailbleDate", objMember.AvailableDate);
                arrParam[6] = new SqlParameter("@AvailableToDate", objMember.AvailableToDate);
                arrParam[7] = new SqlParameter("@AvailableTime", objMember.AvailableTime);
                arrParam[8] = new SqlParameter("@EmailFlag", objMember.EmailFlag);
                arrParam[9] = new SqlParameter("@TimeFlag", objMember.TimeFlag);
                arrParam[10] = new SqlParameter("@SMSFlag", objMember.SMSFlag);

                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_UserPreference_UPD", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }
        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[UserPreferences_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[UserPreferences_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable Child(string strUserId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[UserPreferences_SEL]", false);
            getAllTripParm[0].Value = "CH";
            getAllTripParm[1].Value = strUserId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[UserPreferences_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable UserNameFilter(string strUserName)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[UserPreferences_SEL]", false);
            getAllTripParm[0].Value = "UN";
            getAllTripParm[2].Value = strUserName;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[UserPreferences_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable AirportFilter(string strAirporrt)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[UserPreferences_SEL]", false);
            getAllTripParm[0].Value = "AP";
            getAllTripParm[3].Value = strAirporrt;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[UserPreferences_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable DateFilter(string strDate)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[UserPreferences_SEL]", false);
            getAllTripParm[0].Value = "DT";
            getAllTripParm[4].Value = strDate;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[UserPreferences_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable Edit(string strRowID)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[UserPreferences_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[5].Value = strRowID;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[UserPreferences_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable Preference_Report(string MansgeType, string UserName, string Airport, string FilterDate)
        {
            SqlParameter[] ReportParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[UserPreferences_SEL]", false);
            ReportParm[0].Value = MansgeType;
            ReportParm[2].Value = UserName;
            ReportParm[3].Value = Airport;
            ReportParm[4].Value = FilterDate;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[UserPreferences_SEL]", ReportParm).Tables[0];
        }
    }
}