﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for SchedulerDAL
/// </summary>
/// 
namespace DataLayer
{
    public class SchedulerDAL : SchedulerInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public SchedulerDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.SchedulerBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@ScheID", objMember.RowID);
                arrParam[2] = new SqlParameter("@ToAddress", objMember.ToAddress);
                arrParam[3] = new SqlParameter("@EffDate", objMember.ScheduleFRom);
                arrParam[4] = new SqlParameter("@EffTime", objMember.Time);
                arrParam[5] = new SqlParameter("@Signature", objMember.Signature);
                arrParam[6] = new SqlParameter("@LastPostingTime", null);
                arrParam[7] = new SqlParameter("@NextPostingTime", objMember.PostingTime);
                arrParam[8] = new SqlParameter("@Createdby", objMember.CreatedBy);
                arrParam[9] = new SqlParameter("@UserID", objMember.UserId);
                arrParam[10] = new SqlParameter("@RunFlag", objMember.RunFlag);
                arrParam[11] = new SqlParameter("@SchDays", objMember.SchDays);
                arrParam[12] = new SqlParameter("@Move", objMember.MustMove);
                arrParam[13] = new SqlParameter("@ReportFor", objMember.ReportFor);

                arrParam[14] = new SqlParameter("@MessageFlag", objMember.MessageFlag);
                arrParam[15] = new SqlParameter("@MessageId", objMember.MessageID);
                arrParam[16] = new SqlParameter("@Message", objMember.Message);
                arrParam[17] = new SqlParameter("@StartDate", objMember.StartDate);
                arrParam[18] = new SqlParameter("@StartTime", objMember.StartTime);
                arrParam[19] = new SqlParameter("@EndDate", objMember.EndDate);
                arrParam[20] = new SqlParameter("@EndTime", objMember.EndTime);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "Zoom_INS_SCHEDULER", arrParam);
                return Convert.ToInt32(obj.ToString());

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            
        }
        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ZOOM_SEL_SCHEDULER]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[ZOOM_SEL_SCHEDULER]", getAllTripParm).Tables[0];
        }
        public DataTable BindEMAIl(string strSCHID)
        {
            SqlParameter[] saveBankParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ZOOM_SEL_SCHEDULER]", false);
            saveBankParam[0].Value = "GE";
            saveBankParam[1].Value = strSCHID;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[ZOOM_SEL_SCHEDULER]", saveBankParam).Tables[0];
        }
        public DataTable BindExternalUser(string strSchId)
        {
            SqlParameter[] savePaymentParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ZOOM_ExternalUser_INS]", false);
            savePaymentParam[0].Value = "S";
            savePaymentParam[1].Value = strSchId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[ZOOM_ExternalUser_INS]", savePaymentParam).Tables[0];
        }
        public DataTable BindSchedulePerticular(string strSchID)
        {
            SqlParameter[] saveBankParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ZOOM_SEL_SCHEDULER]", false);
            saveBankParam[0].Value = "G";
            saveBankParam[1].Value = strSchID;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[ZOOM_SEL_SCHEDULER]", saveBankParam).Tables[0];
        }
        public DataTable BindSchedulerStatus(string strSCHID)
        {
            SqlParameter[] BindSchedulerStatus = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ZOOM_INS_SCHEDULER]", false);
            BindSchedulerStatus[0].Value = "SS";
            BindSchedulerStatus[1].Value = strSCHID;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[ZOOM_INS_SCHEDULER]", BindSchedulerStatus).Tables[0];
        }

        public DataTable getToAddress(string strManageType)
        {
            SqlParameter[] saveBankParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "ZOOM_SEL_SCHEDULER", false);
            saveBankParam[0].Value = strManageType;
            return SqlHelper.ExecuteDataset(m_Connection_String, "ZOOM_SEL_SCHEDULER", saveBankParam).Tables[0];
        }

        public string UpdateRunFlag(string strSchId, string strFlag)
        {
            SqlParameter[] updateAlertParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "ZOOM_INS_SCHEDULER", false);
            updateAlertParam[0].Value = "URF";
            updateAlertParam[1].Value = strSchId;
            updateAlertParam[11].Value = strFlag;
            object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, "ZOOM_INS_SCHEDULER", updateAlertParam).ToString();
            return obj.ToString();
        }

        public string DeleteEMAIL(string strScHID)
        {
            SqlParameter[] SaveEMAIL = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "Zoom_INS_SCHEDULER", false);
            SaveEMAIL[0].Value = "DE";
            SaveEMAIL[1].Value = strScHID;
            object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, "Zoom_INS_SCHEDULER", SaveEMAIL);
            return obj.ToString();
        }
        public string SaveEMAIL(string strScHID, string strUserID)
        {
            SqlParameter[] SaveEMAIL = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "Zoom_INS_SCHEDULER", false);
            SaveEMAIL[0].Value = "IE";
            SaveEMAIL[1].Value = strScHID;
            SaveEMAIL[10].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, "Zoom_INS_SCHEDULER", SaveEMAIL);
            return obj.ToString();
        }

        public string SaveScheduleStatus(string strManageType, string strSchID, string strRunFlag, string strUserID)
        {
            SqlParameter[] updateAlertParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "Zoom_INS_SCHEDULER", false);
            updateAlertParam[0].Value = strManageType;
            updateAlertParam[1].Value = strSchID;
            updateAlertParam[11].Value = strRunFlag;
            updateAlertParam[8].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, "Zoom_INS_SCHEDULER", updateAlertParam);
            return obj.ToString();
        }

        public string SaveExternalUser(string strSchid, string strName, string strEmailid, string strUserRole, string strCreatedby)
        {
            SqlParameter[] ExternalUserParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ZOOM_ExternalUser_INS]", false);
            ExternalUserParam[0].Value = "I";
            ExternalUserParam[1].Value = strSchid;
            ExternalUserParam[2].Value = strName;
            ExternalUserParam[3].Value = strEmailid;
            ExternalUserParam[4].Value = strUserRole;
            ExternalUserParam[5].Value = strCreatedby;
            object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, "[ZOOM_ExternalUser_INS]", ExternalUserParam).ToString();
            return obj.ToString();
        }


        #region Customer Signup Scheduler

        public DataTable Get_ReceiveEmailAlerts(string strManageType)
        {
            SqlParameter[] saveBankParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "ZOOM_SEL_SCHEDULER", false);
            saveBankParam[0].Value = strManageType;
            return SqlHelper.ExecuteDataset(m_Connection_String, "ZOOM_SEL_SCHEDULER", saveBankParam).Tables[0];
        }

        public DataTable Select_CustomerSignup_Sch()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[ZOOM_SEL_SCHEDULER]", false);
            getAllTripParm[0].Value = "CUSTSEL";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[ZOOM_SEL_SCHEDULER]", getAllTripParm).Tables[0];
        }

        #endregion

    }
}