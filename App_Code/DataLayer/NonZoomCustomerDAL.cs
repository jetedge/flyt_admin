﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;
/// <summary>
/// Summary description for NonZoomCustomerDAL
/// </summary>
/// 
namespace DataLayer
{
    public class NonZoomCustomerDAL : NonZoomCustomerInterface
    {
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public NonZoomCustomerDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.NonZoomCustomerBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowID);
                arrParam[2] = new SqlParameter("@Title", objMember.Title);
                arrParam[3] = new SqlParameter("@FIrstName", objMember.firstName);
                arrParam[4] = new SqlParameter("@LastName", objMember.lastName);
                arrParam[5] = new SqlParameter("@Email", objMember.Email);
                arrParam[6] = new SqlParameter("@CompanyName", objMember.CompanyName);
                arrParam[7] = new SqlParameter("@Code_Phone", objMember.PhoneCode);
                arrParam[8] = new SqlParameter("@PhoneNumber", objMember.Phone);
                arrParam[9] = new SqlParameter("@Address", objMember.Address1);
                arrParam[10] = new SqlParameter("@Address2", objMember.Address2);
                arrParam[11] = new SqlParameter("@City", objMember.City);
                arrParam[12] = new SqlParameter("@State", objMember.State);
                arrParam[13] = new SqlParameter("@Country", objMember.Country);
                arrParam[14] = new SqlParameter("@Zipcode", objMember.ZipCode);
                arrParam[15] = new SqlParameter("@ACtive", objMember.Active);
                arrParam[16] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[17] = new SqlParameter("@MustMoveFlag", objMember.MustMoveFlag);
                arrParam[18] = new SqlParameter("@CountryId", objMember.CountryId);

                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_NonZoomCutomer_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }
        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_NonZoomCutomer_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_NonZoomCutomer_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable Edit(string strUserId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_NonZoomCutomer_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strUserId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_NonZoomCutomer_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable AlreadyExist(string strEmail)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_NonZoomCutomer_INS]", false);
            getAllTripParm[0].Value = "AE";
            getAllTripParm[5].Value = strEmail;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_NonZoomCutomer_INS]", getAllTripParm).Tables[0];
        }

        public string DeletUser(string strUserID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_NonZoomCutomer_SEL", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_NonZoomCutomer_SEL", SaveNewTicketParams);
            return obj.ToString();
        }
    }
}