﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for CountryFeeDAL
/// </summary>
/// 
namespace DataLayer
{
    public class CountryFeeDAL : CountryFeeInterface
    {
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public CountryFeeDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public string Save(AbstractLayer.CountryFeeBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowID);
                arrParam[2] = new SqlParameter("@CountryID", objMember.Countryid);
                arrParam[3] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[4] = new SqlParameter("@Active", objMember.CreatedBy);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_CountryFee_INS", arrParam);
                return obj.ToString();
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }
        }
        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_CountryFee_INS]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_CountryFee_INS]", getAllTripParm).Tables[0];
        }

        public DataTable Child(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_CountryFee_INS]", false);
            getAllTripParm[0].Value = "C";
            getAllTripParm[1].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_CountryFee_INS]", getAllTripParm).Tables[0];
        }

        public DataTable Edit(string strUserId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_CountryFee_INS]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strUserId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_CountryFee_INS]", getAllTripParm).Tables[0];
        }
        public string DeletUser(string strUserID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_CountryFee_INS", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_CountryFee_INS", SaveNewTicketParams);
            return obj.ToString();
        }
    }
}