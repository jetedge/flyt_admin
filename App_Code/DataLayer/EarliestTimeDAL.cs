﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for BookedReportBLL
/// </summary>
/// 
namespace DataLayer
{
    public class EarliestTimeDAL
    {
        public DataSet getTripsNextLeg(string strTrip, string strLeg, string strTail)
        {
            SqlParameter[] getTripsLegParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", false);
            getTripsLegParm[0].Value = "SEL";
            getTripsLegParm[1].Value = strTrip;
            getTripsLegParm[2].Value = strLeg;
            getTripsLegParm[3].Value = strTail;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", getTripsLegParm);
        }
        public string INTExist(string strAirport, string strManageType)
        {
            SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_Airport", false);
            GetLegDetails[0].Value = strManageType;
            GetLegDetails[2].Value = strAirport;
            return SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_Airport", GetLegDetails).ToString();
        }

    }
}