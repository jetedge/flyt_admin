﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Get the data for binding the home page grid
/// </summary>
/// 
namespace DataLayer
{
    public class PriceCharterDAL : PriceCharterInterface
    {

        //Get the Empty Legs from the table using the stored procedure

        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public PriceCharterDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public DataTable Select()
        {
            string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
            string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[GetEmptytrips]", false);
            saveCategoryParam[0].Value = 1;
            saveCategoryParam[1].Value = "25";
            saveCategoryParam[2].Value = strStartDate;
            saveCategoryParam[3].Value = strEndDate;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[GetEmptytrips]", saveCategoryParam).Tables[1];
        }

        // Get the Charter price list for the selected tail

        public DataTable BindCharterPrice(string strTailNo, string strAvailableFrom, string strStart, string strEnd)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_GET_CHARTERPRICE]", false);
            saveCategoryParam[0].Value = strTailNo;
            saveCategoryParam[1].Value = strAvailableFrom;
            saveCategoryParam[2].Value = strStart;
            saveCategoryParam[3].Value = strEnd;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[SP_GET_CHARTERPRICE]", saveCategoryParam).Tables[0];
        }
    }
}