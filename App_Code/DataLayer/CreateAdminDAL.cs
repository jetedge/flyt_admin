﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;


/// <summary>
/// Summary description for CreateAdminDAL
/// </summary>
/// 
namespace DataLayer
{
    public class CreateAdminDAL : CreateAdminInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public CreateAdminDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public string Save(AbstractLayer.CreateAdminBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowID);
                arrParam[2] = new SqlParameter("@Role", objMember.UserType);
                arrParam[3] = new SqlParameter("@Password", objMember.Password);
                arrParam[4] = new SqlParameter("@MustchangePassword", "1");
                arrParam[5] = new SqlParameter("@Title", objMember.Title);
                arrParam[6] = new SqlParameter("@FIrstName", objMember.firstName);
                arrParam[7] = new SqlParameter("@LastName", objMember.lastName);
                arrParam[8] = new SqlParameter("@Email", objMember.Email);
                arrParam[9] = new SqlParameter("@Code_Phone", objMember.PhoneCode);
                arrParam[10] = new SqlParameter("@PhoneNumber", objMember.Phone);
                arrParam[11] = new SqlParameter("@Address", objMember.Address1);
                arrParam[12] = new SqlParameter("@Address2", objMember.Address2);
                arrParam[13] = new SqlParameter("@City", objMember.City);
                arrParam[14] = new SqlParameter("@State", objMember.State);
                arrParam[15] = new SqlParameter("@Country", objMember.Country);
                arrParam[16] = new SqlParameter("@Zipcode", objMember.ZipCode);
                arrParam[17] = new SqlParameter("@ACtive", objMember.Active);
                arrParam[18] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[19] = new SqlParameter("@CountryId", objMember.CountryId);

                arrParam[20] = new SqlParameter("@BookingFlag", objMember.BookingConfirmation);
                arrParam[21] = new SqlParameter("@ExceptionFlag", objMember.Exception);
                arrParam[22] = new SqlParameter("@SavedRoutesFlag", objMember.SavedRoutes);
                arrParam[23] = new SqlParameter("@MustMoveFlag", objMember.MustMove);
                arrParam[24] = new SqlParameter("@signUpFlag", objMember.signUp);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "SP_UserMaster_INS", arrParam);
                return obj.ToString();

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }
            return "-1";
        }
        public DataTable List()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[CREATEADMINUSER_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[CREATEADMINUSER_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable Edit(string strUserId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[CREATEADMINUSER_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strUserId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[CREATEADMINUSER_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable DuplicateCheck(string strEmail)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_UserMaster_INS]", false);
            getAllTripParm[0].Value = "AE";
            getAllTripParm[6].Value = strEmail;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_UserMaster_INS]", getAllTripParm).Tables[0];
        }
        public string InsertEmailSetup(string strUserID, string strEmail, string strFirstName, string strLastName)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_UserMaster_INS", false);
            SaveNewTicketParams[0].Value = "IE";
            SaveNewTicketParams[1].Value = strUserID;
            SaveNewTicketParams[6].Value = strEmail;
            SaveNewTicketParams[3].Value = strFirstName;
            SaveNewTicketParams[4].Value = strLastName;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_UserMaster_INS", SaveNewTicketParams);
            return obj.ToString();
        }

        public string Delete(string strUserID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "CREATEADMINUSER_SEL", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "CREATEADMINUSER_SEL", SaveNewTicketParams);
            return obj.ToString();
        }


        public DataTable Select_User_Login(string EmailId, string ManageType)
        {
            SqlParameter[] Landing_Images_Param = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_UserMaster_SEL]", false);
            Landing_Images_Param[0].Value = ManageType; //MANAGETYPE
            Landing_Images_Param[1].Value = EmailId;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[SP_UserMaster_SEL]", Landing_Images_Param).Tables[0];
        }
    }
}