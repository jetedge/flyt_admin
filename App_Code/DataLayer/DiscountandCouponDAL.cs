﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for DiscountandCouponDAL
/// </summary>
namespace DataLayer
{
    public class DiscountandCouponDAL : DiscountandCouponInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;
        public DiscountandCouponDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public int SaveDiscountandCoupon(DiscountandCouponBase objDiscount)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@ManageType", objDiscount.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objDiscount.RowID);
                arrParam[2] = new SqlParameter("@CouponCode", objDiscount.CouponCode);
                arrParam[3] = new SqlParameter("@CouponDescription", objDiscount.CouponDescription);
                arrParam[4] = new SqlParameter("@StartDate", objDiscount.StartDate);
                arrParam[5] = new SqlParameter("@ExpiryDate", objDiscount.ExpiryDate);
                arrParam[6] = new SqlParameter("@CouponValue", objDiscount.CouponValue);
                arrParam[7] = new SqlParameter("@CouponPercentage", objDiscount.CouponPercentage);
                arrParam[8] = new SqlParameter("@Discountflag", objDiscount.Discountflag);
                arrParam[9] = new SqlParameter("@Description", objDiscount.Description);
                arrParam[10] = new SqlParameter("@Active", objDiscount.Active);
                arrParam[11] = new SqlParameter("@UserFlag", objDiscount.UserFlag);
                arrParam[12] = new SqlParameter("@Createdby", objDiscount.Createdby);
                arrParam[13] = new SqlParameter("@CreatedOn", objDiscount.CreatedOn);
                arrParam[14] = new SqlParameter("@Modifiedby", objDiscount.Modifiedby);
                arrParam[15] = new SqlParameter("@ModifiedOn", objDiscount.ModifiedOn);
                arrParam[16] = new SqlParameter("@AutoApply", objDiscount.AutoApply);
                arrParam[17] = new SqlParameter("@ApplicableFor", objDiscount.ApplicableFor);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "SP_DiscountandCoupon_INS", arrParam);
                return Convert.ToInt32(obj);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
        }
        public DataTable ListDiscountandCoupon(string Status)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_DiscountandCoupon_SEL]", false);
            getAllTripParm[0].Value = "SEL";
            getAllTripParm[2].Value = Status;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_DiscountandCoupon_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable EditDiscountandCoupon(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_DiscountandCoupon_SEL]", false);
            getAllTripParm[0].Value = "SELC";
            getAllTripParm[1].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_DiscountandCoupon_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable AlreadyExist(string strEmail)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_DiscountandCoupon_INS]", false);
            getAllTripParm[0].Value = "AE";
            getAllTripParm[6].Value = strEmail;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_DiscountandCoupon_INS]", getAllTripParm).Tables[0];
        }
        public DataTable List_RegisteredUser(string ManageType, string UserName, string Email, string DiscountType)
        {
            SqlParameter[] RegisteredUserParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_DiscountandCoupon_SEL]", false);
            RegisteredUserParm[0].Value = ManageType;
            RegisteredUserParm[4].Value = UserName;
            RegisteredUserParm[5].Value = Email;
            RegisteredUserParm[6].Value = DiscountType;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_DiscountandCoupon_SEL]", RegisteredUserParm).Tables[0];
        }
        public int SaveCoupon_Users(DiscountandCouponBase objDiscount)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@ManageType", objDiscount.ManageType);
                arrParam[1] = new SqlParameter("@DiscountId", objDiscount.RowID);
                arrParam[2] = new SqlParameter("@UserId", objDiscount.UserId);
                arrParam[3] = new SqlParameter("@ApplicableFlag", objDiscount.ApplicableFlag);
                arrParam[4] = new SqlParameter("@StartDate", objDiscount.StartDate);
                arrParam[5] = new SqlParameter("@EndDate", objDiscount.EndDate);
                arrParam[6] = new SqlParameter("@ModifiedBy", objDiscount.Modifiedby);
                arrParam[7] = new SqlParameter("@ModifiedOn", objDiscount.ModifiedOn);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "SP_DiscountUsers_INS", arrParam);
                return Convert.ToInt32(obj);
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
        }
        public int DeleteCoupon_Users(DiscountandCouponBase objDiscount)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@ManageType", objDiscount.ManageType);
                arrParam[1] = new SqlParameter("@DiscountId", objDiscount.RowID);

                //pass connection string, storedprocedure name and parameter array
                return SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_DiscountUsers_INS", arrParam);
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
        }
        public DataTable SelectCoupon_Users(string strRowId)
        {
            SqlParameter[] SelectCouponParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_DiscountUsers_INS]", false);
            SelectCouponParm[0].Value = "SEL";
            SelectCouponParm[2].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_DiscountUsers_INS]", SelectCouponParm).Tables[0];
        }
    }
}