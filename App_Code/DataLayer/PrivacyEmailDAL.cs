﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for PrivacyEmailDAL
/// </summary>
namespace DataLayer
{
    public class PrivacyEmailDAL : PrivacyEmailInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public PrivacyEmailDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.PrivacyEmailbase objPrivacy)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@ManageTye", objPrivacy.ManageTye);
                arrParam[1] = new SqlParameter("@PID", objPrivacy.PID);
                arrParam[2] = new SqlParameter("@FirstName", objPrivacy.FirstName);
                arrParam[3] = new SqlParameter("@LastName", objPrivacy.LastName);
                arrParam[4] = new SqlParameter("@EmailID", objPrivacy.EmailID);
                arrParam[5] = new SqlParameter("@CreatedBy", objPrivacy.CreatedBy);


                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "PrivacyEmailAddress_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }

        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PrivacyEmailAddress_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PrivacyEmailAddress_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable Edit(string strUserId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PrivacyEmailAddress_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strUserId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PrivacyEmailAddress_SEL]", getAllTripParm).Tables[0];
        }


        public string DeletUser(string strUserId)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "PrivacyEmailAddress_SEL", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strUserId;
            object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, "PrivacyEmailAddress_SEL", SaveNewTicketParams);
            return obj.ToString();
        }


        public DataTable SelectPR()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PrivacyRemoval_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[PrivacyRemoval_SEL]", getAllTripParm).Tables[0];
        }

    }
}