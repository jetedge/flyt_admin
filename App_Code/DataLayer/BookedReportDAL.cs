﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;


/// <summary>
/// Summary description for BookedReportDAL
/// </summary>
/// 
namespace DataLayer
{
    public class BookedReportDAL : BookedReportInterface
    {

        //Get the Empty Legs from the table using the stored procedure

        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public BookedReportDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public DataTable Select(string strFromDate, string strToDate, string strTailNo, string strTripNo)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[BookedReport_SEL]", false);
            saveCategoryParam[0].Value = strFromDate;
            saveCategoryParam[1].Value = strToDate;
            saveCategoryParam[2].Value = strTailNo;
            saveCategoryParam[3].Value = strTripNo;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[BookedReport_SEL]", saveCategoryParam).Tables[0];
        }

        public DataTable AdminUser_Log_Report(string strRepFromDate, string strRepToDate, string strUserName)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_AdminUser_Log]", false);
            saveCategoryParam[0].Value = strRepFromDate;
            saveCategoryParam[1].Value = strRepToDate;
            saveCategoryParam[2].Value = strUserName;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[SP_AdminUser_Log]", saveCategoryParam).Tables[0];
        }

        public DataSet Select_PageHit(string strRepFromDate, string strRepToDate)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PageHits_SEL]", false);
            saveCategoryParam[0].Value = strRepFromDate;
            saveCategoryParam[1].Value = strRepToDate;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[PageHits_SEL]", saveCategoryParam);
        }

        public DataTable PageHit_TopBookings(string strRepFromDate, string strRepToDate)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PageHits_SEL_Booking]", false);
            saveCategoryParam[0].Value = strRepFromDate;
            saveCategoryParam[1].Value = strRepToDate;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[PageHits_SEL_Booking]", saveCategoryParam).Tables[0];
        }

        public DataTable PageHit_GetPopup_Details(string country, string regionName, string city, string strRepFromDate, string strRepToDate)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[PageHits_SEL_Popup]", false);
            saveCategoryParam[0].Value = "SEL";
            saveCategoryParam[1].Value = strRepFromDate;
            saveCategoryParam[2].Value = strRepToDate;
            saveCategoryParam[3].Value = country;
            saveCategoryParam[4].Value = regionName;
            saveCategoryParam[5].Value = city;
            return SqlHelper.ExecuteDataset(m_Connection_String, "[PageHits_SEL_Popup]", saveCategoryParam).Tables[0];
        }
    }
}