﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for FAQDAL
/// </summary>
/// 
namespace DataLayer
{
    public class GlobalDAL
    {
        public DataSet SELData(string strManageType)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "GlobalConfig_SEL", false);
            SaveNewTicketParams[0].Value = strManageType;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "GlobalConfig_SEL", SaveNewTicketParams);
        }

        public static DataSet Get_JeSecrets()
        {
            SqlParameter[] Landing_Images_Param = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_SECRETS_SEL]", false);
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[SP_SECRETS_SEL]", Landing_Images_Param);
        }

    }
}