﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for AirportRuleDAL
/// </summary>

namespace DataLayer
{
    public class AirportRuleDAL : AirportRuleInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public AirportRuleDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.AirportRuleBase objAirportRule)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@ManageType", objAirportRule.ManageType);
                arrParam[1] = new SqlParameter("@RuleId", objAirportRule.RuleId);

                arrParam[2] = new SqlParameter("@DepSetupBy", objAirportRule.DepSetupBy);
                arrParam[3] = new SqlParameter("@DepRuleCode", objAirportRule.DepRuleCode);
                arrParam[4] = new SqlParameter("@DepRuleName", objAirportRule.DepRuleName);

                arrParam[5] = new SqlParameter("@ArrSetupBy", objAirportRule.ArrSetupBy);
                arrParam[6] = new SqlParameter("@ArrRuleCode", objAirportRule.ArrRuleCode);
                arrParam[7] = new SqlParameter("@ArrRuleName", objAirportRule.ArrRuleName);

                arrParam[8] = new SqlParameter("@Status", objAirportRule.Status);
                arrParam[9] = new SqlParameter("@CreatedBy", objAirportRule.CreatedBy);
                arrParam[10] = new SqlParameter("@ModifiedBy", objAirportRule.ModifiedBy);

                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_AirportRule_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }
        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_AirportRule_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_AirportRule_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable Edit(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_AirportRule_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_AirportRule_SEL]", getAllTripParm).Tables[0];
        }
        public string DeletAirportRule(string strRowId)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportRule_SEL", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strRowId;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_AirportRule_SEL", SaveNewTicketParams);
            return obj.ToString();
        }
    }
}