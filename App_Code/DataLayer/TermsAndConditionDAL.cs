﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for TermsAndConditionDAL
/// </summary>
/// 
namespace DataLayer
{
    public class TermsAndConditionDAL : TermsAndConditionInterface
    {
         // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public TermsAndConditionDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public string Save(AbstractLayer.TermsAndConditionBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowId);
                arrParam[2] = new SqlParameter("@Version", objMember.Version);
                arrParam[3] = new SqlParameter("@FileName", objMember.FileName);
                arrParam[4] = new SqlParameter("@FilePAth", objMember.FilePAth);
                arrParam[5] = new SqlParameter("@Effectivefrom", objMember.EffectiveFrom);
                arrParam[6] = new SqlParameter("@Notes", objMember.Notes);
                arrParam[7] = new SqlParameter("@CreatedBy", objMember.CreatedBy);

                //pass connection string, storedprocedure name and parameter array
             object obj=   SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "SP_TermsAndCondition_INS", arrParam);
             return obj.ToString();
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }
        }
        public DataSet Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[TermsAndCondition_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[TermsAndCondition_SEL]", getAllTripParm);
        }
        public DataTable Edit(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[TermsAndCondition_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[TermsAndCondition_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable History(string strTaxFlag,string strCode)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[TermsAndCondition_SEL]", false);
            getAllTripParm[0].Value = "H";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[TermsAndCondition_SEL]", getAllTripParm).Tables[0];
        }
    }
}