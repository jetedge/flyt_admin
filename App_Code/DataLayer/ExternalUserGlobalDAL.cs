﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using AbstractLayer;
using System.Data;

namespace DataLayer
{
    /// <summary>
    /// Summary description for ExternalUserGlobalDAL
    /// </summary>
    public class ExternalUserGlobalDAL : ExternalUserGlobalInterface
    {
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public ExternalUserGlobalDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public string Save(AbstractLayer.ExternalUserGlobalBase objGlobal)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@ManageTye", objGlobal.ManageTye);
                arrParam[1] = new SqlParameter("@ExtId", objGlobal.ExtId);
                arrParam[2] = new SqlParameter("@UserName", objGlobal.UserName);
                arrParam[3] = new SqlParameter("@EmailId", objGlobal.EmailId);
                arrParam[4] = new SqlParameter("@UserRole", objGlobal.UserRole);
                arrParam[5] = new SqlParameter("@CreatedBy", objGlobal.CreatedBy);


                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "Booking_ExternalUsers_INS", arrParam);
                return obj.ToString();


            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }

        }

        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[Booking_ExternalUsers_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[Booking_ExternalUsers_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable Edit(string strUserId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[Booking_ExternalUsers_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[1].Value = strUserId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[Booking_ExternalUsers_SEL]", getAllTripParm).Tables[0];
        }

        public string Delete(string strUserID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "Booking_ExternalUsers_SEL", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strUserID;
            object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, "Booking_ExternalUsers_SEL", SaveNewTicketParams);
            return obj.ToString();
        }


        public string Save_DayDutyFilter(AbstractLayer.ExternalUserGlobalBase objGlobal)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@ManageTye", objGlobal.ManageTye);
                arrParam[1] = new SqlParameter("@DutyId", objGlobal.DutyId);
                arrParam[2] = new SqlParameter("@BLKHrs", objGlobal.BLKHours);
                arrParam[3] = new SqlParameter("@CrewHrs", objGlobal.CrewDuty);
                arrParam[4] = new SqlParameter("@EnableFlag", objGlobal.CrewDutyFilter);
                arrParam[5] = new SqlParameter("@CreatedBy", objGlobal.CreatedBy);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteScalar(m_Connection_String, CommandType.StoredProcedure, "GlobalConfig_CrewDayFilter_INS", arrParam);
                return obj.ToString();
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }

        }

        public DataTable List_CrewDutyFilter()
        {
            SqlParameter[] CrewDutyFilterParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[GlobalConfig_CrewDayFilter_INS]", false);
            CrewDutyFilterParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[GlobalConfig_CrewDayFilter_INS]", CrewDutyFilterParm).Tables[0];
        }

        public DataTable Bind_CustomFLYT()
        {
            SqlParameter[] CrewDutyFilterParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[CustomFlyt_TotalTime_INS]", false);
            CrewDutyFilterParm[0].Value = "HIS";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[CustomFlyt_TotalTime_INS]", CrewDutyFilterParm).Tables[0];
        }

        public DataTable Bind_ForFlight()
        {
            SqlParameter[] CrewDutyFilterParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[GlobalConfig_SEL]", false);
            CrewDutyFilterParm[0].Value = "FOREF";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[GlobalConfig_SEL]", CrewDutyFilterParm).Tables[0];
        }
    }
}