﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for FixedPriceDAL
/// </summary>

namespace DataLayer
{
    public class FixedPriceDAL : FixedPriceInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public FixedPriceDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.FixedPriceBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", objMember.ManageType);
                arrParam[1] = new SqlParameter("@RowID", objMember.RowID);
                arrParam[2] = new SqlParameter("@StartDate", objMember.StartDate);
                arrParam[3] = new SqlParameter("@EndDate", objMember.EndDate);
                arrParam[4] = new SqlParameter("@SpecialPriceDescription", objMember.SpecialPriceDescription);
                arrParam[5] = new SqlParameter("@PriceDetails", objMember.PriceDetails);
                arrParam[6] = new SqlParameter("@StartAirport", objMember.StartAirport);
                arrParam[7] = new SqlParameter("@EndAirPort", objMember.EndAirport);
                arrParam[8] = new SqlParameter("@TailNo", objMember.TailNo);
                arrParam[9] = new SqlParameter("@createdby", objMember.CreatedBy);

                arrParam[10] = new SqlParameter("@StartDisp", objMember.StartAirportDisplay);
                arrParam[11] = new SqlParameter("@EndDisp", objMember.EndAirportDisplay);

                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_SpeicalPricing_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }
        public DataTable ActiveSelect()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[FixedPrice_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[FixedPrice_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable ExpiredSelect()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[FixedPrice_SEL]", false);
            getAllTripParm[0].Value = "S1";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[FixedPrice_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable Edit(string strRowId)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[FixedPrice_SEL]", false);
            getAllTripParm[0].Value = "E";
            getAllTripParm[2].Value = strRowId;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[FixedPrice_SEL]", getAllTripParm).Tables[0];
        }
        public DataTable AlreadyExist(string strStartDate,string strEndDate,string strSpecialPriceDesc,string strStartAirport,string strEndAirport)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_SpeicalPricing_INS]", false);
            getAllTripParm[0].Value = "AE";
            getAllTripParm[2].Value = strStartDate;
            getAllTripParm[3].Value = strEndDate;
            getAllTripParm[4].Value = strSpecialPriceDesc;
            getAllTripParm[6].Value = strStartAirport;
            getAllTripParm[7].Value = strEndAirport;

            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_SpeicalPricing_INS]", getAllTripParm).Tables[0];
        }
        public string DeletUser(string strRowId)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "FixedPrice_SEL", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[2].Value = strRowId;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "FixedPrice_SEL", SaveNewTicketParams);
            return obj.ToString();
        }
    }
}