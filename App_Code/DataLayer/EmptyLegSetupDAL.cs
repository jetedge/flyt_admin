﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for EmptyLegSetupDAL
/// </summary>
/// 
namespace DataLayer
{
    public class EmptyLegSetupDAL : EmptyLegSetupInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public EmptyLegSetupDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }

        public string InsertLivePricingCalculation(AbstractLayer.EmptyLegSetupBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[45];

                arrParam[0] = new SqlParameter("@TailNo", objMember.TailNo);
                arrParam[1] = new SqlParameter("@FlyingDate", objMember.FlyingDate);
                arrParam[2] = new SqlParameter("@FlyingToDate", objMember.FlyingToDate);
                arrParam[3] = new SqlParameter("@StartTime", objMember.StartTime);
                arrParam[4] = new SqlParameter("@EndTime", objMember.EndTime);
                arrParam[5] = new SqlParameter("@StartAirport", objMember.StartAirport);
                arrParam[6] = new SqlParameter("@StartCity", objMember.StartCity);
                arrParam[7] = new SqlParameter("@EndAirport", objMember.EndAirport);
                arrParam[8] = new SqlParameter("@EndCity", objMember.EndCity);
                arrParam[9] = new SqlParameter("@NoOfSeats", objMember.NoOfSeats);
                arrParam[10] = new SqlParameter("@totalHoours", objMember.totalHoours);
                arrParam[11] = new SqlParameter("@CalculatelyHours", objMember.CalculatelyHours);
                arrParam[12] = new SqlParameter("@HourlyRate", objMember.HourlyRate);
                arrParam[13] = new SqlParameter("@FixedFloor", objMember.FixedFloor);
                arrParam[14] = new SqlParameter("@FixedCeeling", objMember.FixedCeeling);
                arrParam[15] = new SqlParameter("@MinTime", objMember.MinTime);
                arrParam[16] = new SqlParameter("@MaxTime", objMember.MaxTime);
                arrParam[17] = new SqlParameter("@MinSeallable", objMember.MinSeallable);
                arrParam[18] = new SqlParameter("@TotalDays", objMember.TotalDays);
                arrParam[20] = new SqlParameter("@LMinPrice", objMember.LMinPrice);
                arrParam[21] = new SqlParameter("@LMaxPrice", objMember.LMaxPrice);
                arrParam[22] = new SqlParameter("@FinalPrice", objMember.FinalPrice);
                arrParam[23] = new SqlParameter("@Time", objMember.Time);
                arrParam[24] = new SqlParameter("@Date", objMember.Date);
                arrParam[25] = new SqlParameter("@FLAG", objMember.FLAG);
                arrParam[26] = new SqlParameter("@CBPFFlag", objMember.CBPFFlag);
                arrParam[27] = new SqlParameter("@BookingFlag", objMember.BookingFlag);
                arrParam[28] = new SqlParameter("@ApplyTax", objMember.ApplyTax);
                arrParam[29] = new SqlParameter("@TripNo", objMember.TripNo);
                arrParam[30] = new SqlParameter("@EnableTrip", objMember.EnableTrip);
                arrParam[31] = new SqlParameter("@ACtive", objMember.ACtive);
                arrParam[32] = new SqlParameter("@PriceFlag", objMember.PriceFlag);
                arrParam[33] = new SqlParameter("@TailFlag", objMember.TailFlag);
                arrParam[34] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[35] = new SqlParameter("@CreatedOn", objMember.CreatedOn);
                arrParam[36] = new SqlParameter("@TripFlag", objMember.TripFlag);
                arrParam[37] = new SqlParameter("@GroupId", objMember.GroupId);
                arrParam[38] = new SqlParameter("@Mod_Flag", objMember.Mod_Flag);
                arrParam[39] = new SqlParameter("@Leg", objMember.Leg);
                arrParam[40] = new SqlParameter("@SplitType", objMember.SplitType);
                arrParam[41] = new SqlParameter("@NewStartAirport", objMember.NewStartAirport);
                arrParam[42] = new SqlParameter("@NewEndAirport", objMember.NewEndAirport);

                //pass connection string, storedprocedure name and parameter array
                object obj = SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_LivePricingCalcculation_INS", arrParam);
                return obj.ToString();
            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return "-1";
            }
            finally
            {
                myConnection.Close();
            }

        }

        public DataTable BindEmptyLegs_SP(string strManaeType, string strFromDate, string strToDate, string strPageINdex, string strPageSize)
        {
            SqlParameter[] Get_CostSetDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", false);
            Get_CostSetDetails[0].Value = strManaeType;
            Get_CostSetDetails[1].Value = strFromDate;
            Get_CostSetDetails[2].Value = strToDate;
            Get_CostSetDetails[3].Value = strPageINdex;
            Get_CostSetDetails[4].Value = strPageSize;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", Get_CostSetDetails).Tables[0];
        }

        public DataTable GET_LIVEDATA(string strManaeType, string strFromDate, string strToDate, string strTail, string strTrip, string strTriplag)
        {
            SqlParameter[] Get_CostSetDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", false);
            Get_CostSetDetails[0].Value = "DATA";
            Get_CostSetDetails[1].Value = strFromDate;
            Get_CostSetDetails[2].Value = strToDate;
            Get_CostSetDetails[5].Value = strTail;
            Get_CostSetDetails[6].Value = strTrip;
            Get_CostSetDetails[7].Value = strTriplag;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", Get_CostSetDetails).Tables[0];
        }

        public DataTable BindGroupData()
        {
            SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", false);
            GetLegDetails[0].Value = "S";
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", GetLegDetails).Tables[0];
        }

        public DataTable BindGroupChildData(string strGroupId)
        {
            SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", false);
            GetLegDetails[0].Value = "GC";
            GetLegDetails[5].Value = strGroupId;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", GetLegDetails).Tables[0];
        }

        public DataTable CountryList()
        {
            SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "Country_SEL", false);
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "Country_SEL", GetLegDetails).Tables[0];
        }

        public DataSet LivePricing_SEL(string strTailNo, string strTripNo, string strStartAirport, string strEndAirport, string strGroupId, string strFlyingDate)
        {
            SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "LivePricing_SEL", false);
            GetLegDetails[0].Value = strTailNo;
            GetLegDetails[1].Value = strTripNo;
            GetLegDetails[2].Value = strStartAirport;
            GetLegDetails[3].Value = strEndAirport;
            GetLegDetails[4].Value = strGroupId;
            GetLegDetails[5].Value = strFlyingDate;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "LivePricing_SEL", GetLegDetails);
        }

        public DataTable Get_FutureData()
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set flag='OLD'");
            string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
            string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", false);
            saveCategoryParam[0].Value = strStartDate;
            saveCategoryParam[1].Value = strEndDate;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", saveCategoryParam).Tables[0];
        }

        public string SplitDelete(string strGroupId)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_Delete_Split", false);
            SaveNewTicketParams[0].Value = strGroupId;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_Delete_Split", SaveNewTicketParams);
            return obj.ToString();
        }

        public string ManualDelete(string strGroupId, string strTailNo, string strTripNo, string strAvailableFrom, string strStartTime, string strStartAirport, string strEndAirport)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_Delete_Manual", false);
            SaveNewTicketParams[0].Value = strGroupId;
            SaveNewTicketParams[1].Value = strTailNo;
            SaveNewTicketParams[2].Value = strTripNo;
            SaveNewTicketParams[3].Value = strAvailableFrom;
            SaveNewTicketParams[4].Value = strStartTime;
            SaveNewTicketParams[5].Value = strStartAirport;
            SaveNewTicketParams[6].Value = strEndAirport;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_Delete_Manual", SaveNewTicketParams);
            return obj.ToString();
        }

        public string SaveDepartureGroup(string strManageType, string strRowId, string strGroupName, string strFlag, string strCreatedBy)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_INS]", false);
            saveCategoryParam[0].Value = strManageType;
            saveCategoryParam[1].Value = strRowId;
            saveCategoryParam[2].Value = strGroupName;
            saveCategoryParam[3].Value = strFlag;
            saveCategoryParam[4].Value = strCreatedBy;
            object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_INS]", saveCategoryParam).ToString();
            return obj.ToString();
        }


        public string DeleteAirportChild(string strGroupId)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_Child_INS", false);
            SaveNewTicketParams[0].Value = "D";
            SaveNewTicketParams[1].Value = strGroupId;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_AirportGroup_Child_INS", SaveNewTicketParams);
            return obj.ToString();
        }

        public string DeleteAirport(string strRowID)
        {
            SqlParameter[] SaveNewTicketParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_Child_INS", false);
            SaveNewTicketParams[0].Value = "MD";
            SaveNewTicketParams[1].Value = strRowID;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "SP_AirportGroup_Child_INS", SaveNewTicketParams);
            return obj.ToString();
        }

        public string SaveAirportChild(string strManageType, string strGroupId, string strModifiedFlag, string strAirportFor, string strSetupBy, string strAirport,
            string strCreatedBy, string strStateCode, string strCountryCode, string strAirportCode)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_Child_INS]", false);
            saveCategoryParam[0].Value = "I";
            saveCategoryParam[1].Value = strGroupId;
            saveCategoryParam[2].Value = strModifiedFlag;
            saveCategoryParam[3].Value = strAirportFor;
            saveCategoryParam[4].Value = strSetupBy;
            saveCategoryParam[5].Value = strAirport;
            saveCategoryParam[6].Value = strCreatedBy;
            saveCategoryParam[7].Value = strStateCode;
            saveCategoryParam[8].Value = strCountryCode;
            saveCategoryParam[9].Value = strAirportCode;
            object obj = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_Child_INS]", saveCategoryParam).ToString();
            return obj.ToString();
        }

        public DataTable GroupExist(string strGroupName)
        {
            SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", false);
            GetLegDetails[0].Value = "AE";
            GetLegDetails[2].Value = strGroupName;
            return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", GetLegDetails).Tables[0];
        }

        public string saveCategory(string[] arrCategory)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_V1]", false);
            // DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  OneWayTail_Setup.rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours, EndAirport, EndCity  FROM  OneWayTail_Setup Inner join TailPricing_Configuration on TailPricing_Configuration.TailNo=OneWayTail_Setup.TailNo ").Tables[0];

            saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
            saveCategoryParam[1].Value = arrCategory[1];//Rowid
            saveCategoryParam[2].Value = arrCategory[2];//TailId
            saveCategoryParam[3].Value = arrCategory[3];//
            saveCategoryParam[4].Value = arrCategory[4];//
            saveCategoryParam[5].Value = arrCategory[5];//
            saveCategoryParam[6].Value = arrCategory[6];//
            saveCategoryParam[7].Value = arrCategory[7];//
            saveCategoryParam[8].Value = arrCategory[8];//
            saveCategoryParam[9].Value = arrCategory[9];//
            saveCategoryParam[10].Value = arrCategory[10];//
            saveCategoryParam[11].Value = arrCategory[11];//
            saveCategoryParam[12].Value = arrCategory[12];//
            saveCategoryParam[13].Value = arrCategory[13];//
            saveCategoryParam[14].Value = arrCategory[14];//
            // saveCategoryParam[15].Value = arrCategory[15];//
            // saveCategoryParam[16].Value = arrCategory[16];//
            saveCategoryParam[19].Value = arrCategory[19];//

            saveCategoryParam[20].Value = arrCategory[20];//
            saveCategoryParam[21].Value = arrCategory[21];//
            saveCategoryParam[22].Value = arrCategory[22];//
            saveCategoryParam[23].Value = arrCategory[23];//
            saveCategoryParam[24].Value = arrCategory[24];//
            saveCategoryParam[25].Value = arrCategory[25];//
            saveCategoryParam[26].Value = arrCategory[26];//
            saveCategoryParam[27].Value = arrCategory[27];//
            saveCategoryParam[28].Value = arrCategory[28];//
            saveCategoryParam[29].Value = arrCategory[29];//
            saveCategoryParam[30].Value = arrCategory[30];//
            saveCategoryParam[31].Value = arrCategory[31];//
            saveCategoryParam[32].Value = arrCategory[32];//
            saveCategoryParam[33].Value = arrCategory[33];//
            saveCategoryParam[34].Value = arrCategory[34];//
            saveCategoryParam[35].Value = arrCategory[35];//
            saveCategoryParam[36].Value = arrCategory[36];//
            saveCategoryParam[37].Value = Convert.ToDateTime(arrCategory[37]);//
            saveCategoryParam[38].Value = arrCategory[38];//
            saveCategoryParam[39].Value = arrCategory[39];//

            saveCategoryParam[41].Value = arrCategory[41];//
            saveCategoryParam[42].Value = arrCategory[42];//
            saveCategoryParam[43].Value = arrCategory[43];//
            saveCategoryParam[44].Value = arrCategory[44];//
            saveCategoryParam[46].Value = arrCategory[48];//
            saveCategoryParam[47].Value = arrCategory[49];//
            saveCategoryParam[48].Value = arrCategory[50];
            saveCategoryParam[49].Value = arrCategory[51];//


            saveCategoryParam[50].Value = arrCategory[52];//
            saveCategoryParam[51].Value = arrCategory[53];//
            saveCategoryParam[52].Value = arrCategory[54];//



            object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_V1]", saveCategoryParam).ToString();
            return obj.ToString();
        }


        public string saveCategoryGroup(string[] arrCategory)
        {
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_GROUP]", false);
            saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
            saveCategoryParam[1].Value = arrCategory[1];//Rowid
            saveCategoryParam[2].Value = arrCategory[2];//TailId
            saveCategoryParam[3].Value = arrCategory[3];//
            saveCategoryParam[4].Value = arrCategory[4];//
            saveCategoryParam[5].Value = arrCategory[5];//
            saveCategoryParam[6].Value = arrCategory[6];//
            saveCategoryParam[7].Value = arrCategory[7];//
            saveCategoryParam[8].Value = arrCategory[8];//
            saveCategoryParam[9].Value = arrCategory[9];//
            saveCategoryParam[10].Value = arrCategory[10];//
            saveCategoryParam[11].Value = arrCategory[11];//
            saveCategoryParam[12].Value = arrCategory[12];//
            saveCategoryParam[13].Value = arrCategory[13];//
            saveCategoryParam[14].Value = arrCategory[14];//
            // saveCategoryParam[15].Value = arrCategory[15];//
            // saveCategoryParam[16].Value = arrCategory[16];//
            saveCategoryParam[19].Value = arrCategory[19];//

            saveCategoryParam[20].Value = arrCategory[20];//
            saveCategoryParam[21].Value = arrCategory[21];//
            saveCategoryParam[22].Value = arrCategory[22];//
            saveCategoryParam[23].Value = arrCategory[23];//
            saveCategoryParam[24].Value = arrCategory[24];//
            saveCategoryParam[25].Value = arrCategory[25];//
            saveCategoryParam[26].Value = arrCategory[26];//
            saveCategoryParam[27].Value = arrCategory[27];//
            saveCategoryParam[28].Value = arrCategory[28];//
            saveCategoryParam[29].Value = arrCategory[29];//
            saveCategoryParam[30].Value = arrCategory[30];//
            saveCategoryParam[31].Value = arrCategory[31];//
            saveCategoryParam[32].Value = arrCategory[32];//
            saveCategoryParam[33].Value = arrCategory[33];//
            saveCategoryParam[34].Value = arrCategory[34];//
            saveCategoryParam[35].Value = arrCategory[35];//
            saveCategoryParam[36].Value = arrCategory[36];//
            saveCategoryParam[37].Value = Convert.ToDateTime(arrCategory[37]);//
            saveCategoryParam[38].Value = arrCategory[38];//
            saveCategoryParam[39].Value = arrCategory[39];//

            saveCategoryParam[41].Value = arrCategory[41];//
            saveCategoryParam[42].Value = arrCategory[42];//
            saveCategoryParam[43].Value = arrCategory[43];//
            saveCategoryParam[44].Value = arrCategory[44];//
            saveCategoryParam[45].Value = arrCategory[45];//
            saveCategoryParam[46].Value = arrCategory[46];//
            saveCategoryParam[47].Value = arrCategory[47];//
            saveCategoryParam[48].Value = arrCategory[48];//


            object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_GROUP]", saveCategoryParam).ToString();
            return obj.ToString();
        }

       


    }


}