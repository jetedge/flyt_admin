﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using AbstractLayer;

/// <summary>
/// Summary description for EmailSetupDAL
/// </summary>

namespace DataLayer
{
    public class EmailSetupDAL : EmailSetupInterface
    {
        // All the implementation of signatures goes here 
        string m_Connection_String = SqlHelper.FLYTConnectionString;

        public EmailSetupDAL(string _ConnectionString)
        {
            m_Connection_String = _ConnectionString;
        }
        public int Save(AbstractLayer.EmailSetupBase objMember)
        {
            SqlTransaction objTrans = null;
            SqlConnection myConnection = new SqlConnection(m_Connection_String);
            try
            {
                // Insert Member Personal details only
                myConnection.Open();
                objTrans = myConnection.BeginTransaction();
                SqlParameter[] arrParam = new SqlParameter[25];

                arrParam[0] = new SqlParameter("@MANAGETYPE", "I");
                arrParam[1] = new SqlParameter("@EmailId", "0");
                arrParam[2] = new SqlParameter("@UserId", "0");
                arrParam[3] = new SqlParameter("@alertconfim", objMember.ChkTrip);
                arrParam[4] = new SqlParameter("@Exception", objMember.ChkException);
                arrParam[5] = new SqlParameter("@Preference", objMember.ChkPreference);
                arrParam[6] = new SqlParameter("@MustMoves", objMember.ChkMustMove);
                arrParam[7] = new SqlParameter("@SignUp", objMember.ChkSignUp);
                arrParam[8] = new SqlParameter("@CreatedBy", objMember.CreatedBy);
                arrParam[9] = new SqlParameter("@Email", objMember.Email);
                arrParam[10] = new SqlParameter("@Firstname", objMember.FirstName);
                arrParam[11] = new SqlParameter("@LastName", objMember.LastName);
                //pass connection string, storedprocedure name and parameter array
                SqlHelper.ExecuteNonQuery(m_Connection_String, CommandType.StoredProcedure, "SP_EmailSetup_INS", arrParam);

            }
            catch (Exception Ex)
            {
                objTrans.Rollback();
                string sError = Ex.Message.ToString();
                return -1;
            }
            finally
            {
                myConnection.Close();
            }
            return 1;
        }

        public DataTable Select()
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[EmailSetup_SEL]", false);
            getAllTripParm[0].Value = "S";
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[EmailSetup_SEL]", getAllTripParm).Tables[0];
        }

        public DataTable DuplicateUserMasterEmail(string strEmail)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_EmailSetup_INS]", false);
            getAllTripParm[0].Value = "UE";
            getAllTripParm[6].Value = strEmail;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_EmailSetup_INS]", getAllTripParm).Tables[0];
        }

        public DataTable DuplicateEmailSetupEmail(string strEmail)
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(m_Connection_String, "[SP_EmailSetup_INS]", false);
            getAllTripParm[0].Value = "EE";
            getAllTripParm[6].Value = strEmail;
            return SqlHelper.ExecuteDataset(m_Connection_String, CommandType.StoredProcedure, "[SP_EmailSetup_INS]", getAllTripParm).Tables[0];
        }

    }
}