﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Data.Common;
using System.Configuration;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
/// <summary>
/// Summary description for ManagementClass
/// </summary>
public class AirportClass
{
    public static void AirportSplit(string strAirportText, out string ICAO, out string StateCode, out string CountryCode,
        out string City, out string IATA)
    {
        ICAO = string.Empty;
        StateCode = string.Empty;
        CountryCode = string.Empty;
        City = string.Empty;
        IATA = string.Empty;

        string[] strAirport = strAirportText.Split('(');

        if (strAirport.Length > 1)
        {
            //ICAO Code
            if (strAirport[strAirport.Length - 1].Trim().Contains("/")) //Contains / if Airport is Hawaii
            {
                string[] str = strAirport[strAirport.Length - 1].Trim().Split('/');
                ICAO = str[1].Trim().Replace(")", "");
                IATA = str[0].Trim();
            }
            else
            {
                ICAO = strAirport[strAirport.Length - 1].Trim().Replace(")", ""); ;
            }

            //City

            string[] arrCity = strAirport[0].Split(',');

            for (int Ci = 0; Ci < arrCity.Length - 1; Ci++)
            {
                City = City + arrCity[Ci].TrimEnd() + ",";
            }
            City = City.TrimEnd(',');

            //State Code
            StateCode = arrCity[arrCity.Length - 1].Trim();
        }
    }

}