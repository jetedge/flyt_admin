﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DataLayer;

/// <summary>
/// Summary description for SecretsBLL
/// </summary>
public class SecretsBLL
{
    //Office 365
    public static string SenderEmailid = GlobalDAL.Get_JeSecrets().Tables[0].Rows[0]["SenderEmailid"].ToString();
    public static string Password = GlobalDAL.Get_JeSecrets().Tables[0].Rows[0]["BaseAuth"].ToString();
    public static string Office365WebserivceURL = GlobalDAL.Get_JeSecrets().Tables[0].Rows[0]["BaseAuthURL"].ToString();

    //Authorize .Net - Sandbox
    public static string authorizeApiLoginID = GlobalDAL.Get_JeSecrets().Tables[1].Rows[0]["BaseAuthId"].ToString();
    public static string authorizeApiTransactionKey = GlobalDAL.Get_JeSecrets().Tables[1].Rows[0]["BaseAuthKey"].ToString();

    //Authorize .Net - Production
    public static string authorizeApiLoginIDProd = GlobalDAL.Get_JeSecrets().Tables[2].Rows[0]["BaseAuthId"].ToString();
    public static string authorizeApiTransactionKeyProd = GlobalDAL.Get_JeSecrets().Tables[2].Rows[0]["BaseAuthId"].ToString();

    //Twilio SMS
    public static string SMSaccountSid = GlobalDAL.Get_JeSecrets().Tables[3].Rows[0]["BaseAuthId"].ToString();
    public static string SMSauthToken = GlobalDAL.Get_JeSecrets().Tables[3].Rows[0]["BaseAuthToken"].ToString();
    public static string SMSFromNumber = GlobalDAL.Get_JeSecrets().Tables[3].Rows[0]["FromNumber"].ToString();
    public static string SMSAPIURL = GlobalDAL.Get_JeSecrets().Tables[3].Rows[0]["BaseAuthURL"].ToString();

    //Zendesk Development Account
    public static string zendeskDomain = GlobalDAL.Get_JeSecrets().Tables[4].Rows[0]["BaseAuthDomain"].ToString();
    public static string zendeskLoginEmail = GlobalDAL.Get_JeSecrets().Tables[4].Rows[0]["BaseAuthEmail"].ToString();
    public static string zendeskApiKey = GlobalDAL.Get_JeSecrets().Tables[4].Rows[0]["BaseAuth"].ToString();

    //DocuSign Development Account
    public static string accountId = GlobalDAL.Get_JeSecrets().Tables[5].Rows[0]["BaseAuthAccount"].ToString();
    public static string basePath = GlobalDAL.Get_JeSecrets().Tables[5].Rows[0]["BaseAuthPath"].ToString();
    public static string APIAccountID = GlobalDAL.Get_JeSecrets().Tables[5].Rows[0]["BaseAuth"].ToString();
    public static string returnUrl = GlobalDAL.Get_JeSecrets().Tables[5].Rows[0]["BaseAuthURL"].ToString();

    //Rapid API
    public static string RapidAPIKey = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["BaseAuth"].ToString();

    //General Keys
    public static string TailAttacPath = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["TailAttacPath"].ToString();
    public static string PDFWRITE = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["PDFWRITE"].ToString();

    public static string LandingGallery = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["LandingGallery"].ToString();
    public static string LandingImageURL = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["LandingImageURL"].ToString();

    public static string UserPhoto = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["UserPhoto"].ToString();
    public static string UserPhotoURL = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["UserPhotoURL"].ToString();

    public static string TailGallery = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["TailGallery"].ToString();
    public static string TailImageURL = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["TailImageURL"].ToString();

    public static string DocuSignUrl = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["DocuSignUrl"].ToString();
    public static string AdminHomePage = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["AdminHomePage"].ToString();

    public static string MailLoginURL = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["MailLoginURL"].ToString();
    public static string MailLogo = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["MailLogo"].ToString();
   
    public static string TCUrl = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["TCUrl"].ToString();
    public static string InvoiceNewPath = GlobalDAL.Get_JeSecrets().Tables[6].Rows[0]["InvoiceNewPath"].ToString();
}