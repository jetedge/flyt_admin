﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net;
using System.Web;
using System.Data;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
using System.Xml;
using System.Net;
using System.Configuration;
//using WUP_FlightRequest.Models;
using System.Web;
using System.Globalization;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using System.Net.Mail;



public class PDFWrite
{
    //  GeneratePDFDAL objGenPDF = new GeneratePDFDAL();
    // RequestFlightDAL_V3 objRequestFlightDAL_V3 = new RequestFlightDAL_V3();
    public void CreatePDF(DataSet dtPS, string strUserName, DataTable dtUserTable, out string strResponse)
    {
        string fileUrl = string.Empty;
        string fileNameQBD = string.Empty;
        string fileNameCHTR = string.Empty;

        string fileNameFQBD = string.Empty;
        string fileNameFCHTR = string.Empty;

        
        String[] strlist = strUserName.Split(new[] { '@' });
        

        fileUrl = @"D\jetedgefiles\EMPTYLEG\" + strUserName + @"\";
       

        if (!Directory.Exists(fileUrl))
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            Directory.CreateDirectory(fileUrl);
        }
        string sbSection = string.Empty;

        //DataSet dtPS = objGenPDF.BindGrid(strReqID, "S");

        //DataSet dtPF = objGenPDF.BindGrid(strReqID, "F");
        // DataSet dtPS =null;

        // DataSet dtPF =null;

        // sbSection = FloatFleet(dtP);

        fileNameQBD = "JE_QBD_SAL_" + strUserName + "_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf";

        //  fileNameCHTR = "JE_CHTR_SAL_" + strReqID + "_" + dtPS.Tables[1].Rows[0]["Requester"].ToString().Substring(0, 4) + "_" + System.DateTime.Now.ToString("yyyyMMdd") + ".pdf";




        //fileNameFQBD = "JE_QBD_FIN_" + strReqID + "_" + dtPS.Tables[1].Rows[0]["Requester"].ToString().Substring(0, 4) + "_" + System.DateTime.Now.ToString("yyyyMMdd") + ".pdf";

        //fileNameFCHTR = "JE_CHTR_FIN_" + strReqID + "_" + dtPS.Tables[1].Rows[0]["Requester"].ToString().Substring(0, 4) + "_" + System.DateTime.Now.ToString("yyyyMMdd") + ".pdf";



        Save_PDFCHTR(sbSection, fileUrl, fileNameQBD, strUserName, dtPS, dtUserTable);
        strResponse = "S";
        //Save_PDFCHTR(sbSection, fileUrl, fileNameCHTR, strReqID, strUserID, strUserName, dtPS);

        //Save_PDFQBD(sbSection, fileUrl, fileNameFQBD, strReqID, strUserID, strUserName, dtPF);
        //Save_PDFCHTR(sbSection, fileUrl, fileNameFCHTR, strReqID, strUserID, strUserName, dtPF);



        //objRequestFlightDAL_V3.SavePDFAttachment(strReqID, "CHTR Aggrement", fileNameCHTR, fileUrl, "", strUserID, strUserName, fileNameFCHTR);
        // objRequestFlightDAL_V3.SavePDFAttachment(strReqID, "Quote Breakdown", fileNameQBD, fileUrl, "", strUserID, strUserName, fileNameFQBD);
    }
    private void Save_PDFCHTR(string str, string strPath, string fileName, string strUserName, DataSet dtP, DataTable dtUserTable)
    {

        string strEmail = dtUserTable.Rows[0]["Email"].ToString();
        string strlogpath = HttpContext.Current.Server.MapPath("~/Images/JE_LOGO_NEW.png");
        string strFilePath1 = HttpContext.Current.Server.MapPath("~/Images/Firstpage1.png");
        string strFilePath2 = HttpContext.Current.Server.MapPath("~/Images/FirstPage2.png");
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 10f);
        iTextSharp.text.Image myImage = iTextSharp.text.Image.GetInstance(strlogpath);
        iTextSharp.text.Image myImage1 = iTextSharp.text.Image.GetInstance(strFilePath1);
        iTextSharp.text.Image myImage2 = iTextSharp.text.Image.GetInstance(strFilePath2);
        long Height = Convert.ToInt64(PageSize.A4.Height.ToString());
        myImage.ScaleAbsolute(100f, 90f);
        myImage1.ScaleAbsolute(150f, 60f);
        myImage2.ScaleAbsolute(120f, 60f);


        using (MemoryStream memoryStream = new MemoryStream())
        {

            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
            pdfDoc.Open();
            Font ComHeader = FontFactory.GetFont("Arial", 11, Font.BOLD);
            Font verdana2 = FontFactory.GetFont("Arial", 12, Font.NORMAL);
            Font ArialBoldLeft = FontFactory.GetFont("Arial", 8, Font.BOLD);
            Font ArialBoldRight = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            Font GridHeader = FontFactory.GetFont("Arial", 7, Font.BOLD);
            Font verdana5 = FontFactory.GetFont("Arial", 7, Font.NORMAL);
            Font verdana6 = FontFactory.GetFont("Arial", 9, Font.NORMAL);

            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            StringReader sr = new StringReader(str.ToString());
            htmlparser.Parse(sr);


            float[] widths = new float[] { 1f, 2f };

            PdfPTable ParentTable = new PdfPTable(1);
            ParentTable.WidthPercentage = 100;
            PdfPCell cell08 = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 12, Font.BOLD)));
            ParentTable.DefaultCell.BorderWidth = 1;

            #region Sub table 1

            PdfPTable SubTable1 = new PdfPTable(8);
            SubTable1.WidthPercentage = 100;
            float[] widthss = new float[] { 10f, 10f, 10f, 20f, 20f, 10f, 10f, 10f };
            SubTable1.SetWidths(widthss);

            PdfPCell cell2 = new PdfPCell(SubTable1);

            #region Header

            PdfPCell cell = new PdfPCell(myImage);
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingLeft = 5;
            cell.Rowspan = 6;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 4;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            //////

            cell = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Colspan = 4;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);
            ////


            cell = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Colspan = 4;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Colspan = 4;
            cell.PaddingBottom = 5;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);
            ////

            cell = new PdfPCell(new Phrase("AIRCRAFT INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cell.Colspan = 4;
            cell.PaddingBottom = 10;
            cell.BorderWidth = 0;
            cell.BorderColorLeft = BaseColor.WHITE;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.BorderColorTop = BaseColor.WHITE;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);
            ////

            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 10;
            cell.Border = 0;
            cell.MinimumHeight = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 10;
            cell.Border = 2;
            cell.BorderWidthLeft = 0.5f;
            cell.BorderWidthRight = 0.5f;
            cell.BorderWidthTop = 0.5f;
            cell.BorderWidthBottom = 0.5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 10;
            cell.Border = 0;
            cell.MinimumHeight = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion


            #endregion



            #region First Row

            cell = new PdfPCell(new Phrase("Booked For          :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase((dtUserTable.Rows[0]["FirstName"].ToString() + dtUserTable.Rows[0]["LastName"].ToString()), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("     Invoice Number          :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("#JEQ123", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #region Second Row

            cell = new PdfPCell(new Phrase("Contact                :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtUserTable.Rows[0]["PhoneNumber"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("Booked Date               :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #region Third Row

            cell = new PdfPCell(new Phrase("Address               :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtUserTable.Rows[0]["Address"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);



            #endregion

            #region Grid Header

            cell = new PdfPCell(new Phrase("Aircraft Name", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            //cell.Width = 10.00F;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.PaddingTop = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("Departure Date", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            //cell.Width = 10.00F;
            cell.PaddingTop = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;

            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("Departure Time", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            // cell.Width = 10.00F;
            cell.PaddingTop = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("DEPARTURE CITY", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            // cell.Width = 10.00F;
            cell.PaddingTop = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("ARRIVAL CITY", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            //   cell.Width = 10.00F;
            cell.PaddingTop = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("Arrival Time", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            //  cell.Width = 10.00F;
            cell.PaddingTop = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("No of Seats", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            //  cell.Width = 10.00F;
            cell.PaddingTop = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("Minimum Price", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            cell.BackgroundColor = BaseColor.BLACK;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.PaddingTop = 2;
            //  cell.Width = 10.00F;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("PAX", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("NAUT MILES", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("ETE", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("TZC", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            #endregion

            #region Grid Values

            for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            {
                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["TailName"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderColorRight = BaseColor.WHITE;
                cell.Border = 0;
                SubTable1.AddCell(cell);


                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["DDate"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderColorRight = BaseColor.WHITE;
                cell.Border = 0;
                SubTable1.AddCell(cell);

                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["DTime"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.BorderColorRight = BaseColor.WHITE;
                cell.Border = 0;
                SubTable1.AddCell(cell);


                //cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["EndTime"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.BorderColorRight = BaseColor.WHITE;
                //cell.Border = 0;
                //SubTable1.AddCell(cell);

                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["StartCity"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderColorRight = BaseColor.WHITE;
                cell.Border = 0;
                SubTable1.AddCell(cell);


                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["EndCity"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.BorderColorRight = BaseColor.WHITE;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.Border = 0;
                SubTable1.AddCell(cell);


                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["ATime"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.BorderColorRight = BaseColor.WHITE;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Border = 0;
                SubTable1.AddCell(cell);

                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["Noofseats"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.BorderColorRight = BaseColor.WHITE;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.Border = 0;
                SubTable1.AddCell(cell);

                cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["Pricing"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
                cell.BorderColorRight = BaseColor.WHITE;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell.Border = 0;
                SubTable1.AddCell(cell);




            }


            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 5;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 1;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 1;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion



            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 7;
            //cell.Border = 0;
            //cell.MinimumHeight = 5;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[0].Compute("SUM(Pricing)", "").ToString()).ToString(), ArialBoldRight));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(Convert.ToDateTime(dtP.Tables[0].Compute("SUM(flight_time_Hrs)", "").ToString()).ToString("HH:mm"), ArialBoldRight));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.MinimumHeight = 5;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 1;
            //cell.MinimumHeight = 5;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);



            #endregion

            #region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 10;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            #endregion


            #region

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("AMOUNT", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 8;
            //cell.PaddingTop = 7;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingTop = 7;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 5;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 8;
            cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion


            //for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            //{
            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["TailName"].ToString(), FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //    cell.Border = 0;
            //    cell.Colspan = 6;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[0].Rows[i]["Pricing"].ToString()).ToString("C", new CultureInfo("en-US")), ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}

            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;

            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 2;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            //for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            //{
            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["TailName"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //    cell.Border = 0;
            //    cell.Colspan = 6;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[0].Rows[i]["Pricing"].ToString()).ToString("C", new CultureInfo("en-US")), ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}

            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;

            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            cell = new PdfPCell(new Phrase("", ArialBoldRight));
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("SUBTOTAL", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[0].Compute("SUM(Pricing)", "").ToString()).ToString("C", new CultureInfo("en-US")), ArialBoldRight));
            //cell = new PdfPCell(new Phrase(Convert.ToDecimal("20").ToString("C", new CultureInfo("en-US")), ArialBoldRight));
            cell.Border = 0;
            cell.Colspan = 1;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("Segment Tax", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 6;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("0.00", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("FET", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 6;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("0.00", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;

            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion


            cell = new PdfPCell(new Phrase("", ArialBoldRight));
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingBottom = 10;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("GRAND TOTAL", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 10;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[0].Compute("SUM(Pricing)", "").ToString()).ToString("C", new CultureInfo("en-US")), FontFactory.GetFont("Arial", 10, Font.BOLD)));
            // cell = new PdfPCell(new Phrase(20.00).ToString("C", new CultureInfo("en-US")), FontFactory.GetFont("Arial", 10, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 1;
            cell.PaddingBottom = 10;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            //if (dtP.Tables[0].Rows.Count > 0)
            //{
            //    if (dtP.Tables[1].Rows[0]["ExemptFlag"].ToString() == "N")
            //    {
            //      //  cell = new PdfPCell(new Phrase("This quote is exclusive of any applicable taxes. " + dtP.Tables[1].Rows[0]["Requester"].ToString() + " is responsible and hereby agrees to collect the FET, segment and other applicable taxes from the client and remit to the IRS.______ Initial", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //          cell = new PdfPCell(new Phrase("This quote is exclusive of any applicable taxes. " + "vignesh" + " is responsible and hereby agrees to collect the FET, segment and other applicable taxes from the client and remit to the IRS.______ Initial", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //        cell.Border = 0;
            //        cell.Colspan = 10;
            //        cell.PaddingBottom = 5;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.PaddingLeft = 12;
            //        cell.PaddingRight = 12;
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //    else
            //    {
            //        cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //        cell.Border = 0;
            //        cell.Colspan = 10;
            //        cell.PaddingBottom = 10;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.PaddingLeft = 12;
            //        cell.PaddingRight = 12;
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}
            //else
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //    cell.Border = 0;
            //    cell.Colspan = 10;
            //    cell.PaddingBottom = 10;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.PaddingLeft = 12;
            //    cell.PaddingRight = 12;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}


            cell = new PdfPCell(new Phrase("", ArialBoldRight));
            cell.Border = 0;
            cell.MinimumHeight = 8;
            cell.Colspan = 8;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #region Empty Row
            #region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 10;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);




            cell = new PdfPCell(new Phrase("", ArialBoldRight));
            cell.Border = 0;
            cell.MinimumHeight = 8;
            cell.Colspan = 10;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);



            #endregion

            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);
            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion
            #region Empty Row


            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 1;
            cell.Border = 0;
            cell.Rowspan = 45;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 7;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #endregion

            //cell = new PdfPCell(new Phrase("COMMENT:", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Colspan = 10;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //int height = 0;

            //if (dtP.Tables[0].Rows.Count == 6)
            //{
            //    height = 60;
            //}
            //if (dtP.Tables[0].Rows.Count == 5)
            //{
            //    height = 70;
            //}
            //if (dtP.Tables[0].Rows.Count == 4)
            //{
            //    height = 85;
            //}
            //if (dtP.Tables[0].Rows.Count == 3)
            //{
            //    height = 100;
            //}
            //if (dtP.Tables[0].Rows.Count == 2)
            //{
            //    height = 120;
            //}
            //if (dtP.Tables[0].Rows.Count == 1)
            //{
            //    height = 150;
            //}

            //if (dtP.Tables[0].Rows.Count > 0 && dtP.Tables[0].Rows.Count == 0)
            //{
            //    for (int i = 0; i < dtP.Tables[4].Rows.Count; i++)
            //    {
            //        //  cell = new PdfPCell(new Phrase("*" + dtP.Tables[4].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        //cell = new PdfPCell(new Phrase("*" + "Ticket", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}
            //else if (dtP.Tables[0].Rows.Count == 0 && dtP.Tables[0].Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            //    {
            //        // cell = new PdfPCell(new Phrase("*" + dtP.Tables[5].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        //cell = new PdfPCell(new Phrase("*" + "Ticket", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}

            //else if (dtP.Tables[0].Rows.Count > 0 && dtP.Tables[0].Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            //    {
            //        //  cell = new PdfPCell(new Phrase("*" + dtP.Tables[4].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //       // cell = new PdfPCell(new Phrase("*" + "Ticket", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }

            //    for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            //    {
            //        // cell = new PdfPCell(new Phrase("**" + dtP.Tables[5].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //       // cell = new PdfPCell(new Phrase("**" + "Ticket", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}

            //else if (dtP.Tables[0].Rows.Count == 0 && dtP.Tables[0].Rows.Count == 0)
            //{
            //    for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.MinimumHeight = height;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}



            //if (dtP.Tables[0].Rows.Count == 0 && dtP.Tables[0].Rows.Count == 1)
            //{

            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 145;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //}
            //else
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 130;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}





            //if (dtP.Tables[4].Rows.Count == 3)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 8;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[4].Rows.Count == 2)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 40;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[4].Rows.Count == 1 && dtP.Tables[0].Rows.Count == 2)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 80;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[4].Rows.Count == 1 && dtP.Tables[0].Rows.Count == 4)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 67;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}

            //if (dtP.Tables[5].Rows.Count == 3)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 8;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[5].Rows.Count == 2)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 40;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[5].Rows.Count == 1 && dtP.Tables[0].Rows.Count == 2)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 80;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[5].Rows.Count == 1 && dtP.Tables[0].Rows.Count == 4)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 67;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}

            //if (dtP.Tables[5].Rows.Count == 1 && dtP.Tables[4].Rows.Count == 1 && dtP.Tables[0].Rows.Count == 4)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 70;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[5].Rows.Count == 1 && dtP.Tables[4].Rows.Count == 0 && dtP.Tables[0].Rows.Count == 4)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 80;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}
            //if (dtP.Tables[5].Rows.Count == 1 && dtP.Tables[4].Rows.Count == 1)
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.Colspan = 10;
            //    cell.Border = 0;
            //    cell.MinimumHeight = 60;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}


            cell = new PdfPCell(myImage1);
            cell.Colspan = 3;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("THANK YOU FOR THE OPPORTUNITY TO BOOK YOUR TRIP", FontFactory.GetFont("Arial", 7, Font.NORMAL)));
            cell.Colspan = 3;
            cell.PaddingTop = 30;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(myImage2);
            cell.Colspan = 2;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);



            cell = new PdfPCell(new Phrase("", ArialBoldRight));
            cell.Border = 0;
            cell.Colspan = 8;
            cell.MinimumHeight = 8;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 8;
            cell.Border = 2;
            cell.BorderWidthLeft = 0.5f;
            cell.BorderWidthRight = 0.5f;
            cell.BorderWidthTop = 0.5f;
            cell.BorderWidthBottom = 0.5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Arial", 8, Font.BOLD)));
            cell.Colspan = 8;
            cell.PaddingTop = 5;
            cell.PaddingBottom = 5;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            #endregion

            #endregion

            PdfPTable FooterTable1 = new PdfPTable(10);
            FooterTable1.WidthPercentage = 100;
            PdfPCell CellF = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF.Colspan = 5;
            CellF.Border = 0;
            CellF.PaddingTop = 5;
            CellF.PaddingBottom = 5;
            CellF.HorizontalAlignment = Element.ALIGN_LEFT;
            FooterTable1.AddCell(CellF);

            CellF = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF.Colspan = 5;
            CellF.Border = 0;
            CellF.PaddingTop = 5;
            CellF.PaddingBottom = 5;
            CellF.HorizontalAlignment = Element.ALIGN_RIGHT;
            FooterTable1.AddCell(CellF);

            //MainTable.AddCell(SubTable1);

            ParentTable.AddCell(SubTable1);

            pdfDoc.Add(ParentTable);

            pdfDoc.Add(FooterTable1);

            pdfDoc.NewPage();

            PdfPTable ParentTable1 = new PdfPTable(1);
            ParentTable1.WidthPercentage = 100;
            ParentTable1.DefaultCell.BorderWidth = 1;

            #region Sub table 2

            PdfPTable SubTable2 = new PdfPTable(10);
            SubTable2.WidthPercentage = 100;
            PdfPCell cellP2 = new PdfPCell(SubTable2);

            #region Header

            PdfPCell cellP3 = new PdfPCell(myImage);
            cellP3.Border = 0;
            cellP3.Colspan = 2;
            cellP3.Rowspan = 5;
            cellP3.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP3);

            cellP3 = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cellP3.Border = 0;
            cellP3.Colspan = 6;
            cellP3.PaddingBottom = 8;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);


            cellP3 = new PdfPCell(new Phrase("", ComHeader));
            cellP3.Border = 0;
            cellP3.Colspan = 2;
            cellP3.PaddingBottom = 8;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);

            //////

            cellP3 = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP3.Colspan = 6;
            cellP3.Border = 0;
            cellP3.PaddingBottom = 5;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);

            cellP3 = new PdfPCell(new Phrase("", ComHeader));
            cellP3.Border = 0;
            cellP3.Colspan = 2;
            cellP3.PaddingBottom = 5;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);
            ////


            cellP3 = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP3.Colspan = 6;
            cellP3.Border = 0;
            cellP3.PaddingBottom = 5;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);

            cellP3 = new PdfPCell(new Phrase("", ComHeader));
            cellP3.Border = 0;
            cellP3.Colspan = 2;
            cellP3.PaddingBottom = 5;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);

            cellP3 = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP3.Colspan = 6;
            cellP3.PaddingBottom = 5;
            cellP3.Border = 0;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);

            cellP3 = new PdfPCell(new Phrase("", ComHeader));
            cellP3.Border = 0;
            cellP3.Colspan = 2;
            cellP3.PaddingBottom = 5;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);
            ////

            cellP3 = new PdfPCell(new Phrase("AIRCRAFT INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellP3.Colspan = 6;
            cellP3.PaddingBottom = 10;
            cellP3.BorderWidth = 0;
            cellP3.BorderColorLeft = BaseColor.WHITE;
            cellP3.BorderColorRight = BaseColor.WHITE;
            cellP3.BorderColorTop = BaseColor.WHITE;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);


            cellP3 = new PdfPCell(new Phrase("", ComHeader));
            cellP3.Border = 0;
            cellP3.Colspan = 2;
            cellP3.PaddingBottom = 10;
            cellP3.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP3);
            ////

            #endregion


            #region Empty Row

            cellP3 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP3.Colspan = 10;
            cellP3.Border = 2;
            cellP3.BorderWidthLeft = 0.5f;
            cellP3.BorderWidthRight = 0.5f;
            cellP3.BorderWidthTop = 0.5f;
            cellP3.BorderWidthBottom = 0.5f;
            cellP3.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP3);

            #endregion


            #region Empty Row

            cellP3 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP3.Colspan = 10;
            cellP3.Border = 0;
            cellP3.MinimumHeight = 5;
            cellP3.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP3);

            #endregion



            #region First Row

            cellP2 = new PdfPCell(new Phrase("Booked For :", ArialBoldLeft));
            cellP2.Border = 0;
            cellP2.PaddingBottom = 5;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["FirstName"].ToString() + " " + dtUserTable.Rows[0]["Lastname"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.PaddingBottom = 5;
            cellP2.Colspan = 5;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);


            cellP2 = new PdfPCell(new Phrase("     Invoice Number          :", ArialBoldLeft));
            cellP2.Border = 0;
            cellP2.PaddingBottom = 5;
            cellP2.Colspan = 2;
            cellP2.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase("#JEQ123", ArialBoldRight));
            //cellP2 = new PdfPCell(new Phrase("JHSS", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.PaddingBottom = 5;
            cellP2.Colspan = 2;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            #endregion

            #region Second Row

            cellP2 = new PdfPCell(new Phrase("Contact :", ArialBoldLeft));
            cellP2.Border = 0;
            cellP2.PaddingBottom = 5;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["Phonenumber"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("vignesh", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.PaddingBottom = 5;
            cellP2.Colspan = 5;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);


            cellP2 = new PdfPCell(new Phrase("Booked Date               :", ArialBoldLeft));
            cellP2.Border = 0;
            cellP2.PaddingBottom = 5;
            cellP2.Colspan = 2;
            cellP2.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), ArialBoldRight));
            //  cellP2 = new PdfPCell(new Phrase("24/01/2000", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 2;
            cellP2.PaddingBottom = 5;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            #endregion

            #region Second PAge Content

            cellP2 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.MinimumHeight = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase("TERMS & CONDITIONS", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.MinimumHeight = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);




            cellP2 = new PdfPCell(new Phrase("DEFINITIONS:", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            //  DataTable dtPDFMAster = genclass.GetDataTableWheelsUp("SELECT * FROM PDF_Master");
            DataTable dtPDFMAster = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM PDF_Master").Tables[0];
            //  DataTable dtPDFMAster = null;



            cellP2 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["Definitions"].ToString(), ArialBoldRight));
            cellP2.Border = 0;
            cellP2.PaddingLeft = 8;
            cellP2.PaddingRight = 4;
            cellP2.Colspan = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.MinimumHeight = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);


            cellP2 = new PdfPCell(new Phrase("GENERAL:", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);


            cellP2 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["GENERAL"].ToString(), ArialBoldRight));
            cellP2.Border = 0;
            cellP2.PaddingLeft = 8;
            cellP2.PaddingRight = 4;
            cellP2.SetLeading(0.0f, 1.0f);
            cellP2.Colspan = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.MinimumHeight = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);


            cellP2 = new PdfPCell(new Phrase("CONDUCT:", ArialBoldRight));
            cellP2.Border = 0;
            cellP2.Colspan = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);


            cellP2 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["Conduct"].ToString(), ArialBoldRight));
            cellP2.Border = 0;
            cellP2.PaddingLeft = 4;
            cellP2.PaddingRight = 4;
            cellP2.MinimumHeight = 90;
            cellP2.SetLeading(0.0f, 1.5f);
            cellP2.Colspan = 10;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP2.Colspan = 10;
            cellP2.Border = 0;
            cellP2.MinimumHeight = 8;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);




            cellP2 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP2.Colspan = 10;
            cellP2.Border = 2;
            cellP2.BorderWidthLeft = 0.5f;
            cellP2.BorderWidthRight = 0.5f;
            cellP2.BorderWidthTop = 0.5f;
            cellP2.BorderWidthBottom = 0.5f;
            cellP2.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable2.AddCell(cellP2);

            cellP2 = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Italic", 8, Font.BOLD)));
            cellP2.Colspan = 10;
            cellP2.Border = 0;
            cellP2.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable2.AddCell(cellP2);

            #endregion

            #endregion

            PdfPTable FooterTable2 = new PdfPTable(10);
            FooterTable2.WidthPercentage = 100;
            PdfPCell CellF2 = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF2.Colspan = 5;
            CellF2.Border = 0;
            CellF2.PaddingTop = 5;
            CellF2.PaddingBottom = 5;
            CellF2.HorizontalAlignment = Element.ALIGN_LEFT;
            FooterTable2.AddCell(CellF2);

            CellF2 = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF2.Colspan = 5;
            CellF2.Border = 0;
            CellF2.PaddingTop = 5;
            CellF2.PaddingBottom = 5;
            CellF2.HorizontalAlignment = Element.ALIGN_RIGHT;
            FooterTable2.AddCell(CellF2);

            ParentTable1.AddCell(SubTable2);

            pdfDoc.Add(ParentTable1);
            pdfDoc.Add(FooterTable2);



            pdfDoc.NewPage();

            PdfPTable ParentTable3 = new PdfPTable(1);
            ParentTable3.WidthPercentage = 100;
            ParentTable3.DefaultCell.BorderWidth = 1;

            PdfPTable SubTable3 = new PdfPTable(10);
            SubTable3.WidthPercentage = 100;
            PdfPCell cellP5 = new PdfPCell(SubTable3);


            #region Header

            PdfPCell cellP6 = new PdfPCell(myImage);
            cellP6.Border = 0;
            cellP6.Colspan = 2;
            cellP6.Rowspan = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cellP6.Border = 0;
            cellP6.Colspan = 6;
            cellP6.PaddingBottom = 8;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("", ComHeader));
            cellP6.Border = 0;
            cellP6.Colspan = 2;
            cellP6.PaddingBottom = 8;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);

            //////

            cellP6 = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP6.Colspan = 6;
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("", ComHeader));
            cellP6.Border = 0;
            cellP6.Colspan = 2;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);
            ////


            cellP6 = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP6.Colspan = 6;
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("", ComHeader));
            cellP6.Border = 0;
            cellP6.Colspan = 2;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP6.Colspan = 6;
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("", ComHeader));
            cellP6.Border = 0;
            cellP6.Colspan = 2;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);
            ////

            cellP6 = new PdfPCell(new Phrase("AIRCRAFT INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellP6.Colspan = 6;
            cellP6.PaddingBottom = 10;
            cellP6.BorderWidth = 0;
            cellP6.BorderColorLeft = BaseColor.WHITE;
            cellP6.BorderColorRight = BaseColor.WHITE;
            cellP6.BorderColorTop = BaseColor.WHITE;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("", ComHeader));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 10;
            cellP6.Colspan = 2;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);
            ////

            #endregion

            #region Empty Row

            cellP6 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP6.Colspan = 10;
            cellP6.Border = 2;
            cellP6.BorderWidthLeft = 0.5f;
            cellP6.BorderWidthRight = 0.5f;
            cellP6.BorderWidthTop = 0.5f;
            cellP6.BorderWidthBottom = 0.5f;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            #endregion


            #region Empty Row

            cellP6 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP6.Colspan = 10;
            cellP6.Border = 0;
            cellP6.MinimumHeight = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            #endregion

            #region First Row

            cellP6 = new PdfPCell(new Phrase("Booked For  :", ArialBoldLeft));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["FirstName"].ToString() + " " + dtUserTable.Rows[0]["Lastname"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.Colspan = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("     Invoice Number          :", ArialBoldLeft));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.Colspan = 2;
            cellP6.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("#JEQ123", ArialBoldRight));
            //cellP2 = new PdfPCell(new Phrase("JHSS", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.Colspan = 2;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            #endregion

            #region Second Row

            cellP6 = new PdfPCell(new Phrase("Contact  :", ArialBoldLeft));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["Phonenumber"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("vignesh", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.Colspan = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("Booked Date               :", ArialBoldLeft));
            cellP6.Border = 0;
            cellP6.PaddingBottom = 5;
            cellP6.Colspan = 2;
            cellP6.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), ArialBoldRight));
            //  cellP2 = new PdfPCell(new Phrase("24/01/2000", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 2;
            cellP6.PaddingBottom = 5;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            #endregion
            // #region First Row

            // cellP6 = new PdfPCell(new Phrase("Booked For :", ArialBoldLeft));
            //// cellP6 = new PdfPCell(new Phrase("Quoted For :", ArialBoldLeft));
            // cellP6.Border = 0;
            // cellP6.PaddingBottom = 5;
            // cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            // SubTable3.AddCell(cellP6);

            // //   cellP6 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //// cellP6 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            // cellP6 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["FirstName"].ToString() + " " + dtUserTable.Rows[0]["Lastname"].ToString(), ArialBoldRight));
            // cellP6.Border = 0;
            // cellP6.PaddingBottom = 5;
            // cellP6.Colspan = 5;
            // cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            // SubTable3.AddCell(cellP6);


            // cellP6 = new PdfPCell(new Phrase("Quoted Number :", ArialBoldLeft));
            // cellP6.Border = 0;
            // cellP6.PaddingBottom = 5;
            // cellP6.Colspan = 2;
            // cellP6.HorizontalAlignment = Element.ALIGN_RIGHT;
            // SubTable3.AddCell(cellP6);

            // //cellP6 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["JEQUOTENO"].ToString(), ArialBoldRight));
            // cellP6 = new PdfPCell(new Phrase("JVHSS", ArialBoldRight));
            // cellP6.Border = 0;
            // cellP6.PaddingBottom = 5;
            // cellP6.Colspan = 2;
            // cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            // SubTable3.AddCell(cellP6);

            // #endregion

            // #region Second Row

            // cellP6 = new PdfPCell(new Phrase("Contact :", ArialBoldLeft));
            // cellP6.Border = 0;
            // cellP6.PaddingBottom = 5;
            // cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            // SubTable3.AddCell(cellP6);

            // //  cellP6 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            // cellP6 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            // cellP6.Border = 0;
            // cellP6.PaddingBottom = 5;
            // cellP6.Colspan = 5;
            // cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            // SubTable3.AddCell(cellP6);


            // cellP6 = new PdfPCell(new Phrase("Date Quoted :", ArialBoldLeft));
            // cellP6.Border = 0;
            // cellP6.PaddingBottom = 5;
            // cellP6.Colspan = 2;
            // cellP6.HorizontalAlignment = Element.ALIGN_RIGHT;
            // SubTable3.AddCell(cellP6);

            // //    cellP6 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["QuotedDate"].ToString(), ArialBoldRight));
            // cellP6 = new PdfPCell(new Phrase("20/02/2020", ArialBoldRight));
            // cellP6.Border = 0;
            // cellP6.Colspan = 2;
            // cellP6.PaddingBottom = 5;
            // cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            // SubTable3.AddCell(cellP6);

            // #endregion

            #region Second PAge Content

            cellP6 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;
            cellP6.MinimumHeight = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);






            cellP6 = new PdfPCell(new Phrase("QUOTE AND ADDITIONAL EXPENSES:", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["QteAndAddExp"].ToString(), ArialBoldRight));
            cellP6.Border = 0;
            cellP6.PaddingLeft = 4;
            cellP6.PaddingRight = 4;
            cellP6.SetLeading(0.0f, 1.5f);
            cellP6.Colspan = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;
            cellP6.MinimumHeight = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("PET POLICY:", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["PetPolicy"].ToString(), ArialBoldRight));
            cellP6.Border = 0;
            cellP6.PaddingLeft = 4;
            cellP6.PaddingRight = 4;
            cellP6.SetLeading(0.0f, 1.5f);
            cellP6.Colspan = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;
            cellP6.MinimumHeight = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("PAYMENT TERMS:", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["PaymentTerms"].ToString(), ArialBoldRight));
            cellP6.Border = 0;
            cellP6.PaddingLeft = 4;
            cellP6.PaddingRight = 4;
            cellP6.SetLeading(0.0f, 1.5f);
            cellP6.Colspan = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;
            cellP6.MinimumHeight = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("CANCELLATION POLICY:", ArialBoldRight));
            cellP6.Border = 0;
            cellP6.Colspan = 10;

            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["NONHOLCANPOLICY"].ToString(), ArialBoldRight));
            cellP6.Border = 0;
            cellP6.PaddingLeft = 4;
            cellP6.SetLeading(0.0f, 1.5f);
            cellP6.PaddingRight = 4;
            cellP6.MinimumHeight = 180;
            cellP6.Colspan = 10;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP6.Colspan = 10;
            cellP6.Border = 0;
            cellP6.MinimumHeight = 50;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);


            cellP6 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP6.Colspan = 10;
            cellP6.Border = 2;
            cellP6.BorderWidthLeft = 0.5f;
            cellP6.BorderWidthRight = 0.5f;
            cellP6.BorderWidthTop = 0.5f;
            cellP6.BorderWidthBottom = 0.5f;
            cellP6.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable3.AddCell(cellP6);

            cellP6 = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Italic", 8, Font.BOLD)));
            cellP6.Colspan = 10;
            cellP6.Border = 0;
            cellP6.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable3.AddCell(cellP6);

            #endregion

            PdfPTable FooterTable3 = new PdfPTable(10);
            FooterTable3.WidthPercentage = 100;
            PdfPCell CellF3 = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF3.Colspan = 5;
            CellF3.Border = 0;
            CellF3.PaddingTop = 5;
            CellF3.PaddingBottom = 5;
            CellF3.HorizontalAlignment = Element.ALIGN_LEFT;
            FooterTable3.AddCell(CellF3);

            CellF3 = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF3.Colspan = 5;
            CellF3.Border = 0;
            CellF3.PaddingTop = 5;
            CellF3.PaddingBottom = 5;
            CellF3.HorizontalAlignment = Element.ALIGN_RIGHT;
            FooterTable3.AddCell(CellF3);

            ParentTable3.AddCell(SubTable3);

            pdfDoc.Add(ParentTable3);
            pdfDoc.Add(FooterTable3);



            pdfDoc.NewPage();

            PdfPTable ParentTable4 = new PdfPTable(1);
            ParentTable4.WidthPercentage = 100;
            ParentTable4.DefaultCell.BorderWidth = 1;

            PdfPTable SubTable4 = new PdfPTable(10);
            SubTable4.WidthPercentage = 100;
            PdfPCell cellP8 = new PdfPCell(SubTable4);


            #region Header

            PdfPCell cellP9 = new PdfPCell(myImage);
            cellP9.Border = 0;
            cellP9.Colspan = 2;
            cellP9.Rowspan = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cellP9.Border = 0;
            cellP9.Colspan = 6;
            cellP9.PaddingBottom = 8;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase("", ComHeader));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 8;
            cellP9.Colspan = 2;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);

            //////

            cellP9 = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP9.Colspan = 6;
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ComHeader));
            cellP9.Border = 0;
            cellP9.Colspan = 2;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);
            ////


            cellP9 = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP9.Colspan = 6;
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ComHeader));
            cellP9.Border = 0;
            cellP9.Colspan = 2;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP9.Colspan = 6;
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ComHeader));
            cellP9.Border = 0;
            cellP9.Colspan = 2;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);
            ////

            cellP9 = new PdfPCell(new Phrase("AIRCRAFT INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellP9.Colspan = 6;
            cellP9.PaddingBottom = 10;
            cellP9.BorderWidth = 0;
            cellP9.BorderColorLeft = BaseColor.WHITE;
            cellP9.BorderColorRight = BaseColor.WHITE;
            cellP9.BorderColorTop = BaseColor.WHITE;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase("", ComHeader));
            cellP9.Border = 0;
            cellP9.Colspan = 2;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);
            ////

            #endregion


            #region Empty Row

            cellP9 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP9.Colspan = 10;
            cellP9.Border = 2;
            cellP9.BorderWidthLeft = 0.5f;
            cellP9.BorderWidthRight = 0.5f;
            cellP9.BorderWidthTop = 0.5f;
            cellP9.BorderWidthBottom = 0.5f;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            #endregion


            #region Empty Row

            cellP9 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP9.Colspan = 10;
            cellP9.Border = 0;
            cellP9.MinimumHeight = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            #endregion

            #region First Row

            cellP9 = new PdfPCell(new Phrase("Booked For :", ArialBoldLeft));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["FirstName"].ToString() + " " + dtUserTable.Rows[0]["Lastname"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.Colspan = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase("     Invoice Number          :", ArialBoldLeft));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.Colspan = 2;
            cellP9.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("#JEQ123", ArialBoldRight));
            //cellP2 = new PdfPCell(new Phrase("JHSS", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.Colspan = 2;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            #endregion

            #region Second Row

            cellP9 = new PdfPCell(new Phrase("Contact   :", ArialBoldLeft));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["Phonenumber"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("vignesh", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.Colspan = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase("Booked Date               :", ArialBoldLeft));
            cellP9.Border = 0;
            cellP9.PaddingBottom = 5;
            cellP9.Colspan = 2;
            cellP9.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), ArialBoldRight));
            //  cellP2 = new PdfPCell(new Phrase("24/01/2000", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 2;
            cellP9.PaddingBottom = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            #endregion

            //  #region First Row

            //  cellP9 = new PdfPCell(new Phrase("Quoted For :", ArialBoldLeft));
            //  cellP9.Border = 0;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable4.AddCell(cellP9);

            // // cellP9 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //   cellP9 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //  cellP9.Border = 0;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.Colspan = 5;
            //  cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable4.AddCell(cellP9);


            //  cellP9 = new PdfPCell(new Phrase("Quoted Number :", ArialBoldLeft));
            //  cellP9.Border = 0;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.Colspan = 2;
            //  cellP9.HorizontalAlignment = Element.ALIGN_RIGHT;
            //  SubTable4.AddCell(cellP9);

            //  //cellP9 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["JEQUOTENO"].ToString(), ArialBoldRight));
            //  cellP9 = new PdfPCell(new Phrase("JVHSS", ArialBoldRight));
            //  cellP9.Border = 0;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.Colspan = 2;
            //  cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable4.AddCell(cellP9);

            //  #endregion

            //  #region Second Row

            //  cellP9 = new PdfPCell(new Phrase("Contact :", ArialBoldLeft));
            //  cellP9.Border = 0;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable4.AddCell(cellP9);

            // // cellP9 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //   cellP9 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //  cellP9.Border = 0;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.Colspan = 5;
            //  cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable4.AddCell(cellP9);


            //  cellP9 = new PdfPCell(new Phrase("Date Quoted :", ArialBoldLeft));
            //  cellP9.Border = 0;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.Colspan = 2;
            //  cellP9.HorizontalAlignment = Element.ALIGN_RIGHT;
            //  SubTable4.AddCell(cellP9);

            ////  cellP9 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["QuotedDate"].ToString(), ArialBoldRight));
            //    cellP9 = new PdfPCell(new Phrase("20/01/2020", ArialBoldRight));
            //  cellP9.Border = 0;
            //  cellP9.Colspan = 2;
            //  cellP9.PaddingBottom = 5;
            //  cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable4.AddCell(cellP9);

            //  #endregion

            #region Second PAge Content

            cellP9 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.MinimumHeight = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);



            cellP9 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["HOLCANPOLCY"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            cellP9.Border = 0;
            cellP9.PaddingLeft = 4;
            cellP9.PaddingRight = 4;
            cellP9.SetLeading(0.0f, 1.5f);
            cellP9.Colspan = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.MinimumHeight = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase("ITINERARY CHANGE AND TRANSMITTAL OF CHANGES:", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["ITENARAYCHANGE"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            cellP9.Border = 0;
            cellP9.PaddingLeft = 4;
            cellP9.PaddingRight = 4;
            cellP9.SetLeading(0.0f, 1.5f);
            cellP9.Colspan = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.MinimumHeight = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase("INDEMNIFICATION:", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["INDEMNIFICATION"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            cellP9.Border = 0;
            cellP9.PaddingLeft = 4;
            cellP9.PaddingRight = 4;
            cellP9.SetLeading(0.0f, 1.5f);
            cellP9.Colspan = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.MinimumHeight = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase("LIMITATION OF LIABILITY:", ArialBoldRight));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);


            cellP9 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["LIMITATIONOFLIABLITY"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            cellP9.Border = 0;
            cellP9.PaddingLeft = 4;
            cellP9.PaddingRight = 4;
            cellP9.SetLeading(0.0f, 1.5f);
            cellP9.Colspan = 10;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP9.Colspan = 10;
            cellP9.Border = 0;
            cellP9.MinimumHeight = 50;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP9.Colspan = 10;
            cellP9.Border = 2;
            cellP9.BorderWidthLeft = 0.5f;
            cellP9.BorderWidthRight = 0.5f;
            cellP9.BorderWidthTop = 0.5f;
            cellP9.BorderWidthBottom = 0.5f;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            cellP9 = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Italic", 8, Font.BOLD)));
            cellP9.Colspan = 10;
            cellP9.Border = 0;
            cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable4.AddCell(cellP9);

            #endregion



            PdfPTable FooterTable4 = new PdfPTable(10);
            FooterTable4.WidthPercentage = 100;
            PdfPCell CellF4 = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF4.Colspan = 5;
            CellF4.Border = 0;
            CellF4.PaddingTop = 5;
            CellF4.PaddingBottom = 5;
            CellF4.HorizontalAlignment = Element.ALIGN_LEFT;
            FooterTable4.AddCell(CellF4);

            CellF4 = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF4.Colspan = 5;
            CellF4.Border = 0;
            CellF4.PaddingTop = 5;
            CellF4.PaddingBottom = 5;
            CellF4.HorizontalAlignment = Element.ALIGN_RIGHT;
            FooterTable4.AddCell(CellF4);

            ParentTable4.AddCell(SubTable4);

            pdfDoc.Add(ParentTable4);
            pdfDoc.Add(FooterTable4);






            pdfDoc.NewPage();

            PdfPTable ParentTable5 = new PdfPTable(1);
            ParentTable5.WidthPercentage = 100;
            ParentTable5.DefaultCell.BorderWidth = 1;

            PdfPTable SubTable5 = new PdfPTable(10);
            SubTable5.WidthPercentage = 100;
            PdfPCell cellP11 = new PdfPCell(SubTable5);


            #region Header

            PdfPCell cellP12 = new PdfPCell(myImage);
            cellP12.Border = 0;
            cellP12.Colspan = 2;
            cellP12.Rowspan = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cellP12.Border = 0;
            cellP12.Colspan = 6;
            cellP12.PaddingBottom = 8;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("", ComHeader));
            cellP12.Border = 0;
            cellP12.Colspan = 2;
            cellP12.PaddingBottom = 8;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);

            //////

            cellP12 = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP12.Colspan = 6;
            cellP12.PaddingBottom = 5;
            cellP12.Border = 0;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("", ComHeader));
            cellP12.Border = 0;
            cellP12.Colspan = 2;
            cellP12.PaddingBottom = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);
            ////


            cellP12 = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP12.Colspan = 6;
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("", ComHeader));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.Colspan = 2;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP12.Colspan = 6;
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("", ComHeader));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.Colspan = 2;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);
            ////

            cellP12 = new PdfPCell(new Phrase("AIRCRAFT INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellP12.Colspan = 6;
            cellP12.PaddingBottom = 10;
            cellP12.BorderWidth = 0;
            cellP12.BorderColorLeft = BaseColor.WHITE;
            cellP12.BorderColorRight = BaseColor.WHITE;
            cellP12.BorderColorTop = BaseColor.WHITE;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("", ComHeader));
            cellP12.Border = 0;
            cellP12.Colspan = 2;
            cellP12.PaddingBottom = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);
            ////

            #endregion


            #region Empty Row

            cellP12 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP12.Colspan = 10;
            cellP12.Border = 2;
            cellP12.BorderWidthLeft = 0.5f;
            cellP12.BorderWidthRight = 0.5f;
            cellP12.BorderWidthTop = 0.5f;
            cellP12.BorderWidthBottom = 0.5f;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            #endregion


            #region Empty Row

            cellP12 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP12.Colspan = 10;
            cellP12.Border = 0;
            cellP12.MinimumHeight = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            #endregion

            #region First Row

            cellP12 = new PdfPCell(new Phrase("Booked For :", ArialBoldLeft));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["FirstName"].ToString() + " " + dtUserTable.Rows[0]["Lastname"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.Colspan = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("     Invoice Number          :", ArialBoldLeft));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.Colspan = 2;
            cellP12.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("#JEQ123", ArialBoldRight));
            //cellP2 = new PdfPCell(new Phrase("JHSS", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.Colspan = 2;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            #endregion

            #region Second Row

            cellP12 = new PdfPCell(new Phrase("Contact  :", ArialBoldLeft));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["Phonenumber"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("vignesh", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.Colspan = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("Booked Date               :", ArialBoldLeft));
            cellP12.Border = 0;
            cellP12.PaddingBottom = 5;
            cellP12.Colspan = 2;
            cellP12.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), ArialBoldRight));
            //  cellP2 = new PdfPCell(new Phrase("24/01/2000", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 2;
            cellP12.PaddingBottom = 5;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            #endregion


            //  #region First Row

            //  cellP12 = new PdfPCell(new Phrase("Quoted For :", ArialBoldLeft));
            //  cellP12.Border = 0;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable5.AddCell(cellP12);

            // // cellP12 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //   cellP12 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //  cellP12.Border = 0;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.Colspan = 5;
            //  cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable5.AddCell(cellP12);


            //  cellP12 = new PdfPCell(new Phrase("Quoted Number :", ArialBoldLeft));
            //  cellP12.Border = 0;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.Colspan = 2;
            //  cellP12.HorizontalAlignment = Element.ALIGN_RIGHT;
            //  SubTable5.AddCell(cellP12);

            // // cellP12 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["JEQUOTENO"].ToString(), ArialBoldRight));
            //   cellP12 = new PdfPCell(new Phrase("JVHSS", ArialBoldRight));
            //  cellP12.Border = 0;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.Colspan = 2;
            //  cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable5.AddCell(cellP12);

            //  #endregion

            //  #region Second Row

            //  cellP12 = new PdfPCell(new Phrase("Contact :", ArialBoldLeft));
            //  cellP12.Border = 0;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable5.AddCell(cellP12);

            // // cellP12 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //   cellP12 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //  cellP12.Border = 0;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.Colspan = 5;
            //  cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable5.AddCell(cellP12);


            //  cellP12 = new PdfPCell(new Phrase("Date Quoted :", ArialBoldLeft));
            //  cellP12.Border = 0;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.Colspan = 2;
            //  cellP12.HorizontalAlignment = Element.ALIGN_RIGHT;
            //  SubTable5.AddCell(cellP12);

            ////  cellP12 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["QuotedDate"].ToString(), ArialBoldRight));
            //    cellP12 = new PdfPCell(new Phrase("24/02/2020", ArialBoldRight));
            //  cellP12.Border = 0;
            //  cellP12.Colspan = 2;
            //  cellP12.PaddingBottom = 5;
            //  cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            //  SubTable5.AddCell(cellP12);

            //  #endregion

            #region Fifth PAge Content

            cellP12 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 10;
            cellP12.MinimumHeight = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            string strChange = "IN NO EVENT WILL CHARTER COMPANY, AIRCRAFT OWNERS AND AIRCREW BE LIABLE FOR ANY TYPE OF INDIRECT, SPECIAL, " +
                            "INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, WHETHER ARISING IN CONTRACT OR IN TORT, INCLUDING, BUT NOT " +
                            "LIMITED TO, LOST REVENUES, LOST PROFIT, LOSS OF PROSPECTIVE ECONOMIC ADVANTAGE, LOSS OF REPUTATION, OR " +
                            "EXPENSES DUE TO REPLACEMENT TRAVEL ARRANGEMENTS, EXCEPT WHEN SUCH CLAIMS ARE DUE TO THE GROSS " +
                            "NEGLIGENCE OR INTENTIONAL MISCONDUCT OF CHARTER COMPANY, AIRCRAFT OWNERS OR AIRCREW, EVEN IF THE " +
                            "CLIENT HAD BEEN ADVISED, OR KNEW OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES. CLIENT WILL" +
                            "INDEMNIFY AND HOLD CHARTER COMPANY. AIRCRAFT OWNERS AND AIRCREW HARMLESS AGAINST ANY LOSS, DAMAGE OR " +
                            "EXPENSE INCURRED BY CHARTER COMPANY BY REASON OF ANY ACTION OR OMISSION OF CLIENT, ITS EMPLOYEES, " +
                            "AGENTS AND GUESTS. " +
                            "Charter Company and aircraft owners will not be held liable for the negligent actions or omissions of a Cabin Attendant or other support " +
                            "personnel not provided by Charter Company. " +
                            "Charter Company makes no representations or warranties of any kind, either express or implied, as to any matter limited to implied " +
                            "warranties of fitness for a particular purpose, merchantability or otherwise." +
                            "If the Client's Schedule involves an ultimate destination or stop in a country other than the country of departure, the Warsaw Convention " +
                            "may be applicable, and the Warsaw Convention governs, and in most cases, limits the liability of the carrier for death or personal injury and " +
                            "for loss of or damage to baggage." +
                            "If Charter Company provides a Quote on behalf of another air carrier for a contracted aircraft, then Client understands that Charter " +
                            "Company has not represented itself as a Direct Air Carrier nor an Indirect Air Carrier, but is acting solely as agent of the air carrier that " +
                            "provides the contracted aircraft, which will result in Client having to look to the other air carrier for a legal remedy. ";


            cellP12 = new PdfPCell(new Phrase(strChange, ArialBoldRight));
            cellP12.Border = 0;
            cellP12.PaddingLeft = 4;
            cellP12.PaddingRight = 4;
            cellP12.SetLeading(0.0f, 1.5f);
            cellP12.Colspan = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 10;
            cellP12.MinimumHeight = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("OPERATIONAL:", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["OPERATIONAL"].ToString(), ArialBoldRight));
            cellP12.Border = 0;
            cellP12.PaddingLeft = 4;
            cellP12.SetLeading(0.0f, 1.5f);
            cellP12.PaddingRight = 4;
            cellP12.Colspan = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 10;
            cellP12.MinimumHeight = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("REQUIRED TRAVEL DOCUMENTATION:", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["TRAVELDOCUMENTS"].ToString(), ArialBoldRight));
            cellP12.Border = 0;
            cellP12.PaddingLeft = 4;
            cellP12.SetLeading(0.0f, 1.5f);
            cellP12.PaddingRight = 4;
            cellP12.Colspan = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 10;
            cellP12.MinimumHeight = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("PROHIBITED ITEMS:", ArialBoldRight));
            cellP12.Border = 0;
            cellP12.Colspan = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            string strItems = "Passengers have responsibility to comply with Transportation Security Agency (TSA) and Federal Aviation Administration (FAA) regulations" +
"regarding prohibited items. Days in advance of the departure time, passengers shall review TSA and FAA Websites for prohibited items: " +
"www.tsa.gov/travelers/airtravel/prohibited/permitted-prohibited-items.shtm <http://www.tsa.gov/travelers/airtravel/prohibited/permitted-prohib " +
"ited-items.shtm> and <http://www.faa.gov/go/packsafe>. Examples of prohibited items not allowed on aircraft include, but are not limited to: " +
"firearms, ammunition, knives, fireworks, and flammable liquids, gels, or aerosols, as well as items with lithium battery powered products, " +
"such as Hover Boards/Gravity Boards/Balance Boards.* Battery-powered E-cigarettes, vaporizing, vape pens, atomizers, and electronic " +
"nicotine delivery systems are prohibited in stowed baggage, may only be carried on a person, and may not be used or charged on the " +
"aircraft, pursuant to the FAA directives. Spare lithium batteries are prohibited in stowed baggage and may only be carried on a person. " +
"Clients should check the TSA and FAA Websites for a current list of prohibited items. Pursuant to TSA and FAA regulations, Charter";

            cellP12 = new PdfPCell(new Phrase(strItems.ToString(), ArialBoldRight));
            cellP12.Border = 0;
            cellP12.PaddingLeft = 4;
            cellP12.PaddingRight = 4;
            cellP12.MinimumHeight = 100;
            cellP12.SetLeading(0.0f, 1.5f);
            cellP12.Colspan = 10;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP12.Colspan = 10;
            cellP12.Border = 0;
            cellP12.MinimumHeight = 100;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);


            cellP12 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP12.Colspan = 10;
            cellP12.Border = 2;
            cellP12.BorderWidthLeft = 0.5f;
            cellP12.BorderWidthRight = 0.5f;
            cellP12.BorderWidthTop = 0.5f;
            cellP12.BorderWidthBottom = 0.5f;
            cellP12.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable5.AddCell(cellP12);

            cellP12 = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Italic", 8, Font.BOLD)));
            cellP12.Colspan = 10;
            cellP12.Border = 0;
            cellP12.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable5.AddCell(cellP12);

            #endregion

            PdfPTable FooterTable5 = new PdfPTable(10);
            FooterTable5.WidthPercentage = 100;
            PdfPCell CellF5 = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF5.Colspan = 5;
            CellF5.Border = 0;
            CellF5.PaddingTop = 5;
            CellF5.PaddingBottom = 5;
            CellF5.HorizontalAlignment = Element.ALIGN_LEFT;
            FooterTable5.AddCell(CellF5);

            CellF5 = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF5.Colspan = 5;
            CellF5.Border = 0;
            CellF5.PaddingTop = 5;
            CellF5.PaddingBottom = 5;
            CellF5.HorizontalAlignment = Element.ALIGN_RIGHT;
            FooterTable5.AddCell(CellF5);

            ParentTable5.AddCell(SubTable5);

            pdfDoc.Add(ParentTable5);
            pdfDoc.Add(FooterTable5);




            pdfDoc.NewPage();

            PdfPTable ParentTable6 = new PdfPTable(1);
            ParentTable6.WidthPercentage = 100;
            ParentTable6.DefaultCell.BorderWidth = 1;

            PdfPTable SubTable6 = new PdfPTable(10);
            SubTable6.WidthPercentage = 100;
            PdfPCell cellP14 = new PdfPCell(SubTable6);


            #region Header

            PdfPCell cellP15 = new PdfPCell(myImage);
            cellP15.Border = 0;
            cellP15.Colspan = 2;
            cellP15.Rowspan = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cellP15.Border = 0;
            cellP15.Colspan = 6;
            cellP15.PaddingBottom = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("", ComHeader));
            cellP15.Border = 0;
            cellP15.Colspan = 2;
            cellP15.PaddingBottom = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);

            //////

            cellP15 = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP15.Colspan = 6;
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ComHeader));
            cellP15.Border = 0;
            cellP15.Colspan = 2;
            cellP15.PaddingBottom = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);
            ////


            cellP15 = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP15.Colspan = 6;
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ComHeader));
            cellP15.Border = 0;
            cellP15.Colspan = 2;
            cellP15.PaddingBottom = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP15.Colspan = 6;
            cellP15.Border = 0;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ComHeader));
            cellP15.Border = 0;
            cellP15.Colspan = 2;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);
            ////

            cellP15 = new PdfPCell(new Phrase("AIRCRAFT INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellP15.Colspan = 6;
            cellP15.PaddingBottom = 10;
            cellP15.BorderWidth = 0;
            cellP15.PaddingBottom = 10;
            cellP15.BorderColorLeft = BaseColor.WHITE;
            cellP15.BorderColorRight = BaseColor.WHITE;
            cellP15.BorderColorTop = BaseColor.WHITE;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("", ComHeader));
            cellP15.Border = 0;
            cellP15.Colspan = 2;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);
            ////

            #endregion


            #region Empty Row

            cellP15 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP15.Colspan = 10;
            cellP15.Border = 2;
            cellP15.BorderWidthLeft = 0.5f;
            cellP15.BorderWidthRight = 0.5f;
            cellP15.BorderWidthTop = 0.5f;
            cellP15.BorderWidthBottom = 0.5f;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            #endregion


            #region Empty Row

            cellP15 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP15.Colspan = 10;
            cellP15.Border = 0;
            cellP15.MinimumHeight = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            #endregion

            #region First Row

            cellP15 = new PdfPCell(new Phrase("Booked For  :", ArialBoldLeft));
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["FirstName"].ToString() + " " + dtUserTable.Rows[0]["Lastname"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.Colspan = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("     Invoice Number          :", ArialBoldLeft));
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.Colspan = 2;
            cellP15.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("#JEQ123", ArialBoldRight));
            //cellP2 = new PdfPCell(new Phrase("JHSS", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.Colspan = 2;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            #endregion

            #region Second Row

            cellP15 = new PdfPCell(new Phrase("Contact :", ArialBoldLeft));
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["Phonenumber"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("vignesh", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.Colspan = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("Booked Date               :", ArialBoldLeft));
            cellP15.Border = 0;
            cellP15.PaddingBottom = 5;
            cellP15.Colspan = 2;
            cellP15.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), ArialBoldRight));
            //  cellP2 = new PdfPCell(new Phrase("24/01/2000", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 2;
            cellP15.PaddingBottom = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            #endregion
            //    #region First Row

            //    cellP15 = new PdfPCell(new Phrase("Quoted For :", ArialBoldLeft));
            //    cellP15.Border = 0;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable6.AddCell(cellP15);

            //  //  cellP15 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //      cellP15 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //    cellP15.Border = 0;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.Colspan = 5;
            //    cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable6.AddCell(cellP15);


            //    cellP15 = new PdfPCell(new Phrase("Quoted Number :", ArialBoldLeft));
            //    cellP15.Border = 0;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.Colspan = 2;
            //    cellP15.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    SubTable6.AddCell(cellP15);

            //   // cellP15 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["JEQUOTENO"].ToString(), ArialBoldRight));
            //     cellP15 = new PdfPCell(new Phrase("JVHSS", ArialBoldRight));
            //    cellP15.Border = 0;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.Colspan = 2;
            //    cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable6.AddCell(cellP15);

            //    #endregion

            //    #region Second Row

            //    cellP15 = new PdfPCell(new Phrase("Contact :", ArialBoldLeft));
            //    cellP15.Border = 0;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable6.AddCell(cellP15);

            //    //cellP15 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //     cellP15 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //    cellP15.Border = 0;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.Colspan = 5;
            //    cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable6.AddCell(cellP15);


            //    cellP15 = new PdfPCell(new Phrase("Date Quoted :", ArialBoldLeft));
            //    cellP15.Border = 0;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.Colspan = 2;
            //    cellP15.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    SubTable6.AddCell(cellP15);

            ////    cellP15 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["QuotedDate"].ToString(), ArialBoldRight));
            //        cellP15 = new PdfPCell(new Phrase("24/02/2020", ArialBoldRight));
            //    cellP15.Border = 0;
            //    cellP15.Colspan = 2;
            //    cellP15.PaddingBottom = 5;
            //    cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable6.AddCell(cellP15);

            //    #endregion

            #region Sixth PAge Content

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            string strPRo = "Certificate. No later than 24 hours in advance of the departure time, Client shall notify Charter Company that a passenger wishes to bring a " +
        "firearm, ammunition, or both on the aircraft, as well as the description of each firearm and type of ammunition. The flight crew will secure " +
        "the weapon and ammunition through use of a cable lock or locked container prior to departure depending on the aircraft. The flight crew " +
        "shall follow TSA rules in transporting firearms and ammunition aboard an aircraft, including securing the firearm as unloaded and physically " +
        "separated from the ammunition. Each passenger may only bring one case of ammunition. A passenger may also not carry concealed " +
        "weapons onboard an aircraft, unless approved by the Charter Company prior to departure. Concealed weapon requests must be requested " +
        "to the Charter Company at least 24 hours before the flight to allow verification of credentials. Only Federal Law Enforcement Officers or Air " +
        "Marshals are allowed to carry concealed weapons on board an aircraft. The Pilot in Command has final authority on whether any of these " +
        "or other items are allowed on the aircraft. *Due to the recent FAA Statement on the Samsung Galaxy Note 7 device, Charter Company will " +
        "not allow this device on board any flight, including in stowed baggage. " +
        "Without prior approval from the Pilot in Command, photography and filming of the interior and exterior of the aircraft is strictly prohibited. " +
        "Charter Company maintains a 'Zero Tolerance' policy regarding illegal drugs and/or smuggling. Charter Company reserves the right to " +
        "inspect any bags or luggage brought to the aircraft by Client or any person travelling with Client, and may deny boarding of certain items " +
        "deemed unsafe and/or illegal. If any illegal activity occurs, the flight will be grounded. For International flights, Charter Company will return " +
        "to an airport within the United States to ground the flight. In addition, Charter Company will alert customs authorities to any illegal activity. " +
        "In any such event, Client will be charged 100% of the Quote, plus actual expenses incurred for the grounded flight.";

            cellP15 = new PdfPCell(new Phrase(strPRo, ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.PaddingRight = 4;
            cellP15.SetLeading(0.0f, 1.5f);
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 5;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("APPLICABLE LAW:", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["APPLICAABLElAW"].ToString(), ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.PaddingRight = 4;
            cellP15.SetLeading(0.0f, 1.5f);
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("ATTORNEY'S FEES:", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["ATTORNEYSFEE"].ToString(), ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.PaddingRight = 4;
            cellP15.SetLeading(0.0f, 1.5f);
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("ENTIRE AGREEMENT:", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["ENTIREAGREMENT"].ToString(), ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.PaddingRight = 4;
            cellP15.SetLeading(0.0f, 1.5f);
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("ASSIGNMENT:", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["ASSIGNMENT"].ToString(), ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.SetLeading(0.0f, 1.5f);
            cellP15.PaddingRight = 4;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("WAIVER:", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["WAIVER"].ToString(), ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.PaddingRight = 4;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.SetLeading(0.0f, 1.5f);
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("SEVERABILITY:", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["SEVERABLITY"].ToString(), ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.SetLeading(0.0f, 1.5f);
            cellP15.PaddingRight = 4;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.MinimumHeight = 8;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("BINDING ARBITRATION:", ArialBoldRight));
            cellP15.Border = 0;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase(dtPDFMAster.Rows[0]["BINDINGARBITRATION"].ToString(), ArialBoldRight));
            cellP15.Border = 0;
            cellP15.PaddingLeft = 4;
            cellP15.SetLeading(0.0f, 1.0f);
            cellP15.PaddingRight = 4;
            cellP15.Colspan = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP15.Colspan = 10;
            cellP15.Border = 0;
            cellP15.MinimumHeight = 10;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);


            cellP15 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP15.Colspan = 10;
            cellP15.Border = 2;
            cellP15.BorderWidthLeft = 0.5f;
            cellP15.BorderWidthRight = 0.5f;
            cellP15.BorderWidthTop = 0.5f;
            cellP15.BorderWidthBottom = 0.5f;
            cellP15.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable6.AddCell(cellP15);

            cellP15 = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Italic", 8, Font.BOLD)));
            cellP15.Colspan = 10;
            cellP15.Border = 0;
            cellP15.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable6.AddCell(cellP15);

            #endregion

            PdfPTable FooterTable6 = new PdfPTable(10);
            FooterTable6.WidthPercentage = 100;
            PdfPCell CellF6 = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF6.Colspan = 5;
            CellF6.Border = 0;
            CellF6.PaddingTop = 5;
            CellF6.PaddingBottom = 5;
            CellF6.HorizontalAlignment = Element.ALIGN_LEFT;
            FooterTable6.AddCell(CellF6);

            CellF6 = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF6.Colspan = 5;
            CellF6.Border = 0;
            CellF6.PaddingTop = 5;
            CellF6.PaddingBottom = 5;
            CellF6.HorizontalAlignment = Element.ALIGN_RIGHT;
            FooterTable6.AddCell(CellF6);


            ParentTable6.AddCell(SubTable6);

            pdfDoc.Add(ParentTable6);
            pdfDoc.Add(FooterTable6);



            pdfDoc.NewPage();

            PdfPTable ParentTable7 = new PdfPTable(1);
            ParentTable7.WidthPercentage = 100;
            ParentTable7.DefaultCell.BorderWidth = 1;

            PdfPTable SubTable7 = new PdfPTable(10);
            SubTable7.WidthPercentage = 100;
            PdfPCell cellP17 = new PdfPCell(SubTable7);


            #region Header

            PdfPCell cellP18 = new PdfPCell(myImage);
            cellP18.Border = 0;
            cellP18.Colspan = 2;
            cellP18.Rowspan = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cellP18.Border = 0;
            cellP18.Colspan = 6;
            cellP18.PaddingBottom = 8;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("", ComHeader));
            cellP18.Border = 0;
            cellP18.Colspan = 2;
            cellP18.PaddingBottom = 8;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);

            //////

            cellP18 = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP18.Colspan = 6;
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ComHeader));
            cellP18.Border = 0;
            cellP18.Colspan = 2;
            cellP18.PaddingBottom = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);
            ////


            cellP18 = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP18.Colspan = 6;
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ComHeader));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.Colspan = 2;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cellP18.Colspan = 6;
            cellP18.PaddingBottom = 5;
            cellP18.Border = 0;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ComHeader));
            cellP18.Border = 0;
            cellP18.Colspan = 2;
            cellP18.PaddingBottom = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);
            ////

            cellP18 = new PdfPCell(new Phrase("AIRCRAFT INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cellP18.Colspan = 6;
            cellP18.PaddingBottom = 10;
            cellP18.BorderWidth = 0;
            cellP18.BorderColorLeft = BaseColor.WHITE;
            cellP18.BorderColorRight = BaseColor.WHITE;
            cellP18.BorderColorTop = BaseColor.WHITE;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("", ComHeader));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 10;
            cellP18.Colspan = 2;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);
            ////

            #endregion

            #region Empty Row

            cellP18 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP18.Colspan = 10;
            cellP18.Border = 2;
            cellP18.BorderWidthLeft = 0.5f;
            cellP18.BorderWidthRight = 0.5f;
            cellP18.BorderWidthTop = 0.5f;
            cellP18.BorderWidthBottom = 0.5f;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            #endregion


            #region Empty Row

            cellP18 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP18.Colspan = 10;
            cellP18.Border = 0;
            cellP18.MinimumHeight = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            #endregion

            #region First Row

            cellP18 = new PdfPCell(new Phrase("Booked For  :", ArialBoldLeft));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["FirstName"].ToString() + " " + dtUserTable.Rows[0]["Lastname"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.Colspan = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("     Invoice Number          :", ArialBoldLeft));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.Colspan = 2;
            cellP18.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("#JEQ123", ArialBoldRight));
            //cellP2 = new PdfPCell(new Phrase("JHSS", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.Colspan = 2;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            #endregion

            #region Second Row

            cellP18 = new PdfPCell(new Phrase("Contact  :", ArialBoldLeft));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase(dtUserTable.Rows[0]["Phonenumber"].ToString(), ArialBoldRight));
            // cellP2 = new PdfPCell(new Phrase("vignesh", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.Colspan = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("Booked Date               :", ArialBoldLeft));
            cellP18.Border = 0;
            cellP18.PaddingBottom = 5;
            cellP18.Colspan = 2;
            cellP18.HorizontalAlignment = Element.ALIGN_RIGHT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), ArialBoldRight));
            //  cellP2 = new PdfPCell(new Phrase("24/01/2000", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 2;
            cellP18.PaddingBottom = 5;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            #endregion

            //   #region First Row

            //   cellP18 = new PdfPCell(new Phrase("Quoted For :", ArialBoldLeft));
            //   cellP18.Border = 0;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            //   SubTable7.AddCell(cellP18);

            // //  cellP18 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //     cellP18 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //   cellP18.Border = 0;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.Colspan = 5;
            //   cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            //   SubTable7.AddCell(cellP18);


            //   cellP18 = new PdfPCell(new Phrase("Quoted Number :", ArialBoldLeft));
            //   cellP18.Border = 0;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.Colspan = 2;
            //   cellP18.HorizontalAlignment = Element.ALIGN_RIGHT;
            //   SubTable7.AddCell(cellP18);

            //  // cellP18 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["JEQUOTENO"].ToString(), ArialBoldRight));
            //    cellP18 = new PdfPCell(new Phrase("JVHSS", ArialBoldRight));
            //   cellP18.Border = 0;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.Colspan = 2;
            //   cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            //   SubTable7.AddCell(cellP18);

            //   #endregion

            //   #region Second Row

            //   cellP18 = new PdfPCell(new Phrase("Contact :", ArialBoldLeft));
            //   cellP18.Border = 0;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            //   SubTable7.AddCell(cellP18);

            //   //cellP18 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Requester"].ToString(), ArialBoldRight));
            //      cellP18 = new PdfPCell(new Phrase("Vignesh", ArialBoldRight));
            //   cellP18.Border = 0;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.Colspan = 5;
            //   cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            //   SubTable7.AddCell(cellP18);


            //   cellP18 = new PdfPCell(new Phrase("Date Quoted :", ArialBoldLeft));
            //   cellP18.Border = 0;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.Colspan = 2;
            //   cellP18.HorizontalAlignment = Element.ALIGN_RIGHT;
            //   SubTable7.AddCell(cellP18);

            ////   cellP18 = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["QuotedDate"].ToString(), ArialBoldRight));
            //      cellP18 = new PdfPCell(new Phrase("2020/02/24", ArialBoldRight));
            //   cellP18.Border = 0;
            //   cellP18.Colspan = 2;
            //   cellP18.PaddingBottom = 5;
            //   cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            //   SubTable7.AddCell(cellP18);

            //   #endregion

            #region Seventh PAge Content

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            string strAr = "This Arbitration Agreement is made pursuant to a transaction involving interstate commerce, and shall be governed by the Federal" +
                    "Arbitration Act, 9 U.S.C. Sections 1-16. Any dispute concerning the enforceability of this arbitration agreement shall be brought in any state" +
                    "or federal court located in the State of California, in the County of Los Angeles. Client hereby irrevocably submits and consents to the" +
                    "exclusive jurisdiction of the state and federal courts located in the State of California, in the County of Los Angeles. THE PARTIES" +
                    "UNDERSTAND THAT THEY WOULD HAVE HAD A RIGHT OR OPPORTUNITY TO LITIGATE THROUGH A COURT AND TO HAVE A" +
                    "JUDGE OR JURY DECIDE THEIR CASE, BUT THEY CHOOSE TO HAVE ANY AND ALL DISPUTES DECIDED THROUGH" +
                    "ARBITRATION. BY SIGNING THIS AGREEMENT, THE PARTIES ARE GIVING UP ANY RIGHT THEY MIGHT HAVE TO SUE EACH" +
                    "OTHER.";

            cellP18 = new PdfPCell(new Phrase(strAr, ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            string strPRov = "The provisions in the sections titled Indemnification, Limitation of Liability, Applicable Law, Attorney’s Fees, Entire Agreement, Assignment," +
       "Waiver, Severability, and Binding Arbitration shall survive the termination or expiration of this Agreement";


            cellP18 = new PdfPCell(new Phrase(strPRov, ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("CREDIT CARD/ WIRE TRANSFER INSTRUCTIONS:", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            string strCRED = "CREDIT CARD AUTHORIZATION REQUIRED UPON BOOKING FOR ANY CHARTER RELATED REQUESTS: A COPY OF THE FRONT/" +
"BACK OF THE CREDIT CARD AND A COPY OF THE CARD HOLDER'S DRIVER'S LICENSE WILL RE REQUIRED UPON APPROVAL.";

            cellP18 = new PdfPCell(new Phrase(strCRED, ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("Card Type: AMEX I VISA I MC", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("Card Number: ________________________________ Name as it appears on Credit Card: ______________________________", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("Expiration Date: ______ Security Code: ________ Billing Address: ___________________________________________________", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            string strDeclar = "* I, ________________________________, hereby grant Jet Edge International LLC ('Jet Edge'), as a condition precedent for providing " +
            "air charter and related services, continuous approval to authorize my credit card account for all services provided or arranged by Jet Edge at " +
            "my request or the request of my authorized designee. This approval shall remain in full force and effect until cancelled in writing with thirty (3 " +
            "0) calendar days’ advance notice. Such authorizations shall secure my prompt payment for all services. Additionally, I hereby permit Jet " +
            "Edge, to convert without further notice, the credit card authorization(s) to a credit card charge per Jet Edge standard terms and conditions " +
            "hereby incorporated by reference. Credit card charges for catering and ground transportation expenses shall be made without credit card " +
            "surcharge.";

            cellP18 = new PdfPCell(new Phrase(strDeclar, ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("Print Name: __________________________ Signature: _______________________________ Date: __________", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("WIRE TRANSFER INSTRUCTIONS:", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("         Bank: Fifth Third Bank", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("                 38 Fountain Square Plaza", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);



            cellP18 = new PdfPCell(new Phrase("                 Cincinnati, OH 45263-9235", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("         Account Name: Jet Edge International LLC", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("         Account No.: 7028962376", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("         US Wire Routing No.: 042000314", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("         SWIFT Code (For Int’l Transfers) No.: FTBCUS3C", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("   (Jet Edge accepts Wire Transfers 24/7)", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.SetLeading(0.0f, 1.50f);
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);



            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("ACKNOWLEDGEMENT AND ACCEPTANCE OF QUOTE:", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("Your signature below indicates your acknowledgement and acceptance of the Quote and all above terms and conditions.", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 10;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("CLIENT: ______________________________________________________", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 20;
            cellP18.MinimumHeight = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("Print Name: __________________________ Signature: _______________________________ Date: __________", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.PaddingLeft = 4;
            cellP18.PaddingRight = 4;
            cellP18.Colspan = 10;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);


            cellP18 = new PdfPCell(new Phrase("", ArialBoldRight));
            cellP18.Border = 0;
            cellP18.Colspan = 20;
            cellP18.MinimumHeight = 45;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("", ArialBoldLeft));
            cellP18.Colspan = 10;
            cellP18.Border = 2;
            cellP18.BorderWidthLeft = 0.5f;
            cellP18.BorderWidthRight = 0.5f;
            cellP18.BorderWidthTop = 0.5f;
            cellP18.BorderWidthBottom = 0.5f;
            cellP18.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable7.AddCell(cellP18);

            cellP18 = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Arial", 8, Font.BOLD)));
            cellP18.Colspan = 10;
            cellP18.Border = 0;
            cellP18.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable7.AddCell(cellP18);

            #endregion

            PdfPTable FooterTable7 = new PdfPTable(10);
            FooterTable7.WidthPercentage = 100;
            PdfPCell CellF7 = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF7.Colspan = 5;
            CellF7.Border = 0;
            CellF7.PaddingTop = 5;
            CellF7.PaddingBottom = 5;
            CellF7.HorizontalAlignment = Element.ALIGN_LEFT;
            FooterTable7.AddCell(CellF7);

            CellF7 = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            CellF7.Colspan = 5;
            CellF7.Border = 0;
            CellF7.PaddingTop = 5;
            CellF7.PaddingBottom = 5;
            CellF7.HorizontalAlignment = Element.ALIGN_RIGHT;
            FooterTable7.AddCell(CellF7);


            ParentTable7.AddCell(SubTable7);

            pdfDoc.Add(ParentTable7);
            pdfDoc.Add(FooterTable7);

            pdfDoc.Close();

            byte[] bytes = memoryStream.ToArray();
            File.WriteAllBytes(strPath + @"\" + fileName, bytes);
            memoryStream.Close();


        }


        #region  save the PDF

        string res = SendEmail_login(strUserName, "Ticket Booking", "", strEmail);
        //  strResponse = res;

        //////delete empty leg details
        //SqlHelper.ExecuteNonQuery(SqlHelper.QuoteConnectionString, CommandType.Text, "delete from Request_Flight_06_Attach where Type='Quote Breakdown' AND reqID='" + strReqID + "'");


        ////  SqlHelper.ExecuteNonQuery(SqlHelper.QuoteConnectionString, CommandType.Text, "insert into Request_Flight_06_Attach (ReqID, ) where reqID='" + strReqID + "'");
        ////   SA.GetDataTable("select * from JE_QUOTE..Request_Flight_06_Attach Where REQID='" + lblRequestTranId.Text + "'");

        #endregion

    }
    private void Save_PDFQBD(string str, string strPath, string fileName, string strUserName, DataSet dtP)
    {

        string strlogpath = HttpContext.Current.Server.MapPath("~/Images/JE_LOGO_NEW.png");
        string strFilePath1 = HttpContext.Current.Server.MapPath("~/Images/Firstpage1.png");
        string strFilePath2 = HttpContext.Current.Server.MapPath("~/Images/FirstPage2.png");
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 8f, 8f, 10f, 10f);
        iTextSharp.text.Image myImage = iTextSharp.text.Image.GetInstance(strlogpath);
        iTextSharp.text.Image myImage1 = iTextSharp.text.Image.GetInstance(strFilePath1);
        iTextSharp.text.Image myImage2 = iTextSharp.text.Image.GetInstance(strFilePath2);

        myImage.ScaleAbsolute(100f, 90f);
        myImage1.ScaleAbsolute(150f, 60f);
        myImage2.ScaleAbsolute(120f, 60f);


        using (MemoryStream memoryStream = new MemoryStream())
        {

            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
            writer.PageEvent = new PDFFooter();
            pdfDoc.Open();
            Font ComHeader = FontFactory.GetFont("Arial", 11, Font.BOLD);
            Font verdana2 = FontFactory.GetFont("Arial", 12, Font.NORMAL);
            Font ArialBoldLeft = FontFactory.GetFont("Arial", 8, Font.BOLD);
            Font ArialBoldRight = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            Font GridHeader = FontFactory.GetFont("Arial", 7, Font.BOLD);
            Font verdana5 = FontFactory.GetFont("Arial", 7, Font.NORMAL);
            Font verdana6 = FontFactory.GetFont("Arial", 9, Font.NORMAL);

            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            StringReader sr = new StringReader(str.ToString());
            htmlparser.Parse(sr);


            float[] widths = new float[] { 1f, 2f };

            PdfPTable ParentTable = new PdfPTable(1);
            ParentTable.WidthPercentage = 100;

            ParentTable.DefaultCell.FixedHeight = 812f;
            ParentTable.DefaultCell.BorderWidth = 2;
            ParentTable.DefaultCell.BorderWidthBottom = 0.5f;
            ParentTable.DefaultCell.BorderWidthLeft = 0.5f;
            ParentTable.DefaultCell.BorderWidthRight = 0.5f;
            ParentTable.DefaultCell.BorderWidthTop = 0.5f;
            ParentTable.DefaultCell.BorderWidthBottom = 0.5f;

            #region Sub table 1

            PdfPTable SubTable1 = new PdfPTable(10);
            SubTable1.WidthPercentage = 100;
            float[] widthss = new float[] { 4f, 10f, 7f, 20f, 20f, 10f, 5f, 12f, 8f, 4f };
            SubTable1.SetWidths(widthss);

            PdfPCell cell2 = new PdfPCell(SubTable1);

            #region Header

            PdfPCell cell = new PdfPCell(myImage);
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingLeft = 5;
            cell.Rowspan = 6;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("Jet Edge International LLC", FontFactory.GetFont("Arial", 13, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 6;
            cell.PaddingBottom = 8;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            //////

            cell = new PdfPCell(new Phrase("16700C Roscoe Blvd, Van Nuys, CA 91406", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Colspan = 6;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);
            ////


            cell = new PdfPCell(new Phrase("Phone: 818-442-0096     Fax: 818-933-7442", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Colspan = 6;
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("Email: booking@flyjetedge.com", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Colspan = 6;
            cell.PaddingBottom = 5;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);
            ////

            cell = new PdfPCell(new Phrase("AIRCRAFT CHARTER INVOICE", FontFactory.GetFont("Arial", 11, Font.NORMAL)));
            cell.Colspan = 6;
            cell.PaddingBottom = 10;
            cell.BorderWidth = 0;
            cell.BorderColorLeft = BaseColor.WHITE;
            cell.BorderColorRight = BaseColor.WHITE;
            cell.BorderColorTop = BaseColor.WHITE;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("", ComHeader));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            SubTable1.AddCell(cell);
            ////

            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 10;
            cell.Border = 0;
            cell.MinimumHeight = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 10;
            cell.Border = 2;
            cell.BorderWidthLeft = 0.5f;
            cell.BorderWidthRight = 0.5f;
            cell.BorderWidthTop = 0.5f;
            cell.BorderWidthBottom = 0.5f;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #region Empty Row

            cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            cell.Colspan = 10;
            cell.Border = 0;
            cell.MinimumHeight = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion


            #endregion

            #region First Row

            cell = new PdfPCell(new Phrase("Aircraft Name", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["TailName"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("Departure Date :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DDate"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            #region Second Row

            cell = new PdfPCell(new Phrase("Start Time :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["DTime"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("End Time :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["ATime"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.Colspan = 3;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            #endregion

            //#region Third Row

            //cell = new PdfPCell(new Phrase("Address :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Address"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 3;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("Sales Person :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["SalesPerson"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 3;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            #region

            cell = new PdfPCell(new Phrase("No Of Seats :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.Colspan = 2;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["NoOFSeats"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);


            cell = new PdfPCell(new Phrase("Pricing :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            cell.Border = 0;
            cell.PaddingBottom = 5;
            cell.Colspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[0]["Pricing"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.Colspan = 3;
            cell.PaddingBottom = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("Fax :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Fax"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.Colspan = 3;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("Tail #:", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.Colspan = 2;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["TailNoInFuel"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 3;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("Email:", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Email"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 8;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            #endregion

            //#region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 10;
            //cell.Border = 0;
            //cell.MinimumHeight = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //#region sixth Row

            //cell = new PdfPCell(new Phrase("Date Of trip :", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(dtP.Tables[1].Rows[0]["Dateoftrip"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Colspan = 8;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //#region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 10;
            //cell.MinimumHeight = 5;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //#region Grid Header

            //cell = new PdfPCell(new Phrase("LEG", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("DATE", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;

            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("ETD", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("DEPARTURE CITY", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("ARRIVAL CITY", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("ETA", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("PAX", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("NAUT MILES", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("ETE", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("TZC", FontFactory.GetFont("Arial", 8, Font.BOLD, BaseColor.WHITE)));
            //cell.BackgroundColor = BaseColor.BLACK;
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.PaddingBottom = 5;
            //cell.PaddingTop = 2;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //#endregion

            //#region Grid Values

            //for (int i = 0; i < dtP.Tables[0].Rows.Count; i++)
            //{
            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["Leg"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);


            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["StartDate"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["StartTime"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);


            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["DepartCity"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["ArriveCity"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);


            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["EstEndTime"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);


            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["Passengers"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["distance_nm"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(Convert.ToDateTime(dtP.Tables[0].Rows[i]["flight_time_Hrs"].ToString()).ToString("HH:mm"), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[0].Rows[i]["offset"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //    cell.BorderColorRight = BaseColor.WHITE;
            //    cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //    cell.Border = 0;
            //    SubTable1.AddCell(cell);

            //}


            //#region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 7;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 1;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 1;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 7;
            //cell.Border = 0;
            //cell.MinimumHeight = 5;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[0].Compute("SUM(distance_nm)", "").ToString()).ToString(), ArialBoldRight));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(Convert.ToDateTime(dtP.Tables[0].Compute("SUM(flight_time_Hrs)", "").ToString()).ToString("HH:mm"), ArialBoldRight));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.MinimumHeight = 5;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.BorderColorRight = BaseColor.WHITE;
            //cell.Colspan = 1;
            //cell.MinimumHeight = 5;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable1.AddCell(cell);



            //#endregion

            //#region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 10;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //#region

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("AMOUNT", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 8;
            //cell.PaddingTop = 7;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingTop = 7;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion


            //for (int i = 0; i < dtP.Tables[2].Rows.Count; i++)
            //{
            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[2].Rows[i]["LineName"].ToString(), FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //    cell.Border = 0;
            //    cell.Colspan = 6;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[2].Rows[i]["Price"].ToString()).ToString("C", new CultureInfo("en-US")), ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}

            //#region Empty Row


            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //for (int i = 0; i < dtP.Tables[3].Rows.Count; i++)
            //{
            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(dtP.Tables[3].Rows[i]["LineName"].ToString(), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //    cell.Border = 0;
            //    cell.Colspan = 6;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[3].Rows[i]["Price"].ToString()).ToString("C", new CultureInfo("en-US")), ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 2;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //    SubTable1.AddCell(cell);

            //    cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //    cell.Border = 0;
            //    cell.Colspan = 1;
            //    cell.PaddingBottom = 5;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}

            //#region Empty Row


            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("SUBTOTAL", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 6;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[1].Compute("SUM(Price)", "").ToString()).ToString("C", new CultureInfo("en-US")), ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            //#region Empty Row


            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("Segment Tax", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 6;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("0.00", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("FET", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //cell.Border = 0;
            //cell.Colspan = 6;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("0.00", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 2;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 5;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            //#region Empty Row


            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 1;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 8;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;

            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 2;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#endregion


            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("GRAND TOTAL", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 5;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase(Convert.ToDecimal(dtP.Tables[1].Compute("SUM(Price)", "").ToString()).ToString("C", new CultureInfo("en-US")), FontFactory.GetFont("Arial", 10, Font.BOLD)));
            //cell.Border = 0;
            //cell.Colspan = 3;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable1.AddCell(cell);

            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.Colspan = 1;
            //cell.PaddingBottom = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);


            //if (dtP.Tables[1].Rows.Count > 0)
            //{
            //    if (dtP.Tables[1].Rows[0]["ExemptFlag"].ToString() == "N")
            //    {
            //        cell = new PdfPCell(new Phrase("This quote is exclusive of any applicable taxes. " + dtP.Tables[1].Rows[0]["Requester"].ToString() + " is responsible and hereby agrees to collect the FET, segment and other applicable taxes from the client and remit to the IRS.______ Initial", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //        cell.Border = 0;
            //        cell.Colspan = 10;
            //        cell.PaddingBottom = 5;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.PaddingLeft = 12;
            //        cell.PaddingRight = 12;
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //    else
            //    {
            //        cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //        cell.Border = 0;
            //        cell.Colspan = 10;
            //        cell.PaddingBottom = 10;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.PaddingLeft = 12;
            //        cell.PaddingRight = 12;
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}
            //else
            //{
            //    cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            //    cell.Border = 0;
            //    cell.Colspan = 10;
            //    cell.PaddingBottom = 10;
            //    cell.SetLeading(0.0f, 1.5f);
            //    cell.PaddingLeft = 12;
            //    cell.PaddingRight = 12;
            //    cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //    SubTable1.AddCell(cell);
            //}


            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.MinimumHeight = 8;
            //cell.Colspan = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //#region Empty Row

            //cell = new PdfPCell(new Phrase("", ArialBoldLeft));
            //cell.Colspan = 10;
            //cell.Border = 2;
            //cell.BorderWidthLeft = 0.5f;
            //cell.BorderWidthRight = 0.5f;
            //cell.BorderWidthTop = 0.5f;
            //cell.BorderWidthBottom = 0.5f;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);




            //cell = new PdfPCell(new Phrase("", ArialBoldRight));
            //cell.Border = 0;
            //cell.MinimumHeight = 8;
            //cell.Colspan = 10;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);



            //#endregion

            //cell = new PdfPCell(new Phrase("COMMENT:", FontFactory.GetFont("Arial", 9, Font.BOLD)));
            //cell.Colspan = 10;
            //cell.Border = 0;
            //cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //SubTable1.AddCell(cell);

            //if (dtP.Tables[4].Rows.Count > 0 && dtP.Tables[5].Rows.Count == 0)
            //{
            //    for (int i = 0; i < dtP.Tables[4].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("*" + dtP.Tables[4].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}
            //else if (dtP.Tables[4].Rows.Count == 0 && dtP.Tables[5].Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtP.Tables[5].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("*" + dtP.Tables[5].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}

            //else if (dtP.Tables[4].Rows.Count > 0 && dtP.Tables[5].Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtP.Tables[4].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("*" + dtP.Tables[4].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }

            //    for (int i = 0; i < dtP.Tables[5].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("**" + dtP.Tables[5].Rows[i]["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}

            //else if (dtP.Tables[4].Rows.Count == 0 && dtP.Tables[5].Rows.Count == 0)
            //{
            //    for (int i = 0; i < dtP.Tables[4].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}
            //else if (dtP.Tables[4].Rows.Count == 0 && dtP.Tables[5].Rows.Count == 1 && dtP.Tables[0].Rows.Count == 1)
            //{
            //    for (int i = 0; i < dtP.Tables[4].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.MinimumHeight = 250;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}
            //else if (dtP.Tables[4].Rows.Count == 1 && dtP.Tables[5].Rows.Count == 0 && dtP.Tables[0].Rows.Count == 1)
            //{
            //    for (int i = 0; i < dtP.Tables[4].Rows.Count; i++)
            //    {
            //        cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, Font.NORMAL)));
            //        cell.Colspan = 10;
            //        cell.Border = 0;
            //        cell.MinimumHeight = 250;
            //        cell.SetLeading(0.0f, 1.5f);
            //        cell.HorizontalAlignment = Element.ALIGN_LEFT;
            //        SubTable1.AddCell(cell);
            //    }
            //}


            //#endregion

            #endregion


            ParentTable.AddCell(SubTable1);

            pdfDoc.Add(ParentTable);

            //  pdfDoc.Add(FooterTable1);

            pdfDoc.Close();

            byte[] bytes = memoryStream.ToArray();
            File.WriteAllBytes(strPath + @"\" + fileName, bytes);
            memoryStream.Close();
        }

        #region  save the PDF


        // objRequestFlightDAL_V3.SavePDFAttachment(strReqID, "Quote Breakdown", fileName, strPath, "", struserID, strUserName);
        //////delete empty leg details
        //SqlHelper.ExecuteNonQuery(SqlHelper.QuoteConnectionString, CommandType.Text, "delete from Request_Flight_06_Attach where Type='Quote Breakdown' AND reqID='" + strReqID + "'");


        ////  SqlHelper.ExecuteNonQuery(SqlHelper.QuoteConnectionString, CommandType.Text, "insert into Request_Flight_06_Attach (ReqID, ) where reqID='" + strReqID + "'");
        ////   SA.GetDataTable("select * from JE_QUOTE..Request_Flight_06_Attach Where REQID='" + lblRequestTranId.Text + "'");

        #endregion
        SendEmail_login(strUserName, "Ticket Booking", "", strUserName);

    }
    public static string SendEmail_login(string emailTo, string subject, string body, string CurrentUser)
    {
        String[] strlist = CurrentUser.Split(new[] { '@' });
        //  string str = @"J:\JETEDGE\Vignesh\jetedge\jetedge\Uploads\Generate_Quote\" + emailTo + @"\JE_QBD_SAL_" + emailTo + "_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf";
        string str = @"D\jetedgefiles\EMPTYLEG\" + emailTo + @"\JE_QBD_SAL_" + emailTo + "_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf";

        //  string str = @"H:\Vignesh\scorecard1\EMPTYLEG\" + emailTo + @"\JE_QBD_SAL_" + emailTo + "_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf";
        // fileUrl = @"D\jetedgefiles\EMPTYLEG\" + strUserName + @"\";
        string TransactionAttchPath = SecretsBLL.PDFWRITE;

        SmtpClient mailClient = new SmtpClient("localhost");

        mailClient.UseDefaultCredentials = true;

        MailMessage mailDetail = new MailMessage();
        mailDetail.IsBodyHtml = true;
        mailDetail.From = new MailAddress("booking@flyjetedge.com");
        foreach (var address in emailTo.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
        {
            mailDetail.To.Add(CurrentUser);
        }
        //mailDetail.To.Add(new MailAddress("vignesh.murugan@sankarasoftware.com"));
        //mailDetail.To.Add(new MailAddress("debriefteam@allegroresponse.com"));
        // mailDetail.CC.Add(new MailAddress(CurrentUser));
        // mailDetail.CC.Add(new MailAddress("vignesh.murugan@sankarasoftware.com"));
        mailDetail.Bcc.Add(new MailAddress("sakthi@xturion.com"));
        //mailDetail.Bcc.Add(new MailAddress("sakthi@xturion.com"));
        mailDetail.Subject = subject;
        mailDetail.Body = body;
        // string str = string.Empty;
        //if (HttpContext.Current.Request.Url.Host == "localhost")
        //{
        //   //Attachment at = new Attachment("http://localhost:50194/jetedge/Uploads/Generate_Quote" + @"/" + emailTo + @"/JE_QBD_SAL_" + emailTo + "_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf");
        //    str="http://localhost:50194/jetedge/Uploads/Generate_Quote" + @"/" + emailTo + @"/JE_QBD_SAL_" + emailTo + "_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf";
        //  //  mailDetail.Attachments.Add(at);
        //  //  dtRow["ImagePath"] = "http://localhost:50194/jetedge/Uploads/TailImages/" + lblTailId.Text + "/" + dtRow["ImageName"].ToString();
        //   // J:\JETEDGE\Vignesh\jetedge\jetedge\Uploads\Generate_Quote\TomTim

        //}
        //else
        //{
        //   // Attachment at = new Attachment("https://www.flyjetedge.net/JETETRIPS/Uploads/Generate_Quote/" + @"/" + emailTo + @"/JE_QBD_SAL_" + emailTo + "_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".pdf");
        //   // mailDetail.Attachments.Add(at);
        //  //  dtRow["ImagePath"] = "https://www.flyjetedge.net/JETETRIPS/Uploads/TailImages/" + lblTailId.Text + "/" + dtRow["ImageName"].ToString();
        //}


        Attachment at = new Attachment(str);
        mailDetail.Attachments.Add(at);

        mailClient.Send(mailDetail);
        // strResponse = "";
        return "Yes";
    }

    public class PDFFooter : PdfPageEventHelper
    {
        // write on top of document
        //public override void OnOpenDocument(PdfWriter writer, Document document)
        //{
        //    base.OnOpenDocument(writer, document);
        //    PdfPTable tabFot = new PdfPTable(new float[] { 1F });
        //    tabFot.SpacingAfter = 10F;
        //    PdfPCell cell;
        //    tabFot.TotalWidth = 500F;
        //    cell = new PdfPCell(new Phrase("Header"));
        //    tabFot.AddCell(cell);
        //    tabFot.WriteSelectedRows(0, 10, 5, document.Top, writer.DirectContent);
        //}

        // write on start of each page
        //public override void OnStartPage(PdfWriter writer, Document document)
        //{
        //    base.OnStartPage(writer, document);
        //}

        // write on end of each page

        public override void OnEndPage(PdfWriter writer, Document document)
        {


            var footer1 = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/Images/Firstpage1.png"));
            var footer2 = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/Images/FirstPage2.png"));
            footer1.ScaleAbsolute(150f, 60f);
            footer2.ScaleAbsolute(120f, 60f);
            base.OnEndPage(writer, document);

            PdfPTable tabFot = new PdfPTable(10);
            PdfPCell cell;
            tabFot.TotalWidth = 580F;
            tabFot.WidthPercentage = 100;


            cell = new PdfPCell(footer1);
            cell.Colspan = 3;
            cell.PaddingLeft = 10;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            tabFot.AddCell(cell);

            cell = new PdfPCell(new Phrase("THANK YOU FOR THE OPPORTUNITY TO BOOK YOUR TRIP", FontFactory.GetFont("Arial", 7, Font.NORMAL)));
            cell.Colspan = 4;
            cell.PaddingTop = 30;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            tabFot.AddCell(cell);


            cell = new PdfPCell(footer2);
            cell.Colspan = 3;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            tabFot.AddCell(cell);

            cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 7, Font.NORMAL)));
            cell.Colspan = 10;
            cell.Border = 0;
            cell.MinimumHeight = 3;
            cell.GetLeft(30);
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            tabFot.AddCell(cell);

            cell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 7, Font.NORMAL)));
            cell.Colspan = 10;
            cell.Border = 1;
            cell.PaddingLeft = 20;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            tabFot.AddCell(cell);

            cell = new PdfPCell(new Phrase("Jet Edge International", FontFactory.GetFont("Arial", 8, Font.BOLD)));
            cell.Colspan = 10;
            cell.PaddingTop = 2;
            cell.PaddingBottom = 5;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            tabFot.AddCell(cell);



            cell = new PdfPCell(new Phrase("(Jet Edge Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.Colspan = 5;
            cell.PaddingLeft = 5;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            tabFot.AddCell(cell);

            cell = new PdfPCell(new Phrase("PRINTED :   " + System.DateTime.Now.ToString("dd MMM yyyy HH:mm"), FontFactory.GetFont("Arial", 9, Font.NORMAL)));
            cell.Border = 0;
            cell.Colspan = 5;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            tabFot.AddCell(cell);
            tabFot.WriteSelectedRows(0, 10, 8, document.GetBottom(90), writer.DirectContent);
        }

        //write on close of document
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
        }
    }

}

