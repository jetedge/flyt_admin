﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Data.Common;
using System.Configuration;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
/// <summary>
/// Summary description for ManagementClass
/// </summary>
public class CommanClass
{

    public static void bindDropdown(DataTable dtSource, DropDownList ddlSource, string strTextField, string strValueField, string strAll)
    {
        ddlSource.Items.Clear();
        if (dtSource.Rows.Count > 0)
        {
            ddlSource.DataSource = dtSource;
            ddlSource.DataTextField = strTextField;
            ddlSource.DataValueField = strValueField;
            ddlSource.DataBind();
            ddlSource.Items.Insert(0, new ListItem("ALL", "0"));
        }
        else
        {
            ddlSource.Items.Insert(0, new ListItem("ALL", "0"));
        }
    }
    public static void bindDropdown(DataTable dtSource, DropDownList ddlSource, string strTextField, string strValueField)
    {

        ddlSource.Items.Clear();
        if (dtSource.Rows.Count > 0)
        {
            ddlSource.DataSource = dtSource;
            ddlSource.DataTextField = strTextField;
            ddlSource.DataValueField = strValueField;
            ddlSource.DataBind();
            ddlSource.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        else
        {
            ddlSource.Items.Insert(0, new ListItem("Not Available", "0"));
        }

    }




    public static void bindCheckBox(DataTable dtSource, CheckBoxList ddlSource, string strTextField, string strValueField)
    {

        ddlSource.Items.Clear();
        if (dtSource.Rows.Count > 0)
        {
            ddlSource.DataSource = dtSource;
            ddlSource.DataTextField = strTextField;
            ddlSource.DataValueField = strValueField;
            ddlSource.DataBind();
        }

    }



    public static object GetUrl(string domain)
    {
        if (domain == "localhost")
            return @"D:\JetedgeFiles\";
        else
            return @"D:\JetedgeFiles\";
    }


    public static void AddColor(GridView gv, int column, int Row)
    {
        gv.Rows[Row].Cells[column].BackColor = System.Drawing.ColorTranslator.FromHtml("#444");
        gv.Rows[Row].Cells[column].ForeColor = System.Drawing.ColorTranslator.FromHtml("#ec971f");
        gv.Rows[Row].Cells[column].Font.Bold = true;
    }

    public static void AddGrandColor(GridView gv, int column, int Row)
    {
        gv.Rows[Row].Cells[column].BackColor = System.Drawing.ColorTranslator.FromHtml("#333");
        gv.Rows[Row].Cells[column].Font.Bold = true;
        // gv.Rows[Row].Cells[column].HorizontalAlign = HorizontalAlign.Right;
    }

    public bool chkNumeric(string input)
    {
        return Regex.IsMatch(input, @"^\d+$");
    }

    public static void formatPer(Label lbl)
    {
        if (lbl.Text.Contains("-"))
        {
            lbl.Text = lbl.Text.Replace("-", "(").Replace(" %", ") %");
        }
    }



    public static void getDates(string strMonth, string strPeriod, out string strFromDate, out string strToDate)
    {
        strFromDate = null;
        strToDate = null;

        if (strPeriod == "1")
        {
            strFromDate = Convert.ToDateTime(strMonth).ToString("MM/dd/yyyy");
            strToDate = new DateTime(Convert.ToDateTime(strMonth).Year, Convert.ToDateTime(strMonth).Month, 15).ToString("MM/dd/yyyy");
        }
        else
        {
            strFromDate = new DateTime(Convert.ToDateTime(strMonth).Year, Convert.ToDateTime(strMonth).Month, 16).ToString("MM/dd/yyyy");
            strToDate = new DateTime(Convert.ToDateTime(strMonth).Year, Convert.ToDateTime(strMonth).Month, 1).AddMonths(1).AddDays(-1).ToString("MM/dd/yyyy");
        }
    }

    public static void getNPDates(string strMonth, string strPeriod, out string strFromDate, out string strToDate)
    {
        strFromDate = null;
        strToDate = null;

        if (strPeriod == "1")
        {
            strFromDate = new DateTime(Convert.ToDateTime(strMonth).Year, Convert.ToDateTime(strMonth).Month, 16).ToString("MM/dd/yyyy");
            strToDate = new DateTime(Convert.ToDateTime(strMonth).Year, Convert.ToDateTime(strMonth).Month, 1).AddMonths(1).AddDays(-1).ToString("MM/dd/yyyy");


        }
        else
        {
            strFromDate = new DateTime(Convert.ToDateTime(strMonth).AddMonths(1).Year, Convert.ToDateTime(strMonth).AddMonths(1).Month, 1).ToString("MM/dd/yyyy");
            strToDate = new DateTime(Convert.ToDateTime(strFromDate).Year, Convert.ToDateTime(strFromDate).Month, 15).ToString("MM/dd/yyyy");
        }
    }
}