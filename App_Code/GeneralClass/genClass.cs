﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Configuration;
using System.Web.UI;
using System.Net.Mail;
using System.Net.Mime;
using Microsoft.Exchange.WebServices.Data;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for genClass
/// </summary>
public class genclass
{
    /// <summary>
    /// Input : Query
    /// OutPut : It will return a dataset
    /// </summary>
    /// <param name="SQLQuery"></param>
    /// <returns></returns>

    #region To convert a string to Uppercase and to Trim

    public static string StringUpperTrim(string str)
    {
        return str.ToString().ToUpper().Replace(" ", "");
    }

    #endregion
    public static string SpecialCharReplacetoDB(string iChar)
    {
        string ReplaceChar = iChar.Replace("'", "''");
        return ReplaceChar;
    }

    public static string SpecialCharReplacefromDB(string iChar)
    {
        string ReplaceChar = iChar.Replace("''", "'").Replace(" ", "");
        return ReplaceChar;
    }

    public static string LogException_New(Exception ex, int linenum, string MethodName)
    {

        StringBuilder sbExceptionMessage = new StringBuilder();

        sbExceptionMessage.Append("Error: ");
        sbExceptionMessage.Append(ex.Message);
        sbExceptionMessage.Append("\\n\\nAn error occured in Function: ");
        sbExceptionMessage.Append(MethodName);
        sbExceptionMessage.Append("\\nLine: ");
        sbExceptionMessage.Append(linenum);
        sbExceptionMessage.Replace("'", "").Replace("\n", "\\n").Replace("\r", "\\r").ToString();

        return sbExceptionMessage.Replace("'", "").Replace("\n", "\\n").Replace("\r", "\\r").ToString();
    }

    public static object ExecuteScalar(string SQLQuery)
    {
        object obj = null;
        obj = SqlHelper.ExecuteScalar(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["FETConnection"].ConnectionString), CommandType.Text, SQLQuery);
        return obj;
    }
    public static string Decrypt(string hash, bool hashKey = true)
    {
        string key = "MYENC1005";
        if (hash == null || key == null)
            return null;

        var keyArray = HexStringToByteArray(hashKey ? HashSHA256(key) : key);
        var toEncryptArray = HexStringToByteArray(hash);

        var aes = new AesCryptoServiceProvider
        {
            Key = keyArray,
            Mode = CipherMode.ECB,
            Padding = PaddingMode.PKCS7
        };

        var cTransform = aes.CreateDecryptor();
        var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

        aes.Clear();
        return Encoding.UTF8.GetString(resultArray);
    }

    public static DataTable GetDataTable(string SQLQuery)
    {
        SqlConnection Conn = new SqlConnection(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["ETripConnection"].ConnectionString));
        try
        {
            if (Conn.State != ConnectionState.Open)
            {
                Conn.Open();
            }
            SqlDataAdapter adp = new SqlDataAdapter(SQLQuery.Trim(), Conn);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            return dt;
        }
        finally
        {
            Conn.Close();
        }
    }

    public static DataTable GetDataTable_Mgnt(string SQLQuery)
    {
        SqlConnection Conn = new SqlConnection(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["ETripConnection"].ConnectionString));
        try
        {
            if (Conn.State != ConnectionState.Open)
            {
                Conn.Open();
            }
            SqlDataAdapter adp = new SqlDataAdapter(SQLQuery.Trim(), Conn);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            return dt;
        }
        finally
        {
            Conn.Close();
        }
    }

    public static DataTable GetDataTableDOMGNT(string SQLQuery)
    {
        SqlConnection Conn = new SqlConnection(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["DOCMGNTConnection"].ConnectionString));
        try
        {
            if (Conn.State != ConnectionState.Open)
            {
                Conn.Open();
            }
            SqlDataAdapter adp = new SqlDataAdapter(SQLQuery.Trim(), Conn);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            return dt;
        }
        finally
        {
            Conn.Close();
        }
    }

    public static DataTable GetDataTableMain(string SQLQuery)
    {
        SqlConnection Conn = new SqlConnection(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["MAINConnection"].ConnectionString));
        try
        {
            if (Conn.State != ConnectionState.Open)
            {
                Conn.Open();
            }
            SqlDataAdapter adp = new SqlDataAdapter(SQLQuery.Trim(), Conn);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            return dt;
        }
        finally
        {
            Conn.Close();
        }
    }

    public static DataTable GetDataTableMgnt(string SQLQuery)
    {
        SqlConnection Conn = new SqlConnection(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["MGNTConnection"].ConnectionString));
        try
        {
            if (Conn.State != ConnectionState.Open)
            {
                Conn.Open();
            }
            SqlDataAdapter adp = new SqlDataAdapter(SQLQuery.Trim(), Conn);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            return dt;
        }
        finally
        {
            Conn.Close();
        }
    }

    public static DataTable GetDataTableBuyer(string SQLQuery)
    {
        SqlConnection Conn = new SqlConnection(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["JE_BUYER"].ConnectionString));
        try
        {
            if (Conn.State != ConnectionState.Open)
            {
                Conn.Open();
            }
            SqlDataAdapter adp = new SqlDataAdapter(SQLQuery.Trim(), Conn);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            return dt;
        }
        finally
        {
            Conn.Close();
        }
    }



    public static string validateTrialkey(string strTrialKey, string key)
    {
        byte[] inputbytes = System.Text.Encoding.ASCII.GetBytes(strTrialKey);

        System.Security.Cryptography.TripleDESCryptoServiceProvider des = new System.Security.Cryptography.TripleDESCryptoServiceProvider();

        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

        des.Key = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(key));

        des.Mode = System.Security.Cryptography.CipherMode.ECB;
        // create encryptor
        System.Security.Cryptography.ICryptoTransform encrypt = des.CreateEncryptor();

        byte[] output = encrypt.TransformFinalBlock(inputbytes, 0, inputbytes.Length);
        string strencryptionkey = Convert.ToBase64String(output);
        return strencryptionkey;
    }

    public static DataSet getDataSet(string SQLQuery)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["FETConnection"].ConnectionString), SQLQuery);
        return ds;
    }
    //--------------------------------method to give alert message----------------------
    public static void Alert(Page varPage, string varAlertMsg)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script language=JavaScript>alert('" + varAlertMsg.Trim() + "');");
        sb.Append("</script>");
        string ab = @"""";
        ScriptManager.RegisterClientScriptBlock(varPage, varPage.GetType(), Guid.NewGuid().ToString(), "alert('" + varAlertMsg.Replace("'", ab).Replace("\n", "\\n").Replace("\r", "\\r") + "');", true);
    }
    //-------------------------------------------end-------------------------------------
    public static DataTable GetClient()
    {
        DataTable dt = new DataTable();
        string strSQL = string.Empty;
        if (HttpContext.Current.Session["User"].ToString() == "1")
        {
            strSQL = "Select distinct ClientID,ClientName from ClientMaster where Active='1'  order by ClientName ";
        }
        else
        {
            strSQL = "Select distinct a.ClientID,b.ClientName from UserClientMapping a,ClientMaster b   " +
                     " where a.ClientID in (Select distinct clientID from UserClientMapping where UserID ='" + HttpContext.Current.Session["User"].ToString() + "') and b.Active='1' " +
                     " and UserID not in (" + HttpContext.Current.Session["User"].ToString() + ") " +
                     " and a.ClientID = b.ClientID order by ClientName";
        }
        dt = GetDataTable(strSQL);
        return dt;
    }

    public static string EncryptData(string givenValue)
    {
        string password = "SECRETONE";
        TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
        MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

        byte[] pwdhash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));
        hashmd5 = null;
        des.Key = pwdhash;
        des.Mode = CipherMode.ECB;

        byte[] buff = ASCIIEncoding.ASCII.GetBytes(givenValue);
        return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buff, 0, buff.Length));

    }

    public static string DecryptData(string givenValue)
    {
        if (givenValue != null && givenValue != string.Empty)
        {
            string password = "SECRETONE";
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();

            byte[] pwdhash = hashmd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));
            hashmd5 = null;
            des.Key = pwdhash;
            des.Mode = CipherMode.ECB;

            byte[] buff = Convert.FromBase64String(givenValue);
            return ASCIIEncoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buff, 0, buff.Length));
        }
        else
        {
            return string.Empty;
        }
    }

    public static void FillControl(DropDownList ddlId, string sqlQuery)
    {
        ddlId.Items.Clear();
        ArrayList myList = new ArrayList();
        SqlDataReader dr;
        dr = SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, sqlQuery);

        if (dr.HasRows)
        {
            while (dr.Read())
            {
                myList.Add(new string[] { dr[0].ToString(), dr[1].ToString() });
            }

            dr.Close();

            foreach (string[] str in myList)
            {
                ddlId.Items.Add(new ListItem(str[1], str[0]));
            }
            ddlId.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        else
        {
            ddlId.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    public static void FillControlPL(DropDownList ddlId, string sqlQuery)
    {
        ddlId.Items.Clear();
        ArrayList myList = new ArrayList();
        SqlDataReader dr;
        dr = SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, sqlQuery);

        if (dr.HasRows)
        {
            while (dr.Read())
            {
                myList.Add(new string[] { dr[0].ToString(), dr[1].ToString() });
            }

            dr.Close();

            foreach (string[] str in myList)
            {
                ddlId.Items.Add(new ListItem(str[1], str[0]));
            }
            ddlId.Items.Insert(0, new ListItem("ALL", "0"));
        }
        else
        {
            ddlId.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    public static void FillControl(DropDownList ddlId, string sqlQuery, string ALL)
    {
        ddlId.Items.Clear();
        ArrayList myList = new ArrayList();
        SqlDataReader dr;
        dr = SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, sqlQuery);

        if (dr.HasRows)
        {
            while (dr.Read())
            {
                myList.Add(new string[] { dr[0].ToString(), dr[1].ToString() });
            }

            dr.Close();

            foreach (string[] str in myList)
            {
                ddlId.Items.Add(new ListItem(str[1], str[0]));
            }
            ddlId.Items.Insert(0, new ListItem("ALL", "0"));
        }
        else
        {
            ddlId.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    public static void FillControl(ListBox ddlId, string sqlQuery)
    {
        ddlId.Items.Clear();
        ArrayList myList = new ArrayList();
        SqlDataReader dr;
        dr = SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, sqlQuery);

        if (dr.HasRows)
        {
            while (dr.Read())
            {
                myList.Add(new string[] { dr[0].ToString(), dr[1].ToString() });
            }

            dr.Close();

            foreach (string[] str in myList)
            {
                ddlId.Items.Add(new ListItem(str[1], str[0]));
            }

        }
        else
        {
            ddlId.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    // --------------- Get Client Information Based on Logged user ---------------------------
    public static string GetClientString()
    {
        // Get Client Info
        string strClients = string.Empty;
        DataTable dtClient = new DataTable();
        dtClient = GetClient();

        for (int i = 0; i < dtClient.Rows.Count; i++)
        {
            if (i == 0)
            {
                strClients = dtClient.Rows[i]["ClientID"].ToString();
            }
            else
            {
                strClients += "," + dtClient.Rows[i]["ClientID"].ToString();
            }
        }
        return strClients;
    }
    // ------------------------------ Ends Here-----------------------------------------------
    public static DataTable GetProjects()
    {
        string strSQL = string.Empty;
        DataTable dt = new DataTable();
        DataTable dtClient = new DataTable();

        if (HttpContext.Current.Session["User"].ToString() == "1")
        {
            strSQL = "Select ProjectID,ProjectName from ProjectMaster where " +
                     " Active='1' order by ProjectName";
        }
        else
        {
            // Get Client Info
            dtClient = GetClient();
            string strClients = string.Empty;
            for (int i = 0; i < dtClient.Rows.Count; i++)
            {
                if (i == 0)
                {
                    strClients = dtClient.Rows[0]["ClientID"].ToString();
                }
                else
                {
                    strClients += "," + dtClient.Rows[0]["ClientID"].ToString();
                }
            }
            // Get Project Info
            if (strClients.Length > 0)
            {
                strSQL = "Select ProjectID,ProjectName from ProjectMaster where ClientID in(" + strClients + ") " +
                                " and Active='1' order by ProjectName";
            }
        }
        if (strSQL.Length > 0)
        {
            dt = GetDataTable(strSQL);
        }
        return dt;
    }

    #region Getting password from database by passing username

    public static string GetPassword(string Email)
    {
        return SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "select KeyCode from RS_UserMaster WHERE U_Email='" + Email + "' ").ToString();
    }

    #endregion

    #region Getting password from database by passing username

    public static string ForgotPassword(string Email)
    {
        try
        {
            string obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "select password from Users where EMAILID= '" + Email + "' ").ToString();
            if (obj == null)
                return string.Empty;
            else
                return obj.ToString();
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion

    #region Getting LogException
    public static string LogException(Exception ex)
    {
        System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        StringBuilder sbExceptionMessage = new StringBuilder();

        sbExceptionMessage.Append("Error: ");
        sbExceptionMessage.Append(ex.Message);
        sbExceptionMessage.Append("\\n\\nAn error occured in Function: ");
        sbExceptionMessage.Append(trace.GetFrame(0).GetMethod().Name);
        sbExceptionMessage.Append("\\nLine: ");
        sbExceptionMessage.Append(trace.GetFrame(0).GetFileLineNumber());
        return sbExceptionMessage.Replace("'", "").Replace("\n", "\\n").Replace("\r", "\\r").ToString();
    }
    #endregion

    public static string getMonthFUEL()
    {
        return genclass.ExecuteScalar("SELECT max (TRXDATE) as Month FROM FuelTaxTran").ToString();
    }

    public static string getMonthPilot()
    {
        return genclass.ExecuteScalar("select Max(BeginDate) from Crew_Posting_Tran").ToString();
    }

    //Prem Added For Phone Number Format
    public static string PhoneNumber(string value)
    {
        value = new System.Text.RegularExpressions.Regex(@"\D")
            .Replace(value, string.Empty);
        value = value.TrimStart('0');
        if (value.Length == 7)
            return Convert.ToInt64(value).ToString("###-####");
        if (value.Length == 8)
            return Convert.ToInt64(value).ToString("(###)-###-##");
        if (value.Length == 9)
            return Convert.ToInt64(value).ToString("(###)-###-###");
        if (value.Length == 10)
            return Convert.ToInt64(value).ToString("(###)-###-####");

        //if (value.Length > 10)
        //    return Convert.ToInt64(value)
        //        .ToString("###-###-#### " + new String('#', (value.Length - 10)));
        return value;
    }

    public static string Get_MailConfig()
    {
        object obj = null;
        obj = SqlHelper.ExecuteScalar(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["MGNTConnection"].ConnectionString), CommandType.Text, "Select MailFlag FROM GlobalConfig");
        return obj.ToString();
    }

    public static string Get_EnableSMS()
    {
        object obj = null;
        obj = SqlHelper.ExecuteScalar(genclass.Decrypt(WebConfigurationManager.ConnectionStrings["MGNTConnection"].ConnectionString), CommandType.Text, "Select SmsFlag FROM GlobalConfig");
        return obj.ToString();
    }

    public static int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }

    public static string RandomString(int length)
    {
        string allowedLetterChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
        string allowedNumberChars = "23456789";
        char[] chars = new char[length];
        Random rd = new Random();

        bool useLetter = true;
        for (int i = 0; i < length; i++)
        {
            if (useLetter)
            {
                chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)];
                useLetter = false;
            }
            else
            {
                chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                useLetter = true;
            }
        }

        return new string(chars);
    }

    public static string Encrypt(string phrase, bool hashKey = true)
    {
        string key = "MYENC1005";
        if (phrase == null || key == null)
            return null;

        var keyArray = HexStringToByteArray(hashKey ? HashSHA256(key) : key);
        var toEncryptArray = Encoding.UTF8.GetBytes(phrase);
        byte[] result;

        using (var aes = new AesCryptoServiceProvider
        {
            Key = keyArray,
            Mode = CipherMode.ECB,
            Padding = PaddingMode.PKCS7
        })
        {
            var cTransform = aes.CreateEncryptor();
            result = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            aes.Clear();
        }
        return ByteArrayToHexString(result);
    }
    internal static byte[] HexStringToByteArray(string inputString)
    {
        if (inputString == null)
            return null;

        if (inputString.Length == 0)
            return new byte[0];

        if (inputString.Length % 2 != 0)
            throw new Exception("Hex strings have an even number of characters and you have got an odd number of characters!");

        var num = inputString.Length / 2;
        var bytes = new byte[num];
        for (var i = 0; i < num; i++)
        {
            var x = inputString.Substring(i * 2, 2);
            try
            {
                bytes[i] = Convert.ToByte(x, 16);
            }
            catch (Exception ex)
            {
                throw new Exception("Part of your \"hex\" string contains a non-hex value.", ex);
            }
        }
        return bytes;
    }

    public static string HashSHA256(string phrase)
    {
        if (phrase == null)
            return null;
        var encoder = new UTF8Encoding();
        var sha256Hasher = new SHA256CryptoServiceProvider();
        var hashedDataBytes = sha256Hasher.ComputeHash(encoder.GetBytes(phrase));
        return ByteArrayToHexString(hashedDataBytes);
    }

    internal static string ByteArrayToHexString(byte[] inputArray)
    {
        if (inputArray == null)
            return null;
        var o = new StringBuilder("");
        for (var i = 0; i < inputArray.Length; i++)
            o.Append(inputArray[i].ToString("X2"));
        return o.ToString();
    }

    public static string RandomStringNumbers(int length)
    {
        string allowedLetterChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
        string allowedNumberChars = "0123456789";
        char[] chars = new char[length];
        Random rd = new Random();

        bool useLetter = false;
        for (int i = 0; i < length; i++)
        {
            if (useLetter)
            {
                chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)];
                useLetter = false;
            }
            else
            {
                chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                //useLetter = true;
            }
        }

        return new string(chars);
    }


    public static string SendEmail_login(string emailTo, string subject, string body)
    {
        SmtpClient mailClient = new SmtpClient("localhost");
        mailClient.UseDefaultCredentials = true;

        MailMessage mailDetail = new MailMessage();
        mailDetail.IsBodyHtml = true;
        mailDetail.From = new MailAddress("zoom@flyjetedge.com");
        mailDetail.To.Add(new MailAddress(emailTo));
        // mailDetail.CC.Add(new MailAddress(strCCEmail));
        mailDetail.Bcc.Add("premkumar@sankarasoftware.com");

        mailDetail.Subject = subject;
        mailDetail.Body = body;

        mailClient.Send(mailDetail);
        return "Yes";
    }

    public static string SendEmail_Office365(string strEmail, string strCC, string subject, string EmailBody)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
        ExchangeService myservice = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
        myservice.Credentials = new WebCredentials(SecretsBLL.SenderEmailid, SecretsBLL.Password);

        string serviceUrl = SecretsBLL.Office365WebserivceURL;
        myservice.Url = new Uri(serviceUrl);
        EmailMessage emailMessage = new EmailMessage(myservice);

        emailMessage.Subject = subject;
        emailMessage.Body = new MessageBody(EmailBody);
        emailMessage.ToRecipients.Add(strEmail);
        if (strCC.Length > 0)
        {
            emailMessage.CcRecipients.Add(strCC);
        }
        emailMessage.BccRecipients.Add("premkumar@sankarasoftware.com");

        emailMessage.Send();

        return "Yes";
    }

    public static object fun_Get_ScreenName(string TransId, string moduleName, string ScreenName)
    {
        string Time = System.DateTime.Now.ToString();
        return SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into Log_Audit_Screens ([logAuditRowId] , [module] , [ScreenName]  , [VisitedTime]) values ('" + TransId + "' , '" + moduleName + "' , '" + ScreenName + "' , '" + Time + "') ");
    }

    public static void Bind_Country_Dropdown(DropDownList ddlCountry)
    {
        DataTable dtCountry = new DataTable();
        SqlParameter[] Country_Params = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", false);
        Country_Params[0].Value = "COU";
        dtCountry = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", Country_Params).Tables[0];

        ddlCountry.Items.Clear();
        if (dtCountry.Rows.Count > 0)
        {
            ddlCountry.DataSource = dtCountry;
            ddlCountry.DataTextField = "Country_Name";
            ddlCountry.DataValueField = "Row_Id";
            ddlCountry.DataBind();
        }
        else
        {
            ddlCountry.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    public static DataTable Bind_State_Dropdown(string CountryId)
    {
        DataTable dtCountry = new DataTable();
        SqlParameter[] Country_Params = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", false);
        Country_Params[0].Value = "STA";
        Country_Params[1].Value = CountryId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", Country_Params).Tables[0];
    }

    public static void Bind_PhoneCode_Dropdown(DropDownList ddlPhoneCode, string Country)
    {
        DataTable dtPhoneCode = new DataTable();
        SqlParameter[] PhoneCode_Params = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", false);
        PhoneCode_Params[0].Value = "PHO";
        PhoneCode_Params[1].Value = Country;
        dtPhoneCode = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", PhoneCode_Params).Tables[0];

        ddlPhoneCode.Items.Clear();
        if (dtPhoneCode.Rows.Count > 0)
        {
            DataRow[] myResultSet = dtPhoneCode.Select("PhoneCode is not null");
            if (myResultSet.Length > 0)
            {
                DataTable dtNew = myResultSet.CopyToDataTable();

                DataView dv = dtNew.DefaultView;
                dv.Sort = "Country_Name asc";
                DataTable sortedDT = dv.ToTable();

                ddlPhoneCode.DataSource = sortedDT;
                ddlPhoneCode.DataTextField = "NewPhoneCode";
                ddlPhoneCode.DataValueField = "PhoneCode";
                ddlPhoneCode.DataBind();
            }

            ddlPhoneCode.Items.Insert(0, new ListItem("Phone Code", "0"));
        }
        else
        {
            ddlPhoneCode.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    public static DataTable GetZoomAirports(string strManageType)
    {
        SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_Zoom_Aiports]", false);
        getAllTripParm[0].Value = strManageType;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.StoredProcedure, "[SP_Zoom_Aiports]", getAllTripParm).Tables[0];
    }


    // Date Format

    public static string GetDateFormat(string strDate)
    {
        return Convert.ToDateTime(strDate).ToString("MMM dd, yyyy hh:mm tt");
    }


    public static string GETDATE(string strDate)
    {
        return Convert.ToDateTime(strDate).ToString("MMM dd, yyyy hh:mm tt");
    }

    public static string Long_Date(DateTime inputDate)
    {
        return inputDate.ToString("MMMM d, yyyy");
    }
    public static string Long_Date_Time(DateTime inputDate)
    {
        return inputDate.ToString("MMMM d, yyyy h:mm tt");
    }
    public static string Time_Format(DateTime inputDate)
    {
        return inputDate.ToString("h:mm tt");
    }
    public static string Short_Date(DateTime inputDate)
    {
        return inputDate.ToString("MMMM d");
    }

    public static string Format_Phone(string PhoneNumber)
    {
        string strResult = string.Empty;
        if (PhoneNumber.Length == 10)
        {
            strResult = Regex.Replace(PhoneNumber, @"(\d{3})(\d{3})(\d{4})", "$1.$2.$3");
        }
        else
        {
            strResult = PhoneNumber;
        }

        return strResult;
    }

    public static string Authorization_Code_Grant()
    {
        DataTable dtAuthorizationCode = new DataTable();
        string strResult = string.Empty;

        SqlParameter[] Code_Grant_Params = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_Authorization_Code_Grant", false);
        Code_Grant_Params[0].Value = "SEL";
        dtAuthorizationCode = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_Authorization_Code_Grant", Code_Grant_Params).Tables[0];

        if (dtAuthorizationCode.Rows.Count > 0)
        {
            strResult = dtAuthorizationCode.Rows[0]["access_token"].ToString();
        }
        return strResult;
    }

    public static DataTable Bind_Airports_Dropdown(string CountryId)
    {
        DataTable dtCountry = new DataTable();
        SqlParameter[] Country_Params = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", false);
        Country_Params[0].Value = "AIR";
        Country_Params[1].Value = CountryId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "Bind_Country_Dropdown", Country_Params).Tables[0];
    }

    /// <summary>
    /// At least onelower case letter,
    /// At least oneupper case letter,
    /// Not include special character,
    /// At least onenumber
    /// At least 8 characters length
    /// </summary>
    /// 
    public static bool ValidatePassword(string passWord)
    {
        //var hasNumber = new Regex(@"[0-9]+");
        //var hasUpperChar = new Regex(@"[A-Z]+");
        //var hasMinimum8Chars = new Regex(@".{8,}");
        //var hasLowerChar = new Regex(@"[a-z]+");
        //var hasNonSpecial = new Regex(@"[~`!@#$%^&*()+=[{]}\|;:',<.>/?_-]+");

        //var isValidated = hasNumber.IsMatch(txtNew_Password.Text) && hasUpperChar.IsMatch(txtNew_Password.Text) &&
        //                  hasLowerChar.IsMatch(txtNew_Password.Text) && hasNonSpecial.IsMatch(txtNew_Password.Text) &&
        //                  hasMinimum8Chars.IsMatch(txtNew_Password.Text);

        int validConditions = 0;
        foreach (char c in passWord)
        {
            if (c >= 'a' && c <= 'z')
            {
                validConditions++;
                break;
            }
        }
        foreach (char c in passWord)
        {
            if (c >= 'A' && c <= 'Z')
            {
                validConditions++;
                break;
            }
        }
        if (validConditions == 0) return false;
        foreach (char c in passWord)
        {
            if (c >= '0' && c <= '9')
            {
                validConditions++;
                break;
            }
        }
        if (validConditions == 1) return false;
        if (validConditions == 2)
        {
            char[] special = { '%', '$', '#', '@', '`', '~', '!', '-', '_', '?', '/', '>', '.', '<', ',', ':', ';', '|', '}', ']', '{', ']', '=', '+', '(', ')', '*', '&', '^' }; // or whatever    
            if (passWord.IndexOfAny(special) != -1)
            {
                return false;
            }
            else if (passWord.IndexOfAny(special) == -1 && validConditions == 2)
            {
                return false;
            }
        }
        return true;
    }

    public static string GetPhoneCode(string strCountry)
    {
        string strPCode = string.Empty;
        DataTable dtPhoneCode = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select PhoneCode from JETEDGE_FUEL..Country where Row_ID='" + strCountry + "'").Tables[0];

        if (dtPhoneCode.Rows.Count > 0)
        {
            strPCode = dtPhoneCode.Rows[0]["PhoneCode"].ToString().Trim();
        }

        return strPCode;
    }
}
