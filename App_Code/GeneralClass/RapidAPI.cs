﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Reflection;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Data.SqlClient;


public class RapidAPI
{
    public DataTable FlightHourCalculation(string StartAirport, string strEndAirport, string strTailNo, string TimeOfDeparture,
        string strSpeed, string strFlag)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
        DataTable dtResul = new DataTable();
        LegDetails leg = new LegDetails();
        List<LegDetails> RouteListLeg = new List<LegDetails>();


        if (strFlag == "GC")
        {
            #region Great Circle API

            string strRoot = StartAirport + "-" + strEndAirport;

            string WEBSERVICE_URL = "https://greatcirclemapper.p.rapidapi.com/airports/route/";
            WEBSERVICE_URL = WEBSERVICE_URL + strRoot + @"/" + strSpeed;

            var jsonResponse = "";

            var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
            if (webRequest != null)
            {
                webRequest.Method = "GET";
                webRequest.Timeout = 20000;
                webRequest.ContentType = "text/html;charset=UTF-8"; //"application/json";

                webRequest.Headers.Add("X-RapidAPI-Host", "greatcirclemapper.p.rapidapi.com");
                webRequest.Headers.Add("X-RapidAPI-Key", SecretsBLL.RapidAPIKey.ToString());
                webRequest.Headers.Add("vary", "Accept-Encoding");

                using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                    {
                        jsonResponse = sr.ReadToEnd();
                    }
                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic d = js.Deserialize<dynamic>(jsonResponse.ToString());

            dynamic[] arrlegs = d["legs"];

            for (int i = 0; i < arrlegs.Length; i++)
            {
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(arrlegs[i]);

                dynamic dInnerLeg = js.Deserialize<dynamic>(json);

                leg = new LegDetails();

                leg.legNo = Convert.ToString(i + 1);
                leg.startICAO = dInnerLeg["origin"]["ident"];
                leg.endICAO = dInnerLeg["destination"]["ident"];
                leg.distance_km = Convert.ToString(arrlegs[i]["distance_km"]);
                leg.distance_nm = Convert.ToString(arrlegs[i]["distance_nm"]);
                leg.flight_time_min = Convert.ToString(arrlegs[i]["flight_time_min"]);
                leg.heading_deg = Convert.ToString(arrlegs[i]["heading_deg"]);
                leg.heading_compass = Convert.ToString(arrlegs[i]["heading_compass"]);

                RouteListLeg.Add(leg);
            }
            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }
        else if (strFlag == "GF")
        {
            #region Great Circle Mapper Formula

            double LAT1 = 0;
            double LAT2 = 0;

            double LONG1 = 0;
            double LONG2 = 0;

            distanse(StartAirport, strEndAirport, ref LAT1, ref LONG1, ref LAT2, ref LONG2);

            LAT1 = (LAT1 * 3.14159 / 180);
            LONG1 = (LONG1 * 3.14159 / 180);
            LAT2 = (LAT2 * 3.14159 / 180);
            LONG2 = (LONG2 * 3.14159 / 180);

            double strDistance = ((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))) * 6378206 +
                                   5405.6 * (3 * Math.Sin((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) - (Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) /
                                   (1 + Math.Cos((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))))) * Math.Pow((Math.Sin(LAT1) * Math.Sin(LAT2)), 2) - 5405.6 * (3 * Math.Sin((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) *
                                   Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) * (Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) *
                                   Math.Cos(LONG1 - LONG2)))) / (1 - Math.Cos((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))))) * Math.Pow((Math.Sin(LAT1) - Math.Sin(LAT2)), 2)) * 0.0006213699 + 0.5;

            leg = new LegDetails();

            leg.legNo = " 0";
            leg.startICAO = StartAirport;
            leg.endICAO = strEndAirport;
            leg.distance_km = "";
            leg.distance_nm = strDistance.ToString();
            leg.flight_time_min = ((Convert.ToDecimal(strDistance) / Convert.ToDecimal(strSpeed)) * 60).ToString("00");
            leg.heading_deg = "";
            leg.heading_compass = "";

            RouteListLeg.Add(leg);

            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }
        else if (strFlag == "MN")
        {
            #region Distance Formula

            string strDistance = getDistance(StartAirport, strEndAirport);
            leg = new LegDetails();

            leg.legNo = " 0";
            leg.startICAO = StartAirport;
            leg.endICAO = strEndAirport;
            leg.distance_km = "";
            leg.distance_nm = strDistance;
            leg.flight_time_min = ((Convert.ToDecimal(strDistance) / Convert.ToDecimal(strSpeed)) * 60).ToString("00");
            leg.heading_deg = "";
            leg.heading_compass = "";

            RouteListLeg.Add(leg);

            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }
        else
        {
            #region Fore Flight API

            string ForFlightID = "NTEST";
            string profileUUID = string.Empty;

            DataTable dt = getFFTail(strTailNo);
            if (dt.Rows.Count > 0)
            {
                ForFlightID = dt.Rows[0]["TailID"].ToString();
                profileUUID = dt.Rows[0]["CruiseProfiles"].ToString();
            }


            string APIJsonstring = string.Empty;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://public-api.foreflight.com/public/api/flights/performance");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("x-api-key", "pT5PFZxv21jmN8lzZDhwPUDNHhpLwBZdiWz5NY72iR8=");

            string json = string.Empty;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //2020-09-10T16:40:17.776Z
                if (profileUUID == string.Empty)
                {
                    json = "{ \"flight\": { \"departure\": \"" + StartAirport + "\", \"destination\": \"" + strEndAirport + "\", \"scheduledTimeOfDeparture\": \"" + TimeOfDeparture + "\",  \"aircraftRegistration\": \"" + ForFlightID + "\" } }";
                }
                else
                {
                    json = "{ \"flight\": { \"departure\": \"" + StartAirport + "\", \"destination\": \"" + strEndAirport + "\", \"scheduledTimeOfDeparture\": \"" + TimeOfDeparture + "\",  \"aircraftRegistration\": \"" + ForFlightID + "\", \"cruiseProfileUUID\": \"" + profileUUID + "\" } } }";
                }

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                APIJsonstring = streamReader.ReadToEnd();
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            JObject jObjPerf = JObject.Parse(APIJsonstring);
            string strPerform = jObjPerf["performance"].ToString();

            dynamic d = js.Deserialize<dynamic>(strPerform.ToString());

            var x = JObject.Parse(strPerform);
            var success = x["times"];
            double txnId = Convert.ToDouble(x["times"]["timeToDestinationMinutes"]); //totalTimeMinutes

            var success1 = x["distances"];
            double Distancenm = Convert.ToDouble(x["distances"]["destination"]);


            TimeSpan spWorkMin = TimeSpan.FromMinutes(txnId);
            string workHours = spWorkMin.ToString(@"hh\:mm");

            leg = new LegDetails();

            leg.legNo = " 0";
            leg.startICAO = StartAirport;
            leg.endICAO = strEndAirport;
            leg.distance_km = "";
            leg.distance_nm = Distancenm.ToString();
            leg.flight_time_min = txnId.ToString();
            leg.heading_deg = "";
            leg.heading_compass = "";

            RouteListLeg.Add(leg);

            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }

        return dtResul;
    }

    public DataTable FlightHourCalculation_Test(string StartAirport, string strEndAirport, string strTailNo, string TimeOfDeparture,
        string strSpeed, string strFlag)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
        DataTable dtResul = new DataTable();
        LegDetails leg = new LegDetails();
        List<LegDetails> RouteListLeg = new List<LegDetails>();


        if (strFlag == "GC")
        {
            #region Great Circle API

            string strRoot = StartAirport + "-" + strEndAirport;

            string WEBSERVICE_URL = "https://greatcirclemapper.p.rapidapi.com/airports/route/";
            WEBSERVICE_URL = WEBSERVICE_URL + strRoot + @"/" + strSpeed;

            var jsonResponse = "";

            var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
            if (webRequest != null)
            {
                webRequest.Method = "GET";
                webRequest.Timeout = 20000;
                webRequest.ContentType = "text/html;charset=UTF-8"; //"application/json";

                webRequest.Headers.Add("X-RapidAPI-Host", "greatcirclemapper.p.rapidapi.com");
                webRequest.Headers.Add("X-RapidAPI-Key", SecretsBLL.RapidAPIKey.ToString());
                webRequest.Headers.Add("vary", "Accept-Encoding");

                using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                    {
                        jsonResponse = sr.ReadToEnd();
                    }
                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic d = js.Deserialize<dynamic>(jsonResponse.ToString());

            dynamic[] arrlegs = d["legs"];

            for (int i = 0; i < arrlegs.Length; i++)
            {
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(arrlegs[i]);

                dynamic dInnerLeg = js.Deserialize<dynamic>(json);

                leg = new LegDetails();

                leg.legNo = Convert.ToString(i + 1);
                leg.startICAO = dInnerLeg["origin"]["ident"];
                leg.endICAO = dInnerLeg["destination"]["ident"];
                leg.distance_km = Convert.ToString(arrlegs[i]["distance_km"]);
                leg.distance_nm = Convert.ToString(arrlegs[i]["distance_nm"]);
                leg.flight_time_min = Convert.ToString(arrlegs[i]["flight_time_min"]);
                leg.heading_deg = Convert.ToString(arrlegs[i]["heading_deg"]);
                leg.heading_compass = Convert.ToString(arrlegs[i]["heading_compass"]);

                RouteListLeg.Add(leg);
            }
            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }
        else if (strFlag == "GF")
        {
            #region Great Circle Mapper Formula

            double LAT1 = 0;
            double LAT2 = 0;

            double LONG1 = 0;
            double LONG2 = 0;

            distanse(StartAirport, strEndAirport, ref LAT1, ref LONG1, ref LAT2, ref LONG2);

            LAT1 = (LAT1 * 3.14159 / 180);
            LONG1 = (LONG1 * 3.14159 / 180);
            LAT2 = (LAT2 * 3.14159 / 180);
            LONG2 = (LONG2 * 3.14159 / 180);

            double strDistance = ((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))) * 6378206 +
                                   5405.6 * (3 * Math.Sin((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) - (Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) /
                                   (1 + Math.Cos((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))))) * Math.Pow((Math.Sin(LAT1) * Math.Sin(LAT2)), 2) - 5405.6 * (3 * Math.Sin((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) *
                                   Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2)))) * (Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) *
                                   Math.Cos(LONG1 - LONG2)))) / (1 - Math.Cos((Math.Acos(Math.Sin(LAT1) * Math.Sin(LAT2) + Math.Cos(LAT1) * Math.Cos(LAT2) * Math.Cos(LONG1 - LONG2))))) * Math.Pow((Math.Sin(LAT1) - Math.Sin(LAT2)), 2)) * 0.0006213699 + 0.5;

            leg = new LegDetails();

            leg.legNo = " 0";
            leg.startICAO = StartAirport;
            leg.endICAO = strEndAirport;
            leg.distance_km = "";
            leg.distance_nm = strDistance.ToString();
            leg.flight_time_min = ((Convert.ToDecimal(strDistance) / Convert.ToDecimal(strSpeed)) * 60).ToString("00");
            leg.heading_deg = "";
            leg.heading_compass = "";

            RouteListLeg.Add(leg);

            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }
        else if (strFlag == "MN")
        {
            #region Distance Formula

            string strDistance = getDistance(StartAirport, strEndAirport);
            leg = new LegDetails();

            leg.legNo = " 0";
            leg.startICAO = StartAirport;
            leg.endICAO = strEndAirport;
            leg.distance_km = "";
            leg.distance_nm = strDistance;
            leg.flight_time_min = ((Convert.ToDecimal(strDistance) / Convert.ToDecimal(strSpeed)) * 60).ToString("00");
            leg.heading_deg = "";
            leg.heading_compass = "";

            RouteListLeg.Add(leg);

            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }
        else
        {
            #region Fore Flight API

            string ForFlightID = "NTEST";
            string profileUUID = string.Empty;

            string windModel = string.Empty;
            string fixedWindComponent = string.Empty;
            string fixedIsaDeviation = string.Empty;
            string historicalProbability = string.Empty;

            DataTable dt = getFFTail(strTailNo);
            if (dt.Rows.Count > 0)
            {
                ForFlightID = dt.Rows[0]["TailID"].ToString();
                profileUUID = dt.Rows[0]["CruiseProfiles"].ToString();

                windModel = dt.Rows[0]["windModel"].ToString();
                fixedWindComponent = dt.Rows[0]["fixedWindComponent"].ToString();
                fixedIsaDeviation = dt.Rows[0]["fixedIsaDeviation"].ToString();
                historicalProbability = dt.Rows[0]["historicalProbability"].ToString();
            }


            string APIJsonstring = string.Empty;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://public-api.foreflight.com/public/api/flights/performance");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("x-api-key", "pT5PFZxv21jmN8lzZDhwPUDNHhpLwBZdiWz5NY72iR8=");

            string json = string.Empty;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //2020-09-10T16:40:17.776Z
                if (profileUUID == string.Empty)
                {
                    json = "{ \"flight\": { \"departure\": \"" + StartAirport + "\", \"destination\": \"" + strEndAirport + "\", \"scheduledTimeOfDeparture\": \"" + TimeOfDeparture + "\",  \"aircraftRegistration\": \"" + ForFlightID + "\" } }";
                }
                else
                {
                    json = "{ \"flight\": { \"departure\": \"" + StartAirport + "\", \"destination\": \"" + strEndAirport + "\", \"scheduledTimeOfDeparture\": \"" + TimeOfDeparture + "\",  \"aircraftRegistration\": \"" + ForFlightID + "\", \"cruiseProfileUUID\": \"" + profileUUID + "\", \"windOptions\": { \"windModel\": \"" + windModel + "\", \"fixedWindComponent\": " + fixedWindComponent + ", \"fixedIsaDeviation\": " + fixedIsaDeviation + ", \"historicalProbability\": " + historicalProbability + " } } }";
                }

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                APIJsonstring = streamReader.ReadToEnd();
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            JObject jObjPerf = JObject.Parse(APIJsonstring);
            string strPerform = jObjPerf["performance"].ToString();

            dynamic d = js.Deserialize<dynamic>(strPerform.ToString());

            var x = JObject.Parse(strPerform);
            var success = x["times"];
            double txnId = Convert.ToDouble(x["times"]["timeToDestinationMinutes"]); //totalTimeMinutes

            var success1 = x["distances"];
            double Distancenm = Convert.ToDouble(x["distances"]["destination"]);


            TimeSpan spWorkMin = TimeSpan.FromMinutes(txnId);
            string workHours = spWorkMin.ToString(@"hh\:mm");

            leg = new LegDetails();

            leg.legNo = " 0";
            leg.startICAO = StartAirport;
            leg.endICAO = strEndAirport;
            leg.distance_km = "";
            leg.distance_nm = Distancenm.ToString();
            leg.flight_time_min = txnId.ToString();
            leg.heading_deg = "";
            leg.heading_compass = "";

            RouteListLeg.Add(leg);

            dtResul = ToDataTable(RouteListLeg);

            #endregion
        }

        return dtResul;
    }

    public static string AirRoute_ForeFlight(string StartAirport, string strEndAirport, string strTailNo, string TimeOfDeparture)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);

        string APIJsonstring = string.Empty;

        var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://public-api.foreflight.com/public/api/flights/performance");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";
        httpWebRequest.Headers.Add("x-api-key", "pT5PFZxv21jmN8lzZDhwPUDNHhpLwBZdiWz5NY72iR8=");

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            //2020-09-10T16:40:17.776Z
            string json = "{ \"flight\": { \"departure\": \"" + StartAirport + "\", \"destination\": \"" + strEndAirport + "\", \"scheduledTimeOfDeparture\": \"" + TimeOfDeparture + "\",  \"aircraftRegistration\": \"NTEST\" } }";
            streamWriter.Write(json);
        }

        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            APIJsonstring = streamReader.ReadToEnd();
        }

        JavaScriptSerializer js = new JavaScriptSerializer();

        JObject jObjPerf = JObject.Parse(APIJsonstring);
        string strPerform = jObjPerf["performance"].ToString();

        dynamic d = js.Deserialize<dynamic>(strPerform.ToString());

        var x = JObject.Parse(strPerform);
        var success = x["distances"];
        double txnId = Convert.ToDouble(x["distances"]["destination"]);

        TimeSpan spWorkMin = TimeSpan.FromMinutes(txnId);
        string workHours = spWorkMin.ToString(@"hh\:mm");

        return APIJsonstring;
    }

    public DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }

        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }

        //put a breakpoint here and check datatable
        return dataTable;

    }
    public DataTable RapidAPIDatatable()
    {
        DataTable dtRapid = new DataTable();
        dtRapid.Columns.Add("legNo");
        dtRapid.Columns.Add("startICAO");
        dtRapid.Columns.Add("endICAO");
        dtRapid.Columns.Add("distance_km");
        dtRapid.Columns.Add("distance_nm");
        dtRapid.Columns.Add("flight_time_min");
        dtRapid.Columns.Add("heading_deg");
        dtRapid.Columns.Add("heading_compass");

        DataRow row = dtRapid.NewRow();
        row["legNo"] = 1;
        row["startICAO"] = "KVNY";
        row["endICAO"] = "KSNA";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        row = dtRapid.NewRow();
        row["legNo"] = 2;
        row["startICAO"] = "KSNA";
        row["endICAO"] = "KVNY";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        return dtRapid;


    }

    public string getGCSpeedNEW(string strName, string strID)
    {

        string strSpeed = "420";

        //SqlParameter[] SubmitParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.QuoteConnectionString, "FLIGHT_BOOK_09_SPEED", false);
        //SubmitParams[0].Value = "GETSPEED1";
        //SubmitParams[3].Value = strName;
        //SubmitParams[4].Value = strID;
        //object obj = SqlHelper.ExecuteScalar(SqlHelper.QuoteConnectionString, "FLIGHT_BOOK_09_SPEED", SubmitParams);

        //if (obj != null)
        //    strSpeed = obj.ToString();

        return strSpeed;

    }
    public string getDistance(string strStartAirport, string strEndAirport)
    {
        string strSpeed = string.Empty;
        SqlParameter[] SubmitParams = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "FIND_DISTANCE", false);
        SubmitParams[0].Value = "GETDISTANCE";
        SubmitParams[1].Value = strStartAirport;
        SubmitParams[2].Value = strEndAirport;
        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "FIND_DISTANCE", SubmitParams);
        if (obj != null)
            strSpeed = obj.ToString();

        return strSpeed;

    }

    public DataTable getFFTail(string strTailNo)
    {
        SqlParameter[] getFFTailParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "GlobalConfig_SEL", false);
        getFFTailParm[0].Value = "FFS";
        getFFTailParm[1].Value = strTailNo;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "GlobalConfig_SEL", getFFTailParm).Tables[0];
    }

    public void distanse(string strStartAirport, string strEndAirport, ref double LAT1, ref double LONG1,
                           ref double LAT2, ref double LONG2)
    {
        DataTable dtStart = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Latitude, Longitude from JETEDGE_FUEL..[ST_AIRPORT] where ICAO='" + strStartAirport + "'").Tables[0];
        DataTable dtEnd = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Latitude, Longitude from JETEDGE_FUEL..[ST_AIRPORT] where ICAO='" + strEndAirport + "'").Tables[0];

        LAT1 = Convert.ToDouble(dtStart.Rows[0]["Latitude"].ToString());
        LAT2 = Convert.ToDouble(dtEnd.Rows[0]["Latitude"].ToString());

        LONG1 = Convert.ToDouble(dtStart.Rows[0]["Longitude"].ToString());
        LONG2 = Convert.ToDouble(dtEnd.Rows[0]["Longitude"].ToString());
    }
}

public class LegDetails
{
    public string legNo { get; set; }
    public string startICAO { get; set; }
    public string endICAO { get; set; }
    public string distance_km { get; set; }
    public string distance_nm { get; set; }
    public string flight_time_min { get; set; }
    public string heading_deg { get; set; }
    public string heading_compass { get; set; }
}





