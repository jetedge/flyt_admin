﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CountryFeeBase
/// </summary>
/// 
namespace AbstractLayer
{
    public abstract class CountryFeeBase
    {
        private string _ManageType;
        private string _RowID;
        private string _Countryid;
        private string _FeeName;
        private string _Amount;
        private string _ApplyFor;
        private string _EffFrom;
        private string _DiscontinueDate;
        private string _CreatedBy;
        private string _Active;


        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }

        //for Row ID
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for Country Id
        public string Countryid
        {
            get { return _Countryid; }
            set
            {
                if (value != _Countryid)
                {
                    _Countryid = value;
                }
            }
        }

        //for Fee Name
        public string FeeName
        {
            get { return _FeeName; }
            set
            {
                if (value != _FeeName)
                {
                    _FeeName = value;
                }
            }
        }

        //for Amount
        public string Amount
        {
            get { return _Amount; }
            set
            {
                if (value != _Amount)
                {
                    _Amount = value;
                }
            }
        }

        //for Apply For
        public string ApplyFor
        {
            get { return _ApplyFor; }
            set
            {
                if (value != _ApplyFor)
                {
                    _ApplyFor = value;
                }
            }
        }

        //for Eff From
        public string EffectiveFrom
        {
            get { return _EffFrom; }
            set
            {
                if (value != _EffFrom)
                {
                    _EffFrom = value;
                }
            }
        }

        //for Discountinue Date
        public string DiscountinueDate
        {
            get { return _DiscontinueDate; }
            set
            {
                if (value != _DiscontinueDate)
                {
                    _DiscontinueDate = value;
                }
            }
        }

        //for Created By
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        //for Active
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }


    }
}