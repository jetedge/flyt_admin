﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CreateCustomerBase
/// </summary>
/// 

namespace AbstractLayer
{
    public abstract class CreateCustomerBase
    {
        private string _ManageType;
        private string _RowID;
        private string _Title;
        private string _FirstName;
        private string _LastName;
        private string _Email;
        private string _PhoneCode;
        private string _Phone;
        private string _Address1;
        private string _Address2;
        private string _City;
        private string _State;
        private string _Country;
        private string _ZipCode;
        private string _Active;
        private string _Password;
        private string _ConfirmPassword;
        private string _UserType;
        private string _BrokerFlag;
        private string _BrokerFirstName;
        private string _BrokerLastName;
        private string _BrokerManager;
        private string _BrokerAddress;
        private string _BrokerCity;
        private string _BrkerState;
        private string _BrokerCountry;
        private string _BrokerZipCode;
        private string _NumberOfDays;
        private string _CreatedBy;

        private string _PreManageType;
        private string _PreRowID;
        private string _PreStartAirport;
        private string _preEndAirport;
        private string _PreStartDate;
        private string _PreTime;
        private string _PreEmailFlag;
        private string _PreStartCity;
        private string _PreEndCity;
        private string _PreEndDate;
        private string _preTimeFlag;
        private string _preSMSFlag;
        private string _PreMustMoveMailConfig;
        private string _CountryId;
        private string _PayMethod;

        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }


        //for Row ID
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for Title
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value != _Title)
                {
                    _Title = value;
                }
            }
        }

        //for firstname
        public string firstName
        {
            get { return _FirstName; }
            set
            {
                if (value != _FirstName)
                {
                    _FirstName = value;
                }
            }
        }
        //for lastname
        public string lastName
        {
            get { return _LastName; }
            set
            {
                if (value != _LastName)
                {
                    _LastName = value;
                }
            }
        }
        //for Email
        public string Email
        {
            get { return _Email; }
            set
            {
                if (value != _Email)
                {
                    _Email = value;
                }
            }
        }
        //for PhoneCode
        public string PhoneCode
        {
            get { return _PhoneCode; }
            set
            {
                if (value != _PhoneCode)
                {
                    _PhoneCode = value;
                }
            }
        }
        //for Phone
        public string Phone
        {
            get { return _Phone; }
            set
            {
                if (value != _Phone)
                {
                    _Phone = value;
                }
            }
        }

        //for Address1
        public string Address1
        {
            get { return _Address1; }
            set
            {
                if (value != _Address1)
                {
                    _Address1 = value;
                }
            }
        }
        //for Address2
        public string Address2
        {
            get { return _Address2; }
            set
            {
                if (value != _Address2)
                {
                    _Address2 = value;
                }
            }
        }

        //for City
        public string City
        {
            get { return _City; }
            set
            {
                if (value != _City)
                {
                    _City = value;
                }
            }
        }
        //for State
        public string State
        {
            get { return _State; }
            set
            {
                if (value != _State)
                {
                    _State = value;
                }
            }
        }
        //for Country
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value != _Country)
                {
                    _Country = value;
                }
            }
        }
        //for ZipCode
        public string ZipCode
        {
            get { return _ZipCode; }
            set
            {
                if (value != _ZipCode)
                {
                    _ZipCode = value;
                }
            }
        }
        //for Active
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }

        //for Password
        public string Password
        {
            get { return _Password; }
            set
            {
                if (value != _Password)
                {
                    _Password = value;
                }
            }
        }
        //for Confirm Password
        public string ConfirmPassword
        {
            get { return _ConfirmPassword; }
            set
            {
                if (value != _ConfirmPassword)
                {
                    _ConfirmPassword = value;
                }
            }
        }

        //for User Type
        public string UserType
        {
            get { return _UserType; }
            set
            {
                if (value != _UserType)
                {
                    _UserType = value;
                }
            }
        }
        //for BrokerFlag
        public string BrokerFlag
        {
            get { return _BrokerFlag; }
            set
            {
                if (value != _BrokerFlag)
                {
                    _BrokerFlag = value;
                }
            }
        }
        //for Broker FirstName
        public string BrokerFirstName
        {
            get { return _BrokerFirstName; }
            set
            {
                if (value != _BrokerFirstName)
                {
                    _BrokerFirstName = value;
                }
            }
        }
        //for Broker LastName
        public string BrokerLastName
        {
            get { return _BrokerLastName; }
            set
            {
                if (value != _BrokerLastName)
                {
                    _BrokerLastName = value;
                }
            }
        }
        //for Broker Manager
        public string Manger
        {
            get { return _BrokerManager; }
            set
            {
                if (value != _BrokerManager)
                {
                    _BrokerManager = value;
                }
            }
        }

        //for Broker Adrress
        public string BrokerAddress
        {
            get { return _BrokerAddress; }
            set
            {
                if (value != _BrokerAddress)
                {
                    _BrokerAddress = value;
                }
            }
        }



        //for Broker City
        public string BrokerCity
        {
            get { return _BrokerCity; }
            set
            {
                if (value != _BrokerCity)
                {
                    _BrokerCity = value;
                }
            }
        }

        //for Broker State
        public string BrokerState
        {
            get { return _BrkerState; }
            set
            {
                if (value != _BrkerState)
                {
                    _BrkerState = value;
                }
            }
        }



        //for Broker Country
        public string BrokerCountry
        {
            get { return _BrokerCountry; }
            set
            {
                if (value != _BrokerCountry)
                {
                    _BrokerCountry = value;
                }
            }
        }

        //for Broker ZipCode
        public string BrokerZipCode
        {
            get { return _BrokerZipCode; }
            set
            {
                if (value != _BrokerZipCode)
                {
                    _BrokerZipCode = value;
                }
            }
        }

        //for Broker Number Days
        public string NumberDays
        {
            get { return _NumberOfDays; }
            set
            {
                if (value != _NumberOfDays)
                {
                    _NumberOfDays = value;
                }
            }
        }


        //for Pre StarAirport
        public string StartAirport
        {
            get { return _PreStartAirport; }
            set
            {
                if (value != _PreStartAirport)
                {
                    _PreStartAirport = value;
                }
            }
        }

        //for Pre EndAirport
        public string EndAirport
        {
            get { return _preEndAirport; }
            set
            {
                if (value != _preEndAirport)
                {
                    _preEndAirport = value;
                }
            }
        }

        //for Pre StartDate
        public string StartDate
        {
            get { return _PreStartDate; }
            set
            {
                if (value != _PreStartDate)
                {
                    _PreStartDate = value;
                }
            }
        }

        //for Pre Time
        public string Time
        {
            get { return _PreTime; }
            set
            {
                if (value != _PreTime)
                {
                    _PreTime = value;
                }
            }
        }

        //for Email Flag
        public string EmailFlag
        {
            get { return _PreEmailFlag; }
            set
            {
                if (value != _PreTime)
                {
                    _PreEmailFlag = value;
                }
            }
        }

        //for StartCity
        public string StartCity
        {
            get { return _PreStartCity; }
            set
            {
                if (value != _PreStartCity)
                {
                    _PreStartCity = value;
                }
            }
        }

        //for EndCity
        public string EndCity
        {
            get { return _PreEndCity; }
            set
            {
                if (value != _PreEndCity)
                {
                    _PreEndCity = value;
                }
            }
        }

        //for EndDate
        public string EndDate
        {
            get { return _PreEndDate; }
            set
            {
                if (value != _PreEndDate)
                {
                    _PreEndDate = value;
                }
            }
        }

        //for Tmie Flag
        public string TimeFlag
        {
            get { return _preTimeFlag; }
            set
            {
                if (value != _preTimeFlag)
                {
                    _preTimeFlag = value;
                }
            }
        }

        //for Pre Manage Type
        public string PreManageType
        {
            get { return _PreManageType; }
            set
            {
                if (value != _PreManageType)
                {
                    _PreManageType = value;
                }
            }
        }

        //for Pre Row Id
        public string PreRowID
        {
            get { return _PreRowID; }
            set
            {
                if (value != _PreRowID)
                {
                    _PreRowID = value;
                }
            }
        }

        //for Created By
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        //for SMS Flag
        public string SMSFlag
        {
            get { return _preSMSFlag; }
            set
            {
                if (value != _preSMSFlag)
                {
                    _preSMSFlag = value;
                }
            }
        }

        //for Must Move Mail Flag
        public string MustMoveMailFlag
        {
            get { return _PreMustMoveMailConfig; }
            set
            {
                if (value != _PreMustMoveMailConfig)
                {
                    _PreMustMoveMailConfig = value;
                }
            }
        }

        //for Country Id
        public string Countryid
        {
            get { return _CountryId; }
            set
            {
                if (value != _CountryId)
                {
                    _CountryId = value;
                }
            }
        }

        //for Pay Method
        public string PayMehod
        {
            get { return _PayMethod; }
            set
            {
                if (value != _PayMethod)
                {
                    _PayMethod = value;
                }
            }
        }

        private string _StartICAODisp;
        private string _EndICAODisp;

        //for StartICAODisp
        public string StartICAODisp
        {
            get { return _StartICAODisp; }
            set
            {
                if (value != _StartICAODisp)
                {
                    _StartICAODisp = value;
                }
            }
        }
        //for EndICAODisp
        public string EndICAODisp
        {
            get { return _EndICAODisp; }
            set
            {
                if (value != _EndICAODisp)
                {
                    _EndICAODisp = value;
                }
            }
        }

    }
}