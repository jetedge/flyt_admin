﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CreateAdminBase
/// </summary>

namespace AbstractLayer
{
    public abstract class CreateAdminBase
    {
        private string _ManageType;
        private string _RowID;
        private string _Title;
        private string _FirstName;
        private string _LastName;
        private string _Email;
        private string _PhoneCode;
        private string _Phone;
        private string _Address1;
        private string _Address2;
        private string _City;
        private string _State;
        private string _Country;
        private string _ZipCode;
        private string _Active;
        private string _Password;
        private string _UserType;
        private string _CreatedBy;
        private string _CountryId;

        private string _BookingConfirmation;
        private string _Exception;
        private string _SavedRoutes;
        private string _MustMove;
        private string _signUp;

        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }

        //for Country Id
        public string CountryId
        {
            get { return _CountryId; }
            set
            {
                if (value != _CountryId)
                {
                    _CountryId = value;
                }
            }
        }


        //for Row ID
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for Title
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value != _Title)
                {
                    _Title = value;
                }
            }
        }

        //for firstname
        public string firstName
        {
            get { return _FirstName; }
            set
            {
                if (value != _FirstName)
                {
                    _FirstName = value;
                }
            }
        }
        //for lastname
        public string lastName
        {
            get { return _LastName; }
            set
            {
                if (value != _LastName)
                {
                    _LastName = value;
                }
            }
        }
        //for Email
        public string Email
        {
            get { return _Email; }
            set
            {
                if (value != _Email)
                {
                    _Email = value;
                }
            }
        }
        //for PhoneCode
        public string PhoneCode
        {
            get { return _PhoneCode; }
            set
            {
                if (value != _PhoneCode)
                {
                    _PhoneCode = value;
                }
            }
        }
        //for Phone
        public string Phone
        {
            get { return _Phone; }
            set
            {
                if (value != _Phone)
                {
                    _Phone = value;
                }
            }
        }

        //for Address1
        public string Address1
        {
            get { return _Address1; }
            set
            {
                if (value != _Address1)
                {
                    _Address1 = value;
                }
            }
        }
        //for Address2
        public string Address2
        {
            get { return _Address2; }
            set
            {
                if (value != _Address2)
                {
                    _Address2 = value;
                }
            }
        }

        //for City
        public string City
        {
            get { return _City; }
            set
            {
                if (value != _City)
                {
                    _City = value;
                }
            }
        }
        //for State
        public string State
        {
            get { return _State; }
            set
            {
                if (value != _State)
                {
                    _State = value;
                }
            }
        }
        //for Country
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value != _Country)
                {
                    _Country = value;
                }
            }
        }
        //for ZipCode
        public string ZipCode
        {
            get { return _ZipCode; }
            set
            {
                if (value != _ZipCode)
                {
                    _ZipCode = value;
                }
            }
        }
        //for Active
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }

        //for Password
        public string Password
        {
            get { return _Password; }
            set
            {
                if (value != _Password)
                {
                    _Password = value;
                }
            }
        }

        //for User Type
        public string UserType
        {
            get { return _UserType; }
            set
            {
                if (value != _UserType)
                {
                    _UserType = value;
                }
            }
        }

        //for Created By
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        public string BookingConfirmation
        {
            get { return _BookingConfirmation; }
            set
            {
                if (value != _BookingConfirmation)
                {
                    _BookingConfirmation = value;
                }
            }
        }

        public string Exception
        {
            get { return _Exception; }
            set
            {
                if (value != _Exception)
                {
                    _Exception = value;
                }
            }
        }

        public string SavedRoutes
        {
            get { return _SavedRoutes; }
            set
            {
                if (value != _SavedRoutes)
                {
                    _SavedRoutes = value;
                }
            }
        }

        public string MustMove
        {
            get { return _MustMove; }
            set
            {
                if (value != _MustMove)
                {
                    _MustMove = value;
                }
            }
        }

        public string signUp
        {
            get { return _signUp; }
            set
            {
                if (value != _signUp)
                {
                    _signUp = value;
                }
            }
        }


    }
}