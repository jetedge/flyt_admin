﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AirportMasterBase
/// </summary>

namespace AbstractLayer
{
    public abstract class AirportMasterBase
    {
        private string _ManageType;
        private string _RowID;
        private string _ICAO;
        private string _Country;
        private string _City;
        private string _RunWayLength;
        private string _LandingFlag;
        private string _Active;
        private string _CreatedBy;


        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        //for Rowid
        public string RowId
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for ICAO
        public string ICAO
        {
            get { return _ICAO; }
            set
            {
                if (value != _ICAO)
                {
                    _ICAO = value;
                }
            }
        }


        //for Country
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value != _Country)
                {
                    _Country = value;
                }
            }
        }

        //for RunWay Length
        public string RunWayLength
        {
            get { return _RunWayLength; }
            set
            {
                if (value != _RunWayLength)
                {
                    _RunWayLength = value;
                }
            }
        }

        //for Active
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }

        //for Landing
        public string LandingFlag
        {
            get { return _LandingFlag; }
            set
            {
                if (value != _LandingFlag)
                {
                    _LandingFlag = value;
                }
            }
        }

        //for Citu
        public string City
        {
            get { return _City; }
            set
            {
                if (value != _City)
                {
                    _City = value;
                }
            }
        }

        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }
    }
}