﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PrivacyEmailbase
/// </summary>

namespace AbstractLayer
{
    public  class PrivacyEmailbase
    {
        private string _ManageTye;
        private string _PID;
        private string _FirstName;
        private string _LastName;
        private string _EmailID;
        private string _CreatedBy;
       


        //for ManageTye
        public string ManageTye
        {
            get { return _ManageTye; }
            set
            {
                if (value != _ManageTye)
                {
                    _ManageTye = value;
                }
            }
        }
        //for PID
        public string PID
        {
            get { return _PID; }
            set
            {
                if (value != _PID)
                {
                    _PID = value;
                }
            }
        }

        //for FirstName
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                if (value != _FirstName)
                {
                    _FirstName = value;
                }
            }
        }



        //for LastName
        public string LastName
        {
            get { return _LastName; }
            set
            {
                if (value != _LastName)
                {
                    _LastName = value;
                }
            }
        }



        //for EmailID
        public string EmailID
        {
            get { return _EmailID; }
            set
            {
                if (value != _EmailID)
                {
                    _EmailID = value;
                }
            }
        }


        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }


        


    }
}