﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmailSetupBase
/// </summary>

namespace AbstractLayer
{
    public abstract class EmailSetupBase
    {
        private string _ChkTrip;
        private string _ChkException;
        private string _ChkPreference;
        private string _ChkMustMoves;
        private string _ChkSignUp;
        private string _CreatedBy;
        private string _Email;
        private string _FirstName;
        private string _LastName;


        //for Trip
        public string ChkTrip
        {
            get { return _ChkTrip; }
            set
            {
                if (value != _ChkTrip)
                {
                    _ChkTrip = value;
                }
            }
        }

        //for Exception
        public string ChkException
        {
            get { return _ChkException; }
            set
            {
                if (value != _ChkException)
                {
                    _ChkException = value;
                }
            }
        }

        //for Preference
        public string ChkPreference
        {
            get { return _ChkPreference; }
            set
            {
                if (value != _ChkPreference)
                {
                    _ChkPreference = value;
                }
            }
        }

        //for Must Move
        public string ChkMustMove
        {
            get { return _ChkMustMoves; }
            set
            {
                if (value != _ChkMustMoves)
                {
                    _ChkMustMoves = value;
                }
            }
        }

        //for SignUp
        public string ChkSignUp
        {
            get { return _ChkSignUp; }
            set
            {
                if (value != _ChkSignUp)
                {
                    _ChkSignUp = value;
                }
            }
        }

        //for Created By
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        //for Email
        public string Email
        {
            get { return _Email; }
            set
            {
                if (value != _Email)
                {
                    _Email = value;
                }
            }
        }

        //for First name
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                if (value != _FirstName)
                {
                    _FirstName = value;
                }
            }
        }

        //for Last Name
        public string LastName
        {
            get { return _LastName; }
            set
            {
                if (value != _LastName)
                {
                    _LastName = value;
                }
            }
        }

       
    }
}