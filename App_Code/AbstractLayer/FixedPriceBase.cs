﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FixedPriceBase
/// </summary>
/// 
namespace AbstractLayer
{
    public abstract class FixedPriceBase
    {
        private string _ManageType;
        private string _RowID;
        private string _StartDate;
        private string _EndDate;
        private string _SpecialPriceDescription;
        private string _PriceDetails;

        private string _StartAirport;
        private string _EndAirport;
        private string _StartAirportDisplay;
        private string _EndAirportDisplay;

        private string _TailNo;
        private string _CreatedBy;


        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }

        //for RowID
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for StartDate
        public string StartDate
        {
            get { return _StartDate; }
            set
            {
                if (value != _StartDate)
                {
                    _StartDate = value;
                }
            }
        }

        //for EndDate
        public string EndDate
        {
            get { return _EndDate; }
            set
            {
                if (value != _EndDate)
                {
                    _EndDate = value;
                }
            }
        }

        //for SpecialPriceDescription
        public string SpecialPriceDescription
        {
            get { return _SpecialPriceDescription; }
            set
            {
                if (value != _SpecialPriceDescription)
                {
                    _SpecialPriceDescription = value;
                }
            }
        }

        //for PriceDetails
        public string PriceDetails
        {
            get { return _PriceDetails; }
            set
            {
                if (value != _PriceDetails)
                {
                    _PriceDetails = value;
                }
            }
        }

        //for StartAirport
        public string StartAirport
        {
            get { return _StartAirport; }
            set
            {
                if (value != _StartAirport)
                {
                    _StartAirport = value;
                }
            }
        }

        //for EndAirport
        public string EndAirport
        {
            get { return _EndAirport; }
            set
            {
                if (value != _EndAirport)
                {
                    _EndAirport = value;
                }
            }
        }

        //for StartAirport Display
        public string StartAirportDisplay
        {
            get { return _StartAirportDisplay; }
            set
            {
                if (value != _StartAirportDisplay)
                {
                    _StartAirportDisplay = value;
                }
            }
        }

        //for EndAirport Display
        public string EndAirportDisplay
        {
            get { return _EndAirportDisplay; }
            set
            {
                if (value != _EndAirportDisplay)
                {
                    _EndAirportDisplay = value;
                }
            }
        }

        //for TailNo
        public string TailNo
        {
            get { return _TailNo; }
            set
            {
                if (value != _TailNo)
                {
                    _TailNo = value;
                }
            }
        }

        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }
    }
}