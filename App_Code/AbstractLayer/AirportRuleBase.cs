﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace AbstractLayer
{
    /// <summary>
    /// Summary description for AirportRuleBase
    /// </summary>
    public abstract class AirportRuleBase
    {
        private string _ManageType;
        private string _RuleId;
        private string _DepSetupBy;
        private string _ArrSetupBy;

        private string _DepRuleCode;
        private string _DepRuleName;

        private string _ArrRuleCode;
        private string _ArrRuleName;

        private string _Status;
        private string _CreatedBy;
        private string _CreatedOn;
        private string _ModifiedBy;
        private string _ModifiedOn;

        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        public string RuleId
        {
            get { return _RuleId; }
            set
            {
                if (value != _RuleId)
                {
                    _RuleId = value;
                }
            }
        }
        public string DepSetupBy
        {
            get { return _DepSetupBy; }
            set
            {
                if (value != _DepSetupBy)
                {
                    _DepSetupBy = value;
                }
            }
        }
        public string ArrSetupBy
        {
            get { return _ArrSetupBy; }
            set
            {
                if (value != _ArrSetupBy)
                {
                    _ArrSetupBy = value;
                }
            }
        }

        public string DepRuleCode
        {
            get { return _DepRuleCode; }
            set
            {
                if (value != _DepRuleCode)
                {
                    _DepRuleCode = value;
                }
            }
        }
        public string DepRuleName
        {
            get { return _DepRuleName; }
            set
            {
                if (value != _DepRuleName)
                {
                    _DepRuleName = value;
                }
            }
        }

        public string ArrRuleCode
        {
            get { return _ArrRuleCode; }
            set
            {
                if (value != _ArrRuleCode)
                {
                    _ArrRuleCode = value;
                }
            }
        }
        public string ArrRuleName
        {
            get { return _ArrRuleName; }
            set
            {
                if (value != _ArrRuleName)
                {
                    _ArrRuleName = value;
                }
            }
        }

        public string Status
        {
            get { return _Status; }
            set
            {
                if (value != _Status)
                {
                    _Status = value;
                }
            }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }
        public string CreatedOn
        {
            get { return _CreatedOn; }
            set
            {
                if (value != _CreatedOn)
                {
                    _CreatedOn = value;
                }
            }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value != _ModifiedBy)
                {
                    _ModifiedBy = value;
                }
            }
        }
        public string ModifiedOn
        {
            get { return _ModifiedOn; }
            set
            {
                if (value != _ModifiedOn)
                {
                    _ModifiedOn = value;
                }
            }
        }
    }
}