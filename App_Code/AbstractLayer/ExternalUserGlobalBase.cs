﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbstractLayer
{
    /// <summary>
    /// Summary description for ExternalUserGlobalBase
    /// </summary>
    public class ExternalUserGlobalBase
    {
        private string _ManageTye;
        private string _ExtId;
        private string _UserName;
        private string _EmailId;
        private string _UserRole;
        private string _CreatedBy;

        public string ManageTye
        {
            get { return _ManageTye; }
            set
            {
                if (value != _ManageTye)
                {
                    _ManageTye = value;
                }
            }
        }
        //for ExtId
        public string ExtId
        {
            get { return _ExtId; }
            set
            {
                if (value != _ExtId)
                {
                    _ExtId = value;
                }
            }
        }
        //for UserName
        public string UserName
        {
            get { return _UserName; }
            set
            {
                if (value != _UserName)
                {
                    _UserName = value;
                }
            }
        }
        //for EmailId
        public string EmailId
        {
            get { return _EmailId; }
            set
            {
                if (value != _EmailId)
                {
                    _EmailId = value;
                }
            }
        }
        //for UserRole
        public string UserRole
        {
            get { return _UserRole; }
            set
            {
                if (value != _UserRole)
                {
                    _UserRole = value;
                }
            }
        }
        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        #region Crew / Day Duty Filter

        private string _DutyId;
        private string _BLKHours;
        private string _CrewDuty;
        private string _CrewDutyFilter;

        //for Duty Id
        public string DutyId
        {
            get { return _DutyId; }
            set
            {
                if (value != _DutyId)
                {
                    _DutyId = value;
                }
            }
        }
        //for BLK Hours / Day
        public string BLKHours
        {
            get { return _BLKHours; }
            set
            {
                if (value != _BLKHours)
                {
                    _BLKHours = value;
                }
            }
        }
        //for Crew Duty Hours / Day
        public string CrewDuty
        {
            get { return _CrewDuty; }
            set
            {
                if (value != _CrewDuty)
                {
                    _CrewDuty = value;
                }
            }
        }
        //for Do you want enable this Filter ?
        public string CrewDutyFilter
        {
            get { return _CrewDutyFilter; }
            set
            {
                if (value != _CrewDutyFilter)
                {
                    _CrewDutyFilter = value;
                }
            }
        }

        #endregion
    }
}