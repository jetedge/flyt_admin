﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InvoiceNumberBase
/// </summary>

namespace AbstractLayer
{
    public abstract class InvoiceNumberBase
    {
        private string _RowID;
        private string _ManageType;
        private string _ProjectShortCode;
        private string _ProcessShortCode;
        private string _Category;
        private string _AutoIncrement;
        private string _CreatedBy;
        private string _InvoiceNumbert;

        //for RowId
        public string RowId
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for ManageType
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }

        //for Project Short Code
        public string ProjectShortCode
        {
            get { return _ProjectShortCode; }
            set
            {
                if (value != _ProjectShortCode)
                {
                    _ProjectShortCode = value;
                }
            }
        }

        //for Process Short Code
        public string ProcessShortCode
        {
            get { return _ProcessShortCode; }
            set
            {
                if (value != _ProcessShortCode)
                {
                    _ProcessShortCode = value;
                }
            }
        }

        //for Category
        public string Category
        {
            get { return _Category; }
            set
            {
                if (value != _Category)
                {
                    _Category = value;
                }
            }
        }

        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        //for Auto Increment
        public string AutoIncrement
        {
            get { return _AutoIncrement; }
            set
            {
                if (value != _AutoIncrement)
                {
                    _AutoIncrement = value;
                }
            }
        }

        //for Invoice Number
        public string InvoiceNumbert
        {
            get { return _InvoiceNumbert; }
            set
            {
                if (value != _InvoiceNumbert)
                {
                    _InvoiceNumbert = value;
                }
            }
        }
    }
}