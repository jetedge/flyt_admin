﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbstractLayer
{
    public abstract class PricingConfigBase
    {
        private string _ManageType;
        private string _RowID;

        private string _MinTime;
        private string _MaxTime;
        private string _SellabaleHrs;
        private string _RunwayLength;
        private string _AvgSpeed;
        private string _BasAirport;
        private string _HourlyRate;
        private string _FixedFloor;
        private string _FixedCeiling;
        private string _AircraftType;
        private string _DOC;
        private string _Seats;
        private string _Active;
        private string _FET;

        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        public string MinTime
        {
            get { return _MinTime; }
            set
            {
                if (value != _MinTime)
                {
                    _MinTime = value;
                }
            }
        }
        public string MaxTime
        {
            get { return _MaxTime; }
            set
            {
                if (value != _MaxTime)
                {
                    _MaxTime = value;
                }
            }
        }
        public string SellabaleHrs
        {
            get { return _SellabaleHrs; }
            set
            {
                if (value != _SellabaleHrs)
                {
                    _SellabaleHrs = value;
                }
            }
        }
        public string RunwayLength
        {
            get { return _RunwayLength; }
            set
            {
                if (value != _RunwayLength)
                {
                    _RunwayLength = value;
                }
            }
        }
        public string AvgSpeed
        {
            get { return _AvgSpeed; }
            set
            {
                if (value != _AvgSpeed)
                {
                    _AvgSpeed = value;
                }
            }
        }
        public string BasAirport
        {
            get { return _BasAirport; }
            set
            {
                if (value != _BasAirport)
                {
                    _BasAirport = value;
                }
            }
        }
        public string HourlyRate
        {
            get { return _HourlyRate; }
            set
            {
                if (value != _HourlyRate)
                {
                    _HourlyRate = value;
                }
            }
        }
        public string FixedFloor
        {
            get { return _FixedFloor; }
            set
            {
                if (value != _FixedFloor)
                {
                    _FixedFloor = value;
                }
            }
        }
        public string FixedCeiling
        {
            get { return FixedCeiling; }
            set
            {
                if (value != _FixedCeiling)
                {
                    _FixedCeiling = value;
                }
            }
        }
        public string AircraftType
        {
            get { return AircraftType; }
            set
            {
                if (value != _AircraftType)
                {
                    _AircraftType = value;
                }
            }
        }
        public string DOC
        {
            get { return _DOC; }
            set
            {
                if (value != _DOC)
                {
                    _DOC = value;
                }
            }
        }
        public string Seats
        {
            get { return Seats; }
            set
            {
                if (value != _Seats)
                {
                    _Seats = value;
                }
            }
        }
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }
        public string FET
        {
            get { return _FET; }
            set
            {
                if (value != _FET)
                {
                    _FET = value;
                }
            }
        }
    }
}