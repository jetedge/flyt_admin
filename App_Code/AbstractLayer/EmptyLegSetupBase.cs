﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmptyLegSetupBase
/// </summary>
/// 
namespace AbstractLayer
{
    public abstract class EmptyLegSetupBase
    {
        private string _tailno;
        private string _flyingDate;
        private string _FlyingToDate;
        private string _StartTime;
        private string _EndTime;
        private string _StartAirport;
        private string _StartCity;
        private string _EndAirport;
        private string _EndCity;
        private string _NoOfSeats;
        private string _totalHoours;
        private string _CalculatelyHours;
        private string _HourlyRate;
        private string _FixedFloor;
        private string _FixedCeeling;
        private decimal _MinTime;
        private decimal _MaxTime;
        private string _MinSeallable;
        private string _TotalDays;
        private decimal _LMinPrice;
        private decimal _LMaxPrice;
        private decimal _FinalPrice;
        private string _Time;
        private string _Date;
        private string _FLAG;
        private string _CBPFFlag;
        private string _BookingFlag;
        private string _ApplyTax;
        private string _TripNo;
        private string _EnableTrip;
        private string _ACtive;
        private string _PriceFlag;
        private string _TailFlag;
        private string _CreatedBy;
        private string _CreatedOn;
        private string _TripFlag;
        private string _GroupId;
        private string _Mod_Flag;
        private string _Leg;
        private string _SplitType;
        private string _NewStartAirport;
        private string _NewEndAirport;



        //for TailNo
        public string TailNo
        {
            get { return _tailno; }
            set
            {
                if (value != _tailno)
                {
                    _tailno = value;
                }
            }
        }


        //for FlyingDate
        public string FlyingDate
        {
            get { return _flyingDate; }
            set
            {
                if (value != _flyingDate)
                {
                    _flyingDate = value;
                }
            }
        }


        //for FlyingToDate
        public string FlyingToDate
        {
            get { return _FlyingToDate; }
            set
            {
                if (value != _FlyingToDate)
                {
                    _FlyingToDate = value;
                }
            }
        }


        //for StartTime
        public string StartTime
        {
            get { return _StartTime; }
            set
            {
                if (value != _StartTime)
                {
                    _StartTime = value;
                }
            }
        }


        //for EndTime
        public string EndTime
        {
            get { return _EndTime; }
            set
            {
                if (value != _EndTime)
                {
                    _EndTime = value;
                }
            }
        }


        //for StartAirport
        public string StartAirport
        {
            get { return _StartAirport; }
            set
            {
                if (value != _StartAirport)
                {
                    _StartAirport = value;
                }
            }
        }


        //for StartCity
        public string StartCity
        {
            get { return _StartCity; }
            set
            {
                if (value != _StartCity)
                {
                    _StartCity = value;
                }
            }
        }


        //for EndAirport
        public string EndAirport
        {
            get { return _EndAirport; }
            set
            {
                if (value != _EndAirport)
                {
                    _EndAirport = value;
                }
            }
        }


        //for EndCity
        public string EndCity
        {
            get { return _EndCity; }
            set
            {
                if (value != _EndCity)
                {
                    _EndCity = value;
                }
            }
        }


        //for NoOfSeats
        public string NoOfSeats
        {
            get { return _NoOfSeats; }
            set
            {
                if (value != _NoOfSeats)
                {
                    _NoOfSeats = value;
                }
            }
        }


        //for totalHoours
        public string totalHoours
        {
            get { return _totalHoours; }
            set
            {
                if (value != _totalHoours)
                {
                    _totalHoours = value;
                }
            }
        }


        //for CalculatelyHours
        public string CalculatelyHours
        {
            get { return _CalculatelyHours; }
            set
            {
                if (value != _CalculatelyHours)
                {
                    _CalculatelyHours = value;
                }
            }
        }


        //for HourlyRate
        public string HourlyRate
        {
            get { return _HourlyRate; }
            set
            {
                if (value != _HourlyRate)
                {
                    _HourlyRate = value;
                }
            }
        }


        //for FixedFloor
        public string FixedFloor
        {
            get { return _FixedFloor; }
            set
            {
                if (value != _FixedFloor)
                {
                    _FixedFloor = value;
                }
            }
        }


        //for FixedCeeling
        public string FixedCeeling
        {
            get { return _FixedCeeling; }
            set
            {
                if (value != _FixedCeeling)
                {
                    _FixedCeeling = value;
                }
            }
        }


        //for MinTime
        public decimal MinTime
        {
            get { return _MinTime; }
            set
            {
                if (value != _MinTime)
                {
                    _MinTime = value;
                }
            }
        }


        //for MaxTime
        public decimal MaxTime
        {
            get { return _MaxTime; }
            set
            {
                if (value != _MaxTime)
                {
                    _MaxTime = value;
                }
            }
        }


        //for MinSeallable
        public string MinSeallable
        {
            get { return _MinSeallable; }
            set
            {
                if (value != _MinSeallable)
                {
                    _MinSeallable = value;
                }
            }
        }


        //for TotalDays
        public string TotalDays
        {
            get { return _TotalDays; }
            set
            {
                if (value != _TotalDays)
                {
                    _TotalDays = value;
                }
            }
        }


        //for LMinPrice
        public decimal LMinPrice
        {
            get { return _LMinPrice; }
            set
            {
                if (value != _LMinPrice)
                {
                    _LMinPrice = value;
                }
            }
        }


        //for LMaxPrice
        public decimal LMaxPrice
        {
            get { return _LMaxPrice; }
            set
            {
                if (value != _LMaxPrice)
                {
                    _LMaxPrice = value;
                }
            }
        }


        //for FinalPrice
        public decimal FinalPrice
        {
            get { return _FinalPrice; }
            set
            {
                if (value != _FinalPrice)
                {
                    _FinalPrice = value;
                }
            }
        }


        //for Time
        public string Time
        {
            get { return _Time; }
            set
            {
                if (value != _Time)
                {
                    _Time = value;
                }
            }
        }


        //for Date
        public string Date
        {
            get { return _Date; }
            set
            {
                if (value != _Date)
                {
                    _Date = value;
                }
            }
        }


        //for FLAG
        public string FLAG
        {
            get { return _FLAG; }
            set
            {
                if (value != _FLAG)
                {
                    _FLAG = value;
                }
            }
        }


        //for CBPFFlag
        public string CBPFFlag
        {
            get { return _CBPFFlag; }
            set
            {
                if (value != _CBPFFlag)
                {
                    _CBPFFlag = value;
                }
            }
        }


        //for BookingFlag
        public string BookingFlag
        {
            get { return _BookingFlag; }
            set
            {
                if (value != _BookingFlag)
                {
                    _BookingFlag = value;
                }
            }
        }


        //for ApplyTax
        public string ApplyTax
        {
            get { return _ApplyTax; }
            set
            {
                if (value != _ApplyTax)
                {
                    _ApplyTax = value;
                }
            }
        }


        //for TripNo
        public string TripNo
        {
            get { return _TripNo; }
            set
            {
                if (value != _TripNo)
                {
                    _TripNo = value;
                }
            }
        }


        //for EnableTrip
        public string EnableTrip
        {
            get { return _EnableTrip; }
            set
            {
                if (value != _EnableTrip)
                {
                    _EnableTrip = value;
                }
            }
        }


        //for ACtive
        public string ACtive
        {
            get { return _ACtive; }
            set
            {
                if (value != _ACtive)
                {
                    _ACtive = value;
                }
            }
        }


        //for PriceFlag
        public string PriceFlag
        {
            get { return _PriceFlag; }
            set
            {
                if (value != _PriceFlag)
                {
                    _PriceFlag = value;
                }
            }
        }


        //for TailFlag
        public string TailFlag
        {
            get { return _TailFlag; }
            set
            {
                if (value != _TailFlag)
                {
                    _TailFlag = value;
                }
            }
        }


        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }


        //for CreatedOn
        public string CreatedOn
        {
            get { return _CreatedOn; }
            set
            {
                if (value != _CreatedOn)
                {
                    _CreatedOn = value;
                }
            }
        }


        //for TripFlag
        public string TripFlag
        {
            get { return _TripFlag; }
            set
            {
                if (value != _TripFlag)
                {
                    _TripFlag = value;
                }
            }
        }


        //for GroupId
        public string GroupId
        {
            get { return _GroupId; }
            set
            {
                if (value != _GroupId)
                {
                    _GroupId = value;
                }
            }
        }


        //for Mod_Flag
        public string Mod_Flag
        {
            get { return _Mod_Flag; }
            set
            {
                if (value != _Mod_Flag)
                {
                    _Mod_Flag = value;
                }
            }
        }


        //for Leg
        public string Leg
        {
            get { return _Leg; }
            set
            {
                if (value != _Leg)
                {
                    _Leg = value;
                }
            }
        }


        //for SplitType
        public string SplitType
        {
            get { return _SplitType; }
            set
            {
                if (value != _SplitType)
                {
                    _SplitType = value;
                }
            }
        }


        //for NewStartAirport
        public string NewStartAirport
        {
            get { return _NewStartAirport; }
            set
            {
                if (value != _NewStartAirport)
                {
                    _NewStartAirport = value;
                }
            }
        }


        //for NewEndAirport
        public string NewEndAirport
        {
            get { return _NewEndAirport; }
            set
            {
                if (value != _NewEndAirport)
                {
                    _NewEndAirport = value;
                }
            }
        }


    }
}