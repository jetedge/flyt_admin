﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SchedulerBase
/// </summary>
/// 
namespace AbstractLayer
{
    public abstract class SchedulerBase
    {
        private string _ManageType;
        private string _RowID;
        private string _ToAddress;
        private string _ScheduleFrom;
        private string _Time;
        private string _Signature;
        private string _PostingTime;
        private string _RunFlag;
        private string _CreatedBy;
        private string _SchDays;
        private string _UserId;
        private string _MustMove;
        private string _ReportFor;

        private string _MessageFlag;
        private string _Message;
        private string _StartDate;
        private string _StartTime;
        private string _EndDate;
        private string _EndTime;
        private string _MessageID;

        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }


        //for Row ID
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for To Address
        public string ToAddress
        {
            get { return _ToAddress; }
            set
            {
                if (value != _ToAddress)
                {
                    _ToAddress = value;
                }
            }
        }

        //for Schedule From
        public string ScheduleFRom
        {
            get { return _ScheduleFrom; }
            set
            {
                if (value != _ScheduleFrom)
                {
                    _ScheduleFrom = value;
                }
            }
        }
        //for Time
        public string Time
        {
            get { return _Time; }
            set
            {
                if (value != _Time)
                {
                    _Time = value;
                }
            }
        }
        //for Signtaure
        public string Signature
        {
            get { return _Signature; }
            set
            {
                if (value != _Signature)
                {
                    _Signature = value;
                }
            }
        }
        //for PostingTime
        public string PostingTime
        {
            get { return _PostingTime; }
            set
            {
                if (value != _PostingTime)
                {
                    _PostingTime = value;
                }
            }
        }
        //for Phone
        public string RunFlag
        {
            get { return _RunFlag; }
            set
            {
                if (value != _RunFlag)
                {
                    _RunFlag = value;
                }
            }
        }

        //for Created By
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        //for User Id
        public string UserId
        {
            get { return _UserId; }
            set
            {
                if (value != _UserId)
                {
                    _UserId = value;
                }
            }
        }

        //for Sch Days
        public string SchDays
        {
            get { return _SchDays; }
            set
            {
                if (value != _SchDays)
                {
                    _SchDays = value;
                }
            }
        }

        //for Must Move
        public string MustMove
        {
            get { return _MustMove; }
            set
            {
                if (value != _MustMove)
                {
                    _MustMove = value;
                }
            }
        }

        //for Report For
        public string ReportFor
        {
            get { return _ReportFor; }
            set
            {
                if (value != _ReportFor)
                {
                    _ReportFor = value;
                }
            }
        }

        //for MessageFlag
        public string MessageFlag
        {
            get { return _MessageFlag; }
            set
            {
                if (value != _MessageFlag)
                {
                    _MessageFlag = value;
                }
            }
        }

        //for Message ID
        public string MessageID
        {
            get { return _MessageID; }
            set
            {
                if (value != _MessageID)
                {
                    _MessageID = value;
                }
            }
        }

        //for Message
        public string Message
        {
            get { return _Message; }
            set
            {
                if (value != _Message)
                {
                    _Message = value;
                }
            }
        }

        //for Start Date
        public string StartDate
        {
            get { return _StartDate; }
            set
            {
                if (value != _StartDate)
                {
                    _StartDate = value;
                }
            }
        }

        //for Start Time
        public string StartTime
        {
            get { return _StartTime; }
            set
            {
                if (value != _StartTime)
                {
                    _StartTime = value;
                }
            }
        }

        //for End Date
        public string EndDate
        {
            get { return _EndDate; }
            set
            {
                if (value != _EndDate)
                {
                    _EndDate = value;
                }
            }
        }

        //for End Time
        public string EndTime
        {
            get { return _EndTime; }
            set
            {
                if (value != _EndTime)
                {
                    _EndTime = value;
                }
            }
        }
    }
}