﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DiscountandCouponBase
/// </summary>
namespace AbstractLayer
{
    public abstract class DiscountandCouponBase
    {
        private string _ManageType;
        private string _RowID;

        private string _CouponCode;
        private string _CouponDescription;
        private string _StartDate;
        private string _ExpiryDate;
        private string _CouponValue;
        private string _CouponPercentage;
        private string _Discountflag;
        private string _Description;
        private string _Active;
        private string _UserFlag;
        private string _Createdby;
        private string _CreatedOn;
        private string _Modifiedby;
        private string _ModifiedOn;

        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        public string CouponCode
        {
            get { return _CouponCode; }
            set
            {
                if (value != _CouponCode)
                {
                    _CouponCode = value;
                }
            }
        }
        public string CouponDescription
        {
            get { return _CouponDescription; }
            set
            {
                if (value != _CouponDescription)
                {
                    _CouponDescription = value;
                }
            }
        }
        public string StartDate
        {
            get { return _StartDate; }
            set
            {
                if (value != _StartDate)
                {
                    _StartDate = value;
                }
            }
        }
        public string ExpiryDate
        {
            get { return _ExpiryDate; }
            set
            {
                if (value != _ExpiryDate)
                {
                    _ExpiryDate = value;
                }
            }
        }
        public string CouponValue
        {
            get { return _CouponValue; }
            set
            {
                if (value != _CouponValue)
                {
                    _CouponValue = value;
                }
            }
        }
        public string CouponPercentage
        {
            get { return _CouponPercentage; }
            set
            {
                if (value != _CouponPercentage)
                {
                    _CouponPercentage = value;
                }
            }
        }
        public string Discountflag
        {
            get { return _Discountflag; }
            set
            {
                if (value != _Discountflag)
                {
                    _Discountflag = value;
                }
            }
        }
        public string Description
        {
            get { return _Description; }
            set
            {
                if (value != _Description)
                {
                    _Description = value;
                }
            }
        }
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }
        public string UserFlag
        {
            get { return _UserFlag; }
            set
            {
                if (value != _UserFlag)
                {
                    _UserFlag = value;
                }
            }
        }

        public string Createdby
        {
            get { return _Createdby; }
            set
            {
                if (value != _Createdby)
                {
                    _Createdby = value;
                }
            }
        }
        public string CreatedOn
        {
            get { return _CreatedOn; }
            set
            {
                if (value != _CreatedOn)
                {
                    _CreatedOn = value;
                }
            }
        }
        public string Modifiedby
        {
            get { return _Modifiedby; }
            set
            {
                if (value != _Modifiedby)
                {
                    _Modifiedby = value;
                }
            }
        }
        public string ModifiedOn
        {
            get { return _ModifiedOn; }
            set
            {
                if (value != _ModifiedOn)
                {
                    _ModifiedOn = value;
                }
            }
        }


        private string _ApplicableFlag;
        private string _UserId;
        private string _EndDate;
        private string _AutoApply;
        private string _ApplicableFor;

        public string ApplicableFlag
        {
            get { return _ApplicableFlag; }
            set
            {
                if (value != _ApplicableFlag)
                {
                    _ApplicableFlag = value;
                }
            }
        }
        public string UserId
        {
            get { return _UserId; }
            set
            {
                if (value != _UserId)
                {
                    _UserId = value;
                }
            }
        }
        public string EndDate
        {
            get { return _EndDate; }
            set
            {
                if (value != _EndDate)
                {
                    _EndDate = value;
                }
            }
        }

        public string AutoApply
        {
            get { return _AutoApply; }
            set
            {
                if (value != _AutoApply)
                {
                    _AutoApply = value;
                }
            }
        }
        public string ApplicableFor
        {
            get { return _ApplicableFor; }
            set
            {
                if (value != _ApplicableFor)
                {
                    _ApplicableFor = value;
                }
            }
        }
    }
}