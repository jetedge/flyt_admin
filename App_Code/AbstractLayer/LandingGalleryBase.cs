﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LandingGalleryBase
/// </summary>

namespace AbstractLayer
{
    public abstract class LandingGalleryBase
    {
        private string _ManageType;
        private string _RowID;

        private string _ImagePath;
        private string _Image1;
        private string _Image2;
        private string _Image3;
        private string _Image4;
        private string _Notes;
        private string _Status;
        private string _UploadedBy;
        private string _SliderPosition;
        private string _ReportFlag;

        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        //for Row ID
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for ImagePath
        public string ImagePath
        {
            get { return _ImagePath; }
            set
            {
                if (value != _ImagePath)
                {
                    _ImagePath = value;
                }
            }
        }
        //for Image1
        public string Image1
        {
            get { return _Image1; }
            set
            {
                if (value != _Image1)
                {
                    _Image1 = value;
                }
            }
        }
        //for Image2
        public string Image2
        {
            get { return _Image2; }
            set
            {
                if (value != _Image2)
                {
                    _Image2 = value;
                }
            }
        }
        //for Image3
        public string Image3
        {
            get { return _Image3; }
            set
            {
                if (value != _Image3)
                {
                    _Image3 = value;
                }
            }
        }
        //for Image4
        public string Image4
        {
            get { return _Image4; }
            set
            {
                if (value != _Image4)
                {
                    _Image4 = value;
                }
            }
        }
        //for Notes
        public string Notes
        {
            get { return _Notes; }
            set
            {
                if (value != _Notes)
                {
                    _Notes = value;
                }
            }
        }
        //for Status
        public string Status
        {
            get { return _Status; }
            set
            {
                if (value != _Status)
                {
                    _Status = value;
                }
            }
        }
        //for UploadedBy
        public string UploadedBy
        {
            get { return _UploadedBy; }
            set
            {
                if (value != _UploadedBy)
                {
                    _UploadedBy = value;
                }
            }
        }
        //for SliderPosition
        public string SliderPosition
        {
            get { return _SliderPosition; }
            set
            {
                if (value != _SliderPosition)
                {
                    _SliderPosition = value;
                }
            }
        }

        //for ReportFlag
        public string ReportFlag
        {
            get { return _ReportFlag; }
            set
            {
                if (value != _ReportFlag)
                {
                    _ReportFlag = value;
                }
            }
        }

    }
}