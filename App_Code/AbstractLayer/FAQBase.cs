﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for FAQBase
/// </summary>
/// 
namespace AbstractLayer
{
    public abstract class FAQBase
    {
        private string _ManageType;
        private string _RowID;
        private string _topic;
        private string _question;
        private string _Answer;
        private string _DisSeq;
        private string _Active;
        private string _CreatedBy;

         //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        //for Rowid
        public string RowId
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for Topic
        public string Topic
        {
            get { return _topic; }
            set
            {
                if (value != _topic)
                {
                    _topic = value;
                }
            }
        }


        //for QUestion
        public string Question
        {
            get { return _question; }
            set
            {
                if (value != _question)
                {
                    _question = value;
                }
            }
        }

        //for Answer
        public string Answer
        {
            get { return _Answer; }
            set
            {
                if (value != _Answer)
                {
                    _Answer = value;
                }
            }
        }

        //for Active
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }

        //for DisSeq
        public string DisSeq
        {
            get { return _DisSeq; }
            set
            {
                if (value != _DisSeq)
                {
                    _DisSeq = value;
                }
            }
        }

        

        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }
        
    }
}