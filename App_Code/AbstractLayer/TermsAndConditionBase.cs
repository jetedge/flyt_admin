﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TermsAndConditionBase
/// </summary>
/// 
namespace AbstractLayer
{
    public abstract class TermsAndConditionBase
    {
        private string _ManageType;
        private string _RowID;
        private string _Version;
        private string _FileName;
        private string _FilePAth;
        private string _EfectiveFrom;
        private string _Notes;
        private string _CreatedBy;

        //for Manage Type
        public string ManageType 
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        //for Rowid
        public string RowId
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }
        //for Version
        public string Version
        {
            get { return _Version; }
            set
            {
                if (value != _Version)
                {
                    _Version = value;
                }
            }
        }

        //for FileName
        public string FileName
        {
            get { return _FileName; }
            set
            {
                if (value != _FileName)
                {
                    _FileName = value;
                }
            }
        }

        //for FilePath
        public string FilePAth
        {
            get { return _FilePAth; }
            set
            {
                if (value != _FilePAth)
                {
                    _FilePAth = value;
                }
            }
        }

        //for Effective From
        public string EffectiveFrom
        {
            get { return _EfectiveFrom; }
            set
            {
                if (value != _EfectiveFrom)
                {
                    _EfectiveFrom = value;
                }
            }
        }

        //for Notes
        public string Notes
        {
            get { return _Notes; }
            set
            {
                if (value != _Notes)
                {
                    _Notes = value;
                }
            }
        }

        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }
    }
}