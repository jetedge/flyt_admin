﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserPreferenceBase
/// </summary>
/// 

namespace AbstractLayer
{
    public abstract class UserPreferenceBase
    {
        private string _RowID;
        private string _StartAirport;
        private string _EndAirport;
        private string _StartCity;
        private string _EndCity;
        private string _AvailableDate;
        private string _AvailableToDate;
        private string _AvailableTime;
        private string _EmailFlag;
        private string _TimeFlag;
        private string _SMSFlag;

        //for Row Id
        public string RowID
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }

        //for Start Airport
        public string StartAirport
        {
            get { return _StartAirport; }
            set
            {
                if (value != _StartAirport)
                {
                    _StartAirport = value;
                }
            }
        }

        //for End Airport
        public string EndAirport
        {
            get { return _EndAirport; }
            set
            {
                if (value != _EndAirport)
                {
                    _EndAirport = value;
                }
            }
        }

        //for Start City
        public string StatCity
        {
            get { return _StartCity; }
            set
            {
                if (value != _StartCity)
                {
                    _StartCity = value;
                }
            }
        }

        //for End City
        public string EndCity
        {
            get { return _EndCity; }
            set
            {
                if (value != _EndCity)
                {
                    _EndCity = value;
                }
            }
        }

        //for Available Date
        public string AvailableDate
        {
            get { return _AvailableDate; }
            set
            {
                if (value != _AvailableDate)
                {
                    _AvailableDate = value;
                }
            }
        }

        //for Available To Date
        public string AvailableToDate
        {
            get { return _AvailableToDate; }
            set
            {
                if (value != _AvailableToDate)
                {
                    _AvailableToDate = value;
                }
            }
        }

        //for Available Time
        public string AvailableTime
        {
            get { return _AvailableTime; }
            set
            {
                if (value != _AvailableTime)
                {
                    _AvailableTime = value;
                }
            }
        }

        //for Email Flag
        public string EmailFlag
        {
            get { return _EmailFlag; }
            set
            {
                if (value != _EmailFlag)
                {
                    _EmailFlag = value;
                }
            }
        }

        //for Time Flag
        public string TimeFlag
        {
            get { return _TimeFlag; }
            set
            {
                if (value != _TimeFlag)
                {
                    _TimeFlag = value;
                }
            }
        }

        //for SMS Flag
        public string SMSFlag
        {
            get { return _SMSFlag; }
            set
            {
                if (value != _SMSFlag)
                {
                    _SMSFlag = value;
                }
            }
        }
    }
}