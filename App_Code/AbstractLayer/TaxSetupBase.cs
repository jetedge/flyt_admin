﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TaxSetupBase
/// </summary>
/// 
namespace AbstractLayer
{
    public abstract class TaxSetupBase
    {
        private string _ManageType;
        private string _RowID;
        private string _Code;
        private string _Name;
        private string _Description;
        private string _PercntageSymbol;
        private string _Tax;
        private string _EffectiveFrom;
        private string _Notes;
        private string _Active;
        private string _CreatedBy;
        private string _TaxFlag;

        //for Manage Type
        public string ManageType
        {
            get { return _ManageType; }
            set
            {
                if (value != _ManageType)
                {
                    _ManageType = value;
                }
            }
        }
        //for Rowid
        public string RowId
        {
            get { return _RowID; }
            set
            {
                if (value != _RowID)
                {
                    _RowID = value;
                }
            }
        }
        //for Code
        public string Code
        {
            get { return _Code; }
            set
            {
                if (value != _Code)
                {
                    _Code = value;
                }
            }
        }
        //for Name
        public string Name
        {
            get { return _Name; }
            set
            {
                if (value != _Name)
                {
                    _Name = value;
                }
            }
        }
        //for Description
        public string Description
        {
            get { return _Description; }
            set
            {
                if (value != _Description)
                {
                    _Description = value;
                }
            }
        }
        //for Percentage Symbol
        public string PercentageSymbol
        {
            get { return _PercntageSymbol; }
            set
            {
                if (value != _PercntageSymbol)
                {
                    _PercntageSymbol = value;
                }
            }
        }
        //for Tax
        public string Tax
        {
            get { return _Tax; }
            set
            {
                if (value != _Tax)
                {
                    _Tax = value;
                }
            }
        }
        //for Eff From
        public string EffectiveFrom
        {
            get { return _EffectiveFrom; }
            set
            {
                if (value != _EffectiveFrom)
                {
                    _EffectiveFrom = value;
                }
            }
        }
        //for Notes
        public string Notes
        {
            get { return _Notes; }
            set
            {
                if (value != _Notes)
                {
                    _Notes = value;
                }
            }
        }
        //for CreatedBy
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value != _CreatedBy)
                {
                    _CreatedBy = value;
                }
            }
        }

        //for Active
        public string Active
        {
            get { return _Active; }
            set
            {
                if (value != _Active)
                {
                    _Active = value;
                }
            }
        }

        //for Tax Flag
        public string TaxFlag
        {
            get { return _TaxFlag; }
            set
            {
                if (value != _TaxFlag)
                {
                    _TaxFlag = value;
                }
            }
        }
    }
}