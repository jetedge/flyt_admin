﻿function ValidateEmail(inputText) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.value.match(mailformat)) {
        document.form1.text1.focus();
        return true;
    }
    else {
        document.getElementById('lblalert').innerHTML = 'You have entered an invalid email address!';
        $find("mpealert").show();
        document.getElementById("txtEmail").focus();
        inputText.value = "";
        //return false;
    }
}
function Numerics(evt) {
    var keyCode = window.event ? event.keyCode : evt.which ? evt.which : evt.keyCode ? evt.keyCode : evt.charCode;

    if ((keyCode > 47 && keyCode <= 57) || (keyCode == 8) || (keyCode == 9)) {
        return true;
    }
    else {

        return false;
    }
}
function Alphabets(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32 && keyCode != 9 && keyCode != 8 && keyCode != 46)
        return false;
    return true;
}

function date1(inputField) {

    var isValid = /^\d+(?:\.\d{1,2})?$/.test(inputField.value);
    if (isValid) {
    } else {
        inputField.value = '0.0'
    }
    return isValid;
}

function Amount(inputField) {

    var isValid = /^\d{0,6}(\.\d{1,2})?$/.test(inputField.value);
    if (isValid) {
    }
    else {
        var number = inputField.value;
        var n = parseFloat(number).toFixed(2);
        inputField.value = n;
    }
    return isValid;
}

function Contains(password, validChars) {

    for (i = 0; i < password.length; i++) {

        var char = password.charAt(i);
        if (validChars.indexOf(char) > -1) {
            return true;
        }
    }
    return false;
}

function NumericsIphon(evt) {

    var keyCode = window.event ? event.keyCode : evt.which ? evt.which : evt.keyCode ? evt.keyCode : evt.charCode;

    if ((keyCode == 45) || (keyCode > 47 && keyCode <= 57) || (keyCode == 8) || (keyCode == 9)) {
        return true;
    }
    else {

        return false;
    }
}
function isNumberKeyNoDot(evt, id) {
    try {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ((charCode > 31) && (charCode < 48 || charCode > 57 || charCode == 46))
            return false;
    } catch (w) {
        alert(w);
    }
}
function isNumberKeyPer(evt, id) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    var txt = document.getElementById(id).value;
    if (txt > 100.00) {
        //   alert('Enter valid Percentage!');
        document.getElementById('lblalert').innerHTML = 'Enter valid Percentage!';
        $find("mpealert").show();
        document.getElementById(id).value = '';
        document.getElementById(id).focus();
    }
    return true;
}

function Numericswithdot(evt) {
    var keyCode = window.event ? event.keyCode : evt.which ? evt.which : evt.keyCode ? evt.keyCode : evt.charCode;

    if ((keyCode > 47 && keyCode <= 57) || (keyCode == 8) || (keyCode == 9) || (keyCode == 46)) {
        return true;
    }
    else {

        return false;
    }
}

function Numericszero(evt) {
    var keyCode = window.event ? event.keyCode : evt.which ? evt.which : evt.keyCode ? evt.keyCode : evt.charCode;

    if ((keyCode > 48 && keyCode <= 57) || (keyCode == 8) || (keyCode == 9)) {
        return true;
    }
    else {

        return false;
    }
}
function Flyinghours(inputField) {

    var isValid = /^\d{0,2}(\.\d{1,2})?$/.test(inputField.value);
    if (isValid) {
    } else {
        inputField.value = '00.00'
        
    }
    return isValid;
}
