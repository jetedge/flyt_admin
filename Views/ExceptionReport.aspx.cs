﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class Views_ExceptionReport : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.ExceptionReportBLL objMember = new BusinessLayer.ExceptionReportBLL();

    #endregion

    #region Pageload

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Reports", "Exception Report");
                }
               

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Reports";
                lblSubHeader.InnerText = "Exception Report";
                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                txtfromdate.Text = DateTime.Now.AddDays(-7).ToString("MM-dd-yyyy");
                txttodate.Text = DateTime.Now.ToString("MM-dd-yyyy");
                BindEmptyLegs();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion


    #region Filter Events

    protected void ddlstatus_onselectedChanged(object sender, EventArgs e)
    {
        BindEmptyLegs();
    }
    
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            dvGrid.Visible = true;
            dvheader.Visible = true;
            dvheader1.Visible = true;
            gvTailDetails.Columns[9].Visible = false;
            gvTailDetails.Columns[10].Visible = true;
            gvTailDetails.Columns[11].Visible = false;
            gvTailDetails.Columns[12].Visible = true;
            Response.ClearContent();
            Response.AppendHeader("content-disposition", "attachment;filename=Exception" + Convert.ToDateTime(txtfromdate.Text).ToString("MM-dd-yyyy") + "_" + Convert.ToDateTime(txttodate.Text).ToString("MM-dd-yyyy") + ".xls");
            Response.Charset = "";
            Response.ContentType = ("application/vnd.xls;charset=iso-8859-1");
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            tblGrid.RenderControl(htw);

            Response.Write(sw.ToString());
            Response.End();
            // tblHeaderExcel.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    { }
    public void txtfromdate_changed(object sender, EventArgs e)
    {
        try
        {
            BindEmptyLegs();
        }
        catch
        {
        }
    }
    public void txtto_changed(object sender, EventArgs e)
    {
        try
        {
            BindEmptyLegs();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public void textBox1_TextChanged(object sender, EventArgs e)
    {
        try
        {
            BindFilterGrid();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public void txttripno_TextChanged(object sender, EventArgs e)
    {
        try
        {
            BindFilterGrid();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion

    #region UserDefinedFunction

    public void BindEmptyLegs()
    {
        if (ddlstatus.SelectedValue == "Fail")
        {
            DataTable dtOneWayTrip = objMember.Select(ddlstatus.SelectedValue, Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd"), Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd"));

            if (dtOneWayTrip.Rows.Count > 0)
            {
                decimal total = 0;
                for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                {
                    total = total + Convert.ToDecimal(dtOneWayTrip.Rows[i]["TotalAmount"].ToString());
                }
                DataRow row = dtOneWayTrip.NewRow();
                dtOneWayTrip.Rows.Add(row);
                row["InvoiceNumber"] = "Total Amount";
                row["Totalamount"] = total;

                gvTailDetails.DataSource = dtOneWayTrip;
                gvTailDetails.DataBind();
                for (int row1 = 0; row1 < gvTailDetails.Rows.Count; row1++)
                {
                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#D5E2E6");
                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].Font.Bold = true;
                }
            }
            else
            {
                gvTailDetails.DataSource = null;
                gvTailDetails.DataBind();
            }
        }
        else
        {
            DataTable dtOneWayTrip = objMember.Select(ddlstatus.SelectedValue.Trim(), Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd"), Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd"));

            DataTable dtclone = new DataTable();
            if (dtOneWayTrip.Rows.Count > 0)
            {
                for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                {
                    string strtailno = dtOneWayTrip.Rows[i]["tailno"].ToString();
                    string strtripno = dtOneWayTrip.Rows[i]["tripno"].ToString();

                    DataTable dtOneWayTrip1 = objMember.Child("CH", Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd"), Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd"), strtailno, strtripno);

                    dtclone = dtOneWayTrip1.Clone();
                    if (dtOneWayTrip1.Rows.Count > 1)
                    {
                        dtclone.Merge(dtOneWayTrip1);

                    }

                }
                gvTailDetails.DataSource = dtclone;
                gvTailDetails.DataBind();

            }
            if (dtclone.Rows.Count > 0)
            {
                decimal total = 0;
                for (int i = 0; i < dtclone.Rows.Count; i++)
                {
                    total = total + Convert.ToDecimal(dtclone.Rows[i]["TotalAmount"].ToString());
                }
                DataRow row = dtclone.NewRow();
                dtclone.Rows.Add(row);
                row["InvoiceNumber"] = "Total Amount";
                row["Totalamount"] = total;
                gvTailDetails.DataSource = dtclone;
                gvTailDetails.DataBind();
                for (int row1 = 0; row1 < gvTailDetails.Rows.Count; row1++)
                {
                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#D5E2E6");
                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].Font.Bold = true;
                }
            }
            else
            {
                gvTailDetails.DataSource = null;
                gvTailDetails.DataBind();
            }

        }
    }

    public void BindFilterGrid()
    {
        if (ddlstatus.SelectedValue == "Fail")
        {
            DataTable dtOneWayTrip = objMember.Child("CH", Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd"), Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd"), txtTailFilter.Text, txttripno.Text);

            if (dtOneWayTrip.Rows.Count > 0)
            {
                decimal total = 0;
                for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                {
                    total = total + Convert.ToDecimal(dtOneWayTrip.Rows[i]["TotalAmount"].ToString());
                }
                DataRow row = dtOneWayTrip.NewRow();
                dtOneWayTrip.Rows.Add(row);
                row["InvoiceNumber"] = "Total Amount";
                row["Totalamount"] = total;

                gvTailDetails.DataSource = dtOneWayTrip;
                gvTailDetails.DataBind();
                for (int row1 = 0; row1 < gvTailDetails.Rows.Count; row1++)
                {

                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#D5E2E6");
                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].Font.Bold = true;
                }
            }
            else
            {
                gvTailDetails.DataSource = null;
                gvTailDetails.DataBind();
            }
        }
        else
        {
            DataTable dtOneWayTrip = objMember.Child("CH",Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd"),Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd"),txtTailFilter.Text,txttripno.Text);

            if (dtOneWayTrip.Rows.Count > 0)
            {
                decimal total = 0;
                for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                {
                    total = total + Convert.ToDecimal(dtOneWayTrip.Rows[i]["TotalAmount"].ToString());
                }
                DataRow row = dtOneWayTrip.NewRow();
                dtOneWayTrip.Rows.Add(row);
                row["InvoiceNumber"] = "Total Amount";
                row["Totalamount"] = total;
                gvTailDetails.DataSource = dtOneWayTrip;
                gvTailDetails.DataBind();
                for (int row1 = 0; row1 < gvTailDetails.Rows.Count; row1++)
                {
                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#D5E2E6");
                    gvTailDetails.Rows[gvTailDetails.Rows.Count - 1].Font.Bold = true;

                }
            }
            else
            {
                gvTailDetails.DataSource = null;
                gvTailDetails.DataBind();
            }

        }
    }

    void funFileDownload(string Location)
    {
        try
        {
            FileInfo file = new FileInfo(Location);
            Session.Contents.Remove("FullFileName");
            if (file.Exists)
            {
                Response.Buffer = false;
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(Location));
                Response.TransmitFile(Location);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion

    #region  Gridvie Events

    protected void gvTailDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LinkButton Invoicenumber = (LinkButton)gvTailDetails.SelectedRow.FindControl("lnkbtn");
            string str = @"D:\jetedgefiles\EMPTYLEG\" + Invoicenumber.Text + ".pdf";
            funFileDownload(str);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    
    protected void gvTailDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            BindEmptyLegs();
            gvTailDetails.PageIndex = e.NewPageIndex;
            gvTailDetails.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }

    }
    protected void gvpricing_price_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label Label2 = e.Row.FindControl("Label2") as Label;
                if (Label2.Text.Length > 0)
                {
                    if (Convert.ToDateTime(Label2.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        Label2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        Label2.Font.Bold = true;
                    }
                    else
                    {
                        Label2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void ContactsGridView_RowCommandNew(Object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string invoice = string.Empty;

            if (e.CommandName == "Status")
            {
                int rowIndex = Convert.ToInt32(e.CommandArgument);

                //Reference the GridView Row.
                GridViewRow row = gvTailDetails.Rows[rowIndex];
                //Fetch value of Name.
                invoice = (row.FindControl("lblinvoice") as Label).Text;
            }

            DataTable dtPS = objMember.Booking(invoice);

            if (dtPS.Rows.Count > 0)
            {
                lblbookedname.Text = dtPS.Rows[0]["CFirstname"].ToString() + " " + dtPS.Rows[0]["CLastname"].ToString();
                lblbookeddate.Text = dtPS.Rows[0]["Transdate"].ToString();
                lbltransaction.Text = dtPS.Rows[0]["Paymentstatus"].ToString();
                lbltransid.Text = dtPS.Rows[0]["transid"].ToString();
                lblamount.Text = Convert.ToDecimal(dtPS.Rows[0]["totalamount"].ToString()).ToString("c2");
                lblmblno.Text = dtPS.Rows[0]["CPhone"].ToString();

            }
            else
            {
                lblbookedname.Text = "";
                lblbookeddate.Text = "";
                lbltransaction.Text = "";
                lbltransid.Text = "";
                lblamount.Text = "";
                lblmblno.Text = "";
            }

            mpetrans.Show();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion
}