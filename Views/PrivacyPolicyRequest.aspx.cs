﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net;
using Microsoft.Exchange.WebServices.Data;

public partial class Views_PrivacyPolicyRequest : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.PrivacyEmailBLL objPrivacy = new BusinessLayer.PrivacyEmailBLL();

    #endregion


    public int linenum = 0;
    public string MethodName = string.Empty;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            try
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Privacy Policy Request");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Admin";
                lblSubHeader.InnerText = "Privacy Policy Request";

                List();
            }
            catch (System.Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }

    }

    public void List()
    {
        DataTable dtPrivacy = objPrivacy.SelectPR();
        if (dtPrivacy.Rows.Count > 0)
        {
            gvRemoval.DataSource = dtPrivacy;
            gvRemoval.DataBind();
        }
        else
        {
            gvRemoval.DataSource = null;
            gvRemoval.DataBind();

        }
    }

    protected void gvRemoval_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblPID = (Label)gvRemoval.SelectedRow.FindControl("lblPID");

            mpeRemoval.Show();

            lblPopCountry.Text = ((Label)gvRemoval.SelectedRow.FindControl("lblCountryName")).Text;
            lblPopEmail.Text = ((Label)gvRemoval.SelectedRow.FindControl("lblEmailAddress")).Text;
            lblPopFirstname.Text = ((Label)gvRemoval.SelectedRow.FindControl("lblFirstName")).Text;
            lblPopLastname.Text = ((Label)gvRemoval.SelectedRow.FindControl("lblLastName")).Text;
            lblPopSignedDate.Text = Convert.ToDateTime(((Label)gvRemoval.SelectedRow.FindControl("lblSignedDate")).Text).ToString("MMM d, yyyy");

            lblPopReason.Text = ((Label)gvRemoval.SelectedRow.FindControl("lblReason")).Text;
            lblPopRequestNumber.Text = ((Label)gvRemoval.SelectedRow.FindControl("lblRequestNumber")).Text;
            lblPopRequestOn.Text = Convert.ToDateTime(((Label)gvRemoval.SelectedRow.FindControl("lblRequestedOn")).Text).ToString("MMM d, yyyy hh:mm:ss tt");
            lblPopIPAddress.Text = ((Label)gvRemoval.SelectedRow.FindControl("lblIPAddress")).Text;

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }
}