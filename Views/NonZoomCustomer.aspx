﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="NonZoomCustomer.aspx.cs" Inherits="Views_NonZoomCustomer" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="kt-portlet">
                            <div class="form-group row">
                                <div class="col-xl-12 text-right" style="padding-right: 2%;">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add Customer" CssClass="btn btn-danger"
                                        TabIndex="1" />
                                    <asp:Button ID="btnSch" runat="server" Text="Scheduler" CssClass="btn btn-danger"
                                        TabIndex="1" Visible="false" PostBackUrl="Scheduler.aspx" />
                                    <label id="lblMan" runat="server" class="importantlabels" visible="false">
                                        <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                            ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                                    </label>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                        ValidationGroup="SP" />
                                    <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                                        OnClick="btnViewOnClick" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                        runat="server" ShowSummary="false" />
                                </div>
                            </div>
                            <div class="row m-0" id="tblGrid" runat="server">
                                <div class="col-xl-12 table-responsive boxshadow">
                                    <asp:GridView ID="gvUser" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                        Width="100%" AutoGenerateColumns="false" OnSelectedIndexChanged="gvPricingDetails_SelectedIndexChanged"
                                        PageSize="50" AllowPaging="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    <asp:Label ID="lblrowid" Text='<%# Eval("RowID") %>' runat="server" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="First Name" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%"
                                                ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfirstName" Text='<%# Eval("firstName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                                ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastName" Text='<%# Eval("lastname") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="25%"
                                                ItemStyle-Width="25%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" Text='<%# Eval("Email") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Company" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="25%"
                                                ItemStyle-Width="25%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCompany" Text='<%# Eval("CompanyName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phone Number" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%"
                                                ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPhone" Text='<%# Eval("PhoneNumber") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Active" HeaderStyle-Width="3%" ItemStyle-Width="3%"
                                                HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                                HeaderStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkactiveM" Enabled="false" TabIndex="7" runat="server" Checked='<%# Convert.ToBoolean(Eval("ActiveAirport")) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                                ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbView1" runat="server" TabIndex="2" ImageUrl="~/Images/pencil_Edit.png"
                                                        CommandName="Select" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                                ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbDelete" runat="server" TabIndex="3" ImageUrl="~/Images/icon_delete.png"
                                                        CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row" id="tblForm" runat="server" visible="false">
                                <div class="col-xl-12" style="padding-left: 4% !important; padding-right: 4% !important;">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Title
                                        </label>
                                        <div class="col-4">
                                            <asp:DropDownList ID="ddlgender" runat="server" TabIndex="4" class="cusinputuser">
                                                <asp:ListItem>Mr</asp:ListItem>
                                                <asp:ListItem>Mrs</asp:ListItem>
                                                <asp:ListItem>Miss</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            Address1
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TabIndex="10"
                                                MaxLength="200" autocomplete="off" ToolTip="Enter all characters.Length is limited to 200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            First Name <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtFname" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFname"
                                                Display="None" ErrorMessage="First Name is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            Address2
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control" TabIndex="11"
                                                autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Last Name <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" TabIndex="6"
                                                MaxLength="100" autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                                                Display="None" ErrorMessage="Last Name is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            City
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" TabIndex="12" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Email <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" TabIndex="7" autocomplete="off"
                                                MaxLength="200" ToolTip="Enter all characters. Length is limited to 200" onchange="ValidateEmail(this)"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail"
                                                Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            State
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtState" runat="server" CssClass="form-control" TabIndex="13" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label" style="padding-right: 0px;">
                                            Company Name <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtComName" runat="server" CssClass="form-control" TabIndex="7"
                                                autocomplete="off" MaxLength="100" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtComName"
                                                Display="None" ErrorMessage="Company Name is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            Country <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <%--<asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" TabIndex="14"
                                                MaxLength="100" autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control " TabIndex="14">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlCountry"
                                                InitialValue="0" Display="None" ErrorMessage="Country is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Phone #
                                        </label>
                                        <div class="col-2 p-l-0 " style="display: none;">
                                            <%-- <asp:TextBox ID="txtPhoneCode" runat="server" CssClass="form-control" TabIndex="8"
                                                MaxLength="3" autocomplete="off" ToolTip="Enter all characters. Length is limited to 3"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlPhoneCode" runat="server" CssClass="form-control " TabIndex="8">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" TabIndex="8" autocomplete="off"
                                                MaxLength="15" Width="100%" ToolTip="Enter Valid Phone NUmber. Length is limited to 15"></asp:TextBox>
                                        </div>
                                        <label class="col-2  col-form-label">
                                            Zip Code
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtZip" runat="server" CssClass="form-control" TabIndex="15" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Active
                                        </label>
                                        <div class="col-4" style="padding-top: 15px;">
                                            <asp:CheckBox ID="chkActive" Text="Check for Active" Checked="true" TabIndex="9"
                                                runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                            padding-top: 5px !important; padding-bottom: 5px; padding-top: 10px; width: 40%;
                                            border-bottom: 1px solid #C0C0C0 !important;">
                                            Must Move Mail Configuration
                                        </label>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">
                                            Do you want to recieve <span style="font-weight: 600;">MUST MOVE</span> mail ?
                                        </label>
                                        <div class="col-2" style="padding-top: 7px;">
                                            <asp:RadioButtonList ID="rblMustMoveMail" runat="server" RepeatDirection="Horizontal"
                                                CssClass="cusinputuser123" TabIndex="17" RepeatColumns="2" Width="70%">
                                                <asp:ListItem Selected="True" Value="Y">&nbsp;Yes&nbsp;&nbsp;</asp:ListItem>
                                                <asp:ListItem Value="N">&nbsp;No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="divReason" runat="server" visible="false">
                                        <label class="col-2 col-form-label" style="max-width: 12%;">
                                            Reason
                                        </label>
                                        <label class="col-10 col-form-label">
                                            <asp:Label ID="lblReason" runat="server" Style="font-weight: 600;"></asp:Label>
                                        </label>
                                    </div>
                                    <div class="form-group row" id="divUnSub" runat="server" visible="false">
                                        <label class="col-2 col-form-label" style="max-width: 12%;">
                                            Unsubscribed By
                                        </label>
                                        <label class="col-10 col-form-label">
                                            <asp:Label ID="lblUnSubBy" runat="server" Style="font-weight: 600;"></asp:Label>
                                        </label>
                                    </div>
                                    <div class="form-group row">
                                    </div>
                                    <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                        <div style="float: left; padding-top: 10px; padding-left: 15px">
                                            <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                                font-size: 15px; font-weight: 600;"></asp:Label>
                                            <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600; font-size: 15px"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="lblMessage" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
