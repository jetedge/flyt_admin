﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LandingPage.aspx.cs" Inherits="Views_LandingPage"
    EnableEventValidation="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>.:: Jet Egde | FLYT ::.</title>
    <meta name="description" content="Latest updates and statistic charts" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="shortcut icon" href="../assets/img/Logo_Tra.gif" />
    <link href="../assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="../assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/demo/demo4/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
       
        .kt-header__topbar .kt-header__topbar-item
        {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: stretch;
            margin: 1.4rem 0.25rem;
        }
        .book-text
        {
            position: relative;
            text-align: center;
            font-size: 14px;
            line-height: 40px;
            font-weight: 400;
            letter-spacing: 2px;
            text-transform: uppercase;
        }
        .book-a-flight-box
        {
            position: fixed;
            left: -2%;
            top: 20vh;
            z-index: 998;
            width: 0vw;
            padding-top: 10px;
            padding-bottom: 10px;
            background-color: rgba(0, 0, 0, 0.95);
        }
        .book-button-in
        {
            width: 420px;
            height: 40px;
            -webkit-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }
        .form-botton
        {
            width: 50px;
            position: fixed;
            top: 50vh;
            right: 0;
            left: -6px;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-pack: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            color: #fff;
        }
        .buttongrid
        {
            display: inline-block;
            text-align: center;
            vertical-align: middle;
            padding: 5px 5px;
            border: 1px solid rgb(178, 30, 40);
            border-radius: 2px;
            background-color: rgb(178, 30, 40);
            font-size: 12px;
            font-family: Gotham;
            color: #FFFFFF;
            text-decoration: none;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        function AutoHeightPopup(obj) {

            var barHeight = (window.screenTop) || (window.screen.availHeight - window.innerHeight + 30);
            var browserName = navigator.appName;
            var value = 100;
            if (browserName != 'Microsoft Internet Explorer') {
                value = 55;
            }
            obj.height = window.screen.availHeight - (value + barHeight);
            var urlStr = obj.contentDocument.URL;
            if (urlStr.substring(urlStr.length - 10) == '../Login.aspx') {
                obj.src = '../SessionExp.htm';
            }
        }
    </script>
</head>
<%--style="background-image: url(../assets/demo/demo4/media/bg/header.jpg); background-position: center top;
    background-size: 100% 350px;"--%>
<body class="kt-page--loading-enabled kt-page--loading kt-page--fixed kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scmZoom123" runat="server">
    </asp:ScriptManager>
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="LandingPage.aspx">
                <img alt="Logo" src="../assets/img/whitelogo.png" style="height: 53px;" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler">
                <span></span>
            </button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler">
                <i class="flaticon-more-1"></i>
            </button>
        </div>
    </div>
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper "
                id="kt_wrapper">
                <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on"
                    runat="server">
                    <div class="kt-container">
                        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                            <a class="kt-header__brand-logo" href="index.html">
                                <img alt="Logo" src="../assets/img/whitelogo.png" style="height: 53px;" class="kt-header__brand-logo-default" />
                                <img alt="Logo" src="../assets/img/whitelogo.png" class="kt-header__brand-logo-sticky" />
                            </a>
                        </div>
                        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn">
                            <i class="la la-close"></i>
                        </button>
                        <div class="kt-header__topbar kt-grid__item">
                            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                <asp:Button ID="btlLoIN" runat="server" OnClick="OpenLogin_Click" CssClass="btn btn-danger"
                                    Text="LOGIN" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
                    <div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                            <img src="../Images/flightHome.jpg" style="height: 350px;" />
                            <div class="row">
                                <div class="col-lg-1">
                                </div>
                                <div class="col-lg-10 text-center">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <label style="text-decoration: underline; letter-spacing: 0.1vw; font-size: 1.5vw;
                                                font-weight: bold; text-transform: uppercase;">
                                                CURRENT FLYT LISTINGS
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <label style="letter-spacing: 0.1vw; font-size: 1.5vw; font-weight: bold; text-transform: uppercase;">
                                                ALL FLIGHTS OFFERED ON JET EDGE’S <a href="https://www.flyjetedge.com/fleet" target="_blank"
                                                    id="btnok" runat="server" style="color: Blue; text-decoration: none">FLEET</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <asp:GridView runat="server" ID="gvSimpleSearch" AutoGenerateColumns="false" PagerStyle-CssClass="pager"
                                                CssClass="table" HeaderStyle-CssClass="thead-dark" Width="100%" role="grid" Style="padding-left: 0px;
                                                margin-left: 0px" OnRowDataBound="gvSimpleSearch_OnRowDataBound" OnSelectedIndexChanged="gvSimpleSearch_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:TemplateField Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblrowid" runat="server" Text='<%# Eval("Sno") %>' Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblTailNo" runat="server" Text='<%# Eval("TailNo") %>' Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblDtime" runat="server" Text='<%# Eval("DTime") %>' Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblAtime" runat="server" Text='<%# Eval("ATime") %>' Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblstartcity" runat="server" Text='<%# Eval("StartCity") %>' Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblEndcity" runat="server" Text='<%# Eval("EndCity") %>' Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblDDate" runat="server" Text='<%# Eval("DDate", "{0:MMM d yyyy}") %>'
                                                                Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblstart" Style="font-size: 16pt" runat="server" Text='<%# Eval("Start") %>'
                                                                Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblEnd" Style="font-size: 16pt" runat="server" Text='<%# Eval("End") %>'
                                                                Visible="false"> </asp:Label>
                                                            <asp:Label ID="lblFlyingHours" runat="server" Visible="false" Text='<%# Eval("FLTTIMEMINS") %>'></asp:Label>
                                                            <asp:Label ID="lblroutes" runat="server" Visible="false" Text='<%# Eval("routes") %>'></asp:Label>
                                                            <asp:Label ID="lbltripno" runat="server" Visible="false" Text='<%# Eval("tripno") %>'></asp:Label>
                                                            <asp:Label ID="lbltailflag" runat="server" Visible="false" Text='<%# Eval("tailflag") %>'></asp:Label>
                                                            <asp:Label ID="lbltripflag" runat="server" Visible="false" Text='<%# Eval("tripflag") %>'></asp:Label>
                                                            <asp:Label ID="lblgroupid" runat="server" Visible="false" Text='<%# Eval("groupid") %>'></asp:Label>
                                                            <asp:Label ID="lblADate" runat="server" Visible="false" Text='<%# Eval("ADate") %>'></asp:Label>
                                                            <asp:Label ID="lblleg" runat="server" Visible="false" Text='<%# Eval("leg") %>'></asp:Label>
                                                            <asp:Label ID="lblsplitflag" runat="server" Visible="false" Text='<%# Eval("Splittype") %>'></asp:Label>
                                                            <asp:Label ID="lblmodflag" runat="server" Visible="false" Text='<%# Eval("Mod_flag") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Names="Gotham"
                                                        HeaderStyle-Width="17%" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label12" runat="server" Style="font-size: 1vw; font-family: Gotham"
                                                                Text='<%# Eval("DateFormat", "{0:MMMM d}") %>'> </asp:Label>
                                                            <asp:Label ID="lbltosplit" runat="server" Visible="false" Style="font-size: 1vw;
                                                                font-family: Gotham" Text="-"> </asp:Label>
                                                            <asp:Label ID="lbltodate" runat="server" Visible="false" Style="font-size: 1vw; font-family: Gotham"
                                                                Text='<%# Eval("ADate", "{0:MMMM d}") %>'> </asp:Label>
                                                            <asp:Label ID="Label4" runat="server" Visible="false" Style="font-size: 1vw; font-family: Gotham"
                                                                Text='<%# Eval("DDate", "{0:MMMM dd, yyyy}") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="42%" ItemStyle-Font-Names="Gotham"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <div class="row">
                                                                <div class="col-lg-5 p-0">
                                                                    Departure
                                                                </div>
                                                                <div class="col-lg-2  p-0">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-5 p-0">
                                                                    Arrival
                                                                </div>
                                                            </div>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <div class="row">
                                                                <div class="col-lg-5 p-0" style="padding-right: 0; text-align: left;">
                                                                    <asp:Label ID="lblscity" runat="server" Style="font-size: 1vw; text-transform: uppercase;
                                                                        width: 70%" Text='<%# Eval("StartCityShow") %>'> </asp:Label>
                                                                    <asp:Label ID="lblstairport" Style="font-size: 1vw; width: 30%" runat="server" Text='<%# Eval("startshow") %>'> </asp:Label>
                                                                    <asp:Label ID="lblStart11" Style="font-size: 1vw" runat="server" Visible="false"
                                                                        Text='<%# Eval("Start") %>' />
                                                                </div>
                                                                <div class="col-lg-2" style="padding: 0;">
                                                                    -
                                                                </div>
                                                                <div class="col-lg-5" style="padding-right: 0; text-align: left;">
                                                                    <asp:Label ID="lblEcity" Style="font-size: 1vw; text-transform: uppercase; width: 70%"
                                                                        runat="server" Text='<%# Eval("EndCityShow") %>'> </asp:Label>
                                                                    <asp:Label ID="Label3" Style="font-size: 1vw" runat="server" Visible="false" Text='<%# Eval("End") %>'></asp:Label>
                                                                    <asp:Label ID="lblendairport" Style="font-size: 1vw; width: 30%" runat="server" Text='<%# Eval("Endshow") %>'></asp:Label>
                                                                    <asp:Label ID="lblenttime" Style="font-size: 1vw; width: 30%" runat="server" Visible="false"
                                                                        Text='<%# Eval("endfortime") %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Aircraft Capacity (Seats)" HeaderStyle-Width="15%"
                                                        HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNoSeats" runat="server" Text='<%# Eval("NoOFSeats") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Price" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="13%"
                                                        HeaderStyle-CssClass="newheader" ItemStyle-BorderWidth="0px">
                                                        <ItemTemplate>
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding: 0; text-align: right;">
                                                                    <asp:Label ID="Label26" Style="font-size: 1vw" Visible="true" runat="server" Text='$'></asp:Label>
                                                                </div>
                                                                <div class="col-lg-8" style="text-align: right;">
                                                                    <asp:Label ID="Label7" Style="font-size: 1vw" runat="server" Text='<%# Eval("Pricing","{0:N0}") %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                            <asp:Label ID="Price" Style="font-size: 1vw;" Visible="false" runat="server" Text='<%# Eval("Pricing","{0:N0}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Book" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                                        HeaderStyle-Width="11%" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton2" CommandName="Select" runat="server" Text="Book Now"
                                                                class="buttongrid"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div style="font-size: 1.15vw; font-weight: 400; color: rgb(17,24,32); padding-left: 36%;
                                                font-family: Gotham" id="divfooyrt" runat="server">
                                                <ul>
                                                    <li style="padding-bottom: 10px; text-align: left">
                                                        <asp:LinkButton ID="lnkbtndwnld" runat="server" Style="color: #346beb; font-weight: bold"
                                                            Text="Terms and conditions"></asp:LinkButton>
                                                        applicable to FLYT flights include: </li>
                                                    <li style="padding-bottom: 10px; text-align: left; padding-left: 10px">- 100% cancellation
                                                        penalty upon booking </li>
                                                    <li style="padding-bottom: 10px; text-align: left; padding-left: 10px">- No schedule
                                                        changes </li>
                                                    <li style="padding-bottom: 10px; text-align: left; padding-left: 10px">- Taxes and fees
                                                        are additional </li>
                                                    <li style="padding-bottom: 10px; text-align: left; padding-left: 10px">- Subject to
                                                        prior sale and availability </li>
                                                    <li style="text-align: left; display: none">
                                                        <asp:LinkButton ID="LinkButton8" runat="server" Style="color: #346beb; font-weight: bold"
                                                            Text="privacy & security policy."></asp:LinkButton>
                                                    </li>
                                                    <li style="padding-top: 10px; text-align: left">Questions? Please contact
                                                        <asp:LinkButton ID="LinkButton7" runat="server" Style="color: #b72025; text-decoration: none;
                                                            font-weight: bold" Text="FLYT@flyjetedge.com"></asp:LinkButton>
                                                    </li>
                                                    <li style="padding-top: 10px; text-align: left"><a href="https://www.flyjetedge.net/FLYT/TersmsOfUse.aspx"
                                                        target="_blank" id="lnkbtnterms" runat="server" style="color: #346beb; font-weight: bold">
                                                        Terms of Use</a> and <a href="https://www.flyjetedge.net/FLYT/PrivacyPolicy.aspx"
                                                            target="_blank" id="A1" runat="server" style="color: #346beb; font-weight: bold">
                                                            Privacy Policy</a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Page -->
    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->
    <div class="book-a-flight-box w-clearfix" style="top: 50vh;">
        <div data-w-id="030b7ee7-9f40-1290-01d5-3aa2dd72d91b" class="form-botton">
            <div class="book-button-in w-clearfix" style="background-color: rgb(178, 30, 40)">
                <div class="book-text">
                    <strong>Book a Charter </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    (818) 442-0096
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnTerms" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeterms" runat="server" TargetControlID="btnTerms" PopupControlID="pnlTerms"
        CancelControlID="imgCloseterms" Y="15" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlTerms" runat="server" Width="60%" ScrollBars="Auto" Style="display: none;
        background-color: #525659f5;">
        <table width="100%">
            <tr style="background-color: rgb(17,24,32); margin-left: 2%; margin-top: 5px; color: White">
                <td align="left" style="vertical-align: middle;">
                    <h4 style="margin-left: 2%; margin-top: 5px; color: White; font-size: 15px">
                    </h4>
                </td>
                <td align="right" style="vertical-align: middle; padding-right: 10px">
                    <asp:LinkButton ID="imgCloseterms" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" Text="Close" CausesValidation="false" />
                </td>
            </tr>
            <tr style="margin-top: 50px">
                <td style="width: 100%; padding: 0px" colspan="2">
                    <center>
                        <iframe id="iframeterms" runat="server" frameborder="0" width="100%" style="background-color: white">
                        </iframe>
                    </center>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblindex" runat="server" Visible="false"></asp:Label>
    <script src="../assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/demo/demo4/base/scripts.bundle.js" type="text/javascript"></script>
    <script src="../assets/app/bundle/app.bundle.js" type="text/javascript"></script>
    </form>
</body>
</html>
