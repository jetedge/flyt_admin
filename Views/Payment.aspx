﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="Payment.aspx.cs" Inherits="Views_Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-xl-12" style="border-bottom: 6px solid #f2f3f7;">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row m-0">
                        <div class="col-lg-12 p-0">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 11%; text-align: center">
                                    Payment Status
                                </label>
                                <div class="col-3" style="max-width: 20%;">
                                    <asp:DropDownList ID="ddlPayStatus" runat="server">
                                        <asp:ListItem Text="ALL" Value="L"></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="P"></asp:ListItem>
                                        <asp:ListItem Text="Approval Pending" Value="W"></asp:ListItem>
                                        <asp:ListItem Text="Pending And Approval Pending" Value="G" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Paid" Value="A"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 15%;">
                                    Trip Reference Number
                                </label>
                                <div class="col-3" style="max-width: 20%;">
                                    <asp:TextBox ID="txtTripRefNumber" class="cusinputuser" placeholder="Enter trip Reference #"
                                        runat="server" Width="100%"></asp:TextBox>
                                </div>
                                <div class="col-2" style="padding-top: 5px; max-width: 12%;">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-danger"
                                        OnClick="btnSearch_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="dvGrid" runat="server">
        <div class="col-lg-12">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-body p-3">
                    <asp:GridView ID="gvTripDetail" runat="server" AutoGenerateColumns="false" Width="100%"
                        DataKeyNames="rowid" ShowFooter="false" UseAccessibleHeader="true" CssClass="table"
                        HeaderStyle-CssClass="thead-dark" OnSelectedIndexChanged="gvTripDetail_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                                <ItemTemplate>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trip Ref. Number" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-Width="13%">
                                <ItemTemplate>
                                    <asp:Label ID="lblrowid" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TransactionReference") %>'></asp:Label>
                                    <asp:Label ID="lblMainId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "RowID") %>'
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking  Placed" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="15%" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookedDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "BookedDateTime") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booked For" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="13%">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookedFor" runat="server" Font-Size="14px" Text='<%# Eval("CFirstName") %>'
                                        Visible="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Departure" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <%-- <asp:Label ID="lblDeparture" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FromAirport") %>'></asp:Label>--%>
                                    <asp:Label ID="Label3" runat="server" Font-Size="14px" Text='<%# Eval("FromCity") %>'
                                        Visible="true"></asp:Label>
                                    (
                                    <asp:Label ID="Label2" runat="server" Font-Size="14px" Text='<%#Eval("FromAirport").ToString().Trim()%>'></asp:Label>
                                    )
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Arrival" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <asp:Label ID="Label3w" runat="server" Font-Size="14px" Text='<%# Eval("ToCity") %>'
                                        Visible="true"></asp:Label>
                                    (
                                    <asp:Label ID="lblArrival" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ToAirport") %>'></asp:Label>
                                    )
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Amount" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbltime" runat="server" Text='<%#  Eval("TotalAmount","{0:c0}")  %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction Id" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="13%"
                                Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactionId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TransID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction Date" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="13%"
                                Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblTransactinDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaidTransactionDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Payment Status" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="12%">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaidStatus") %>'
                                        Style="font-weight: 600;"></asp:Label>
                                    <asp:Label ID="lblPaidAmount" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaidTransactionAmount") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblPaidNotes" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PaidNotes") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblAdminNotes" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "AdminNotes") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblApprovedBy" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ApprovedBy") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblAppon" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ApprovedOn") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblCustUpdatedBy" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CustUpdatedBy") %>'
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblCustUpdatedOn" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CustUpdatedOn") %>'
                                        Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/pencil_Edit.png" HeaderText="Edit"
                                HeaderStyle-Width="5%" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" CommandName="Select" />
                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                <HeaderTemplate>
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                        <input type="checkbox" id="checkAll" onclick="checkAllCountry(this);" runat="server" />Select
                                        All<span /></span>
                                    </label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <label class="kt-checkbox kt-checkbox--boldkt-checkbox--brand" style="padding-top: 17px !important;">
                                        <input type="checkbox" id="chkChoose" runat="server" /><span></span>
                                    </label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnPayStatus" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpePayStatus" runat="server" TargetControlID="btnPayStatus"
        PopupControlID="pnlPayStatus" BackgroundCssClass="modalBackground" Y="20" CancelControlID="PrefClose">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlPayStatus" runat="server" Style="display: none; width: 55%; background-color: White;
        padding: 10px">
        <table width="100%" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            <h5 style="margin-left: 0%; font-size: 18px">
                                Update Status</h5>
                        </label>
                        <div class="col-6 text-right">
                            <asp:LinkButton ID="PrefClose" runat="server" Style="color: rgb(255,255,255); float: right;
                                text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                        </div>
                    </div>
                    <div style="border-bottom: 1px dashed #9E9E9E;">
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                            <label class="col-3 col-form-label">
                                Trip Reference Number
                            </label>
                            <label class="col-3 col-form-label">
                                <asp:Label ID="lblTripRefNumber" runat="server" Style="font-weight: bold;"> </asp:Label>
                            </label>
                            <label class="col-3 col-form-label" style="max-width: 20%;">
                                Total Amount
                            </label>
                            <label class="col-3 col-form-label">
                                <asp:Label ID="lblEAmount" runat="server" Style="font-weight: bold;"> </asp:Label>
                            </label>
                        </div>
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                            <label class="col-3 col-form-label">
                                Booked For
                            </label>
                            <label class="col-3 col-form-label">
                                <asp:Label ID="lblEBookedFor" runat="server" Style="font-weight: bold;"> </asp:Label>
                            </label>
                            <label class="col-3 col-form-label" style="max-width: 20%;">
                                Booked Placed
                            </label>
                            <label class="col-4 col-form-label" style="max-width: 30%;">
                                <asp:Label ID="lblBokkPlaced" runat="server" Style="font-weight: bold;"> </asp:Label>
                            </label>
                        </div>
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                            <label class="col-3 col-form-label">
                                Departure
                            </label>
                            <label class="col-9 col-form-label">
                                <asp:Label ID="lblDeparture" runat="server" Style="font-weight: bold;"> </asp:Label>
                            </label>
                        </div>
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                            <label class="col-3 col-form-label">
                                Arrival
                            </label>
                            <label class="col-9 col-form-label">
                                <asp:Label ID="lblArrival" runat="server" Style="font-weight: bold;"> </asp:Label>
                            </label>
                        </div>
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                            <label class="col-3 col-form-label">
                                Notes
                            </label>
                            <label class="col-9 col-form-label">
                                <asp:Label ID="lblCustNotes" runat="server" Style="font-weight: bold;"> </asp:Label>
                            </label>
                        </div>
                    </div>
                    <div style="border-bottom: 1px dashed #9E9E9E;">
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400; padding-top: 10px;">
                            <label class="col-12 col-form-label" style="font-weight: bold; color: Black;">
                                Wire Transfer Details
                            </label>
                        </div>
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                            <label class="col-3 col-form-label">
                                Bank Tranaction Id <span style="color: Red">*</span>
                            </label>
                            <div class="col-9">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtTransactionId" Style="width: 100%;"
                                    TabIndex="1" MaxLength="50">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtTransactionId"
                                    Display="None" ErrorMessage="Bank Transaction Id is required." SetFocusOnError="True"
                                    ValidationGroup="SP">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                            <label class="col-3 col-form-label">
                                Bank Transaction Date <span style="color: Red">*</span>
                            </label>
                            <div class="col-3" style="max-width: 20%;">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtTransactionDate" Style="width: 100%;"
                                    TabIndex="1" MaxLength="50" onkeypress="return false;">
                                </asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtTransactionDate"
                                    Format="MMM dd,yyyy" Enabled="True">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTransactionDate"
                                    Display="None" ErrorMessage="Bank Transaction Date is required." SetFocusOnError="True"
                                    ValidationGroup="SP">*</asp:RequiredFieldValidator>
                            </div>
                            <label class="col-3 col-form-label">
                                Transaction Amount <span style="color: Red">*</span>
                            </label>
                            <div class="col-2">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtAmount" Style="width: 100%;"
                                    TabIndex="1" MaxLength="7">
                                </asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FT" runat="server" ValidChars="0123456789." TargetControlID="txtAmount" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAmount"
                                    Display="None" ErrorMessage="Transaction Amount is required." SetFocusOnError="True"
                                    ValidationGroup="SP">*</asp:RequiredFieldValidator>
                            </div>
                            <label class="col-2 col-form-label" style="text-align: left; max-width: 12%;">
                                ( in USD )
                            </label>
                        </div>
                        <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;" id="dvCusUpdatedBy"
                            runat="server" visible="false">
                            <label class="col-12 col-form-label">
                                <asp:Label ID="lblCMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                    font-weight: 600;"></asp:Label>
                                <asp:Label ID="lblCLMB" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                            </label>
                        </div>
                    </div>
                    <div class="form-group row" style="font-size: 1.15vw; font-weight: 400; padding-top: 10px;">
                        <label class="col-12 col-form-label" style="font-weight: bold; color: Black;">
                            Payment Details
                        </label>
                    </div>
                    <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                        <label class="col-4 col-form-label">
                            Have you received payment ?
                        </label>
                        <div class="col-8" style="padding-top: 9px;">
                            <asp:RadioButtonList ID="rblPayRece" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Yes" Value="A" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="P"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                        <div class="col-12">
                            <asp:TextBox runat="server" CssClass="form-control" Height="70px" ID="txtNotes" Style="width: 100%;"
                                TabIndex="27" MaxLength="200" TextMode="MultiLine" placeholder="Notes">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-8 col-form-label">
                            <div id="divLMBy" runat="server" visible="false" style="float: left; padding-top: 10px;
                                padding-left: 0px;">
                                <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                    font-weight: 600;"></asp:Label>
                                <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                            </div>
                        </label>
                        <div class="col-4 text-right">
                            <asp:Button ID="btnAddMore" runat="server" Text="Update" CssClass="btn btn-danger"
                                TabIndex="33" ValidationGroup="SP" OnClick="btnUpdate_Status" />
                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="SP" ShowMessageBox="true"
                                runat="server" ShowSummary="false" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="lblEditValueId" runat="server" Value="0" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
</asp:Content>
