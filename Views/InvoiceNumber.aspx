﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="InvoiceNumber.aspx.cs" Inherits="Views_InvoiceNumber" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        function Project() {

            var txtpr1 = '';
            var project = document.getElementById('MainContent_txtTailFilter').value;
            var pr = project;
            txtpr1 = pr;
            //  var strDate = document.getElementById('spinvoice');

            document.getElementById("MainContent_spinvoice").innerHTML = txtpr1;
            document.getElementById('MainContent_spinvoice').style.display = '';
            document.getElementById('MainContent_spinvoice').style.textTransform = 'uppercase';
        }

        function Process() {

            var txtpr1 = '';
            var project = document.getElementById('MainContent_txtTailFilter').value;
            var process = document.getElementById('MainContent_txtprocess').value;
            var spinvoice = document.getElementById('MainContent_spinvoice').innerHTML;
            var sp = spinvoice;
            var pr = project;
            var proce = process;
            txtpr1 = pr + '-' + proce;

            document.getElementById("MainContent_spinvoice").innerHTML = txtpr1;
            document.getElementById('MainContent_spinvoice').style.display = '';
            document.getElementById('MainContent_spinvoice').style.textTransform = 'uppercase';

        }


        function Category() {

            var txtpr1 = '';
            var project = document.getElementById('MainContent_txtTailFilter').value;
            var process = document.getElementById('MainContent_txtprocess').value;
            var category = document.getElementById('MainContent_txtcategory').value;
            var spinvoice = document.getElementById('MainContent_spinvoice').innerHTML;
            var sp = spinvoice;
            var pr = project;
            var pro = process;
            var Cat = category;
            txtpr1 = pr + '-' + pro + '-' + Cat;
            document.getElementById('MainContent_spinvoice').innerHTML = txtpr1;
            document.getElementById('MainContent_spinvoice').style.display = '';
            document.getElementById('MainContent_spinvoice').style.textTransform = 'uppercase';
        }


        function Increment() {

            var txtpr1 = '';
            var spinvoice = '';
            var project = document.getElementById('MainContent_txtTailFilter').value;
            var process = document.getElementById('MainContent_txtprocess').value;
            var category = document.getElementById('MainContent_txtcategory').value;
            var Num = document.getElementById('MainContent_txtNumber').value;
            spinvoice = document.getElementById('MainContent_spinvoice').innerHTML;
            var sp = spinvoice;
            var pr = project;
            var pro = process;
            var Cat = category;
            var Number = Num;
            txtpr1 = pr + '-' + pro + '-' + Cat + '-' + Number;
            //  var strDate = document.getElementById('spinvoice');

            document.getElementById("MainContent_spinvoice").innerHTML = txtpr1;
            document.getElementById('MainContent_spinvoice').style.display = '';
            document.getElementById('MainContent_spinvoice').style.textTransform = 'uppercase';
        }
        
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-xl-12" style="padding-left: 4%; padding-right: 4%">
            <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                <div class="kt-portlet__body p-2" id="tblForm" runat="server" visible="true" style="padding-top: 5px !important;">
                    <div class="form-group row">
                        <div class="col-xl-12 text-right">
                            <label class="importantlabels">
                                <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                    ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                            </label>
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                ValidationGroup="A" />
                            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-danger" TabIndex="1" />
                            <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="A" ShowMessageBox="true"
                                runat="server" ShowSummary="false" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div style="font-size: 16px; text-align: left; font-weight: bold; padding-top: 5px !important;
                                    padding-top: 10px; width: 100%; border-bottom: 1px solid #C0C0C0 !important;">
                                    <label class="col-3 col-form-label" style="font-weight: bold;">
                                        Invoice Number Generation
                                    </label>
                                    <label class="col-5 col-form-label">
                                        Maximum of consecutive invoice number is &nbsp&nbsp
                                        <asp:Label ID="lblinvoice" Style="font-weight: bold" runat="server"></asp:Label>
                                    </label>
                                    <label class="col-3 col-form-label">
                                        Sample Invoice # &nbsp&nbsp
                                        <asp:Label ID="spinvoice" Text="." Style="font-weight: bold" runat="server"></asp:Label>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">
                                    Project/ Product Short code <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox ID="txtTailFilter" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                        onblur="Project();" ToolTip="Enter all characters. Length is limited to 100"
                                        CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTailFilter"
                                        Display="None" ErrorMessage="Project/ Product Short code is required." SetFocusOnError="True"
                                        ValidationGroup="A">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-6 col-form-label">
                                    ( Short form of Project/ Product i.e JE )
                                </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">
                                    Process Name (short code) <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox ID="txtprocess" runat="server" CssClass="form-control" TabIndex="6"
                                        onblur="Process();" Style="text-transform: uppercase" MaxLength="3" autocomplete="off"
                                        ToolTip="Enter all characters. Length is limited to 3" onkeypress="return Alphabets(event)"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtprocess"
                                        Display="None" ErrorMessage="Process Name is required." SetFocusOnError="True" ValidationGroup="A">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-6 col-form-label">
                                    (Short form of Process Name i.e ET)
                                </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">
                                    Category <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox ID="txtcategory" runat="server" CssClass="form-control" TabIndex="7"
                                        onblur="Category();" autocomplete="off" MaxLength="10" ToolTip="Enter all characters. Length is limited to 10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcategory"
                                        Display="None" ErrorMessage="Category is required." SetFocusOnError="True" ValidationGroup="A">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-6 col-form-label">
                                    (Short form of Category i.e INV)
                                </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label">
                                    Auto Increment <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox ID="txtNumber" runat="server" CssClass="form-control" TabIndex="8" autocomplete="off"
                                        onblur="Increment();" MaxLength="15" ToolTip="Enter Numerics only. Length is limited to 15"
                                        onkeypress="return Numerics(event)"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNumber"
                                        Display="None" ErrorMessage="Auto Increment is required." SetFocusOnError="True"
                                        ValidationGroup="A">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-6 col-form-label">
                                    (Auto increment number for invoice i.e 1000)
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__body p-0" id="tblGrid" runat="server">
                    <div class="row m-0">
                        <div class="col-xl-12 p-0 boxshadow table-responsive">
                            <asp:GridView ID="gvinvoice" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                GridLines="Both" EmptyDataText="No invoice number found." Width="100%" AutoGenerateColumns="false"
                                PageSize="25" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rowid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrowid" Text='<%# Eval("RowID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project/ Product Short code" ItemStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="23%" ItemStyle-Width="23%" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblfirstName" Text='<%# Eval("Project") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process Name (short code)" ItemStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="23%" ItemStyle-Width="23%" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLastName" Text='<%# Eval("Process") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Category" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left" HeaderStyle-Wrap="false"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" Text='<%# Eval("Category") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Auto Increment" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%"
                                        ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="left" HeaderStyle-Wrap="false"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPhone" Text='<%# Eval("AutoIncrement") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left" HeaderStyle-Wrap="false"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedby" Text='<%# Eval("created") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left" HeaderStyle-Wrap="false"
                                        ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedon" Text='<%# Eval("createdonN") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="lblalert" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
