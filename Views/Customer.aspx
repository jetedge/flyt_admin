﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="Customer.aspx.cs" Inherits="Views_Customer" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        /* The hint to Hide and Show */
        .pager
        {
            background-color: #cbcbd2;
            font-family: Arial;
            color: Black;
            height: 30px;
            text-align: left;
        }
        .hint
        {
            border: 3px solid #b72025;
            text-align: left;
            padding: 3px 0px 3px 3px;
            width: 251px;
            position: relative;
            display: none;
            background: #ffc;
            z-index: 9999999;
            line-height: 23px;
            font-size: 0.8rem;
            color: black;
            font-size: 12px;
            letter-spacing: 0.5px;
        }
        
        .hint .col-lg-12
        {
            display: flex;
            align-items: center;
        }
        
        /* The pointer image is hadded by using another span */
        .hint-pointer
        {
            position: absolute;
            left: -13px;
            top: 0;
            width: 10px;
            height: 19px;
            background: url(../Components/images/pointer.gif) no-repeat;
            z-index: 9999999;
        }
    </style>
    <style type="text/css">
        .cusinputuser123
        {
            margin-top: 3px;
            padding: 8px;
            font-size: 1.15vw;
            width: 100%;
            border-radius: 0px;
            color: White;
            font-weight: 400;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function ValidateInputLength(obj, textType) {
            if (obj.value != "" && obj.value.length < 8) {
                alert(textType + ' should not be less than eight characters!');
                obj.value = ""; obj.focus();
            }
        }

        if (document.getElementById('MainContent_txthidden') != null)
            var count = document.getElementById('MainContent_txthidden').value;
        else
            var count = 0;

        if (document.getElementById('MainContent_txthidden1') != null)
            var count1 = document.getElementById('MainContent_txthidden1').value;
        else
            var count1 = 0;

        function show(obj) {
            document.getElementById('MainContent_divShow').style.display = "block";
            var password = obj.value;

            var lowercase = "abcdefghijklmnopqrstuvwxyz";
            var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var number = "1234567890";
            //var special = "";
            var nonspecial = "~`!@#$%^&*()+=[{]}\|;:',<.>/?_-";



            var containsLowerCase = Contains(password, lowercase);
            var containsUpperCase = Contains(password, uppercase);
            var containsNumber = Contains(password, number);
            //var containsSpecial = Contains(password, special);
            var containsNonSpecial = Contains(password, nonspecial);

            if (password.length > 8) {
                document.getElementById('MainContent_imgyes1').style.display = "block";
                document.getElementById('MainContent_imgno1').style.display = "none";
            }
            else {
                document.getElementById('MainContent_imgno1').style.display = "block";
                document.getElementById('MainContent_imgyes1').style.display = "none";
                count = count + 1;
            }

            if (containsLowerCase || containsUpperCase) {
                document.getElementById('MainContent_imgyes2').style.display = "block";
                document.getElementById('MainContent_imgno2').style.display = "none";
            }
            else {
                document.getElementById('MainContent_imgno2').style.display = "block";
                document.getElementById('MainContent_imgyes2').style.display = "none";
                count = count + 1;
            }

            if (containsLowerCase) {
                document.getElementById('MainContent_imgyes3').style.display = "block";
                document.getElementById('MainContent_imgno3').style.display = "none";
            }
            else {
                document.getElementById('MainContent_imgno3').style.display = "block";
                document.getElementById('MainContent_imgyes3').style.display = "none";
                count = count + 1;
            }

            if (containsUpperCase) {
                document.getElementById('MainContent_imgyes4').style.display = "block";
                document.getElementById('MainContent_imgno4').style.display = "none";
            }
            else {
                document.getElementById('MainContent_imgno4').style.display = "block";
                document.getElementById('MainContent_imgyes4').style.display = "none";
                count = count + 1;
            }

            if (containsNumber) {
                document.getElementById('MainContent_imgyes5').style.display = "block";
                document.getElementById('MainContent_imgno5').style.display = "none";
            }
            else {
                document.getElementById('MainContent_imgno5').style.display = "block";
                document.getElementById('MainContent_imgyes5').style.display = "none";
                count = count + 1;
            }


            if (containsNonSpecial) {
                document.getElementById('MainContent_imgyes7').style.display = "none";
                document.getElementById('MainContent_imgno7').style.display = "block";
                count = count + 1;
            }
            else {
                document.getElementById('MainContent_imgno7').style.display = "none";
                document.getElementById('MainContent_imgyes7').style.display = "block";
            }
        }

        function passvalidate(obj) {
            show(obj);

            if (document.getElementById('MainContent_imgno1').style.display == "block") {
                //obj.focus();
                document.getElementById('MainContent_divShow').style.display = "block";
                document.getElementById('MainContent_lblalert').innerHTML = "Please check the password criteria";
                //$find("mpealert").show();
                // alert("Please check the password criteria");
            }
            else if (document.getElementById('MainContent_imgno2').style.display == "block") {
                //obj.focus();
                document.getElementById('MainContent_divShow').style.display = "block";
                document.getElementById('MainContent_lblalert').innerHTML = "Please check the password criteria";
                //$find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else if (document.getElementById('MainContent_imgno3').style.display == "block") {
                //obj.focus();
                document.getElementById('MainContent_divShow').style.display = "block";
                //  alert("Please check the password criteria");
                document.getElementById('MainContent_lblalert').innerHTML = "Please check the password criteria";
                //$find("mpealert").show();
            }
            else if (document.getElementById('MainContent_imgno4').style.display == "block") {
                //obj.focus();
                document.getElementById('MainContent_divShow').style.display = "block";
                document.getElementById('MainContent_lblalert').innerHTML = "Please check the password criteria";
                //$find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else if (document.getElementById('MainContent_imgno5').style.display == "block") {
                //obj.focus();
                document.getElementById('MainContent_divShow').style.display = "block";
                document.getElementById('MainContent_lblalert').innerHTML = "Please check the password criteria";
                //$find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else if (document.getElementById('MainContent_imgno7').style.display == "block") {
                //obj.focus();
                document.getElementById('MainContent_divShow').style.display = "block";
                document.getElementById('MainContent_lblalert').innerHTML = "Please check the password criteria";
                //$find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else {
                document.getElementById('MainContent_divShow').style.display = "none";
            }
        }

        function showlist() {
            document.getElementById('MainContent_divShow').style.display = "block";
        }

        function showlistpass() {
            ;
            document.getElementById('MainContent_divshowpassmatch').style.display = "block";
        }

        function checkpassmatch(obj) {
            ;
            validatepass(obj);
            if (count1 > 0) {
                //alert("Please check the password criteria");
                //obj.focus();
                document.getElementById('MainContent_divshowpassmatch').style.display = "block";
            }
            document.getElementById('MainContent_divshowpassmatch').style.display = "none";
        }

        function validatepass(obj) {
            ;
            var confirmpassword = obj.value;
            var password = document.getElementById('MainContent_txtNew_Password').value;

            if (confirmpassword == password) {
                document.getElementById('MainContent_Imgpassyes').style.display = "block";
                document.getElementById('MainContent_Imgpassno').style.display = "none";
                count1 = 0;
                //  document.getElementById('btnSubmit').removeAttribute("disabled");
            }
            else {
                document.getElementById('MainContent_Imgpassyes').style.display = "none";
                document.getElementById('MainContent_Imgpassno').style.display = "block";
                count1 = count1 + 1;
                //  document.getElementById('btnSubmit').setAttribute("disabled", "disabled");
            }
        }

        function CheckPassword(obj) {
            var password = obj.value;
            var userName = document.getElementById('MainContent_txtUserName').value;

            var lowercase = "abcdefghijklmnopqrstuvwxyz";
            var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var number = "1234567890";

            // find out if the password different cases
            var containsLowerCase = Contains(password, lowercase);
            var containsUpperCase = Contains(password, uppercase);
            var containsNumber = Contains(password, number);

            if ((containsLowerCase || containsUpperCase) && containsNumber) {

                if (password.length < 8) {
                    if (password != "") {
                        alert("Password length should not be less than 8 digits");
                    }
                    obj.value = "";
                    obj.focus();
                }
                else if (password == userName) {
                    if (password != "") {
                        alert("Password can not be same as username");
                    }
                    obj.value = "";
                    obj.focus();
                }
            }
            else {
                if (password != "") {
                    alert("Password should contain both Alphabets and Number");
                    obj.value = "";
                    obj.focus();
                }
            }
        }

        // checks if a password contains one of a set of characters
        function Contains(password, validChars) {

            for (i = 0; i < password.length; i++) {

                var char = password.charAt(i);
                if (validChars.indexOf(char) > -1) {
                    return true;
                }
            }
            return false;
        }
    </script>
    <script type="text/javascript" language="javascript">
        function CheckDate(sender, args) {
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;

            var today1 = sender._selectedDate;
            var date1 = today1.getFullYear() + '-' + (today1.getMonth() + 1) + '-' + today1.getDate();
            var time1 = today1.getHours() + ":" + today1.getMinutes() + ":" + today1.getSeconds();
            var dateTime1 = date1 + ' ' + time1;

            var dateOne = new Date(today.getFullYear(), (today.getMonth() + 1), today.getDate()); //Year, Month, Date    
            var dateTwo = new Date(today1.getFullYear(), (today1.getMonth() + 1), today1.getDate()); //Year, Month, Date    

            if (dateOne > dateTwo) {
                //  alert("You cannot select past date!");
                document.getElementById('lblalert').innerHTML = 'You cannot select past date!';
                ////$find("mpealert").show();
                sender._selectedDate = new Date();
                sender._textbox.set_Value('')
                return;
            }
        }        
    </script>
    <script type="text/javascript">
        function txtCityStartTimefun(object) {
            var txtCityStartTim = document.getElementById('txtTime1New');
            document.getElementById('MainContent_txtTime1').value = txtCityStartTim.value;
        }

    </script>
    <script type="text/javascript">
        function NewSplit() {

            document.getElementById('txtTime1New').value = document.getElementById('MainContent_txtTime1').value;
        }
    </script>
    <style type="text/css">
        .HeaderTop
        {
            font-size: 16px;
            text-align: left;
            font-weight: bold;
            padding-top: 5px !important;
            padding-bottom: 10px;
            padding-top: 10px;
            border-bottom: 1px solid #C0C0C0 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="uplCustomer" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <div class="row" id="tblGrid" runat="server">
                <div class="col-xl-1">
                </div>
                <div class="col-xl-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-lg-3 text-right">
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group row" id="dvGridHeader" runat="server">
                                        <div class="col-2">
                                        </div>
                                        <div class="col-8">
                                            <asp:TextBox ID="txtusername" runat="server" Style="width: 100%" CssClass="form-control"
                                                placeholder="Enter User / Broker Name" autocomplete="off" AutoPostBack="true"
                                                OnTextChanged="txtusername_changed" TabIndex="1"></asp:TextBox>
                                        </div>
                                        <div class="col-2">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 text-right" style="padding-right: 2%;">
                                    <asp:Button ID="btnExportExcel" runat="server" Text="Export Excel" CssClass="btn btn-danger"
                                        TabIndex="2" />
                                    &nbsp;
                                    <asp:Button ID="btnScheduler" runat="server" Text="Scheduler" CssClass="btn btn-danger"
                                        PostBackUrl="~/Views/CustomerScheduler.aspx" TabIndex="3" />
                                    &nbsp;
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" TabIndex="1" />
                                </div>
                            </div>
                            <div class="row m-0">
                                <div class="col-xl-12 boxshadow table-responsive">
                                    <asp:GridView ID="gvpricing" runat="server" HeaderStyle-CssClass="thead-dark" PagerStyle-CssClass="pager"
                                        AutoGenerateColumns="false" PageSize="500" CssClass="table" AllowPaging="True"
                                        Width="100%" EmptyDataText="No Customer details found." OnSelectedIndexChanged="gvPricingDetails_SelectedIndexChanged">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                ItemStyle-Width="20%" HeaderStyle-Width="20%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                                Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblrowid" Text='<%# Eval("rowid") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="First Name" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                ItemStyle-Width="15%" HeaderStyle-Width="15%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfirstname" Text='<%# Eval("Firstname") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                ItemStyle-Width="10%" HeaderStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <span>
                                                        <asp:Label ID="lbllastname" Text='<%# Eval("Lastname") %>' runat="server"></asp:Label>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Broker Name" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                ItemStyle-Width="15%" HeaderStyle-Width="15%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <span>
                                                        <asp:Label ID="lblBrokerNamw" Text='<%# Eval("BrokerName") %>' runat="server"></asp:Label>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                ItemStyle-Width="25%" HeaderStyle-Width="25%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblemail" Text='<%# Eval("Email") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phone Number" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                ItemStyle-Width="16%" HeaderStyle-Width="16%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblphone" Text='<%# Eval("Phonenumber") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User Type" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                ItemStyle-Width="25%" HeaderStyle-Width="25%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUserType" Text='<%# Eval("BrokerF") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%"
                                                ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbView1" runat="server" TabIndex="3" ImageUrl="~/Images/pencil_Edit.png"
                                                        CommandName="Select" OnClientClick="return showloader();" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1">
                </div>
            </div>
            <div class="row" id="tblForm" runat="server" visible="false">
                <div class="col-xl-12" style="padding-left: 4%; padding-right: 4%;">
                    <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                        <div class="form-group row">
                            <div class="col-xl-12 text-right">
                                <label class="importantlabels" id="dvFormHeader" runat="server" visible="false">
                                    <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                        ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                                </label>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                    ValidationGroup="SP" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset Password" CssClass="btn btn-danger"
                                    TabIndex="16" Visible="false" />
                                <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                                    OnClick="btnViewOnClick" />
                                <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                    runat="server" ShowSummary="false" />
                                <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; width: 40%;
                                border-bottom: 1px solid #C0C0C0 !important;">
                                User Details
                                <div style="float: right; padding-top: 10px;" id="divLMBy" runat="server" visible="false">
                                    <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                        font-weight: 600;"></asp:Label>
                                    <asp:Label ID="lblLMby" runat="server" Style="font-weight: 600;"></asp:Label>
                                </div>
                            </label>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Title <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:DropDownList ID="ddlgender" runat="server" TabIndex="4" CssClass="form-control">
                                    <asp:ListItem>Mr</asp:ListItem>
                                    <asp:ListItem>Mrs</asp:ListItem>
                                    <asp:ListItem>Miss</asp:ListItem>
                                    <asp:ListItem>Sir</asp:ListItem>
                                    <asp:ListItem>Dr</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <label class="col-2 col-form-label">
                                Address 1
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TabIndex="10"
                                    MaxLength="200" autocomplete="off" ToolTip="Enter all characters.Length is limited to 200"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                First Name <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtFname" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFname"
                                    Display="None" ErrorMessage="First Name is required." SetFocusOnError="True"
                                    ValidationGroup="SP">*</asp:RequiredFieldValidator>
                            </div>
                            <label class="col-2 col-form-label">
                                Address 2
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control" TabIndex="11"
                                    MaxLength="200" autocomplete="off" ToolTip="Enter all characters. Length is limited to 200"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Last Name <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" TabIndex="6"
                                    MaxLength="100" autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                                    Display="None" ErrorMessage="Last Name is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                            </div>
                            <label class="col-2 col-form-label">
                                City
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" TabIndex="12" autocomplete="off"
                                    MaxLength="100" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Email <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" TabIndex="7" autocomplete="off"
                                    MaxLength="200" ToolTip="Enter all characters. Length is limited to 200"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmailValidation" runat="server" ControlToValidate="txtEmail"
                                    Display="None" ErrorMessage="Please enter valid email address." ValidationGroup="SP"
                                    SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail"
                                    Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                            </div>
                            <label class="col-2 col-form-label">
                                State
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtState" runat="server" CssClass="form-control" TabIndex="13" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100" MaxLength="100"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Mobile Number <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-2 p-l-0 " style="display: none;">
                                <%--  <asp:TextBox ID="txtPhoneCode" runat="server" CssClass="form-control" TabIndex="8"
                                    MaxLength="3" autocomplete="off" ToolTip="Enter all characters. Length is limited to 3"></asp:TextBox>--%>
                                <asp:DropDownList ID="ddlPhoneCode" runat="server" CssClass="form-control" TabIndex="8">
                                </asp:DropDownList>
                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlPhoneCode"
                                    Display="None" ErrorMessage="Phone Code is required." SetFocusOnError="True"
                                    InitialValue="0" ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                            <div class="col-4">
                                <input class="cusinputuser" runat="server" id="txtPhone" onkeypress="return Numerics(event)"
                                    data-mask="999.999.9999" maxlength="15" type="text" />
                                <%-- <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" TabIndex="8" autocomplete="off"
                                    MaxLength="15" Width="100%" ToolTip="Enter Valid Mobile Number. Length is limited to 15" />--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPhone"
                                    Display="None" ErrorMessage="Mobile Number is required." SetFocusOnError="True"
                                    ValidationGroup="SP">*</asp:RequiredFieldValidator>
                            </div>
                            <label class="col-2 col-form-label">
                                Country <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control " TabIndex="14">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlCountry"
                                    InitialValue="0" Display="None" ErrorMessage="Country is required." SetFocusOnError="True"
                                    ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                <%-- <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" TabIndex="14"
                                    MaxLength="100" autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>--%>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2  col-form-label">
                                User Type
                            </label>
                            <div class="col-4">
                                <asp:DropDownList ID="ddlUserType" runat="server" CssClass="form-control" TabIndex="15"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
                                    <asp:ListItem Text="Customer" Value="User" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Broker" Value="Broker"></asp:ListItem>
                                    <asp:ListItem Text="Jet Edge Team" Value="JEUser"></asp:ListItem>
                                    <asp:ListItem Text="Jet Edge Retail" Value="JERetail"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <label class="col-2  col-form-label">
                                Zip Code
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txt_Zip" runat="server" CssClass="form-control" TabIndex="15" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row align-items-center mt-5">
                            <label class="col-2 col-form-label">
                                Active <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:CheckBox ID="chkActive" Checked="true" TabIndex="9" runat="server" Text="Check for Active" />
                            </div>
                            <label class="col-2 col-form-label">
                                Allowed Pay Methods
                            </label>
                            <div class="col-4" style="padding-top: 10px;">
                                <asp:CheckBoxList ID="rblPayMethod" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Credit Card" Value="CC"></asp:ListItem>
                                    <%--<asp:ListItem Text="E Check" Value="EC"></asp:ListItem>--%>
                                    <asp:ListItem Text="Wire Transfer" Value="WT"></asp:ListItem>
                                </asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;"
                        id="trPAsword" runat="server">
                        <div class="form-group row">
                            <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; width: 40%;
                                border-bottom: 1px solid #C0C0C0 !important;">
                                Password Details
                            </label>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Password <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:TextBox class="cusinputuser" runat="server" ID="txtNew_Password" TabIndex="16"
                                    CssClass="form-control" MaxLength="20" onfocus="return showlist()" onblur="return passvalidate(this)"
                                    onkeypress="return show(this)" onkeyup="return show(this)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtNew_Password"
                                    Display="None" ErrorMessage="New Password is required." SetFocusOnError="True"
                                    ValidationGroup="TP">*</asp:RequiredFieldValidator>
                                <div id="divShow" runat="server" style="width: 240px; z-index: 9999999; position: fixed;
                                    display: none;" class="hint">
                                    <%--<div class="hint-pointer">
                                    </div>--%>
                                    <div class="row">
                                        <div class="col-lg-12 text-left" style="font-weight: bold;">
                                            Password Criteria
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Image ID="imgyes1" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno1" CssClass="mr-2" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                            At least 8 characters long
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Image ID="imgyes2" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno2" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            Start with a letter
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Image ID="imgyes3" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno3" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            Include a lower case letter
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Image ID="imgyes4" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno4" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            Include an upper case letter
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Image ID="imgyes5" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno5" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            Include a number
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Image ID="imgyes7" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno7" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            No special characters
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label class="col-2  col-form-label">
                                Confirm Password <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-4">
                                <asp:TextBox class="cusinputuser" runat="server" ID="txtConformPass" TabIndex="17"
                                    MaxLength="20" onfocus="return showlistpass()" onblur="checkpassmatch(this)"
                                    CssClass="form-control" onkeypress="validatepass(this)" onkeyup="validatepass(this)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtConformPass"
                                    Display="None" ErrorMessage="Confirm Password is required." SetFocusOnError="True"
                                    ValidationGroup="TP">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNew_Password"
                                    SetFocusOnError="false" ValidationGroup="SP" ControlToValidate="txtConformPass"
                                    Display="None" ErrorMessage="Confirm Password not matched." Operator="Equal"
                                    Type="String"></asp:CompareValidator>
                                <div id="divshowpassmatch" runat="server" style="width: 240px; z-index: 9999999;
                                    position: fixed; display: none;" class="hint">
                                    <%--<div class="hint-pointer">
                                    </div>--%>
                                    <div class="row">
                                        <div class="col-lg-12 text-left" style="font-weight: bold;">
                                            Password Criteria
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Image ID="Imgpassyes" CssClass="mr-2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="Imgpassno" CssClass="mr-2" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                            Password Matches
                                        </div>
                                    </div>
                                </div>
                                <input type='hidden' id='txthidden' runat="server" name='txthidden' />
                                <input type='hidden' id='txthidden1' runat="server" name='txthidden1' />
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                        <div class="form-group row">
                            <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                padding-top: 5px !important; padding-bottom: 5px; padding-top: 10px; width: 40%;
                                border-bottom: 1px solid #C0C0C0 !important;">
                                Must Move Mail Configuration
                            </label>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">
                                Do you want to recieve <span style="font-weight: 600;">MUST MOVE</span> mail ?
                            </label>
                            <div class="col-4" style="padding-top: 7px;">
                                <asp:RadioButtonList ID="rblMustMoveMail" runat="server" RepeatDirection="Horizontal"
                                    CssClass="cusinputuser123" TabIndex="17" RepeatColumns="2" Width="30%">
                                    <asp:ListItem Selected="True" Value="Y">&nbsp;Yes&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="N">&nbsp;No</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group row" id="divReason" runat="server" visible="false">
                            <label class="col-2 col-form-label">
                                Reason
                            </label>
                            <div class="col-10">
                                <asp:Label ID="lblReason" runat="server" Style="font-weight: 600;" />
                            </div>
                        </div>
                        <div class="form-group row" id="divUnSub" runat="server" visible="false">
                            <label class="col-2 col-form-label">
                                <asp:Label ID="spnSubscribe" runat="server" />
                            </label>
                            <div class="col-10">
                                <asp:Label ID="lblUnSubBy" runat="server" Style="font-weight: 600;" />
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;"
                        id="trBrokerAdress" runat="server" visible="false">
                        <div class="form-group row">
                            <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                padding-top: 5px !important; padding-bottom: 5px; padding-top: 10px; border-bottom: 1px solid #C0C0C0 !important;">
                                Broker Details (For Internal Use Only)
                                <div id="dvCopy" runat="server" visible="false" style="float: right;">
                                    <asp:CheckBox ID="chkCopy" runat="server" Text="Copy User Details to Broker Details"
                                        AutoPostBack="true" />
                                </div>
                            </label>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                First Name<%-- <span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtBrokerName" runat="server" TabIndex="18" MaxLength="100" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtBrokerName"
                                                Display="None" ErrorMessage="Broker First Name is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                            <label class="col-2 col-form-label">
                                City
                                <%--<span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtBrokerCIty" runat="server" CssClass="form-control" TabIndex="22"
                                    autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtBrokerCIty"
                                                Display="None" ErrorMessage="Broker City is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Last Name<%-- <span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtBLName" runat="server" TabIndex="19" MaxLength="100" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtBrokerName"
                                                Display="None" ErrorMessage="Broker Last Name is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                            <label class="col-2 col-form-label">
                                State<%-- <span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtBrokerState" runat="server" CssClass="form-control" TabIndex="23"
                                    autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtBrokerState"
                                                Display="None" ErrorMessage="Broker State is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Manager
                                <%--<span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtManger" runat="server" TabIndex="20" MaxLength="100" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtManger"
                                                Display="None" ErrorMessage="Manager is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                            <label class="col-2 col-form-label">
                                Country<%-- <span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtBrokerCountry" runat="server" CssClass="form-control" TabIndex="24"
                                    autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtBrokerCountry"
                                                Display="None" ErrorMessage="Broker Country is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Address
                                <%--<span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtBrokerAdd1" runat="server" TabIndex="21" MaxLength="100" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtBrokerAdd1"
                                                Display="None" ErrorMessage="Broker Address is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                            <label class="col-2 col-form-label">
                                Zip Code
                                <%--<span class="MandatoryField">* </span>--%>
                            </label>
                            <div class="col-4">
                                <asp:TextBox ID="txtBrokerZipCode" runat="server" CssClass="form-control" TabIndex="25"
                                    autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtBrokerZipCode"
                                                Display="None" ErrorMessage="Broker ZipCode is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                border-bottom: 1px solid #C0C0C0 !important;">
                                Payment Term(s)
                            </label>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">
                                Enter Pay Terms <span class="MandatoryField">*</span>
                            </label>
                            <div class="col-2">
                                <asp:TextBox ID="txtDays" runat="server" CssClass="form-control" TabIndex="26" autocomplete="off"
                                    ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDays"
                                    Display="None" ErrorMessage="Pay Terms is required." SetFocusOnError="True" ValidationGroup="SP" />
                            </div>
                            <label class="col-2 col-form-label">
                                ( in Days )
                            </label>
                        </div>
                    </div>
                    <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                        <div class="form-group row" style="border-bottom: 1px solid #C0C0C0 !important;">
                            <label class="col-8 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                padding-top: 5px !important; padding-bottom: 10px;">
                                User Preferences (
                                <asp:Label ID="lblPre" runat="server" Text="you can add 3 more preferences" Style="color: brown;" />
                                )
                            </label>
                            <div class="col-4">
                                <asp:Button ID="btnAddPref" runat="server" Text="Add Preference" CssClass="btn btn-danger"
                                    TabIndex="1" Style="float: right;" />
                            </div>
                        </div>
                        <div class="form-group row" id="divGVChild" runat="server">
                            <div class="col-12">
                                <asp:GridView ID="gv_Child" runat="server" AutoGenerateColumns="false" Width="100%"
                                    ShowFooter="false" HeaderStyle-CssClass="thead-dark" CssClass="table" OnRowDeleting="gv_Child_RowDeleting"
                                    OnSelectedIndexChanged="gv_Child_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="3%" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start city" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="left"
                                            HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="24%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstartcity1" Visible="false" runat="server" Text='<%# Eval("startcity") %>' />
                                                <asp:Label ID="lblstartcity2" Visible="false" runat="server" Text='<%# Eval("StartAirport") %>' />
                                                <asp:Label ID="lblstartcity3" Visible="false" runat="server" Text='<%# Eval("StartShow") %>' />
                                                <asp:Label ID="lblStartICAODisp" runat="server" Text='<%# Eval("StartICAODisp") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End city" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="left"
                                            HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="24%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblendcity1" Visible="false" runat="server" Text='<%# Eval("endcity") %>' />
                                                <asp:Label ID="lblendcity2" Visible="false" runat="server" Text='<%# Eval("EndAiport") %>' />
                                                <asp:Label ID="lblendcity3" Visible="false" runat="server" Text='<%# Eval("Endshow") %>' />
                                                <asp:Label ID="lblEndICAODisp" runat="server" Text='<%# Eval("EndICAODisp") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstartdate" runat="server" Text='<%# Eval("StartDate") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblenddate" runat="server" Text='<%# Eval("EndDate") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fly Time" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltime1" runat="server" Text='<%# Eval("Timeflag") %>' />
                                                <asp:Label ID="lbltime2" runat="server" Visible="false" Text='<%# Eval("time") %>' /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblemail" runat="server" Text='<%# Eval("Emailflag") %>' /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SMS" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSMs" runat="server" Text='<%# Eval("SMSFlag") %>' /></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbView" runat="server" Height="20px" ImageUrl="~/Images/pencil_Edit.png"
                                                    TabIndex="13" CausesValidation="false" CommandName="Select" AlternateText="Edit"
                                                    ToolTip="Click here to edit." />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="true"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbDel" runat="server" CommandName="Delete" ImageUrl="~/Images/icon_delete.png"
                                                    CausesValidation="false" AlternateText="Delete" ToolTip="Click here to Remove"
                                                    TabIndex="14" Width="20px" Height="20px" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet" style="padding-top: 5px !important;" id="tripblock" runat="server">
                        <div class="form-group row">
                            <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; width: 40%;
                                border-bottom: 1px solid #C0C0C0 !important;">
                                Trip Details
                            </label>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <asp:GridView ID="gvSimpleSearch" runat="server" HeaderStyle-CssClass="thead-dark"
                                    CssClass="table" AutoGenerateColumns="False" EmptyDataText="No Data Available"
                                    Width="75%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-Wrap="false" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-Wrap="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblindex" Style="font-size: 14px" runat="server" Text='<%# Eval("sno") %>'
                                                    Visible="false"> </asp:Label>
                                                <asp:Label ID="lbltailno" Style="font-size: 14px" runat="server" Text='<%# Eval("tailno") %>'
                                                    Visible="false"> </asp:Label>
                                                <asp:Label ID="Label12" runat="server" Style="font-size: 14px; font-weight: normal"
                                                    Text='<%# Eval("DateFormat", "{0:MMMM d, yyyy}") %>'> </asp:Label>
                                                <asp:Label ID="Label4" runat="server" Visible="false" Style="font-size: 14px" Text='<%# Eval("DDate", "{0:MMM d}") %>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Departure" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-Wrap="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" Style="font-size: 14px" runat="server" Text='<%# Eval("Start") %>'> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Arrival" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-Wrap="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" Style="font-size: 14px" runat="server" Text='<%# Eval("End") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Price" HeaderStyle-Width="9%" ItemStyle-Width="9%"
                                            ItemStyle-Wrap="false" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <table width="100%" style="border: 0px">
                                                    <tr style="border: 0px">
                                                        <td style="width: 70%; border: 0px; text-align: right">
                                                            <asp:Label ID="Label1" Font-Size="14px" Style="text-align: center" runat="server"
                                                                Text='<%# Eval("Priceflag") %>'></asp:Label>
                                                        </td>
                                                        <td style="width: 30%; border: 0px; text-align: right">
                                                            <asp:Label ID="Label7" Font-Size="14px" Style="text-align: right" runat="server"
                                                                Text='<%# Eval("Pricing","{0:c0}") %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnShow" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpetime" runat="server" TargetControlID="btnShow" PopupControlID="pnltime"
                CancelControlID="btncancelpopup" BackgroundCssClass="modalBackground" Y="100">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnltime" runat="server" Width="40%" ScrollBars="Auto" Style="display: none;
                background-color: rgb(17,24,32)">
                <table width="100%" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
                    <tr style="margin-left: 4%; border-right: 0px; border-left: 0px">
                        <td style="height: 30px; color: White; font-weight: bold; font-size: 15px; vertical-align: middle"
                            align="left">
                            <h5 style="color: White; margin-left: 6%; font-size: 18px">
                                Reset Password</h5>
                        </td>
                        <td style="color: White; font-weight: bold; font-size: larger; vertical-align: bottom;
                            padding-top: 20px; padding-right: 3%; border-right: 0px" align="right">
                            <asp:LinkButton ID="btncancelpopup" runat="server" Style="color: rgb(255,255,255);
                                text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 50%; padding-left: 3%; padding-right: 3%">
                            <div class="cusfielduser-cuscontaineruser">
                                <label class="cuslabeluser" style="text-align: left; color: rgb(255,255,255)">
                                    Password <span style="color: Red">*</span>
                                </label>
                                <input class="cusinputuserTextbox" id="txtMPass" runat="server" maxlength="20" tabindex="1"
                                    type="text" />
                                <asp:RequiredFieldValidator ID="rfvCCode" runat="server" ControlToValidate="txtMPass"
                                    Display="None" ErrorMessage="Password is required." SetFocusOnError="True" ValidationGroup="DC">*</asp:RequiredFieldValidator>
                            </div>
                        </td>
                        <td style="text-align: left; width: 50%; padding-left: 3%; padding-right: 3%">
                            <div class="cusfielduser-cuscontaineruser">
                                <label class="cuslabeluser" for="Phone" style="text-align: left; color: rgb(255,255,255)">
                                    Confirm Password <span style="color: Red">*</span>
                                </label>
                                <input class="cusinputuserTextbox" runat="server" id="txtMConfirmPass" maxlength="20"
                                    tabindex="2" type="text" />
                                <asp:RequiredFieldValidator ID="rfvEDate" runat="server" ControlToValidate="txtMConfirmPass"
                                    Display="None" ErrorMessage="Confirm Password is required." SetFocusOnError="True"
                                    ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtMPass"
                                    SetFocusOnError="false" ValidationGroup="DC" ControlToValidate="txtMConfirmPass"
                                    Display="None" ErrorMessage="Confirm Password not matched." Operator="Equal"
                                    Type="String"></asp:CompareValidator>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; padding-left: 2%; padding-right: 2%; padding-bottom: 20px;
                            padding-top: 20px; text-align: center" colspan="2">
                            <asp:Button ID="btnSaveExternal" runat="server" Text="Reset" CssClass="btn btn-danger"
                                ValidationGroup="DC" CausesValidation="true" TabIndex="5" OnClick="btnUpdatePasswordClick" />
                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="DC" ShowMessageBox="true"
                                runat="server" ShowSummary="false" />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; padding-left: 2%; padding-right: 2%; padding-bottom: 20px;
                            padding-top: 10px; text-align: left; color: White;" colspan="2">
                            Password Criteria :<br />
                            <br />
                            1. At least one lower case letter
                            <br />
                            2. At least one upper case letter<br />
                            3. Not include special character
                            <br />
                            4. At least one number
                            <br />
                            5. At least 8 characters length
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button ID="btnUserPre" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpeUserPref" runat="server" TargetControlID="btnUserPre"
                PopupControlID="pnlUserPref" BackgroundCssClass="modalBackground" Y="80" CancelControlID="PrefClose">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlUserPref" runat="server" Style="display: none; width: 60%; background-color: White;
                padding: 10px">
                <div class="form-group row">
                    <label class="col-12 col-form-label">
                        <h5 style="margin-left: 0%; font-size: 18px">
                            User Preferences</h5>
                    </label>
                </div>
                <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                    <label class="col-5 col-form-label">
                        Which city you want fly from ? <span class="MandatoryField">* </span>
                    </label>
                    <div class="col-7">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtScity1" Style="width: 90%;
                            text-transform: uppercase;" TabIndex="27" MaxLength="50" placeholder="Start City">
                        </asp:TextBox>
                        <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                            CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                            CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="true"
                            CompletionSetCount="1" TargetControlID="txtScity1" ID="AutoCompleteExtender1"
                            runat="server" FirstRowSelected="true">
                        </asp:AutoCompleteExtender>
                        <asp:Label ID="lblScity1" class="labelwithcolor" runat="server" Visible="false">
                        </asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtScity1"
                            Display="None" ErrorMessage="Start City is required." SetFocusOnError="True"
                            ValidationGroup="ST">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                    <label class="col-5 col-form-label">
                        Which city you want fly to ? <span class="MandatoryField">* </span>
                    </label>
                    <div class="col-7">
                        <asp:TextBox CssClass="form-control" runat="server" ID="txtEcity1" TabIndex="28"
                            Style="width: 90%; text-transform: uppercase;" MaxLength="50" placeholder="End City"> </asp:TextBox>
                        <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                            CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                            CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="true"
                            CompletionSetCount="1" TargetControlID="txtEcity1" ID="AutoCompleteExtender2"
                            runat="server" FirstRowSelected="true">
                        </asp:AutoCompleteExtender>
                        <asp:Label ID="lblEcity1" class="labelwithcolor" runat="server" Visible="false"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtEcity1"
                            Display="None" ErrorMessage="End City is required." SetFocusOnError="True" ValidationGroup="ST">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                    <label class="col-5 col-form-label">
                        From Date
                    </label>
                    <div class="col-7">
                        <asp:Label ID="lbldate1" runat="server" class="labelwithcolor" Visible="false" />
                        <asp:TextBox CssClass="form-control" runat="server" Style="width: 40%" ID="txtDate1"
                            TabIndex="29" MaxLength="20" Placeholder="MMM d, yyyy" onkeypress="return false;" />
                        <asp:CalendarExtender ID="CalendarExtender1" OnClientDateSelectionChanged="CheckDate"
                            CssClass="cal_Theme1" runat="server" TargetControlID="txtDate1" Format="MMM d, yyyy"
                            Enabled="True">
                        </asp:CalendarExtender>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtDate1"
                                Display="None" ErrorMessage="Available From Date is required." SetFocusOnError="True"
                                ValidationGroup="ST">*</asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                    <label class="col-5 col-form-label">
                        To Date
                    </label>
                    <div class="col-7">
                        <asp:Label ID="lbltodate1" runat="server" class="labelwithcolor" Visible="false" />
                        <asp:TextBox CssClass="form-control" runat="server" onkeypress="return false;" Placeholder="MMM d, yyyy"
                            Style="width: 40%" ID="txtToDate1" TabIndex="30" MaxLength="20" />
                        <asp:CalendarExtender ID="CalendarExtender4" OnClientDateSelectionChanged="CheckDate"
                            CssClass="cal_Theme1" runat="server" TargetControlID="txtToDate1" Format="MMM d, yyyy"
                            Enabled="True">
                        </asp:CalendarExtender>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtToDate1"
                                Display="None" ErrorMessage="Available To Date is required." SetFocusOnError="True"
                                ValidationGroup="ST">*</asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                    <label class="col-5 col-form-label">
                        What time you want to fly ?
                    </label>
                    <div class="col-5" style="padding-top: 5px;">
                        <asp:Label ID="lblflytime1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                        <asp:RadioButtonList ID="rblflytime1" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                            TabIndex="31">
                            <asp:ListItem Selected="True">&nbsp;Anytime&nbsp;</asp:ListItem>
                            <asp:ListItem>&nbsp;Specific Time&nbsp;</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-group row" id="trtime" runat="server" visible="false" style="font-size: 1.15vw;
                    font-weight: 400;">
                    <label class="col-5 col-form-label">
                        Enter Time <span class="MandatoryField">* </span>
                    </label>
                    <div class="col-7">
                        <asp:Label ID="lbltime1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                        <%--  <input class="form-control" runat="server" id="txtTime1" onkeypress="return NumericsColon(event)"
                                placeholder="Time(HH:mm)" style="width: 50%" maxlength="20" type="text" tabindex="31">--%>
                        <asp:TextBox ID="txtTime1" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
                        <input class="form-control" id="txtTime1New" tabindex="10" onblur="txtCityStartTimefun(this);"
                            type="time" style="width: 50%;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtTime1"
                            Display="None" ErrorMessage="Available time is required." SetFocusOnError="True"
                            ValidationGroup="ST">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row align-items-center">
                    <label class="col-8 col-form-label">
                        Do you want to recieve a email notification when trip is available ?
                    </label>
                    <div class="col-4">
                        <asp:Label ID="lblEmail1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                        <asp:RadioButtonList ID="rblEmailFlag" runat="server" Style="width: 45%;" RepeatDirection="Horizontal"
                            TabIndex="32">
                            <asp:ListItem Selected="True">&nbsp;Yes&nbsp;</asp:ListItem>
                            <asp:ListItem>&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-group row align-items-center">
                    <label class="col-8 col-form-label">
                        Do you want to recieve alert via SMS ?
                    </label>
                    <div class="col-4">
                        <asp:RadioButtonList ID="rblSMSNotifi" runat="server" Style="width: 45%;" RepeatDirection="Horizontal"
                            TabIndex="32">
                            <asp:ListItem Selected="True">&nbsp;No&nbsp;</asp:ListItem>
                            <asp:ListItem>&nbsp;Yes</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 text-center">
                        <asp:Button ID="btnAddMore" runat="server" Text="Add to List" CssClass="btn btn-danger"
                            TabIndex="33" ValidationGroup="ST" />&nbsp;
                        <asp:LinkButton ID="PrefClose" runat="server" Style="color: rgb(255,255,255); text-decoration: none"
                            CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="ST" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
                BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
                border: 2px solid red; min-width: 25%; padding: 10px">
                <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
                    cellpadding="0" cellspacing="0">
                    <tr style="border-right: 0px; border-left: 0px">
                        <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                            <asp:Label ID="lblalert" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                                text-decoration: none" CausesValidation="false" Text="Ok" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button ID="Button1" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpeAlertNew" runat="server" TargetControlID="Button1"
                PopupControlID="pnlAlertNew" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlAlertNew" runat="server" ScrollBars="Auto" Style="display: none;
                background-color: rgb(17,24,32); border: 2px solid red; min-width: 25%; padding: 10px">
                <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
                    cellpadding="0" cellspacing="0">
                    <tr style="border-right: 0px; border-left: 0px">
                        <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                            <asp:Label ID="lblAlertNew" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                                text-decoration: none" CausesValidation="false" Text="Ok" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
            <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
            <asp:HiddenField ID="hdfPSRID" runat="server" />
            <%-- <asp:HiddenField ID="lblMessage" runat="server" />--%>
            <%--<input type='hidden' id='txthidden2' runat="server" name='txthidden' />
    <input type='hidden' id='txthidden3' runat="server" name='txthidden1' />--%>
            <asp:Label ID="userflag" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblPreviousUserType" Text="Email" runat="server" Visible="false" />
            <asp:Label ID="lblCurrentUserType" Text="Email" runat="server" Visible="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
