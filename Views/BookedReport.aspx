﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="BookedReport.aspx.cs" Inherits="Views_BookedReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row m-0">
                        <div class="col-lg-12 p-0">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 11%; text-align: center">
                                    Tail Number
                                </label>
                                <div class="col-2" style="max-width: 11%;">
                                    <asp:TextBox ID="txtTailFilter" class="cusinputuser" placeholder="Enter tail #" runat="server"
                                        Width="100%" AutoPostBack="true" OnTextChanged="textBox1_TextChanged"></asp:TextBox>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    Trip Number
                                </label>
                                <div class="col-2" style="max-width: 11%;">
                                    <asp:TextBox ID="txttripno" class="cusinputuser" placeholder="Enter trip #" runat="server"
                                        Width="100%" AutoPostBack="true" OnTextChanged="txttripno_TextChanged"></asp:TextBox>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 10%;">
                                    From Date<span style="color: Red">*</span>
                                </label>
                                <div class="col-2" style="max-width: 12%;">
                                    <asp:TextBox ID="txtfromdate" class="cusinputuser" Placeholder="Date(MM-DD-YYYY)"
                                        onkeypress="return NumericsIphon(event)" runat="server" Width="98%" AutoPostBack="true"
                                        OnTextChanged="txtfromdate_changed"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtfromdate"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 8%;">
                                    To Date<span style="color: Red">*</span>
                                </label>
                                <div class="col-2" style="max-width: 12%;">
                                    <asp:TextBox ID="txttodate" class="cusinputuser" Placeholder="Date(MM-DD-YYYY)" onkeypress="return NumericsIphon(event)"
                                        runat="server" Width="98%" AutoPostBack="true" OnTextChanged="txtto_changed"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttodate"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                </div>
                                <div class="col-2" style="padding-top: 5px; max-width: 12%;">
                                    <asp:Button ID="Button2" runat="server" Text="Export To Excel" CssClass="btn btn-danger"
                                        OnClick="btnExport_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="dvGrid" runat="server">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body p-0" id="tblGrid" runat="server">
                    <div class="row m-0" id="dvheader" runat="server" visible="false">
                        <div class="col-xl-12 p-0">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="color: Black; font-size: 22px; background-color: gray;">
                                    JET EDGE
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0" id="dvheader1" runat="server" visible="false">
                        <div class="col-xl-12 p-0">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="color: Black; font-size: 22px; background-color: gray;">
                                    Booked Report
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 p-0 boxshadow table-responsive">
                            <asp:GridView ID="gvTailDetails" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                PagerStyle-CssClass="pager" AutoGenerateColumns="false" PageSize="25" EmptyDataText="No trips found."
                                AllowPaging="True" Width="100%" OnPageIndexChanging="gvTailDetails_PageIndexChanging"
                                OnSelectedIndexChanged="gvTailDetails_SelectedIndexChanged" OnRowCommand="ContactsGridView_RowCommandNew"
                                OnRowDataBound="gvpricing_price_OnRowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%"
                                        Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tail Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTailNo" Text='<%# Eval("TailNo") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trip Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltripno" Text='<%# Eval("Tripno") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Booked Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" Text='<%# Eval("book","{0:MMMM d, yyyy}") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trip Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" Text='<%# Eval("FromDate","{0:MMMM d, yyyy}") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Airport" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstartair" Text='<%# Eval("ActualStartAirport") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Airport" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblendair" Text='<%# Eval("ActualEndAirport") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Time" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstarttime" Text='<%# Eval("FromTime") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ToTime" HeaderText="End Time" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                    <asp:TemplateField HeaderText="Invoice Number" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID='lnkbtn' runat="server" CssClass="lnkbtn" Text='<%# Eval("Invoicenumber") %>'
                                                Style="color: #4A148C; font-weight: 600;" CommandName="Select"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Number" ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-HorizontalAlign="center" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblinvoice" Text='<%# Eval("Invoicenumber") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkstatus" CommandName="Status" runat="server" CommandArgument="<%# Container.DataItemIndex %>"
                                                CssClass="lnkbtn" Text='<%# Eval("PaymentStatus") %>' Style="color: #4A148C;
                                                font-weight: 600;"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                        Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lnkstatus1" Text='<%# Eval("PaymentStatus") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Type" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblusertype" Text='<%# Eval("UserTypeFlag") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltotal" Text='<%# Eval("TotalAmount","{0:c2}") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Left" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btntrans" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="mpetrans" runat="server" TargetControlID="btntrans" PopupControlID="pnltrans"
        BackgroundCssClass="modalBackground" Y="50" CancelControlID="btncancelpopup">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnltrans" runat="server" CssClass="modalPopup" BackColor="Black" Style="display: none;
        float: right; align: right; color: White; width: 30%; padding: 10px; position: fixed">
        <table style="width: 100%; padding-left: 5%">
            <tr style="padding-bottom: 20px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td style="color: White; font-size: 16px; vertical-align: middle">
                    Booking Details
                </td>
                <td style="margin-right: 2%; padding-right: 3%; color: White; font-weight: bold;
                    padding-bottom: 10px; padding-top: 10px; border-color: none; font-size: larger;
                    vertical-align: middle" align="right" colspan="5">
                    <asp:LinkButton ID="btncancelpopup" runat="server" Style="color: rgb(255,255,255);
                        text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label7" Text="Booked For" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblbookedname" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label8" Text="Booked Date" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblbookeddate" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label3" Text="Transaction Status" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lbltransaction" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label9" Text="Transaction ID" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lbltransid" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label4" Text="Amount" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblamount" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label10" Text="Mobile Number" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblmblno" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
