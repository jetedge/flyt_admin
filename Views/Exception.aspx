﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="Exception.aspx.cs" Inherits="Views_Exception" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-xl-125">
        </div>
        <div class="col-xl-75">
            <div class="row">
                <div class="col-xl-12 boxshadow table-responsive">
                    <asp:GridView ID="gvpricing" runat="server" AutoGenerateColumns="false" PageSize="50"
                        CssClass="table" HeaderStyle-CssClass="thead-dark" AllowPaging="True" DataKeyNames="TailNo"
                        Width="100%" EmptyDataText="No data found." OnPageIndexChanging="gvPricingDetails_PageIndexChanging"
                        OnRowDataBound="gvpricing_price_OnRowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tail Number" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                ItemStyle-Width="12%" HeaderStyle-Width="12%">
                                <ItemTemplate>
                                    <asp:Label ID="lblTailNo" Text='<%# Eval("TailNo") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Hourly Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                ItemStyle-Width="12%" HeaderStyle-Width="12%">
                                <ItemTemplate>
                                    <span>
                                        <asp:Label ID="lblHourlyRate" Text='<%# String.Format("{0:c0}", Eval("HourlyRate")) %>'
                                            runat="server"></asp:Label>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fixed Floor" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                ItemStyle-Width="11%" HeaderStyle-Width="11%">
                                <ItemTemplate>
                                    <asp:Label ID="lblFixedFloor" Text='<%# String.Format("{0:c0}", Eval("FixedFloor")) %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fixed Ceiling" HeaderStyle-HorizontalAlign="Right"
                                ItemStyle-HorizontalAlign="Right" ItemStyle-Width="11%" HeaderStyle-Width="11%">
                                <ItemTemplate>
                                    <asp:Label ID="lblstart" Text='<%# String.Format("{0:c0}", Eval("FixedCeiling")) %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pricing Status" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="13%" HeaderStyle-Width="13%">
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" />
                                    <asp:Label ID="lblprice" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Pricingcompleted") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Updated By" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblcreatedby" Text='<%# Eval("created") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Updated On" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblcreatedon" Text='<%# Eval("createdon") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="col-xl-125">
        </div>
    </div>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
