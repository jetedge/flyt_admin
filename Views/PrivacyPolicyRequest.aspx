﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="PrivacyPolicyRequest.aspx.cs" Inherits="Views_PrivacyPolicyRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="form-group row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
            <asp:Button ID="btnEmailSetup" runat="server" CssClass="btn btn-danger" Text="Email Setup"
                TabIndex="1" ToolTip="Click here to add" PostBackUrl="~/Views/PrivacyPolicySetup1.aspx" />
        </div>
    </div>
    <div class="form-group row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <asp:GridView ID="gvRemoval" runat="server" AutoGenerateColumns="False" Width="100%"
                OnSelectedIndexChanged="gvRemoval_SelectedIndexChanged" CssClass="table" HeaderStyle-CssClass="thead-dark"
                DataKeyNames="RowId" EmptyDataText="No Data Found">
                <Columns>
                    <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-Width="3%">
                        <ItemTemplate>
                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                            <asp:Label ID="lblPID" Visible="false" runat="server" Text='<%# Bind("RowId") %>'></asp:Label>
                            <asp:Label ID="lblSignedDate" Visible="false" runat="server" Text='<%# Bind("SignedDate") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-Wrap="true" HeaderStyle-Width="9%">
                        <ItemTemplate>
                            <asp:Label ID="lblCountryName" runat="server" Text='<%# Bind("CountryName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="First Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-Wrap="true" HeaderStyle-Width="9%">
                        <ItemTemplate>
                            <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-Wrap="true" HeaderStyle-Width="9%">
                        <ItemTemplate>
                            <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email Address" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-Wrap="true" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblEmailAddress" runat="server" Text='<%# Bind("EmailAddress") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-Wrap="true" HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:Label ID="lblRequestNumber" runat="server" Text='<%# Bind("RequestNumber") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reason" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-Wrap="true" HeaderStyle-Width="17%">
                        <ItemTemplate>
                            <asp:Label ID="lblReason" Visible="false" runat="server" Text='<%# Bind("Reason") %>'></asp:Label>
                            <asp:Label ID="lblIPAddress" Visible="false" runat="server" Text='<%# Bind("IPAddress") %>'></asp:Label>
                            <asp:Label ID="lblReaseonShort" runat="server" Text='<%# Bind("ReasonShort") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested On" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                        ItemStyle-Wrap="true" HeaderStyle-Width="14%">
                        <ItemTemplate>
                            <asp:Label ID="lblRequestedOn" runat="server" Text='<%# Bind("RequestedOn") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="View">
                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon-edit-new.png"
                                Height="18px" CommandName="Select" CausesValidation="false" AlternateText="Select" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeRemoval" BehaviorID="mpealert" runat="server" TargetControlID="btnalert"
        Y="55" PopupControlID="pnlRemoval" BackgroundCssClass="modalBackground" CancelControlID="btncancel">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlRemoval" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); width: 40%; overflow-x: hidden;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Personal Information Removal Request Form</h5>
                    <asp:Button ID="btncancel" runat="server" CssClass="btn btn-danger" CausesValidation="false"
                        Text="Close" />
                </div>
                <div class="modal-body" style="color: white; padding: 1.2rem;">
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Country of orgin : &nbsp;&nbsp;
                            <asp:Label ID="lblPopCountry" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Contact Email Address : &nbsp;&nbsp;
                            <asp:Label ID="lblPopEmail" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            First Name : &nbsp;&nbsp;
                            <asp:Label ID="lblPopFirstname" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Last Name : &nbsp;&nbsp;
                            <asp:Label ID="lblPopLastname" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Signed on this date of : &nbsp;&nbsp;
                            <asp:Label ID="lblPopSignedDate" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Reason for removal : &nbsp;&nbsp;
                            <asp:Label ID="lblPopReason" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Request from IP : &nbsp;&nbsp;
                            <asp:Label ID="lblPopIPAddress" runat="server" />
                        </div>
                    </div>
                    <br />
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Request Number : &nbsp;&nbsp;
                            <asp:Label ID="lblPopRequestNumber" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <div class="col-lg-12 text-left">
                            Requested On : &nbsp;&nbsp;
                            <asp:Label ID="lblPopRequestOn" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
