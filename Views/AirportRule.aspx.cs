﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using BusinessLayer;
using System.Drawing;

public partial class AirportRule : System.Web.UI.Page
{
    #region Global Declaration

    AirportRuleBLL objAirportRule = new AirportRuleBLL();

    public int linenum = 0;
    public string MethodName = string.Empty;

    #endregion

    #region Pageload

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!Page.IsPostBack)
            {
                // Put user code to initialize the page here

                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Airport Group");
                }


                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "General Setup";
                lblSubHeader.InnerText = "Airport Rule";

                Bind_Country_Dropdown();

                lblUserId.Text = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

                Bind_AirportRule();

                Session["ICAOP"] = null;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    protected void rblDepSetup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            dvDepState.Visible = false;
            dvDepAirports.Visible = false;

            if (rblDepSetup.SelectedValue == "S")
            {
                dvDepState.Visible = true;
                Bind_State(ddlDepCountry.SelectedValue, cbDepState);
            }
            else if (rblDepSetup.SelectedValue == "C")
            {
                dvDepCountry.Visible = true;
            }
            else if (rblDepSetup.SelectedValue == "A")
            {
                dvDepAirports.Visible = true;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void rblArrvSetup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            dvArrvState.Visible = false;
            dvArrvAirports.Visible = false;

            if (rblArrvSetup.SelectedValue == "S")
            {
                dvArrvState.Visible = true;
                Bind_State(ddlArrvCountry.SelectedValue, cblArrvState);
            }
            else if (rblArrvSetup.SelectedValue == "C")
            {
                dvArrvCountry.Visible = true;
            }
            else if (rblArrvSetup.SelectedValue == "A")
            {
                dvArrvAirports.Visible = true;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void ddlDepCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Bind_State(ddlDepCountry.SelectedValue, cbDepState);
            AutoCompleteExtender1.ContextKey = ddlDepCountry.SelectedValue;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void ddlArrvCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Bind_State(ddlArrvCountry.SelectedValue, cblArrvState);
            AutoCompleteExtender2.ContextKey = ddlArrvCountry.SelectedValue;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    private void Bind_State(string strCountryId, CheckBoxList cbDepState)
    {
        DataTable dtState = genclass.Bind_State_Dropdown(strCountryId);
        if (dtState.Rows.Count > 0)
        {
            cbDepState.Items.Clear();

            cbDepState.DataSource = dtState;
            cbDepState.DataTextField = "StateFullName";
            cbDepState.DataValueField = "State_Code";
            cbDepState.DataBind();
        }
        else
        {
            cbDepState.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    private void Bind_Airports(string strCountryId, CheckBoxList cblAirport)
    {
        DataTable dtState = genclass.Bind_Airports_Dropdown(strCountryId);
        if (dtState.Rows.Count > 0)
        {
            cblAirport.Items.Clear();

            cblAirport.DataSource = dtState;
            cblAirport.DataTextField = "ICAOCity";
            cblAirport.DataValueField = "ICAO";
            cblAirport.DataBind();
        }
        else
        {
            cblAirport.Items.Insert(0, new ListItem("Not Available", "0"));
        }
    }

    private void Bind_Country_Dropdown()
    {
        genclass.Bind_Country_Dropdown(ddlDepCountry);
        Bind_State(ddlDepCountry.SelectedValue, cbDepState);
        AutoCompleteExtender1.ContextKey = ddlDepCountry.SelectedValue;

        genclass.Bind_Country_Dropdown(ddlArrvCountry);
        Bind_State(ddlArrvCountry.SelectedValue, cblArrvState);
        AutoCompleteExtender2.ContextKey = ddlArrvCountry.SelectedValue;
    }

    protected void btnAdd_Clilck(object sender, EventArgs e)
    {
        try
        {
            if (hdnRowId.Text == "0")
            {
                objAirportRule.ManageType = "I";
                lblalert.Text = "Airport Rule Details have been saved successfully";
            }
            else
            {
                objAirportRule.ManageType = "U";
                lblalert.Text = "Airport Rule Details have been updated successfully";
            }

            objAirportRule.RuleId = hdnRowId.Text;
            objAirportRule.DepSetupBy = rblDepSetup.SelectedValue;
            objAirportRule.ArrSetupBy = rblArrvSetup.SelectedValue;

            if (rblDepSetup.SelectedValue == "C")
            {
                #region Country

                objAirportRule.DepRuleCode = ddlDepCountry.SelectedValue;
                objAirportRule.DepRuleName = ddlDepCountry.SelectedItem.Text;

                #endregion
            }
            else if (rblDepSetup.SelectedValue == "S")
            {
                #region State

                string DepState = string.Empty;
                string DepStateName = string.Empty;
                foreach (ListItem item in cbDepState.Items)
                {
                    if (item.Selected == true)
                    {
                        DepState += item.Value + ",";
                        DepStateName += item.Text + ",";
                    }
                }
                objAirportRule.DepRuleCode = DepState.TrimEnd(',');
                objAirportRule.DepRuleName = DepStateName.TrimEnd(',');

                #endregion
            }
            else if (rblDepSetup.SelectedValue == "A")
            {
                #region Airport

                string DepICAO = string.Empty;
                string[] arrDepICAO = txtDepAirport.Text.Split(',');
                if (arrDepICAO.Length > 0)
                {
                    for (int i = 0; i < arrDepICAO.Length; i++)
                    {
                        DepICAO += arrDepICAO[i].ToString() + ",";
                    }
                }
                else
                {
                    DepICAO += txtDepAirport.Text.ToString() + ",";
                }

                objAirportRule.DepRuleCode = DepICAO.TrimEnd(',');
                objAirportRule.DepRuleName = txtDepAirport.Text;

                #endregion
            }


            if (rblArrvSetup.SelectedValue == "C")
            {
                #region Country

                objAirportRule.ArrRuleCode = ddlArrvCountry.SelectedValue;
                objAirportRule.ArrRuleName = ddlArrvCountry.SelectedItem.Text;

                #endregion
            }
            else if (rblArrvSetup.SelectedValue == "S")
            {
                #region State

                string ArrvState = string.Empty;
                string ArrvStateName = string.Empty;
                foreach (ListItem item in cblArrvState.Items)
                {
                    if (item.Selected == true)
                    {
                        ArrvState += item.Value + ",";
                        ArrvStateName += item.Text + ",";
                    }
                }
                objAirportRule.ArrRuleCode = ArrvState.TrimEnd(',');
                objAirportRule.ArrRuleName = ArrvStateName.TrimEnd(',');

                #endregion
            }
            else if (rblArrvSetup.SelectedValue == "A")
            {
                #region Airport

                string ArrvICAO = string.Empty;
                string[] arrArrvICAO = txtArrvAirport.Text.Split(',');
                if (arrArrvICAO.Length > 0)
                {
                    for (int i = 0; i < arrArrvICAO.Length; i++)
                    {
                        ArrvICAO += arrArrvICAO[i].ToString() + ",";
                    }
                }
                else
                {
                    ArrvICAO += txtArrvAirport.Text.ToString() + ",";
                }

                objAirportRule.ArrRuleCode = ArrvICAO.TrimEnd(',');
                objAirportRule.ArrRuleName = txtArrvAirport.Text;

                #endregion
            }

            objAirportRule.Status = "Y";
            objAirportRule.CreatedBy = lblUserId.Text;
            objAirportRule.CreatedOn = DateTime.Now.ToString();
            objAirportRule.ModifiedBy = lblUserId.Text;
            objAirportRule.ModifiedOn = DateTime.Now.ToString();

            int retVal = objAirportRule.Save(objAirportRule);

            if (retVal > 0)
            {
                mpealert.Show();

                Clear();
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request. Please try after sometime.";
                mpealert.Show();
            }

            Bind_AirportRule();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void btnCancel_Clilck(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    private void Bind_AirportRule()
    {
        try
        {
            DataTable dtAirportRule = objAirportRule.Select();
            if (dtAirportRule.Rows.Count > 0)
            {
                gvAirportRule.DataSource = dtAirportRule;
                gvAirportRule.DataBind();

                lnkRefreshData.Visible = true;
            }
            else
            {
                gvAirportRule.DataSource = null;
                gvAirportRule.DataBind();

                lnkRefreshData.Visible = false;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void gvAirportRule_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow HeaderRow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);

                TableCell HeaderCell2 = new TableCell();
                HeaderCell2.Text = "";
                HeaderCell2.ColumnSpan = 1;
                HeaderCell2.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell2.Font.Bold = true;
                HeaderRow.Cells.Add(HeaderCell2);

                HeaderCell2 = new TableCell();
                HeaderCell2.Text = "<i class='fa fa-plane-departure' style='color: black;'></i> &nbsp; Departure Airport";
                HeaderCell2.ColumnSpan = 2;
                HeaderCell2.HorizontalAlign = HorizontalAlign.Left;
                HeaderCell2.Font.Bold = true;
                HeaderRow.Cells.Add(HeaderCell2);

                HeaderCell2 = new TableCell();
                HeaderCell2.Text = "<i class='fa fa-plane-arrival' style='color: black;'></i> &nbsp; Arrival Airport";
                HeaderCell2.ColumnSpan = 2;
                HeaderCell2.Font.Bold = true;
                HeaderCell2.HorizontalAlign = HorizontalAlign.Left;
                HeaderRow.Cells.Add(HeaderCell2);

                HeaderCell2 = new TableCell();
                HeaderCell2.Text = " ";
                HeaderCell2.ColumnSpan = 1;
                HeaderCell2.Font.Bold = true;
                HeaderCell2.HorizontalAlign = HorizontalAlign.Left;
                HeaderRow.Cells.Add(HeaderCell2);

                HeaderCell2 = new TableCell();
                HeaderCell2.Text = " ";
                HeaderCell2.ColumnSpan = 1;
                HeaderCell2.Font.Bold = true;
                HeaderCell2.HorizontalAlign = HorizontalAlign.Center;
                HeaderRow.Cells.Add(HeaderCell2);

                HeaderCell2 = new TableCell();
                HeaderCell2.Text = " ";
                HeaderCell2.ColumnSpan = 1;
                HeaderCell2.Font.Bold = true;
                HeaderCell2.HorizontalAlign = HorizontalAlign.Center;
                HeaderRow.Cells.Add(HeaderCell2);

                HeaderRow.BackColor = ColorTranslator.FromHtml("#eceaea");
                gvAirportRule.Controls[0].Controls.AddAt(0, HeaderRow);
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    private void Clear()
    {
        rblDepSetup.SelectedIndex = 0;
        ddlDepCountry.ClearSelection();

        dvDepState.Visible = false;
        cbDepState.Items.Clear();
        txtDepState.Text = string.Empty;

        dvDepAirports.Visible = false;
        txtDepAirport.Text = string.Empty;

        rblArrvSetup.SelectedIndex = 0;
        ddlArrvCountry.ClearSelection();

        dvArrvState.Visible = false;
        cblArrvState.Items.Clear();
        txtArrvState.Text = string.Empty;

        dvArrvAirports.Visible = false;
        txtArrvAirport.Text = string.Empty;

        hdnRowId.Text = "0";
        btnAdd.Text = "Add to List";

        foreach (GridViewRow row in gvAirportRule.Rows)
        {
            row.BackColor = Color.Transparent;
        }
    }

    protected void imbDelete_click(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;
            Label lblRuleId = (Label)gvrow.FindControl("lblRuleId");

            objAirportRule.DeletAirportRule(lblRuleId.Text.Trim());

            Bind_AirportRule();

            lblalert.Text = "Airport Rule Details have been deleted successfully";
            mpealert.Show();


        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void imbEdit_click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow row in gvAirportRule.Rows)
            {
                row.BackColor = Color.Transparent;
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;

            Label lblRuleId = (Label)gvrow.FindControl("lblRuleId");
            hdnRowId.Text = lblRuleId.Text;

            #region Departure Airport
            Label lblDepSetupByFlag = (Label)gvrow.FindControl("lblDepSetupByFlag");
            Label lblDepRuleCode = (Label)gvrow.FindControl("lblDepRuleCode");
            rblDepSetup.SelectedIndex = rblDepSetup.Items.IndexOf(rblDepSetup.Items.FindByValue(lblDepSetupByFlag.Text));
            rblDepSetup_SelectedIndexChanged(sender, e);
            if (rblDepSetup.SelectedValue == "S")
            {
                txtDepState.Text = ((Label)gvrow.FindControl("lblDepRuleName")).Text;
                if (lblDepRuleCode.Text.ToString().Length > 0)
                {
                    string[] strDept = lblDepRuleCode.Text.Split(',');
                    if (strDept.Length > 0)
                    {
                        for (int i = 0; i < strDept.Length; i++)
                        {
                            foreach (ListItem item in cbDepState.Items)
                            {
                                if (item.Value == strDept[i].Trim())
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        cbDepState.SelectedIndex = cbDepState.Items.IndexOf(cbDepState.Items.FindByValue(lblDepRuleCode.Text));
                    }
                }
            }
            else
            {
                txtDepAirport.Text = ((Label)gvrow.FindControl("lblDepRuleName")).Text;
            }
            #endregion

            #region Arrival Airport
            Label lblArrSetupByFlag = (Label)gvrow.FindControl("lblArrSetupByFlag");
            Label lblArrRuleCode = (Label)gvrow.FindControl("lblArrRuleCode");
            rblArrvSetup.SelectedIndex = rblArrvSetup.Items.IndexOf(rblArrvSetup.Items.FindByValue(lblArrSetupByFlag.Text));
            rblArrvSetup_SelectedIndexChanged(sender, e);
            if (rblArrvSetup.SelectedValue == "S")
            {
                txtArrvState.Text = ((Label)gvrow.FindControl("lblArrRuleName")).Text;
                if (lblArrRuleCode.Text.ToString().Length > 0)
                {
                    string[] strArrv = lblArrRuleCode.Text.Split(',');
                    if (strArrv.Length > 0)
                    {
                        for (int i = 0; i < strArrv.Length; i++)
                        {
                            foreach (ListItem item in cblArrvState.Items)
                            {
                                if (item.Value == strArrv[i].Trim())
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        cblArrvState.SelectedIndex = cblArrvState.Items.IndexOf(cblArrvState.Items.FindByValue(lblArrRuleCode.Text));
                    }
                }
            }
            else
            {
                txtArrvAirport.Text = ((Label)gvrow.FindControl("lblArrRuleName")).Text;
            }
            #endregion

            btnAdd.Text = "Update";
            gvrow.BackColor = Color.LightCyan;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void lnkRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[CON_Setup_05_CDFLAG]", false);
            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.StoredProcedure, "[CON_Setup_05_CDFLAG]", getAllTripParm).Tables[0];

            mpeRefreshData.Hide();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #region Web Services

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText, string contextKey)
    {
        return AutoFillProducts(prefixText, contextKey);

    }

    private static List<string> AutoFillProducts(string prefixText, string contextKey)
    {
        DataTable dtNew = new DataTable();
        DataTable dt = new DataTable();
        DataRow[] drNew = null;
        if (HttpContext.Current.Session["ICAOP"] == null)
        {
            dt = GetZoomAirports("AIRP");
            HttpContext.Current.Session["ICAOP"] = dt;
        }

        dt = (DataTable)HttpContext.Current.Session["ICAOP"];
        if (dt.Rows.Count > 0)
        {
            DataRow[] results = dt.Select("Country = '" + contextKey + "'");
            if (results.Length > 0)
            {
                DataTable dtCountry = results.CopyToDataTable();

                if (prefixText.Length < 5)
                {
                    drNew = dtCountry.Select("ICAO like '%' + '" + prefixText + "' + '%'");
                }
                else
                {
                    drNew = dtCountry.Select("StateName like  '" + prefixText + "' + '%' OR City like '" + prefixText + "' + '%'");
                }

                if (drNew.Length > 0)
                {
                    dtNew = drNew.CopyToDataTable();
                }
            }

        }

        List<string> sEmail = new List<string>();
        for (int i = 0; i < drNew.Length; i++)
        {
            sEmail.Add(drNew[i]["ICAO"].ToString());
        }
        return sEmail;
    }

    public static DataTable GetZoomAirports(string strManageType)
    {
        SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_Zoom_Aiports]", false);
        getAllTripParm[0].Value = strManageType;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.StoredProcedure, "[SP_Zoom_Aiports]", getAllTripParm).Tables[0];
    }

    #endregion
}