﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Views_UnSubscribeV1 : System.Web.UI.Page
{
    public int linenum = 0;
    public string MethodName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                //if (Request.QueryString["Id"] != null && Request.QueryString["Flag"] != null)
                //{
                //    if (Request.QueryString["id"].ToString().Length > 0 && Request.QueryString["Flag"].ToString().Length > 0)
                //    {
                //        lblFromFlag.Text = Request.QueryString["Flag"].ToString();

                //        lblFromId.Text = Request.QueryString["id"].ToString();

                //        if (lblFromFlag.Text.Trim() == "NZ")
                //        {
                //            GetNonZoomMail();
                //        }
                //        else
                //        {
                //            GetZoomMail();
                //        }


                //    }
                //}
                divAfter.Visible = false;
                divBefore.Visible = true;
            }
            catch (Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }
    }


    public void GetNonZoomMail()
    {
        DataTable dtGetMailId = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "Select * from NonZoomCustomer Where Email='" + lblFromId.Text + "'").Tables[0];
        if (dtGetMailId.Rows.Count > 0)
        {

            lblAMailId.Text = dtGetMailId.Rows[0]["Email"].ToString();
        }
    }

    public void GetZoomMail()
    {
        DataTable dtGetMailId = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "Select * from UserMasterCreate Where RowId='" + lblFromId.Text + "'").Tables[0];
        if (dtGetMailId.Rows.Count > 0)
        {
            lblAMailId.Text = dtGetMailId.Rows[0]["Email"].ToString();
        }
    }
    protected void btnUnsub_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtEmailId.Text.Length > 0)
            {
                DataTable dtGetMailIdForNZ = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "Select * from NonZoomCustomer Where Email='" + txtEmailId.Text.Trim() + "'").Tables[0];

                if (dtGetMailIdForNZ.Rows.Count > 0)
                {
                    if (selectUnsubscribeReason.SelectedValue == "others")
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update NonZoomCustomer set MustMoveFlag='N',UnSubReason='" + unsubscribereason.Text + "',UnSubBy='" + dtGetMailIdForNZ.Rows[0]["FIrstName"].ToString().Trim() + " " + dtGetMailIdForNZ.Rows[0]["LastName"].ToString().Trim() + "',UnSubOn='" + DateTime.Now.ToString() + "' where RowID='" + dtGetMailIdForNZ.Rows[0]["RowId"].ToString().Trim() + "'");
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update NonZoomCustomer set MustMoveFlag='N',UnSubReason='" + selectUnsubscribeReason.SelectedValue + "',UnSubBy='" + dtGetMailIdForNZ.Rows[0]["FIrstName"].ToString().Trim() + " " + dtGetMailIdForNZ.Rows[0]["LastName"].ToString().Trim() + "',UnSubOn='" + DateTime.Now.ToString() + "' where RowID='" + dtGetMailIdForNZ.Rows[0]["RowId"].ToString().Trim() + "'");
                    }
                }

                DataTable dtGetMailIdZoom = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "Select * from UserMasterCreate Where Email='" + txtEmailId.Text.Trim() + "' and Role not in ('Admin')").Tables[0];

                if (dtGetMailIdZoom.Rows.Count > 0)
                {
                    if (selectUnsubscribeReason.SelectedValue == "others")
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set MustMoveMailFlag='N',UnSubReason='" + unsubscribereason.Text + "',UnSubBy='" + dtGetMailIdZoom.Rows[0]["FIrstName"].ToString().Trim() + " " + dtGetMailIdZoom.Rows[0]["LastName"].ToString().Trim() + "',UnSubOn='" + DateTime.Now.ToString() + "' where RowID='" + dtGetMailIdZoom.Rows[0]["RowId"].ToString().Trim() + "'");
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set MustMoveMailFlag='N',UnSubReason='" + selectUnsubscribeReason.SelectedValue + "',UnSubBy='" + dtGetMailIdZoom.Rows[0]["FIrstName"].ToString().Trim() + " " + dtGetMailIdZoom.Rows[0]["LastName"].ToString().Trim() + "',UnSubOn='" + DateTime.Now.ToString() + "' where RowID='" + dtGetMailIdZoom.Rows[0]["RowId"].ToString().Trim() + "'");
                    }
                }

            }


            lblAMailId.Text = txtEmailId.Text;
            divAfter.Visible = true;
            divBefore.Visible = false;

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void selectUnsubscribeReason_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (selectUnsubscribeReason.SelectedValue == "others")
            {
                unsubscribereason.Visible = true;
                selectUnsubscribeReason.Visible = false;
            }
            else
            {
                unsubscribereason.Visible = false;
                selectUnsubscribeReason.Visible = true;
            }

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
}