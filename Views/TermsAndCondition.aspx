﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="TermsAndCondition.aspx.cs" Inherits="Views_TermsAndCondition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .file-upload-new
        {
            display: inline-block;
            overflow: hidden;
            position: relative;
            text-align: center;
            vertical-align: middle;
            background: white;
            color: black;
            border-radius: 5px;
            -moz-border-radius: 6px;
            font-weight: 600;
            border: 2px dashed lightgray;
            min-height: 60px;
            padding: 60px;
            margin-bottom: 0px;
            cursor: pointer;
        }
        
        .file-upload-new:hover
        {
            background: white;
        }
        
        .file-upload-new, .file-upload-new span
        {
            width: 99%;
        }
        
        .file-upload-new input
        {
            position: absolute;
            top: 0;
            left: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        
        .file-upload-new strong
        {
            font: normal 13.5px sans-serif;
            font-weight: 600;
            vertical-align: middle;
        }
        
        .file-upload-new span
        {
            position: absolute;
            top: 4px;
            left: 4px; /* display: inline-block; */ /* padding-top: 1.15em; */ /* font-size: 0rem; */
            padding: 4px;
            text-align: left;
            font-size: 0;
        }
    </style>
    <script type="text/javascript">
        function FileDetails1() {
            debugger;
            var fi = document.getElementById('MainContent_fuFile');
            document.getElementById('MainContent_listofuploadedfilesCount1').innerHTML = '';
            document.getElementById('MainContent_listofuploadedfiles1').innerHTML = '';

            if (fi.files.length > 0) {

                document.getElementById('MainContent_listofuploadedfilesCount1').innerHTML =
                'File Name : ';

                for (var i = 0; i <= fi.files.length - 1; i++) {

                    var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
                    var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.

                    document.getElementById('MainContent_listofuploadedfiles1').innerHTML =
                    document.getElementById('MainContent_listofuploadedfiles1').innerHTML + ' ' +
                         fname + ' ( <b>' + fsize + '</b> bytes ) ';
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
        <ContentTemplate>--%>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row" id="tblGrid" runat="server">
                    <div class="col-xl-12">
                        <div class="kt-portlet">
                            <div class="form-group row">
                                <div class="col-xl-1">
                                </div>
                                <div class="col-xl-7 text-left">
                                    <div class="form-group row">
                                        <label class="col-4 col-form-label">
                                            Version # :
                                            <asp:Label ID="lblVersion" runat="server" Style="font-weight: 500;"></asp:Label>
                                        </label>
                                        <label class="col-4 col-form-label">
                                            Effective From :
                                            <asp:Label ID="lblEfFrom" runat="server" Style="font-weight: 500;"></asp:Label>
                                        </label>
                                        <label class="col-4 col-form-label">
                                            Notes :
                                            <asp:Label ID="lblNotes" runat="server" Style="font-weight: 500;"></asp:Label>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xl-3 text-right" style="padding-right: 30px;">
                                    <label id="lblMan" runat="server" class="importantlabels" visible="false">
                                        <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                            ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                                    </label>
                                    <asp:Button ID="btnHist" runat="server" Text="History" CssClass="btn btn-danger"
                                        TabIndex="1" />
                                    <asp:Button ID="btnAdd" runat="server" Text="Add / Update" Style="display: none;"
                                        CssClass="btn btn-danger" TabIndex="1" />
                                </div>
                                <div class="col-xl-1">
                                </div>
                            </div>
                            <div class="row" id="tblForm" runat="server">
                                <div class="col-xl-1">
                                </div>
                                <div class="col-xl-10">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <iframe id="pdfIFrame" style="width: 100%; background: white; border: 0;" runat="server">
                                            </iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-1">
                                </div>
                                <div class="col-xl-5">
                                    <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                        <div style="float: left; padding-top: 10px; padding-left: 15px;">
                                            <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                                font-weight: 600;"></asp:Label>
                                            <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="tblMain" runat="server" visible="false">
                    <div class="col-xl-2">
                    </div>
                    <div class="col-xl-8">
                        <div class="kt-portlet">
                            <div class="form-group row">
                                <div class="col-xl-3 text-left">
                                </div>
                                <div class="col-xl-9 text-right">
                                    <label id="Label1" runat="server" class="importantlabels">
                                        <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                            ID="Label2" Text="Mandatory fields"></asp:Label>
                                    </label>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                        ValidationGroup="ST" />
                                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1" />
                                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="ST" ShowMessageBox="true"
                                        runat="server" ShowSummary="false" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Version # <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-10">
                                    <asp:TextBox ID="txtVersionNo" ValidationGroup="ST" runat="server" MaxLength="50"
                                        TabIndex="1" ToolTip="enter all type of characters. Length is limited to 50"
                                        Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtVersionNo"
                                        Display="None" ErrorMessage="Version # is required." SetFocusOnError="True" ValidationGroup="ST"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Effective From <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-10">
                                    <asp:TextBox ID="txtEffFrom" runat="server" ValidationGroup="ST" MaxLength="50" onkeypress="return false;"
                                        TabIndex="3" ToolTip="Please enter only alphabets and space. Length is limited to 50."
                                        Width="30%"></asp:TextBox>
                                    <asp:CalendarExtender ID="calendar2" ClientIDMode="Static" runat="server" TargetControlID="txtEffFrom"
                                        Format="MM/dd/yyyy" DefaultView="Days">
                                    </asp:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEffFrom"
                                        Display="None" ErrorMessage="Effective From is required." SetFocusOnError="True"
                                        ValidationGroup="ST"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                </label>
                                <div class="col-10 text-center">
                                    <label class="file-upload-new" id="lblfuFile" runat="server">
                                        <div class="kt-dropzone__msg dz-message needsclick">
                                            <h3 class="kt-dropzone__msg-title" style="color: Green">
                                                Drop file or click to upload.</h3>
                                        </div>
                                        <asp:FileUpload ID="fuFile" AllowMultiple="false" runat="server" TabIndex="7" ToolTip="Drop files here or click to upload."
                                            accept=".pdf,.PDF" onchange="FileDetails1()" Style="width: 100%; min-height: 120px;
                                            cursor: pointer;"></asp:FileUpload>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="fuFile"
                                            ValidationGroup="ST" Display="None" ErrorMessage="Please Upload File"></asp:RequiredFieldValidator>
                                    </label>
                                </div>
                                <label class="col-2 col-form-label">
                                </label>
                                <div class="col-10" style="padding-top: 10px; padding-bottom: 10px;">
                                    <asp:Label ID="listofuploadedfilesCount1" Style="line-height: 13px; color: #a72224;
                                        font-weight: 600;" runat="server" />
                                    <asp:Label ID="listofuploadedfiles1" Style="line-height: 13px;" runat="server" />
                                </div>
                            </div>
                            <div style="display: none;">
                                <asp:Label ID="lblFilePath" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblFileName" runat="server" Visible="false" Style="padding-right: 10px;"></asp:Label>
                                <asp:LinkButton ID="imgFUF206Download1" runat="server" Visible="false" CommandName="Download"
                                    Style="color: Blue; font-weight: bold;" ToolTip="Click here to Download" Text="View"
                                    CausesValidation="true" OnClick="imgFUF206Download1_Click" CssClass="pagenav">
                                </asp:LinkButton>
                                <asp:LinkButton ID="img206Form" runat="server" Visible="false" CommandName="Delete"
                                    ToolTip="Click here to delete" CausesValidation="true" OnClick="img206Form_Delete"
                                    Text="Delete" Style="color: Red; font-weight: bold; padding-left: 10px;" CssClass="pagenav">
                                                        
                                </asp:LinkButton>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Notes
                                </label>
                                <div class="col-10">
                                    <asp:TextBox ID="txtNotes" runat="server" ToolTip="Enter all type of characters. Length is limited to 100."
                                        MaxLength="100" Width="100%" TabIndex="5"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnHistNr" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeHistory" runat="server" PopupControlID="pnlHistory"
        TargetControlID="btnHistNr" Y="10" CancelControlID="btnHistClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlHistory" runat="server" CssClass="modalPopupnew" Style="display: none;
        background-color: rgb(17,24,32); width: 65%; height: 60%; padding: 10px">
        <div class="form-group row">
            <label class="col-9 col-form-label" style="color: White;">
                <h5 style="color: White; margin-left: 0%; font-size: 18px">
                    History</h5>
            </label>
            <div class="col-lg-3 text-right">
                <asp:Button ID="btnHistClose" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="padding: 3px 12px;" ToolTip="Close Popup"></asp:Button>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-12">
                <asp:GridView ID="gvHistory" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                    EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Effective From">
                            <ItemTemplate>
                                <asp:Label ID="lblEffFrom" runat="server" Text='<%#Bind("EffFrom")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Version">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Bind("Version")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes">
                            <ItemTemplate>
                                <asp:Label ID="lblNotes" runat="server" Text='<%#Bind("Notes")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Download" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblFileName" runat="server" Text='<%#Bind("FileName")%>' Style="color: White;
                                    display: none"></asp:Label>
                                <asp:Label ID="lblFilePath" runat="server" Text='<%#Bind("FilePath")%>' Style="color: White;
                                    display: none"></asp:Label>
                                <asp:ImageButton ID="imgDownload" runat="server" TabIndex="4" Height="18px" ImageUrl="~/Images/download.png"
                                    CausesValidation="false" AlternateText="download" ToolTip="Click here to download."
                                    OnClick="imgDownload_onClick" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
