﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginPage.aspx.cs" Inherits="Views_LoginPage"
    Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>JetEdge | FLYT | Admin</title>
    <meta name="description" content="Latest updates and statistic charts" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="shortcut icon" href="../assets/img/Logo_Tra.gif" />
    <link href="../assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="../assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/vendors/custom/vendors/fontawesome5/css/all.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../assets/demo/demo4/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .btn i
        {
            padding-right: 0;
            vertical-align: middle;
            line-height: initial;
        }
        .homepage
        {
            background-image: url('../Images/LandingPage/flightHome2.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: 100% 100%;
            background-size: cover;
        }
        .cusinputuserTextbox
        {
            margin: 0 !important;
            width: 100% !important;
            background-color: rgb(153,153,154) !important;
            color: rgb(255,255,255) !important;
            border: 1px solid #ccc !important;
            padding: 11px !important;
            font-family: inherit !important;
            font-size: 17px !important;
        }
        .input-group i
        {
            color: #555 !important;
            line-height: 0;
        }
        .form-group
        {
            margin-bottom: 1.5rem;
        }
        .input-group [class^="fa-"], .input-group [class*=" fa-"]
        {
            font-size: 1.4rem;
        }
        
        .hint
        {
            border: 3px solid #c93;
            letter-spacing: 0.5px;
            font-size: 12px;
            text-align: left;
            padding: 3px 0px 3px 3px;
            width: 251px;
            position: relative;
            display: none;
            background: #ffc;
            z-index: 9999999;
            line-height: 20px;
            color: #111820;
        }
        
        /* The pointer image is hadded by using another span */
        .hint-pointer
        {
            position: absolute;
            left: -13px;
            top: -2px;
            width: 10px;
            height: 19px;
            background: url(../Components/Images/pointer.gif) no-repeat;
            z-index: 9999999;
        }
        .hint-img
        {
            margin-right: 5px;
            margin-left: 8px;
            height: 13px;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function Numericswithspecial(event) {
            var regex = new RegExp("^[0-9-!@#$%*?+-]");
            var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }

        function ValidateInputLength(obj, textType) {
            if (obj.value != "" && obj.value.length < 8) {
                alert(textType + ' should not be less than eight characters!');
                obj.value = ""; obj.focus();
            }
        }

        if (document.getElementById('txthidden') != null)
            var count = document.getElementById('txthidden').value;
        else
            var count = 0;

        if (document.getElementById('txthidden1') != null)
            var count1 = document.getElementById('txthidden1').value;
        else
            var count1 = 0;

        function show(obj) {
            document.getElementById('divShow').style.display = "block";
            var password = obj.value;

            var lowercase = "abcdefghijklmnopqrstuvwxyz";
            var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var number = "1234567890";
            //var special = "";
            var nonspecial = "~`!@#$%^&*()+=[{]}\|;:',<.>/?_-";



            var containsLowerCase = Contains(password, lowercase);
            var containsUpperCase = Contains(password, uppercase);
            var containsNumber = Contains(password, number);
            //var containsSpecial = Contains(password, special);
            var containsNonSpecial = Contains(password, nonspecial);

            if (password.length > 8) {
                document.getElementById('imgyes1').style.display = "block";
                document.getElementById('imgno1').style.display = "none";
            }
            else {
                document.getElementById('imgno1').style.display = "block";
                document.getElementById('imgyes1').style.display = "none";
                count = count + 1;
            }

            if (containsLowerCase || containsUpperCase) {
                document.getElementById('imgyes2').style.display = "block";
                document.getElementById('imgno2').style.display = "none";
            }
            else {
                document.getElementById('imgno2').style.display = "block";
                document.getElementById('imgyes2').style.display = "none";
                count = count + 1;
            }

            if (containsLowerCase) {
                document.getElementById('imgyes3').style.display = "block";
                document.getElementById('imgno3').style.display = "none";
            }
            else {
                document.getElementById('imgno3').style.display = "block";
                document.getElementById('imgyes3').style.display = "none";
                count = count + 1;
            }

            if (containsUpperCase) {
                document.getElementById('imgyes4').style.display = "block";
                document.getElementById('imgno4').style.display = "none";
            }
            else {
                document.getElementById('imgno4').style.display = "block";
                document.getElementById('imgyes4').style.display = "none";
                count = count + 1;
            }

            if (containsNumber) {
                document.getElementById('imgyes5').style.display = "block";
                document.getElementById('imgno5').style.display = "none";
            }
            else {
                document.getElementById('imgno5').style.display = "block";
                document.getElementById('imgyes5').style.display = "none";
                count = count + 1;
            }


            if (containsNonSpecial) {
                document.getElementById('imgyes7').style.display = "none";
                document.getElementById('imgno7').style.display = "block";
                count = count + 1;
            }
            else {
                document.getElementById('imgno7').style.display = "none";
                document.getElementById('imgyes7').style.display = "block";
            }
        }

        function passvalidate(obj) {
            show(obj);

            if (document.getElementById('imgno1').style.display == "block") {
                //obj.focus();
                document.getElementById('divShow').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                // alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno2').style.display == "block") {
                //obj.focus();
                document.getElementById('divShow').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno3').style.display == "block") {
                //obj.focus();
                document.getElementById('divShow').style.display = "block";
                //  alert("Please check the password criteria");
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
            }
            else if (document.getElementById('imgno4').style.display == "block") {
                //obj.focus();
                document.getElementById('divShow').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno5').style.display == "block") {
                //obj.focus();
                document.getElementById('divShow').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno7').style.display == "block") {
                //obj.focus();
                document.getElementById('divShow').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                //  alert("Please check the password criteria");
            }
            else {
                document.getElementById('divShow').style.display = "none";
            }
        }

        function showlist() {
            document.getElementById('divShow').style.display = "block";
        }

        function showlistpass() {

            document.getElementById('divshowpassmatch').style.display = "block";
        }

        function checkpassmatch(obj) {

            validatepass(obj);
            if (count1 > 0) {
                //alert("Please check the password criteria");
                //obj.focus();
                document.getElementById('divshowpassmatch').style.display = "block";
            }
            document.getElementById('divshowpassmatch').style.display = "none";
        }

        function validatepass(obj) {

            var confirmpassword = obj.value;
            var password = document.getElementById('txtNewPassword').value;

            if (confirmpassword == password) {
                document.getElementById('Imgpassyes').style.display = "block";
                document.getElementById('Imgpassno').style.display = "none";
                count1 = 0;
                //  document.getElementById('btnSubmit').removeAttribute("disabled");
            }
            else {
                document.getElementById('Imgpassyes').style.display = "none";
                document.getElementById('Imgpassno').style.display = "block";
                count1 = count1 + 1;
                //  document.getElementById('btnSubmit').setAttribute("disabled", "disabled");
            }
        }

        function CheckPassword(obj) {
            var password = obj.value;
            var userName = document.getElementById('txtUserName').value;

            var lowercase = "abcdefghijklmnopqrstuvwxyz";
            var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var number = "1234567890";

            // find out if the password different cases
            var containsLowerCase = Contains(password, lowercase);
            var containsUpperCase = Contains(password, uppercase);
            var containsNumber = Contains(password, number);

            if ((containsLowerCase || containsUpperCase) && containsNumber) {

                if (password.length < 8) {
                    if (password != "") {
                        alert("Password length should not be less than 8 digits");
                    }
                    obj.value = "";
                    obj.focus();
                }
                else if (password == userName) {
                    if (password != "") {
                        alert("Password can not be same as username");
                    }
                    obj.value = "";
                    obj.focus();
                }
            }
            else {
                if (password != "") {
                    alert("Password should contain both Alphabets and Number");
                    obj.value = "";
                    obj.focus();
                }
            }
        }

        // checks if a password contains one of a set of characters
        function Contains(password, validChars) {

            for (i = 0; i < password.length; i++) {

                var char = password.charAt(i);
                if (validChars.indexOf(char) > -1) {
                    return true;
                }
            }
            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function ValidateInputLength1(obj, textType) {
            if (obj.value != "" && obj.value.length < 8) {
                alert(textType + ' should not be less than eight characters!');
                obj.value = ""; obj.focus();
            }
        }

        if (document.getElementById('hdnpassword1') != null)
            var count = document.getElementById('hdnpassword1').value;
        else
            var count = 0;

        if (document.getElementById('hdnpassword2') != null)
            var count1 = document.getElementById('hdnpassword2').value;
        else
            var count1 = 0;

        function show1(obj) {
            document.getElementById('divshowuserC').style.display = "block";
            var password = obj.value;

            var lowercase = "abcdefghijklmnopqrstuvwxyz";
            var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var number = "1234567890";
            //var special = "";
            var nonspecial = "~`!@#$%^&*()+=[{]}\|;:',<.>/?_-";



            var containsLowerCase = Contains(password, lowercase);
            var containsUpperCase = Contains(password, uppercase);
            var containsNumber = Contains(password, number);
            //var containsSpecial = Contains(password, special);
            var containsNonSpecial = Contains(password, nonspecial);

            if (password.length > 8) {
                document.getElementById('imgyes1C').style.display = "block";
                document.getElementById('imgno1C').style.display = "none";
            }
            else {
                document.getElementById('imgno1C').style.display = "block";
                document.getElementById('imgyes1C').style.display = "none";
                count = count + 1;
            }

            if (containsLowerCase || containsUpperCase) {
                document.getElementById('imgyes2C').style.display = "block";
                document.getElementById('imgno2C').style.display = "none";
            }
            else {
                document.getElementById('imgno2C').style.display = "block";
                document.getElementById('imgyes2C').style.display = "none";
                count = count + 1;
            }

            if (containsLowerCase) {
                document.getElementById('imgyes3C').style.display = "block";
                document.getElementById('imgno3C').style.display = "none";
            }
            else {
                document.getElementById('imgno3C').style.display = "block";
                document.getElementById('imgyes3C').style.display = "none";
                count = count + 1;
            }

            if (containsUpperCase) {
                document.getElementById('imgyes4C').style.display = "block";
                document.getElementById('imgno4C').style.display = "none";
            }
            else {
                document.getElementById('imgno4C').style.display = "block";
                document.getElementById('imgyes4C').style.display = "none";
                count = count + 1;
            }

            if (containsNumber) {
                document.getElementById('imgyes5C').style.display = "block";
                document.getElementById('imgno5C').style.display = "none";
            }
            else {
                document.getElementById('imgno5C').style.display = "block";
                document.getElementById('imgyes5C').style.display = "none";
                count = count + 1;
            }


            if (containsNonSpecial) {
                document.getElementById('imgyes6C').style.display = "none";
                document.getElementById('imgno6C').style.display = "block";
                count = count + 1;
            }
            else {
                document.getElementById('imgno6C').style.display = "none";
                document.getElementById('imgyes6C').style.display = "block";
            }
        }

        function passvalidate1(obj) {
            show1(obj);

            if (document.getElementById('imgno1C').style.display == "block") {
                //  obj.focus();
                document.getElementById('divshowuserC').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                // alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno2C').style.display == "block") {
                //  obj.focus();
                document.getElementById('divshowuserC').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                //alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno3C').style.display == "block") {
                //  obj.focus();
                document.getElementById('divshowuserC').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                // alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno4C').style.display == "block") {
                // obj.focus();
                document.getElementById('divshowuserC').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                // alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno5C').style.display == "block") {
                //  obj.focus();
                document.getElementById('divshowuserC').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                // alert("Please check the password criteria");
            }
            else if (document.getElementById('imgno6C').style.display == "block") {
                // obj.focus();
                document.getElementById('divshowuserC').style.display = "block";
                document.getElementById('lblalert').innerHTML = "Your password does not meet security criteria – please try again.";
                $find("mpealert").show();
                //alert("Please check the password criteria");
            }
            else {
                document.getElementById('divshowuserC').style.display = "none";
            }
        }

        function showlist1() {
            document.getElementById('divshowuserC').style.display = "block";
        }

        function showlistpass1() {
            document.getElementById('divshowpassC').style.display = "block";
        }

        function checkpassmatch1(obj) {

            validatepass1(obj);
            if (count1 > 0) {

                document.getElementById('divshowpassC').style.display = "";
            }
            document.getElementById('divshowpassC').style.display = "none";
        }

        function validatepass1(obj) {
            var confirmpassword = obj.value;
            var password = document.getElementById('txtpassword1C').value;

            if (confirmpassword == password) {
                document.getElementById('imgyespassC1').style.display = "block";
                document.getElementById('imgnopassC1').style.display = "none";
                count1 = 0;
                // document.getElementById('buttonLogin').removeAttribute("disabled");
            }
            else {
                document.getElementById('imgyespassC1').style.display = "none";
                document.getElementById('imgnopassC1').style.display = "block";
                count1 = count1 + 1;
                // document.getElementById('buttonLogin').setAttribute("disabled", "disabled");
            }
        }

        function CheckPassword1(obj) {
            var password = obj.value;
            var userName = document.getElementById('txtemail').value;

            var lowercase = "abcdefghijklmnopqrstuvwxyz";
            var uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var number = "1234567890";

            // find out if the password different cases
            var containsLowerCase = Contains(password, lowercase);
            var containsUpperCase = Contains(password, uppercase);
            var containsNumber = Contains(password, number);

            if ((containsLowerCase || containsUpperCase) && containsNumber) {

                if (password.length < 8) {
                    if (password != "") {
                        alert("Password length should not be less than 8 digits");
                    }
                    obj.value = "";
                    obj.focus();
                }
                else if (password == userName) {
                    if (password != "") {
                        alert("Password can not be same as username");
                    }
                    obj.value = "";
                    obj.focus();
                }
            }
            else {
                if (password != "") {
                    alert("Password should contain both Alphabets and Number");
                    obj.value = "";
                    obj.focus();
                }
            }
        }

        // checks if a password contains one of a set of characters
        function Contains1(password, validChars) {

            for (i = 0; i < password.length; i++) {

                var char = password.charAt(i);
                if (validChars.indexOf(char) > -1) {
                    return true;
                }
            }
            return false;
        }
    </script>
    <style type="text/css">
        .verhor-center
        {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
</head>
<body class="kt-page--loading-enabled kt-page--loading kt-page--fixed kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"
    style="background-color: #353131;">
    <form id="frmLoginPage" runat="server" autocomplete="off">
    <asp:ScriptManager runat="server" ID="ToolkitScriptManager" ScriptMode="Release"
        EnablePartialRendering="true">
    </asp:ScriptManager>
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a>
                <img alt="Logo" src="../assets/img/whitelogo.png" style="height: 45px;" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar" id="dvMobileMenu" runat="server" visible="false">
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler">
                <span></span>
            </button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler">
                <i class="flaticon-more-1"></i>
            </button>
        </div>
    </div>
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper "
                id="kt_wrapper">
                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
                    <div class="kt-container">
                        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                            <asp:LinkButton OnClick="btncloseLogin" runat="server" ID="lnkNavigate" CssClass="kt-header__brand-logo">
                                <img alt="Logo" src="../assets/img/whitelogo.png" style="height: 50px;" class="kt-header__brand-logo-default" />
                                <img alt="Logo" src="../assets/img/Logo_Tra.gif" style="height: 50px;" class="kt-header__brand-logo-sticky" />
                            </asp:LinkButton>
                        </div>
                        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn">
                            <i class="la la-close"></i>
                        </button>
                        <div class="kt-header__topbar kt-grid__item" style="width: 60%;">
                            <div class="kt-header-menu-wrapper ml-4" id="kt_header_menu_wrapper3" style="text-align: right;">
                                <div id="tdHeader3" runat="server" style="align-self: center; width: 100%;">
                                    <asp:LinkButton ID="btnHome" runat="server" OnClick="btncloseLogin" Text="Home" CssClass="btn btn-danger"> </asp:LinkButton>
                                    <%--<i class="fa fa-home"></i>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end:: Header -->
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch"
                    id="dvScrollBody">
                    <div class="kt-container kt-body kt-grid kt-grid--ver" id="kt_body">
                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                            <div class="kt-content kt-grid__item kt-grid__item--fluid p-3">
                                <!-- begin:: Content -->
                                <div class="row" id="divLogin" runat="server">
                                    <div class="col-lg-15">
                                    </div>
                                    <div class="col-lg-60 verhor-center" style="background-color: #111820; padding: 1rem;">
                                        <div class="row ">
                                            <div class="col-lg-6 pl-0" style="border-right: 1px solid white; padding: 15px; padding-top: 17px;">
                                                <div class="form-group row">
                                                    <div class="col-xl-12">
                                                        <label style="font-weight: bold; color: rgb(255, 255, 255); font-size: 16px; letter-spacing: 0.7px;">
                                                            RETURNING CUSTOMERS</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-xl-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="background-color: rgb(153,153,154); padding: 14px;
                                                                    border: 1px solid #ccc;"><i class="icon-xl far fa-envelope"></i></span>
                                                            </div>
                                                            <input name="email" id="txtLoginEmail" onfocus="$(this).removeAttr('readonly');"
                                                                autocompletetype="Disabled" runat="server" autocomplete="off" style="margin: 0;
                                                                background-color: rgb(153,153,154); color: White; border: 1px solid #ccc; padding: 12px;
                                                                font-family: inherit; font-size: 16px;" class="form-control input-lg" tabindex="1"
                                                                placeholder="Enter Email" required data-parsley-type="email" />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLoginEmail"
                                                            Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="MP">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-xl-12">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" style="background-color: rgb(153,153,154); padding: 14px;
                                                                    border: 1px solid #ccc;"><i class="icon-xl fas fa-lock"></i></span>
                                                            </div>
                                                            <input name="password" id="txtPassword" tabindex="2" runat="server" autocompletetype="Disabled"
                                                                autocomplete="false" style="margin: 0; background-color: rgb(153,153,154); color: White;
                                                                border: 1px solid #ccc; padding: 12px; font-family: inherit; font-size: 16px;"
                                                                type="password" class="form-control input-lg" placeholder="Enter Password" required
                                                                data-parsley-length="[6, 10]" data-parsley-trigger="keyup" />
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPassword"
                                                            Display="None" ErrorMessage="Password is required." SetFocusOnError="True" ValidationGroup="MP">*</asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-xl-12">
                                                        <asp:LinkButton ID="lnkforgot" OnClick="btnforgetPassOnClick" Style="text-decoration: underline;
                                                            color: rgb(255, 255, 255); font-size: 15px; letter-spacing: 0.5px;" UseSubmitBehavior="false"
                                                            runat="server" Text="Forgot Password?" />
                                                    </div>
                                                </div>
                                                <div class="form-group row ">
                                                    <div class="col-xl-12 text-center">
                                                        <asp:Button ID="btnLogin" TabIndex="3" class="btn btn-danger" OnClick="logged_Click"
                                                            Width="99%" Style="padding: 0.9rem;" UseSubmitBehavior="false" runat="server"
                                                            Text="SIGN IN" />
                                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="MP" ShowMessageBox="true"
                                                            runat="server" ShowSummary="false" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 pr-0" style="padding: 15px; padding-top: 17px;">
                                                <div class="form-group row">
                                                    <div class="col-xl-12">
                                                        <label style="font-weight: bold; color: rgb(255, 255, 255); font-size: 16px; letter-spacing: 0.7px;">
                                                            NEW CUSTOMERS</label>
                                                        <asp:Button ID="btnLoginClose" Text="Close" runat="server" OnClick="btncloseLogin"
                                                            Style="float: right; display: none" CausesValidation="false" UseSubmitBehavior="false"
                                                            CssClass="btn btn-danger" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-xl-12 text-center">
                                                        <br />
                                                        <asp:Button ID="btnCheckout" class="btn btn-danger" Style="padding: 0.9rem;" Width="80%"
                                                            UseSubmitBehavior="false" runat="server" Text="CHECKOUT AS A GUEST" OnClick="CheckGuest" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-xl-12 text-center">
                                                        <h6 style="color: rgb(255, 255, 255); font-size: 14px; margin: 26px 0px; letter-spacing: 0.5px;">
                                                            If you don’t have a Zoom account, you can create one now.
                                                        </h6>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-xl-12 text-center">
                                                        <asp:Button ID="btnregister" Width="80%" Style="padding: 0.9rem;" class="btn btn-danger"
                                                            OnClick="ShowCreateUserPopup" UseSubmitBehavior="false" runat="server" Text="SIGNUP NOW!" />
                                                        <%-- <asp:Label ID="lblforgot" runat="server" Visible="false" Style="color: rgb(255,255,255);"></asp:Label>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-15">
                                    </div>
                                </div>
                                <div class="row" id="divSIgnUp" runat="server" visible="false">
                                    <div class="col-lg-15">
                                    </div>
                                    <div class="col-lg-60" style="background: #111820; padding: 0.7rem;">
                                        <table width="100%">
                                            <tr>
                                                <td style="vertical-align: middle; padding-top; 10px">
                                                    <h4 class="modal-title" style="float: left; padding-left: 6%; padding-top: 10px;
                                                        letter-spacing: 0.5px; font-weight: bold; color: White">
                                                        <span class="icon-xl fas fa-lock" style="text-align: left"></span>&nbsp; Signup
                                                        Now!</h4>
                                                </td>
                                                <td style="height: 30px; margin-right: 2%; padding-right: 5%; color: White; font-weight: bold;
                                                    padding-top: 20px; font-size: larger; vertical-align: bottom" align="right" colspan="5">
                                                    <asp:Button ID="btnSignupClose" Text="Close" runat="server" OnClick="lnkSignupClose_Click"
                                                        Style="float: right;" CausesValidation="false" UseSubmitBehavior="false" CssClass="btn btn-danger" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 15px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 4%; padding-right: 6%; padding-bottom: 20px" colspan="2">
                                                    <div id="Signin" role="form" style="align: center">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 18%; padding-right: 2%" nowrap="nowrap">
                                                                            <span style="color: rgb(17,24,32)">*</span>
                                                                            <asp:DropDownList ID="ddlTitle" class="cusinputuserTextbox" Width="100%" runat="server">
                                                                                <asp:ListItem>Mr</asp:ListItem>
                                                                                <asp:ListItem>Mrs</asp:ListItem>
                                                                                <asp:ListItem>Miss</asp:ListItem>
                                                                                <asp:ListItem>Sir</asp:ListItem>
                                                                                <asp:ListItem>Dr</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 41%; padding-right: 2%" nowrap="nowrap">
                                                                            <span style="color: Red">*</span>
                                                                            <input class="cusinputuserTextbox" width="48%" runat="server" id="txtFirstName" type="text"
                                                                                maxlength="50" onkeypress="return Alphabets(event)" placeholder="First Name" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                                                                                Display="None" ErrorMessage="First Name is required." SetFocusOnError="True"
                                                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td style="width: 41%" nowrap="nowrap">
                                                                            <span style="color: Red">*</span>
                                                                            <input class="cusinputuserTextbox" width="48%" runat="server" id="txtLastName" type="text"
                                                                                maxlength="50" onkeypress="return Alphabets(event)" placeholder="Last Name" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                                                                                Display="None" ErrorMessage="Last Name is required." SetFocusOnError="True" ValidationGroup="SP" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group" style="width: 100%">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 50%; padding-left: 1%" nowrap="nowrap">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td width="20%">
                                                                                        <input class="cusinputuserTextbox" placeholder="code" width="100%" runat="server"
                                                                                            id="txtPhoneCode" maxlength="4" tabindex="5" onkeypress="return Numericswithspecial(event);" />
                                                                                    </td>
                                                                                    <td width="1%">
                                                                                    </td>
                                                                                    <td>
                                                                                        <input class="cusinputuserTextbox" width="100%" runat="server" id="txtphone" placeholder="Phone#"
                                                                                            maxlength="15" onkeypress="return Numerics(event)" />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtphone"
                                                                                            Display="None" ErrorMessage="Phone # is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td style="width: 50%;" nowrap="nowrap">
                                                                            <span style="color: Red">*</span>
                                                                            <input class="cusinputuserTextbox" width="98%" runat="server" id="txtemail" placeholder="E-mail"
                                                                                maxlength="200" autocomplete="off" onchange="ValidateEmail(this)" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtemail"
                                                                                Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 50%; padding-right: 2%;" nowrap="nowrap">
                                                                            <span style="color: red">*</span>
                                                                            <input class="cusinputuserTextbox" runat="server" id="txtpassword1C" autocomplete="off"
                                                                                maxlength="20" type="password" placeholder="Password" onfocus="return showlist1()"
                                                                                onblur="return passvalidate1(this)" onkeypress="return show1(this)" onkeyup="return show1(this)" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtpassword1C"
                                                                                Display="None" ErrorMessage="New Password is required." SetFocusOnError="True"
                                                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td style="width: 50%" nowrap="nowrap">
                                                                            <span style="color: red">*</span>
                                                                            <input class="cusinputuserTextbox" runat="server" id="txtpassword2C" maxlength="20"
                                                                                type="password" onfocus="return showlistpass1()" onblur="checkpassmatch1(this)"
                                                                                onkeypress="validatepass1(this)" onkeyup="validatepass1(this)" placeholder="Confirm Password" />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtpassword2C"
                                                                                Display="None" ErrorMessage="Confirm Password is required." SetFocusOnError="True"
                                                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                                                            <div id="divshowpassC" runat="server" style="width: 250px; left: 55%; top: 38%; z-index: 9999999;
                                                                                position: fixed; display: none;" class="hint">
                                                                                <div class="hint-pointer">
                                                                                </div>
                                                                                <div>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="font-weight: bold; text-align: center">
                                                                                                Password criteria:
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Image ID="imgyespassC1" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                    ImageUrl="~/Components/images/tick_yes.png" />
                                                                                                <asp:Image ID="imgnopassC1" CssClass="hint-img" runat="server" Style="display: block"
                                                                                                    ImageUrl="~/Components/images/tick_no.png" />
                                                                                            </td>
                                                                                            <td>
                                                                                                Password Matches
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                            <div id="divshowuserC" runat="server" style="width: 240px; left: 35%; top: 38%; z-index: 9999999;
                                                                                position: fixed; display: none;" class="hint">
                                                                                <div class="hint-pointer">
                                                                                </div>
                                                                                <div>
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td style="font-weight: bold; text-align: center">
                                                                                                    Password criteria:
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Image ID="imgyes1C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_yes.png" />
                                                                                                    <asp:Image ID="imgno1C" CssClass="hint-img" runat="server" Style="display: block"
                                                                                                        ImageUrl="~/Components/images/tick_no.png" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    At least 8 characters long
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Image ID="imgyes2C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_yes.png" />
                                                                                                    <asp:Image ID="imgno2C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_no.png" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    Start with a letter
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Image ID="imgyes3C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_yes.png" />
                                                                                                    <asp:Image ID="imgno3C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_no.png" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    Include a lower case letter
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Image ID="imgyes4C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_yes.png" />
                                                                                                    <asp:Image ID="imgno4C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_no.png" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    Include an upper case letter
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Image ID="imgyes5C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_yes.png" />
                                                                                                    <asp:Image ID="imgno5C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_no.png" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    Include a number
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:Image ID="imgyes6C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_yes.png" />
                                                                                                    <asp:Image ID="imgno6C" CssClass="hint-img" runat="server" Style="display: none"
                                                                                                        ImageUrl="~/Components/images/tick_no.png" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    No special characters
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group" style="width: 100%">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 100%;" nowrap="nowrap">
                                                                            <span style="color: rgb(17,24,32)">*</span>
                                                                            <input class="cusinputuserTextbox" width="100%" runat="server" id="txtAddress" placeholder="Address1"
                                                                                maxlength="200">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group" style="width: 100%">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 100%;" nowrap="nowrap">
                                                                            <span style="color: rgb(17,24,32)">*</span>
                                                                            <input class="cusinputuserTextbox" width="100%" runat="server" id="txtAddress2" placeholder="Address2"
                                                                                maxlength="200">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 50%; padding-right: 2%;" nowrap="nowrap">
                                                                            <span style="color: rgb(17,24,32)">*</span>
                                                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtCity" placeholder="City"
                                                                                maxlength="200">
                                                                        </td>
                                                                        <td style="width: 50%" nowrap="nowrap">
                                                                            <span style="color: rgb(17,24,32)">*</span>
                                                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtstate" placeholder="State"
                                                                                maxlength="200">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 50%; padding-right: 2%;" nowrap="nowrap">
                                                                            <span style="color: rgb(17,24,32)">*</span>
                                                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtcountry" placeholder="Country"
                                                                                maxlength="200">
                                                                        </td>
                                                                        <td style="width: 50%" nowrap="nowrap">
                                                                            <span style="color: rgb(17,24,32)">*</span>
                                                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtZipcode" placeholder="Zip Code"
                                                                                maxlength="6">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <center>
                                                            <asp:Button ID="Button2" class="btn btn-danger" OnClick="btnCreateLoginOnClick" UseSubmitBehavior="false"
                                                                runat="server" Width="50%" Text="CREATE AN ACCOUNT" />
                                                            <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                                                runat="server" ShowSummary="false" />
                                                            <input type='hidden' id='hdnpassword1' runat="server" name='hdnpassword1' />
                                                            <input type='hidden' id='hdnpassword2' runat="server" name='hdnpassword2' />
                                                            <asp:HiddenField ID="hdnpassword3" runat="server" />
                                                        </center>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-lg-15">
                                    </div>
                                </div>
                                <div class="verhor-center">
                                    <div class="row" id="div" runat="server" visible="false" style="padding: 10px; background: #111820;">
                                    </div>
                                </div>
                                <!-- end:: Content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        border: 2px solid red; min-width: 25%; padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr>
                <td style="color: rgb(255,255,255); font-size: 13.5px; letter-spacing: 0.5px;" valign="middle"
                    align="left">
                    <asp:Label ID="lblalert" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="Button3" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpethank" runat="server" TargetControlID="Button3" PopupControlID="pnlthank"
        BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlthank" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        border: 2px solid red; padding: 10px">
        <table width="100%" style="width: 100%;" cellpadding="2" cellspacing="2">
            <tr>
                <td style="color: rgb(255,255,255); font-weight: 400; line-height: 30px; font-size: 15px;
                    vertical-align: middle" align="center">
                    User details have been saved successfully.
                    <br />
                    Token has been sent to your registered email address.
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lnkthank" runat="server" CssClass="btn btn-danger" Text="Ok"
                        OnClick="btnOkThank_onclick" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="Button4" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeToken" runat="server" TargetControlID="Button4" PopupControlID="pnlToken"
        BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlToken" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        width: 33%; border: 2px solid red; padding: 15px">
        <div class="col-12">
            <div class="form-group row">
                <div class="col-xl-12">
                    <label style="font-weight: bold; color: rgb(255, 255, 255); font-size: 16px; width: 100%;
                        letter-spacing: 0.7px; text-align: left">
                        JET EDGE ZOOM
                        <asp:Button ID="imgcancelotp" runat="server" OnClick="btnTokClose" Style="float: right;"
                            CssClass="btn btn-danger" Text="Close" />
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-12">
                    <div class="input-group">
                        <div class="input-group-prepend" style="width: 100%;">
                            <span class="input-group-text" style="background-color: rgb(153,153,154); padding: 14px;
                                border: 1px solid #ccc;"><i class="fa fa-key" aria-hidden="true"></i></span>
                            <input name="email" id="txtotp" runat="server" style="margin: 0; background-color: rgb(153,153,154);
                                color: rgb(255,255,255); border: 1px solid #ccc; padding: 12px; font-family: inherit;
                                font-size: 17px; width: 100%;" class="form-control input-lg" tabindex="1" placeholder="Enter Token"
                                required data-parsley-type="email">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtotp"
                                Display="None" ErrorMessage="token is required." SetFocusOnError="True" ValidationGroup="OG">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-12">
                    <asp:LinkButton ID="btnresendotp" OnClick="btnresendotp_Click" Style="text-decoration: underline;
                        float: right; width: 100%; text-align: right; font-size: 13.5px; color: rgb(255,255,255);"
                        UseSubmitBehavior="false" runat="server" Text="Resend Token" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-12 center">
                    <asp:Button ID="btnOTP" TabIndex="3" class="btn btn-danger" Width="100%" OnClick="btnOTP_click"
                        UseSubmitBehavior="false" runat="server" Text="SIGN IN" />
                    <asp:ValidationSummary ID="ValidationSummary6" ValidationGroup="OG" ShowMessageBox="true"
                        runat="server" ShowSummary="false" />
                </div>
            </div>
            <div class="form-group row" id="alertOTp" runat="server" visible="false">
                <div class="col-xl-12">
                    <div class="input-group">
                        <asp:Label ID="lblOTPalert" Style="color: rgb(255,255,255); font-weight: bold" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="Button5" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeChangePassword" runat="server" TargetControlID="Button5"
        PopupControlID="pnlChangePassword" BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlChangePassword" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); border: 2px solid red; padding: 15px">
        <div class="row">
            <div class="col-lg-12" style="text-align: center; padding-left: 10%; padding-right: 10%;">
                <div class="form-group row">
                    <div class="col-xl-12">
                        <label style="color: rgb(255, 255, 255); font-weight: 600; letter-spacing: 0.7px;
                            font-size: 1.5rem;">
                            Change Password
                        </label>
                    </div>
                </div>
                <div class="form-group row" style="text-align: left; display: none">
                    <div class="col-xl-12">
                        <div class="cusfielduser-cuscontaineruser">
                            <label class="cuslabeluser " for="txtOldPassword" style="text-align: left; color: rgb(255,255,255);
                                padding-bottom: 10px; padding-top: 10px; font-size: 1.15vw; font-family: Gotham;
                                color: rgb(17,24,32); font-weight: 400;">
                                Old Password <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" style="width: 100%" runat="server" id="txtOldPassword"
                                maxlength="20" type="password">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtOldPassword"
                                Display="None" ErrorMessage="Old Password is required." SetFocusOnError="True"
                                ValidationGroup="TP">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12">
                        <div class="cusfielduser-cuscontaineruser" style="text-align: left">
                            <label for="txtNewPassword" style="font-size: 1.2rem; color: white; letter-spacing: 0.5px;">
                                New Password <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" runat="server" tabindex="1" id="txtNewPassword"
                                maxlength="20" type="password" onfocus="return showlist()" onblur="return passvalidate(this)"
                                onkeypress="return show(this)" onkeyup="return show(this)">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtNewPassword"
                                Display="None" ErrorMessage="New Password is required." SetFocusOnError="True"
                                ValidationGroup="TP">*</asp:RequiredFieldValidator>
                            <asp:TextBox runat="server" ID="txtUserName" Style="display: none"></asp:TextBox>
                        </div>
                        <div id="divShow" runat="server" style="z-index: 9999999; position: fixed; color: rgb(17,24,32);
                            display: none;" class="hint">
                            <%-- <div class="hint-pointer">
                            </div>--%>
                            <div>
                                <div>
                                    <table>
                                        <tr>
                                            <td style="font-weight: bold; text-align: center">
                                                Password criteria:
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgyes1" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                <asp:Image ID="imgno1" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                            </td>
                                            <td>
                                                At least 8 characters long
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgyes2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                <asp:Image ID="imgno2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            </td>
                                            <td>
                                                Start with a letter
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgyes3" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                <asp:Image ID="imgno3" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            </td>
                                            <td>
                                                Include a lower case letter
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgyes4" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                <asp:Image ID="imgno4" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            </td>
                                            <td>
                                                Include an upper case letter
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgyes5" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                <asp:Image ID="imgno5" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            </td>
                                            <td>
                                                Include a number
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Image ID="imgyes7" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                <asp:Image ID="imgno7" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                            </td>
                                            <td>
                                                No special characters
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12">
                        <div class="cusfielduser-cuscontaineruser" style="text-align: left">
                            <label for="txtConformPass" style="font-size: 1.2rem; color: white; letter-spacing: 0.5px;">
                                Confirm Password <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" runat="server" id="txtConformPass" tabindex="2"
                                maxlength="20" type="password" onfocus="return showlistpass()" onblur="checkpassmatch(this)"
                                onkeypress="validatepass(this)" onkeyup="validatepass(this)">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNewPassword"
                                Display="None" ErrorMessage="Confirm Password is required." SetFocusOnError="True"
                                ValidationGroup="TP">*</asp:RequiredFieldValidator>
                        </div>
                        <div id="divshowpassmatch" runat="server" style="color: rgb(17,24,32); z-index: 9999999;
                            position: fixed; display: none;" class="hint">
                            <%-- <div class="hint-pointer">
                            </div>--%>
                            <div>
                                <table>
                                    <tr>
                                        <td style="font-weight: bold; text-align: center">
                                            Password criteria:
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="Imgpassyes" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="Imgpassno" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                        </td>
                                        <td>
                                            Password Matches
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12">
                        <asp:LinkButton ID="LinkButton4" runat="server" OnClick="btnSubmit_Click" Text="Submit"
                            CssClass="btn btn-danger" TabIndex="3" />
                        &nbsp;
                        <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-danger" TabIndex="4"
                            Text="Close" OnClick="btnClose_Click" />
                        <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="TP" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                        <asp:Label ID="lblMessage" runat="server" Text="Last four passwords cannot be used for security reasons"
                            Visible="false" CssClass="mandatory"></asp:Label>
                        <input type='hidden' id='txthidden' runat="server" name='txthidden' />
                        <input type='hidden' id='txthidden1' runat="server" name='txthidden1' />
                        <asp:HiddenField ID="txtTelephony" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12">
                        <div align="center" style="font-size: 15px">
                            <div id="div1" runat="server" class="content" visible="false">
                                <p>
                                    Your password has been changed successfully. Please use your new password to <a href="LandingPage.aspx"
                                        target="_parent" class="highlight">Login here</a>.</p>
                            </div>
                        </div>
                        <div align="center">
                            <div id="divEnd" runat="server" class="content" visible="false">
                                <p style="font-size: 18px">
                                    Your password has been changed successfully. Please use your new password to <a href="LandingPage.aspx"
                                        target="_parent" class="highlight" style="font-size: 22px">Login here</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="Button6" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeThankForgot" runat="server" TargetControlID="Button6"
        PopupControlID="pnlChangePassword" BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlThankForgot" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); border: 2px solid red; padding: 10px">
        <table width="50%" align="center" style="width: 50%; height: 100%; padding-left: 0px;
            border-right: 0px; padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr style="margin-left: 10%; border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-weight: 400; font-size: 15px; letter-spacing: 0.5px;"
                    align="center" valign="middle">
                    A Token has been sent to your registered email address to reset your password.
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lnkthankForget" OnClick="btnOkThankForget_onclick" runat="server"
                        CssClass="btn btn-danger" Text="Ok" />
                </td>
            </tr>
            <tr>
                <td style="height: 10px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="Button7" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeOTPForgot" runat="server" TargetControlID="Button7"
        PopupControlID="pnlOTPForgot" BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlOTPForgot" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); border: 2px solid red; padding: 10px">
        <div class="row">
            <div class="col-12" style="text-align: center; padding-left: 18%; padding-right: 18%;
                margin-top: 4%;">
                <div class="form-group row">
                    <div class="col-xl-12">
                        <label style="font-weight: bold; color: rgb(255, 255, 255); font-size: 16px; width: 100%;
                            letter-spacing: 0.7px; text-align: left">
                            JET EDGE ZOOM
                            <asp:Button ID="imgcancelotpForget" runat="server" Style="float: right;" CssClass="btn btn-danger"
                                Text="Close" OnClick="imgcancelotpForget_Click" />
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12">
                        <div class="input-group">
                            <div class="input-group-prepend" style="width: 100%;">
                                <span class="input-group-text" style="background-color: rgb(153,153,154); padding: 14px;
                                    border: 1px solid #ccc;"><i class="fa fa-key" aria-hidden="true"></i></span>
                                <input name="email" id="txtotpForget" runat="server" style="margin: 0; background-color: rgb(153,153,154);
                                    color: rgb(255,255,255); border: 1px solid #ccc; padding: 12px; font-family: inherit;
                                    font-size: 17px; width: 100%;" class="form-control input-lg" tabindex="1" placeholder="Enter Token"
                                    required data-parsley-type="email" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtotpForget"
                                    Display="None" ErrorMessage="token is required." SetFocusOnError="True" ValidationGroup="OGF">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12 text-right">
                        <asp:LinkButton ID="btnresendotpForget" OnClick="btnresendotpForget_Click" Style="text-decoration: underline;
                            color: rgb(255,255,255); font-size: 14px;" UseSubmitBehavior="false" runat="server"
                            Text="Resend Token" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12 center">
                        <asp:Button ID="btnOTPForget" TabIndex="3" class="btn btn-danger" Width="100%" OnClick="btnOTP_clickForget"
                            UseSubmitBehavior="false" runat="server" Text="SIGN IN" ValidationGroup="OG" />
                        <asp:ValidationSummary ID="ValidationSummary7" ValidationGroup="OGF" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                    </div>
                </div>
                <div class="form-group row" id="alertOTpForget" runat="server" visible="false">
                    <div class="col-xl-12">
                        <div class="input-group">
                            <asp:Label ID="lblOTPalertForget" Style="color: rgb(255,255,255); font-weight: bold"
                                runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="Button1" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpecookie" runat="server" TargetControlID="Button1" PopupControlID="pnlcookie"
        BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlcookie" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); border: 2px solid red; padding: 10px">
        <table width="100%" id="tblcookie" runat="server" visible="true">
            <tr>
                <td style="font-size: 18px; color: rgb(255,255,255); font-weight: bold; letter-spacing: 0.5px;">
                    Cookies
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="font-size: 13px; letter-spacing: 0.5px; padding-left: 20px; vertical-align: middle;
                    color: rgb(255,255,255)" nowrap="nowrap" align="left">
                    This site uses cookies :
                    <asp:LinkButton ID="LinkButton10" Style="text-decoration: underline; color: White;
                        font-size: 15px; font-weight: bold" runat="server" Text="Find out more" OnClick="lnkcookesshow_click" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center" nowrap="nowrap">
                    <asp:LinkButton ID="lnkReject" Style="display: none;" runat="server" CssClass="btn btn-danger"
                        Text="Reject" OnClick="lnkReject_Onclick" />
                    <asp:LinkButton ID="lnkAllow" OnClick="lnkAllow_Onclick" runat="server" CssClass="btn btn-danger"
                        Text="Accept" />
                </td>
            </tr>
        </table>
        <table width="100%" id="mpefrmaecookie" runat="server" visible="false">
            <tr>
                <td style="vertical-align: middle; text-align: left;">
                    <span style="font-size: 18px; font-weight: bold; color: rgb(255,255,255)">Cookies
                    </span>
                    <asp:LinkButton ID="ImageButton2" OnClick="ImageButton2_Click" runat="server" CssClass="btn btn-danger"
                        Style="color: rgb(255,255,255); float: right; text-decoration: none" Text="Close" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px;">
                </td>
            </tr>
            <tr>
                <td valign="middle" align="left" style="line-height: 25px;">
                    <label style="color: white; font-size: 14px; letter-spacing: 0.5px;">
                        Jet Edge uses cookies and similar technologies on this site and around the web that
                        collect and use personal data (e.g. your IP address) in order to select and deliver
                        measurable personalized from this site.
                    </label>
                    <br />
                    <label style="color: white; font-size: 14px; letter-spacing: 0.5px;">
                        By clicking "Accept", you consent to the placement and use of cookies and similar
                        technologies.
                    </label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblFromFlag" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="loggedflag" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="BookedFlag" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="DBMailFlag" runat="server" Visible="false"></asp:Label>
    <script type="text/javascript">
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#366cf3",
                    "light": "#ffffff",
                    "dark": "#282a3c",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>
    <script src="../assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
    <script src="../assets/demo/demo4/base/scripts.bundle.js" type="text/javascript"></script>
    <script src="../assets/app/bundle/app.bundle.js" type="text/javascript"></script>
    <script type="text/javascript" src="../assets/js/Common.js"></script>
    </form>
</body>
</html>
