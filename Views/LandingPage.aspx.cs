﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Views_LandingPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            try
            {
                pnlTerms.Attributes.Add("onload", "AutoHeightPopup(this)");
                iframeterms.Attributes.Add("onload", "AutoHeightPopup(this)");
                if (Request.Browser.IsMobileDevice)
                {
                    string path5 = Request.Url.PathAndQuery;
                    if (!path5.Contains("userflag=ADMINUSER"))
                    {
                        Response.Redirect("SimpleSearchBookingM.aspx", true);
                    }
                    else
                    {
                        string url = Request.Url.ToString().Replace("simplesearchbooking.aspx", "simplesearchbookingM.aspx");
                        Response.Redirect(url, true);
                    }
                }

                BindEmptyLegs(1);
                string path4 = Request.Url.PathAndQuery;

                if (!path4.Contains("userflag=ADMINUSER"))
                {
                }
                else
                {
                    gvSimpleSearch_SelectedIndexChanged(gvSimpleSearch, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
            }

        }
    }
    protected void gvSimpleSearch_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvSimpleSearch_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label Label2 = e.Row.FindControl("lblscity") as Label;
                Label Label3 = e.Row.FindControl("lblEcity") as Label;
                Label lblstairport = e.Row.FindControl("lblstairport") as Label;
                Label lblendairport = e.Row.FindControl("lblendairport") as Label;

                Label lblmodflag = e.Row.FindControl("lblmodflag") as Label;
                Label lbltosplit = e.Row.FindControl("lbltosplit") as Label;
                Label lbltodate = e.Row.FindControl("lbltodate") as Label;

                if (lblmodflag.Text == "MF")
                {
                    lbltosplit.Visible = true;
                    lbltodate.Visible = true;
                }
                else
                {
                    lbltosplit.Visible = false;
                    lbltodate.Visible = false;
                }

                if ((Label3.Text == "Hawaii" || Label3.Text == "Alaska" || Label3.Text == "Puerto Rico"))
                {
                    Label3.ForeColor = System.Drawing.Color.Blue;
                    lblendairport.ForeColor = System.Drawing.Color.Blue;
                    lblendairport.Visible = false;
                }

                if (Label2.Text.Length == 0)
                {
                    lblstairport.Text = lblstairport.Text.Replace("(", "").Replace(")", "").TrimStart();
                }
                if (Label3.Text.Length == 0)
                {
                    lblendairport.Text = lblendairport.Text.Replace("(", "").Replace(")", "").TrimStart();
                }

            }

        }

        catch (Exception ex)
        {
            // lblMsg.Text = ex.Message;
        }

    }
    public void BindEmptyLegs(int pageindex)
    {
        #region BindEmptyLegsinlist

        string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
        string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytrips]", false);

        saveCategoryParam[0].Value = pageindex;//MANAGETYPE
        saveCategoryParam[1].Value = "25";//Rowid
        saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
        saveCategoryParam[3].Value = strEndDate;//Rowid

        DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytrips]", saveCategoryParam).Tables[0];
        DataTable dtAdvSearch = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM Airportmaster  where  ShowinEmptyleg=1").Tables[0];
        if (dtOneWayTrip.Rows.Count > 0)
        {
            for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
            {
                for (int j = 0; j < dtAdvSearch.Rows.Count; j++)
                {
                    if (dtOneWayTrip.Rows[i]["End"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                    {
                        dtOneWayTrip.Rows[i]["End"] = dtAdvSearch.Rows[j]["Country"].ToString();
                        dtOneWayTrip.Rows[i]["EndCityShow"] = dtAdvSearch.Rows[j]["Country"].ToString();
                    }
                    if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtOneWayTrip.Rows[i]["End"].ToString())
                    {
                        string strk = dtOneWayTrip.Rows[i]["routes"].ToString().Replace("-->", @"@");
                        String[] strlist = strk.Split(new[] { '@' });
                        dtOneWayTrip.Rows[i]["Start"] = strlist[0];
                        dtOneWayTrip.Rows[i]["End"] = strlist[1];
                    }
                }
            }
        }



        DataView dv = dtOneWayTrip.DefaultView;
        DataTable sortedDT = dv.ToTable();
        sortedDT.Columns.Add("SNo", typeof(int));
        for (int count = 0; count < sortedDT.Rows.Count; count++)
        {
            sortedDT.Rows[count]["SNo"] = count + 1;
        }
        for (int i = 0; i < sortedDT.Rows.Count; i++)
        {
            sortedDT.Rows[i]["SNo"] = Convert.ToInt32(sortedDT.Rows[i]["SNo"].ToString()) - 1;
        }

        string path3 = Request.Url.PathAndQuery;
        if (!path3.Contains("userflag=ADMINUSER"))
        {

            gvSimpleSearch.DataSource = sortedDT;
            gvSimpleSearch.DataBind();
        }
        else
        {

            gvSimpleSearch.DataSource = sortedDT;
            gvSimpleSearch.DataBind();

            string routes = Request.QueryString["startairport"].ToString() + "-->" + Request.QueryString["endairport"].ToString();
            DataRow[] result1 = sortedDT.Select("Dateformat='" + Convert.ToDateTime(Request.QueryString["flydate"].ToString()) + "' and routes='" + routes + "'  and Tailno='" + Request.QueryString["tailno"].ToString() + "'");
            if (result1.Length > 0)
            {
                DataTable dt1 = result1.CopyToDataTable();
                if (dt1.Rows.Count > 0)
                {
                    lblindex.Text = dt1.Rows[0]["Sno"].ToString();
                }
                else
                {
                    Response.Redirect("SimpleSearchBooking.aspx?Flag=L");
                }

            }
            else
            {
                Response.Redirect("SimpleSearchBooking.aspx?Flag=L");
            }

        }


        #endregion
    }

    protected void OpenLogin_Click(object sender, EventArgs e)
    {
        try
        {
            mpeterms.Show();
            iframeterms.Attributes.Add("src", "Login.aspx");
        }
        catch (Exception ex)
        {
            // lblMsg.Text = ex.Message;
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            mpeterms.Show();
            iframeterms.Attributes.Add("src", "Login.aspx");
        }
        catch (Exception ex)
        {
            // lblMsg.Text = ex.Message;
        }
    }
}