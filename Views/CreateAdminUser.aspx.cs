﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class General_Setup_CreateAdminUser : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.CreateAdminBLL objMember = new BusinessLayer.CreateAdminBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Create Admin User");
            }


            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Create Admin User";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;

            Bind_Country_Dropdown();
            Bind_PhoneCode_Dropdown("0");

            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    #region Button Events

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        string autoGenPwd = string.Empty;
        autoGenPwd = genclass.RandomString(genclass.RandomNumber(8, 15));

        if (btnSave.Text == "Save")
        {
            objMember.ManageType = "I";

            #region Email Address Exists

            DataTable dt = objMember.AlreadyExist(txtloginID.Text.Trim());
            if (dt.Rows.Count > 0)
            {
                txtloginID.Focus();
                lblalert.Text = txtloginID.Text + " already exists";
                mpealert.Show();
                return;
            }

            #endregion
        }
        else
        {
            objMember.ManageType = "U";
        }

        objMember.RowID = hdfPSRID.Value;
        objMember.Title = ddlgender.SelectedItem.Text.Trim();
        objMember.firstName = txtFname.Text.Trim();
        objMember.lastName = txtLastName.Text.Trim();
        objMember.Email = txtloginID.Text.Trim();

        string strPCode = GetPhoneCode(ddlCountry.SelectedValue.Trim());

        objMember.PhoneCode = strPCode.Trim();
        objMember.Phone = txtPhone.Value.Trim().Replace(".", "");
        objMember.Address1 = txtAddress.Text.Trim();
        objMember.Address2 = txtAddress2.Text.Trim();
        objMember.City = txtCity.Text.Trim();
        objMember.Country = ddlCountry.SelectedItem.Text;
        objMember.State = txtState.Text.Trim();
        objMember.ZipCode = txtZip.Text.Trim();
        objMember.Password = (genclass.Encrypt(autoGenPwd));
        objMember.UserType = "Admin";
        if (chkActive.Checked == true)
        {
            objMember.Active = "1";
        }
        else
        {
            objMember.Active = "0";
        }
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

        objMember.CountryId = ddlCountry.SelectedValue.Trim();

        string strBooking = "0";
        string strException = "0";
        string strSavedRoutes = "0";
        string strMustMoves = "0";
        string strSignUp = "0";

        for (int i = 0; i < rblEmailConfig.Items.Count; i++)
        {
            if (rblEmailConfig.Items[i].Selected == true)
            {
                if (rblEmailConfig.Items[i].Value == "BC")
                {
                    strBooking = "1";
                }
                if (rblEmailConfig.Items[i].Value == "EXC")
                {
                    strException = "1";
                }
                if (rblEmailConfig.Items[i].Value == "SR")
                {
                    strSavedRoutes = "1";
                }
                if (rblEmailConfig.Items[i].Value == "MM")
                {
                    strMustMoves = "1";
                }
                if (rblEmailConfig.Items[i].Value == "SU")
                {
                    strSignUp = "1";
                }
            }
        }

        objMember.BookingConfirmation = strBooking;
        objMember.Exception = strException;
        objMember.SavedRoutes = strSavedRoutes;
        objMember.MustMove = strMustMoves;
        objMember.signUp = strSignUp;

        string retVal = objMember.AddnewMember(objMember);
        if (retVal != "0" && retVal != "-1")
        {
            if (btnSave.Text == "Save")
            {
                lblalert.Text = "Admin User Details has been saved successfully";

                SendEmail(txtloginID.Text, autoGenPwd, txtFname.Text + " " + txtLastName.Text);
            }
            else
            {
                lblalert.Text = "Admin User Details has been updated successfully";
            }

            mpealert.Show();
            btnAdd.Focus();
            btnViewOnClick(sender, e);
            Clear();
        }
        else
        {
            lblMessage.Value = "An error has occured while processing your request";
        }
    }

    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            ddlgender.Focus();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            lblMan.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();

            if (gvUser.Rows.Count > 0)
            {
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
                lblMan.Visible = false;
            }
            else
            {
                tblForm.Visible = true;
                tblGrid.Visible = false;
                btnAdd.Visible = false;
                btnSave.Visible = true;
                btnView.Visible = true;
                lblMan.Visible = true;
            }

            btnAdd.Focus();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            objMember.DeletUser(lblrowtodelete.Text.Trim());
            List();
            lblalert.Text = "User Details have been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvPricingDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvUser.SelectedRow.FindControl("lblrowid");

            DataTable dtPS = objMember.Edit(lblrowid.Text.Trim());
            if (dtPS != null && dtPS.Rows.Count > 0)
            {
                ddlgender.Focus();
                hdfPSRID.Value = lblrowid.Text;
                ddlgender.SelectedIndex = ddlgender.Items.IndexOf(ddlgender.Items.FindByValue(dtPS.Rows[0]["title"].ToString()));
                txtFname.Text = dtPS.Rows[0]["FirstName"].ToString();
                txtLastName.Text = dtPS.Rows[0]["LastName"].ToString();
                txtloginID.Text = dtPS.Rows[0]["Email"].ToString();
                txtPhone.Value = genclass.Format_Phone(dtPS.Rows[0]["PhoneNumber"].ToString());
                txtAddress2.Text = dtPS.Rows[0]["Address2"].ToString();
                txtAddress.Text = dtPS.Rows[0]["Address"].ToString();
                txtCity.Text = dtPS.Rows[0]["City"].ToString();
                txtState.Text = dtPS.Rows[0]["State"].ToString();
                txtZip.Text = dtPS.Rows[0]["ZipCode"].ToString();

                ddlPhoneCode.SelectedIndex = ddlPhoneCode.Items.IndexOf(ddlPhoneCode.Items.FindByValue(dtPS.Rows[0]["Code_Phone"].ToString()));
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(dtPS.Rows[0]["CountryId"].ToString()));

                if (dtPS.Rows[0]["Active"].ToString() == "1")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }

                string strCreatedBy = dtPS.Rows[0]["CreatedBy"].ToString();
                string trCreatedOn = string.Empty;
                if (dtPS.Rows[0]["USR_Createdon"].ToString().Length > 0)
                {
                    trCreatedOn = genclass.Long_Date_Time(Convert.ToDateTime(dtPS.Rows[0]["USR_Createdon"].ToString()));
                }

                if (strCreatedBy != "" && trCreatedOn != "")
                {
                    lblLMby.Text = strCreatedBy + " ( on  ) " + trCreatedOn;
                    divLMBy.Visible = true;
                }
                else
                {
                    divLMBy.Visible = false;
                }

                for (int i = 0; i < rblEmailConfig.Items.Count; i++)
                {
                    rblEmailConfig.Items[i].Selected = false;
                }

                DataTable dtEmail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from EmailSetup where Email='" + txtloginID.Text.Trim() + "'").Tables[0];
                if (dtEmail.Rows.Count > 0)
                {
                    string strBooking = dtEmail.Rows[0]["BookingConfirmation"].ToString();
                    string strException = dtEmail.Rows[0]["Exception"].ToString();
                    string strMustMoves = dtEmail.Rows[0]["MustMoves"].ToString();
                    string strSIgnUp = dtEmail.Rows[0]["SIgnUp"].ToString();
                    string strpreference = dtEmail.Rows[0]["Preference"].ToString();

                    for (int i = 0; i < rblEmailConfig.Items.Count; i++)
                    {
                        if (strBooking == "1" && rblEmailConfig.Items[i].Value == "BC")
                        {
                            rblEmailConfig.Items[i].Selected = true;
                        }
                        if (strException == "1" && rblEmailConfig.Items[i].Value == "EXC")
                        {
                            rblEmailConfig.Items[i].Selected = true;
                        }
                        if (strMustMoves == "1" && rblEmailConfig.Items[i].Value == "MM")
                        {
                            rblEmailConfig.Items[i].Selected = true;
                        }
                        //if (strSIgnUp == "1" && rblEmailConfig.Items[i].Value == "SU")
                        //{
                        //    rblEmailConfig.Items[i].Selected = true;
                        //}
                        if (strpreference == "1" && rblEmailConfig.Items[i].Value == "SR")
                        {
                            rblEmailConfig.Items[i].Selected = true;
                        }
                    }
                }
            }
            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            lblMan.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            lblrowtodelete.Text = lblrowid.Text;
            mpeconfirm.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion

    #region  User Defined Function

    public void List()
    {
        DataTable dt = objMember.UserList();
        if (dt.Rows.Count > 0)
        {
            gvUser.DataSource = dt;
            gvUser.DataBind();

            foreach (GridViewRow item in gvUser.Rows)
            {
                Label lblPhone = (Label)item.FindControl("lblPhone");
                lblPhone.Text = genclass.Format_Phone(lblPhone.Text.Trim().Replace(".", ""));
            }
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();
        }
    }

    public string GetPhoneCode(string strCountry)
    {
        string strPCode = string.Empty;
        DataTable dtPhoneCode = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select PhoneCode from JETEDGE_FUEL..Country where Row_ID='" + strCountry + "'").Tables[0];

        if (dtPhoneCode.Rows.Count > 0)
        {
            strPCode = dtPhoneCode.Rows[0]["PhoneCode"].ToString().Trim();
        }

        return strPCode;
    }

    public void Clear()
    {
        try
        {
            hdfPSRID.Value = "0";
            btnSave.Text = "Save";
            divLMBy.Visible = false;
            ddlgender.ClearSelection();

            txtFname.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtloginID.Text = string.Empty;
            txtPhone.Value = string.Empty;
            txtAddress.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtState.Text = string.Empty;
            ddlCountry.ClearSelection();
            txtZip.Text = string.Empty;
            ddlPhoneCode.ClearSelection();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void Bind_Country_Dropdown()
    {
        genclass.Bind_Country_Dropdown(ddlCountry);
    }

    private void Bind_PhoneCode_Dropdown(string Country)
    {
        genclass.Bind_PhoneCode_Dropdown(ddlPhoneCode, Country);
    }

    void SendEmail(string strEmail, string strPassword, string strName)
    {
        StringBuilder sbEmailBody = new StringBuilder();

        // Subject of the mail
        string subject = "Welcome to Jet Edge !";
        
        string strURL = "https://www.flyjetedge.net/FLYT/Views/LandingPage.aspx";
        //Mail Body
        sbEmailBody.Append("Dear " + strName + ", <img height=50 width=150 style='float:right;padding-right:5%' src= https://www.flyjetedge.net/JETEDGE/Images/Jet_Edge_logo.jpg /><br><br>");
        sbEmailBody.Append("Jet Edge has created a Zoom user account for you.<br><br>");
        sbEmailBody.Append("Your account details are as follows:<br><br>");
        sbEmailBody.Append("URL : <a href='" + strURL + "'>" + strURL + " </a><br><br>");
        sbEmailBody.Append("Login ID : " + strEmail + "<br><br>");
        sbEmailBody.Append("Password : " + strPassword + " <br><br>");

        sbEmailBody.Append("To log into Jet Edge Zoom, please use the above URL. Please save this <a href='" + strURL + "'>" + strURL + " </a> in browser favorites to access Zoom in the future.  <br><br>");
        sbEmailBody.Append("If you have questions, please do not hesitate to contact Jet Edge support at zoom@flyjetedge.com.<br><br><br>");


        sbEmailBody.Append("Thank you for your co-operation,<br><br>Jet Edge Zoom Team<br>");
        sbEmailBody.Append("<h3><u>NOTE: Replies to this email will not be accepted</u></h3><br>");


        try
        {
            string strRes = genclass.SendEmail_Office365(strEmail, "", subject, sbEmailBody.ToString());

            if (strRes.ToLower().Trim() != "yes")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Email sending failed.');", true);
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Email sending failed.');", true);
        }

    }

    #endregion

}