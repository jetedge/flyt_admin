﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="UserPreference.aspx.cs" Inherits="Views_UserPreference" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script language="javascript" type="text/javascript">
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row" id="tblGrid" runat="server">
        <div class="col-xl-1">
        </div>
        <div class="col-xl-10">
            <div class="kt-portlet">
                <div class="kt-portlet__body p-2">
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <div class="form-group row" id="dvFilter" runat="server" visible="true">
                                <div class="col-4">
                                    <asp:TextBox ID="txtusername" runat="server" CssClass="form-control" AutoPostBack="true"
                                        Autocomplete="off" placeholder="Search By User"></asp:TextBox>
                                </div>
                                <div class="col-4">
                                    <asp:TextBox ID="txtairport" runat="server" CssClass="form-control" AutoPostBack="true"
                                        Autocomplete="off" placeholder="Search By Airport"></asp:TextBox>
                                </div>
                                <div class="col-4">
                                    <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" placeholder="Search By Date"
                                        AutoPostBack="true" Autocomplete="off"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDate"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 p-0 boxshadow table-responsive">
                            <asp:GridView ID="gvUser" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                CssClass="table" HeaderStyle-CssClass="thead-dark" EmptyDataText="No user preference details found."
                                Width="100%" OnRowDataBound="gvuser_OnRowDataBound" role="grid">
                                <Columns>
                                    <asp:TemplateField Visible="true">
                                        <ItemTemplate>
                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("userid") %>');">
                                                <img id="imgdiv<%# Eval("userid") %>" width="9px" border="0" src="../Images/minus.gif"
                                                    alt="" /></a></ItemTemplate>
                                        <ItemStyle Width="2%" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lbluserid" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "userid") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle Width="10%" VerticalAlign="Middle"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" HeaderStyle-Font-Bold="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHeaderPosition" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "username") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle Width="30%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        HeaderStyle-Font-Bold="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHeaderCode" runat="server" Style='white-space: normal' Text='<%#DataBinder.Eval(Container.DataItem, "Email") %>'></asp:Label></ItemTemplate>
                                        <ItemStyle Width="60%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" HeaderStyle-Font-Bold="true">
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="4">
                                                    <div id="div<%# Eval("userid") %>" style="overflow: auto; display: inline; position: relative;
                                                        overflow: auto; width: 100%;">
                                                        <asp:GridView ID="gv_Child" runat="server" AutoGenerateColumns="false" Width="100%"
                                                            DataKeyNames="rowid" ShowFooter="false" OnRowCommand="ContactsGridView_RowCommand"
                                                            UseAccessibleHeader="true" CssClass="table" HeaderStyle-CssClass="thead-dark">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%"
                                                                    HeaderStyle-Font-Bold="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Start city" ItemStyle-HorizontalAlign="Right" Visible="false"
                                                                    HeaderStyle-Font-Bold="true">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblrowid" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "rowid") %>'></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Start city" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="left"
                                                                    HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblstartcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "startcity") %>'></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="End city" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="left"
                                                                    HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblendcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "endcity") %>'></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Start Date" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblstartdate" runat="server"></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="End Date" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="13%" ItemStyle-Width="13%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblenddate" runat="server"></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Time" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Timeflag") %>'></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblemail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Emailflag") %>'></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="SMS" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSMs" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SMSflag") %>'></asp:Label></ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/pencil_Edit.png" HeaderText="Edit"
                                                                    HeaderStyle-Width="5%" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-Font-Bold="true" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
        </div>
    </div>
    <div class="row" id="tblForm" runat="server" visible="false">
        <div class="col-xl-12" style="padding-left: 4%; padding-right: 4%;">
            <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <label class="importantlabels">
                            <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                        </label>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                            ValidationGroup="SP" />
                        <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                            OnClick="btnViewOnClick" />
                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                        <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="kt-portlet__body p-4">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label class="col-3 col-form-label" style="max-width: 20%;">
                                    Which city you want fly from ? <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox class="cusinputuser" runat="server" ID="txtScity1" Style="width: 100%"
                                        TabIndex="1" MaxLength="20" placeholder="Start City" AutoPostBack="true"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                        CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                        CompletionSetCount="1" TargetControlID="txtScity1" ID="AutoCompleteExtender2"
                                        runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                    <asp:Label ID="lblScity1" class="labelwithcolor" runat="server" Visible="false"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtScity1"
                                        Display="None" ErrorMessage="Start City is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 20%;">
                                    Which city you want fly to ? <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox class="cusinputuser" runat="server" ID="txtEcity1" AutoPostBack="true"
                                        Style="width: 100%" MaxLength="20" placeholder="End City"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                        CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                        CompletionSetCount="1" TargetControlID="txtEcity1" ID="AutoCompleteExtender1"
                                        runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                    <asp:Label ID="lblEcity1" class="labelwithcolor" runat="server" Visible="false"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtEcity1"
                                        Display="None" ErrorMessage="End City is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label" style="max-width: 20%;">
                                    What date you want fly from 
                                </label>
                                <div class="col-3">
                                    <asp:Label ID="lbldate1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                                    <asp:TextBox class="cusinputuser" runat="server" Style="width: 100%" ID="txtDate1"
                                        TabIndex="2" MaxLength="20" Placeholder="Date(MM-DD-YYYY)" onkeypress="return NumericsIphon(event)" />
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate1"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                  <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDate1"
                                        Display="None" ErrorMessage="Available From Date is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 20%;">
                                    To <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:Label ID="lbltodate1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                                    <asp:TextBox class="cusinputuser" runat="server" onkeypress="return NumericsIphon(event)"
                                        Placeholder="Date(MM-DD-YYYY)" Style="width: 100%" ID="txtToDate1" TabIndex="2"
                                        MaxLength="20" />
                                    <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtToDate1"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtToDate1"
                                        Display="None" ErrorMessage="Available To Date is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label" style="max-width: 20%;">
                                    What time you want to fly ?
                                </label>
                                <div class="col-3" style="padding-top: 10px;">
                                    <asp:Label ID="lblflytime1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                                    <asp:RadioButtonList ID="rblflytime1" runat="server" Style="width: 100%" AutoPostBack="true"
                                        OnSelectedIndexChanged="rblflytime1_OnSelectedIndexChanged" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True">Anytime</asp:ListItem>
                                        <asp:ListItem>Specific Time</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-6">
                                    <div class="row" id="trtime" runat="server" visible="false">
                                        <label class="col-6 col-form-label" style="max-width: 40%;">
                                            What time you want to fly ?
                                        </label>
                                        <div class="col-6">
                                            <asp:Label ID="lbltime1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                                            <asp:TextBox runat="server" onkeypress="return NumericsColon(event)" Placeholder="Time(HH:mm)"
                                                Style="width: 100%" ID="txtTime1" TabIndex="2" MaxLength="20" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTime1"
                                                Display="None" ErrorMessage="Available time is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-5 col-form-label">
                                    Do you want to recieve a email notification when trip is available ?
                                </label>
                                <div class="col-3" style="padding-top: 10px;">
                                    <asp:Label ID="lblEmail1" runat="server" class="labelwithcolor" Visible="false"></asp:Label>
                                    <asp:RadioButtonList ID="rblEmailFlag" runat="server" Style="width: 40%" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True">Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-5 col-form-label">
                                    Do you want to recieve a SMS notification when trip is available ?
                                </label>
                                <div class="col-3" style="padding-top: 10px;">
                                    <asp:RadioButtonList ID="rblSMSFlag" runat="server" Style="width: 40%" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True">Yes</asp:ListItem>
                                        <asp:ListItem>No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                <div style="float: left; padding-top: 10px;">
                                    <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                        font-weight: 600;"></asp:Label>
                                    <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:Label ID="lbluserT" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
