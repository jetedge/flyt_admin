﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="PageHits.aspx.cs" Inherits="Views_PageHits" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .card.card-custom.card-stretch.gutter-b
        {
            height: auto;
        }
        .card.card-custom > .card-header
        {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-box-align: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            min-height: 50px;
            padding-top: 0;
            padding-bottom: 0;
            background-color: transparent;
        }
        .card-header
        {
            padding: 1.3rem;
            margin-bottom: 0;
            background-color: #ffffff;
            border-bottom: 1px solid #EBEDF3;
        }
        .box-shadow
        {
            box-shadow: 0 0 8px 0px rgb(197 195 195) !important;
        }
        
        .text-muted
        {
            color: #3f4254 !important;
            margin-top: 0;
            font-weight: 400;
            font-size: 0.95rem;
            letter-spacing: 0.5px;
        }
        .nav.nav-pills .nav-link
        {
            color: #B5B5C3;
            -webkit-transition: color 0.15s ease, background-color 0.15s ease, border-color 0.15s ease, -webkit-box-shadow 0.15s ease;
            transition: color 0.15s ease, background-color 0.15s ease, border-color 0.15s ease, -webkit-box-shadow 0.15s ease;
            transition: color 0.15s ease, background-color 0.15s ease, border-color 0.15s ease, box-shadow 0.15s ease;
            transition: color 0.15s ease, background-color 0.15s ease, border-color 0.15s ease, box-shadow 0.15s ease, -webkit-box-shadow 0.15s ease;
            align-self: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="uplPageHits" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
            <asp:PostBackTrigger ControlID="btnSubmit" />
            <asp:PostBackTrigger ControlID="gvPageHits" />
        </Triggers>
        <ContentTemplate>
            <div class="kt-portlet mt-2 mb-2">
                <div class="form-group row mb-3">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <label class="col-lg-2 col-form-label text-center" style="align-self: center;">
                                From Date <span style="color: Red">*</span>
                            </label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtfromdate" class="cusinputuser" Placeholder="MMM d, yyyy" onkeypress="return false;"
                                    runat="server" Width="98%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtfromdate"
                                    Display="None" ErrorMessage="From Date is required." SetFocusOnError="True" ValidationGroup="FORM" />
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfromdate"
                                    CssClass="cal_Theme1" Format="MMM d, yyyy" Enabled="True">
                                </asp:CalendarExtender>
                            </div>
                            <label class="col-lg-2 col-form-label text-center" style="align-self: center;">
                                To Date<span style="color: Red">*</span>
                            </label>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txttodate" class="cusinputuser" Placeholder="MMM d, yyyy" onkeypress="return false;"
                                    runat="server" Width="98%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttodate"
                                    Display="None" ErrorMessage="To Date is required." SetFocusOnError="True" ValidationGroup="FORM" />
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txttodate"
                                    CssClass="cal_Theme1" Format="MMM d, yyyy" Enabled="True">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-lg-2 text-left">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-danger"
                                    OnClick="btnSubmit_Click" ValidationGroup="FORM" CausesValidation="true" />
                                <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="FORM" ShowMessageBox="true"
                                    runat="server" ShowSummary="false" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 text-right" style="align-self: center;">
                    </div>
                </div>
                <div class="row m-0 mb-1">
                    <div class="col-xl-8">
                        <div class="card card-custom gutter-b card-stretch card-shadowless">
                            <div class="card-body p-0">
                                <ul class="dashboard-tabs nav nav-pills nav-danger row row-paddingless m-0 p-0" role="tablist">
                                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0"><a class="nav-link border  box-shadow d-flex flex-grow-1 rounded flex-column align-items-center"
                                        href="#dvDetailedView"><span class="nav-text font-size-lg pt-2 font-weight-bold text-center">
                                            <asp:Label ID="lblTotalHits" runat="server" Style="font-size: 1.3rem; font-weight: 600;" />
                                        </span><span class="nav-text font-size-lg py-2 font-weight-bold text-center"># Hits
                                        </span></a></li>
                                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0"><a class="nav-link border  box-shadow d-flex flex-grow-1 rounded flex-column align-items-center"
                                        href="#tab_forms_widget_2"><span class="nav-text font-size-lg pt-2 font-weight-bold text-center">
                                            <asp:Label ID="lblIPHits" runat="server" Style="font-size: 1.3rem; font-weight: 600;" />
                                        </span><span class="nav-text font-size-lg py-2 font-weight-bolder text-center"># Unique
                                            Hits </span></a></li>
                                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0"><a class="nav-link border  box-shadow d-flex flex-grow-1 rounded flex-column align-items-center"
                                        href="#tab_forms_widget_3"><span class="nav-text font-size-lg pt-2 font-weight-bold text-center">
                                            <asp:Label ID="lblRegionHits" runat="server" Style="font-size: 1.3rem; font-weight: 600;" />
                                        </span><span class="nav-text font-size-lg py-2 font-weight-bolder text-center"># Hit
                                            States </span></a></li>
                                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-0 mb-3 mb-lg-0"><a class="nav-link border  box-shadow d-flex flex-grow-1 rounded flex-column align-items-center"
                                        href="#tab_forms_widget_4"><span class="nav-text font-size-lg pt-2 font-weight-bold text-center">
                                            <asp:Label ID="lblUserCreated" runat="server" Style="font-size: 1.3rem; font-weight: 600;" />
                                        </span><span class="nav-text font-size-lg py-2 font-weight-bolder text-center"># Users
                                            Created </span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card card-custom gutter-b card-stretch card-shadowless">
                            <div class="card-body p-0">
                                <ul class="dashboard-tabs nav nav-pills nav-danger row row-paddingless m-0 p-0" role="tablist">
                                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0"><a class="nav-link border  box-shadow d-flex flex-grow-1 rounded flex-column align-items-center"
                                        href="#dvDetailedView"><span class="nav-text font-size-lg pt-2 font-weight-bold text-center">
                                            <asp:Label ID="lblNoofBookings" runat="server" Style="font-size: 1.3rem; font-weight: 600;" />
                                        </span><span class="nav-text font-size-lg py-2 font-weight-bold text-center"># Bookings
                                        </span></a></li>
                                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-0"><a class="nav-link border  box-shadow d-flex flex-grow-1 rounded flex-column align-items-center"
                                        href="#tab_forms_widget_5"><span class="nav-text font-size-lg pt-2 font-weight-bold text-center">
                                            <asp:Label ID="lblTotalAmount" runat="server" Text="$0.00" Style="font-size: 1.3rem;
                                                font-weight: 600;" />
                                        </span><span class="nav-text font-size-lg py-2 font-weight-bolder text-center">Total
                                            Amount </span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-0 mb-1">
                    <div class="col-xl-8">
                        <div class="card card-custom box-shadow gutter-b pt-2 card-stretch card-shadowless">
                            <div class="card-header h-auto border-0">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        <span class="d-block text-dark font-weight-bolder">Hour wise unique Hits</span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body p-3" style="position: relative;">
                                <div id="kt_chart_Charter_Hours" style="height: 300px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="card card-custom box-shadow gutter-b pt-2 card-stretch card-shadowless">
                            <div class="card-header border-0">
                                <div class="card-title">
                                    <h3 class="card-label">
                                        <span class="d-block text-dark font-weight-bolder">Today Bookings</span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body card-body p-2">
                                <asp:DataList ID="dlRecentBookings" RepeatDirection="Horizontal" RepeatColumns="1"
                                    runat="server" Width="100%">
                                    <ItemTemplate>
                                        <div class="d-flex align-items-center mb-3 pb-3" style="border-bottom: 1px dotted #ccc;">
                                            <div class="symbol symbol-40 symbol-light-white mr-5">
                                                <div class="symbol-label">
                                                    <span class="navi-icon"><i class="flaticon2-list-3" style="font-size: 1.7rem;"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column font-weight-bold">
                                                <span class="text-dark text-hover-primary mb-1 font-size-lg">
                                                    <asp:Label ID="lblFromICAODisp" runat="server" Text='<%# Eval("FromICAODisp") %>' />
                                                    -
                                                    <asp:Label ID="lblToICAODisp" runat="server" Text='<%# Eval("ToICAODisp") %>' /></span>
                                                <span class="text-muted">FLYT
                                                    <asp:Label ID="lblTransactionReference" runat="server" Text='<%# Eval("TransactionReference") %>' />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Amount :
                                                    <asp:Label ID="lblTotalAmount" runat="server" Text='<%# "$" + Eval("TotalAmount","{0:N2}") %>' />
                                                </span><span class="text-muted">Booked For :
                                                    <asp:Label ID="lblCFirstName" runat="server" Text='<%# Eval("CFirstName") %>' />
                                                    <asp:Label ID="lblCLastName" runat="server" Text='<%# Eval("CLastName") %>' /></span>
                                                <span class="text-muted">Booking Placed :
                                                    <asp:Label ID="lblBookedDateTime" runat="server" Text='<%# Eval("BookedDateTime") %>' /></span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                                <div class="form-group row m-0" id="lblNoTrips" runat="server" visible="false">
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 m-0 text-center bg-light-warning"
                                        style="padding: 2rem; border-radius: 10px;">
                                        <label style="font-size: 1.3rem; letter-spacing: 0.3px; color: Black;">
                                            No Recent Bookings
                                        </label>
                                    </div>
                                </div>
                                <%--<div class="d-flex align-items-center mb-10">
                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                        <div class="symbol-label">
                                            <span class="navi-icon"><i class="flaticon2-list-3"></i></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column font-weight-bold">
                                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Booking Number 002</a>
                                        <span class="text-muted">Creative Director</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center mb-10">
                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                        <div class="symbol-label">
                                            <span class="navi-icon"><i class="flaticon2-list-3"></i></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column font-weight-bold">
                                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Booking Number 003</a>
                                        <span class="text-muted">Lead Developer</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center mb-10">
                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                        <div class="symbol-label">
                                            <span class="navi-icon"><i class="flaticon2-list-3"></i></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column font-weight-bold">
                                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Booking Number 004</a>
                                        <span class="text-muted">Backend Developer</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center mb-2">
                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                        <div class="symbol-label">
                                            <span class="navi-icon"><i class="flaticon2-list-3"></i></span>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column font-weight-bold">
                                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Booking Number 005</a>
                                        <span class="text-muted">Project Manager</span>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-0 mb-1" id="dvDetailedView" runat="server">
                    <div class="col-xl-12">
                        <div class="card card-custom box-shadow gutter-b mb-0 card-stretch card-shadowless">
                            <div class="card-header h-auto border-0">
                                <div class="card-title" style="width: 50%;">
                                    <h3 class="card-label" style="width: 100%;">
                                        <span class="d-block text-dark font-weight-bolder">Page Hits <span class="text-dark-50 mt-2 font-size-sm">
                                            ( Detailed View )</span> </span>
                                    </h3>
                                </div>
                                <div class="card-toolbar">
                                    <asp:Button ID="btnExport" runat="server" Visible="false" Text="Export To Excel"
                                        CssClass="btn btn-danger font-weight-bolder font-size-sm" OnClick="btnExport_Click" />
                                </div>
                            </div>
                            <div class="card-body p-4" id="dvPageHits" runat="server" style="max-height: 70vh;
                                padding-top: 3px !important; overflow-x: auto;">
                                <asp:GridView runat="server" ID="gvPageHits" AutoGenerateColumns="false" Width="100%"
                                    EmptyDataText="No Details Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                    role="grid" OnSelectedIndexChanged="gvPageHits_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="3%"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSNo" runat="server" Text=' <%#Container.DataItemIndex+1 %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="12%"
                                            HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcountry" runat="server" Text='<%# String.Format("{0} ( {1} )", Eval("country"), Eval("countryCode")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Region" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="14%"
                                            HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblregionName" runat="server" Text='<%# String.Format("{0} ( {1} )", Eval("regionName"), Eval("region")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="city" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="12%"
                                            HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcity" runat="server" Text='<%# Eval("city") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Timezone" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="12%"
                                            HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltimezone" runat="server" Text='<%# Eval("timezone") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Hits" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalHits" runat="server" Text='<%# Eval("TotalHits") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="# Unique Hits" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUniqIPHits" runat="server" Text='<%# Eval("UniqIPHits") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%"
                                            ItemStyle-Width="6%" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbView" runat="server" Height="19px" TabIndex="3" ImageUrl="~/Images/view_new.png"
                                                    CommandName="Select" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
            <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
            <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
            <asp:Button ID="btnUserPre" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpeDetailed" runat="server" TargetControlID="btnUserPre"
                PopupControlID="pnlDetailed" BackgroundCssClass="modalBackground" CancelControlID="PrefClose">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlDetailed" runat="server" Style="display: none; overflow-y: auto;
                width: 90%; height: 90%; background-color: White; padding: 10px">
                <div class="form-group row m-0 mb-2" style="align-items: center;">
                    <div class="col-3 ">
                        <label style="font-weight: 600; letter-spacing: 0.5px; font-size: 1.1rem;">
                            Page Hits ( Detailed View )</label>
                    </div>
                    <div class="col-3 text-center">
                        <label style="font-weight: 600; letter-spacing: 0.5px; font-size: 1.1rem;">
                            # Hits :
                        </label>
                        <asp:Label ID="lblPopupHits" runat="server" Style="font-weight: bold; color: #ff5722;" />
                    </div>
                    <div class="col-3 text-center">
                        <label style="font-weight: 600; letter-spacing: 0.5px; font-size: 1.1rem;">
                            # Unique Hits :
                        </label>
                        <asp:Label ID="lblPopUniqueHits" runat="server" Style="font-weight: bold; color: #ff5722;" />
                    </div>
                    <div class="col-3 text-right">
                        <asp:LinkButton ID="PrefClose" runat="server" Style="color: rgb(255,255,255); float: right;
                            text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                    </div>
                </div>
                <div class="form-group row m-0">
                    <div class="col-12 ">
                        <asp:GridView runat="server" ID="gvDetailed" AutoGenerateColumns="false" Width="100%"
                            EmptyDataText="No Details Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                            role="grid">
                            <Columns>
                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="3%"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSNo" runat="server" Text=' <%#Container.DataItemIndex+1 %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Hit Date /Time" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="11%"
                                    HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblHittedOn" runat="server" Text='<%# Eval("HittedOn") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IP" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%"
                                    HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIP" runat="server" Text='<%# Eval("IP") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="12%"
                                    HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcountry" runat="server" Text='<%# String.Format("{0} ( {1} )", Eval("country"), Eval("countryCode")) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Region" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="14%"
                                    HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblregionName" runat="server" Text='<%# String.Format("{0} ( {1} )", Eval("regionName"), Eval("region")) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="city" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="12%"
                                    HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcity" runat="server" Text='<%# Eval("city") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Timezone" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="12%"
                                    HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbltimezone" runat="server" Text='<%# Eval("timezone") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            <script type="text/javascript" src="../assets/vendors/general/jquery/dist/jquery.js"></script>
            <script type="text/javascript" src="../assets/vendors/general/raphael/raphael.js"></script>
            <script type="text/javascript" src="../assets/vendors/general/morris.js/morris.js"></script>
            <script type="text/javascript">
                function parseSVG(s) {
                    var div = document.createElementNS('http://www.w3.org/1999/xhtml', 'div');
                    div.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg">' + s + '</svg>';
                    var frag = document.createDocumentFragment();
                    while (div.firstChild.firstChild)
                        frag.appendChild(div.firstChild.firstChild);
                    return frag;
                }

                $(function () {
                    $.ajax({
                        type: "POST",
                        url: "PageHits.aspx/GetSummary",
                        data: "",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        failure: function (response) {
                            alert(response.responseText);
                        },
                        error: function (response) {
                            alert(response.responseText);
                        }
                    });
                });

                function OnSuccess(response) {
                    var day_data = response.d;
                    var CharterDate = JSON.parse(day_data);
                    var color_array = ['#12437C', '#B8CCDB', '#8D929D', '#D2D1DA', '#8D929D', '#3498DB', '#B29215', '#d24a67', '#00739a', '#36AFB2', '#9c6db2', '#d24a67', '#89a958'];
                    //var bar =
                    Morris.Bar({
                        element: 'kt_chart_Charter_Hours',
                        data: CharterDate,
                        xkey: 'MyHour',
                        parseTime: false,
                        barColors: ['#12437C', '#B8CCDB', '#8D929D'],
                        ykeys: ['HitCount'],
                        labels: ['# Unique Hits'],
                        fillOpacity: 0.8,
                        barShape: 'soft',
                        barGap: 3,
                        stacked: false,
                        barSizeRatio: 0.40,
                        xLabelAngle: 0,
                        xLabelMargin: 0,
                        yLabelAngle: 1,
                        smooth: true,
                        hideHover: true,
                        resize: true,
                        redraw: true,
                        labelTop: true
                    });
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
