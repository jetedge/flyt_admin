﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class Views_LandingGallery : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.LandingGalleryBLL objLandingGallery = new BusinessLayer.LandingGalleryBLL();

    public int linenum = 0;
    public string MethodName = string.Empty;

    #endregion

    #region Pageload

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            try
            {
                // Put user code to initialize the page here
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Landing Page Gallery");
                }


                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "General Setup";
                lblSubHeader.InnerText = "Landing Page Gallery";

                lblUserId.Text = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

                List();
            }
            catch (Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }
    }

    #endregion

    #region  User Defined Function

    private void List()
    {
        imgSlider1.Src = "../assets/img/No_Image_Available.jpg";
        imgSlider2.Src = "../assets/img/No_Image_Available.jpg";
        imgSlider3.Src = "../assets/img/No_Image_Available.jpg";
        imgSlider4.Src = "../assets/img/No_Image_Available.jpg";

        imgSlder1.Src = "../assets/img/No_Image_Available.jpg";
        imgSlder2.Src = "../assets/img/No_Image_Available.jpg";
        imgSlder3.Src = "../assets/img/No_Image_Available.jpg";
        imgSlder4.Src = "../assets/img/No_Image_Available.jpg";

        lblSlider1.Text = string.Empty;
        lblSlider1RowId.Text = string.Empty;

        lblSlider2.Text = string.Empty;
        lblSlider2RowId.Text = string.Empty;

        lblSlider3.Text = string.Empty;
        lblSlider3RowId.Text = string.Empty;

        lblSlider4.Text = string.Empty;
        lblSlider4RowId.Text = string.Empty;

        DataTable dt = objLandingGallery.ListGallery(rblSetupBy.SelectedValue.Trim());
        if (dt.Rows.Count > 0)
        {
            divLMBy.Visible = true;
            lblLMby.Text = dt.Rows[0]["UserName"].ToString() + " ( on ) " + Convert.ToDateTime(dt.Rows[0]["UploadedOn"]).ToString("MMM dd,yyyy hh:mm tt").ToUpper();

            string strPath = SecretsBLL.LandingImageURL + rblSetupBy.SelectedValue.Trim() + "/";

            DataTable dtTemp = new DataTable();
            DataRow[] drSlide1 = dt.Select("SliderPosition = 1");
            DataRow[] drSlide2 = dt.Select("SliderPosition = 2");
            DataRow[] drSlide3 = dt.Select("SliderPosition = 3");
            DataRow[] drSlide4 = dt.Select("SliderPosition = 4");

            if (drSlide1.Length > 0)
            {
                dtTemp = drSlide1.CopyToDataTable();

                imgSlder1.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
                lblSlider1.Text = dtTemp.Rows[0]["Image1"].ToString();
                lblSlider1RowId.Text = dtTemp.Rows[0]["RowId"].ToString();
                SliderDelete1.Visible = true;

                imgSlider1.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
            }

            if (drSlide2.Length > 0)
            {
                dtTemp = drSlide2.CopyToDataTable();

                imgSlder2.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
                lblSlider2.Text = dtTemp.Rows[0]["Image1"].ToString();
                lblSlider2RowId.Text = dtTemp.Rows[0]["RowId"].ToString();
                SliderDelete2.Visible = true;

                imgSlider2.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
            }

            if (drSlide3.Length > 0)
            {
                dtTemp = drSlide3.CopyToDataTable();

                imgSlder3.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
                lblSlider3.Text = dtTemp.Rows[0]["Image1"].ToString();
                lblSlider3RowId.Text = dtTemp.Rows[0]["RowId"].ToString();
                SliderDelete3.Visible = true;

                imgSlider3.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
            }

            if (drSlide4.Length > 0)
            {
                dtTemp = drSlide4.CopyToDataTable();

                imgSlder4.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
                lblSlider4.Text = dtTemp.Rows[0]["Image1"].ToString();
                lblSlider4RowId.Text = dtTemp.Rows[0]["RowId"].ToString();
                SliderDelete4.Visible = true;

                imgSlider4.Src = strPath + dtTemp.Rows[0]["Image1"].ToString();
            }
        }
        else
        {
            divLMBy.Visible = false;
            lblLMby.Text = string.Empty;
        }
    }

    #endregion

    #region Button Events

    public void rblSetupBy_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnPreview_Click(object sender, System.EventArgs e)
    {
        try
        {
            mpePreview.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void lnkSliderDelete_Click(object sender, System.EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            string strCommand = lnk.CommandName;

            string strRowId = string.Empty;
            string strFileName = string.Empty;
            string retVal = string.Empty;

            if (strCommand == "SLD1")
            {
                strRowId = lblSlider1RowId.Text;
                strFileName = lblSlider1.Text;
                retVal = objLandingGallery.DeleteGallery(strRowId);

                lblSlider1.Text = string.Empty;
                lblSlider1RowId.Text = string.Empty;
            }
            else if (strCommand == "SLD2")
            {
                strRowId = lblSlider2RowId.Text;
                strFileName = lblSlider2.Text;
                retVal = objLandingGallery.DeleteGallery(strRowId);

                lblSlider2.Text = string.Empty;
                lblSlider2RowId.Text = string.Empty;
            }
            else if (strCommand == "SLD3")
            {
                strRowId = lblSlider3RowId.Text;
                strFileName = lblSlider3.Text;
                retVal = objLandingGallery.DeleteGallery(strRowId);

                lblSlider3.Text = string.Empty;
                lblSlider3RowId.Text = string.Empty;
            }
            else if (strCommand == "SLD4")
            {
                strRowId = lblSlider4RowId.Text;
                strFileName = lblSlider4.Text;
                retVal = objLandingGallery.DeleteGallery(strRowId);

                lblSlider4.Text = string.Empty;
                lblSlider4RowId.Text = string.Empty;
            }

            if (retVal != null)
            {
                string FileURL = SecretsBLL.LandingGallery + rblSetupBy.SelectedValue.Trim();
                bool exists = Directory.Exists(FileURL.ToString());
                if (exists)
                {
                    bool existsfile = File.Exists(FileURL.ToString() + strFileName);
                    if (existsfile)
                    {
                        File.Delete(FileURL.ToString() + strFileName);
                    }
                }

                lblalert.Text = "Landing page Image Gallery has been deleted successfully";
                mpealert.Show();
            }

            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            string strSlidePosition = string.Empty;
            string strFileName = string.Empty;

            string FileURL = SecretsBLL.LandingGallery + rblSetupBy.SelectedValue.Trim() + "/";

            bool exists = Directory.Exists(FileURL.ToString());
            if (!exists)
            {
                Directory.CreateDirectory(FileURL.ToString());
            }

            objLandingGallery.UploadedBy = lblUserId.Text;
            objLandingGallery.ImagePath = FileURL;

            int retVal = 0;

            if (fupSlider1.HasFile)
            {
                objLandingGallery.SliderPosition = "1";
                objLandingGallery.ManageType = "INS";
                objLandingGallery.ReportFlag = rblSetupBy.SelectedValue.Trim();

                fupSlider1.SaveAs(FileURL + removeSpecialChar(fupSlider1.FileName));
                objLandingGallery.Image1 = removeSpecialChar(fupSlider1.FileName);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }
            else if (lblSlider1.Text.Length > 0)
            {
                objLandingGallery.SliderPosition = "1";
                objLandingGallery.ManageType = "UPD";
                objLandingGallery.ReportFlag = rblSetupBy.SelectedValue.Trim();
                objLandingGallery.RowID = lblSlider1RowId.Text;
                objLandingGallery.Image1 = removeSpecialChar(lblSlider1.Text);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }

            if (fupSlider2.HasFile)
            {
                objLandingGallery.SliderPosition = "2";
                objLandingGallery.ManageType = "INS";
                objLandingGallery.ReportFlag = rblSetupBy.SelectedValue.Trim();
                fupSlider2.SaveAs(FileURL + removeSpecialChar(fupSlider2.FileName));
                objLandingGallery.Image1 = removeSpecialChar(fupSlider2.FileName);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }
            else if (lblSlider2.Text.Length > 0)
            {
                objLandingGallery.SliderPosition = "2";
                objLandingGallery.ManageType = "UPD";
                objLandingGallery.ReportFlag = rblSetupBy.SelectedValue.Trim();
                objLandingGallery.RowID = lblSlider2RowId.Text;
                objLandingGallery.Image1 = removeSpecialChar(lblSlider2.Text);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }

            if (fupSlider3.HasFile)
            {
                objLandingGallery.SliderPosition = "3";
                objLandingGallery.ManageType = "INS";

                fupSlider3.SaveAs(FileURL + removeSpecialChar(fupSlider3.FileName));
                objLandingGallery.Image1 = removeSpecialChar(fupSlider3.FileName);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }
            else if (lblSlider3.Text.Length > 0)
            {
                objLandingGallery.SliderPosition = "3";
                objLandingGallery.ManageType = "UPD";
                objLandingGallery.ReportFlag = rblSetupBy.SelectedValue.Trim();
                objLandingGallery.RowID = lblSlider3RowId.Text;
                objLandingGallery.Image1 = removeSpecialChar(lblSlider3.Text);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }


            if (fupSlider4.HasFile)
            {
                objLandingGallery.SliderPosition = "4";
                objLandingGallery.ManageType = "INS";
                objLandingGallery.ReportFlag = rblSetupBy.SelectedValue.Trim();
                fupSlider4.SaveAs(FileURL + removeSpecialChar(fupSlider4.FileName));
                objLandingGallery.Image1 = removeSpecialChar(fupSlider4.FileName);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }
            else if (lblSlider4.Text.Length > 0)
            {
                objLandingGallery.SliderPosition = "4";
                objLandingGallery.ManageType = "UPD";
                objLandingGallery.ReportFlag = rblSetupBy.SelectedValue.Trim();
                objLandingGallery.RowID = lblSlider4RowId.Text;
                objLandingGallery.Image1 = removeSpecialChar(lblSlider4.Text);

                retVal = objLandingGallery.SaveGallery(objLandingGallery);
            }


            if (retVal > 0)
            {
                lblalert.Text = "Landing page Image Gallery has been uploaded successfully";
                mpealert.Show();
            }

            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public static string removeSpecialChar(string input)
    {
        return Regex.Replace(input, "[^0-9A-Za-z.-]", "_");
    }

    #endregion
}