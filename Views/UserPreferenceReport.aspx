﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="UserPreferenceReport.aspx.cs" Inherits="Views_UserPreferenceReport"
    Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script language="javascript" type="text/javascript">
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                img.src = "../Images/minus.gif";
            } else {
                div.style.display = "none";
                img.src = "../Images/plus.gif";
            }
        }
    </script>
    <style type="text/css">
        .ajax__calendar_today
        {
            cursor: pointer;
            padding-top: 3px;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="uplPageHits" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
        <ContentTemplate>
            <div class="kt-portlet p-4">
                <div class="form-group row">
                    <div class="col-lg-12">
                        <div class="row" id="dvFilter" runat="server">
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtusername" runat="server" CssClass="form-control" AutoPostBack="true"
                                    OnTextChanged="txtusername_changed" placeholder="Search By User" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtairport" runat="server" CssClass="form-control" AutoPostBack="true"
                                    Style="text-transform: uppercase;" OnTextChanged="txtusername_changed" placeholder="Search By Airport"
                                    autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtFilterDate" runat="server" CssClass="form-control" placeholder="Search By Date"
                                    Width="70%" onkeypress="return false;" AutoPostBack="true" OnTextChanged="txtusername_changed"
                                    autocomplete="off"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFilterDate"
                                    CssClass="cal_Theme1" Format="MMM dd, yyyy" Enabled="True">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-lg-3">
                            </div>
                            <div class="col-lg-3 text-right">
                                <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="btn btn-danger"
                                    Visible="false" OnClick="btnExport_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row" id="dvGrid" runat="server">
                    <div class="col-lg-12  boxshadow table-responsive" id="tblGrid" runat="server">
                        <asp:GridView ID="gvUser" runat="server" AutoGenerateColumns="False" ShowFooter="True"
                            CssClass="table" HeaderStyle-CssClass="thead-dark" EmptyDataText="No User Preference details found."
                            Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSNo" runat="server" Text=' <%#Container.DataItemIndex+1 %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="4%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                                        <asp:Label ID="lblPrefId" runat="server" Text='<%# Eval("PrefId") %>' Visible="false" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="12%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserType" runat="server" Text='<%# Eval("UserType") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="6%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Start City">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartICAO" runat="server" Text='<%# Eval("StartICAO") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="14%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End city">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEndICAO" runat="server" Text='<%# Eval("EndICAO") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="14%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Start Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvailableFromDate" runat="server" Text='<%# Eval("AvailableFromDate") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="End Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvailabletodate" runat="server" Text='<%# Eval("Availabletodate") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Time">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTimeflag" runat="server" Text='<%# Eval("Timeflag") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmailFlag" runat="server" Text='<%# Eval("EmailFlag") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contact Email">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="12%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SMS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSMSFlag" runat="server" Text='<%# Eval("SMSFlag") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="6%" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone Number">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPhoneNumber" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
            <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
            <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
