﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Web.Script.Services;

public partial class Views_FixedPrice : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.FixedPriceBLL objMember = new BusinessLayer.FixedPriceBLL();
    EmptyLegPriceCal objEmptyLegPriceCal = new EmptyLegPriceCal();

    string STARTICAO = string.Empty;
    string STARTStateCode = string.Empty;
    string STARTCountryCode = string.Empty;
    string STARTCity = string.Empty;
    string STARTIATA = string.Empty;


    string ENDICAO = string.Empty;
    string ENDStateCode = string.Empty;
    string ENDCountryCode = string.Empty;
    string ENDCity = string.Empty;
    string ENDIATA = string.Empty;

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Fixed Price");
            }


            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "Admin";
            lblSubHeader.InnerText = "Fixed Price";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();
            Session["ICAOF"] = null;
            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;

            GetAirport();

            List();

            CalendarExtender1.StartDate = DateTime.Now;
            CalendarExtender2.StartDate = DateTime.Now;
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.ddlstatus.SelectedIndexChanged += new System.EventHandler(this.btnViewOnClick);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    #region Button Events

    protected void ddlstatus_onselectedChanged(object sender, EventArgs e)
    {
        List();
    }

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        AirportClass.AirportSplit(txtSAirport.Text, out STARTICAO, out STARTStateCode, out  STARTCountryCode, out STARTCity, out STARTIATA);

        AirportClass.AirportSplit(txtEAirport.Text, out ENDICAO, out  ENDStateCode, out   ENDCountryCode, out  ENDCity, out  ENDIATA);


        if (hdfSPRID.Value == "0")
        {
            objMember.ManageType = "I";
            DataTable dt = objMember.AlreadyExist(txtPriceFrom.Text.Trim(), txtPriceTo.Text.Trim(), rblPricing.SelectedValue.Trim(), STARTICAO.Trim(), ENDICAO.Trim());
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0][0].ToString()) > 0)
                {
                    lblalert.Text = "Fixed Price details from [ " + txtPriceFrom.Text.Trim() + " ] to [ " + txtPriceTo.Text.Trim() + " ] for [ " + STARTICAO.Trim() + " ] to [ " + ENDICAO.Trim() + " ] already exist ";
                    mpealert.Show();
                    txtPriceFrom.Text = "";
                    txtPriceTo.Text = "";
                    txtPriceFrom.Focus();
                    return;
                }
            }
        }
        else
        {
            objMember.ManageType = "U";
        }

        objMember.RowID = hdfSPRID.Value;
        objMember.StartDate = txtPriceFrom.Text;
        objMember.EndDate = txtPriceTo.Text;
        objMember.SpecialPriceDescription = rblPricing.SelectedValue.Trim();
        objMember.PriceDetails = txtAmount.Value;
        objMember.StartAirport = STARTICAO.Trim();
        objMember.EndAirport = ENDICAO.Trim();
        objMember.TailNo = "0";
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

        objMember.StartAirportDisplay = txtSAirport.Text;
        objMember.EndAirportDisplay = txtEAirport.Text;

        int retVal = objMember.Save(objMember);

        if (retVal > 0)
        {

            lblalert.Text = "Fixed Price Details have been saved successfully";
            mpealert.Show();
            btnAdd.Focus();
        }
        else
        {
            lblalert.Text = "An error has occured while processing your request";
            mpealert.Show();
        }

        Clear();
        List();
        tblForm.Visible = false;
        tblGrid.Visible = true;
        btnAdd.Visible = true;
        btnSave.Visible = false;
        btnView.Visible = false;
    }

    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            //dvFilter.Visible = false;
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();


            tblForm.Visible = false;
            tblGrid.Visible = true;
            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
            //dvFilter.Visible = true;

            btnAdd.Focus();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            objMember.DeletUser(lblrowtodelete.Text.Trim());
            List();
           // objEmptyLegPriceCal.FunEmptyPriceCal();
            hdfSPRID.Value = "0";
            lblalert.Text = "Fixed Price Details have been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvSpecialPricing_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvSpecialPricing.SelectedRow.FindControl("lblrowid");
            Label lblcreatedby = (Label)gvSpecialPricing.SelectedRow.FindControl("lblcreatedby");
            DataTable dtSP = objMember.Edit(lblrowid.Text.Trim());

            if (dtSP != null && dtSP.Rows.Count > 0)
            {
                hdfSPRID.Value = lblrowid.Text;
                txtPriceFrom.Text = Convert.ToDateTime(dtSP.Rows[0]["StartDate"].ToString()).ToString("MM-dd-yyyy");
                txtPriceTo.Text = Convert.ToDateTime(dtSP.Rows[0]["EndDate"].ToString()).ToString("MM-dd-yyyy");
                rblPricing.SelectedIndex = rblPricing.Items.IndexOf(rblPricing.Items.FindByValue(dtSP.Rows[0]["SpecialPriceDescription"].ToString()));
                txtAmount.Value = dtSP.Rows[0]["PriceDetails"].ToString();


                DataTable dtSPstart = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, " Select   UPPER(case when SI.StateCode='HI' then SI.City+', '+SI.StateCode+' ('+SI.IATA+'/'+SI.ICAO +')' "
               + " WHEN SI.Country='1' then SI.City+', '+SI.StateCode+' ('+ case when ISNULL(SI.IATA,'')<>'' then SI.IATA+'/' else '' end+ SI.ICAO +')'  "
               + " ELSE SI.City+', '+SIC.Country_Code+' ('+SI.ICAO +')' END)   Departure FROM JETEDGE_FUEL..ST_AIRPORT  SI LEFT JOIN JETEDGE_FUEL..Country SIC on SI.Country=SIC.Row_ID  Where icao='" + dtSP.Rows[0]["StartAirport"].ToString() + "'").Tables[0];


                txtSAirport.Text = dtSPstart.Rows[0]["Departure"].ToString();


                DataTable dtSPend = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, " Select   UPPER(case when SI.StateCode='HI' then SI.City+', '+SI.StateCode+' ('+SI.IATA+'/'+SI.ICAO +')' "
              + " WHEN SI.Country='1' then SI.City+', '+SI.StateCode+' ('+case when ISNULL(SI.IATA,'')<>'' then SI.IATA+'/' else '' end+SI.ICAO +')'  "
              + " ELSE SI.City+', '+SIC.Country_Code+' ('+SI.ICAO +')' END)   Arrival FROM JETEDGE_FUEL..ST_AIRPORT  SI LEFT JOIN JETEDGE_FUEL..Country SIC on SI.Country=SIC.Row_ID  Where icao='" + dtSP.Rows[0]["EndAirPort"].ToString() + "'").Tables[0];


                //DataTable dtSPend = SqlHelper.ExecuteDataset(SqlHelper.FourSeasonsConnectionString, CommandType.Text, "select city,StateName from JETEDGE_FUEL..st_airport Where icao='" + dtSP.Rows[0]["EndAirPort"].ToString() + "'").Tables[0];

                txtEAirport.Text = dtSPend.Rows[0]["Arrival"].ToString();

                //lblupdatedby.Text = ((Label)gvSpecialPricing.SelectedRow.FindControl("lblcreatedby")).Text;
                //lblupdatedon.Text = dtSP.Rows[0]["createdon"].ToString();
            }
            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            //dvFilter.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvTailDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;
                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //  lblMsg.Text = ex.Message;
        }
    }

    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            lblrowtodelete.Text = lblrowid.Text;
            mpeconfirm.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion

    #region  User Defined Function

    public void GetAirport()
    {
        DataTable dt = genclass.GetZoomAirports("YES");

        if (dt.Rows.Count > 0)
        {
            Session["ICAOF"] = dt;
        }
        else
        {
            Session["ICAOF"] = null;
        }

    }

    public void List()
    {
        if (ddlstatus.SelectedValue == "Active")
        {
            DataTable dtActiveGrid = objMember.ActiveSelect();
            if (dtActiveGrid.Rows.Count > 0)
            {
                for (int i = 0; i < dtActiveGrid.Rows.Count; i++)
                {
                    if (dtActiveGrid.Rows[i]["SpecialPriceDescription"].ToString() == "H" || dtActiveGrid.Rows[i]["SpecialPriceDescription"].ToString() == "h")
                    {
                        dtActiveGrid.Rows[i]["SpecialPriceDescription"] = "Per Hour";
                    }
                    else if (dtActiveGrid.Rows[i]["SpecialPriceDescription"].ToString() == "D" || dtActiveGrid.Rows[i]["SpecialPriceDescription"].ToString() == "d")
                    {
                        dtActiveGrid.Rows[i]["SpecialPriceDescription"] = "Fixed Price";
                    }
                }

                gvSpecialPricing.DataSource = dtActiveGrid;
                gvSpecialPricing.DataBind();
            }
            else
            {
                gvSpecialPricing.DataSource = null;
                gvSpecialPricing.DataBind();
            }
        }
        else
        {
            DataTable dtExpiredSelect = objMember.ExpiredSelect();
            if (dtExpiredSelect.Rows.Count > 0)
            {
                for (int i = 0; i < dtExpiredSelect.Rows.Count; i++)
                {
                    if (dtExpiredSelect.Rows[i]["SpecialPriceDescription"].ToString() == "H" || dtExpiredSelect.Rows[i]["SpecialPriceDescription"].ToString() == "h")
                    {
                        dtExpiredSelect.Rows[i]["SpecialPriceDescription"] = "Per Hour";
                    }
                    else if (dtExpiredSelect.Rows[i]["SpecialPriceDescription"].ToString() == "D" || dtExpiredSelect.Rows[i]["SpecialPriceDescription"].ToString() == "d")
                    {
                        dtExpiredSelect.Rows[i]["SpecialPriceDescription"] = "Fixed Price";
                    }
                }

                gvSpecialPricing.DataSource = dtExpiredSelect;
                gvSpecialPricing.DataBind();

            }
            else
            {
                gvSpecialPricing.DataSource = null;
                gvSpecialPricing.DataBind();
            }
        }



    }



    public void Clear()
    {
        try
        {
            hdfSPRID.Value = "0";
            btnSave.Text = "Save";
            txtPriceFrom.Text = string.Empty;
            txtPriceTo.Text = string.Empty;
            rblPricing.SelectedIndex = 0;
            txtAmount.Value = string.Empty;
            txtSAirport.Text = string.Empty;
            txtEAirport.Text = string.Empty;

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText, int count)
    {
        return AutoFillProducts_V1(prefixText);

    }
    private static List<string> AutoFillProducts_V1(string prefixText)
    {
        DataTable dtNew = new DataTable();
        DataTable dt = new DataTable();
        DataRow[] drNew = null;
        if (HttpContext.Current.Session["ICAOC"] == null)
        {
            dt = genclass.GetZoomAirports("YES");

            HttpContext.Current.Session["ICAOC"] = dt;
        }

        dt = (DataTable)HttpContext.Current.Session["ICAOC"];

        if (dt.Rows.Count > 0)
        {
            if (prefixText.Length < 5)
            {
                drNew = dt.Select("ICAO like '%' + '" + prefixText + "' + '%'");
            }
            else
            {
                drNew = dt.Select("StateName like  '" + prefixText + "' + '%' OR City like '" + prefixText + "' + '%'");
            }

            if (drNew.Length > 0)
            {
                dtNew = drNew.CopyToDataTable();
            }
        }
        List<string> sEmail = new List<string>();
        for (int i = 0; i < drNew.Length; i++)
        {
            sEmail.Add(drNew[i]["ICAOName"].ToString());
        }

        //foreach (DataRow row in dtNew.Rows)
        //{
        //    sEmail.Add(row[0].ToString());
        //}
        return sEmail;
    }
    private static List<string> AutoFillProducts(string prefixText)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {
                // com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%'";
                com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "icao like  @Search + '%' AND Access=1";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;


            }

        }
    }

    #endregion
}