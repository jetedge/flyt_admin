﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SessionExpired.aspx.cs" Inherits="SessionExpired" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>Jet Edge | Four Season | Session Expired</title>
    <meta name="description" content="Latest updates and statistic charts" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="shortcut icon" href="../assets/img/Logo_Tra.gif" />
    <link href="../assets/app/custom/error/error-v3.default.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
</head>
<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading"
    style="background-image: url(assets/media/error/bg1.jpg);">
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v3">
            <div class="kt-error_container" style="position: absolute; bottom: 0; margin-bottom: 2rem;">
                <p class="kt-error_title kt-font-light">
                    <img alt="Logo" src="../assets/img/Jet_Edge_logo.jpg" style="height: 85px;" />
                </p>
                <p class="kt-error_title kt-font-light">
                    Session Expired
                </p>
                <p class="kt-error_description" style="color: #464457;">
                    Sorry, your session has expired.<br>
                    Please go back to Login page to continue
                </p>
                <br />
                <p class="kt-error_description">
                    <a href="https://www.flyjetedge.net/FLYT/" class="btn btn-danger">Back to Login Page</a>
                </p>
            </div>
        </div>
    </div>
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>
    <script src="../assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
    <script src="../assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
    <script src="../assets/demo/demo4/base/scripts.bundle.js" type="text/javascript"></script>
    <script src="../assets/app/bundle/app.bundle.js" type="text/javascript"></script>
</body>
</html>
