﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="TaxSetup.aspx.cs" Inherits="Views_TaxSetup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="tblGrid" runat="server">
        <div class="kt-portlet__body p-4" style="padding-right: 4% !important; padding-left: 4% !important;">
            <div class="row">
                <div class="col-xl-12">
                    <div class="form-group row" style="margin-bottom: 0rem;">
                        <label class="col-6 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                            padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; padding-left: 0px;">
                            Tax For Domestic Flight
                        </label>
                        <div class="col-6 right" style="text-align: right; padding-right: 0px;">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" TabIndex="1" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12 p-0 table-responsive boxshadow">
                            <asp:GridView ID="gvdomestic" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                OnSelectedIndexChanged="gvdiscount_SelectedIndexChanged" AutoGenerateColumns="false"
                                PageSize="25" AllowPaging="false" Width="100%">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrowid" Text='<%# Eval("rowid") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="1%" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Effective From" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEffFrom" Text='<%# Eval("EffFrom") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="12%" ItemStyle-Width="12%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" Text='<%# Eval("code") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Width="15%" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="PFLAG" HeaderText="Percentage/Tax(%)" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="6%" ItemStyle-Width="6%" />
                                    <asp:BoundField DataField="Tax" HeaderText="Fees per Passenger" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="Notes" HeaderText="Notes" ItemStyle-HorizontalAlign="left"
                                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="15%" ItemStyle-Width="15%" />
                                    <asp:TemplateField HeaderText="Active" HeaderStyle-Width="3%" ItemStyle-Width="3%"
                                        HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                        HeaderStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkactiveM" TabIndex="7" runat="server" Checked='<%# Convert.ToBoolean(Eval("Show")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbView1" runat="server" TabIndex="4" ImageUrl="~/Images/pencil_Edit.png"
                                                CommandName="Select" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="History" ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle Width="5%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbHist" runat="server" TabIndex="4" ImageUrl="~/Images/view_new.png"
                                                Height="18px" Width="18px" OnClick="imbHistoryDomestic" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 0rem;">
                        <label class="col-6 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                            padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; padding-left: 0px;
                            width: 40%;">
                            ALASKA / HAWAI / Puerto Rico
                        </label>
                        <div class="col-6 right" style="text-align: right; padding-right: 0px;">
                            <asp:Button ID="btnAddAlaska" runat="server" Text="Add" CssClass="btn btn-danger"
                                TabIndex="1" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12 p-0 table-responsive boxshadow">
                            <asp:GridView ID="gvinterntional" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                OnSelectedIndexChanged="gvdiscountInternal_SelectedIndexChanged" AutoGenerateColumns="false"
                                AllowPaging="false" Width="100%">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrowid" Text='<%# Eval("rowid") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="1%" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Effective From" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEffFrom" Text='<%# Eval("EffFrom") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="12%" ItemStyle-Width="12%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCode" Text='<%# Eval("code") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Width="15%" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="PFLAG" HeaderText="Percentage/Tax(%)" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="6%" ItemStyle-Width="6%" />
                                    <asp:BoundField DataField="Tax" HeaderText="Fees per Passenger" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="Notes" HeaderText="Notes" ItemStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left" />
                                    <asp:TemplateField HeaderText="Active" HeaderStyle-Width="3%" ItemStyle-Width="3%"
                                        HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                        HeaderStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkactiveM" TabIndex="7" runat="server" Checked='<%# Convert.ToBoolean(Eval("Show")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbView1" runat="server" TabIndex="4" ImageUrl="~/Images/pencil_Edit.png"
                                                CommandName="Select" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="History" ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle Width="5%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbHist" runat="server" TabIndex="4" ImageUrl="~/Images/view_new.png"
                                                OnClick="imbHistoryIntl" Height="18px" Width="18px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tblForm" runat="server" visible="false">
        <div class="kt-portlet__body p-4">
            <div class="form-group row">
                <div class="col-xl-1">
                </div>
                <div class="col-xl-10 text-right">
                    <label class="importantlabels">
                        <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                            ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                    </label>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                        ValidationGroup="SP" />
                    <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                        OnClick="btnViewOnClick" />
                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                        runat="server" ShowSummary="false" />
                    <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
                </div>
                <div class="col-xl-1">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xl-1">
                </div>
                <div class="col-xl-10">
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            Code <span class="MandatoryField">* </span>
                        </label>
                        <div class="col-4">
                            <asp:TextBox ID="txtcode" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                ToolTip="Enter all characters. Length is limited to 100" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcode"
                                Display="None" ErrorMessage="Code is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                        </div>
                        <label class="col-2 col-form-label">
                            Efective From
                        </label>
                        <div class="col-4">
                            <asp:TextBox CssClass="form-control" TabIndex="6" runat="server" ID="txtEDate" Placeholder="Date(MM-DD-YYYY)"
                                onkeypress="return NumericsIphon(event)" MaxLength="20" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEDate"
                                Format="MM-dd-yyyy" Enabled="True">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEDate"
                                Display="None" ErrorMessage="Effective From is required." SetFocusOnError="True"
                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            Name <span class="MandatoryField">* </span>
                        </label>
                        <div class="col-4">
                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" TabIndex="6" MaxLength="100"
                                autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                                Display="None" ErrorMessage="Name is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                        </div>
                        <label class="col-2 col-form-label">
                            Percentage / Tax
                        </label>
                        <div class="col-1">
                            <asp:DropDownList ID="ddlDiscounttype" TabIndex="4" runat="server" CssClass="form-control">
                                <asp:ListItem Value="$" Text="$" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="%" Text="%"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-3">
                            <asp:TextBox ID="txtRPercent" runat="server" CssClass="form-control" TabIndex="12"
                                Width="100%" onkeypress="return isNumberKey(event,this.id)" onblur="isNumberKeyPer(event,this.id)"
                                autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FT" runat="server" TargetControlID="txtRPercent"
                                ValidChars="0123456789.">
                            </asp:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRPercent"
                                Display="None" ErrorMessage="Tax / Percentage is required." SetFocusOnError="True"
                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            Description
                        </label>
                        <div class="col-4">
                            <asp:TextBox ID="txtdesc" runat="server" CssClass="form-control" TabIndex="12" autocomplete="off"
                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtdesc"
                                Display="None" ErrorMessage="Description is required." SetFocusOnError="True"
                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                        </div>
                        <label class="col-2 col-form-label">
                            Active
                        </label>
                        <div class="col-4" style="padding-top: 15px;">
                            <asp:CheckBox ID="chkActive" Checked="true" TabIndex="9" runat="server" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">
                            Notes
                        </label>
                        <div class="col-10">
                            <asp:TextBox ID="txtnotes" runat="server" CssClass="form-control" TabIndex="12" autocomplete="off"
                                ToolTip="Enter all characters. Length is limited to 100" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row" id="divLMBy" runat="server" visible="false">
                        <div style="float: left; padding-top: 10px;">
                            <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                font-weight: 600;"></asp:Label>
                            <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1">
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnHist" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeHistory" runat="server" PopupControlID="pnlHistory"
        TargetControlID="btnHist" Y="10" CancelControlID="btnHistClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlHistory" runat="server" CssClass="modalPopupnew" Style="display: none;
        background-color: rgb(17,24,32); width: 65%; height: 60%; padding: 10px">
        <div class="form-group row">
            <label class="col-9 col-form-label" style="color: White;">
                <h5 style="color: White; margin-left: 0%; font-size: 18px">
                    History</h5>
            </label>
            <div class="col-lg-3 text-right">
                <asp:Button ID="btnHistClose" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="padding: 3px 12px;" ToolTip="Close Popup"></asp:Button>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-12">
                <asp:GridView ID="gvHistory" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                    EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Effective From">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Bind("EffFrom")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Code">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Bind("Code")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblEmailID" runat="server" Text='<%#Bind("Name")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tax">
                            <ItemTemplate>
                                <asp:Label ID="lblUserType" runat="server" Text='<%#Bind("PFlag")%>' Style="color: White;"></asp:Label>&nbsp;
                                <asp:Label ID="Label1" runat="server" Text='<%#Bind("Tax")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="hdfCDRID" runat="server" />
    <asp:HiddenField ID="hdfTaxFlag" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
