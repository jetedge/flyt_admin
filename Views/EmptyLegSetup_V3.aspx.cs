﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.Services;
using System.Web.Script.Services;
using BusinessLayer;
using DataLayer;

public partial class EmptyLegSetup_V3 : System.Web.UI.Page
{

    EmptyLegPriceCal objEmptyLegPriceCal = new EmptyLegPriceCal();

    EmptyLegDataUpdateCal objEmptyLegDataUpdateCal = new EmptyLegDataUpdateCal();
    ZoomCalculatinDAL objZoomCalculatinDAL = new ZoomCalculatinDAL();
    EarliestTimeBLL objEarliestTimeBLL = new EarliestTimeBLL();
    EarliestTimeDAL objEarliestTimeDAL = new EarliestTimeDAL();

    string ICAO = string.Empty;
    string StateCode = string.Empty;
    string CountryCode = string.Empty;
    string City = string.Empty;
    string IATA = string.Empty;


    string EndICAO = string.Empty;
    string EndStateCode = string.Empty;
    string EndCountryCode = string.Empty;
    string EndCity = string.Empty;
    string EndIATA = string.Empty;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                //objEarliestTimeBLL.distanse("KVNY", "KHPN");
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Empty Leg Details");
                }

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                Session["ICAOE"] = null;
                GetAirport();

                lblMainHeader.InnerText = "Admin";
                lblSubHeader.InnerText = "Empty Leg Setup";

                CalendarExtender3.StartDate = System.DateTime.Now;
                CalendarExtender4.StartDate = System.DateTime.Now;
                CalendarExtender1.StartDate = System.DateTime.Now;
                CalendarExtender2.StartDate = System.DateTime.Now;

                BindEmptyLegs();

                hdfSCID.Value = "0";
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
                btnAdd.Focus();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    public void BindEmptyLegs()
    {
        string strMangeType = string.Empty;
        string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
        string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

        if (rblStatus.SelectedValue.Trim() == "Active")
        {
            strMangeType = "Active";
            lblSubScren.Text = "Add a Manual Trip";
            btnAdd.Visible = true;
        }
        else if (rblStatus.SelectedValue.Trim() == "Booked Trips")
        {
            strMangeType = "Booked";
            lblSubScren.Text = "Modify a FOS Trip";
            btnAdd.Visible = false;
        }
        else if (rblStatus.SelectedValue.Trim() == "Group")
        {
            strMangeType = "Split";
            lblSubScren.Text = "Add a Manual Leg";
            btnAdd.Visible = false;
        }

        DataTable dtOneWayTrip = BindEmptyLegs_SP(strMangeType, strStartDate, strEndDate, "1", "25");
        dtOneWayTrip.Columns.Add("leg");

        if (dtOneWayTrip.Rows.Count > 0)
        {
            gvTailDetails.DataSource = dtOneWayTrip;
            gvTailDetails.DataBind();
        }
        else
        {
            gvTailDetails.DataSource = null;
            gvTailDetails.DataBind();
        }
        gvTailDetails.Visible = true;
    }
    private void Loadcountry()
    {
        try
        {
            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code,Country_Name  from JETEDGE_FUEL..Country where Active='Y' order by Country_Name").Tables[0];

            cblcountry.DataSource = dt;
            cblcountry.DataTextField = "Country_Name";
            cblcountry.DataValueField = "Country_Name";
            cblcountry.DataBind();

            // txtcountry.Text = "United States";
        }
        catch (Exception Ex)
        {

        }
    }
    public void GetAirport()
    {
        DataTable dt = genclass.GetZoomAirports("YES");

        if (dt.Rows.Count > 0)
        {
            Session["ICAOE"] = dt;
        }
        else
        {
            Session["ICAOE"] = null;
        }

    }
    protected void btnSaveOnClick(object sender, EventArgs e)
    {
        string[] CategoryDetails = new string[70];
        string strMsg = string.Empty;
        string strActive = string.Empty;
        lblalert.Text = string.Empty;

        try
        {
            #region

            string strStartDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            string strEndDate = DateTime.Now.AddDays(7).ToString("MM/dd/yyyy HH:mm:ss");

            DataTable dtOneWayTrip = objZoomCalculatinDAL.Get_EmptyTripSetup(strStartDate, strEndDate);
            #endregion
            #region For Manual and Modiy Fos

            string strDepartTrip = "D";
            string strArrTrip = "D";

            if (lblgroup.Text != "Group")
            {
                #region Check earliest depart time

                if (lblEarliestTime.Text != string.Empty)
                {
                    DateTime dtStartTime = Convert.ToDateTime(Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00");
                    if (Convert.ToDateTime(lblEarliestTime.Text) > dtStartTime)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                        lblalert.Text = "Please ensure that the Available from is greater than or equal to the Earliest Departure Time.</br>";
                        mpealert.Show();
                        return;
                    }
                }
                if (lblMaxDepartTime.Text != string.Empty)
                {
                    DateTime dtEndTime = Convert.ToDateTime(Convert.ToDateTime(txtAvaTo.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtEndTime.Value + ":00");
                    if (Convert.ToDateTime(lblMaxDepartTime.Text) < dtEndTime)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                        lblalert.Text = lblalert.Text + "Please ensure that the Available To is less than or equal to the Latest Departure Time.";
                        mpealert.Show();
                        return;
                    }
                }

                #endregion

                string strBusinessAlert = string.Empty;
                if (txtTailNo.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Tail number is required. <br/>";
                }
                if (txttrip.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Trip number is required. <br/>";
                }
                if (txtFlyHours1.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Flying Hours is required. <br/>";
                }
                if (txtFlyHours1.Value.ToString().Length > 0)
                {
                    if (Convert.ToDouble(txtFlyHours1.Value) < 0.7)
                    {
                        strBusinessAlert += "The Selected route is not allowed for booking. Please modify your request. <br/>";
                    }
                }
                if (txtAvaFrom.Text.ToString().Length == 0)
                {
                    strBusinessAlert += "- Available From is required. <br/>";
                }
                if (txtAvaTo.Text.ToString().Length == 0)
                {
                    strBusinessAlert += "- Available To is required. <br/>";
                }
                if (txtMPrice.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Price is required. <br/>";
                }

                if (txtStartTime.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Start Time is required. <br/>";
                }

                if (txtEndTime.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- End Time is required. <br/>";
                }


                if (lblgroup.Text != "Modify")
                {
                    if (gvDepAirport.Rows.Count == 0)
                    {
                        strBusinessAlert += "- Departure Airport is required. <br/>";
                    }

                    if (gvArrivalAirport.Rows.Count == 0)
                    {
                        strBusinessAlert += "- Arrival Airport is required. <br/>";
                    }
                }

                if (gvDepAirport.Rows.Count > 0)
                {
                    if (txtDepDisLandPage.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Departure Landing Page input is required. <br/>";
                    }
                }
                if (gvArrivalAirport.Rows.Count > 0)
                {
                    if (txtArrDisLandPage.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Arrival Landing Page input is required. <br/>";
                    }
                }


                DataTable dtPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select HourlyRate,pricingcompleted  from Tailsetup where tailno='" + txtTailNo.Value.Trim() + "'").Tables[0];

                if (dtPrice.Rows.Count > 0)
                {
                    if (dtPrice.Rows[0]["pricingcompleted"].ToString().Trim() == "0")
                    {
                        strBusinessAlert += "- Please complete the pricing setup for the tail [ " + txtTailNo.Value.Trim() + " ] <br/>";
                    }

                    if (txtMPrice.Value.Trim() != "")
                    {
                        string strHourlyRate = dtPrice.Rows[0]["HourlyRate"].ToString().Trim();

                        if (Convert.ToDecimal(txtMPrice.Value.Trim()) < Convert.ToDecimal(strHourlyRate.Trim()))
                        {
                            strBusinessAlert += "- Please enter price greater than $ " + strHourlyRate + " <br/>";
                        }
                    }
                }
                else
                {
                    strBusinessAlert += "- Tail not exist in pricing setup. <br/>";
                }


                if (strBusinessAlert.Length > 0)
                {
                    lblalert.Text = strBusinessAlert;
                    mpealert.Show();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                    return;
                }
                decimal daystoflight = 0;
                decimal L10_minPrice = 0;
                decimal M10_maxPrice = 0;
                decimal price = 0;

                decimal totalflyingHours = Convert.ToDecimal(txtFlyHours1.Value);

                var HourlyRate = Convert.ToDecimal("0.00"); ;
                var fixedCeiling = Convert.ToDecimal("0.00");
                var fixedFloor = Convert.ToDecimal("0.00");
                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                var MinFLHSellable = Convert.ToDecimal("0.00");
                var finalPricevalidation2 = (decimal)0;


                var dtFlyingDays = Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00";

                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00").Subtract(DateTime.Now);

                var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                string stractive = string.Empty;

                if (chkEmpty.Checked)
                {
                    stractive = "1";
                }
                else
                {
                    stractive = "0";
                }

                if (hdfSCID.Value == "0")
                {
                    CategoryDetails[0] = "I";

                    DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
                    if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
                    {
                        gv_Child_Tail.DataSource = dttailtrip;
                        gv_Child_Tail.DataBind();
                        mpetrip.Show();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                        return;
                    }

                    DataTable dttail = new DataTable();

                    //if (gvDepAirport.Rows.Count > 0)
                    //{
                    //    if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
                    //    {
                    //        string strstartcity2 = txtStartCity.Text.Replace(" - ", "-");
                    //        String[] strliststart = strstartcity2.Split('-');

                    //        string strendcity2 = txtEndCity.Text.Replace(" - ", "-");
                    //        String[] strlistend = strendcity2.Split('-');
                    //        if (strliststart.Length > 1 && strlistend.Length > 1)
                    //        {
                    //            dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + strliststart[1] + "' and EndAirport='" + strlistend[1] + "'").Tables[0];
                    //        }
                    //    }
                    //    else
                    //    {
                    //        dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + txtStartCity.Text.Trim() + "' and EndAirport='" + txtEndCity.Text.Trim() + "'").Tables[0];
                    //    }
                    //}




                    if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
                    {
                        lblalert.Text = "Tail No  already exists.";
                        mpeconfirm.Show();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                        return;
                        txtTailNo.Value = string.Empty;
                    }
                }
                else
                {
                    if (LBLTRIPNO.Text != txttrip.Value)
                    {
                        DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
                        if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
                        {
                            gv_Child_Tail.DataSource = dttailtrip;
                            gv_Child_Tail.DataBind();
                            mpetrip.Show();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                            return;
                        }
                        DataTable dttail = new DataTable();

                        //if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
                        //{
                        //    string strstartcity1 = txtStartCity.Text.Replace(" - ", "-");
                        //    String[] strliststart = strstartcity1.Split('-');

                        //    string strendcity1 = txtEndCity.Text.Replace(" - ", "-");
                        //    String[] strlistend = strendcity1.Split('-');
                        //    dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + strliststart[1] + "' and EndAirport='" + strlistend[1] + "'").Tables[0];
                        //}
                        //else
                        //{
                        //    dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + txtStartCity.Text.Trim() + "' and EndAirport='" + txtEndCity.Text.Trim() + "'").Tables[0];
                        //}

                        if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
                        {
                            lblalert.Text = "Tail No  already exists.";
                            mpeconfirm.Show();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                            return;
                        }
                    }

                    CategoryDetails[0] = "U";
                }
                string atctivetail = string.Empty;
                if (chkEmpty.Checked)
                {
                    atctivetail = "1";
                }
                else
                {
                    atctivetail = "0";
                }

                DataTable dtstatrt = new DataTable();
                DataTable dtend = new DataTable();

                DateTime fromDate = new DateTime();
                DateTime toDate = new DateTime();
                string dateTimeNow = string.Empty;


                DateTime DatefromTZ = new DateTime();
                DateTime DatetoTZ = new DateTime();
                DatefromTZ = Convert.ToDateTime(txtAvaFrom.Text + " " + txtStartTime.Value);
                if (dtstatrt.Rows.Count > 0)
                {
                    fromDate = Convert.ToDateTime(DatefromTZ).AddHours(-Convert.ToInt32(dtstatrt.Rows[0]["Timezone"].ToString()));
                }

                DatetoTZ = Convert.ToDateTime(txtAvaTo.Text + " " + txtEndTime.Value);
                if (dtend.Rows.Count > 0)
                {
                    toDate = Convert.ToDateTime(DatetoTZ).AddHours(-Convert.ToInt32(dtend.Rows[0]["Timezone"].ToString()));
                }


                string strstartdate = fromDate.ToString("yyyy-MM-dd");
                string strstarttime = fromDate.ToString("HH:mm");

                string strenddate = toDate.ToString("yyyy-MM-dd");
                string strendtime = toDate.ToString("HH:mm");
                string strstartcity = string.Empty;
                string strendcity = string.Empty;

                if (hdfSCID.Value == "")
                {
                    CategoryDetails[1] = "0";
                }
                else
                {
                    CategoryDetails[1] = hdfSCID.Value;
                }
                CategoryDetails[2] = txtTailNo.Value;
                CategoryDetails[3] = txtTailNo.Value;

                if (gvDepAirport.Rows.Count > 0)
                {
                    string strAirportCode = string.Empty;
                    string strAirportName = string.Empty;

                    string strStateCode = string.Empty;
                    string strStateName = string.Empty;

                    string strCountryCode = string.Empty;
                    string strCountryName = string.Empty;



                    for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                    {
                        Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                        Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                        if (lblSetupBy.Text.Trim() == "Airport")
                        {

                            string ICAO1 = string.Empty;
                            string IATA1 = string.Empty;
                            string City1 = string.Empty;
                            AirportClass.AirportSplit(lblAirport.Text, out ICAO1, out StateCode, out  CountryCode, out City1, out IATA1);


                            strstartcity = lblAirport.Text.Replace(",", "-");
                            String[] strliststart = strstartcity.Trim().Split('-');

                            strAirportCode += ICAO1.Trim() + "/";
                            strAirportName += City1.Trim() + "/";
                        }
                        if (lblSetupBy.Text.Trim() == "State")
                        {
                            string strState = lblAirport.Text;
                            if (strState.Contains(','))
                            {
                                string[] strSplit = strState.Trim().Split(',');

                                if (strSplit.Length > 1)
                                {
                                    strStateName += strSplit[0].ToString().Trim() + "/";

                                    string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                    if (strSTCode.Length > 1)
                                    {
                                        strStateCode += strSTCode[0].ToString().Trim() + "/";
                                    }
                                }
                            }
                        }
                        if (lblSetupBy.Text.Trim() == "Country")
                        {
                            string strCountry = lblAirport.Text;
                            if (strCountry.Contains(','))
                            {
                                string[] strSplit = strCountry.Trim().Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strCountryCode += strSplit[1].ToString().Trim() + "/";
                                    strCountryName += strSplit[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }


                    CategoryDetails[4] = (strCountryCode + strStateCode + strAirportCode).TrimEnd('/');
                    CategoryDetails[5] = (strCountryName + strStateName + strAirportName).TrimEnd('/');

                    CategoryDetails[52] = txtDepDisLandPage.Text.Trim();
                }
                else
                {
                    //CategoryDetails[4] = lblStartData.Text;
                    //CategoryDetails[5] = lblscitydata.Text;

                    //CategoryDetails[52] = lblStartData.Text;


                    string ICAO1 = string.Empty;
                    string IATA1 = string.Empty;
                    string City1 = string.Empty;
                    AirportClass.AirportSplit(lblActualStartICAO.Text, out ICAO1, out StateCode, out  CountryCode, out City1, out IATA1);


                    CategoryDetails[4] = ICAO1;
                    CategoryDetails[5] = City1;
                    CategoryDetails[52] = ICAO1;

                }
                if (gvArrivalAirport.Rows.Count > 0)
                {
                    string strEndAirportCode = string.Empty;
                    string strEndAirportName = string.Empty;

                    string strEndStateCode = string.Empty;
                    string strEndStateName = string.Empty;

                    string strEndCountryCode = string.Empty;
                    string strEndCountryName = string.Empty;
                    for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                    {
                        Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                        Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                        if (lblSetupBy.Text.Trim() == "Airport")
                        {

                            string ICAO1 = string.Empty;
                            string IATA1 = string.Empty;
                            string City1 = string.Empty;
                            AirportClass.AirportSplit(lblAirport.Text, out ICAO1, out StateCode, out  CountryCode, out City1, out IATA1);



                            strstartcity = lblAirport.Text.Replace(",", "-");
                            String[] strliststart = strstartcity.Trim().Split('-');

                            strEndAirportCode += ICAO1.Trim() + "/";
                            strEndAirportName += City1.Trim() + "/";
                        }
                        if (lblSetupBy.Text.Trim() == "State")
                        {
                            string strState = lblAirport.Text;
                            if (strState.Contains(','))
                            {
                                string[] strSplit = strState.Trim().Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strEndStateName += strSplit[0].ToString().Trim() + "/";

                                    string[] strSTCode = strSplit[1].Trim().ToString().Trim().Replace(" ", "").Split('-');

                                    if (strSTCode.Length > 1)
                                    {
                                        strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                    }
                                }
                            }
                        }
                        if (lblSetupBy.Text.Trim() == "Country")
                        {
                            string strCountry = lblAirport.Text;
                            if (strCountry.Contains(','))
                            {
                                string[] strSplit = strCountry.Trim().Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                    strEndCountryName += strSplit[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }

                    CategoryDetails[7] = (strEndCountryCode + strEndStateCode + strEndAirportCode).TrimEnd('/');
                    CategoryDetails[8] = (strEndCountryName + strEndStateName + strEndAirportName).TrimEnd('/');
                    CategoryDetails[53] = txtArrDisLandPage.Text.Trim();
                }
                else
                {
                    string ICAO1 = string.Empty;
                    string IATA1 = string.Empty;
                    string City1 = string.Empty;
                    AirportClass.AirportSplit(lblActualEndICAO.Text, out ICAO1, out StateCode, out  CountryCode, out City1, out IATA1);


                    CategoryDetails[7] = ICAO1.Trim();
                    CategoryDetails[8] = City1.Trim();
                    CategoryDetails[53] = ICAO1.Trim();

                    //CategoryDetails[7] = lblendData.Text.Trim();
                    //CategoryDetails[8] = lblEcityData.Text.Trim();
                    //CategoryDetails[53] = lblendData.Text.Trim();
                }



                CategoryDetails[6] = txtStartTime.Value;
                CategoryDetails[9] = txtEndTime.Value;
                CategoryDetails[10] = "";
                CategoryDetails[11] = txtFlyHours1.Value;
                CategoryDetails[12] = Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd");
                CategoryDetails[13] = Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd");

                CategoryDetails[15] = "DN";
                CategoryDetails[41] = strstartdate;
                CategoryDetails[42] = strenddate;
                CategoryDetails[43] = strstarttime;
                CategoryDetails[44] = strendtime;
                if (hdfSCID.Value == "0")
                {
                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' ").Tables[0];
                    if (dttailTable.Rows.Count > 0)
                    {
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                    }

                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *, isnull(noofseats,0) as noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' and pricingcompleted='1'").Tables[0];
                    if (dttailTableEx.Rows.Count > 0)
                    {
                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[35] = "1";
                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                    }
                    else
                    {
                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                        CategoryDetails[35] = "0";
                        CategoryDetails[33] = "1";
                        CategoryDetails[40] = "0";
                        CategoryDetails[14] = "0";
                    }
                }
                else
                {
                    if (lbloldtail.Text != txtTailNo.Value)
                    {
                        DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + lbloldtail.Text + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
                        if (dttail.Rows.Count > 0)
                        {
                            CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
                            CategoryDetails[35] = "1";
                            CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
                        }
                        else
                        {
                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                            CategoryDetails[35] = "0";
                            CategoryDetails[33] = "1";
                        }
                    }
                    else
                    {
                        DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + txtTailNo.Value + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
                        if (dttail.Rows.Count > 0)
                        {
                            CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
                            CategoryDetails[35] = "1";
                            CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
                        }
                        else
                        {
                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                            CategoryDetails[35] = "0";
                            CategoryDetails[33] = "1";
                        }
                    }
                }

                CategoryDetails[16] = "0.00";
                CategoryDetails[16] = "0.00";
                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                if (chkEmpty.Checked)
                {
                    CategoryDetails[19] = "1";
                }
                else
                {
                    CategoryDetails[19] = "0";
                }
                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                TimeSpan ts = TimeSpan.Parse(s);
                CategoryDetails[31] = ts.ToString();
                CategoryDetails[32] = DateTime.Now.Date.ToString();
                if (hdfSCID.Value == "0")
                {
                    CategoryDetails[34] = txttrip.Value;
                }
                else
                {
                    CategoryDetails[34] = txttrip.Value;
                }
                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                CategoryDetails[37] = DateTime.Now.Date.ToString();
                CategoryDetails[38] = LBLTRIPNO.Text;
                CategoryDetails[39] = lbloldtail.Text;

                CategoryDetails[48] = txtMPrice.Value.Trim();

                if (lblgroup.Text == "Modify")
                {
                    CategoryDetails[49] = "MF";
                    CategoryDetails[0] = "CH";
                    if (lbllegdisplay.Text.Trim() == "" || lbllegdisplay.Text.Trim() == "0")
                    {
                        CategoryDetails[51] = "1";
                    }
                    else
                    {
                        CategoryDetails[51] = lbllegdisplay.Text.Trim();
                    }

                    if (gvArrivalAirport.Rows.Count > 0 || gvDepAirport.Rows.Count > 0)
                    {

                        if (lblModGroupID.Text == "0")
                        {
                            DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from EmptyLegAirport_Child").Tables[0];
                            int ModifyGroupId = 1;
                            if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
                            {
                                if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
                                {
                                    ModifyGroupId = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
                                }
                                else
                                {
                                    ModifyGroupId = 1;
                                }
                            }

                            CategoryDetails[50] = ModifyGroupId.ToString();
                        }
                        else
                        {
                            CategoryDetails[50] = lblModGroupID.Text;
                        }
                    }
                    else
                    {
                        CategoryDetails[50] = "0";
                    }


                }
                else
                {
                    CategoryDetails[49] = "ET";
                    CategoryDetails[51] = "1";
                    //Newly Empty Leg Added


                }

                CategoryDetails[54] = rblLanding.SelectedValue;

                string strReturnVal = saveCategory(CategoryDetails);

                if (strReturnVal != "0")
                {
                    lblalert.Text = "Empty leg details have been saved successfully";
                    mpealert.Show();
                    if (hdfSCID.Value == "0")
                    {
                        lblManEditGroupId.Text = strReturnVal;
                    }
                    else if (hdfSCID.Value != "0" && lblgroup.Text == "Modify")
                    {
                        lblManEditGroupId.Text = strReturnVal;
                    }
                    string strModFlag = string.Empty;
                    if (lblgroup.Text == "Modify")
                    {
                        strModFlag = "MF";
                    }
                    else
                    {
                        strModFlag = "ET";
                    }

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");


                    if (gvDepAirport.Rows.Count > 0)
                    {

                        for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                        {
                            string strCount = "0";
                            Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                            Label lblAir = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");
                            Label lblCreatedBy = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedBy");
                            Label lblCreatedOn = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedOn");

                            Label lblStateCode = (Label)gvDepAirport.Rows[i].FindControl("lblStateCode");
                            Label lblCountryCode = (Label)gvDepAirport.Rows[i].FindControl("lblCountryCode");
                            Label lblAirportCode = (Label)gvDepAirport.Rows[i].FindControl("lblAirportCode");
                            Label lblStateID = (Label)gvDepAirport.Rows[i].FindControl("lblStateId");


                            if (lblSetupBy.Text == "Airport")
                            {
                                strCount = objEarliestTimeDAL.INTExist(lblAirportCode.Text.Trim(), "AIRT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strDepartTrip = "I";
                                }
                            }
                            else if (lblSetupBy.Text == "Country")
                            {
                                strCount = objEarliestTimeDAL.INTExist(lblCountryCode.Text.Trim(), "CONT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strDepartTrip = "I";
                                }
                            }
                            else
                            {
                                //strCount = objEarliestTimeDAL.INTExist(lblStateCode.Text.Trim(), "STAT");
                                strCount = objEarliestTimeDAL.INTExist(lblStateID.Text.Trim(), "STAT");
                                if (Convert.ToInt32(strCount) == 0)
                                {
                                    strDepartTrip = "I";
                                }
                            }

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,StateID) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Dep', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text.Trim() + "','" + lblCountryCode.Text.Trim() + "','" + lblAirportCode.Text.Trim() + "','" + lblStateID.Text.Trim() + "')");
                        }
                    }

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");

                    if (gvArrivalAirport.Rows.Count > 0)
                    {

                        for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                        {

                            string strCount = "0";
                            Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                            Label lblAir = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");
                            Label lblCreatedBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedBy");
                            Label lblCreatedOn = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedOn");

                            Label lblStateCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateCode");
                            Label lblCountryCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblCountryCode");
                            Label lblAirportCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblAirportCode");
                            Label lblStateID = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateId");



                            if (lblSetupBy.Text == "Airport")
                            {
                                strCount = objEarliestTimeDAL.INTExist(lblAirportCode.Text.Trim(), "AIRT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strArrTrip = "I";
                                }
                            }
                            else if (lblSetupBy.Text == "Country")
                            {
                                strCount = objEarliestTimeDAL.INTExist(lblCountryCode.Text.Trim(), "CONT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strArrTrip = "I";
                                }
                            }
                            else
                            {
                                // strCount = objEarliestTimeDAL.INTExist(lblStateCode.Text.Trim(), "STAT");

                                strCount = objEarliestTimeDAL.INTExist(lblStateID.Text.Trim(), "STAT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strArrTrip = "I";
                                }
                            }

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,StateID) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Arr', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text.Trim() + "','" + lblCountryCode.Text.Trim() + "','" + lblAirportCode.Text.Trim() + "','" + lblStateID.Text.Trim() + "')");

                        }
                    }

                }
                if (hdfSCID.Value != "0")
                {
                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set Active='" + stractive + " ',Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "'  and tripno='" + LBLTRIPNO.Text + "'");
                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update bookeddetails set Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "' and tripno  ='" + LBLTRIPNO.Text + "'  ");
                    DataTable dtPScheck = new DataTable();
                    if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
                    {
                        string strstartcity1 = txtStartCity.Text.Replace(" - ", "-");
                        String[] strliststart = strstartcity1.Trim().Split('-');

                        string strendcity1 = txtEndCity.Text.Replace(" - ", "-");
                        String[] strlistend = strendcity1.Trim().Split('-');
                        dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "' and Flag='FINAL'").Tables[0];
                        if (dtPScheck.Rows.Count > 0)
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "'  and FLAG='FINAL'");
                        }
                    }

                    else
                    {
                        dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "' and Flag='FINAL'").Tables[0];

                        if (dtPScheck.Rows.Count > 0)
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "'  and FLAG='FINAL'");
                        }
                    }

                }
            }

            #endregion

            #region Split Insert

            else
            {
                #region get Parent ID
                string strParentFlag = string.Empty;
                string strParentID = string.Empty;

                strParentFlag = lblEModFlag.Text;
                strParentID = lblModGroupID.Text;

                DataTable dtParent = genclass.GetDataTable("select ParentID,ParentFlag from EmptyLegAirport_Child where groupID='" + strParentID + "' and ModifiedFlag='SP'");
                if (dtParent.Rows.Count > 0)
                {
                    if (dtParent.Rows[0]["ParentID"].ToString() != string.Empty)
                    {
                        strParentFlag = dtParent.Rows[0]["ParentFlag"].ToString();
                        strParentID = dtParent.Rows[0]["ParentID"].ToString();
                    }
                }


                #endregion

                #region Check earliest depart time
                string strAlert = string.Empty;


                if (lblEarliestTime.Text != string.Empty)
                {
                    DateTime dtStartTime = Convert.ToDateTime(txtSplitAvaFrom.Text);
                    if (Convert.ToDateTime(lblEarliestTime.Text).Date > dtStartTime.Date)
                    {
                        strAlert = "Please ensure that the Available from is greater than or equal to the Earliest Departure Time.</br>";

                    }
                }


                if (lblMaxDepartTime.Text != string.Empty)
                {
                    DateTime dtEndTime = Convert.ToDateTime(txtSplitAvaTo.Text);
                    if (Convert.ToDateTime(lblMaxDepartTime.Text).Date < dtEndTime.Date)
                    {
                        strAlert = strAlert + "Please ensure that the Available To is less than or equal to the Latest Departure Time.</br>";

                    }
                }




                if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
                {
                    if (lblStartData.Text.Length == 4)
                    {
                        if (((Label)gvLegDetails.Rows[0].FindControl("lblstartair")).Text != lblStartData.Text)
                        {
                            strAlert = strAlert + "Please ensure that the Start Airport matches the actual Start Airport [" + lblStartData.Text + "] of the trip.</br>";
                        }
                    }
                    if (lblendData.Text.Length == 4)
                    {
                        if (((Label)gvLegDetails.Rows[gvLegDetails.Rows.Count - 1].FindControl("lblendair")).Text != lblendData.Text)
                        {
                            strAlert = strAlert + "Please ensure that the End Airport matches the actual End Airport   [" + lblendData.Text + "]  of the trip.</br>";
                        }
                    }

                }


                if (strAlert != string.Empty)
                {
                    lblalert.Text = strAlert;
                    mpealert.Show();
                    return;
                }

                #endregion

                #region Validations

                string strBusinessAlert = string.Empty;
                if (rblBreak.SelectedValue.Trim() == "CO")
                {
                    if (txtcountry.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Start county is required. <br/>";
                    }
                    if (ddlLeg1EndCity.Text.ToString() == "")
                    {
                        strBusinessAlert += "- End city is required. <br/>";
                    }
                    if (txtCouStartTime.Value.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Start Time is required. <br/>";
                    }
                    if (txtSplitLeg1EndTime.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- End Time is required. <br/>";
                    }
                    if (txtSpliFlyHour.Value.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Flying Hour is required. <br/>";
                    }
                    if (txtSpliFlyHour.Value.ToString().Length > 0)
                    {
                        if (Convert.ToDouble(txtSpliFlyHour.Value) < 0.7)
                        {
                            strBusinessAlert += "The Selected route is not allowed for booking. Please modify your request. <br/>";
                        }
                    }
                }
                else if (rblBreak.SelectedValue.Trim() == "CT")
                {
                    if (gvLegDetails.Rows.Count == 0)
                    {
                        strBusinessAlert = "Please add leg";
                    }
                }
                if (strBusinessAlert.Length > 0)
                {
                    lblalert.Text = strBusinessAlert;
                    mpealert.Show();
                    return;
                }

                #endregion

                #region Split New Insert

                if (btnSave.Text == "Save")
                {
                    int groupid = 1;
                    decimal daystoflight = 0; decimal L10_minPrice = 0; decimal M10_maxPrice = 0; decimal price = 0;

                    var HourlyRate = Convert.ToDecimal("0.00");
                    var fixedCeiling = Convert.ToDecimal("0.00");
                    var fixedFloor = Convert.ToDecimal("0.00");
                    var D4_MinTimeBD = Convert.ToDecimal("0.0");
                    var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                    var MinFLHSellable = Convert.ToDecimal("0.00");
                    var finalPricevalidation2 = (decimal)0;
                    var _totalDaysK101 = "0";

                    string stractive = string.Empty;
                    stractive = "1";

                    if (hdfSCID.Value == "0")
                    {
                        CategoryDetails[0] = "I";
                    }
                    else
                    {
                        CategoryDetails[0] = "U";
                    }
                    string atctivetail = string.Empty;

                    atctivetail = "1";

                    //if (lblModGroupID.Text == "0")
                    //{

                    DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from Tailsetup_tripDetails").Tables[0];

                    if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
                    {
                        if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
                        {
                            groupid = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
                        }
                        else
                        {
                            groupid = 1;
                        }
                    }

                    //}
                    //else
                    //{
                    //    groupid = Convert.ToInt32(lblModGroupID.Text);
                    //}

                    #region Country INert

                    if (rblBreak.SelectedValue.Trim() == "CO")
                    {
                        //SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CO',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text).ToString("yyyy-MM-dd") + "' and leg=" + lbllegdisplay.Text + "  ");
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CO',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and TripFlag='B0' and leg=" + lbllegdisplay.Text + "  ");

                        if (lblbaseflag.Text.Trim() != "B0")
                        {
                            var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + txtCouStartTime.Value.Trim() + ":00");
                            var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                            var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                            var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value + ":00").Subtract(DateTime.Now);
                            _totalDaysK101 = K10_totalDays1.TotalDays.ToString("0.00");

                            string strstartcity = string.Empty;
                            strstartcity = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
                            String[] strlistend = strstartcity.Trim().Split('-');
                            string startAirport = string.Empty;
                            string startcity = string.Empty;
                            if (strlistend.Length > 1)
                            {
                                startAirport = strlistend[0].Trim();
                                startcity = strlistend[1].Trim();
                            }

                            CategoryDetails[1] = hdfSCID.Value;
                            CategoryDetails[2] = txtTailNo.Value.Trim();
                            CategoryDetails[3] = txtTailNo.Value.Trim();
                            CategoryDetails[4] = txtcountry.Text;
                            CategoryDetails[5] = "";

                            CategoryDetails[6] = txtCouStartTime.Value.Trim();

                            CategoryDetails[7] = startAirport;
                            CategoryDetails[8] = startcity;
                            CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                            CategoryDetails[10] = "";
                            CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                            CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
                            CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
                            CategoryDetails[15] = "";
                            CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                            CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                            CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                            CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

                            if (hdfSCID.Value == "0")
                            {
                                DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                if (dttailTable.Rows.Count > 0)
                                {

                                }
                                else
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                }

                                DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                if (dttailTableEx.Rows.Count > 0)
                                {
                                    CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                    CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                    CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                    CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                    CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                    CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                    CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
                                    CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[35] = "1";
                                    CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                    CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                    CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                }
                                else
                                {
                                    CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                    CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                    CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                    CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                    CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                    CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                    CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
                                    CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                    CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                    CategoryDetails[35] = "0";
                                    CategoryDetails[33] = "1";
                                    CategoryDetails[40] = "0";
                                    CategoryDetails[14] = "0";
                                }
                            }
                            decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
                            CategoryDetails[16] = "0.00";
                            CategoryDetails[16] = "0.00";
                            CategoryDetails[30] = totalflyingHours.ToString("0.00");
                            if (cbSplitEmpty.Checked)
                            {
                                CategoryDetails[19] = "1";
                            }
                            else
                            {
                                CategoryDetails[19] = "0";
                            }
                            string s1 = DateTime.Now.ToString("HH:mm:ss.fff");

                            TimeSpan ts1 = TimeSpan.Parse(s1);
                            CategoryDetails[31] = ts1.ToString();
                            CategoryDetails[32] = DateTime.Now.Date.ToString();
                            if (hdfSCID.Value == "0")
                            {
                                CategoryDetails[34] = txttrip.Value.Trim();
                            }
                            else
                            {
                                CategoryDetails[34] = txttrip.Value.Trim();
                            }
                            CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                            CategoryDetails[37] = DateTime.Now.Date.ToString();
                            CategoryDetails[38] = txttrip.Value.Trim();
                            CategoryDetails[39] = txtTailNo.Value.Trim();
                            CategoryDetails[45] = "B1";
                            CategoryDetails[46] = groupid.ToString();
                            CategoryDetails[47] = lbllegdisplay.Text;
                            CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                            if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
                            {
                                CategoryDetails[49] = lblStartData.Text.Trim();
                                CategoryDetails[50] = lblendData.Text.Trim();
                            }
                            else
                            {
                                CategoryDetails[49] = lblStartData.Text.Trim();
                                CategoryDetails[50] = lblendData.Text.Trim();
                            }

                            string strReturnVal1 = saveCategoryGroup(CategoryDetails);
                        }
                        else
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");
                        }


                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

                        string strCountryCode = string.Empty;

                        if (txtcountry.Text.Contains("/"))
                        {
                            string[] strSplitCountry = txtcountry.Text.Trim().Split('/');

                            if (strSplitCountry.Length > 1)
                            {
                                for (int i = 0; i < strSplitCountry.Length; i++)
                                {
                                    DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + strSplitCountry[i].ToString().Trim() + "' ").Tables[0];

                                    if (detr.Rows.Count > 0)
                                    {
                                        strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                                    }

                                    string strCount = objEarliestTimeDAL.INTExist(strCountryCode.Trim(), "CONT");
                                    if (Convert.ToInt32(strCount) > 0)
                                    {
                                        strDepartTrip = "I";
                                    }




                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Country', '" + strSplitCountry[i].ToString().Replace("'", "''") + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                                }
                            }
                        }
                        else
                        {
                            DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + txtcountry.Text.Trim() + "' ").Tables[0];

                            if (detr.Rows.Count > 0)
                            {
                                strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                            }

                            string strCount = objEarliestTimeDAL.INTExist(strCountryCode.Trim(), "CONT");
                            if (Convert.ToInt32(strCount) > 0)
                            {
                                strDepartTrip = "I";
                            }
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Country', '" + txtcountry.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

                        string strstartcity1 = string.Empty;
                        strstartcity1 = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
                        String[] strlistend1 = strstartcity1.Trim().Split('-');
                        string startAirport1 = string.Empty;
                        string startcity1 = string.Empty;
                        if (strlistend1.Length > 1)
                        {
                            startAirport1 = strlistend1[0].Trim();
                            startcity1 = strlistend1[1].Trim();
                        }

                        string strCount1 = objEarliestTimeDAL.INTExist(startAirport1.Trim(), "CONT");
                        if (Convert.ToInt32(strCount1) > 0)
                        {
                            strArrTrip = "I";
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Arr', 'Airport', '" + ddlLeg1EndCity.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + startAirport1.Trim() + "')");


                    }

                    #endregion

                    #region City Insert

                    if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
                    {
                        //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text).ToString("yyyy-MM-dd") + "' and leg=" + lbllegdisplay.Text + "  ");

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and TripFlag='B0' and leg=" + lbllegdisplay.Text + "  ");

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

                        for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                        {
                            if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
                            {
                                string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString();
                                var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + strStartTime.Trim() + ":00");
                                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                                var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                K10_totalDays1.TotalDays.ToString("0.00");

                                #region Leg 2 Insert Breakdown by City

                                var K10_totalDays2 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                var _totalDaysK102 = K10_totalDays2.TotalDays.ToString("0.00");

                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
                                CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
                                CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {

                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }

                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString()).ToString("0.00");
                                if (chkEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s2 = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts2 = TimeSpan.Parse(s2);
                                CategoryDetails[31] = ts2.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString();
                                CategoryDetails[46] = groupid.ToString();
                                CategoryDetails[47] = lbllegdisplay.Text;
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
                                {
                                    CategoryDetails[49] = lblStartData.Text.Trim();
                                    CategoryDetails[50] = lblendData.Text.Trim();
                                }
                                else
                                {
                                    CategoryDetails[49] = lblStartData.Text.Trim();
                                    CategoryDetails[50] = lblendData.Text.Trim();
                                }
                                string strReturnVal2 = saveCategoryGroup(CategoryDetails);

                                #endregion
                            }
                            if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
                            {
                                string strCount = "0";
                                string strStartAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text.Trim();
                                string strStartCity = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text.Trim();

                                string strEndAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text.Trim();
                                string strEndCity = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text.Trim();

                                strCount = objEarliestTimeDAL.INTExist(strStartAirport.Trim(), "AIRT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strDepartTrip = "I";
                                }

                                strCount = objEarliestTimeDAL.INTExist(strEndAirport.Trim(), "AIRT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strArrTrip = "I";
                                }

                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Airport', '" + strStartCity.Trim() + " - " + strStartAirport.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strStartAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");

                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + groupid.ToString().Trim() + "','SP','Arr', 'Airport', '" + strEndCity.Trim() + " - " + strEndAirport.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strEndAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");
                            }
                        }
                    }

                    #endregion
                }

                #endregion

                #region  Update Split Trip

                else
                {


                    #region Check earliest depart time


                    if (lblEarliestTime.Text != string.Empty)
                    {
                        DateTime dtStartTime = Convert.ToDateTime(txtSplitAvaFrom.Text);
                        if (Convert.ToDateTime(lblEarliestTime.Text).Date > dtStartTime.Date)
                        {
                            strAlert = "Please ensure that the Available from is greater than or equal to the Earliest Departure Time.</br>";

                        }
                    }


                    if (lblMaxDepartTime.Text != string.Empty)
                    {
                        DateTime dtEndTime = Convert.ToDateTime(txtSplitAvaTo.Text);
                        if (Convert.ToDateTime(lblMaxDepartTime.Text).Date < dtEndTime.Date)
                        {
                            strAlert = strAlert + "Please ensure that the Available To is less than or equal to the Latest Departure Time.</br>";

                        }
                    }




                    if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
                    {
                        if (lblStartData.Text.Length == 4)
                        {
                            if (((Label)gvLegDetails.Rows[0].FindControl("lblstartair")).Text != lblStartData.Text)
                            {
                                strAlert = strAlert + "Please ensure that the Start Airport same as splited start Aiport.</br>";
                            }
                        }
                        if (lblendData.Text.Length == 4)
                        {
                            if (((Label)gvLegDetails.Rows[gvLegDetails.Rows.Count - 1].FindControl("lblendair")).Text != lblendData.Text)
                            {
                                strAlert = strAlert + "Please ensure that the End Airport same as splited start Aiport.</br>";
                            }
                        }

                    }


                    if (strAlert != string.Empty)
                    {
                        lblalert.Text = strAlert;
                        mpealert.Show();
                        return;
                    }

                    #endregion



                    // DataTable dtOutput = (DataTable)ViewState["Tripgrid"];

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + lblgroupiddisplay.Text + "' and Tripflag<>'B0' ");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + lblgroupiddisplay.Text + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and TripFlag='B0' and leg=" + lbllegdisplay.Text + "  ");


                    //  SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + lblgroupiddisplay.Text + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "'  and leg=" + lbllegdisplay.Text + "  ");

                    if (lblbaseflag.Text != "B0")
                    {
                        if (rblBreak.SelectedValue.Trim() == "CO")
                        {
                            #region Breakdown by Country

                            DataTable dtexist = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='B1' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

                            if (dtexist.Rows.Count > 0)
                            {
                                decimal daystoflight = 0;
                                decimal L10_minPrice = 0;
                                decimal M10_maxPrice = 0;
                                decimal price = 0;

                                decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
                                var HourlyRate = Convert.ToDecimal("0.00"); ;
                                var fixedCeiling = Convert.ToDecimal("0.00");
                                var fixedFloor = Convert.ToDecimal("0.00");
                                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                var MinFLHSellable = Convert.ToDecimal("0.00");
                                var finalPricevalidation2 = (decimal)0;
                                var _totalDaysK10 = "0";

                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }
                                else
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }


                                string stractive = string.Empty;
                                stractive = "1";
                                CategoryDetails[0] = "U";
                                string atctivetail = string.Empty;
                                atctivetail = "1";

                                string strEndCity = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
                                String[] strlistend = strEndCity.Trim().Split('-');
                                string endAirport = string.Empty;
                                string endCity = string.Empty;
                                if (strlistend.Length > 1)
                                {
                                    endAirport = strlistend[0].Trim();
                                    endCity = strlistend[1].Trim();
                                }
                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                if (rblBreak.SelectedValue.Trim() == "CT")
                                {
                                    string strstartcity1 = string.Empty;
                                    strstartcity1 = ddlStartCitySplit.Text.Replace(",", "-").Replace(" ", "");
                                    String[] strliststart1 = strstartcity1.Trim().Split('-');
                                    string startAirport1 = string.Empty;
                                    string startcity1 = string.Empty;
                                    if (strliststart1.Length > 1)
                                    {
                                        startAirport1 = strliststart1[0].Trim();
                                        startcity1 = strliststart1[1].Trim();

                                        CategoryDetails[4] = startAirport1;
                                        CategoryDetails[5] = startcity1;
                                    }
                                    else
                                    {

                                        CategoryDetails[4] = ddlStartCitySplit.Text;
                                        CategoryDetails[5] = "";
                                    }


                                }
                                else
                                {
                                    CategoryDetails[4] = txtcountry.Text;
                                    CategoryDetails[5] = "";
                                }
                                if (rblBreak.SelectedValue == "CO")
                                {
                                    CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
                                }

                                // CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                CategoryDetails[7] = endAirport;
                                CategoryDetails[8] = endCity;
                                CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                                CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
                                CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
                                //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                if (cbSplitEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts = TimeSpan.Parse(s);
                                CategoryDetails[31] = ts.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = "B1";
                                CategoryDetails[46] = lblgroupiddisplay.Text;
                                CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
                                {
                                    CategoryDetails[49] = lblStartData.Text.Trim();
                                    CategoryDetails[50] = lblendData.Text.Trim();
                                }
                                else
                                {
                                    CategoryDetails[49] = lblStartData.Text.Trim();
                                    CategoryDetails[50] = lblendData.Text.Trim();
                                }

                                string strReturnVal = saveCategoryGroup(CategoryDetails);



                            }
                            else
                            {
                                decimal daystoflight = 0;
                                decimal L10_minPrice = 0;
                                decimal M10_maxPrice = 0;
                                decimal price = 0;
                                decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());

                                var HourlyRate = Convert.ToDecimal("0.00"); ;
                                var fixedCeiling = Convert.ToDecimal("0.00");
                                var fixedFloor = Convert.ToDecimal("0.00");
                                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                var MinFLHSellable = Convert.ToDecimal("0.00");
                                var finalPricevalidation2 = (decimal)0;
                                var _totalDaysK10 = "0";
                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }
                                else
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }


                                string stractive = string.Empty;

                                stractive = "1";

                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[0] = "I";
                                }
                                else
                                {
                                    CategoryDetails[0] = "U";
                                }
                                string atctivetail = string.Empty;

                                atctivetail = "1";
                                string strEndCity = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
                                String[] strlistend = strEndCity.Trim().Split('-');
                                string endAirport = string.Empty;
                                string endCity = string.Empty;
                                if (strlistend.Length > 1)
                                {
                                    endAirport = strlistend[0].Trim();
                                    endCity = strlistend[1].Trim();
                                }

                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                CategoryDetails[4] = txtcountry.Text.Trim();
                                CategoryDetails[5] = "";
                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
                                }
                                CategoryDetails[7] = endAirport.Trim();
                                CategoryDetails[8] = endCity.Trim();
                                CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                                CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
                                CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
                                //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }

                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                if (cbSplitEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts = TimeSpan.Parse(s);
                                CategoryDetails[31] = ts.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = "B1";
                                CategoryDetails[46] = lblgroupiddisplay.Text;
                                CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
                                {
                                    CategoryDetails[49] = lblStartData.Text.Trim();
                                    CategoryDetails[50] = lblendData.Text.Trim();
                                }
                                else
                                {
                                    CategoryDetails[49] = "";
                                    CategoryDetails[50] = "";
                                }
                                string strReturnVal = saveCategoryGroup(CategoryDetails);
                            }


                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.ToString().Trim() + "'");

                            string strCountryCode = string.Empty;

                            if (txtcountry.Text.Contains("/"))
                            {
                                string[] strSplitCountry = txtcountry.Text.Trim().Split('/');

                                if (strSplitCountry.Length > 1)
                                {

                                    for (int i = 0; i < strSplitCountry.Length; i++)
                                    {
                                        DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + strSplitCountry[i].ToString().Trim() + "' ").Tables[0];

                                        if (detr.Rows.Count > 0)
                                        {
                                            strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                                        }
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Dep', 'Country', '" + strSplitCountry[i].ToString().Replace("'", "''") + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                                    }
                                }
                            }
                            else
                            {
                                DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + txtcountry.Text.Trim() + "' ").Tables[0];

                                if (detr.Rows.Count > 0)
                                {
                                    strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                                }

                                string strCount = objEarliestTimeDAL.INTExist(strCountryCode.Trim(), "CONT");
                                if (Convert.ToInt32(strCount) > 0)
                                {
                                    strDepartTrip = "I";
                                }



                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Dep', 'Country', '" + txtcountry.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                            }

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.ToString().Trim() + "'");

                            string strstartcity2 = string.Empty;
                            strstartcity2 = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
                            String[] strlistend2 = strstartcity2.Trim().Split('-');
                            string startAirport2 = string.Empty;
                            string startcity2 = string.Empty;
                            if (strlistend2.Length > 1)
                            {
                                startAirport2 = strlistend2[0].Trim();
                                startcity2 = strlistend2[1].Trim();
                            }

                            string strCount1 = objEarliestTimeDAL.INTExist(startAirport2.Trim(), "AIRT");
                            if (Convert.ToInt32(strCount1) > 0)
                            {
                                strArrTrip = "I";
                            }

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Arr', 'Airport', '" + ddlLeg1EndCity.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + startAirport2.Trim() + "')");

                            #endregion
                        }

                        #region Breakdown by City

                        else if (rblBreak.SelectedValue.Trim() == "CT")
                        {
                            if (gvLegDetails.Rows.Count > 0)
                            {
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE  ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.Trim() + "'");

                                for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                                {
                                    string strTriplag = gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim();

                                    DataTable dtexist1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='" + strTriplag + "' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

                                    #region Leg2

                                    if (dtexist1.Rows.Count > 0)
                                    {
                                        decimal daystoflight = 0;
                                        decimal L10_minPrice = 0;
                                        decimal M10_maxPrice = 0;
                                        decimal price = 0;

                                        decimal totalflyingHours = Convert.ToDecimal(gvLegDetails.Rows[i].FindControl("lblFlyHours").ToString().Trim());
                                        var HourlyRate = Convert.ToDecimal("0.00"); ;
                                        var fixedCeiling = Convert.ToDecimal("0.00");
                                        var fixedFloor = Convert.ToDecimal("0.00");
                                        var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                        var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                        var MinFLHSellable = Convert.ToDecimal("0.00");
                                        var finalPricevalidation2 = (decimal)0;
                                        string strStartTime = gvLegDetails.Rows[i].FindControl("lblstarttime").ToString().Trim();
                                        var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
                                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                                        string stractive = string.Empty;
                                        stractive = "1";
                                        CategoryDetails[0] = "U";
                                        string atctivetail = string.Empty;
                                        atctivetail = "1";

                                        CategoryDetails[1] = hdfSCID.Value;
                                        CategoryDetails[2] = txtTailNo.Value.Trim();
                                        CategoryDetails[3] = txtTailNo.Value.Trim();
                                        CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                        CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                        CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                        CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                        CategoryDetails[10] = "";
                                        CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
                                        CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
                                        CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
                                        CategoryDetails[15] = "";
                                        CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                        CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                        if (hdfSCID.Value == "0")
                                        {
                                            DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                            if (dttailTable.Rows.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                            }

                                            DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                            if (dttailTableEx.Rows.Count > 0)
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[35] = "1";
                                                CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                                CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                                CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                            }
                                            else
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                                CategoryDetails[35] = "0";
                                                CategoryDetails[33] = "1";
                                                CategoryDetails[40] = "0";
                                                CategoryDetails[14] = "0";
                                            }
                                        }
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                        if (cbSplitEmpty.Checked)
                                        {
                                            CategoryDetails[19] = "1";
                                        }
                                        else
                                        {
                                            CategoryDetails[19] = "0";
                                        }
                                        string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                        TimeSpan ts = TimeSpan.Parse(s);
                                        CategoryDetails[31] = ts.ToString();
                                        CategoryDetails[32] = DateTime.Now.Date.ToString();
                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                        CategoryDetails[37] = DateTime.Now.Date.ToString();
                                        CategoryDetails[38] = txttrip.Value.Trim();
                                        CategoryDetails[39] = txtTailNo.Value.Trim();
                                        CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
                                        CategoryDetails[46] = lblgroupiddisplay.Text;
                                        CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                        CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                        if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
                                        {
                                            CategoryDetails[49] = lblStartData.Text.Trim();
                                            CategoryDetails[50] = lblendData.Text.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[49] = lblStartData.Text.Trim();
                                            CategoryDetails[50] = lblendData.Text.Trim();
                                        }
                                        string strReturnVal = saveCategoryGroup(CategoryDetails);
                                    }
                                    else
                                    {
                                        decimal daystoflight = 0;
                                        decimal L10_minPrice = 0;
                                        decimal M10_maxPrice = 0;
                                        decimal price = 0;
                                        decimal totalflyingHours = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim());

                                        var HourlyRate = Convert.ToDecimal("0.00"); ;
                                        var fixedCeiling = Convert.ToDecimal("0.00");
                                        var fixedFloor = Convert.ToDecimal("0.00");
                                        var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                        var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                        var MinFLHSellable = Convert.ToDecimal("0.00");
                                        var finalPricevalidation2 = (decimal)0;
                                        string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString().Trim();

                                        var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
                                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");


                                        string stractive = string.Empty;

                                        stractive = "1";

                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[0] = "I";
                                        }
                                        else
                                        {
                                            CategoryDetails[0] = "U";
                                        }
                                        string atctivetail = string.Empty;

                                        atctivetail = "1";
                                        CategoryDetails[1] = hdfSCID.Value;
                                        CategoryDetails[2] = txtTailNo.Value.Trim();
                                        CategoryDetails[3] = txtTailNo.Value.Trim();
                                        CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                        CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                        CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                        CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                        CategoryDetails[10] = "";
                                        CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim();
                                        CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
                                        CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
                                        CategoryDetails[15] = "";
                                        CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                        CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                        if (hdfSCID.Value == "0")
                                        {
                                            DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                            if (dttailTable.Rows.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                            }

                                            DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                            if (dttailTableEx.Rows.Count > 0)
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[35] = "1";
                                                CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                                CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                                CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                            }
                                            else
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                                CategoryDetails[35] = "0";
                                                CategoryDetails[33] = "1";
                                                CategoryDetails[40] = "0";
                                                CategoryDetails[14] = "0";
                                            }
                                        }

                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                        if (chkEmpty.Checked)
                                        {
                                            CategoryDetails[19] = "1";
                                        }
                                        else
                                        {
                                            CategoryDetails[19] = "0";
                                        }
                                        string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                        TimeSpan ts = TimeSpan.Parse(s);
                                        CategoryDetails[31] = ts.ToString();
                                        CategoryDetails[32] = DateTime.Now.Date.ToString();
                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                        CategoryDetails[37] = DateTime.Now.Date.ToString();
                                        CategoryDetails[38] = txttrip.Value.Trim();
                                        CategoryDetails[39] = txtTailNo.Value.Trim();
                                        CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
                                        CategoryDetails[46] = lblgroupiddisplay.Text;
                                        CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                        CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                        if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
                                        {
                                            CategoryDetails[49] = lblStartData.Text.Trim();
                                            CategoryDetails[50] = lblendData.Text.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[49] = lblStartData.Text.Trim();
                                            CategoryDetails[50] = lblendData.Text.Trim();
                                        }
                                        string strReturnVal = saveCategoryGroup(CategoryDetails);
                                    }

                                    #endregion


                                    if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
                                    {
                                        string strCount = "0";
                                        string strStartAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        string strStartCity = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;

                                        string strEndAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        string strEndCity = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;

                                        strCount = objEarliestTimeDAL.INTExist(strStartAirport.Trim(), "AIRT");
                                        if (Convert.ToInt32(strCount) > 0)
                                        {
                                            strDepartTrip = "I";
                                        }

                                        strCount = objEarliestTimeDAL.INTExist(strEndAirport.Trim(), "AIRT");
                                        if (Convert.ToInt32(strCount) > 0)
                                        {
                                            strArrTrip = "I";
                                        }

                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + lblgroupiddisplay.Text.Trim() + "','SP','Dep', 'Airport', '" + strStartCity + " - " + strStartAirport + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strStartAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");

                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + lblgroupiddisplay.Text.Trim() + "','SP','Arr', 'Airport', '" + strEndCity + " - " + strEndAirport + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strEndAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");

                                    }
                                }
                            }
                        }

                        #endregion
                    }
                }

                #endregion
            }

            #endregion

            #region LivepriceCalculation

            objEmptyLegDataUpdateCal.FunEmptyPriceCal(dtOneWayTrip);

            #endregion

            if (lblgroup.Text == string.Empty)
            {
                if (lblETSU.Text == "I")
                {
                    //if (txtMPrice.End)
                    //objEmptyLegPriceCal.PriceUpdate(txttrip.Value, "1", Convert.ToInt32(txtMPrice.Value));
                    objEmptyLegPriceCal.PriceUpdate(txttrip.Value, "1", Convert.ToInt32(txtMPrice.Value), Convert.ToInt32(txtMPrice.Value).ToString(), Request.Cookies["JETRIPSADMIN"]["UserId"].ToString(), "");
                }

            }


            lblalert.Text = "Empty leg details have been saved successfully";

            if (strDepartTrip == "I" && strArrTrip == "I")
            {
                lblalert.Text = "Empty leg details have been saved successfully.<br/> These trip not allowed for booking.";
            }
            mpealert.Show();
            hdfSCID.Value = "0";
            BindEmptyLegs();
            tblForm.Visible = false;
            tblGrid.Visible = true;
            rblStatus.Visible = true;
            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
        }
        catch (Exception ex)
        {

            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + " " + linenum + "');", true);
        }
    }

    //protected void btnSaveOnClick(object sender, EventArgs e)
    //{
    //    string[] CategoryDetails = new string[70];
    //    string strMsg = string.Empty;
    //    string strActive = string.Empty;
    //    lblalert.Text = string.Empty;

    //    try
    //    {

    //        #region For Manual and Modiy Fos

    //        string strDepartTrip = "D";
    //        string strArrTrip = "D";

    //        if (lblgroup.Text != "Group")
    //        {
    //            //lblEarliestTimeLabel.Text 
    //            #region Check earliest depart time

    //            if (lblEarliestTime.Text != string.Empty)
    //            {
    //                DateTime dtStartTime = Convert.ToDateTime(Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00");
    //                if (Convert.ToDateTime(lblEarliestTime.Text) > dtStartTime)
    //                {
    //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
    //                    lblalert.Text = "Please ensure that the Available from is greater than or equal to the Earliest Departure Time.</br>";
    //                    mpealert.Show();
    //                    return;
    //                }
    //            }
    //            if (lblMaxDepartTime.Text != string.Empty)
    //            {
    //                DateTime dtEndTime = Convert.ToDateTime(Convert.ToDateTime(txtAvaTo.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtEndTime.Value + ":00");
    //                if (Convert.ToDateTime(lblMaxDepartTime.Text) < dtEndTime)
    //                {
    //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
    //                    lblalert.Text = lblalert.Text + "Please ensure that the Available To is less than or equal to the Latest Departure Time.";
    //                    mpealert.Show();
    //                    return;
    //                }
    //            }

    //            #endregion





    //            string strBusinessAlert = string.Empty;

    //            if (txtTailNo.Value.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- Tail number is required. <br/>";
    //            }
    //            if (txttrip.Value.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- Trip number is required. <br/>";
    //            }
    //            if (txtFlyHours1.Value.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- Flying Hours is required. <br/>";
    //            }
    //            if (txtFlyHours1.Value.ToString().Length > 0)
    //            {
    //                if (Convert.ToDouble(txtFlyHours1.Value) < 0.7)
    //                {
    //                    strBusinessAlert += "The Selected route is not allowed for booking. Please modify your request. <br/>";
    //                }
    //            }
    //            if (txtAvaFrom.Text.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- Available From is required. <br/>";
    //            }
    //            if (txtAvaTo.Text.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- Available To is required. <br/>";
    //            }
    //            if (txtMPrice.Value.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- Price is required. <br/>";
    //            }

    //            if (txtStartTime.Value.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- Start Time is required. <br/>";
    //            }

    //            if (txtEndTime.Value.ToString().Length == 0)
    //            {
    //                strBusinessAlert += "- End Time is required. <br/>";
    //            }


    //            if (lblgroup.Text != "Modify")
    //            {
    //                if (gvDepAirport.Rows.Count == 0)
    //                {
    //                    strBusinessAlert += "- Departure Airport is required. <br/>";
    //                }

    //                if (gvArrivalAirport.Rows.Count == 0)
    //                {
    //                    strBusinessAlert += "- Arrival Airport is required. <br/>";
    //                }
    //            }

    //            if (gvDepAirport.Rows.Count > 0)
    //            {
    //                if (txtDepDisLandPage.Text.ToString().Length == 0)
    //                {
    //                    strBusinessAlert += "- Departure Landing Page input is required. <br/>";
    //                }
    //            }
    //            if (gvArrivalAirport.Rows.Count > 0)
    //            {
    //                if (txtArrDisLandPage.Text.ToString().Length == 0)
    //                {
    //                    strBusinessAlert += "- Arrival Landing Page input is required. <br/>";
    //                }
    //            }


    //            DataTable dtPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select HourlyRate,pricingcompleted  from Tailsetup where tailno='" + txtTailNo.Value.Trim() + "'").Tables[0];

    //            if (dtPrice.Rows.Count > 0)
    //            {
    //                if (dtPrice.Rows[0]["pricingcompleted"].ToString().Trim() == "0")
    //                {
    //                    strBusinessAlert += "- Please complete the pricing setup for the tail [ " + txtTailNo.Value.Trim() + " ] <br/>";
    //                }

    //                if (txtMPrice.Value.Trim() != "")
    //                {
    //                    string strHourlyRate = dtPrice.Rows[0]["HourlyRate"].ToString().Trim();

    //                    if (Convert.ToDecimal(txtMPrice.Value.Trim()) < Convert.ToDecimal(strHourlyRate.Trim()))
    //                    {
    //                        strBusinessAlert += "- Please enter price greater than $ " + strHourlyRate + " <br/>";
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                strBusinessAlert += "- Tail not exist in pricing setup. <br/>";
    //            }


    //            if (strBusinessAlert.Length > 0)
    //            {
    //                lblalert.Text = strBusinessAlert;
    //                mpealert.Show();
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
    //                return;
    //            }
    //            decimal daystoflight = 0;
    //            decimal L10_minPrice = 0;
    //            decimal M10_maxPrice = 0;
    //            decimal price = 0;

    //            decimal totalflyingHours = Convert.ToDecimal(txtFlyHours1.Value);

    //            var HourlyRate = Convert.ToDecimal("0.00"); ;
    //            var fixedCeiling = Convert.ToDecimal("0.00");
    //            var fixedFloor = Convert.ToDecimal("0.00");
    //            var D4_MinTimeBD = Convert.ToDecimal("0.0");
    //            var D5_MaxTimeBD = Convert.ToDecimal("0.00");
    //            var MinFLHSellable = Convert.ToDecimal("0.00");
    //            var finalPricevalidation2 = (decimal)0;


    //            var dtFlyingDays = Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00";

    //            var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00").Subtract(DateTime.Now);

    //            var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

    //            string stractive = string.Empty;

    //            if (chkEmpty.Checked)
    //            {
    //                stractive = "1";
    //            }
    //            else
    //            {
    //                stractive = "0";
    //            }

    //            if (hdfSCID.Value == "0")
    //            {
    //                CategoryDetails[0] = "I";

    //                DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
    //                if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
    //                {
    //                    gv_Child_Tail.DataSource = dttailtrip;
    //                    gv_Child_Tail.DataBind();
    //                    mpetrip.Show();
    //                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
    //                    return;
    //                }

    //                DataTable dttail = new DataTable();

    //                //if (gvDepAirport.Rows.Count > 0)
    //                //{
    //                //    if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
    //                //    {
    //                //        string strstartcity2 = txtStartCity.Text.Replace(" - ", "-");
    //                //        String[] strliststart = strstartcity2.Split('-');

    //                //        string strendcity2 = txtEndCity.Text.Replace(" - ", "-");
    //                //        String[] strlistend = strendcity2.Split('-');
    //                //        if (strliststart.Length > 1 && strlistend.Length > 1)
    //                //        {
    //                //            dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + strliststart[1] + "' and EndAirport='" + strlistend[1] + "'").Tables[0];
    //                //        }
    //                //    }
    //                //    else
    //                //    {
    //                //        dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + txtStartCity.Text.Trim() + "' and EndAirport='" + txtEndCity.Text.Trim() + "'").Tables[0];
    //                //    }
    //                //}




    //                if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
    //                {
    //                    lblalert.Text = "Tail No  already exists.";
    //                    mpeconfirm.Show();
    //                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
    //                    return;
    //                    txtTailNo.Value = string.Empty;
    //                }
    //            }
    //            else
    //            {
    //                if (LBLTRIPNO.Text != txttrip.Value)
    //                {
    //                    DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
    //                    if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
    //                    {
    //                        gv_Child_Tail.DataSource = dttailtrip;
    //                        gv_Child_Tail.DataBind();
    //                        mpetrip.Show();
    //                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
    //                        return;
    //                    }
    //                    DataTable dttail = new DataTable();

    //                    //if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
    //                    //{
    //                    //    string strstartcity1 = txtStartCity.Text.Replace(" - ", "-");
    //                    //    String[] strliststart = strstartcity1.Split('-');

    //                    //    string strendcity1 = txtEndCity.Text.Replace(" - ", "-");
    //                    //    String[] strlistend = strendcity1.Split('-');
    //                    //    dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + strliststart[1] + "' and EndAirport='" + strlistend[1] + "'").Tables[0];
    //                    //}
    //                    //else
    //                    //{
    //                    //    dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + txtStartCity.Text.Trim() + "' and EndAirport='" + txtEndCity.Text.Trim() + "'").Tables[0];
    //                    //}

    //                    if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
    //                    {
    //                        lblalert.Text = "Tail No  already exists.";
    //                        mpeconfirm.Show();
    //                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
    //                        return;
    //                    }
    //                }

    //                CategoryDetails[0] = "U";
    //            }
    //            string atctivetail = string.Empty;
    //            if (chkEmpty.Checked)
    //            {
    //                atctivetail = "1";
    //            }
    //            else
    //            {
    //                atctivetail = "0";
    //            }

    //            DataTable dtstatrt = new DataTable();
    //            DataTable dtend = new DataTable();

    //            DateTime fromDate = new DateTime();
    //            DateTime toDate = new DateTime();
    //            string dateTimeNow = string.Empty;


    //            DateTime DatefromTZ = new DateTime();
    //            DateTime DatetoTZ = new DateTime();
    //            DatefromTZ = Convert.ToDateTime(txtAvaFrom.Text + " " + txtStartTime.Value);
    //            if (dtstatrt.Rows.Count > 0)
    //            {
    //                fromDate = Convert.ToDateTime(DatefromTZ).AddHours(-Convert.ToInt32(dtstatrt.Rows[0]["Timezone"].ToString()));
    //            }

    //            DatetoTZ = Convert.ToDateTime(txtAvaTo.Text + " " + txtEndTime.Value);
    //            if (dtend.Rows.Count > 0)
    //            {
    //                toDate = Convert.ToDateTime(DatetoTZ).AddHours(-Convert.ToInt32(dtend.Rows[0]["Timezone"].ToString()));
    //            }


    //            string strstartdate = fromDate.ToString("yyyy-MM-dd");
    //            string strstarttime = fromDate.ToString("HH:mm");

    //            string strenddate = toDate.ToString("yyyy-MM-dd");
    //            string strendtime = toDate.ToString("HH:mm");
    //            string strstartcity = string.Empty;
    //            string strendcity = string.Empty;

    //            if (hdfSCID.Value == "")
    //            {
    //                CategoryDetails[1] = "0";
    //            }
    //            else
    //            {
    //                CategoryDetails[1] = hdfSCID.Value;
    //            }
    //            CategoryDetails[2] = txtTailNo.Value;
    //            CategoryDetails[3] = txtTailNo.Value;

    //            if (gvDepAirport.Rows.Count > 0)
    //            {
    //                string strAirportCode = string.Empty;
    //                string strAirportName = string.Empty;

    //                string strStateCode = string.Empty;
    //                string strStateName = string.Empty;

    //                string strCountryCode = string.Empty;
    //                string strCountryName = string.Empty;



    //                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
    //                {
    //                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
    //                    Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

    //                    if (lblSetupBy.Text.Trim() == "Airport")
    //                    {
    //                        strstartcity = lblAirport.Text.Replace(",", "-").Replace(" ", "");
    //                        String[] strliststart = strstartcity.Trim().Split('-');

    //                        strAirportCode += strliststart[0].Trim() + "/";
    //                        strAirportName += strliststart[1].Trim() + "/";
    //                    }
    //                    if (lblSetupBy.Text.Trim() == "State")
    //                    {
    //                        string strState = lblAirport.Text;
    //                        if (strState.Contains(','))
    //                        {
    //                            string[] strSplit = strState.Trim().Split(',');

    //                            if (strSplit.Length > 1)
    //                            {
    //                                strStateName += strSplit[0].ToString().Trim() + "/";

    //                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

    //                                if (strSTCode.Length > 1)
    //                                {
    //                                    strStateCode += strSTCode[0].ToString().Trim() + "/";
    //                                }
    //                            }
    //                        }
    //                    }
    //                    if (lblSetupBy.Text.Trim() == "Country")
    //                    {
    //                        string strCountry = lblAirport.Text;
    //                        if (strCountry.Contains(','))
    //                        {
    //                            string[] strSplit = strCountry.Trim().Split(',');
    //                            if (strSplit.Length > 1)
    //                            {
    //                                strCountryCode += strSplit[1].ToString().Trim() + "/";
    //                                strCountryName += strSplit[0].ToString().Trim() + "/";
    //                            }
    //                        }
    //                    }
    //                }


    //                CategoryDetails[4] = (strCountryCode + strStateCode + strAirportCode).TrimEnd('/');
    //                CategoryDetails[5] = (strCountryName + strStateName + strAirportName).TrimEnd('/');

    //                CategoryDetails[52] = txtDepDisLandPage.Text.Trim();
    //            }
    //            else
    //            {
    //                CategoryDetails[4] = lblStartData.Text;
    //                CategoryDetails[5] = lblscitydata.Text;

    //                CategoryDetails[52] = lblStartData.Text;
    //            }
    //            if (gvArrivalAirport.Rows.Count > 0)
    //            {
    //                string strEndAirportCode = string.Empty;
    //                string strEndAirportName = string.Empty;

    //                string strEndStateCode = string.Empty;
    //                string strEndStateName = string.Empty;

    //                string strEndCountryCode = string.Empty;
    //                string strEndCountryName = string.Empty;
    //                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
    //                {
    //                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
    //                    Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

    //                    if (lblSetupBy.Text.Trim() == "Airport")
    //                    {
    //                        strstartcity = lblAirport.Text.Replace(",", "-").Replace(" ", "");
    //                        String[] strliststart = strstartcity.Trim().Split('-');

    //                        strEndAirportCode += strliststart[0].Trim() + "/";
    //                        strEndAirportName += strliststart[1].Trim() + "/";
    //                    }
    //                    if (lblSetupBy.Text.Trim() == "State")
    //                    {
    //                        string strState = lblAirport.Text;
    //                        if (strState.Contains(','))
    //                        {
    //                            string[] strSplit = strState.Trim().Split(',');
    //                            if (strSplit.Length > 1)
    //                            {
    //                                strEndStateName += strSplit[0].ToString().Trim() + "/";

    //                                string[] strSTCode = strSplit[1].Trim().ToString().Trim().Replace(" ", "").Split('-');

    //                                if (strSTCode.Length > 1)
    //                                {
    //                                    strEndStateCode += strSTCode[0].ToString().Trim() + "/";
    //                                }
    //                            }
    //                        }
    //                    }
    //                    if (lblSetupBy.Text.Trim() == "Country")
    //                    {
    //                        string strCountry = lblAirport.Text;
    //                        if (strCountry.Contains(','))
    //                        {
    //                            string[] strSplit = strCountry.Trim().Split(',');
    //                            if (strSplit.Length > 1)
    //                            {
    //                                strEndCountryCode += strSplit[1].ToString().Trim() + "/";
    //                                strEndCountryName += strSplit[0].ToString().Trim() + "/";
    //                            }
    //                        }
    //                    }
    //                }

    //                CategoryDetails[7] = (strEndCountryCode + strEndStateCode + strEndAirportCode).TrimEnd('/');
    //                CategoryDetails[8] = (strEndCountryName + strEndStateName + strEndAirportName).TrimEnd('/');
    //                CategoryDetails[53] = txtArrDisLandPage.Text.Trim();
    //            }
    //            else
    //            {
    //                CategoryDetails[7] = lblendData.Text.Trim();
    //                CategoryDetails[8] = lblEcityData.Text.Trim();
    //                CategoryDetails[53] = lblendData.Text.Trim();
    //            }



    //            CategoryDetails[6] = txtStartTime.Value;
    //            CategoryDetails[9] = txtEndTime.Value;
    //            CategoryDetails[10] = "";
    //            CategoryDetails[11] = txtFlyHours1.Value;
    //            CategoryDetails[12] = Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd");
    //            CategoryDetails[13] = Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd");

    //            CategoryDetails[15] = "DN";
    //            CategoryDetails[41] = strstartdate;
    //            CategoryDetails[42] = strenddate;
    //            CategoryDetails[43] = strstarttime;
    //            CategoryDetails[44] = strendtime;
    //            if (hdfSCID.Value == "0")
    //            {
    //                DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' ").Tables[0];
    //                if (dttailTable.Rows.Count > 0)
    //                {
    //                }
    //                else
    //                {
    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
    //                }

    //                DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *, isnull(noofseats,0) as noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' and pricingcompleted='1'").Tables[0];
    //                if (dttailTableEx.Rows.Count > 0)
    //                {
    //                    CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
    //                    CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
    //                    CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
    //                    CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
    //                    CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
    //                    CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                    CategoryDetails[26] = Convert.ToDecimal(0).ToString();
    //                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                    CategoryDetails[28] = Convert.ToDecimal(0).ToString();
    //                    CategoryDetails[29] = Convert.ToDecimal(0).ToString();
    //                    CategoryDetails[35] = "1";
    //                    CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
    //                    CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
    //                    CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
    //                }
    //                else
    //                {
    //                    CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                    CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                    CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                    CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                    CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                    CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                    CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                    CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                    CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                    CategoryDetails[35] = "0";
    //                    CategoryDetails[33] = "1";
    //                    CategoryDetails[40] = "0";
    //                    CategoryDetails[14] = "0";
    //                }
    //            }
    //            else
    //            {
    //                if (lbloldtail.Text != txtTailNo.Value)
    //                {
    //                    DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + lbloldtail.Text + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
    //                    if (dttail.Rows.Count > 0)
    //                    {
    //                        CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
    //                        CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
    //                        CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
    //                        CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
    //                        CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
    //                        CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                        CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
    //                        CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
    //                        CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
    //                        CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
    //                        CategoryDetails[35] = "1";
    //                        CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
    //                    }
    //                    else
    //                    {
    //                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                        CategoryDetails[35] = "0";
    //                        CategoryDetails[33] = "1";
    //                    }
    //                }
    //                else
    //                {
    //                    DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + txtTailNo.Value + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
    //                    if (dttail.Rows.Count > 0)
    //                    {
    //                        CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
    //                        CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
    //                        CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
    //                        CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
    //                        CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
    //                        CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                        CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
    //                        CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
    //                        CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
    //                        CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
    //                        CategoryDetails[35] = "1";
    //                        CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
    //                    }
    //                    else
    //                    {
    //                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                        CategoryDetails[35] = "0";
    //                        CategoryDetails[33] = "1";
    //                    }
    //                }
    //            }

    //            CategoryDetails[16] = "0.00";
    //            CategoryDetails[16] = "0.00";
    //            CategoryDetails[30] = totalflyingHours.ToString("0.00");
    //            if (chkEmpty.Checked)
    //            {
    //                CategoryDetails[19] = "1";
    //            }
    //            else
    //            {
    //                CategoryDetails[19] = "0";
    //            }
    //            string s = DateTime.Now.ToString("HH:mm:ss.fff");

    //            TimeSpan ts = TimeSpan.Parse(s);
    //            CategoryDetails[31] = ts.ToString();
    //            CategoryDetails[32] = DateTime.Now.Date.ToString();
    //            if (hdfSCID.Value == "0")
    //            {
    //                CategoryDetails[34] = txttrip.Value;
    //            }
    //            else
    //            {
    //                CategoryDetails[34] = txttrip.Value;
    //            }
    //            CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
    //            CategoryDetails[37] = DateTime.Now.Date.ToString();
    //            CategoryDetails[38] = LBLTRIPNO.Text;
    //            CategoryDetails[39] = lbloldtail.Text;

    //            CategoryDetails[48] = txtMPrice.Value.Trim();

    //            if (lblgroup.Text == "Modify")
    //            {
    //                CategoryDetails[49] = "MF";
    //                CategoryDetails[0] = "CH";
    //                if (lbllegdisplay.Text.Trim() == "" || lbllegdisplay.Text.Trim() == "0")
    //                {
    //                    CategoryDetails[51] = "1";
    //                }
    //                else
    //                {
    //                    CategoryDetails[51] = lbllegdisplay.Text.Trim();
    //                }
    //                if (lblModGroupID.Text == "0")
    //                {
    //                    DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from Tailsetup_tripDetails").Tables[0];
    //                    int ModifyGroupId = 1;
    //                    if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
    //                    {
    //                        if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
    //                        {
    //                            ModifyGroupId = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
    //                        }
    //                        else
    //                        {
    //                            ModifyGroupId = 1;
    //                        }
    //                    }

    //                    CategoryDetails[50] = ModifyGroupId.ToString();
    //                }
    //                else
    //                {
    //                    CategoryDetails[50] = lblModGroupID.Text;
    //                }


    //            }
    //            else
    //            {
    //                CategoryDetails[49] = "ET";
    //                CategoryDetails[51] = "1";

    //            }

    //            string strReturnVal = saveCategory(CategoryDetails);

    //            if (strReturnVal != "0")
    //            {
    //                lblalert.Text = "Empty leg details have been saved successfully";
    //                mpealert.Show();
    //                if (hdfSCID.Value == "0")
    //                {
    //                    lblManEditGroupId.Text = strReturnVal;
    //                }
    //                else if (hdfSCID.Value != "0" && lblgroup.Text == "Modify")
    //                {
    //                    lblManEditGroupId.Text = strReturnVal;
    //                }

    //                if (gvDepAirport.Rows.Count > 0)
    //                {
    //                    string strModFlag = string.Empty;
    //                    if (lblgroup.Text == "Modify")
    //                    {
    //                        strModFlag = "MF";
    //                    }
    //                    else
    //                    {
    //                        strModFlag = "ET";
    //                    }

    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");

    //                    for (int i = 0; i < gvDepAirport.Rows.Count; i++)
    //                    {
    //                        string strCount = "0";
    //                        Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
    //                        Label lblAir = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");
    //                        Label lblCreatedBy = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedBy");
    //                        Label lblCreatedOn = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedOn");

    //                        Label lblStateCode = (Label)gvDepAirport.Rows[i].FindControl("lblStateCode");
    //                        Label lblCountryCode = (Label)gvDepAirport.Rows[i].FindControl("lblCountryCode");
    //                        Label lblAirportCode = (Label)gvDepAirport.Rows[i].FindControl("lblAirportCode");
    //                        Label lblStateID = (Label)gvDepAirport.Rows[i].FindControl("lblStateId");


    //                        if (lblSetupBy.Text == "Airport")
    //                        {
    //                            strCount = objEarliestTimeDAL.INTExist(lblAirportCode.Text.Trim(), "AIRT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strDepartTrip = "I";
    //                            }
    //                        }
    //                        else if (lblSetupBy.Text == "Country")
    //                        {
    //                            strCount = objEarliestTimeDAL.INTExist(lblCountryCode.Text.Trim(), "CONT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strDepartTrip = "I";
    //                            }
    //                        }
    //                        else
    //                        {
    //                            //strCount = objEarliestTimeDAL.INTExist(lblStateCode.Text.Trim(), "STAT");
    //                            strCount = objEarliestTimeDAL.INTExist(lblStateID.Text.Trim(), "STAT");
    //                            if (Convert.ToInt32(strCount) == 0)
    //                            {
    //                                strDepartTrip = "I";
    //                            }
    //                        }

    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,StateID) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Dep', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text.Trim() + "','" + lblCountryCode.Text.Trim() + "','" + lblAirportCode.Text.Trim() + "','" + lblStateID.Text.Trim() + "')");
    //                    }
    //                }

    //                if (gvArrivalAirport.Rows.Count > 0)
    //                {
    //                    string strModFlag = string.Empty;
    //                    if (lblgroup.Text == "Modify")
    //                    {
    //                        strModFlag = "MF";
    //                    }
    //                    else
    //                    {
    //                        strModFlag = "ET";
    //                    }

    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");

    //                    for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
    //                    {

    //                        string strCount = "0";
    //                        Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
    //                        Label lblAir = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");
    //                        Label lblCreatedBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedBy");
    //                        Label lblCreatedOn = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedOn");

    //                        Label lblStateCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateCode");
    //                        Label lblCountryCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblCountryCode");
    //                        Label lblAirportCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblAirportCode");
    //                        Label lblStateID = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateId");



    //                        if (lblSetupBy.Text == "Airport")
    //                        {
    //                            strCount = objEarliestTimeDAL.INTExist(lblAirportCode.Text.Trim(), "AIRT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strArrTrip = "I";
    //                            }
    //                        }
    //                        else if (lblSetupBy.Text == "Country")
    //                        {
    //                            strCount = objEarliestTimeDAL.INTExist(lblCountryCode.Text.Trim(), "CONT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strArrTrip = "I";
    //                            }
    //                        }
    //                        else
    //                        {
    //                            // strCount = objEarliestTimeDAL.INTExist(lblStateCode.Text.Trim(), "STAT");

    //                            strCount = objEarliestTimeDAL.INTExist(lblStateID.Text.Trim(), "STAT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strArrTrip = "I";
    //                            }
    //                        }

    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,StateID) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Arr', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text.Trim() + "','" + lblCountryCode.Text.Trim() + "','" + lblAirportCode.Text.Trim() + "','" + lblStateID.Text.Trim() + "')");

    //                    }
    //                }

    //            }
    //            if (hdfSCID.Value != "0")
    //            {
    //                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set Active='" + stractive + " ',Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "'  and tripno='" + LBLTRIPNO.Text + "'");
    //                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update bookeddetails set Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "' and tripno  ='" + LBLTRIPNO.Text + "'  ");
    //                DataTable dtPScheck = new DataTable();
    //                if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
    //                {
    //                    string strstartcity1 = txtStartCity.Text.Replace(" - ", "-");
    //                    String[] strliststart = strstartcity1.Trim().Split('-');

    //                    string strendcity1 = txtEndCity.Text.Replace(" - ", "-");
    //                    String[] strlistend = strendcity1.Trim().Split('-');
    //                    dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "' and Flag='FINAL'").Tables[0];
    //                    if (dtPScheck.Rows.Count > 0)
    //                    {
    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "'  and FLAG='FINAL'");
    //                    }
    //                }

    //                else
    //                {
    //                    dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "' and Flag='FINAL'").Tables[0];

    //                    if (dtPScheck.Rows.Count > 0)
    //                    {
    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "'  and FLAG='FINAL'");
    //                    }
    //                }

    //            }
    //        }

    //        #endregion

    //        #region Split Insert

    //        else
    //        {

    //            #region // get Parent ID
    //            string strParentFlag = string.Empty;
    //            string strParentID = string.Empty;

    //            strParentFlag = lblEModFlag.Text;
    //            strParentID = lblModGroupID.Text;

    //            DataTable dtParent = genclass.GetDataTable("select ParentID,ParentFlag from EmptyLegAirport_Child where groupID='" + strParentID + "' and ModifiedFlag='SP'");
    //            if (dtParent.Rows.Count > 0)
    //            {
    //                if (dtParent.Rows[0]["ParentID"].ToString() != string.Empty)
    //                {
    //                    strParentFlag = dtParent.Rows[0]["ParentFlag"].ToString();
    //                    strParentID = dtParent.Rows[0]["ParentID"].ToString();
    //                }
    //            }


    //            #endregion


    //            #region Check earliest depart time
    //            string strAlert = string.Empty;


    //            if (lblEarliestTime.Text != string.Empty)
    //            {
    //                DateTime dtStartTime = Convert.ToDateTime(txtSplitAvaFrom.Text);
    //                if (Convert.ToDateTime(lblEarliestTime.Text).Date > dtStartTime.Date)
    //                {
    //                    strAlert = "Please ensure that the Available from is greater than or equal to the Earliest Departure Time.</br>";

    //                }
    //            }


    //            if (lblMaxDepartTime.Text != string.Empty)
    //            {
    //                DateTime dtEndTime = Convert.ToDateTime(txtSplitAvaTo.Text);
    //                if (Convert.ToDateTime(lblMaxDepartTime.Text).Date < dtEndTime.Date)
    //                {
    //                    strAlert = strAlert + "Please ensure that the Available To is less than or equal to the Latest Departure Time.</br>";

    //                }
    //            }




    //            if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
    //            {
    //                if (lblStartData.Text.Length == 4)
    //                {
    //                    if (((Label)gvLegDetails.Rows[0].FindControl("lblstartair")).Text != lblStartData.Text)
    //                    {
    //                        strAlert = strAlert + "Please ensure that the Start Airport matches the actual Start Airport [" + lblStartData.Text + "] of the trip.</br>";
    //                    }
    //                }
    //                if (lblendData.Text.Length == 4)
    //                {
    //                    if (((Label)gvLegDetails.Rows[gvLegDetails.Rows.Count - 1].FindControl("lblendair")).Text != lblendData.Text)
    //                    {
    //                        strAlert = strAlert + "Please ensure that the End Airport matches the actual End Airport   [" + lblendData.Text + "]  of the trip.</br>";
    //                    }
    //                }

    //            }


    //            if (strAlert != string.Empty)
    //            {
    //                lblalert.Text = strAlert;
    //                mpealert.Show();
    //                return;
    //            }

    //            #endregion

    //            string strBusinessAlert = string.Empty;

    //            if (rblBreak.SelectedValue.Trim() == "CO")
    //            {
    //                if (txtcountry.Text.ToString().Length == 0)
    //                {
    //                    strBusinessAlert += "- Start county is required. <br/>";
    //                }
    //                if (ddlLeg1EndCity.Text.ToString() == "")
    //                {
    //                    strBusinessAlert += "- End city is required. <br/>";
    //                }
    //                if (txtCouStartTime.Value.ToString().Length == 0)
    //                {
    //                    strBusinessAlert += "- Start Time is required. <br/>";
    //                }
    //                if (txtSplitLeg1EndTime.Text.ToString().Length == 0)
    //                {
    //                    strBusinessAlert += "- End Time is required. <br/>";
    //                }
    //                if (txtSpliFlyHour.Value.ToString().Length == 0)
    //                {
    //                    strBusinessAlert += "- Flying Hour is required. <br/>";
    //                }
    //                if (txtSpliFlyHour.Value.ToString().Length > 0)
    //                {
    //                    if (Convert.ToDouble(txtSpliFlyHour.Value) < 0.7)
    //                    {
    //                        strBusinessAlert += "The Selected route is not allowed for booking. Please modify your request. <br/>";
    //                    }
    //                }
    //            }
    //            else if (rblBreak.SelectedValue.Trim() == "CT")
    //            {
    //                if (gvLegDetails.Rows.Count == 0)
    //                {
    //                    strBusinessAlert = "Please add leg";
    //                }
    //            }
    //            if (strBusinessAlert.Length > 0)
    //            {
    //                lblalert.Text = strBusinessAlert;
    //                mpealert.Show();
    //                return;
    //            }


    //            #region Split New Insert

    //            if (btnSave.Text == "Save")
    //            {
    //                int groupid = 1;
    //                decimal daystoflight = 0; decimal L10_minPrice = 0; decimal M10_maxPrice = 0; decimal price = 0;

    //                var HourlyRate = Convert.ToDecimal("0.00");
    //                var fixedCeiling = Convert.ToDecimal("0.00");
    //                var fixedFloor = Convert.ToDecimal("0.00");
    //                var D4_MinTimeBD = Convert.ToDecimal("0.0");
    //                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
    //                var MinFLHSellable = Convert.ToDecimal("0.00");
    //                var finalPricevalidation2 = (decimal)0;
    //                var _totalDaysK101 = "0";

    //                string stractive = string.Empty;
    //                stractive = "1";

    //                if (hdfSCID.Value == "0")
    //                {
    //                    CategoryDetails[0] = "I";
    //                }
    //                else
    //                {
    //                    CategoryDetails[0] = "U";
    //                }
    //                string atctivetail = string.Empty;

    //                atctivetail = "1";

    //                //if (lblModGroupID.Text == "0")
    //                //{

    //                DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from Tailsetup_tripDetails").Tables[0];

    //                if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
    //                {
    //                    if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
    //                    {
    //                        groupid = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
    //                    }
    //                    else
    //                    {
    //                        groupid = 1;
    //                    }
    //                }

    //                //}
    //                //else
    //                //{
    //                //    groupid = Convert.ToInt32(lblModGroupID.Text);
    //                //}

    //                #region Country INert

    //                if (rblBreak.SelectedValue.Trim() == "CO")
    //                {
    //                    //SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CO',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text).ToString("yyyy-MM-dd") + "' and leg=" + lbllegdisplay.Text + "  ");
    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CO',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and TripFlag='B0' and leg=" + lbllegdisplay.Text + "  ");

    //                    if (lblbaseflag.Text.Trim() != "B0")
    //                    {
    //                        var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + txtCouStartTime.Value.Trim() + ":00");
    //                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
    //                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

    //                        var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value + ":00").Subtract(DateTime.Now);
    //                        _totalDaysK101 = K10_totalDays1.TotalDays.ToString("0.00");

    //                        string strstartcity = string.Empty;
    //                        strstartcity = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
    //                        String[] strlistend = strstartcity.Trim().Split('-');
    //                        string startAirport = string.Empty;
    //                        string startcity = string.Empty;
    //                        if (strlistend.Length > 1)
    //                        {
    //                            startAirport = strlistend[0].Trim();
    //                            startcity = strlistend[1].Trim();
    //                        }

    //                        CategoryDetails[1] = hdfSCID.Value;
    //                        CategoryDetails[2] = txtTailNo.Value.Trim();
    //                        CategoryDetails[3] = txtTailNo.Value.Trim();
    //                        CategoryDetails[4] = txtcountry.Text;
    //                        CategoryDetails[5] = "";

    //                        CategoryDetails[6] = txtCouStartTime.Value.Trim();

    //                        CategoryDetails[7] = startAirport;
    //                        CategoryDetails[8] = startcity;
    //                        CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
    //                        CategoryDetails[10] = "";
    //                        CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
    //                        CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
    //                        CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
    //                        CategoryDetails[15] = "";
    //                        CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
    //                        CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
    //                        CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
    //                        CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

    //                        if (hdfSCID.Value == "0")
    //                        {
    //                            DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
    //                            if (dttailTable.Rows.Count > 0)
    //                            {

    //                            }
    //                            else
    //                            {
    //                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
    //                            }

    //                            DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
    //                            if (dttailTableEx.Rows.Count > 0)
    //                            {
    //                                CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
    //                                CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
    //                                CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
    //                                CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
    //                                CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
    //                                CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                                CategoryDetails[26] = Convert.ToDecimal(0).ToString();
    //                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
    //                                CategoryDetails[28] = Convert.ToDecimal(0).ToString();
    //                                CategoryDetails[29] = Convert.ToDecimal(0).ToString();
    //                                CategoryDetails[35] = "1";
    //                                CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
    //                                CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
    //                                CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                                CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                                CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                                CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                                CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                                CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                                CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
    //                                CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                                CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                                CategoryDetails[35] = "0";
    //                                CategoryDetails[33] = "1";
    //                                CategoryDetails[40] = "0";
    //                                CategoryDetails[14] = "0";
    //                            }
    //                        }
    //                        decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
    //                        CategoryDetails[16] = "0.00";
    //                        CategoryDetails[16] = "0.00";
    //                        CategoryDetails[30] = totalflyingHours.ToString("0.00");
    //                        if (cbSplitEmpty.Checked)
    //                        {
    //                            CategoryDetails[19] = "1";
    //                        }
    //                        else
    //                        {
    //                            CategoryDetails[19] = "0";
    //                        }
    //                        string s1 = DateTime.Now.ToString("HH:mm:ss.fff");

    //                        TimeSpan ts1 = TimeSpan.Parse(s1);
    //                        CategoryDetails[31] = ts1.ToString();
    //                        CategoryDetails[32] = DateTime.Now.Date.ToString();
    //                        if (hdfSCID.Value == "0")
    //                        {
    //                            CategoryDetails[34] = txttrip.Value.Trim();
    //                        }
    //                        else
    //                        {
    //                            CategoryDetails[34] = txttrip.Value.Trim();
    //                        }
    //                        CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
    //                        CategoryDetails[37] = DateTime.Now.Date.ToString();
    //                        CategoryDetails[38] = txttrip.Value.Trim();
    //                        CategoryDetails[39] = txtTailNo.Value.Trim();
    //                        CategoryDetails[45] = "B1";
    //                        CategoryDetails[46] = groupid.ToString();
    //                        CategoryDetails[47] = lbllegdisplay.Text;
    //                        CategoryDetails[48] = rblBreak.SelectedValue.Trim();
    //                        if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
    //                        {
    //                            CategoryDetails[49] = lblStartData.Text.Trim();
    //                            CategoryDetails[50] = lblendData.Text.Trim();
    //                        }
    //                        else
    //                        {
    //                            CategoryDetails[49] = lblStartData.Text.Trim();
    //                            CategoryDetails[50] = lblendData.Text.Trim();
    //                        }

    //                        string strReturnVal1 = saveCategoryGroup(CategoryDetails);
    //                    }
    //                    else
    //                    {
    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");
    //                    }


    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

    //                    string strCountryCode = string.Empty;

    //                    if (txtcountry.Text.Contains("/"))
    //                    {
    //                        string[] strSplitCountry = txtcountry.Text.Trim().Split('/');

    //                        if (strSplitCountry.Length > 1)
    //                        {
    //                            for (int i = 0; i < strSplitCountry.Length; i++)
    //                            {
    //                                DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + strSplitCountry[i].ToString().Trim() + "' ").Tables[0];

    //                                if (detr.Rows.Count > 0)
    //                                {
    //                                    strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
    //                                }

    //                                string strCount = objEarliestTimeDAL.INTExist(strCountryCode.Trim(), "CONT");
    //                                if (Convert.ToInt32(strCount) > 0)
    //                                {
    //                                    strDepartTrip = "I";
    //                                }




    //                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Country', '" + strSplitCountry[i].ToString().Replace("'", "''") + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
    //                            }
    //                        }
    //                    }
    //                    else
    //                    {
    //                        DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + txtcountry.Text.Trim() + "' ").Tables[0];

    //                        if (detr.Rows.Count > 0)
    //                        {
    //                            strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
    //                        }

    //                        string strCount = objEarliestTimeDAL.INTExist(strCountryCode.Trim(), "CONT");
    //                        if (Convert.ToInt32(strCount) > 0)
    //                        {
    //                            strDepartTrip = "I";
    //                        }
    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Country', '" + txtcountry.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
    //                    }

    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

    //                    string strstartcity1 = string.Empty;
    //                    strstartcity1 = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
    //                    String[] strlistend1 = strstartcity1.Trim().Split('-');
    //                    string startAirport1 = string.Empty;
    //                    string startcity1 = string.Empty;
    //                    if (strlistend1.Length > 1)
    //                    {
    //                        startAirport1 = strlistend1[0].Trim();
    //                        startcity1 = strlistend1[1].Trim();
    //                    }

    //                    string strCount1 = objEarliestTimeDAL.INTExist(startAirport1.Trim(), "CONT");
    //                    if (Convert.ToInt32(strCount1) > 0)
    //                    {
    //                        strArrTrip = "I";
    //                    }

    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Arr', 'Airport', '" + ddlLeg1EndCity.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + startAirport1.Trim() + "')");


    //                }

    //                #endregion

    //                #region City Insert

    //                if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
    //                {
    //                    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text).ToString("yyyy-MM-dd") + "' and leg=" + lbllegdisplay.Text + "  ");

    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and TripFlag='B0' and leg=" + lbllegdisplay.Text + "  ");

    //                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

    //                    for (int i = 0; i < gvLegDetails.Rows.Count; i++)
    //                    {
    //                        if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
    //                        {
    //                            string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString();
    //                            var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + strStartTime.Trim() + ":00");
    //                            var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
    //                            var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

    //                            var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
    //                            K10_totalDays1.TotalDays.ToString("0.00");

    //                            #region Leg 2 Insert Breakdown by City

    //                            var K10_totalDays2 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
    //                            var _totalDaysK102 = K10_totalDays2.TotalDays.ToString("0.00");

    //                            CategoryDetails[1] = hdfSCID.Value;
    //                            CategoryDetails[2] = txtTailNo.Value.Trim();
    //                            CategoryDetails[3] = txtTailNo.Value.Trim();
    //                            CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
    //                            CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
    //                            CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
    //                            CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
    //                            CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
    //                            CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
    //                            CategoryDetails[10] = "";
    //                            CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
    //                            CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
    //                            CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
    //                            CategoryDetails[15] = "";
    //                            CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
    //                            CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
    //                            CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
    //                            CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

    //                            if (hdfSCID.Value == "0")
    //                            {
    //                                DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
    //                                if (dttailTable.Rows.Count > 0)
    //                                {

    //                                }
    //                                else
    //                                {
    //                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
    //                                }

    //                                DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
    //                                if (dttailTableEx.Rows.Count > 0)
    //                                {
    //                                    CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
    //                                    CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
    //                                    CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
    //                                    CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
    //                                    CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
    //                                    CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                                    CategoryDetails[26] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
    //                                    CategoryDetails[28] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[29] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[35] = "1";
    //                                    CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
    //                                    CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
    //                                    CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
    //                                }
    //                                else
    //                                {
    //                                    CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                                    CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                                    CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                                    CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                                    CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                                    CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                                    CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
    //                                    CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                                    CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                                    CategoryDetails[35] = "0";
    //                                    CategoryDetails[33] = "1";
    //                                    CategoryDetails[40] = "0";
    //                                    CategoryDetails[14] = "0";
    //                                }
    //                            }

    //                            CategoryDetails[16] = "0.00";
    //                            CategoryDetails[16] = "0.00";
    //                            CategoryDetails[30] = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString()).ToString("0.00");
    //                            if (chkEmpty.Checked)
    //                            {
    //                                CategoryDetails[19] = "1";
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[19] = "0";
    //                            }
    //                            string s2 = DateTime.Now.ToString("HH:mm:ss.fff");

    //                            TimeSpan ts2 = TimeSpan.Parse(s2);
    //                            CategoryDetails[31] = ts2.ToString();
    //                            CategoryDetails[32] = DateTime.Now.Date.ToString();
    //                            if (hdfSCID.Value == "0")
    //                            {
    //                                CategoryDetails[34] = txttrip.Value.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[34] = txttrip.Value.Trim();
    //                            }
    //                            CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
    //                            CategoryDetails[37] = DateTime.Now.Date.ToString();
    //                            CategoryDetails[38] = txttrip.Value.Trim();
    //                            CategoryDetails[39] = txtTailNo.Value.Trim();
    //                            CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString();
    //                            CategoryDetails[46] = groupid.ToString();
    //                            CategoryDetails[47] = lbllegdisplay.Text;
    //                            CategoryDetails[48] = rblBreak.SelectedValue.Trim();
    //                            if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
    //                            {
    //                                CategoryDetails[49] = lblStartData.Text.Trim();
    //                                CategoryDetails[50] = lblendData.Text.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[49] = lblStartData.Text.Trim();
    //                                CategoryDetails[50] = lblendData.Text.Trim();
    //                            }
    //                            string strReturnVal2 = saveCategoryGroup(CategoryDetails);

    //                            #endregion
    //                        }
    //                        if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
    //                        {
    //                            string strCount = "0";
    //                            string strStartAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text.Trim();
    //                            string strStartCity = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text.Trim();

    //                            string strEndAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text.Trim();
    //                            string strEndCity = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text.Trim();

    //                            strCount = objEarliestTimeDAL.INTExist(strStartAirport.Trim(), "AIRT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strDepartTrip = "I";
    //                            }

    //                            strCount = objEarliestTimeDAL.INTExist(strEndAirport.Trim(), "AIRT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strArrTrip = "I";
    //                            }

    //                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Airport', '" + strStartCity.Trim() + " - " + strStartAirport.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strStartAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");

    //                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + groupid.ToString().Trim() + "','SP','Arr', 'Airport', '" + strEndCity.Trim() + " - " + strEndAirport.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strEndAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");
    //                        }
    //                    }
    //                }

    //                #endregion
    //            }

    //            #endregion

    //            #region  Update Split Trip

    //            else
    //            {


    //                #region Check earliest depart time


    //                if (lblEarliestTime.Text != string.Empty)
    //                {
    //                    DateTime dtStartTime = Convert.ToDateTime(txtSplitAvaFrom.Text);
    //                    if (Convert.ToDateTime(lblEarliestTime.Text).Date > dtStartTime.Date)
    //                    {
    //                        strAlert = "Please ensure that the Available from is greater than or equal to the Earliest Departure Time.</br>";

    //                    }
    //                }


    //                if (lblMaxDepartTime.Text != string.Empty)
    //                {
    //                    DateTime dtEndTime = Convert.ToDateTime(txtSplitAvaTo.Text);
    //                    if (Convert.ToDateTime(lblMaxDepartTime.Text).Date < dtEndTime.Date)
    //                    {
    //                        strAlert = strAlert + "Please ensure that the Available To is less than or equal to the Latest Departure Time.</br>";

    //                    }
    //                }




    //                if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
    //                {
    //                    if (lblStartData.Text.Length == 4)
    //                    {
    //                        if (((Label)gvLegDetails.Rows[0].FindControl("lblstartair")).Text != lblStartData.Text)
    //                        {
    //                            strAlert = strAlert + "Please ensure that the Start Airport same as splited start Aiport.</br>";
    //                        }
    //                    }
    //                    if (lblendData.Text.Length == 4)
    //                    {
    //                        if (((Label)gvLegDetails.Rows[gvLegDetails.Rows.Count - 1].FindControl("lblendair")).Text != lblendData.Text)
    //                        {
    //                            strAlert = strAlert + "Please ensure that the End Airport same as splited start Aiport.</br>";
    //                        }
    //                    }

    //                }


    //                if (strAlert != string.Empty)
    //                {
    //                    lblalert.Text = strAlert;
    //                    mpealert.Show();
    //                    return;
    //                }

    //                #endregion



    //                // DataTable dtOutput = (DataTable)ViewState["Tripgrid"];

    //                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + lblgroupiddisplay.Text + "' and Tripflag<>'B0' ");

    //                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + lblgroupiddisplay.Text + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and TripFlag='B0' and leg=" + lbllegdisplay.Text + "  ");


    //                //  SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + lblgroupiddisplay.Text + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "'  and leg=" + lbllegdisplay.Text + "  ");

    //                if (lblbaseflag.Text != "B0")
    //                {
    //                    if (rblBreak.SelectedValue.Trim() == "CO")
    //                    {
    //                        #region Breakdown by Country

    //                        DataTable dtexist = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='B1' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

    //                        if (dtexist.Rows.Count > 0)
    //                        {
    //                            decimal daystoflight = 0;
    //                            decimal L10_minPrice = 0;
    //                            decimal M10_maxPrice = 0;
    //                            decimal price = 0;

    //                            decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
    //                            var HourlyRate = Convert.ToDecimal("0.00"); ;
    //                            var fixedCeiling = Convert.ToDecimal("0.00");
    //                            var fixedFloor = Convert.ToDecimal("0.00");
    //                            var D4_MinTimeBD = Convert.ToDecimal("0.0");
    //                            var D5_MaxTimeBD = Convert.ToDecimal("0.00");
    //                            var MinFLHSellable = Convert.ToDecimal("0.00");
    //                            var finalPricevalidation2 = (decimal)0;
    //                            var _totalDaysK10 = "0";

    //                            if (rblBreak.SelectedValue.Trim() == "CO")
    //                            {
    //                                var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
    //                                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
    //                                _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
    //                            }
    //                            else
    //                            {
    //                                var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
    //                                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
    //                                _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
    //                            }


    //                            string stractive = string.Empty;
    //                            stractive = "1";
    //                            CategoryDetails[0] = "U";
    //                            string atctivetail = string.Empty;
    //                            atctivetail = "1";

    //                            string strEndCity = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
    //                            String[] strlistend = strEndCity.Trim().Split('-');
    //                            string endAirport = string.Empty;
    //                            string endCity = string.Empty;
    //                            if (strlistend.Length > 1)
    //                            {
    //                                endAirport = strlistend[0].Trim();
    //                                endCity = strlistend[1].Trim();
    //                            }
    //                            CategoryDetails[1] = hdfSCID.Value;
    //                            CategoryDetails[2] = txtTailNo.Value.Trim();
    //                            CategoryDetails[3] = txtTailNo.Value.Trim();
    //                            if (rblBreak.SelectedValue.Trim() == "CT")
    //                            {
    //                                string strstartcity1 = string.Empty;
    //                                strstartcity1 = ddlStartCitySplit.Text.Replace(",", "-").Replace(" ", "");
    //                                String[] strliststart1 = strstartcity1.Trim().Split('-');
    //                                string startAirport1 = string.Empty;
    //                                string startcity1 = string.Empty;
    //                                if (strliststart1.Length > 1)
    //                                {
    //                                    startAirport1 = strliststart1[0].Trim();
    //                                    startcity1 = strliststart1[1].Trim();

    //                                    CategoryDetails[4] = startAirport1;
    //                                    CategoryDetails[5] = startcity1;
    //                                }
    //                                else
    //                                {

    //                                    CategoryDetails[4] = ddlStartCitySplit.Text;
    //                                    CategoryDetails[5] = "";
    //                                }


    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[4] = txtcountry.Text;
    //                                CategoryDetails[5] = "";
    //                            }
    //                            if (rblBreak.SelectedValue == "CO")
    //                            {
    //                                CategoryDetails[6] = txtCouStartTime.Value.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
    //                            }

    //                            // CategoryDetails[6] = txtCouStartTime.Value.Trim();
    //                            CategoryDetails[7] = endAirport;
    //                            CategoryDetails[8] = endCity;
    //                            CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
    //                            CategoryDetails[10] = "";
    //                            CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
    //                            CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
    //                            CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
    //                            //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
    //                            CategoryDetails[15] = "";
    //                            CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
    //                            CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
    //                            CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
    //                            CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
    //                            if (hdfSCID.Value == "0")
    //                            {
    //                                DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
    //                                if (dttailTable.Rows.Count > 0)
    //                                {
    //                                }
    //                                else
    //                                {
    //                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
    //                                }

    //                                DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
    //                                if (dttailTableEx.Rows.Count > 0)
    //                                {
    //                                    CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
    //                                    CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
    //                                    CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
    //                                    CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
    //                                    CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
    //                                    CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                                    CategoryDetails[26] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                    CategoryDetails[28] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[29] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[35] = "1";
    //                                    CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
    //                                    CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
    //                                    CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
    //                                }
    //                                else
    //                                {
    //                                    CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                                    CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                                    CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                                    CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                                    CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                                    CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                                    CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                    CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                                    CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                                    CategoryDetails[35] = "0";
    //                                    CategoryDetails[33] = "1";
    //                                    CategoryDetails[40] = "0";
    //                                    CategoryDetails[14] = "0";
    //                                }
    //                            }
    //                            CategoryDetails[16] = "0.00";
    //                            CategoryDetails[16] = "0.00";
    //                            CategoryDetails[30] = totalflyingHours.ToString("0.00");
    //                            if (cbSplitEmpty.Checked)
    //                            {
    //                                CategoryDetails[19] = "1";
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[19] = "0";
    //                            }
    //                            string s = DateTime.Now.ToString("HH:mm:ss.fff");

    //                            TimeSpan ts = TimeSpan.Parse(s);
    //                            CategoryDetails[31] = ts.ToString();
    //                            CategoryDetails[32] = DateTime.Now.Date.ToString();
    //                            if (hdfSCID.Value == "0")
    //                            {
    //                                CategoryDetails[34] = txttrip.Value.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[34] = txttrip.Value.Trim();
    //                            }
    //                            CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
    //                            CategoryDetails[37] = DateTime.Now.Date.ToString();
    //                            CategoryDetails[38] = txttrip.Value.Trim();
    //                            CategoryDetails[39] = txtTailNo.Value.Trim();
    //                            CategoryDetails[45] = "B1";
    //                            CategoryDetails[46] = lblgroupiddisplay.Text;
    //                            CategoryDetails[47] = lbllegdisplay.Text.Trim();
    //                            CategoryDetails[48] = rblBreak.SelectedValue.Trim();
    //                            if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
    //                            {
    //                                CategoryDetails[49] = lblStartData.Text.Trim();
    //                                CategoryDetails[50] = lblendData.Text.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[49] = lblStartData.Text.Trim();
    //                                CategoryDetails[50] = lblendData.Text.Trim();
    //                            }

    //                            string strReturnVal = saveCategoryGroup(CategoryDetails);



    //                        }
    //                        else
    //                        {
    //                            decimal daystoflight = 0;
    //                            decimal L10_minPrice = 0;
    //                            decimal M10_maxPrice = 0;
    //                            decimal price = 0;
    //                            decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());

    //                            var HourlyRate = Convert.ToDecimal("0.00"); ;
    //                            var fixedCeiling = Convert.ToDecimal("0.00");
    //                            var fixedFloor = Convert.ToDecimal("0.00");
    //                            var D4_MinTimeBD = Convert.ToDecimal("0.0");
    //                            var D5_MaxTimeBD = Convert.ToDecimal("0.00");
    //                            var MinFLHSellable = Convert.ToDecimal("0.00");
    //                            var finalPricevalidation2 = (decimal)0;
    //                            var _totalDaysK10 = "0";
    //                            if (rblBreak.SelectedValue.Trim() == "CO")
    //                            {
    //                                var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
    //                                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
    //                                _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
    //                            }
    //                            else
    //                            {
    //                                var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
    //                                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
    //                                _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
    //                            }


    //                            string stractive = string.Empty;

    //                            stractive = "1";

    //                            if (hdfSCID.Value == "0")
    //                            {
    //                                CategoryDetails[0] = "I";
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[0] = "U";
    //                            }
    //                            string atctivetail = string.Empty;

    //                            atctivetail = "1";
    //                            string strEndCity = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
    //                            String[] strlistend = strEndCity.Trim().Split('-');
    //                            string endAirport = string.Empty;
    //                            string endCity = string.Empty;
    //                            if (strlistend.Length > 1)
    //                            {
    //                                endAirport = strlistend[0].Trim();
    //                                endCity = strlistend[1].Trim();
    //                            }

    //                            CategoryDetails[1] = hdfSCID.Value;
    //                            CategoryDetails[2] = txtTailNo.Value.Trim();
    //                            CategoryDetails[3] = txtTailNo.Value.Trim();
    //                            CategoryDetails[4] = txtcountry.Text.Trim();
    //                            CategoryDetails[5] = "";
    //                            if (rblBreak.SelectedValue.Trim() == "CO")
    //                            {
    //                                CategoryDetails[6] = txtCouStartTime.Value.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
    //                            }
    //                            CategoryDetails[7] = endAirport.Trim();
    //                            CategoryDetails[8] = endCity.Trim();
    //                            CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
    //                            CategoryDetails[10] = "";
    //                            CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
    //                            CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
    //                            CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
    //                            //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
    //                            CategoryDetails[15] = "";
    //                            CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
    //                            CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
    //                            CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
    //                            CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
    //                            if (hdfSCID.Value == "0")
    //                            {
    //                                DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
    //                                if (dttailTable.Rows.Count > 0)
    //                                {
    //                                }
    //                                else
    //                                {
    //                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
    //                                }

    //                                DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
    //                                if (dttailTableEx.Rows.Count > 0)
    //                                {
    //                                    CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
    //                                    CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
    //                                    CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
    //                                    CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
    //                                    CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
    //                                    CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                                    CategoryDetails[26] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                    CategoryDetails[28] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[29] = Convert.ToDecimal(0).ToString();
    //                                    CategoryDetails[35] = "1";
    //                                    CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
    //                                    CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
    //                                    CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
    //                                }
    //                                else
    //                                {
    //                                    CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                                    CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                                    CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                                    CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                                    CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                                    CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                                    CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                    CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                                    CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                                    CategoryDetails[35] = "0";
    //                                    CategoryDetails[33] = "1";
    //                                    CategoryDetails[40] = "0";
    //                                    CategoryDetails[14] = "0";
    //                                }
    //                            }

    //                            CategoryDetails[16] = "0.00";
    //                            CategoryDetails[16] = "0.00";
    //                            CategoryDetails[30] = totalflyingHours.ToString("0.00");
    //                            if (cbSplitEmpty.Checked)
    //                            {
    //                                CategoryDetails[19] = "1";
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[19] = "0";
    //                            }
    //                            string s = DateTime.Now.ToString("HH:mm:ss.fff");

    //                            TimeSpan ts = TimeSpan.Parse(s);
    //                            CategoryDetails[31] = ts.ToString();
    //                            CategoryDetails[32] = DateTime.Now.Date.ToString();
    //                            if (hdfSCID.Value == "0")
    //                            {
    //                                CategoryDetails[34] = txttrip.Value.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[34] = txttrip.Value.Trim();
    //                            }
    //                            CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
    //                            CategoryDetails[37] = DateTime.Now.Date.ToString();
    //                            CategoryDetails[38] = txttrip.Value.Trim();
    //                            CategoryDetails[39] = txtTailNo.Value.Trim();
    //                            CategoryDetails[45] = "B1";
    //                            CategoryDetails[46] = lblgroupiddisplay.Text;
    //                            CategoryDetails[47] = lbllegdisplay.Text.Trim();
    //                            CategoryDetails[48] = rblBreak.SelectedValue.Trim();
    //                            if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
    //                            {
    //                                CategoryDetails[49] = lblStartData.Text.Trim();
    //                                CategoryDetails[50] = lblendData.Text.Trim();
    //                            }
    //                            else
    //                            {
    //                                CategoryDetails[49] = "";
    //                                CategoryDetails[50] = "";
    //                            }
    //                            string strReturnVal = saveCategoryGroup(CategoryDetails);
    //                        }


    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.ToString().Trim() + "'");

    //                        string strCountryCode = string.Empty;

    //                        if (txtcountry.Text.Contains("/"))
    //                        {
    //                            string[] strSplitCountry = txtcountry.Text.Trim().Split('/');

    //                            if (strSplitCountry.Length > 1)
    //                            {

    //                                for (int i = 0; i < strSplitCountry.Length; i++)
    //                                {
    //                                    DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + strSplitCountry[i].ToString().Trim() + "' ").Tables[0];

    //                                    if (detr.Rows.Count > 0)
    //                                    {
    //                                        strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
    //                                    }
    //                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Dep', 'Country', '" + strSplitCountry[i].ToString().Replace("'", "''") + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
    //                                }
    //                            }
    //                        }
    //                        else
    //                        {
    //                            DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + txtcountry.Text.Trim() + "' ").Tables[0];

    //                            if (detr.Rows.Count > 0)
    //                            {
    //                                strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
    //                            }

    //                            string strCount = objEarliestTimeDAL.INTExist(strCountryCode.Trim(), "CONT");
    //                            if (Convert.ToInt32(strCount) > 0)
    //                            {
    //                                strDepartTrip = "I";
    //                            }



    //                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Dep', 'Country', '" + txtcountry.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
    //                        }

    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.ToString().Trim() + "'");

    //                        string strstartcity2 = string.Empty;
    //                        strstartcity2 = ddlLeg1EndCity.Text.Replace(",", "-").Replace(" ", "");
    //                        String[] strlistend2 = strstartcity2.Trim().Split('-');
    //                        string startAirport2 = string.Empty;
    //                        string startcity2 = string.Empty;
    //                        if (strlistend2.Length > 1)
    //                        {
    //                            startAirport2 = strlistend2[0].Trim();
    //                            startcity2 = strlistend2[1].Trim();
    //                        }

    //                        string strCount1 = objEarliestTimeDAL.INTExist(startAirport2.Trim(), "AIRT");
    //                        if (Convert.ToInt32(strCount1) > 0)
    //                        {
    //                            strArrTrip = "I";
    //                        }

    //                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Arr', 'Airport', '" + ddlLeg1EndCity.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + startAirport2.Trim() + "')");

    //                        #endregion
    //                    }

    //                    #region Breakdown by City

    //                    else if (rblBreak.SelectedValue.Trim() == "CT")
    //                    {
    //                        if (gvLegDetails.Rows.Count > 0)
    //                        {
    //                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE  ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.Trim() + "'");

    //                            for (int i = 0; i < gvLegDetails.Rows.Count; i++)
    //                            {
    //                                string strTriplag = gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim();

    //                                DataTable dtexist1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='" + strTriplag + "' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

    //                                #region Leg2

    //                                if (dtexist1.Rows.Count > 0)
    //                                {
    //                                    decimal daystoflight = 0;
    //                                    decimal L10_minPrice = 0;
    //                                    decimal M10_maxPrice = 0;
    //                                    decimal price = 0;

    //                                    decimal totalflyingHours = Convert.ToDecimal(gvLegDetails.Rows[i].FindControl("lblFlyHours").ToString().Trim());
    //                                    var HourlyRate = Convert.ToDecimal("0.00"); ;
    //                                    var fixedCeiling = Convert.ToDecimal("0.00");
    //                                    var fixedFloor = Convert.ToDecimal("0.00");
    //                                    var D4_MinTimeBD = Convert.ToDecimal("0.0");
    //                                    var D5_MaxTimeBD = Convert.ToDecimal("0.00");
    //                                    var MinFLHSellable = Convert.ToDecimal("0.00");
    //                                    var finalPricevalidation2 = (decimal)0;
    //                                    string strStartTime = gvLegDetails.Rows[i].FindControl("lblstarttime").ToString().Trim();
    //                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
    //                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
    //                                    var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

    //                                    string stractive = string.Empty;
    //                                    stractive = "1";
    //                                    CategoryDetails[0] = "U";
    //                                    string atctivetail = string.Empty;
    //                                    atctivetail = "1";

    //                                    CategoryDetails[1] = hdfSCID.Value;
    //                                    CategoryDetails[2] = txtTailNo.Value.Trim();
    //                                    CategoryDetails[3] = txtTailNo.Value.Trim();
    //                                    CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
    //                                    CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
    //                                    CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
    //                                    CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
    //                                    CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
    //                                    CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
    //                                    CategoryDetails[10] = "";
    //                                    CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
    //                                    CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
    //                                    CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
    //                                    CategoryDetails[15] = "";
    //                                    CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
    //                                    CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
    //                                    CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
    //                                    CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
    //                                    if (hdfSCID.Value == "0")
    //                                    {
    //                                        DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
    //                                        if (dttailTable.Rows.Count > 0)
    //                                        {
    //                                        }
    //                                        else
    //                                        {
    //                                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
    //                                        }

    //                                        DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
    //                                        if (dttailTableEx.Rows.Count > 0)
    //                                        {
    //                                            CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
    //                                            CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
    //                                            CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
    //                                            CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
    //                                            CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
    //                                            CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                                            CategoryDetails[26] = Convert.ToDecimal(0).ToString();
    //                                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                            CategoryDetails[28] = Convert.ToDecimal(0).ToString();
    //                                            CategoryDetails[29] = Convert.ToDecimal(0).ToString();
    //                                            CategoryDetails[35] = "1";
    //                                            CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
    //                                            CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
    //                                            CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
    //                                        }
    //                                        else
    //                                        {
    //                                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                                            CategoryDetails[35] = "0";
    //                                            CategoryDetails[33] = "1";
    //                                            CategoryDetails[40] = "0";
    //                                            CategoryDetails[14] = "0";
    //                                        }
    //                                    }
    //                                    CategoryDetails[16] = "0.00";
    //                                    CategoryDetails[16] = "0.00";
    //                                    CategoryDetails[30] = totalflyingHours.ToString("0.00");
    //                                    if (cbSplitEmpty.Checked)
    //                                    {
    //                                        CategoryDetails[19] = "1";
    //                                    }
    //                                    else
    //                                    {
    //                                        CategoryDetails[19] = "0";
    //                                    }
    //                                    string s = DateTime.Now.ToString("HH:mm:ss.fff");

    //                                    TimeSpan ts = TimeSpan.Parse(s);
    //                                    CategoryDetails[31] = ts.ToString();
    //                                    CategoryDetails[32] = DateTime.Now.Date.ToString();
    //                                    if (hdfSCID.Value == "0")
    //                                    {
    //                                        CategoryDetails[34] = txttrip.Value.Trim();
    //                                    }
    //                                    else
    //                                    {
    //                                        CategoryDetails[34] = txttrip.Value.Trim();
    //                                    }
    //                                    CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
    //                                    CategoryDetails[37] = DateTime.Now.Date.ToString();
    //                                    CategoryDetails[38] = txttrip.Value.Trim();
    //                                    CategoryDetails[39] = txtTailNo.Value.Trim();
    //                                    CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
    //                                    CategoryDetails[46] = lblgroupiddisplay.Text;
    //                                    CategoryDetails[47] = lbllegdisplay.Text.Trim();
    //                                    CategoryDetails[48] = rblBreak.SelectedValue.Trim();
    //                                    if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
    //                                    {
    //                                        CategoryDetails[49] = lblStartData.Text.Trim();
    //                                        CategoryDetails[50] = lblendData.Text.Trim();
    //                                    }
    //                                    else
    //                                    {
    //                                        CategoryDetails[49] = lblStartData.Text.Trim();
    //                                        CategoryDetails[50] = lblendData.Text.Trim();
    //                                    }
    //                                    string strReturnVal = saveCategoryGroup(CategoryDetails);
    //                                }
    //                                else
    //                                {
    //                                    decimal daystoflight = 0;
    //                                    decimal L10_minPrice = 0;
    //                                    decimal M10_maxPrice = 0;
    //                                    decimal price = 0;
    //                                    decimal totalflyingHours = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim());

    //                                    var HourlyRate = Convert.ToDecimal("0.00"); ;
    //                                    var fixedCeiling = Convert.ToDecimal("0.00");
    //                                    var fixedFloor = Convert.ToDecimal("0.00");
    //                                    var D4_MinTimeBD = Convert.ToDecimal("0.0");
    //                                    var D5_MaxTimeBD = Convert.ToDecimal("0.00");
    //                                    var MinFLHSellable = Convert.ToDecimal("0.00");
    //                                    var finalPricevalidation2 = (decimal)0;
    //                                    string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString().Trim();

    //                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
    //                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
    //                                    var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");


    //                                    string stractive = string.Empty;

    //                                    stractive = "1";

    //                                    if (hdfSCID.Value == "0")
    //                                    {
    //                                        CategoryDetails[0] = "I";
    //                                    }
    //                                    else
    //                                    {
    //                                        CategoryDetails[0] = "U";
    //                                    }
    //                                    string atctivetail = string.Empty;

    //                                    atctivetail = "1";
    //                                    CategoryDetails[1] = hdfSCID.Value;
    //                                    CategoryDetails[2] = txtTailNo.Value.Trim();
    //                                    CategoryDetails[3] = txtTailNo.Value.Trim();
    //                                    CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
    //                                    CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
    //                                    CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
    //                                    CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
    //                                    CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
    //                                    CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
    //                                    CategoryDetails[10] = "";
    //                                    CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim();
    //                                    CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
    //                                    CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
    //                                    CategoryDetails[15] = "";
    //                                    CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
    //                                    CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
    //                                    CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
    //                                    CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
    //                                    if (hdfSCID.Value == "0")
    //                                    {
    //                                        DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
    //                                        if (dttailTable.Rows.Count > 0)
    //                                        {
    //                                        }
    //                                        else
    //                                        {
    //                                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
    //                                        }

    //                                        DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
    //                                        if (dttailTableEx.Rows.Count > 0)
    //                                        {
    //                                            CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
    //                                            CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
    //                                            CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
    //                                            CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
    //                                            CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
    //                                            CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
    //                                            CategoryDetails[26] = Convert.ToDecimal(0).ToString();
    //                                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                            CategoryDetails[28] = Convert.ToDecimal(0).ToString();
    //                                            CategoryDetails[29] = Convert.ToDecimal(0).ToString();
    //                                            CategoryDetails[35] = "1";
    //                                            CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
    //                                            CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
    //                                            CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
    //                                        }
    //                                        else
    //                                        {
    //                                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
    //                                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
    //                                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
    //                                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
    //                                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
    //                                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
    //                                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
    //                                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
    //                                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
    //                                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
    //                                            CategoryDetails[35] = "0";
    //                                            CategoryDetails[33] = "1";
    //                                            CategoryDetails[40] = "0";
    //                                            CategoryDetails[14] = "0";
    //                                        }
    //                                    }

    //                                    CategoryDetails[16] = "0.00";
    //                                    CategoryDetails[16] = "0.00";
    //                                    CategoryDetails[30] = totalflyingHours.ToString("0.00");
    //                                    if (chkEmpty.Checked)
    //                                    {
    //                                        CategoryDetails[19] = "1";
    //                                    }
    //                                    else
    //                                    {
    //                                        CategoryDetails[19] = "0";
    //                                    }
    //                                    string s = DateTime.Now.ToString("HH:mm:ss.fff");

    //                                    TimeSpan ts = TimeSpan.Parse(s);
    //                                    CategoryDetails[31] = ts.ToString();
    //                                    CategoryDetails[32] = DateTime.Now.Date.ToString();
    //                                    if (hdfSCID.Value == "0")
    //                                    {
    //                                        CategoryDetails[34] = txttrip.Value.Trim();
    //                                    }
    //                                    else
    //                                    {
    //                                        CategoryDetails[34] = txttrip.Value.Trim();
    //                                    }
    //                                    CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
    //                                    CategoryDetails[37] = DateTime.Now.Date.ToString();
    //                                    CategoryDetails[38] = txttrip.Value.Trim();
    //                                    CategoryDetails[39] = txtTailNo.Value.Trim();
    //                                    CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
    //                                    CategoryDetails[46] = lblgroupiddisplay.Text;
    //                                    CategoryDetails[47] = lbllegdisplay.Text.Trim();
    //                                    CategoryDetails[48] = rblBreak.SelectedValue.Trim();
    //                                    if (lblEModFlag.Text.Trim() == "MF" || lblEModFlag.Text.Trim() == "ET")
    //                                    {
    //                                        CategoryDetails[49] = lblStartData.Text.Trim();
    //                                        CategoryDetails[50] = lblendData.Text.Trim();
    //                                    }
    //                                    else
    //                                    {
    //                                        CategoryDetails[49] = lblStartData.Text.Trim();
    //                                        CategoryDetails[50] = lblendData.Text.Trim();
    //                                    }
    //                                    string strReturnVal = saveCategoryGroup(CategoryDetails);
    //                                }

    //                                #endregion


    //                                if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
    //                                {
    //                                    string strCount = "0";
    //                                    string strStartAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
    //                                    string strStartCity = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;

    //                                    string strEndAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
    //                                    string strEndCity = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;

    //                                    strCount = objEarliestTimeDAL.INTExist(strStartAirport.Trim(), "AIRT");
    //                                    if (Convert.ToInt32(strCount) > 0)
    //                                    {
    //                                        strDepartTrip = "I";
    //                                    }

    //                                    strCount = objEarliestTimeDAL.INTExist(strEndAirport.Trim(), "AIRT");
    //                                    if (Convert.ToInt32(strCount) > 0)
    //                                    {
    //                                        strArrTrip = "I";
    //                                    }

    //                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + lblgroupiddisplay.Text.Trim() + "','SP','Dep', 'Airport', '" + strStartCity + " - " + strStartAirport + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strStartAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");

    //                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,ParentFlag,ParentID) VALUES ('" + lblgroupiddisplay.Text.Trim() + "','SP','Arr', 'Airport', '" + strEndCity + " - " + strEndAirport + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strEndAirport.Trim() + "','" + strParentFlag.Trim() + "','" + strParentID.Trim() + "')");

    //                                }
    //                            }
    //                        }
    //                    }

    //                    #endregion
    //                }
    //            }

    //            #endregion
    //        }

    //        #endregion

    //        #region Livepricecalculation

    //        objEmptyLegPriceCal.FunEmptyPriceCal();

    //        #endregion

    //        lblalert.Text = "Empty leg details have been saved successfully";

    //        if (strDepartTrip == "I" && strArrTrip == "I")
    //        {
    //            lblalert.Text = "Empty leg details have been saved successfully.<br/> These trip not allowed for booking.";
    //        }
    //        mpealert.Show();
    //        hdfSCID.Value = "0";
    //        BindEmptyLegs();
    //        tblForm.Visible = false;
    //        tblGrid.Visible = true;
    //        rblStatus.Visible = true;
    //        btnAdd.Visible = true;
    //        btnSave.Visible = false;
    //        btnView.Visible = false;
    //    }
    //    catch (Exception ex)
    //    {

    //        int linenum = 0;
    //        linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + " " + linenum + "');", true);
    //    }
    //}
    //    protected void gvTailDetails_SelectedIndexChanged(object sender, EventArgs e)
    //    {
    //        try
    //        {
    //            ViewState["dtLegDetails"] = null;
    //            gvLegDetails.DataSource = null;
    //            gvLegDetails.DataBind();

    //            ViewState["dtDepAirport"] = null;
    //            gvDepAirport.DataSource = null;
    //            gvDepAirport.DataBind();
    //            txtDepDisLandPage.Text = string.Empty;
    //            divDepLan.Visible = false;
    //            ddlDepSetupBy.ClearSelection();


    //            ViewState["dtArrAirport"] = null;
    //            gvArrivalAirport.DataSource = null;
    //            gvArrivalAirport.DataBind();
    //            txtArrDisLandPage.Text = string.Empty;
    //            divArrLan.Visible = false;
    //            ddlArrSetupBy.ClearSelection();
    //            lblModGroupID.Text = "0";
    //            trSplit.Visible = false;
    //            btnSave.Visible = true;
    //            btnView.Visible = true;
    //            btnAdd.Visible = false;
    //            rblStatus.Visible = false;
    //            trManualandModiy.Visible = false;
    //            divModify.Visible = true;
    //            divManualAirport.Visible = true;
    //            lblEarliestTime.Text = string.Empty;
    //            lblMaxDepartTime.Text = string.Empty;
    //            lblMinDepartTime.Text = string.Empty;
    //            dvEarliestDepart.Visible = false;


    //            divPGLMBy.Visible = false;

    //            divPGLMBy1.Visible = false;
    //            if (((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text != string.Empty)
    //            {
    //                divPGLMBy.Visible = true;
    //                lblPGLMby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text + " ( on ) " + ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text;
    //                divPGLMBy1.Visible = true;
    //                lblPGLMby1.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text + " ( on ) " + ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text;

    //            }
    //            Label TailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
    //            Label lbltrip = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
    //            Label lblType = (Label)gvTailDetails.SelectedRow.FindControl("lblType");
    //            Label lblGroupId = (Label)gvTailDetails.SelectedRow.FindControl("lblGroupId");
    //            Label lblMFlag = (Label)gvTailDetails.SelectedRow.FindControl("lblModFlag");
    //            lblEModFlag.Text = lblMFlag.Text;





    //            lblEarliestTime.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblEarliestDepartTime")).Text;
    //            lblEarliestTimeLabel.Text = string.Empty;
    //            lblEarliestTime1.Text = string.Empty;
    //            if (lblEarliestTime.Text != string.Empty)
    //            {
    //                lblEarliestTimeLabel.Text = "Earliest Departure Time (in FOS)";
    //                lblEarliestTimeLabel1.Text = "Earliest Departure Time (in FOS)";
    //                lblEarliestTime1.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MMM dd yyyy HH:mm");
    //                lblEarliestTime.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MMM dd yyyy HH:mm");
    //            }

    //            string MINDepartTime = string.Empty;
    //            string MAXDepartTime = string.Empty;

    //            Label lblTailNo1 = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
    //            Label lblleg1 = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
    //            Label lblFlyingHours1 = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");

    //            if (lblleg1.Text != string.Empty)
    //            {
    //                objEarliestTimeBLL.getTripsLeg(lbltrip.Text, lblleg1.Text, lblTailNo1.Text, lblFlyingHours1.Text,
    //                    ref   MINDepartTime, ref   MAXDepartTime);

    //                lblMinDepartTime.Text = MINDepartTime;
    //                lblMaxDepartTime.Text = MAXDepartTime;
    //                if (lblMinDepartTime.Text != string.Empty)
    //                {
    //                    lblEarliestTimeLabel.Text = "Earliest Departure Time  ";
    //                    lblEarliestTimeLabel1.Text = "Earliest Departure Time ";
    //                    lblEarliestTime1.Text = Convert.ToDateTime(lblMinDepartTime.Text).ToString("MMM dd yyyy HH:mm");
    //                    lblEarliestTime.Text = Convert.ToDateTime(lblMinDepartTime.Text).ToString("MMM dd yyyy HH:mm");

    //                    lblMinDepartTime.Text = Convert.ToDateTime(MINDepartTime).ToString("MMM dd yyyy HH:mm");
    //                    lblMinDepartTime.Text = Convert.ToDateTime(MINDepartTime).ToString("MMM dd yyyy HH:mm");
    //                    lblMaxDepartTime.Text = Convert.ToDateTime(MAXDepartTime).ToString("MMM dd yyyy HH:mm");
    //                    lblMaxDepartTime1.Text = Convert.ToDateTime(MAXDepartTime).ToString("MMM dd yyyy HH:mm");

    //                }
    //                dvEarliestDepart.Visible = true;
    //            }


    //            Loadcountry();

    //            #region

    //            if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text == "0")
    //            {
    //                tbldata.Visible = true;
    //                divPriceModRight.Visible = true;
    //                divPricModLeft.Visible = true;
    //                divManualAirport.Visible = false;
    //                divModify.Visible = true;
    //                lblSubScren.Text = "Modify a FOS Trip";
    //                lblModGroupID.Text = "0";
    //                trManualandModiy.Visible = true;
    //                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
    //                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
    //                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
    //                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
    //                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
    //                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
    //                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
    //                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
    //                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
    //                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
    //                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
    //                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
    //                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
    //                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");


    //                //Earlist DepartTime





    //                hdfSCID.Value = "0";
    //                LBLTRIPNO.Text = lbltripnoG.Text.Trim();

    //                lblTailData.Text = lblTailNo.Text;
    //                lblTripData.Text = lbltripnoG.Text;
    //                lblStartData.Text = lblstart.Text;
    //                lblendData.Text = lblend.Text;
    //                lblscitydata.Text = lblstartcity.Text;
    //                lblEcityData.Text = lblEndcity.Text;
    //                lblStimedata.Text = lblStarttime.Text;
    //                lblEtimeData.Text = lblEndTime.Text;
    //                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
    //                lbllegdisplay.Text = lblleg.Text;

    //                txtTailNo.Value = lblTailNo.Text;
    //                txttrip.Value = lblTripData.Text;
    //                txtFlyHours1.Value = lblFlyingHours.Text;
    //                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "").Replace(".00", "");

    //                lblStimedata1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStimedata.Text;
    //                lblEtimeData1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEtimeData.Text;




    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
    //                //                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:GetDepAirport();", true);
    //                txtMod_DepCity.Visible = true;
    //                txtModDepState.Visible = false;
    //                txtModDepCountry.Visible = false;
    //                txtstartsplit.Value = lblstartcity.Text + " - " + lblstart.Text;

    //                txtEndCity.Text = lblEndcity.Text + " - " + lblend.Text;

    //                txtStartCity.Text = lblstartcity.Text + " - " + lblstart.Text;



    //                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
    //                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");

    //                if (lblMinDepartTime.Text != string.Empty && lblMaxDepartTime.Text != string.Empty)
    //                {
    //                    if (Convert.ToDateTime(lblMinDepartTime.Text) <= System.DateTime.Now)
    //                    {
    //                        txtAvaFrom.Text = System.DateTime.Now.AddHours(6).ToString("MM-dd-yyyy");
    //                        txtStartTime.Value = System.DateTime.Now.AddHours(6).ToString("HH:mm");
    //                    }
    //                    else
    //                    {
    //                        txtAvaFrom.Text = Convert.ToDateTime(lblMinDepartTime.Text).ToString("MM-dd-yyyy");
    //                        txtStartTime.Value = Convert.ToDateTime(lblMinDepartTime.Text).ToString("HH:mm");
    //                    }

    //                    txtAvaTo.Text = Convert.ToDateTime(lblMaxDepartTime.Text).ToString("MM-dd-yyyy");
    //                    txtEndTime.Value = Convert.ToDateTime(lblMaxDepartTime.Text).ToString("HH:mm");

    //                }
    //                else
    //                {
    //                    txtStartTime.Value = "00:00";
    //                    txtEndTime.Value = "23:00";

    //                }

    //                //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
    //                //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

    //                //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
    //                //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

    //                if (lblMFlag.Text.Trim() == "ET" && lblGroupId.Text.Trim() == "0")
    //                {
    //                    DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid, New_StartAirport,New_EndAirport  FROM  OneWayTail_Setup  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "'").Tables[0];

    //                    if (dtOneWayTSel.Rows.Count > 0)
    //                    {
    //                        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and ModifiedFlag='ET' and AirportFor='Dep'").Tables[0];

    //                        if (dt.Rows.Count > 0)
    //                        {
    //                            gvDepAirport.DataSource = dt;
    //                            gvDepAirport.DataBind();
    //                            ViewState["dtDepAirport"] = dt;
    //                            divDepLan.Visible = true;
    //                            txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
    //                        }
    //                        else
    //                        {
    //                            divDepLan.Visible = false;
    //                        }

    //                        DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and ModifiedFlag='ET' and AirportFor='Arr'").Tables[0];

    //                        if (dtArr.Rows.Count > 0)
    //                        {
    //                            gvArrivalAirport.DataSource = dtArr;
    //                            gvArrivalAirport.DataBind();
    //                            ViewState["dtArrAirport"] = dtArr;
    //                            txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
    //                            divArrLan.Visible = true;
    //                        }
    //                        else
    //                        {
    //                            divArrLan.Visible = false;
    //                        }
    //                    }

    //                }


    //                lblgroup.Text = "Modify";
    //                txtTailNo.Disabled = true;
    //                txttrip.Disabled = true;
    //                tbldata.Visible = true;
    //                divstartcity.Visible = true;
    //                divstartcityLeft.Visible = true;
    //                divstartcounty.Attributes.Add("style", "display:none");
    //                divstartsplit.Visible = false;
    //                divstartsplitLeft.Attributes.Add("style", "display:none");
    //                txtstartsplit.Disabled = true;
    //                divendcity.Visible = true;
    //                btnSave.Visible = true;
    //                tblForm.Visible = true;
    //                tblGrid.Visible = false;
    //                if (lblMFlag.Text == "ET")
    //                {
    //                    lblscitydata.Visible = false;
    //                    lblEcityData.Visible = false;
    //                }
    //                else
    //                {
    //                    lblscitydata.Visible = true;
    //                    lblEcityData.Visible = true;
    //                }
    //            }

    //            #endregion

    //            #region Edited Split

    //            else if (lblType.Text.Trim() == "Split" && lblGroupId.Text != "0")
    //            {

    //                ddlStartCitySplit.Enabled = false;
    //                lblSubScren.Text = "Add a Manual Leg";
    //                divPriceModRight.Visible = false;
    //                divPricModLeft.Visible = false;
    //                trManualandModiy.Visible = false;
    //                DataTable dtPS = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag<>'B0' Order by groupid").Tables[0];

    //                lblgroupiddisplay.Text = lblGroupId.Text.Trim();
    //                lblModGroupID.Text = lblGroupId.Text.Trim();
    //                trSplit.Visible = true;
    //                tbldata.Visible = true;
    //                divstartcounty.Attributes.Add("style", "display:none");
    //                divstartsplitLeft.Attributes.Add("style", "display:''");
    //                //trLeg1.Attributes.Add("style", "display:''");
    //                trLeg2.Attributes.Add("style", "display:''");

    //                if (dtPS != null && dtPS.Rows.Count > 0)
    //                {
    //                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
    //                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
    //                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
    //                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
    //                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

    //                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

    //                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

    //                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

    //                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

    //                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
    //                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


    //                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

    //                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

    //                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");





    //                    //  lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;

    //                    //lblupdatedon.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text;


    //                    txtSpliFlyHour.Value = lblFlyingHours.Text;
    //                    txtNoSeats.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
    //                    txtSpliFlyHour.Value = lblFlyingHours.Text;
    //                    txtMPrice.Value = dtPS.Rows[0]["CurrentPrice"].ToString();

    //                    lblTailData.Text = lblTailNo.Text;
    //                    lblTripData.Text = lbltripnoG.Text;
    //                    lblStartData.Text = lblstart.Text;
    //                    lblendData.Text = lblend.Text;
    //                    lblscitydata.Text = lblstartcity.Text;
    //                    lblEcityData.Text = lblEndcity.Text;
    //                    lblStimedata.Text = lblStarttime.Text;
    //                    lblEtimeData.Text = lblEndTime.Text;
    //                    lblDateData.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MMM dd, yyyy").ToUpper();
    //                    lblstartdate.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MM-dd-yyyy");
    //                    lblenddate.Text = Convert.ToDateTime(dtPS.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
    //                    lbllegdisplay.Text = lblleg.Text;

    //                    lblStimedata1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStimedata.Text;
    //                    lblEtimeData1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEtimeData.Text;



    //                    txtTailNo.Value = lblTailNo.Text;
    //                    txttrip.Value = lbltripnoG.Text;
    //                    LBLTRIPNO.Text = txttrip.Value.Trim();

    //                    txtcountry.Text = string.Empty;
    //                    DataTable dtbase2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
    //                    if (dtbase2.Rows.Count > 0)
    //                    {
    //                        if (dtbase2.Rows[0]["tripflag"].ToString() == "B1")
    //                        {

    //                            DataTable dtPS2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag='B1' Order by groupid").Tables[0];
    //                            // txtStartCity.Text = dtPS2.Rows[0]["Endcity"].ToString() + " - " + dtPS2.Rows[0]["End"].ToString();
    //                            string strSpeed = string.Empty;
    //                            DataTable dtPS1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup Where TailNo='" + txtTailNo.Value + "'").Tables[0];
    //                            if (dtPS1.Rows.Count > 0)
    //                            {
    //                                strSpeed = dtPS1.Rows[0]["AvgSpeed"].ToString();
    //                            }
    //                            DataTable dtResul = new DataTable();
    //                            try
    //                            {

    //                                dtResul = AirRouteCalculation(dtPS2.Rows[0]["End"].ToString(), lblendData.Text, strSpeed);
    //                            }
    //                            catch (Exception ex)
    //                            {

    //                            }

    //                            if (dtResul.Rows.Count > 0)
    //                            {
    //                                txtSpliFlyHour.Value = (Math.Round(Convert.ToDecimal(dtResul.Rows[0]["flight_time_min"].ToString()) / 60, 2)).ToString();
    //                            }
    //                        }
    //                    }

    //                    DataTable dtpreference = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select TailNo as TailName,   TailNo, convert(varchar(12),DDate,109) DDate ,convert(varchar(12),ADate,109) ADate ,DDate [DateFormat],  CONVERT(VARCHAR(5), StartTime, 108) DTime,
    //		                                                CONVERT(VARCHAR(5), EndTime, 108)  ATime,Start+'-->'+[End] [routes], Start, StartCity, [End], EndCity, isnull(NoOFSeats,0) NoOFSeats,
    //		                                                FlyingHoursFOS, CalcualtedFlyingHrs FLTTIMEMINS,TripNo,
    //		                                                DaysToFlight,tripflag,Groupid,splittype from LivePricingCaculation where Groupid is not null and 
    //                                                        Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL'  Order by groupid").Tables[0];

    //                    if (dtpreference.Rows.Count > 0)
    //                    {
    //                        dtpreference.Columns.Add("leg");

    //                        for (int i = 0; i < dtpreference.Rows.Count; i++)
    //                        {
    //                            DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
    //                Where TailNo='" + dtpreference.Rows[i]["Tailno"].ToString() + @"' and tripno='" + dtpreference.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(dtpreference.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(dtpreference.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(dtpreference.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + lblstart.Text + @"'
    //                and EndAirport='" + lblend.Text + @"'").Tables[0];
    //                            if (dtleg.Rows.Count > 0)
    //                            {
    //                                dtpreference.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
    //                            }
    //                        }

    //                        for (int i = 0; i < dtpreference.Rows.Count; i++)
    //                        {
    //                            if (i > 0)
    //                            {
    //                                dtpreference.Rows[i]["leg"] = dtpreference.Rows[0]["leg"].ToString();
    //                            }
    //                        }

    //                        ViewState["Tripgrid"] = dtpreference;

    //                        DataTable dt = new DataTable();
    //                        dtpreference.DefaultView.Sort = "Tripflag asc";
    //                        dt = dtpreference.DefaultView.ToTable();
    //                        rblBreak.SelectedValue = "CO";
    //                        DataRow[] result1 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag='B1' and splittype='CO'");

    //                        if (result1.Length > 0)
    //                        {
    //                            divstartcounty.Attributes.Add("style", "display:''");
    //                            divstartsplitLeft.Attributes.Add("style", "display:none");
    //                            //trLeg1.Attributes.Add("style", "display:none");
    //                            trLeg2.Attributes.Add("style", "display:none");

    //                            rblBreak.SelectedValue = "CO";
    //                            DataTable dttemp1 = result1.CopyToDataTable();
    //                            hdfSCID.Value = "0";
    //                            LBLTRIPNO.Text = dttemp1.Rows[0]["tripno"].ToString();
    //                            lbloldtail.Text = dttemp1.Rows[0]["tailno"].ToString();
    //                            txttrip.Value = dttemp1.Rows[0]["tripno"].ToString();
    //                            txtTailNo.Value = dttemp1.Rows[0]["TailNo"].ToString();
    //                            txtCouStartTime.Value = dttemp1.Rows[0]["DTime"].ToString();
    //                            txtSplitLeg1EndTime.Text = dttemp1.Rows[0]["ATime"].ToString();
    //                            txtSpliFlyHour.Value = dttemp1.Rows[0]["FlyinghoursFOS"].ToString().Replace(":", ".");

    //                            DataTable dtTake = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select Mod_AvailableTo,Mod_AvilableFrom,Mod_EndAirport,Mod_Endcity,Mod_StartAirport,Mod_Startcity from Tailsetup_tripDetails where Groupid='" + lblGroupId.Text.Trim() + "' and TripFlag='B1' and  ModifyFlag='SP'  and Mod_AvilableFrom!='1900-01-01 00:00:00.000'").Tables[0];

    //                            if (dtTake.Rows.Count > 0)
    //                            {
    //                                txtSplitAvaFrom.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
    //                                txtSplitAvaTo.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");
    //                                ddlLeg1EndCity.Text = dtTake.Rows[0]["Mod_Endcity"].ToString() + " - " + dtTake.Rows[0]["Mod_EndAirport"].ToString();
    //                            }
    //                            else
    //                            {
    //                                txtSplitAvaFrom.Text = Convert.ToDateTime(dttemp1.Rows[0]["DDate"].ToString()).ToString("MM-dd-yyyy");
    //                                txtSplitAvaTo.Text = Convert.ToDateTime(dttemp1.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
    //                            }

    //                            if (Convert.ToDateTime(txtSplitAvaFrom.Text).Date < System.DateTime.Now.Date)
    //                            {
    //                                if (lblEarliestTime.Text != string.Empty)
    //                                    txtSplitAvaFrom.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MM-dd-yyyy");
    //                            }
    //                            txtNoSeats.Value = dttemp1.Rows[0]["NoOFSeats"].ToString();
    //                            txtcountry.Text = dttemp1.Rows[0]["Start"].ToString();
    //                            divstartcounty.Attributes.Add("style", "display:''");

    //                        }
    //                        DataRow[] result2 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag in ('B1','B2','B3') and splittype='CT' ");

    //                        if (result2.Length > 0)
    //                        {
    //                            divstartcounty.Attributes.Add("style", "display:none");
    //                            divstartsplitLeft.Attributes.Add("style", "display:''");
    //                            //trLeg1.Attributes.Add("style", "display:''");
    //                            trLeg2.Attributes.Add("style", "display:''");
    //                            rblBreak.SelectedValue = "CT";


    //                            ddlStartCitySplit.Text = string.Empty;
    //                            ddlLeg1EndCity.Text = string.Empty;
    //                            txtSpliFlyHour.Value = string.Empty;
    //                            txtSplitLeg1StartTime.Text = string.Empty;
    //                            txtSplitLeg1EndTime.Text = string.Empty;

    //                            DataTable dttemp1 = result2.CopyToDataTable();
    //                            ViewState["dtLegDetails"] = dttemp1;
    //                            gvLegDetails.DataSource = dttemp1;
    //                            gvLegDetails.DataBind();
    //                            if (gvLegDetails.Rows.Count == 3)
    //                            {
    //                                btnAddLeg.Visible = false;
    //                                divSplitEnd.Visible = false;
    //                                divSplitFlyHour.Visible = false;
    //                                divstartsplitLeft.Attributes.Add("style", "display:none;");
    //                            }
    //                            else
    //                            {

    //                                Label lblendair = (Label)gvLegDetails.Rows[gvLegDetails.Rows.Count - 1].FindControl("lblendair");

    //                                DataTable dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + lblendair.Text + "'");

    //                                if (dtcity.Rows.Count > 0)
    //                                {
    //                                    ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

    //                                }
    //                                else
    //                                {
    //                                    ddlStartCitySplit.Text = string.Empty;
    //                                }




    //                                btnAddLeg.Visible = true;
    //                                divSplitEnd.Visible = true;
    //                                divSplitFlyHour.Visible = true;
    //                                divstartsplitLeft.Attributes.Add("style", "display:'';");
    //                            }
    //                            DataTable dtTake = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select Mod_AvailableTo,Mod_AvilableFrom,Mod_EndAirport,Mod_Endcity,Mod_StartAirport,Mod_Startcity from Tailsetup_tripDetails where Groupid='" + lblGroupId.Text.Trim() + "' and TripFlag='B1' and  ModifyFlag='SP'  and Mod_AvilableFrom!='1900-01-01 00:00:00.000'").Tables[0];

    //                            if (dtTake.Rows.Count > 0)
    //                            {
    //                                txtSplitAvaFrom.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
    //                                txtSplitAvaTo.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");
    //                                //  ddlLeg1EndCity.Text = dtTake.Rows[0]["Mod_Endcity"].ToString() + " - " + dtTake.Rows[0]["Mod_EndAirport"].ToString();
    //                            }
    //                            else
    //                            {
    //                                txtSplitAvaFrom.Text = Convert.ToDateTime(dttemp1.Rows[0]["DDate"].ToString()).ToString("MM-dd-yyyy");
    //                                txtSplitAvaTo.Text = Convert.ToDateTime(dttemp1.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
    //                            }

    //                            if (Convert.ToDateTime(txtSplitAvaFrom.Text).Date < System.DateTime.Now.Date)
    //                            {
    //                                if (lblEarliestTime.Text != string.Empty)
    //                                    txtSplitAvaFrom.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MM-dd-yyyy");
    //                            }
    //                            hdfSCID.Value = "0";
    //                        }
    //                        else
    //                        {
    //                            ViewState["dtLegDetails"] = null;
    //                            gvLegDetails.DataSource = null;
    //                            gvLegDetails.DataBind();
    //                            divSplitEnd.Visible = true;
    //                            divSplitFlyHour.Visible = true;
    //                        }
    //                    }
    //                }
    //                DataTable dtbase = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
    //                if (dtbase.Rows.Count > 0)
    //                {
    //                    if (dtbase.Rows[0]["tripflag"].ToString() == "B0")
    //                    {
    //                        lblbaseflag.Text = "B1";
    //                    }
    //                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B1")
    //                    {
    //                        lblbaseflag.Text = "B2";
    //                    }
    //                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B2")
    //                    {
    //                        lblbaseflag.Text = "B3";
    //                    }
    //                }

    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Split();", true);
    //                btnSave.Text = "Update";
    //                tblForm.Visible = true;
    //                tblGrid.Visible = false;
    //                lblscitydata.Visible = false;
    //                lblEcityData.Visible = false;
    //                lblgroup.Text = "Group";
    //            }

    //            #endregion

    //            #region New Split

    //            else if ((lblType.Text.Trim() == "Split" && lblGroupId.Text == "0"))
    //            {
    //                trSplit.Visible = true;
    //                divPriceModRight.Visible = false;
    //                divPricModLeft.Visible = false;
    //                lblSubScren.Text = "Add a Manual Leg";
    //                trManualandModiy.Visible = false;
    //                lblModGroupID.Text = "0";
    //                // txtcountry.Text = "United States";
    //                rblBreak.SelectedValue = "CT";
    //                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
    //                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
    //                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
    //                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
    //                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

    //                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

    //                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

    //                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

    //                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

    //                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
    //                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


    //                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

    //                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

    //                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");





    //                LBLTRIPNO.Text = lbltripnoG.Text.Trim();
    //                lblTailData.Text = lblTailNo.Text;
    //                lblTripData.Text = lbltripnoG.Text;
    //                lblStartData.Text = lblstart.Text;
    //                lblendData.Text = lblend.Text;
    //                lblscitydata.Text = lblstartcity.Text;
    //                lblEcityData.Text = lblEndcity.Text;
    //                lblStimedata.Text = lblStarttime.Text;
    //                lblEtimeData.Text = lblEndTime.Text;
    //                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
    //                lbllegdisplay.Text = lblleg.Text;

    //                lblStimedata1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStimedata.Text;
    //                lblEtimeData1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEtimeData.Text;



    //                txtTailNo.Value = lblTailNo.Text;
    //                txttrip.Value = lbltripnoG.Text;
    //                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
    //                hdnstarttime.Value = lblStarttime.Text;
    //                hdnendtime.Value = lblEndTime.Text;

    //                string strAir = string.Empty;
    //                if (lblstart.Text.Length == 4)
    //                {
    //                    strAir = lblstart.Text;
    //                }
    //                else
    //                {
    //                    strAir = lblstart.Text.Split(',')[0].Trim();
    //                }

    //                DataTable dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + lblstart.Text + "'");

    //                if (dtcity.Rows.Count > 0)
    //                {
    //                    ddlStartCitySplit.Enabled = false;
    //                    ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

    //                }
    //                else
    //                {
    //                    ddlStartCitySplit.Enabled = true;
    //                    ddlStartCitySplit.Text = string.Empty;
    //                }


    //                // ddlStartCitySplit.Enabled = false;
    //                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
    //                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
    //                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
    //                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
    //                tbldata.Visible = true;

    //                if (Convert.ToDateTime(txtSplitAvaFrom.Text).Date < System.DateTime.Now.Date)
    //                {
    //                    if (lblEarliestTime.Text != string.Empty)
    //                        txtSplitAvaFrom.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MM-dd-yyyy");
    //                }
    //                btnSave.Visible = true;

    //                divstartcounty.Attributes.Add("style", "display:none");
    //                divstartsplitLeft.Attributes.Add("style", "display:''");
    //                //trLeg1.Attributes.Add("style", "display:''");
    //                trLeg2.Attributes.Add("style", "display:''");

    //                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
    //                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

    //                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
    //                saveCategoryParam[0].Value = 1;//MANAGETYPE
    //                saveCategoryParam[1].Value = 25;//Rowid
    //                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
    //                saveCategoryParam[3].Value = strEndDate;//Rowid
    //                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];





    //                DataView dv = dtOneWayTrip.DefaultView;

    //                DataTable sortedDT = dv.ToTable();
    //                sortedDT.Columns.Add("leg");
    //                for (int i = 0; i < sortedDT.Rows.Count; i++)
    //                {
    //                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
    //                    String[] strlist = routes.Split(new[] { ',' });
    //                    string startD = strlist[0];
    //                    string EndD = strlist[1];
    //                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
    //                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
    //                and EndAirport='" + EndD + @"'").Tables[0];
    //                    if (dtleg.Rows.Count > 0)
    //                    {
    //                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
    //                    }
    //                }

    //                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
    //                DataTable dt = new DataTable();
    //                if (rowsFilteredConditionAND.Length > 0)
    //                {
    //                    dt = rowsFilteredConditionAND.CopyToDataTable();
    //                }

    //                if (dt.Rows.Count > 0)
    //                {
    //                    strAir = string.Empty;

    //                    if (dt.Rows[0]["start"].ToString().Length == 4)
    //                    {
    //                        strAir = dt.Rows[0]["start"].ToString();
    //                    }
    //                    else
    //                    {
    //                        strAir = dt.Rows[0]["start"].ToString().Split(',')[0].Trim();
    //                    }
    //                    dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + strAir + "'");

    //                    if (dtcity.Rows.Count > 0)
    //                    {
    //                        ddlStartCitySplit.Enabled = false;
    //                        ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

    //                    }
    //                    else
    //                    {
    //                        ddlStartCitySplit.Enabled = true;
    //                        ddlStartCitySplit.Text = string.Empty;
    //                    }

    //                    // ddlStartCitySplit.Text = dt.Rows[0]["StartCity"].ToString() + " - " + dt.Rows[0]["start"].ToString();

    //                    ViewState["tripBase"] = dt;

    //                    lblbaseflag.Text = "B1";

    //                    lblgroup.Text = "Group";
    //                }

    //                if (lblEModFlag.Text == "ET")
    //                {
    //                    lblscitydata.Visible = false;
    //                    lblEcityData.Visible = false;
    //                }
    //                else
    //                {
    //                    lblscitydata.Visible = true;
    //                    lblEcityData.Visible = true;
    //                }

    //                tblForm.Visible = true;
    //                tblGrid.Visible = false;
    //                ddlLeg1EndCity.Text = string.Empty;
    //                txtSpliFlyHour.Value = string.Empty;
    //                lblgroup.Text = "Group";
    //            }

    //            #endregion

    //            #region Modified Split

    //            else if (lblType.Text == "MSplit" && lblGroupId.Text != "0")
    //            {
    //                trSplit.Visible = true;
    //                divPriceModRight.Visible = false;
    //                divPricModLeft.Visible = false;
    //                lblSubScren.Text = "Add a Manual Leg";
    //                trManualandModiy.Visible = false;
    //                btnSave.Text = "Save";
    //                lblModGroupID.Text = lblGroupId.Text;
    //                txtcountry.Text = string.Empty;
    //                rblBreak.SelectedValue = "CT";
    //                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
    //                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
    //                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
    //                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
    //                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
    //                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
    //                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
    //                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("Label1");
    //                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
    //                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
    //                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
    //                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
    //                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label2");
    //                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label1");



    //                lblTailData.Text = lblTailNo.Text;
    //                lblTripData.Text = lbltripnoG.Text;
    //                lblStartData.Text = lblstart.Text;
    //                lblendData.Text = lblend.Text;
    //                lblscitydata.Text = lblstartcity.Text;
    //                lblEcityData.Text = lblEndcity.Text;
    //                lblStimedata.Text = lblStarttime.Text;
    //                lblEtimeData.Text = lblEndTime.Text;
    //                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();

    //                lblStimedata1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStimedata.Text;
    //                lblEtimeData1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEtimeData.Text;



    //                if (lblleg.Text == "" || lblleg.Text == "0")
    //                {
    //                    lbllegdisplay.Text = "1";
    //                }
    //                else
    //                {
    //                    lbllegdisplay.Text = lblleg.Text;
    //                }


    //                txtTailNo.Value = lblTailNo.Text;
    //                txttrip.Value = lbltripnoG.Text;
    //                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
    //                LBLTRIPNO.Text = txttrip.Value.Trim();


    //                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
    //                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
    //                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
    //                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
    //                tbldata.Visible = true;

    //                lblscitydata.Visible = false;
    //                lblEcityData.Visible = false;
    //                btnSave.Visible = true;

    //                divstartcounty.Attributes.Add("style", "display:none");
    //                divstartsplitLeft.Attributes.Add("style", "display:''");
    //                //trLeg1.Attributes.Add("style", "display:''");
    //                trLeg2.Attributes.Add("style", "display:''");

    //                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
    //                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

    //                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
    //                saveCategoryParam[0].Value = 1;//MANAGETYPE
    //                saveCategoryParam[1].Value = 25;//Rowid
    //                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
    //                saveCategoryParam[3].Value = strEndDate;//Rowid
    //                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];
    //                DataView dv = dtOneWayTrip.DefaultView;

    //                DataTable sortedDT = dv.ToTable();
    //                sortedDT.Columns.Add("leg");
    //                for (int i = 0; i < sortedDT.Rows.Count; i++)
    //                {
    //                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
    //                    String[] strlist = routes.Split(new[] { ',' });
    //                    string startD = strlist[0];
    //                    string EndD = strlist[1];
    //                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
    //                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
    //                and EndAirport='" + EndD + @"'").Tables[0];
    //                    if (dtleg.Rows.Count > 0)
    //                    {
    //                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
    //                    }
    //                }

    //                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
    //                DataTable dt = new DataTable();
    //                if (rowsFilteredConditionAND.Length > 0)
    //                {
    //                    dt = rowsFilteredConditionAND.CopyToDataTable();
    //                }
    //                string strAirport = string.Empty;

    //                if (dt.Rows.Count > 0)
    //                {
    //                    ddlStartCitySplit.Text = dt.Rows[0]["StartCity"].ToString() + " - " + dt.Rows[0]["start"].ToString();
    //                    ViewState["tripBase"] = dt;
    //                    strAirport = dt.Rows[0]["start"].ToString();
    //                }
    //                else
    //                {
    //                    ddlStartCitySplit.Text = lblStartData.Text.Trim();
    //                    strAirport = lblStartData.Text;
    //                }

    //                DataTable dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + strAirport + "'");

    //                if (dtcity.Rows.Count > 0)
    //                {
    //                    ddlStartCitySplit.Enabled = false;
    //                    ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

    //                }
    //                else
    //                {
    //                    ddlStartCitySplit.Enabled = true;
    //                    ddlStartCitySplit.Text = string.Empty;
    //                }
    //                lblbaseflag.Text = "B1";

    //                lblgroup.Text = "Group";
    //                tblForm.Visible = true;
    //                tblGrid.Visible = false;
    //            }

    //            #endregion

    //            #region Modiied FOS
    //            else if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text != "0")
    //            {
    //                lblgroup.Text = "Modify";
    //                lblSubScren.Text = "Modify a FOS Trip";
    //                trManualandModiy.Visible = true;
    //                trSplit.Visible = false;
    //                tbldata.Visible = true;
    //                lblModGroupID.Text = lblGroupId.Text;
    //                divManualAirport.Visible = false;
    //                divModify.Visible = true;
    //                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  TailNO,TripNo,NoOfseats, Mod_AvilableFrom, Mod_AvailableTo, Mod_StartAirport, Mod_Startcity,Mod_EndAirport,Mod_Endcity,Mod_Starttime,Mod_Endtime,Mod_FlyingHours,Mod_Price,CreatedOn,Active,New_StartAirport,New_EndAirport  FROM  Tailsetup_tripDetails  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and GroupId='" + lblGroupId.Text.Trim() + "'").Tables[0];
    //                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
    //                {
    //                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
    //                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
    //                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
    //                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
    //                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
    //                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
    //                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
    //                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
    //                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
    //                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
    //                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
    //                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
    //                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
    //                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
    //                    lbllegdisplay.Text = lblleg.Text;
    //                    string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
    //                    string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
    //                    DataTable dtGetData = GET_LIVEDATA("", strStartDate, strEndDate, lblTailNo.Text, lbltrip.Text);


    //                    if (dtGetData.Rows.Count > 0)
    //                    {
    //                        lblTailData.Text = dtGetData.Rows[0]["TailNo"].ToString();
    //                        lblTripData.Text = dtGetData.Rows[0]["TripNo"].ToString();
    //                        lblStartData.Text = dtGetData.Rows[0]["StartAirport"].ToString();
    //                        lblendData.Text = dtGetData.Rows[0]["EndAirport"].ToString();

    //                        lblscitydata.Text = dtGetData.Rows[0]["StartCity"].ToString();
    //                        lblEcityData.Text = dtGetData.Rows[0]["EndCity"].ToString();
    //                        lblStimedata.Text = dtGetData.Rows[0]["StartTime"].ToString();
    //                        lblEtimeData.Text = dtGetData.Rows[0]["Endtime"].ToString();
    //                        lblDateData.Text = Convert.ToDateTime(dtGetData.Rows[0]["AvailbleFrom"].ToString()).ToString("MMM dd, yyyy").ToUpper();

    //                        lblStimedata1.Text = Convert.ToDateTime(dtGetData.Rows[0]["LegDate"].ToString()).ToString("MMM dd, yyyy").ToUpper() + " " + lblStimedata.Text;
    //                        lblEtimeData1.Text = Convert.ToDateTime(dtGetData.Rows[0]["LegDate"].ToString()).ToString("MMM dd, yyyy").ToUpper() + " " + lblEtimeData.Text;

    //                        lblscitydata.Visible = false;
    //                        lblEcityData.Visible = false;

    //                    }

    //                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Dep'").Tables[0];

    //                    if (dt.Rows.Count > 0)
    //                    {
    //                        gvDepAirport.DataSource = dt;
    //                        gvDepAirport.DataBind();
    //                        ViewState["dtDepAirport"] = dt;
    //                        divDepLan.Visible = true;
    //                        txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
    //                    }
    //                    else
    //                    {
    //                        divDepLan.Visible = false;
    //                    }

    //                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Arr'").Tables[0];

    //                    if (dtArr.Rows.Count > 0)
    //                    {
    //                        gvArrivalAirport.DataSource = dtArr;
    //                        gvArrivalAirport.DataBind();
    //                        ViewState["dtArrAirport"] = dtArr;
    //                        txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
    //                        divArrLan.Visible = true;
    //                    }
    //                    else
    //                    {
    //                        divArrLan.Visible = false;
    //                    }

    //                    txtTailNo.Value = lblTailNo.Text;
    //                    txttrip.Value = lbltripnoG.Text;
    //                    txtFlyHours1.Value = lblFlyingHours.Text;
    //                    txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "").Replace(".00", "");
    //                    txtFlyHours1.Value = lblFlyingHours.Text;

    //                    txtStartTime.Value = lblStarttime.Text;
    //                    txtEndTime.Value = lblEndTime.Text;

    //                    hdfSCID.Value = "1";
    //                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["TripNo"].ToString();
    //                    lbloldtail.Text = dtOneWayTSel.Rows[0]["TailNo"].ToString();
    //                    txttrip.Value = dtOneWayTSel.Rows[0]["TripNo"].ToString();
    //                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();

    //                    if (dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() != "")
    //                    {
    //                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
    //                    }
    //                    else
    //                    {
    //                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
    //                    }
    //                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
    //                    if (dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() != "")
    //                    {
    //                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
    //                    }
    //                    else
    //                    {
    //                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
    //                    }

    //                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
    //                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Mod_FlyingHours"].ToString().Replace(":", ".");
    //                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
    //                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");

    //                    //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
    //                    //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

    //                    //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
    //                    //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

    //                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOfseats"].ToString();

    //                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
    //                    hdnDDate.Value = dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString();
    //                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
    //                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
    //                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString();
    //                    hdnendcity.Value = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString();
    //                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
    //                    hdnendairport.Value = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
    //                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
    //                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();
    //                    txtMPrice.Value = dtOneWayTSel.Rows[0]["Mod_Price"].ToString();
    //                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
    //                    {
    //                        chkEmpty.Checked = true;
    //                    }
    //                    else
    //                    {
    //                        chkEmpty.Checked = false;
    //                    }
    //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
    //                }
    //                tbldata.Visible = true;
    //                tblForm.Visible = true;
    //                tblGrid.Visible = false;
    //            }

    //            #endregion

    //            #region Manual Edit

    //            else
    //            {
    //                trManualandModiy.Visible = true;
    //                trSplit.Visible = false;
    //                divManualAirport.Visible = false;
    //                divModify.Visible = true;
    //                lblSubScren.Text = "Add a Manual Trip";
    //                lblgroup.Text = string.Empty;
    //                lblModGroupID.Text = "0";
    //                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours,  AvailbleFrom,AvilbleTo,NoOFSeats,DecrementType,DecrementAmount,EndAirport, EndCity,Active,TRIPNO,createdby,createdon,MPrice,New_StartAirport,New_EndAirport  FROM  OneWayTail_Setup  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "'").Tables[0];
    //                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
    //                {
    //                    hdfSCID.Value = "1";
    //                    lblManEditGroupId.Text = dtOneWayTSel.Rows[0]["rowid"].ToString();
    //                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["tripno"].ToString();
    //                    lbloldtail.Text = dtOneWayTSel.Rows[0]["tailno"].ToString();
    //                    txttrip.Value = dtOneWayTSel.Rows[0]["tripno"].ToString();
    //                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
    //                    txtStartCity.Text = dtOneWayTSel.Rows[0]["StartCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["StartAirport"].ToString();
    //                    txtStartTime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();

    //                    txtEndCity.Text = dtOneWayTSel.Rows[0]["EndCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["EndAirport"].ToString();
    //                    txtEndTime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
    //                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Flyinghours"].ToString().Replace(":", ".");
    //                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString()).ToString("MM-dd-yyyy");
    //                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvilbleTo"].ToString()).ToString("MM-dd-yyyy");
    //                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOFSeats"].ToString();
    //                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
    //                    hdnDDate.Value = dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString();
    //                    hdnstarttime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();
    //                    hdnendtime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
    //                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["StartCity"].ToString();
    //                    hdnendcity.Value = dtOneWayTSel.Rows[0]["EndCity"].ToString();
    //                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["StartAirport"].ToString();
    //                    hdnendairport.Value = dtOneWayTSel.Rows[0]["EndAirport"].ToString();
    //                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
    //                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();
    //                    txtMPrice.Value = dtOneWayTSel.Rows[0]["MPrice"].ToString().Replace(".00", "");
    //                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
    //                    {
    //                        chkEmpty.Checked = true;
    //                    }
    //                    else
    //                    {
    //                        chkEmpty.Checked = false;
    //                    }


    //                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Dep'").Tables[0];

    //                    if (dt.Rows.Count > 0)
    //                    {
    //                        gvDepAirport.DataSource = dt;
    //                        gvDepAirport.DataBind();
    //                        ViewState["dtDepAirport"] = dt;
    //                        divDepLan.Visible = true;
    //                        txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
    //                    }
    //                    else
    //                    {
    //                        divDepLan.Visible = false;
    //                    }

    //                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Arr'").Tables[0];

    //                    if (dtArr.Rows.Count > 0)
    //                    {
    //                        gvArrivalAirport.DataSource = dtArr;
    //                        gvArrivalAirport.DataBind();
    //                        ViewState["dtArrAirport"] = dtArr;
    //                        txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
    //                        divArrLan.Visible = true;
    //                    }
    //                    else
    //                    {
    //                        divArrLan.Visible = false;
    //                    }
    //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

    //                }

    //                tblForm.Visible = true;
    //                tblGrid.Visible = false;
    //            }
    //            #endregion

    //            if (gvArrivalAirport.Rows.Count > 1)
    //            {
    //                btnArrGroup.Visible = true;
    //            }
    //            else
    //            {
    //                btnArrGroup.Visible = false;
    //            }

    //            if (gvDepAirport.Rows.Count > 1)
    //            {
    //                btnCreateGroup.Visible = true;
    //            }
    //            else
    //            {
    //                btnCreateGroup.Visible = false;
    //            }




    //        }
    //        catch (Exception ex)
    //        {
    //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
    //        }
    //    }

    protected void gvTailDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblETSU.Text = string.Empty;
            ViewState["dtDepAirport"] = null;
            ViewState["dtArrAirport"] = null;
            ViewState["dtLegDetails"] = null;

            txtDepDisLandPage.Text = string.Empty;
            divDepLan.Visible = false;
            ddlDepSetupBy.ClearSelection();

            gvLegDetails.DataSource = null;
            gvLegDetails.DataBind();

            gvArrivalAirport.DataSource = null;
            gvArrivalAirport.DataBind();

            gvDepAirport.DataSource = null;
            gvDepAirport.DataBind();

            txtArrDisLandPage.Text = string.Empty;
            divArrLan.Visible = false;
            ddlArrSetupBy.ClearSelection();
            rblLanding.SelectedIndex = 0;
            lblModGroupID.Text = "0";
            trSplit.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            btnAdd.Visible = false;
            rblStatus.Visible = false;
            trManualandModiy.Visible = false;
            divModify.Visible = true;
            divManualAirport.Visible = true;
            lblEarliestTime.Text = string.Empty;
            lblMaxDepartTime.Text = string.Empty;
            lblMinDepartTime.Text = string.Empty;
            dvEarliestDepart.Visible = false;
            divPriceModLabel.Visible = false;
            divPGLMBy.Visible = false;

            divPGLMBy1.Visible = false;


            txtMod_DepCity.Visible = true;
            txtModDepState.Visible = false;
            txtModDepCountry.Visible = false;
            txtMod_DepCity.Text = string.Empty;
            txtModDepState.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;

            txtArrAirport.Visible = true;
            txtArrState.Visible = false;
            txtArrCountry.Visible = false;

            txtArrAirport.Text = string.Empty;
            txtArrState.Text = string.Empty;
            txtArrCountry.Text = string.Empty;


            if (((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text != string.Empty)
            {
                divPGLMBy.Visible = true;
                lblPGLMby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text + " ( on ) " + genclass.Long_Date_Time(Convert.ToDateTime(((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text));
                divPGLMBy1.Visible = true;
                lblPGLMby1.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text + " ( on ) " + genclass.Long_Date_Time(Convert.ToDateTime(((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text));

            }

            Label TailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
            Label lbltrip = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
            Label lblType = (Label)gvTailDetails.SelectedRow.FindControl("lblType");
            Label lblGroupId = (Label)gvTailDetails.SelectedRow.FindControl("lblGroupId");
            Label lblMFlag = (Label)gvTailDetails.SelectedRow.FindControl("lblModFlag");
            Label lblLandingFlag = (Label)gvTailDetails.SelectedRow.FindControl("lblLandingFlag");
            lblTripData.Text = lbltrip.Text;
            lblTailData.Text = TailNo.Text;
            Label lblFromDateFormat = (Label)gvTailDetails.SelectedRow.FindControl("lblFromDateFormat");
            Label lblToDateFormat = (Label)gvTailDetails.SelectedRow.FindControl("lblToDateFormat");


            rblLanding.SelectedIndex = rblLanding.Items.IndexOf(rblLanding.Items.FindByValue(lblLandingFlag.Text.Trim()));
            lblEModFlag.Text = lblMFlag.Text;

            lblEarliestTime.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblEarliestDepartTime")).Text;
            lblEarliestTimeLabel.Text = string.Empty;
            lblEarliestTime1.Text = string.Empty;
            if (lblEarliestTime.Text != string.Empty)
            {
                lblEarliestTimeLabel.Text = "Earliest Departure Time (in FOS)";
                lblEarliestTimeLabel1.Text = "Earliest Departure Time (in FOS)";
                lblEarliestTime1.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MMM dd yyyy HH:mm");
                lblEarliestTime.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MMM dd yyyy HH:mm");
            }

            string MINDepartTime = string.Empty;
            string MAXDepartTime = string.Empty;

            Label lblTailNo1 = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
            Label lblleg1 = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
            Label lblFlyingHours1 = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");

            if (lblleg1.Text != string.Empty)
            {
                objEarliestTimeBLL.getTripsLeg(lbltrip.Text, lblleg1.Text, lblTailNo1.Text, lblFlyingHours1.Text,
                    ref   MINDepartTime, ref   MAXDepartTime);

                lblMinDepartTime.Text = MINDepartTime;
                lblMaxDepartTime.Text = MAXDepartTime;
                if (lblMinDepartTime.Text != string.Empty)
                {
                    lblEarliestTimeLabel.Text = "Earliest Departure Time  ";
                    lblEarliestTimeLabel1.Text = "Earliest Departure Time ";
                    lblEarliestTime1.Text = Convert.ToDateTime(lblMinDepartTime.Text).ToString("MMM dd yyyy HH:mm");
                    lblEarliestTime.Text = Convert.ToDateTime(lblMinDepartTime.Text).ToString("MMM dd yyyy HH:mm");

                    lblMinDepartTime.Text = Convert.ToDateTime(MINDepartTime).ToString("MMM dd yyyy HH:mm");
                    lblMinDepartTime.Text = Convert.ToDateTime(MINDepartTime).ToString("MMM dd yyyy HH:mm");
                    lblMaxDepartTime.Text = Convert.ToDateTime(MAXDepartTime).ToString("MMM dd yyyy HH:mm");
                    lblMaxDepartTime1.Text = Convert.ToDateTime(MAXDepartTime).ToString("MMM dd yyyy HH:mm");

                }
                dvEarliestDepart.Visible = true;
            }

            Loadcountry();

            #region

            // if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text == "0")
            if (lblType.Text.Trim() == "FOS DH" && lblEModFlag.Text == "NO")
            {
                tbldata.Visible = true;
                divPriceModRight.Visible = true;
                lblmodLeft.Visible = true;
                divPricModLeft.Visible = true;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                lblSubScren.Text = "Modify a FOS Trip";
                lblModGroupID.Text = "0";
                trManualandModiy.Visible = true;
                tdPriceleft.Visible = true;
                tdPriceRight.Visible = true;
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");


                //Earlist DepartTime





                hdfSCID.Value = "0";
                LBLTRIPNO.Text = lbltripnoG.Text.Trim();

                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                lbllegdisplay.Text = lblleg.Text;


                lblStimedata1.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStarttime.Text;
                lblEtimeData1.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEndTime.Text;



                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lblTripData.Text;
                txtFlyHours1.Value = lblFlyingHours.Text;

                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "").Replace(".00", "");
                txtMPrice.Attributes.Add("disabled", "disabled");
                txtMPrice.Attributes.Add("style", "cursor: no-drop");

                lblUPDPrice.Text = String.Format("{0:C0}", Convert.ToDecimal(txtMPrice.Value));
                lblUPDPriceET.Text = String.Format("{0:C0}", Convert.ToDecimal(txtMPrice.Value));
                divPricModLeft.Visible = false;
                divPriceModRight.Visible = false;
                lblmodLeft.Visible = false;


                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                //                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:GetDepAirport();", true);
                txtMod_DepCity.Visible = true;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = false;
                txtstartsplit.Value = lblstartcity.Text + " - " + lblstart.Text;

                txtEndCity.Text = lblEndcity.Text + " - " + lblend.Text;

                txtStartCity.Text = lblstartcity.Text + " - " + lblstart.Text;


                lblstartdate.Text = Convert.ToDateTime(lblFromDateFormat.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblToDateFormat.Text).ToString("MM-dd-yyyy");

                if (lblMinDepartTime.Text != string.Empty && lblMaxDepartTime.Text != string.Empty)
                {
                    if (Convert.ToDateTime(lblMinDepartTime.Text) <= System.DateTime.Now)
                    {
                        txtAvaFrom.Text = System.DateTime.Now.AddHours(6).ToString("MMM d, yyyy");
                        txtStartTime.Value = System.DateTime.Now.AddHours(6).ToString("HH:mm");
                    }
                    else
                    {
                        txtAvaFrom.Text = Convert.ToDateTime(lblMinDepartTime.Text).ToString("MMM d, yyyy");
                        txtStartTime.Value = Convert.ToDateTime(lblMinDepartTime.Text).ToString("HH:mm");
                    }

                    txtAvaTo.Text = Convert.ToDateTime(lblMaxDepartTime.Text).ToString("MMM d, yyyy");
                    txtEndTime.Value = Convert.ToDateTime(lblMaxDepartTime.Text).ToString("HH:mm");
                }
                else
                {
                    txtStartTime.Value = lblStarttime.Text;
                    txtEndTime.Value = lblEndTime.Text;

                    txtAvaFrom.Text = Convert.ToDateTime(lblFromDateFormat.Text).ToString("MMM d, yyyy");
                    txtAvaTo.Text = Convert.ToDateTime(lblToDateFormat.Text).ToString("MMM d, yyyy");

                }

                //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
                //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

                //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
                //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

                if (lblMFlag.Text.Trim() == "ET" && lblGroupId.Text.Trim() == "0")
                {
                    DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid, New_StartAirport,New_EndAirport  FROM  OneWayTail_Setup  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "'").Tables[0];

                    if (dtOneWayTSel.Rows.Count > 0)
                    {
                        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and ModifiedFlag='ET' and AirportFor='Dep'").Tables[0];

                        if (dt.Rows.Count > 0)
                        {
                            gvDepAirport.DataSource = dt;
                            gvDepAirport.DataBind();
                            ViewState["dtDepAirport"] = dt;
                            divDepLan.Visible = true;
                            txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
                        }
                        else
                        {
                            divDepLan.Visible = false;
                        }

                        DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and ModifiedFlag='ET' and AirportFor='Arr'").Tables[0];

                        if (dtArr.Rows.Count > 0)
                        {
                            gvArrivalAirport.DataSource = dtArr;
                            gvArrivalAirport.DataBind();
                            ViewState["dtArrAirport"] = dtArr;
                            txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
                            divArrLan.Visible = true;
                        }
                        else
                        {
                            divArrLan.Visible = false;
                        }
                    }

                }


                lblgroup.Text = "Modify";
                txtTailNo.Disabled = true;
                txttrip.Disabled = true;
                tbldata.Visible = true;
                divstartcity.Visible = true;
                divstartcityLeft.Visible = true;
                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplit.Visible = false;
                divstartsplitLeft.Attributes.Add("style", "display:none");
                txtstartsplit.Disabled = true;
                divendcity.Visible = true;
                btnSave.Visible = true;
                tblForm.Visible = true;
                tblGrid.Visible = false;
                if (lblMFlag.Text == "ET")
                {
                    lblscitydata.Visible = false;
                    lblEcityData.Visible = false;


                }
                else
                {
                    lblscitydata.Visible = false;
                    lblEcityData.Visible = false;
                }
            }

            #endregion

            #region Edited Split

            else if (lblType.Text.Trim() == "Split" && lblGroupId.Text != "0")
            {

                ddlStartCitySplit.Enabled = false;
                lblSubScren.Text = "Add a Manual Leg";
                divPriceModRight.Visible = false;
                divPricModLeft.Visible = false;
                trManualandModiy.Visible = false;
                lblmodLeft.Visible = false;
                tdPriceleft.Visible = false;
                tdPriceRight.Visible = false;

                DataTable dtPS = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag<>'B0' Order by groupid").Tables[0];

                lblgroupiddisplay.Text = lblGroupId.Text.Trim();
                lblModGroupID.Text = lblGroupId.Text.Trim();
                trSplit.Visible = true;
                tbldata.Visible = true;
                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplitLeft.Attributes.Add("style", "display:''");
                //trLeg1.Attributes.Add("style", "display:''");
                trLeg2.Attributes.Add("style", "display:''");

                if (dtPS != null && dtPS.Rows.Count > 0)
                {
                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");





                    //  lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;

                    //lblupdatedon.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text;


                    txtSpliFlyHour.Value = lblFlyingHours.Text;
                    txtNoSeats.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                    txtSpliFlyHour.Value = lblFlyingHours.Text;

                    txtMPrice.Value = dtPS.Rows[0]["CurrentPrice"].ToString();
                    txtMPrice.Attributes.Add("disabled", "disabled");
                    txtMPrice.Attributes.Add("style", "cursor: no-drop");

                    lblTailData.Text = lblTailNo.Text;
                    lblTripData.Text = lbltripnoG.Text;
                    lblStartData.Text = lblstart.Text;
                    lblendData.Text = lblend.Text;
                    lblscitydata.Text = lblstartcity.Text;
                    lblEcityData.Text = lblEndcity.Text;
                    lblStimedata.Text = lblStarttime.Text;
                    lblEtimeData.Text = lblEndTime.Text;
                    lblDateData.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MMM dd, yyyy").ToUpper();
                    lblstartdate.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MM-dd-yyyy");
                    lblenddate.Text = Convert.ToDateTime(dtPS.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
                    lbllegdisplay.Text = lblleg.Text;


                    lblStimedata1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStarttime.Text;
                    lblEtimeData1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEndTime.Text;


                    txtTailNo.Value = lblTailNo.Text;
                    txttrip.Value = lbltripnoG.Text;
                    LBLTRIPNO.Text = txttrip.Value.Trim();

                    txtcountry.Text = string.Empty;
                    DataTable dtbase2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
                    if (dtbase2.Rows.Count > 0)
                    {
                        if (dtbase2.Rows[0]["tripflag"].ToString() == "B1")
                        {

                            DataTable dtPS2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag='B1' Order by groupid").Tables[0];
                            // txtStartCity.Text = dtPS2.Rows[0]["Endcity"].ToString() + " - " + dtPS2.Rows[0]["End"].ToString();
                            string strSpeed = string.Empty;
                            DataTable dtPS1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup Where TailNo='" + txtTailNo.Value + "'").Tables[0];
                            if (dtPS1.Rows.Count > 0)
                            {
                                strSpeed = dtPS1.Rows[0]["AvgSpeed"].ToString();
                            }
                            DataTable dtResul = new DataTable();
                            try
                            {

                                dtResul = AirRouteCalculation(dtPS2.Rows[0]["End"].ToString(), lblendData.Text, strSpeed);
                            }
                            catch (Exception ex)
                            {

                            }

                            if (dtResul.Rows.Count > 0)
                            {
                                txtSpliFlyHour.Value = (Math.Round(Convert.ToDecimal(dtResul.Rows[0]["flight_time_min"].ToString()) / 60, 2)).ToString();
                            }
                        }
                    }

                    DataTable dtpreference = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select TailNo as TailName,   TailNo, convert(varchar(12),DDate,109) DDate ,convert(varchar(12),ADate,109) ADate ,DDate [DateFormat],  CONVERT(VARCHAR(5), StartTime, 108) DTime,
		                                                CONVERT(VARCHAR(5), EndTime, 108)  ATime,Start+'-->'+[End] [routes], Start, StartCity, [End], EndCity, isnull(NoOFSeats,0) NoOFSeats,
		                                                FlyingHoursFOS, CalcualtedFlyingHrs FLTTIMEMINS,TripNo,
		                                                DaysToFlight,tripflag,Groupid,splittype from LivePricingCaculation where Groupid is not null and 
                                                        Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL'  Order by groupid").Tables[0];

                    if (dtpreference.Rows.Count > 0)
                    {
                        dtpreference.Columns.Add("leg");

                        for (int i = 0; i < dtpreference.Rows.Count; i++)
                        {
                            DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + dtpreference.Rows[i]["Tailno"].ToString() + @"' and tripno='" + dtpreference.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(dtpreference.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(dtpreference.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(dtpreference.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + lblstart.Text + @"'
                and EndAirport='" + lblend.Text + @"'").Tables[0];
                            if (dtleg.Rows.Count > 0)
                            {
                                dtpreference.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                            }
                        }

                        for (int i = 0; i < dtpreference.Rows.Count; i++)
                        {
                            if (i > 0)
                            {
                                dtpreference.Rows[i]["leg"] = dtpreference.Rows[0]["leg"].ToString();
                            }
                        }

                        ViewState["Tripgrid"] = dtpreference;

                        DataTable dt = new DataTable();
                        dtpreference.DefaultView.Sort = "Tripflag asc";
                        dt = dtpreference.DefaultView.ToTable();
                        rblBreak.SelectedValue = "CO";
                        DataRow[] result1 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag='B1' and splittype='CO'");

                        if (result1.Length > 0)
                        {
                            divstartcounty.Attributes.Add("style", "display:''");
                            divstartsplitLeft.Attributes.Add("style", "display:none");
                            //trLeg1.Attributes.Add("style", "display:none");
                            trLeg2.Attributes.Add("style", "display:none");

                            rblBreak.SelectedValue = "CO";
                            DataTable dttemp1 = result1.CopyToDataTable();
                            hdfSCID.Value = "0";
                            LBLTRIPNO.Text = dttemp1.Rows[0]["tripno"].ToString();
                            lbloldtail.Text = dttemp1.Rows[0]["tailno"].ToString();
                            txttrip.Value = dttemp1.Rows[0]["tripno"].ToString();
                            txtTailNo.Value = dttemp1.Rows[0]["TailNo"].ToString();
                            txtCouStartTime.Value = dttemp1.Rows[0]["DTime"].ToString();
                            txtSplitLeg1EndTime.Text = dttemp1.Rows[0]["ATime"].ToString();
                            txtSpliFlyHour.Value = dttemp1.Rows[0]["FlyinghoursFOS"].ToString().Replace(":", ".");

                            DataTable dtTake = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select Mod_AvailableTo,Mod_AvilableFrom,Mod_EndAirport,Mod_Endcity,Mod_StartAirport,Mod_Startcity from Tailsetup_tripDetails where Groupid='" + lblGroupId.Text.Trim() + "' and TripFlag='B1' and  ModifyFlag='SP'  and Mod_AvilableFrom!='1900-01-01 00:00:00.000'").Tables[0];

                            if (dtTake.Rows.Count > 0)
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");
                                ddlLeg1EndCity.Text = dtTake.Rows[0]["Mod_Endcity"].ToString() + " - " + dtTake.Rows[0]["Mod_EndAirport"].ToString();
                            }
                            else
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dttemp1.Rows[0]["DDate"].ToString()).ToString("MM-dd-yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dttemp1.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
                            }

                            if (Convert.ToDateTime(txtSplitAvaFrom.Text).Date < System.DateTime.Now.Date)
                            {
                                if (lblEarliestTime.Text != string.Empty)
                                    txtSplitAvaFrom.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MM-dd-yyyy");
                            }
                            txtNoSeats.Value = dttemp1.Rows[0]["NoOFSeats"].ToString();
                            txtcountry.Text = dttemp1.Rows[0]["Start"].ToString();
                            divstartcounty.Attributes.Add("style", "display:''");

                        }
                        DataRow[] result2 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag in ('B1','B2','B3') and splittype='CT' ");

                        if (result2.Length > 0)
                        {
                            divstartcounty.Attributes.Add("style", "display:none");
                            divstartsplitLeft.Attributes.Add("style", "display:''");
                            //trLeg1.Attributes.Add("style", "display:''");
                            trLeg2.Attributes.Add("style", "display:''");
                            rblBreak.SelectedValue = "CT";


                            ddlStartCitySplit.Text = string.Empty;
                            ddlLeg1EndCity.Text = string.Empty;
                            txtSpliFlyHour.Value = string.Empty;
                            txtSplitLeg1StartTime.Text = string.Empty;
                            txtSplitLeg1EndTime.Text = string.Empty;

                            DataTable dttemp1 = result2.CopyToDataTable();
                            ViewState["dtLegDetails"] = dttemp1;
                            gvLegDetails.DataSource = dttemp1;
                            gvLegDetails.DataBind();
                            if (gvLegDetails.Rows.Count == 3)
                            {
                                btnAddLeg.Visible = false;
                                divSplitEnd.Visible = false;
                                divSplitFlyHour.Visible = false;
                                divstartsplitLeft.Attributes.Add("style", "display:none;");
                            }
                            else
                            {

                                Label lblendair = (Label)gvLegDetails.Rows[gvLegDetails.Rows.Count - 1].FindControl("lblendair");

                                // DataTable dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + lblendair.Text + "'");

                                DataTable dtcity = genclass.GetDataTable(" Select   UPPER(case when SI.StateCode='HI' then SI.City+', '+SI.StateCode+' ('+SI.IATA+'/'+SI.ICAO +')' " +
             " WHEN SI.Country='1' then SI.City+', '+SI.StateCode+' ('+ case when ISNULL(SI.IATA,'') <>'' then SI.IATA+'/' else ''end +SI.ICAO +')' " +
             " ELSE SI.City+', '+SIC.Country_Code+' ('+SI.ICAO +')' END)   ICAOCity FROM JETEDGE_FUEL..ST_AIRPORT  SI LEFT JOIN JETEDGE_FUEL..Country SIC on SI.Country=SIC.Row_ID  " +
             " where ICAO='" + lblendair.Text + "'");


                                if (dtcity.Rows.Count > 0)
                                {
                                    ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

                                }
                                else
                                {
                                    ddlStartCitySplit.Text = string.Empty;
                                }




                                btnAddLeg.Visible = true;
                                divSplitEnd.Visible = true;
                                divSplitFlyHour.Visible = true;
                                divstartsplitLeft.Attributes.Add("style", "display:'';");
                            }
                            DataTable dtTake = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select Mod_AvailableTo,Mod_AvilableFrom,Mod_EndAirport,Mod_Endcity,Mod_StartAirport,Mod_Startcity from Tailsetup_tripDetails where Groupid='" + lblGroupId.Text.Trim() + "' and TripFlag='B1' and  ModifyFlag='SP'  and Mod_AvilableFrom!='1900-01-01 00:00:00.000'").Tables[0];

                            if (dtTake.Rows.Count > 0)
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MMM d, yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MMM d, yyyy");
                                //  ddlLeg1EndCity.Text = dtTake.Rows[0]["Mod_Endcity"].ToString() + " - " + dtTake.Rows[0]["Mod_EndAirport"].ToString();
                            }
                            else
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dttemp1.Rows[0]["DDate"].ToString()).ToString("MMM d, yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dttemp1.Rows[0]["ADate"].ToString()).ToString("MMM d, yyyy");
                            }

                            if (Convert.ToDateTime(txtSplitAvaFrom.Text).Date < System.DateTime.Now.Date)
                            {
                                if (lblEarliestTime.Text != string.Empty)
                                    txtSplitAvaFrom.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MM-dd-yyyy");
                            }
                            hdfSCID.Value = "0";
                        }
                        else
                        {
                            ViewState["dtLegDetails"] = null;
                            gvLegDetails.DataSource = null;
                            gvLegDetails.DataBind();
                            divSplitEnd.Visible = true;
                            divSplitFlyHour.Visible = true;
                        }
                    }
                }
                DataTable dtbase = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
                if (dtbase.Rows.Count > 0)
                {
                    if (dtbase.Rows[0]["tripflag"].ToString() == "B0")
                    {
                        lblbaseflag.Text = "B1";
                    }
                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B1")
                    {
                        lblbaseflag.Text = "B2";
                    }
                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B2")
                    {
                        lblbaseflag.Text = "B3";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Split();", true);
                btnSave.Text = "Update";
                tblForm.Visible = true;
                tblGrid.Visible = false;
                lblscitydata.Visible = false;
                lblEcityData.Visible = false;
                lblgroup.Text = "Group";
            }

            #endregion

            #region New Split

            else if ((lblType.Text.Trim() == "Split" && lblGroupId.Text == "0"))
            {
                trSplit.Visible = true;
                divPriceModRight.Visible = false;
                lblmodLeft.Visible = false;
                divPricModLeft.Visible = false;
                tdPriceleft.Visible = false;
                tdPriceRight.Visible = false;
                lblSubScren.Text = "Add a Manual Leg";
                trManualandModiy.Visible = false;
                lblModGroupID.Text = "0";
                // txtcountry.Text = "United States";
                rblBreak.SelectedValue = "CT";
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");





                LBLTRIPNO.Text = lbltripnoG.Text.Trim();
                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                lbllegdisplay.Text = lblleg.Text;

                lblStimedata1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStimedata.Text;
                lblEtimeData1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEtimeData.Text;


                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lbltripnoG.Text;

                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                txtMPrice.Attributes.Add("disabled", "disabled");
                txtMPrice.Attributes.Add("style", "cursor: no-drop");

                hdnstarttime.Value = lblStarttime.Text;
                hdnendtime.Value = lblEndTime.Text;

                string strAir = string.Empty;
                if (lblstart.Text.Length == 4)
                {
                    strAir = lblstart.Text;
                }
                else
                {
                    strAir = lblstart.Text.Split(',')[0].Trim();
                }

                DataTable dtcity = genclass.GetDataTable(" Select   UPPER(case when SI.StateCode='HI' then SI.City+', '+SI.StateCode+' ('+SI.IATA+'/'+SI.ICAO +')' " +
                " WHEN SI.Country='1' then SI.City+', '+SI.StateCode+' ('+ case when ISNULL(SI.IATA,'') <>'' then SI.IATA+'/' else ''end +SI.ICAO +')' " +
                " ELSE SI.City+', '+SIC.Country_Code+' ('+SI.ICAO +')' END)   ICAOCity FROM JETEDGE_FUEL..ST_AIRPORT  SI LEFT JOIN JETEDGE_FUEL..Country SIC on SI.Country=SIC.Row_ID  " +
                " where ICAO='" + lblstart.Text + "'");

                if (dtcity.Rows.Count > 0)
                {
                    ddlStartCitySplit.Enabled = false;
                    ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

                }
                else
                {
                    ddlStartCitySplit.Enabled = true;
                    ddlStartCitySplit.Text = string.Empty;
                }


                // ddlStartCitySplit.Enabled = false;
                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MMM dd, yyyy");
                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MMM dd, yyyy");
                tbldata.Visible = true;

                if (Convert.ToDateTime(txtSplitAvaFrom.Text).Date < System.DateTime.Now.Date)
                {
                    if (lblEarliestTime.Text != string.Empty)
                        txtSplitAvaFrom.Text = Convert.ToDateTime(lblEarliestTime.Text).ToString("MMM dd, yyyy");
                }
                btnSave.Visible = true;

                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplitLeft.Attributes.Add("style", "display:''");
                //trLeg1.Attributes.Add("style", "display:''");
                trLeg2.Attributes.Add("style", "display:''");

                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
                saveCategoryParam[0].Value = 1;//MANAGETYPE
                saveCategoryParam[1].Value = 25;//Rowid
                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
                saveCategoryParam[3].Value = strEndDate;//Rowid
                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];





                DataView dv = dtOneWayTrip.DefaultView;

                DataTable sortedDT = dv.ToTable();
                sortedDT.Columns.Add("leg");
                for (int i = 0; i < sortedDT.Rows.Count; i++)
                {
                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
                    String[] strlist = routes.Split(new[] { ',' });
                    string startD = strlist[0];
                    string EndD = strlist[1];
                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
                and EndAirport='" + EndD + @"'").Tables[0];
                    if (dtleg.Rows.Count > 0)
                    {
                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                    }
                }

                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
                DataTable dt = new DataTable();
                if (rowsFilteredConditionAND.Length > 0)
                {
                    dt = rowsFilteredConditionAND.CopyToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    strAir = string.Empty;

                    if (dt.Rows[0]["start"].ToString().Length == 4)
                    {
                        strAir = dt.Rows[0]["start"].ToString();
                    }
                    else
                    {
                        strAir = dt.Rows[0]["start"].ToString().Split(',')[0].Trim();
                    }
                    dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + strAir + "'");

                    if (dtcity.Rows.Count > 0)
                    {
                        ddlStartCitySplit.Enabled = false;
                        ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

                    }
                    else
                    {
                        ddlStartCitySplit.Enabled = true;
                        ddlStartCitySplit.Text = string.Empty;
                    }

                    // ddlStartCitySplit.Text = dt.Rows[0]["StartCity"].ToString() + " - " + dt.Rows[0]["start"].ToString();

                    ViewState["tripBase"] = dt;

                    lblbaseflag.Text = "B1";

                    lblgroup.Text = "Group";
                }

                if (lblEModFlag.Text == "ET")
                {
                    lblscitydata.Visible = false;
                    lblEcityData.Visible = false;
                }
                else
                {
                    lblscitydata.Visible = false;
                    lblEcityData.Visible = false;
                }

                tblForm.Visible = true;
                tblGrid.Visible = false;
                ddlLeg1EndCity.Text = string.Empty;
                txtSpliFlyHour.Value = string.Empty;
                lblgroup.Text = "Group";
            }

            #endregion

            #region Modified Split

            else if (lblType.Text == "MSplit" && lblGroupId.Text != "0")
            {
                tdPriceleft.Visible = false;
                tdPriceRight.Visible = false;
                trSplit.Visible = true;
                divPriceModRight.Visible = false;
                lblmodLeft.Visible = false;
                divPricModLeft.Visible = false;
                lblSubScren.Text = "Add a Manual Leg";
                trManualandModiy.Visible = false;
                btnSave.Text = "Save";
                lblModGroupID.Text = lblGroupId.Text;
                txtcountry.Text = string.Empty;
                rblBreak.SelectedValue = "CT";
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label2");
                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label1");



                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                lblStimedata1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblStimedata.Text;
                lblEtimeData1.Text = Convert.ToDateTime(lblDateData.Text).ToString("MMM dd, yyyy").ToUpper() + " " + lblEtimeData.Text;

                if (lblleg.Text == "" || lblleg.Text == "0")
                {
                    lbllegdisplay.Text = "1";
                }
                else
                {
                    lbllegdisplay.Text = lblleg.Text;
                }


                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lbltripnoG.Text;

                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                txtMPrice.Attributes.Add("disabled", "disabled");
                txtMPrice.Attributes.Add("style", "cursor: no-drop");

                LBLTRIPNO.Text = txttrip.Value.Trim();


                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                tbldata.Visible = true;

                lblscitydata.Visible = false;
                lblEcityData.Visible = false;
                btnSave.Visible = true;

                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplitLeft.Attributes.Add("style", "display:''");
                //trLeg1.Attributes.Add("style", "display:''");
                trLeg2.Attributes.Add("style", "display:''");

                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
                saveCategoryParam[0].Value = 1;//MANAGETYPE
                saveCategoryParam[1].Value = 25;//Rowid
                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
                saveCategoryParam[3].Value = strEndDate;//Rowid
                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];
                DataView dv = dtOneWayTrip.DefaultView;

                DataTable sortedDT = dv.ToTable();
                sortedDT.Columns.Add("leg");
                for (int i = 0; i < sortedDT.Rows.Count; i++)
                {
                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
                    String[] strlist = routes.Split(new[] { ',' });
                    string startD = strlist[0];
                    string EndD = strlist[1];
                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
                and EndAirport='" + EndD + @"'").Tables[0];
                    if (dtleg.Rows.Count > 0)
                    {
                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                    }
                }

                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
                DataTable dt = new DataTable();
                if (rowsFilteredConditionAND.Length > 0)
                {
                    dt = rowsFilteredConditionAND.CopyToDataTable();
                }
                string strAirport = string.Empty;

                if (dt.Rows.Count > 0)
                {
                    ddlStartCitySplit.Text = dt.Rows[0]["StartCity"].ToString() + " - " + dt.Rows[0]["start"].ToString();
                    ViewState["tripBase"] = dt;
                    strAirport = dt.Rows[0]["start"].ToString();
                }
                else
                {
                    ddlStartCitySplit.Text = lblStartData.Text.Trim();
                    strAirport = lblStartData.Text;
                }

                DataTable dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + strAirport + "'");

                if (dtcity.Rows.Count > 0)
                {
                    ddlStartCitySplit.Enabled = false;
                    ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

                }
                else
                {
                    ddlStartCitySplit.Enabled = true;
                    ddlStartCitySplit.Text = string.Empty;
                }
                lblbaseflag.Text = "B1";

                lblgroup.Text = "Group";
                tblForm.Visible = true;
                tblGrid.Visible = false;
            }

            #endregion

            #region Modiied FOS
            // else if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text != "0")
            else if (lblType.Text.Trim() == "FOS DH" && lblEModFlag.Text != "NO")
            {
                tdPriceleft.Visible = true;
                tdPriceRight.Visible = true;
                lblgroup.Text = "Modify";
                lblSubScren.Text = "Modify a FOS Trip";
                trManualandModiy.Visible = true;
                trSplit.Visible = false;
                tbldata.Visible = true;
                lblModGroupID.Text = lblGroupId.Text;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  TailNO,TripNo,NoOfseats, Mod_AvilableFrom, Mod_AvailableTo, Mod_StartAirport, Mod_Startcity,Mod_EndAirport,Mod_Endcity,Mod_Starttime,Mod_Endtime,Mod_FlyingHours,Mod_Price,CreatedOn,Active,New_StartAirport,New_EndAirport  FROM  Tailsetup_tripDetails  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and GroupId='" + lblGroupId.Text.Trim() + "'").Tables[0];
                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
                {
                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                    lbllegdisplay.Text = lblleg.Text;
                    string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                    string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
                    DataTable dtGetData = GET_LIVEDATA("", strStartDate, strEndDate, lblTailNo.Text, lbltrip.Text);


                    if (dtGetData.Rows.Count > 0)
                    {
                        lblTailData.Text = dtGetData.Rows[0]["TailNo"].ToString();
                        lblTripData.Text = dtGetData.Rows[0]["TripNo"].ToString();
                        lblStartData.Text = dtGetData.Rows[0]["StartAirport"].ToString();
                        lblendData.Text = dtGetData.Rows[0]["EndAirport"].ToString();

                        lblscitydata.Text = dtGetData.Rows[0]["StartCity"].ToString();
                        lblEcityData.Text = dtGetData.Rows[0]["EndCity"].ToString();
                        lblStimedata.Text = dtGetData.Rows[0]["StartTime"].ToString();
                        lblEtimeData.Text = dtGetData.Rows[0]["Endtime"].ToString();
                        lblDateData.Text = Convert.ToDateTime(dtGetData.Rows[0]["AvailbleFrom"].ToString()).ToString("MMM dd, yyyy").ToUpper();

                        lblStimedata1.Text = Convert.ToDateTime(dtGetData.Rows[0]["LegDate"].ToString()).ToString("MMM dd, yyyy").ToUpper() + " " + dtGetData.Rows[0]["StartTime"].ToString();
                        lblEtimeData1.Text = Convert.ToDateTime(dtGetData.Rows[0]["LegDate"].ToString()).ToString("MMM dd, yyyy").ToUpper() + " " + dtGetData.Rows[0]["Endtime"].ToString();


                        lblscitydata.Visible = false;
                        lblEcityData.Visible = false;

                    }

                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Dep'").Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        gvDepAirport.DataSource = dt;
                        gvDepAirport.DataBind();
                        ViewState["dtDepAirport"] = dt;
                        divDepLan.Visible = true;
                        txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
                    }
                    else
                    {
                        divDepLan.Visible = false;
                    }

                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Arr'").Tables[0];

                    if (dtArr.Rows.Count > 0)
                    {
                        gvArrivalAirport.DataSource = dtArr;
                        gvArrivalAirport.DataBind();
                        ViewState["dtArrAirport"] = dtArr;
                        txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
                        divArrLan.Visible = true;
                    }
                    else
                    {
                        divArrLan.Visible = false;
                    }

                    txtTailNo.Value = lblTailNo.Text;
                    txttrip.Value = lbltripnoG.Text;
                    txtFlyHours1.Value = lblFlyingHours.Text;

                    txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "").Replace(".00", "");
                    txtMPrice.Attributes.Add("disabled", "disabled");
                    txtMPrice.Attributes.Add("style", "cursor: no-drop");


                    txtFlyHours1.Value = lblFlyingHours.Text;

                    txtStartTime.Value = lblStarttime.Text;
                    txtEndTime.Value = lblEndTime.Text;

                    hdfSCID.Value = "1";
                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["TripNo"].ToString();
                    lbloldtail.Text = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    txttrip.Value = dtOneWayTSel.Rows[0]["TripNo"].ToString();
                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();

                    if (dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() != "")
                    {
                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    }
                    else
                    {
                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    }
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
                    if (dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() != "")
                    {
                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    }
                    else
                    {
                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    }

                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Mod_FlyingHours"].ToString().Replace(":", ".");
                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MMM d, yyyy");
                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MMM d, yyyy");

                    //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
                    //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

                    //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
                    //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOfseats"].ToString();

                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    hdnDDate.Value = dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString();
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString();
                    hdnendcity.Value = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString();
                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    hdnendairport.Value = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();

                    // txtMPrice.Value = dtOneWayTSel.Rows[0]["Mod_Price"].ToString();
                    txtMPrice.Attributes.Add("disabled", "disabled");
                    txtMPrice.Attributes.Add("style", "cursor: no-drop");

                    lblUPDPrice.Text = String.Format("{0:C0}", Convert.ToDecimal(txtMPrice.Value));
                    lblUPDPriceET.Text = String.Format("{0:C0}", Convert.ToDecimal(txtMPrice.Value));
                    divPricModLeft.Visible = false;
                    divPriceModRight.Visible = false;
                    lblmodLeft.Visible = false;

                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
                    {
                        chkEmpty.Checked = true;
                    }
                    else
                    {
                        chkEmpty.Checked = false;
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                }
                tbldata.Visible = true;
                tblForm.Visible = true;
                tblGrid.Visible = false;





            }

            #endregion

            #region Manual Edit

            else
            {
                trManualandModiy.Visible = true;
                trSplit.Visible = false;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                divPriceModRight.Visible = true;
                lblmodLeft.Visible = true;
                divPricModLeft.Visible = true;
                lblSubScren.Text = "Add a Manual Trip";
                lblgroup.Text = string.Empty;
                lblModGroupID.Text = "0";
                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours,  AvailbleFrom,AvilbleTo,NoOFSeats,DecrementType,DecrementAmount,EndAirport, EndCity,Active,TRIPNO,createdby,createdon,MPrice,New_StartAirport,New_EndAirport  FROM  OneWayTail_Setup  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "'").Tables[0];
                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
                {
                    hdfSCID.Value = "1";
                    lblTripData.Text = dtOneWayTSel.Rows[0]["tripno"].ToString();
                    lbllegdisplay.Text = "1";
                    lblManEditGroupId.Text = dtOneWayTSel.Rows[0]["rowid"].ToString();
                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["tripno"].ToString();
                    lbloldtail.Text = dtOneWayTSel.Rows[0]["tailno"].ToString();
                    txttrip.Value = dtOneWayTSel.Rows[0]["tripno"].ToString();
                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    txtStartCity.Text = dtOneWayTSel.Rows[0]["StartCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["StartAirport"].ToString();
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();

                    txtEndCity.Text = dtOneWayTSel.Rows[0]["EndCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["EndAirport"].ToString();
                    txtEndTime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Flyinghours"].ToString().Replace(":", ".");
                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString()).ToString("MMM d, yyyy");
                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvilbleTo"].ToString()).ToString("MMM d, yyyy");
                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOFSeats"].ToString();
                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    hdnDDate.Value = dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString();
                    hdnstarttime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();
                    hdnendtime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["StartCity"].ToString();
                    hdnendcity.Value = dtOneWayTSel.Rows[0]["EndCity"].ToString();
                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["StartAirport"].ToString();
                    hdnendairport.Value = dtOneWayTSel.Rows[0]["EndAirport"].ToString();
                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();

                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

                    txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                    txtMPrice.Attributes.Add("disabled", "disabled");
                    txtMPrice.Attributes.Add("style", "cursor: no-drop");

                    divPricModLeft.Visible = true;
                    divPriceModRight.Visible = false;
                    lblmodLeft.Visible = false;
                    lblUPDPrice.Text = String.Format("{0:C0}", Convert.ToDecimal(txtMPrice.Value));
                    lblUPDPriceET.Text = String.Format("{0:C0}", Convert.ToDecimal(txtMPrice.Value));
                    divPriceModLabel.Visible = true;

                    lblETSU.Text = "U";

                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
                    {
                        chkEmpty.Checked = true;
                    }
                    else
                    {
                        chkEmpty.Checked = false;
                    }


                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Dep'").Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        gvDepAirport.DataSource = dt;
                        gvDepAirport.DataBind();
                        ViewState["dtDepAirport"] = dt;
                        divDepLan.Visible = true;
                        txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
                    }
                    else
                    {
                        divDepLan.Visible = false;
                    }

                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Arr'").Tables[0];

                    if (dtArr.Rows.Count > 0)
                    {
                        gvArrivalAirport.DataSource = dtArr;
                        gvArrivalAirport.DataBind();
                        ViewState["dtArrAirport"] = dtArr;
                        txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
                        divArrLan.Visible = true;
                    }
                    else
                    {
                        divArrLan.Visible = false;
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

                }

                tblForm.Visible = true;
                tblGrid.Visible = false;
            }
            #endregion

            DataTable dtLeg = objZoomCalculatinDAL.Get_EmptyTripSetupTrip(lblTripData.Text, lbllegdisplay.Text).Tables[1];
            lblActualStartICAO.Text = lblStartData.Text + " " + lblscitydata.Text;
            lblActualEndICAO.Text = lblendData.Text + " " + lblEcityData.Text;
            if (dtLeg.Rows.Count > 0)
            {
                lblStimedata1.Text = Convert.ToDateTime(dtLeg.Rows[0]["TDATE"].ToString()).ToString("MMM dd, yyyy").ToUpper() + " " + dtLeg.Rows[0]["StartTime"].ToString();
                lblEtimeData1.Text = Convert.ToDateTime(dtLeg.Rows[0]["TDATE"].ToString()).ToString("MMM dd, yyyy").ToUpper() + " " + dtLeg.Rows[0]["EndTime"].ToString();

                if (dtLeg.Rows[0]["Departure"].ToString() != string.Empty)
                    lblActualStartICAO.Text = dtLeg.Rows[0]["Departure"].ToString();
                if (dtLeg.Rows[0]["Arraival"].ToString() != string.Empty)
                    lblActualEndICAO.Text = dtLeg.Rows[0]["Arraival"].ToString();

            }

            if (gvArrivalAirport.Rows.Count > 1)
            {
                btnArrGroup.Visible = true;
            }
            else
            {
                btnArrGroup.Visible = false;
            }

            if (gvDepAirport.Rows.Count > 1)
            {
                btnCreateGroup.Visible = true;
            }
            else
            {
                btnCreateGroup.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public string saveCategory(string[] arrCategory)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_V1]", false);
        // DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  OneWayTail_Setup.rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours, EndAirport, EndCity  FROM  OneWayTail_Setup Inner join TailPricing_Configuration on TailPricing_Configuration.TailNo=OneWayTail_Setup.TailNo ").Tables[0];

        saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
        saveCategoryParam[1].Value = arrCategory[1];//Rowid
        saveCategoryParam[2].Value = arrCategory[2];//TailId
        saveCategoryParam[3].Value = arrCategory[3];//
        saveCategoryParam[4].Value = arrCategory[4];//
        saveCategoryParam[5].Value = arrCategory[5];//
        saveCategoryParam[6].Value = arrCategory[6];//
        saveCategoryParam[7].Value = arrCategory[7];//
        saveCategoryParam[8].Value = arrCategory[8];//
        saveCategoryParam[9].Value = arrCategory[9];//
        saveCategoryParam[10].Value = arrCategory[10];//
        saveCategoryParam[11].Value = arrCategory[11];//
        saveCategoryParam[12].Value = arrCategory[12];//
        saveCategoryParam[13].Value = arrCategory[13];//
        saveCategoryParam[14].Value = arrCategory[14];//
        // saveCategoryParam[15].Value = arrCategory[15];//
        // saveCategoryParam[16].Value = arrCategory[16];//
        saveCategoryParam[19].Value = arrCategory[19];//

        saveCategoryParam[20].Value = arrCategory[20];//
        saveCategoryParam[21].Value = arrCategory[21];//
        saveCategoryParam[22].Value = arrCategory[22];//
        saveCategoryParam[23].Value = arrCategory[23];//
        saveCategoryParam[24].Value = arrCategory[24];//
        saveCategoryParam[25].Value = arrCategory[25];//
        saveCategoryParam[26].Value = arrCategory[26];//
        saveCategoryParam[27].Value = arrCategory[27];//
        saveCategoryParam[28].Value = arrCategory[28];//
        saveCategoryParam[29].Value = arrCategory[29];//
        saveCategoryParam[30].Value = arrCategory[30];//
        saveCategoryParam[31].Value = arrCategory[31];//
        saveCategoryParam[32].Value = arrCategory[32];//
        saveCategoryParam[33].Value = arrCategory[33];//
        saveCategoryParam[34].Value = arrCategory[34];//
        saveCategoryParam[35].Value = arrCategory[35];//
        saveCategoryParam[36].Value = arrCategory[36];//
        saveCategoryParam[37].Value = Convert.ToDateTime(arrCategory[37]);//
        saveCategoryParam[38].Value = arrCategory[38];//
        saveCategoryParam[39].Value = arrCategory[39];//

        saveCategoryParam[41].Value = arrCategory[41];//
        saveCategoryParam[42].Value = arrCategory[42];//
        saveCategoryParam[43].Value = arrCategory[43];//
        saveCategoryParam[44].Value = arrCategory[44];//
        saveCategoryParam[46].Value = arrCategory[48];//
        saveCategoryParam[47].Value = arrCategory[49];//
        saveCategoryParam[48].Value = arrCategory[50];
        saveCategoryParam[49].Value = arrCategory[51];//


        saveCategoryParam[50].Value = arrCategory[52];//
        saveCategoryParam[51].Value = arrCategory[53];//



        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_V1]", saveCategoryParam).ToString();
        return obj.ToString();
    }
    public string saveCategoryGroup(string[] arrCategory)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_GROUP]", false);
        // DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  OneWayTail_Setup.rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours, EndAirport, EndCity  FROM  OneWayTail_Setup Inner join TailPricing_Configuration on TailPricing_Configuration.TailNo=OneWayTail_Setup.TailNo ").Tables[0];

        saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
        saveCategoryParam[1].Value = arrCategory[1];//Rowid
        saveCategoryParam[2].Value = arrCategory[2];//TailId
        saveCategoryParam[3].Value = arrCategory[3];//
        saveCategoryParam[4].Value = arrCategory[4];//
        saveCategoryParam[5].Value = arrCategory[5];//
        saveCategoryParam[6].Value = arrCategory[6];//
        saveCategoryParam[7].Value = arrCategory[7];//
        saveCategoryParam[8].Value = arrCategory[8];//
        saveCategoryParam[9].Value = arrCategory[9];//
        saveCategoryParam[10].Value = arrCategory[10];//
        saveCategoryParam[11].Value = arrCategory[11];//
        saveCategoryParam[12].Value = arrCategory[12];//
        saveCategoryParam[13].Value = arrCategory[13];//
        saveCategoryParam[14].Value = arrCategory[14];//
        // saveCategoryParam[15].Value = arrCategory[15];//
        // saveCategoryParam[16].Value = arrCategory[16];//
        saveCategoryParam[19].Value = arrCategory[19];//

        saveCategoryParam[20].Value = arrCategory[20];//
        saveCategoryParam[21].Value = arrCategory[21];//
        saveCategoryParam[22].Value = arrCategory[22];//
        saveCategoryParam[23].Value = arrCategory[23];//
        saveCategoryParam[24].Value = arrCategory[24];//
        saveCategoryParam[25].Value = arrCategory[25];//
        saveCategoryParam[26].Value = arrCategory[26];//
        saveCategoryParam[27].Value = arrCategory[27];//
        saveCategoryParam[28].Value = arrCategory[28];//
        saveCategoryParam[29].Value = arrCategory[29];//
        saveCategoryParam[30].Value = arrCategory[30];//
        saveCategoryParam[31].Value = arrCategory[31];//
        saveCategoryParam[32].Value = arrCategory[32];//
        saveCategoryParam[33].Value = arrCategory[33];//
        saveCategoryParam[34].Value = arrCategory[34];//
        saveCategoryParam[35].Value = arrCategory[35];//
        saveCategoryParam[36].Value = arrCategory[36];//
        saveCategoryParam[37].Value = Convert.ToDateTime(arrCategory[37]);//
        saveCategoryParam[38].Value = arrCategory[38];//
        saveCategoryParam[39].Value = arrCategory[39];//

        saveCategoryParam[41].Value = arrCategory[41];//
        saveCategoryParam[42].Value = arrCategory[42];//
        saveCategoryParam[43].Value = arrCategory[43];//
        saveCategoryParam[44].Value = arrCategory[44];//
        saveCategoryParam[45].Value = arrCategory[45];//
        saveCategoryParam[46].Value = arrCategory[46];//
        saveCategoryParam[47].Value = arrCategory[47];//
        saveCategoryParam[48].Value = arrCategory[48];//

        saveCategoryParam[49].Value = arrCategory[49];//
        saveCategoryParam[50].Value = arrCategory[50];//


        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_GROUP]", saveCategoryParam).ToString();
        return obj.ToString();
    }

    public string SaveDepartureGroup(string strManageType, string strRowId, string strGroupName, string strFlag, string strCreatedBy)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_INS]", false);
        saveCategoryParam[0].Value = strManageType;
        saveCategoryParam[1].Value = strRowId;
        saveCategoryParam[2].Value = strGroupName;
        saveCategoryParam[3].Value = strFlag;
        saveCategoryParam[4].Value = strCreatedBy;
        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_INS]", saveCategoryParam).ToString();
        return obj.ToString();
    }

    private void BindGroup()
    {
        DataTable dtAirport = BindGroupData();
        if (dtAirport.Rows.Count > 0)
        {
            gvGroup.DataSource = dtAirport;
            gvGroup.DataBind();

            for (int i = 0; i < gvGroup.Rows.Count; i++)
            {
                Label lblRowId = (Label)gvGroup.Rows[i].FindControl("lblAirportId");

                DataList dsScreen = (DataList)gvGroup.Rows[i].FindControl("rptAirports");

                DataTable dtChild = BindGroupChildData(lblRowId.Text);

                if (dtChild.Rows.Count > 0)
                {
                    dsScreen.DataSource = dtChild;
                    dsScreen.DataBind();
                }
                else
                {
                    dsScreen.DataSource = null;
                    dsScreen.DataBind();
                }

            }

            gvGroupArrival.DataSource = dtAirport;
            gvGroupArrival.DataBind();

            for (int i = 0; i < gvGroupArrival.Rows.Count; i++)
            {
                Label lblRowId = (Label)gvGroupArrival.Rows[i].FindControl("lblAirportId");

                DataList dsScreen = (DataList)gvGroupArrival.Rows[i].FindControl("rptAirports");

                DataTable dtChild = BindGroupChildData(lblRowId.Text);

                if (dtChild.Rows.Count > 0)
                {
                    dsScreen.DataSource = dtChild;
                    dsScreen.DataBind();
                }
                else
                {
                    dsScreen.DataSource = null;
                    dsScreen.DataBind();
                }

            }
        }
        else
        {
            gvGroup.DataSource = null;
            gvGroup.DataBind();

            gvGroupArrival.DataSource = null;
            gvGroupArrival.DataBind();
        }
    }

    protected void ddlstatus_onselectedChanged(object sender, EventArgs e)
    {
        BindEmptyLegs();
        gvTailDetails.PageIndex = 0;
        gvTailDetails.DataBind();
    }
    protected void btnconfirmdelsplitdelete_click(object sender, EventArgs e)
    {
        try
        {
            #region

            string strStartDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            string strEndDate = DateTime.Now.AddDays(7).ToString("MM/dd/yyyy HH:mm:ss");

            DataTable dtOneWayTrip = objZoomCalculatinDAL.Get_EmptyTripSetup(strStartDate, strEndDate);
            #endregion

            if (lblDeletModFlag.Text.Trim() == "ET" && lblgroupiddelete.Text.Trim() == "0")
            {
                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid, New_StartAirport,New_EndAirport  FROM  OneWayTail_Setup  where Tailno ='" + lblDeleteTailNo.Text + "' and tripno='" + lblDeleteTripNo.Text + "'").Tables[0];

                if (dtOneWayTSel.Rows.Count > 0)
                {
                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  EmptyLegAirport_Child  where GroupId = '" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and ModifiedFlag in ('MF','SP') ");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and tripflag<>'B0'");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Groupid=0,ModifyFlag='',Createdby='',CreatedOn=null,Splittype='',New_StartAirport='',New_EndAirport='' Where Groupid='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "'");
                }


            }
            else if (lblDeletModFlag.Text.Trim() == "MF" && lblgroupiddelete.Text.Trim() == "0")
            {

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  EmptyLegAirport_Child  where GroupId = '" + lblgroupiddelete.Text.Trim() + "' and ModifiedFlag in ('MF','SP') ");

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + lblgroupiddelete.Text + "' and TripNo='" + lbltripconfirm.Text + "' and Leg='" + lblLegConfirm.Text + "' and tripflag<>'B0'");

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Groupid=0,ModifyFlag='',Createdby='',CreatedOn=null,Splittype='',New_StartAirport='',New_EndAirport='' Where Groupid='" + lblgroupiddelete.Text + "' and TripNo='" + lbltripconfirm.Text + "' and Leg='" + lblLegConfirm.Text + "' ");
            }

            else
            {
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  EmptyLegAirport_Child  where GroupId = '" + lblgroupiddelete.Text.Trim() + "' and ModifiedFlag in ('MF','SP') ");

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + lblgroupiddelete.Text + "' and tripflag<>'B0'");

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Groupid=0,ModifyFlag='',Createdby='',CreatedOn=null,Splittype='',New_StartAirport='',New_EndAirport='' Where Groupid='" + lblgroupiddelete.Text + "'");
            }


            objEmptyLegDataUpdateCal.FunEmptyPriceCal(dtOneWayTrip);



            tblForm.Visible = false;
            tblGrid.Visible = true;
            BindEmptyLegs();
            hdfSCID.Value = "0";
            lblalert.Text = "Tail setup details has been deleted successfully";
            mpealert.Show();
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Tail setup details has been deleted successfully ');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            //ImageButton imbExport = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            // Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            Label lblTailNo = (Label)gvrow.FindControl("lblTailNo");
            Label lbltrip = (Label)gvrow.FindControl("lbltrip");
            Label lbldate = (Label)gvrow.FindControl("lbldate");
            Label lblstarttime = (Label)gvrow.FindControl("lblstarttime");
            Label lblstartair = (Label)gvrow.FindControl("lblstartair");
            Label lblendair = (Label)gvrow.FindControl("lblendair");
            Label lblGroupId = (Label)gvrow.FindControl("lblGroupId");
            Label lblType = (Label)gvrow.FindControl("lblType");

            lblDeleteTailNo.Text = lblTailNo.Text;
            lblDeleteTripNo.Text = lbltrip.Text;

            lblDeletModFlag.Text = ((Label)gvrow.FindControl("lblModFlag")).Text;

            if (lblType.Text.Trim() == "Manual DH")
            {
                lbltailconfirm.Text = lblTailNo.Text;
                lbltripconfirm.Text = lbltrip.Text;
                lblDateconfirm.Text = lbldate.Text;
                lblstarttimeconfirm.Text = lblstarttime.Text;
                lblstartairconfirm.Text = lblstartair.Text;
                lblendairconfirm.Text = lblendair.Text;

                mpeconfirm.Show();
            }
            else
            {


                Label lblgroupid = (Label)gvrow.FindControl("lblgroupid");
                Label lblleg = (Label)gvrow.FindControl("lblleg");
                lbltripconfirm.Text = lbltrip.Text;
                lblLegConfirm.Text = lblleg.Text;

                lblgroupiddelete.Text = lblgroupid.Text;

                mpesplit.Show();
                //lbllegnoP.Text = lblName.Text;
            }




        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            #region

            string strStartDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            string strEndDate = DateTime.Now.AddDays(7).ToString("MM/dd/yyyy HH:mm:ss");

            DataTable dtOneWayTrip = objZoomCalculatinDAL.Get_EmptyTripSetup(strStartDate, strEndDate);
            #endregion

            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select RowID from OneWayTail_Setup Where tailno = '" + lbltailconfirm.Text.Trim() + "' and tripno = '" + lbltripconfirm.Text.Trim() + "'").Tables[0];

            if (dt.Rows.Count > 0)
            {
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  EmptyLegAirport_Child  where GroupId = '" + dt.Rows[0]["RowID"].ToString().Trim() + "' and ModifiedFlag='ET' ");
            }


            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  OneWayTail_Setup  where tailno = '" + lbltailconfirm.Text.Trim() + "' and tripno = '" + lbltripconfirm.Text.Trim() + "'");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  Tailsetup_tripDetails  where tailno = '" + lbltailconfirm.Text.Trim() + "' and tripno = '" + lbltripconfirm.Text.Trim() + "'  and Groupid='" + lblgroupiddelete.Text + "'");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  Tailsetup_tripDetails  where tailno = '" + lbltailconfirm.Text.Trim() + "' and tripno = '" + lbltripconfirm.Text.Trim() + "'  and AvailbleFrom='" + lblDateconfirm.Text + "'");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update LivePricingCaculation Set Flag='OLD',GroupId='0',Mod_Flag='',New_StartAirport='',New_EndAirport='' where tailno = '" + lbltailconfirm.Text.Trim() + "' and tripno = '" + lbltripconfirm.Text.Trim() + "' and Groupid='" + lblgroupiddelete.Text + "'");


            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update LivePricingCaculation Set Flag='OLD',GroupId='0',Mod_Flag='',New_StartAirport='',New_EndAirport='' where TailNo='" + lbltailconfirm.Text.Trim() + "' and Start='" + lblstartairconfirm.Text + "' and [END]='" + lblendairconfirm.Text + "' and DDate='" + lblDateconfirm.Text + "' ");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update LivePricingCaculation Set Flag='OLD',GroupId='0',Mod_Flag='',New_StartAirport='',New_EndAirport='' where TailNo='" + lbltailconfirm.Text.Trim() + "' and New_StartAirport='" + lblstartairconfirm.Text + "' and New_EndAirport='" + lblendairconfirm.Text + "' and DDate='" + lblDateconfirm.Text + "' ");



            DataTable dtPS1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where tailno = '" + lbltailconfirm.Text.Trim() + "'").Tables[0];

            if (dtPS1.Rows.Count > 0)
            {

            }
            else
            {
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  Tailsetup  where tailno = '" + lbltailconfirm.Text.Trim() + "'");
            }

            objEmptyLegDataUpdateCal.FunEmptyPriceCal(dtOneWayTrip);

            tblForm.Visible = false;
            tblGrid.Visible = true;
            BindEmptyLegs();
            hdfSCID.Value = "0";
            lblalert.Text = "Tail setup details has been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnAddOnClick(object sender, EventArgs e)
    {
        lblModGroupID.Text = "0";
        lblEarliestTime.Text = string.Empty;
        lblMaxDepartTime.Text = string.Empty;
        lblMinDepartTime.Text = string.Empty;
        lblgroup.Text = string.Empty;
        divPricModLeft.Visible = true;
        divPriceModRight.Visible = true;
        lblmodLeft.Visible = true;
        tblForm.Visible = true;
        tblGrid.Visible = false;
        txtTailNo.Disabled = false;
        txttrip.Disabled = false;
        divstartcity.Visible = true;
        divModify.Visible = true;
        divManualAirport.Visible = false;
        divstartcityLeft.Visible = true;
        divstartcounty.Attributes.Add("style", "display:none");
        btnSave.Text = "Save";
        lblSubScren.Text = "Add a Manual Trip";
        clearform();
        txtTailNo.Focus();
        trSplit.Visible = false;
        rblStatus.Visible = false;
        rblStatus.Visible = true;
        btnSave.Visible = true;
        btnView.Visible = true;
        tbldata.Visible = false;
        trManualandModiy.Visible = true;
        ViewState["dtLegDetails"] = null;
        gvLegDetails.DataSource = null;
        gvLegDetails.DataBind();


        ViewState["dtDepAirport"] = null;
        gvDepAirport.DataSource = null;
        gvDepAirport.DataBind();
        txtDepDisLandPage.Text = string.Empty;
        divDepLan.Visible = false;
        ddlDepSetupBy.ClearSelection();
        ddlDepSetupBy_SelectedIndexChanged(sender, e);

        ViewState["dtArrAirport"] = null;
        gvArrivalAirport.DataSource = null;
        gvArrivalAirport.DataBind();
        txtArrDisLandPage.Text = string.Empty;
        divArrLan.Visible = false;
        ddlArrSetupBy_SelectedIndexChanged(sender, e);
        ddlArrSetupBy.ClearSelection();

        txtMPrice.Attributes.Remove("disabled");
        txtMPrice.Attributes.Remove("cursor");
        lblETSU.Text = "I";
        divPriceModLabel.Visible = false;
        lblEarliestTimeLabel.Text = "";

    }
    protected void btnViewOnClick(object sender, EventArgs e)
    {
        BindEmptyLegs();
        tblForm.Visible = false;
        tblGrid.Visible = true;
        tbldata.Visible = false;
        rblStatus.Visible = true;
    }
    protected void gvTailDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindEmptyLegs();
        gvTailDetails.PageIndex = e.NewPageIndex;
        gvTailDetails.DataBind();
    }
    protected void gvTailDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;
                Label lblGroupID = e.Row.FindControl("lblGroupID") as Label;
                Label lblType = e.Row.FindControl("lblType") as Label;
                Label Label1 = e.Row.FindControl("Label1") as Label;
                Label Label2 = e.Row.FindControl("Label2") as Label;
                Label lblstarttime = e.Row.FindControl("lblstarttime") as Label;
                Label lblEndTime = e.Row.FindControl("lblEndTime") as Label;
                Label lblTailNo = e.Row.FindControl("lblTailNo") as Label;
                Label lbltrip = e.Row.FindControl("lbltrip") as Label;
                Label lblFlyHours = e.Row.FindControl("lblFlyHours") as Label;
                Label lblMFlag = e.Row.FindControl("lblModFlag") as Label;

                ImageButton imgDelete = e.Row.FindControl("imbDelete") as ImageButton;

                if (lblType.Text.Trim() == "FOS DH")
                {
                    lblType.ForeColor = System.Drawing.ColorTranslator.FromHtml("#1B5E20");
                    lblType.Font.Bold = true;
                }
                else if (lblType.Text.Trim() == "Manual DH")
                {
                    lblType.ForeColor = System.Drawing.ColorTranslator.FromHtml("#d32f2f");
                    lblType.Font.Bold = true;
                }
                if (lblGroupID.Text.Trim() != "0" && lblType.Text.Trim() == "Manual DH")
                {
                    imgDelete.Visible = true;
                }

                if (rblStatus.SelectedValue.Trim() == "Booked Trips")
                {
                    if (lblMFlag.Text.Trim() == "NO" && lblType.Text.Trim() == "FOS DH")
                    {
                        imgDelete.Visible = false;
                    }
                    else if (lblMFlag.Text.Trim() != "NO" && lblType.Text.Trim() == "FOS DH")
                    {
                        imgDelete.Visible = true;
                    }
                    else if (lblGroupID.Text.Trim() != "0" && lblType.Text.Trim() == "MSplit")
                    {
                        imgDelete.Visible = true;
                    }
                    else
                    {
                        imgDelete.Visible = false;
                    }
                }

                if (rblStatus.SelectedValue.Trim() == "Group")
                {
                    if (lblGroupID.Text.Trim() == "0" && lblType.Text.Trim() == "Split")
                    {
                        imgDelete.Visible = false;
                    }
                    else if (lblGroupID.Text.Trim() != "0" && lblType.Text.Trim() == "Split" && lblcreatedon.Text != "1/1/1900 12:00:00 AM")
                    {
                        imgDelete.Visible = true;
                    }
                    else
                    {
                        imgDelete.Visible = false;
                    }

                    if (lblGroupID.Text != "" && lblGroupID.Text != "0")
                    {
                        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TailNo='" + lblTailNo.Text.Trim() + "' and TripNo='" + lbltrip.Text.Trim() + "' and GroupID='" + lblGroupID.Text + "' and tripflag='B0'").Tables[0];

                        if (dt.Rows.Count > 0)
                        {
                            string strStart = dt.Rows[0]["Mod_AvilableFrom"].ToString();
                            if (strStart != "")
                            {
                                Label1.Text = Convert.ToDateTime(strStart).ToString("MMM dd,yyyy");
                            }
                            string strEnd = dt.Rows[0]["Mod_AvailableTo"].ToString();
                            if (strEnd != "")
                            {
                                Label2.Text = Convert.ToDateTime(strEnd).ToString("MMM dd,yyyy");
                            }
                            lblstarttime.Text = dt.Rows[0]["Mod_Starttime"].ToString();
                            lblEndTime.Text = dt.Rows[0]["Mod_Endtime"].ToString();
                            lblFlyHours.Text = dt.Rows[0]["Mod_FlyingHours"].ToString();
                        }
                    }
                }

                if (lblcreatedon.Text.Trim() == "1/1/1900 12:00:00 AM")
                {
                    lblcreatedon.Text = null;
                }

                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //  lblMsg.Text = ex.Message;
        }
    }

    protected void Buttonyes_Click(object sender, EventArgs e)
    {
        mpetrip.Hide();
        tblForm.Visible = true;
        tblGrid.Visible = false;
    }
    public DataTable BindEmptyLegs_SP(string strManaeType, string strFromDate, string strToDate, string strPageINdex, string strPageSize)
    {
        SqlParameter[] Get_CostSetDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", false);
        Get_CostSetDetails[0].Value = strManaeType;
        Get_CostSetDetails[1].Value = strFromDate;
        Get_CostSetDetails[2].Value = strToDate;
        Get_CostSetDetails[3].Value = strPageINdex;
        Get_CostSetDetails[4].Value = strPageSize;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", Get_CostSetDetails).Tables[0];
    }
    public DataTable GET_LIVEDATA(string strManaeType, string strFromDate, string strToDate, string strTail, string strTrip)
    {
        SqlParameter[] Get_CostSetDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", false);
        Get_CostSetDetails[0].Value = "DATA";
        Get_CostSetDetails[1].Value = strFromDate;
        Get_CostSetDetails[2].Value = strToDate;
        Get_CostSetDetails[5].Value = strTail;
        Get_CostSetDetails[6].Value = strTrip;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", Get_CostSetDetails).Tables[0];
    }
    void clearform()
    {
        hdfSCID.Value = "0";
        txtTailNo.Value = "";
        txtStartCity.Text = "";
        txtStartTime.Value = "";
        txtEndCity.Text = "";
        txtEndTime.Value = "";
        txtFlyHours1.Value = "";
        txtAvaFrom.Text = "";
        txtAvaTo.Text = "";
        txtNoSeats.Value = "";
        txttrip.Value = "";
        txtMPrice.Value = "";
    }

    protected void btnGroupCloseclick(object sender, EventArgs e)
    {
        try
        {
            mpeGroup.Hide();
            ddlArrSetupBy.ClearSelection();
            ddlDepSetupBy.ClearSelection();
            btnAddDeparture.Visible = true;
            btnAddArrival.Visible = true;
            ddlArrSetupBy_SelectedIndexChanged(sender, e);
            ddlDepSetupBy_SelectedIndexChanged(sender, e);
            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnAddLegOnClick(object sender, EventArgs e)
    {


        trLeg2.Attributes.Add("style", "display:block");
        //trLeg1.Attributes.Add("style", "display:block");
        divstartsplitLeft.Attributes.Add("style", "display:''");
        divstartcounty.Attributes.Add("style", "display:none");
        DataRow drMappingDetails;
        DataTable dtMapping = new DataTable();
        string strDepartTrip = string.Empty;
        string strArrTrip = string.Empty;
        try
        {

            if (Convert.ToDouble(txtSpliFlyHour.Value) < 0.7)
            {
                lblalert.Text = "The Selected route is not allowed for booking. Please modify your request.";
                mpealert.Show();
                if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                }
                return;
            }

            string D4_MinTimeBD = "0.25";
            DataTable dtTail = genclass.GetDataTable("select * from Tailsetup where TailNo='" + lblTailData.Text + "'");
            if (dtTail.Rows.Count > 0)
            {
                D4_MinTimeBD = dtTail.Rows[0]["MINBD"].ToString();
            }
            var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text).Subtract(DateTime.Now);
            var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
            if (Convert.ToDecimal(_totalDaysK10) < Convert.ToDecimal(D4_MinTimeBD))
            {
                lblalert.Text = "The entered Departure Time does not meet the pricing setup rule.";
                mpealert.Show();
                if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                }
                return;
            }

            if (ddlStartCitySplit.Text.Contains(","))
            {

            }
            else
            {
                lblalert.Text = "Invalid start city, Please choose the start city from the list ";
                mpealert.Show();
                if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                }
                return;
            }

            if (ddlLeg1EndCity.Text.Contains("("))
            {

            }
            else
            {
                lblalert.Text = "Invalid end city, Please choose the end city from the list ";
                mpealert.Show();
                if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                }
                return;
            }

            AirportClass.AirportSplit(ddlStartCitySplit.Text, out ICAO, out StateCode, out  CountryCode, out City, out IATA);
            AirportClass.AirportSplit(ddlLeg1EndCity.Text, out EndICAO, out EndStateCode, out  EndCountryCode, out EndCity, out EndIATA);


            // string[] str = ddlStartCitySplit.Text.Trim().Split(',');

            if (ICAO != string.Empty)
            {
                string strAirport = ICAO.Trim();
                DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select distinct icao  from JETEDGE_FUEL..st_airport  where icao ='" + strAirport.Trim() + "'").Tables[0];

                if (dtDuplicateCheck.Rows.Count > 0)
                {

                }
                else
                {
                    lblalert.Text = "Invalid start city, Please choose the start city from the list ";
                    mpealert.Show();
                    if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                    }
                    return;
                }

            }
            else
            {
                lblalert.Text = "Invalid start city, Please choose the start city from the list ";
                if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                }
                mpealert.Show();
                return;
            }

            //string[] str1 = ddlLeg1EndCity.Text.Trim().Split(',');
            if (EndICAO != string.Empty)
            {
                string strAirport = EndICAO.Trim();
                DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select distinct icao  from JETEDGE_FUEL..st_airport  where icao ='" + strAirport.Trim() + "'").Tables[0];

                if (dtDuplicateCheck.Rows.Count > 0)
                {

                }
                else
                {
                    lblalert.Text = "Invalid end city, Please choose the end city from the list ";
                    mpealert.Show();

                    if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                    }

                    return;
                }
            }
            else
            {
                lblalert.Text = "Invalid end city, Please choose the end city from the list ";
                mpealert.Show();

                if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                }
                return;
            }

            if (ViewState["dtLegDetails"] != null)
            {
                dtMapping = (DataTable)ViewState["dtLegDetails"];
            }
            else
            {
                dtMapping.Columns.Clear();
                dtMapping.Columns.Add("TripFlag");
                dtMapping.Columns.Add("TailNo");
                dtMapping.Columns.Add("tripno");
                dtMapping.Columns.Add("Start");
                dtMapping.Columns.Add("startCity");
                dtMapping.Columns.Add("End");
                dtMapping.Columns.Add("EndCity");
                dtMapping.Columns.Add("DTime");
                dtMapping.Columns.Add("ATime");
                dtMapping.Columns.Add("FlyinghoursFOS");
            }
            string strDEpartAlert = string.Empty;

            ddlStartCitySplit.Enabled = false;

            if (dtMapping.Rows.Count > 0)
            {

                DateTime dtPreviousEndTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + dtMapping.Rows[dtMapping.Rows.Count - 1]["ATime"].ToString());
                DateTime dtNextStartTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.ToString());
                if (dtPreviousEndTime.AddHours(1) > dtNextStartTime)
                {
                    strDEpartAlert = "Please ensure that the Start Time is greater than the Last Leg End Time.</br>";
                }

                DateTime dtMaxEndTime = new DateTime();
                if (lblMaxDepartTime.Text == string.Empty)
                    dtMaxEndTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + lblEtimeData.Text);
                else
                    dtMaxEndTime = Convert.ToDateTime(lblMaxDepartTime.Text);

                DateTime dtNextEndTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1EndTime.Text.ToString());

                if (dtMaxEndTime < dtNextEndTime)
                {
                    if (lblEarliestTime1.Text == string.Empty)
                        strDEpartAlert = strDEpartAlert + "Please ensure that the End Time is less than the Trip End Time.</br>";
                    else
                        strDEpartAlert = strDEpartAlert + "Please ensure that the End Time is less than the Latest Arrival Time.</br>";
                }

            }
            else
            {
                DateTime dtMaxEndTime = new DateTime();
                if (lblMaxDepartTime.Text == string.Empty)
                    dtMaxEndTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + lblEtimeData.Text);
                else
                    dtMaxEndTime = Convert.ToDateTime(lblMaxDepartTime.Text);


                DateTime dtPreviousEndTime = new DateTime();
                if (lblEarliestTime1.Text == string.Empty)
                    dtPreviousEndTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + lblStimedata.Text);
                else
                    dtPreviousEndTime = Convert.ToDateTime(lblEarliestTime1.Text);

                DateTime dtNextStartTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.ToString());
                if (dtPreviousEndTime > dtNextStartTime || dtMaxEndTime < dtNextStartTime)
                {
                    if (lblMinDepartTime.Text == string.Empty)
                        strDEpartAlert = "Please ensure that the Start Time is greater than the Trip Start Time.</br>";
                    else
                        strDEpartAlert = "Please ensure that the Start Time is greater than the Earliest Departure Time.</br>";
                }


                DateTime dtNextEndTime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1EndTime.Text.ToString());

                if (dtMaxEndTime < dtNextEndTime)
                {
                    if (lblEarliestTime1.Text == string.Empty)
                        strDEpartAlert = strDEpartAlert + "Please ensure that the End Time is less than the Trip End Time.</br>";
                    else
                        strDEpartAlert = strDEpartAlert + "Please ensure that the End Time is less than the Latest Arrival Time.</br>";
                }


            }

            if (strDEpartAlert != string.Empty)
            {
                if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
                }
                lblalert.Text = strDEpartAlert;
                mpealert.Show();
                return;
            }

            drMappingDetails = dtMapping.NewRow();
            if (gvLegDetails.Rows.Count == 0)
            {
                drMappingDetails["TripFlag"] = "B1";
            }
            else if (gvLegDetails.Rows.Count == 1)
            {
                drMappingDetails["TripFlag"] = "B2";
            }
            else if (gvLegDetails.Rows.Count == 2)
            {
                drMappingDetails["TripFlag"] = "B3";
            }

            drMappingDetails["TailNo"] = lblTailData.Text;
            drMappingDetails["tripno"] = lblTripData.Text;

            // string[] strStartCitySplit = ddlStartCitySplit.Text.Trim().Split(',');

            //if (strStartCitySplit.Length >= 1)
            //{
            drMappingDetails["start"] = ICAO.ToString().Trim();
            //string strCity = strStartCitySplit[1].ToString().Trim();
            //if (strCity.Contains('-'))
            //{
            //    string[] strFrom = strCity.Trim().Split('-');
            //    if (strFrom.Length >= 1)
            //    {
            drMappingDetails["startCity"] = City.ToString().Trim();
            //    }
            //}
            //else
            //{
            //    drMappingDetails["startCity"] = strStartCitySplit[1].ToString().Trim();
            //}

            string strCount = objEarliestTimeDAL.INTExist(ICAO.ToString().Trim(), "AIRT");
            if (Convert.ToInt32(strCount) > 0)
            {
                strDepartTrip = "I";
            }

            //}

            //string[] strEndCitySplit = ddlLeg1EndCity.Text.Trim().Split(',');
            //if (strEndCitySplit.Length >= 1)
            //{
            drMappingDetails["End"] = EndICAO.Trim();
            //string strECity = strEndCitySplit[1].ToString().Trim();
            //if (strECity.Contains('-'))
            //{
            //    string[] strTo = strECity.Trim().Split('-');
            //    if (strTo.Length >= 1)
            //    {
            drMappingDetails["EndCity"] = EndCity.ToString().Trim();
            //    }
            //}
            //else
            //{
            //    drMappingDetails["EndCity"] = strEndCitySplit[1].ToString().Trim();
            //}
            strCount = objEarliestTimeDAL.INTExist(EndICAO.ToString().Trim(), "AIRT");
            if (Convert.ToInt32(strCount) > 0)
            {
                strArrTrip = "I";
            }
            // }

            drMappingDetails["DTime"] = txtSplitLeg1StartTime.Text;
            drMappingDetails["ATime"] = txtSplitLeg1EndTime.Text;
            drMappingDetails["FlyinghoursFOS"] = txtSpliFlyHour.Value.Trim();
            dtMapping.Rows.Add(drMappingDetails);


            ViewState["dtLegDetails"] = dtMapping;
            gvLegDetails.DataSource = dtMapping;
            gvLegDetails.DataBind();

            ddlStartCitySplit.Text = ddlLeg1EndCity.Text.Trim();

            //ddlStartCitySplit.Text = string.Empty;
            ddlLeg1EndCity.Text = string.Empty;
            txtSpliFlyHour.Value = "";

            if (gvLegDetails.Rows.Count == 3)
            {
                btnAddLeg.Visible = false;
                divSplitEnd.Visible = false;
                divSplitFlyHour.Visible = false;
                divstartsplitLeft.Attributes.Add("style", "display:none;");
            }
            else
            {
                btnAddLeg.Visible = true;
                divSplitEnd.Visible = true;
                divSplitFlyHour.Visible = true;
                divstartsplitLeft.Attributes.Add("style", "display:'';");
            }

            if (strDepartTrip == "I" && strArrTrip == "I")
            {
                lblalert.Text = "These trip not allowed for booking.";
                mpealert.Show();
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvLegDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtLegDetails"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtLegDetails"] = dt;
            gvLegDetails.EditIndex = -1;
            gvLegDetails.DataSource = ViewState["dtLegDetails"];
            gvLegDetails.DataBind();

            if (gvLegDetails.Rows.Count == 3)
            {
                btnAddLeg.Visible = false;
                divSplitEnd.Visible = false;
                divSplitFlyHour.Visible = false;
                divstartsplitLeft.Attributes.Add("style", "display:'none';");
            }
            else
            {
                btnAddLeg.Visible = true;
                divSplitEnd.Visible = true;
                divSplitFlyHour.Visible = true;
                divstartsplitLeft.Attributes.Add("style", "display:'';");
            }

            if (gvLegDetails.Rows.Count > 0)
            {
                for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                {
                    Label lblEndCity = (Label)gvLegDetails.Rows[i].FindControl("lblEndCity");

                    Label lblendair = (Label)gvLegDetails.Rows[i].FindControl("lblendair");

                    DataTable dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + lblendair.Text + "'");

                    if (dtcity.Rows.Count > 0)
                    {
                        ddlStartCitySplit.Enabled = false;
                        ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

                    }
                    else
                    {
                        ddlStartCitySplit.Enabled = true;
                        ddlStartCitySplit.Text = string.Empty;
                    }


                }
            }
            else
            {
                DataTable dtcity = genclass.GetDataTable("select ICAO,ICAO+', '+City +' - '+ StateName as ICAOCity  from JETEDGE_FUEL..ST_AIRPORT where ICAO='" + lblStartData.Text + "'");

                if (dtcity.Rows.Count > 0)
                {
                    ddlStartCitySplit.Enabled = false;
                    ddlStartCitySplit.Text = dtcity.Rows[0]["ICAOCity"].ToString();

                }
                else
                {
                    ddlStartCitySplit.Enabled = true;
                    ddlStartCitySplit.Text = string.Empty;
                }

            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvArrivalAirport_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtArrAirport"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtArrAirport"] = dt;
            gvArrivalAirport.EditIndex = -1;
            gvArrivalAirport.DataSource = ViewState["dtArrAirport"];
            gvArrivalAirport.DataBind();

            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }
            if (gvArrivalAirport.Rows.Count > 0)
            {
                string strEndAirportCode = string.Empty;
                string strEndAirportName = string.Empty;

                string strEndStateCode = string.Empty;
                string strEndStateName = string.Empty;

                string strEndCountryCode = string.Empty;
                string strEndCountryName = string.Empty;
                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strEndAirportName += lblAirport.Text.Trim() + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Trim().Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Trim().Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                strEndCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }
                divArrLan.Visible = true;
                txtArrDisLandPage.Text = (strEndCountryName + strEndStateCode + strEndAirportName).TrimEnd('/');
            }
            else
            {
                divArrLan.Visible = false;
                txtArrDisLandPage.Text = string.Empty;
            }
            if (gvArrivalAirport.Rows.Count > 1)
            {
                btnArrGroup.Visible = true;
            }
            else
            {
                btnArrGroup.Visible = false;
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }


    protected void gvDepAirport_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtDepAirport"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtDepAirport"] = dt;
            gvDepAirport.EditIndex = -1;
            gvDepAirport.DataSource = ViewState["dtDepAirport"];
            gvDepAirport.DataBind();

            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }

            if (gvDepAirport.Rows.Count > 0)
            {
                string strAirportCode = string.Empty;
                string strAirportName = string.Empty;

                string strStateCode = string.Empty;
                string strStateName = string.Empty;

                string strCountryCode = string.Empty;
                string strCountryName = string.Empty;

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strAirportName += lblAirport.Text + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Trim().Split(',');

                            if (strSplit.Length > 1)
                            {
                                strStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Trim().Split(',');
                            if (strSplit.Length > 1)
                            {
                                strCountryCode += strSplit[1].ToString().Trim() + "/";
                                strCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }

                divDepLan.Visible = true;

                txtDepDisLandPage.Text = (strCountryName + strStateCode + strAirportName).TrimEnd('/');
            }
            else
            {
                divDepLan.Visible = false;
                txtDepDisLandPage.Text = string.Empty;
            }
            if (gvDepAirport.Rows.Count > 1)
            {
                btnCreateGroup.Visible = true;
            }
            else
            {
                btnCreateGroup.Visible = false;
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            genclass.FillControl(ddlState, "Select Row_ID,State_Name from State where Active = 'Y' and Country_ID='" + ddlCountry.SelectedValue + "' order by State_Name", "ALL");
            ddlCountry.Focus();

            if (rblDepartureSetup.SelectedValue == "ST")
            {
                dvStates.Visible = true;

                DataTable dt = genclass.GetDataTable("select Row_ID, State_Code,State_Name from  JETEDGE_FUEL..State where Country_ID='" + ddlCountry.SelectedValue + "'");
                gvStates.DataSource = dt;
                gvStates.DataBind();
            }
            else
            {
                dvStates.Visible = false;
                gvStates.DataSource = null;
                gvStates.DataBind();
            }

            mpeRoute.Show();
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

        }
    }
    protected void ddlCountry1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();

            BindAirport_Country();

            foreach (GridViewRow row in gvAirport.Rows)
            {
                CheckBox chkChoose = ((CheckBox)row.FindControl("chkChoose"));
                chkChoose.Checked = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

        }
    }
    protected void rblDepartureSetup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();

            dvDepartureAirport.Visible = false;
            dvDepartureState.Visible = false;
            dvCountry.Visible = false;

            rblAirportMap.SelectedIndex = 0;

            if (rblDepartureSetup.SelectedValue == "AI")
            {
                dvDepartureAirport.Visible = true;

                dvStates.Visible = false;
                gvStates.DataSource = null;
                gvStates.DataBind();
                divCountry.Visible = false;
            }
            else if (rblDepartureSetup.SelectedValue == "ST")
            {
                dvDepartureState.Visible = true;

                dvStates.Visible = true;
                DataTable dt = genclass.GetDataTable("select Row_ID, State_Code,State_Name from  JETEDGE_FUEL..State where Country_ID='" + ddlCountry.SelectedValue + "'");
                gvStates.DataSource = dt;
                gvStates.DataBind();

                divCountry.Visible = false;
            }
            else if (rblDepartureSetup.SelectedValue == "CO")
            {
                dvCountry.Visible = false;

                dvStates.Visible = false;
                gvStates.DataSource = null;
                gvStates.DataBind();
                dvDepartureState.Visible = false;
                divCountry.Visible = true;
                //dvDepartureState2.Visible = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    public void BindCountryGrid()
    {
        DataTable dt = genclass.GetDataTable("Select Row_ID,Country_Name,Country_Code from JETEDGE_FUEL..Country where Active = 'Y' order by Country_Name");

        if (dt.Rows.Count > 0)
        {
            gvCountry.DataSource = dt;
            gvCountry.DataBind();
        }
        else
        {
            gvCountry.DataSource = null;
            gvCountry.DataBind();
        }
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();
            BindAirport();

            if (ddlState.SelectedIndex > 0)
            {
                dvDepartureState1.Visible = false;
            }

            foreach (GridViewRow row in gvAirport.Rows)
            {
                CheckBox chkChoose = ((CheckBox)row.FindControl("chkChoose"));
                chkChoose.Checked = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Clear_Popup();
            BindCountryGrid();
            ddlCountry_SelectedIndexChanged(sender, e);
            ddlCountry1_SelectedIndexChanged(sender, e);
            btnRouteAdd.Text = "Submit";
            if (lnkSearch.CommandName == "Start" || btnAddDeparture.CommandName == "Start")
            {
                lblAirportFor.Text = "Start";
            }
            else if (lnkSearch.CommandName == "End" || btnAddArrival.CommandName == "Start")
            {
                lblAirportFor.Text = "End";
            }
            dvStates.Visible = false;
            divCountry.Visible = false;
            mpeRoute.Show();
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    private void Clear_Popup()
    {
        rblDepartureSetup.ClearSelection();
        rblDepartureSetup.SelectedIndex = 0;
        txtFromICAO.Text = string.Empty;

        dvDepartureAirport.Visible = true;
        dvDepartureState.Visible = false;
        dvDepartureState1.Visible = false;
        dvDepartureState2.Visible = false;
        dvCountry.Visible = false;

        BindCountry();
    }
    private void BindCountry()
    {
        genclass.FillControl(ddlCountry, "Select Row_ID, LTRIM(RTRIM(Country_Name)) Country_Name from JETEDGE_FUEL..Country where Active = 'Y' order by Country_Name");
        genclass.FillControl(ddlCountry1, "Select Row_ID, LTRIM(RTRIM(Country_Name)) Country_Name from JETEDGE_FUEL..Country where Active = 'Y' order by Country_Name");

        ddlCountry.SelectedValue = "1";
        ddlCountry1.SelectedValue = "1";

        ddlState.Items.Clear();
        ddlState.Items.Insert(0, new ListItem("Not Available", "0"));
    }

    protected void imbGroupDepDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            if (txtStartTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblAirportId");
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete from AirportGroup where Rowid='" + lblrowid.Text.Trim() + "'");
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete from AirportGroup_Child where Groupid='" + lblrowid.Text.Trim() + "'");

            BindGroup();
            mpeGroup.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnRouteAddDep_Click(object sender, EventArgs e)
    {
        string strAirport = string.Empty;
        string strCode = string.Empty;
        DataRow drDepAirport;
        DataTable dtDepAIrport = new DataTable();
        try
        {
            AirportClass.AirportSplit(txtMod_DepCity.Text, out ICAO, out StateCode, out  CountryCode, out City, out IATA);


            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                string strvalid = "Y";

                if (!txtMod_DepCity.Text.Contains("("))
                {
                    strvalid = "N";
                }



                string[] str = txtMod_DepCity.Text.Trim().Split(',');

                if (ICAO != string.Empty)
                {
                    strAirport = ICAO;
                    DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select distinct icao  from JETEDGE_FUEL..st_airport  where icao ='" + strAirport.Trim() + "'").Tables[0];
                    if (dtDuplicateCheck.Rows.Count == 0)
                    {
                        strvalid = "N";
                    }
                }
                else
                {
                    strvalid = "N";
                }
                if (strvalid == "N")
                {
                    lblalert.Text = "Invalid Depature Airport, Please choose the Depature Airport from the list ";
                    mpealert.Show();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                    return;
                }

            }

            //if (txtMod_DepCity.Text.Contains(","))
            //{

            //}
            //else
            //{
            //    lblalert.Text = "Invalid start city, Please choose the start city from the list ";
            //    mpealert.Show();
            //    if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
            //    }
            //    return;
            //}

            //if (ddlLeg1EndCity.Text.Contains(","))
            //{

            //}
            //else
            //{
            //    lblalert.Text = "Invalid end city, Please choose the end city from the list ";
            //    mpealert.Show();
            //    if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
            //    }
            //    return;
            //}
            //string[] str = ddlStartCitySplit.Text.Trim().Split(',');

            //if (str.Length >= 1)
            //{
            //     strAirport = str[0].ToString().Trim();
            //    DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select distinct icao  from JETEDGE_FUEL..st_airport  where icao ='" + strAirport.Trim() + "'").Tables[0];

            //    if (dtDuplicateCheck.Rows.Count > 0)
            //    {

            //    }
            //    else
            //    {
            //        lblalert.Text = "Invalid start city, Please choose the start city from the list ";
            //        mpealert.Show();
            //        if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
            //        {
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
            //        }
            //        return;
            //    }

            //}
            //else
            //{
            //    lblalert.Text = "Invalid start city, Please choose the start city from the list ";
            //    if (txtSplitLeg1StartTime.Text != "" && txtSplitLeg1EndTime.Text != "")
            //    {
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
            //    }
            //    mpealert.Show();
            //    return;
            //}



            if (ViewState["dtDepAirport"] != null)
            {
                dtDepAIrport = (DataTable)ViewState["dtDepAirport"];
            }
            else
            {
                dtDepAIrport.Columns.Clear();
                dtDepAIrport.Columns.Add("SetupBy");
                dtDepAIrport.Columns.Add("Airport");
                dtDepAIrport.Columns.Add("StateCode");
                dtDepAIrport.Columns.Add("StateID");
                dtDepAIrport.Columns.Add("CountryCode");
                dtDepAIrport.Columns.Add("AirportCode");
                dtDepAIrport.Columns.Add("CreatedBy");
                dtDepAIrport.Columns.Add("CreatedOn");
            }

            drDepAirport = dtDepAIrport.NewRow();

            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                drDepAirport["SetupBy"] = "Airport";
                drDepAirport["Airport"] = txtMod_DepCity.Text.Trim();

                //if (txtMod_DepCity.Text.Contains("-"))
                //{
                //    string strstartcity2 = txtMod_DepCity.Text.Replace(",", "-").Replace(" ", "");
                //    String[] strliststart = strstartcity2.Trim().Split('-');
                //    strCode = strliststart[0].ToString();
                ////}

                strCode = ICAO;

                drDepAirport["AirportCode"] = strCode.Trim();

            }
            else if (ddlDepSetupBy.SelectedValue == "CO")
            {
                drDepAirport["SetupBy"] = "Country";
                drDepAirport["Airport"] = txtModDepCountry.Text.Trim();

                if (txtModDepCountry.Text.Contains(','))
                {
                    string[] strSplit = txtModDepCountry.Text.Trim().Split(',');

                    if (strSplit.Length > 1)
                    {
                        strCode = strSplit[1].ToString().Trim();
                    }
                }

                drDepAirport["CountryCode"] = strCode.Trim();
            }
            else if (ddlDepSetupBy.SelectedValue == "ST")
            {
                drDepAirport["SetupBy"] = "State";
                drDepAirport["Airport"] = txtModDepState.Text.Trim();

                if (txtModDepState.Text.Length > 0)
                {
                    string[] strSplit = txtModDepState.Text.Trim().Split(',');

                    if (strSplit.Length > 1)
                    {

                        string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                        if (strSTCode.Length > 1)
                        {
                            strCode = strSTCode[0].ToString().Trim();
                        }
                        DataTable dtStateID = genclass.GetDataTable("select Row_ID from JETEDGE_FUEL..State WHERE State_Name='" + strSplit[0].ToString().Trim() + "'");
                        if (dtStateID.Rows.Count > 0)
                        {
                            drDepAirport["StateID"] = dtStateID.Rows[0]["Row_ID"].ToString();
                        }
                    }
                }

                drDepAirport["StateCode"] = strCode.Trim();

            }

            drDepAirport["CreatedBy"] = lblUser.Text;
            drDepAirport["CreatedOn"] = DateTime.Now.ToString();
            dtDepAIrport.Rows.Add(drDepAirport);

            ViewState["dtDepAirport"] = dtDepAIrport;
            gvDepAirport.DataSource = dtDepAIrport;
            gvDepAirport.DataBind();

            if (gvDepAirport.Rows.Count > 0)
            {
                string strAirportCode = string.Empty;
                string strAirportName = string.Empty;

                string strStateCode = string.Empty;
                string strStateName = string.Empty;

                string strCountryCode = string.Empty;
                string strCountryName = string.Empty;

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strAirportName += lblAirport.Text + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Trim().Split(',');

                            if (strSplit.Length > 1)
                            {
                                strStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Trim().Split(',');
                            if (strSplit.Length > 1)
                            {
                                strCountryCode += strSplit[1].ToString().Trim() + "/";
                                strCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }



                divDepLan.Visible = true;

                txtDepDisLandPage.Text = (strCountryName + strStateCode + strAirportName).TrimEnd('/');
            }
            else
            {
                divDepLan.Visible = false;
            }
            txtMod_DepCity.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;
            txtModDepState.Text = string.Empty;
            //  ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            ddlDepSetupBy_SelectedIndexChanged(sender, e);

            if (gvDepAirport.Rows.Count > 1)
            {
                btnCreateGroup.Visible = true;
            }
            else
            {
                btnCreateGroup.Visible = false;
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnRouteAddArr_Click(object sender, EventArgs e)
    {
        string strAirport = string.Empty;
        string strCode = string.Empty;
        DataRow drArrAirport;
        DataTable dtArrAIrport = new DataTable();
        try
        {

            AirportClass.AirportSplit(txtArrAirport.Text, out ICAO, out StateCode, out  CountryCode, out City, out IATA);


            if (ddlArrSetupBy.SelectedValue == "CT")
            {
                string strvalid = "Y";

                if (!txtArrAirport.Text.Contains("("))
                {
                    strvalid = "N";
                }

                string[] str = txtArrAirport.Text.Trim().Split(',');

                if (ICAO != string.Empty)
                {
                    strAirport = ICAO.ToString().Trim();
                    DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select distinct icao  from JETEDGE_FUEL..st_airport  where icao ='" + strAirport.Trim() + "'").Tables[0];

                    if (dtDuplicateCheck.Rows.Count == 0)
                    {
                        strvalid = "N";
                    }
                }
                else
                {
                    strvalid = "N";
                }
                if (strvalid == "N")
                {
                    lblalert.Text = "Invalid Arrival Airport, Please choose the Arrival Airport from the list ";
                    mpealert.Show();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                    return;
                }

            }


            if (ViewState["dtArrAirport"] != null)
            {
                dtArrAIrport = (DataTable)ViewState["dtArrAirport"];
            }
            else
            {
                dtArrAIrport.Columns.Clear();
                dtArrAIrport.Columns.Add("SetupBy");
                dtArrAIrport.Columns.Add("Airport");
                dtArrAIrport.Columns.Add("StateCode");
                dtArrAIrport.Columns.Add("StateID");
                dtArrAIrport.Columns.Add("CountryCode");
                dtArrAIrport.Columns.Add("AirportCode");
                dtArrAIrport.Columns.Add("CreatedBy");
                dtArrAIrport.Columns.Add("CreatedOn");

            }
            drArrAirport = dtArrAIrport.NewRow();

            if (ddlArrSetupBy.SelectedValue == "CT")
            {
                drArrAirport["SetupBy"] = "Airport";
                drArrAirport["Airport"] = txtArrAirport.Text;
                //if (txtArrAirport.Text.Contains("-"))
                //{
                //    string strstartcity2 = txtArrAirport.Text.Replace(",", "-").Replace(" ", "");
                //    String[] strliststart = strstartcity2.Trim().Split('-');
                //    strCode = strliststart[0];
                //}
                strCode = ICAO;
                drArrAirport["AirportCode"] = strCode.Trim();
            }
            else if (ddlArrSetupBy.SelectedValue == "CO")
            {
                drArrAirport["SetupBy"] = "Country";
                drArrAirport["Airport"] = txtArrCountry.Text;
                if (txtArrCountry.Text.Contains(','))
                {
                    string[] strSplit = txtArrCountry.Text.Trim().Split(',');

                    if (strSplit.Length > 1)
                    {
                        strCode = strSplit[1].ToString().Trim();
                    }
                }

                drArrAirport["CountryCode"] = strCode.Trim();
            }
            else if (ddlArrSetupBy.SelectedValue == "ST")
            {
                drArrAirport["SetupBy"] = "State";
                drArrAirport["Airport"] = txtArrState.Text;
                if (txtArrState.Text.Length > 0)
                {
                    string[] strSplit = txtArrState.Text.Trim().Split(',');

                    if (strSplit.Length > 1)
                    {
                        string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                        if (strSTCode.Length > 1)
                        {
                            strCode = strSTCode[0].ToString().Trim();
                        }
                        DataTable dtStateID = genclass.GetDataTable("select Row_ID from JETEDGE_FUEL..State WHERE State_Name='" + strSplit[0].ToString().Trim() + "'");
                        if (dtStateID.Rows.Count > 0)
                        {
                            drArrAirport["StateID"] = dtStateID.Rows[0]["Row_ID"].ToString();
                        }
                    }

                }
                drArrAirport["StateCode"] = strCode.Trim();
            }

            drArrAirport["CreatedBy"] = lblUser.Text;
            drArrAirport["CreatedOn"] = DateTime.Now.ToString();
            dtArrAIrport.Rows.Add(drArrAirport);

            ViewState["dtArrAirport"] = dtArrAIrport;
            gvArrivalAirport.DataSource = dtArrAIrport;
            gvArrivalAirport.DataBind();

            if (gvArrivalAirport.Rows.Count > 0)
            {
                string strEndAirportCode = string.Empty;
                string strEndAirportName = string.Empty;

                string strEndStateCode = string.Empty;
                string strEndStateName = string.Empty;

                string strEndCountryCode = string.Empty;
                string strEndCountryName = string.Empty;
                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strEndAirportName += lblAirport.Text.Trim() + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Trim().Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Trim().Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                strEndCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }
                divArrLan.Visible = true;
                txtArrDisLandPage.Text = (strEndCountryName + strEndStateCode + strEndAirportName).TrimEnd('/');
            }
            else
            {
                divArrLan.Visible = false;
            }

            txtArrAirport.Text = string.Empty;
            txtArrCountry.Text = string.Empty;
            txtArrState.Text = string.Empty;

            // ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            ddlArrSetupBy_SelectedIndexChanged(sender, e);

            if (gvArrivalAirport.Rows.Count > 1)
            {
                btnArrGroup.Visible = true;
            }
            else
            {
                btnArrGroup.Visible = false;
            }




        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void rblAirportMap_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();
            dvDepartureState2.Visible = false;

            if (rblAirportMap.SelectedValue == "ALL")
            {
                dvDepartureState2.Visible = false;
            }
            else
            {
                dvDepartureState2.Visible = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void ddlDepSetupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            txtMod_DepCity.Text = string.Empty;
            txtModDepState.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;

            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                txtMod_DepCity.Visible = true;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = false;
                btnCreateGroup.Visible = true;
                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "ST")
            {
                txtMod_DepCity.Visible = false;
                txtModDepState.Visible = true;
                txtModDepCountry.Visible = false;
                btnCreateGroup.Visible = true;
                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "CO")
            {
                txtMod_DepCity.Visible = false;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = true;
                btnCreateGroup.Visible = true;
                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "GP")
            {
                BindGroup();
                mpeGroup.Show();
                btnCreateGroup.Visible = false;
                divDepart.Visible = true;
                divArrival.Visible = false;
                btnAddDeparture.Visible = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void ddlArrSetupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            txtArrAirport.Text = string.Empty;
            txtArrState.Text = string.Empty;
            txtArrCountry.Text = string.Empty;

            if (ddlArrSetupBy.SelectedValue == "CT")
            {
                txtArrAirport.Visible = true;
                txtArrState.Visible = false;
                txtArrCountry.Visible = false;
                btnArrGroup.Visible = true;
                btnAddArrival.Visible = true;
            }
            else if (ddlArrSetupBy.SelectedValue == "ST")
            {
                txtArrAirport.Visible = false;
                txtArrState.Visible = true;
                txtArrCountry.Visible = false;
                btnArrGroup.Visible = true;
                btnAddArrival.Visible = true;
            }
            else if (ddlArrSetupBy.SelectedValue == "CO")
            {
                txtArrAirport.Visible = false;
                txtArrState.Visible = false;
                txtArrCountry.Visible = true;
                btnArrGroup.Visible = true;
                btnAddArrival.Visible = true;
            }
            else if (ddlArrSetupBy.SelectedValue == "GP")
            {
                BindGroup();
                mpeGroup.Show();
                divDepart.Visible = false;
                divArrival.Visible = true;
                btnArrGroup.Visible = false;
                btnAddArrival.Visible = false;

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void btnDepCreateGroup(object sender, EventArgs e)
    {
        try
        {

            DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  AirportGroup  where GroupName ='" + txtDepDisLandPage.Text.Trim() + "'").Tables[0];

            if (dtDuplicateCheck.Rows.Count > 0)
            {
                lblalert.Text = "Group Name [ " + txtDepDisLandPage.Text.Trim() + " ] already exist";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                return;
            }


            string strRtVal = SaveDepartureGroup("I", "0", txtDepDisLandPage.Text, "Dep", Request.Cookies["JETRIPSADMIN"]["UserId"].ToString());

            if (strRtVal.ToString() != "")
            {
                string strModFlag = string.Empty;
                if (lblgroup.Text == "Modify")
                {
                    strModFlag = "MF";
                }
                else
                {
                    strModFlag = "ET";
                }

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAir = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");
                    Label lblCreatedBy = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedBy");
                    Label lblCreatedOn = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedOn");
                    Label lblStateCode = (Label)gvDepAirport.Rows[i].FindControl("lblStateCode");
                    Label lblCountryCode = (Label)gvDepAirport.Rows[i].FindControl("lblCountryCode");
                    Label lblAirportCode = (Label)gvDepAirport.Rows[i].FindControl("lblAirportCode");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO AirportGroup_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + strRtVal.ToString().Trim() + "','" + strModFlag + "','Dep', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text + "','" + lblCountryCode.Text + "','" + lblAirportCode.Text + "')");
                }
            }
            lblalert.Text = "Group [ " + txtDepDisLandPage.Text.Trim() + " ] created successfully";
            mpealert.Show();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnArrCreateGroup(object sender, EventArgs e)
    {
        try
        {
            DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  AirportGroup  where GroupName ='" + txtArrDisLandPage.Text.Trim() + "'").Tables[0];

            if (dtDuplicateCheck.Rows.Count > 0)
            {
                lblalert.Text = "Group Name [ " + txtArrDisLandPage.Text.Trim() + " ] already exist";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                return;
            }


            string strRtVal = SaveDepartureGroup("I", "0", txtDepDisLandPage.Text, "Arr", Request.Cookies["JETRIPSADMIN"]["UserId"].ToString());

            if (strRtVal.ToString() != "")
            {
                string strModFlag = string.Empty;
                if (lblgroup.Text == "Modify")
                {
                    strModFlag = "MF";
                }
                else
                {
                    strModFlag = "ET";
                }

                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAir = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");
                    Label lblCreatedBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedBy");
                    Label lblCreatedOn = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedOn");

                    Label lblStateCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateCode");
                    Label lblCountryCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblCountryCode");
                    Label lblAirportCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblAirportCode");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO AirportGroup_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + strRtVal.ToString().Trim() + "','" + strModFlag + "','Arr', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text + "','" + lblCountryCode.Text + "','" + lblAirportCode.Text + "')");
                }

                lblalert.Text = "Group [ " + txtDepDisLandPage.Text.Trim() + " ] created successfully";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvGroup.SelectedRow.FindControl("lblAirportId");

            Label lblGroupName = (Label)gvGroup.SelectedRow.FindControl("lblGroupName");

            DataTable dtGetGroupAirport = BindGroupChildData(lblrowid.Text);


            DataTable dtDepAIrport = new DataTable();
            if (ViewState["dtDepAirport"] != null)
            {
                dtDepAIrport = (DataTable)ViewState["dtDepAirport"];
            }
            else
            {
                dtDepAIrport.Columns.Clear();
                dtDepAIrport.Columns.Add("SetupBy");
                dtDepAIrport.Columns.Add("Airport");
                dtDepAIrport.Columns.Add("StateCode");
                dtDepAIrport.Columns.Add("StateID");
                dtDepAIrport.Columns.Add("CountryCode");
                dtDepAIrport.Columns.Add("AirportCode");
                dtDepAIrport.Columns.Add("CreatedBy");
                dtDepAIrport.Columns.Add("CreatedOn");

            }
            dtDepAIrport.Merge(dtGetGroupAirport);

            if (dtGetGroupAirport.Rows.Count > 0)
            {
                gvDepAirport.DataSource = dtDepAIrport;
                gvDepAirport.DataBind();
                ViewState["dtDepAirport"] = dtDepAIrport;
                divDepLan.Visible = true;
            }
            else
            {
                gvDepAirport.DataSource = null;
                gvDepAirport.DataBind();
                divDepLan.Visible = false;
            }

            txtDepDisLandPage.Text = lblGroupName.Text;
            ddlDepSetupBy.ClearSelection();
            ddlDepSetupBy_SelectedIndexChanged(sender, e);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvGroupArrival_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvGroupArrival.SelectedRow.FindControl("lblAirportId");

            Label lblGroupName = (Label)gvGroupArrival.SelectedRow.FindControl("lblGroupName");

            DataTable dtGetGroupAirport = BindGroupChildData(lblrowid.Text);

            DataTable dtArrAIrport = new DataTable();
            if (ViewState["dtArrAirport"] != null)
            {
                dtArrAIrport = (DataTable)ViewState["dtArrAirport"];
            }
            else
            {
                dtArrAIrport.Columns.Clear();
                dtArrAIrport.Columns.Add("SetupBy");
                dtArrAIrport.Columns.Add("Airport");
                dtArrAIrport.Columns.Add("StateCode");
                dtArrAIrport.Columns.Add("StateID");
                dtArrAIrport.Columns.Add("CountryCode");
                dtArrAIrport.Columns.Add("AirportCode");
                dtArrAIrport.Columns.Add("CreatedBy");
                dtArrAIrport.Columns.Add("CreatedOn");

            }
            dtArrAIrport.Merge(dtGetGroupAirport);


            if (dtGetGroupAirport.Rows.Count > 0)
            {
                gvArrivalAirport.DataSource = dtArrAIrport;
                gvArrivalAirport.DataBind();
                ViewState["dtArrAirport"] = dtArrAIrport;
                divArrLan.Visible = true;
            }
            else
            {
                gvArrivalAirport.DataSource = null;
                gvArrivalAirport.DataBind();
                divArrLan.Visible = false;
            }


            txtArrDisLandPage.Text = lblGroupName.Text;
            ddlArrSetupBy.ClearSelection();
            ddlArrSetupBy_SelectedIndexChanged(sender, e);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    private void BindAirport_Country()
    {
        DataTable dtAirport = BindAirport_Country(ddlCountry1.SelectedValue);
        if (dtAirport.Rows.Count > 0)
        {
            gvAirport.DataSource = dtAirport;
            gvAirport.DataBind();
        }
        else
        {
            gvAirport.DataSource = null;
            gvAirport.DataBind();
        }
    }
    private void BindAirport()
    {
        DataTable dtAirport = BindAirport(ddlCountry.SelectedValue, ddlState.SelectedValue);
        if (dtAirport.Rows.Count > 0)
        {
            gvAirport.DataSource = dtAirport;
            gvAirport.DataBind();
        }
        else
        {
            gvAirport.DataSource = null;
            gvAirport.DataBind();
        }
    }


    public DataTable BindAirport(string CountryId, string StateId)
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", false);
        GetLegDetails[0].Value = "SEL";
        GetLegDetails[1].Value = CountryId;
        GetLegDetails[2].Value = StateId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", GetLegDetails).Tables[0];
    }

    public DataTable BindGroupData()
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", false);
        GetLegDetails[0].Value = "S";
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", GetLegDetails).Tables[0];
    }

    public DataTable BindGroupChildData(string strGroupId)
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", false);
        GetLegDetails[0].Value = "GC";
        GetLegDetails[5].Value = strGroupId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", GetLegDetails).Tables[0];
    }

    public DataTable BindAirport_Country(string CountryId)
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", false);
        GetLegDetails[0].Value = "SELC";
        GetLegDetails[1].Value = CountryId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", GetLegDetails).Tables[0];
    }

    public DataTable AirRouteCalculation(string StartAirport, string strEndAirport, string strSpeed)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
        string strRoot = StartAirport + "-" + strEndAirport;

        string WEBSERVICE_URL = "https://greatcirclemapper.p.rapidapi.com/airports/route/";
        WEBSERVICE_URL = WEBSERVICE_URL + strRoot + @"/" + strSpeed;

        var jsonResponse = "";

        var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
        if (webRequest != null)
        {
            webRequest.Method = "GET";
            webRequest.Timeout = 20000;
            webRequest.ContentType = "text/html;charset=UTF-8"; //"application/json";

            webRequest.Headers.Add("X-RapidAPI-Host", "greatcirclemapper.p.rapidapi.com");
            webRequest.Headers.Add("X-RapidAPI-Key", SecretsBLL.RapidAPIKey.ToString());
            webRequest.Headers.Add("vary", "Accept-Encoding");

            //webRequest.Headers.Add("content-type", "text/html;charset=UTF-8");

            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                {
                    jsonResponse = sr.ReadToEnd();
                }
            }
        }
        LegDetails8 leg = new LegDetails8();
        List<LegDetails8> RouteListLeg = new List<LegDetails8>();

        JavaScriptSerializer js = new JavaScriptSerializer();
        dynamic d = js.Deserialize<dynamic>(jsonResponse.ToString());

        dynamic[] arrlegs = d["legs"];

        for (int i = 0; i < arrlegs.Length; i++)
        {
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(arrlegs[i]);

            dynamic dInnerLeg = js.Deserialize<dynamic>(json);

            leg = new LegDetails8();

            leg.legNo = Convert.ToString(i + 1);
            leg.startICAO = dInnerLeg["origin"]["ident"];
            leg.endICAO = dInnerLeg["destination"]["ident"];
            leg.distance_km = Convert.ToString(arrlegs[i]["distance_km"]);
            leg.distance_nm = Convert.ToString(arrlegs[i]["distance_nm"]);
            leg.flight_time_min = Convert.ToString(arrlegs[i]["flight_time_min"]);
            leg.heading_deg = Convert.ToString(arrlegs[i]["heading_deg"]);
            leg.heading_compass = Convert.ToString(arrlegs[i]["heading_compass"]);

            RouteListLeg.Add(leg);
        }

        DataTable dtResul = ToDataTable(RouteListLeg);

        return dtResul;

    }
    public DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }

        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }

        //put a breakpoint here and check datatable
        return dataTable;

    }
    public DataTable RapidAPIDatatable()
    {
        DataTable dtRapid = new DataTable();
        dtRapid.Columns.Add("legNo");
        dtRapid.Columns.Add("startICAO");
        dtRapid.Columns.Add("endICAO");
        dtRapid.Columns.Add("distance_km");
        dtRapid.Columns.Add("distance_nm");
        dtRapid.Columns.Add("flight_time_min");
        dtRapid.Columns.Add("heading_deg");
        dtRapid.Columns.Add("heading_compass");

        DataRow row = dtRapid.NewRow();
        row["legNo"] = 1;
        row["startICAO"] = "KVNY";
        row["endICAO"] = "KSNA";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        row = dtRapid.NewRow();
        row["legNo"] = 2;
        row["startICAO"] = "KSNA";
        row["endICAO"] = "KVNY";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        return dtRapid;


    }


    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

            lblPCurrent.Text = lblUPDPrice.Text;
            txtRevisedPrice.Text = lblUPDPrice.Text.Replace("$", "").Replace(",", "");
            txtReasons.Text = string.Empty;
            DataSet dsRawData = objZoomCalculatinDAL.Get_EmptyTripSetupTrip(lblTripData.Text, lbllegdisplay.Text);

            DataTable dtRawData = dsRawData.Tables[1];
            if (dtRawData.Rows.Count > 0)
            {
                lblPRoute.Text = dtRawData.Rows[0]["StartAirport"].ToString() + "-" + dtRawData.Rows[0]["EndAirport"].ToString();
                lblPHourlyRate.Text = String.Format("{0:C0}", Convert.ToDecimal(dtRawData.Rows[0]["HourlyRate"].ToString()));
            }

            if (dsRawData.Tables[0].Rows.Count > 0)
            {
                lblMinimumPrice.Text = String.Format("{0:C0}", Convert.ToDecimal(dsRawData.Tables[0].Rows[0]["CurrentMinPrice"]));
            }


            mpeAmountUpdate.Show();

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnPriceUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);


            if (Convert.ToDecimal(lblMinimumPrice.Text.Replace("$", "").Replace(",", "")) > Convert.ToDecimal(txtRevisedPrice.Text.Replace("$", "").Replace(",", "")))
            {
                lblMINMinimumPrice.Text = lblMinimumPrice.Text;
                lblMINRevisedPrice.Text = String.Format("{0:C0}", Convert.ToDecimal(txtRevisedPrice.Text));
                mpeMinPrice.Show();
                return;

            }



            objEmptyLegPriceCal.PriceUpdate(lblTripData.Text, lbllegdisplay.Text, Convert.ToInt32(txtRevisedPrice.Text), lblPCurrent.Text, Request.Cookies["JETRIPSADMIN"]["UserId"].ToString(), txtReasons.Text);
            lblUPDPrice.Text = String.Format("{0:C0}", Convert.ToInt32(txtRevisedPrice.Text));
            lblUPDPriceET.Text = String.Format("{0:C0}", Convert.ToInt32(txtRevisedPrice.Text));
            lblalert.Text = "Revised price has been updated Successfully.";
            mpealert.Show();

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnMinUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);


            objEmptyLegPriceCal.PriceUpdate(lblTripData.Text, lbllegdisplay.Text, Convert.ToInt32(txtRevisedPrice.Text), lblPCurrent.Text, Request.Cookies["JETRIPSADMIN"]["UserId"].ToString(), txtReasons.Text);
            lblUPDPrice.Text = String.Format("{0:C0}", Convert.ToInt32(txtRevisedPrice.Text));
            lblUPDPriceET.Text = String.Format("{0:C0}", Convert.ToInt32(txtRevisedPrice.Text));
            lblalert.Text = "Revised price has been updated Successfully.";
            mpealert.Show();

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }


    //protected void btnPriceUpdate_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

    //        objEmptyLegPriceCal.PriceUpdate(lblTripData.Text, lbllegdisplay.Text, Convert.ToInt32(txtRevisedPrice.Text));
    //        lblUPDPrice.Text = String.Format("{0:C0}", Convert.ToInt32(txtRevisedPrice.Text));
    //        lblalert.Text = "Revised price has been updated Successfully.";
    //        mpealert.Show();

    //    }
    //    catch (Exception ex)
    //    {
    //        int linenum = 0;
    //        linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
    //        System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
    //    }
    //}



    public void lnkbutton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
        RapidAPI objRapidAPI = new RapidAPI();
        DataTable dt = genclass.GetDataTable("select * from Globalconfig ");

        if (dt.Rows.Count > 0)
        {
            DateTime strDepttime = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text);

            DataTable dtGC = objRapidAPI.FlightHourCalculation(ddlStartCitySplit.Text.Split(',')[0].Trim(), ddlLeg1EndCity.Text.Split(',')[0].Trim(), "", strDepttime.ToString(), "420", dt.Rows[0]["FlightHourFlag"].ToString());
            if (dtGC.Rows.Count > 0)
            {
                double mini = Convert.ToDouble(dtGC.Rows[0]["flight_time_min"].ToString());
                TimeSpan spWorkMin = TimeSpan.FromMinutes(mini);
                string strHourMinute = spWorkMin.ToString(@"hh\:mm");
                txtSpliFlyHour.Value = (mini / 60).ToString("0.0");
            }


        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText)
    {
        return AutoFillProducts_V1(prefixText);

    }

    private static List<string> AutoFillProducts_V1(string prefixText)
    {
        DataTable dtNew = new DataTable();
        DataTable dt = new DataTable();
        DataRow[] drNew = null;
        if (HttpContext.Current.Session["ICAOC"] == null)
        {
            dt = genclass.GetZoomAirports("YES");

            HttpContext.Current.Session["ICAOC"] = dt;
        }

        dt = (DataTable)HttpContext.Current.Session["ICAOC"];

        if (dt.Rows.Count > 0)
        {
            if (prefixText.Length < 5)
            {
                drNew = dt.Select("ICAO like '%' + '" + prefixText + "' + '%'");
            }
            else
            {
                drNew = dt.Select("StateName like  '" + prefixText + "' + '%' OR City like '" + prefixText + "' + '%'");
            }

            if (drNew.Length > 0)
            {
                dtNew = drNew.CopyToDataTable();
            }
        }
        List<string> sEmail = new List<string>();
        for (int i = 0; i < drNew.Length; i++)
        {
            sEmail.Add(drNew[i]["ICAOName"].ToString());
        }

        //foreach (DataRow row in dtNew.Rows)
        //{
        //    sEmail.Add(row[0].ToString());
        //}
        return sEmail;
    }


    private static List<string> AutoFillProducts(string prefixText)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {
                if (prefixText.Length > 4)
                {
                    com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' AND  ZoomFlag='Yes'";
                }
                else
                {
                    com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "icao like  @Search + '%' AND  ZoomFlag='Yes'";
                }
                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;


            }

        }
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCity(string prefixText, int count)
    {
        return AutoFillProducts_V1(prefixText);
        //using (SqlConnection con = new SqlConnection())
        //{
        //    con.ConnectionString = SqlHelper.FLYTConnectionString;
        //    using (SqlCommand com = new SqlCommand())
        //    {
        //        if (prefixText.Length > 4)
        //        {
        //            com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' AND Access=1";
        //        }
        //        else
        //        {
        //            com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "icao like  @Search + '%' AND Access=1";
        //        }
        //        com.Parameters.AddWithValue("@Search", prefixText);
        //        com.Connection = con;
        //        con.Open();
        //        List<string> countryNames = new List<string>();
        //        using (SqlDataReader sdr = com.ExecuteReader())
        //        {
        //            while (sdr.Read())
        //            {
        //                countryNames.Add(sdr["city"].ToString());
        //            }
        //        }
        //        con.Close();
        //        return countryNames;
        //    }
        //}
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCityNew(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%' And Country='1' AND Access=1";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepState(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select S.State_Name+' , '+S.State_Code+ ' - '+C.Country_Name as State from JETEDGE_FUEL..State S Left Join JETEDGE_FUEL..Country C on C.Row_ID=S.Country_ID where " + "State_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["State"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepStateNEW(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select S.State_Name+' , '+S.State_Code+ ' - '+C.Country_Name as State from JETEDGE_FUEL..State S Left Join JETEDGE_FUEL..Country C on C.Row_ID=S.Country_ID where " + "State_Name like  @Search + '%' and C.Row_ID=1";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["State"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCountry(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct Country_Name+ ' , '+Country_Code as Country from JETEDGE_FUEL..Country where " + "Country_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["Country"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCountryNEW(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {
                com.CommandText = "select distinct Country_Name+ ' , '+Country_Code as Country from JETEDGE_FUEL..Country where " + "Country_Name like  @Search + '%' and Row_ID=1";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["Country"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }


    public class LegDetails8
    {
        public string legNo { get; set; }
        public string startICAO { get; set; }
        public string endICAO { get; set; }
        public string distance_km { get; set; }
        public string distance_nm { get; set; }
        public string flight_time_min { get; set; }
        public string heading_deg { get; set; }
        public string heading_compass { get; set; }
    }
}