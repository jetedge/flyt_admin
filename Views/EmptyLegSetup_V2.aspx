﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="EmptyLegSetup_V2.aspx.cs" Inherits="Views_EmptyLegSetup_V2" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript">
        function GetSelectedClient(checkboxlist, textbox) {
            var appendvalue = "";
            var cblclient = document.getElementById(checkboxlist.toString());
            var txtClient = document.getElementById(textbox.toString());
            var checkbox = cblclient.getElementsByTagName("input");
            var label = cblclient.getElementsByTagName("label");
            var splitval = 0;
            for (var i = 0; i < checkbox.length; i++) {

                if (checkbox[i].checked) {

                    appendvalue += label[i].innerHTML;
                    splitval++;
                }
            }
            txtClient.value = appendvalue;
            return false;
        }
    </script>
    <script type="text/javascript">
        function MutExChkList(chk) {
            var chkList = chk.parentNode.parentNode.parentNode;
            var chks = chkList.getElementsByTagName("input");
            for (var i = 0; i < chks.length; i++) {
                if (chks[i] != chk && chk.checked) {
                    chks[i].checked = false;
                }
            }
        }
    </script>
    <script type="text/javascript">
        function HidePopupClose() {
            debugger;
            var popup = $find('MainContent_popupcountry');
            popup.hidePopup();
        }
    </script>
    <script type="text/javascript" language="javascript">
        function CheckDate(sender, args) {


            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;

            var today1 = sender._selectedDate;
            var date1 = today1.getFullYear() + '-' + (today1.getMonth() + 1) + '-' + today1.getDate();
            var time1 = today1.getHours() + ":" + today1.getMinutes() + ":" + today1.getSeconds();
            var dateTime1 = date1 + ' ' + time1;

            var dateOne = new Date(today.getFullYear(), (today.getMonth() + 1), today.getDate()); //Year, Month, Date    
            var dateTwo = new Date(today1.getFullYear(), (today1.getMonth() + 1), today1.getDate()); //Year, Month, Date    

            if (dateOne > dateTwo) {
                document.getElementById('MainContent_lblalert').innerHTML = 'You cannot select past date!';
                $find("mpealert").show();
                // alert("You cannot select past date!");
                sender._selectedDate = new Date();
                sender._textbox.set_Value('')
                return;
            }

            if (document.getElementById("MainContent_txtAvaFrom").value != '' && document.getElementById("MainContent_txtAvaTo").value != '') {
                var endDate = new Date(document.getElementById("MainContent_txtAvaTo").value);
                var startDate = new Date(document.getElementById("MainContent_txtAvaFrom").value);

                if (startDate > endDate) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'Please ensure that the Available to is greater than or equal to the Available from.';
                    $find("MainContent_mpealert").show();
                    // alert("Please ensure that the Available to is greater than or equal to the Available from.");
                    sender._textbox.set_Value('')
                    return false;
                }
            }
        }   
    </script>
    <script type="text/javascript">
        function GetSelectedClient(checkboxlist, textbox) {
            var appendvalue = "";
            var cblclient = document.getElementById(checkboxlist.toString());
            var txtClient = document.getElementById(textbox.toString());
            var checkbox = cblclient.getElementsByTagName("input");
            var label = cblclient.getElementsByTagName("label");
            var splitval = 0;
            for (var i = 0; i < checkbox.length; i++) {

                if (checkbox[i].checked) {
                    if (appendvalue.length == 0) {
                        appendvalue += label[i].innerHTML;
                        splitval++;
                    }
                    else {
                        appendvalue += "/" + label[i].innerHTML;
                        splitval++;
                    }
                }
            }
            txtClient.value = appendvalue;
            return false;
        }
        
    </script>
    <script type="text/javascript">
        function CheckCheck() {
            debugger;
            var chkBoxList = document.getElementById('MainContent_cblcountry');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            var i = 0;
            var tot = 0;
            for (i = 0; i < chkBoxCount.length; i++) {
                if (chkBoxCount[i].checked) {
                    tot = tot + 1;
                    if (tot > 2) {
                        document.getElementById('MainContent_lblalert').innerHTML = 'You can not select more than two countries';
                        $find("MainContent_mpealert").show();
                        chkBoxCount[i].checked = false;
                    }
                }
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        function getCheckedRadio() {
            debugger;
            var list = document.getElementById("MainContent_rblBreak"); //Client ID of the radiolist
            var inputs = list.getElementsByTagName("input");
            var selected;
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                    selected = inputs[i];
                    break;
                }
            }

            if (selected.value == 'CT') {
                document.getElementById("MainContent_trLeg2").style.display = '';
                document.getElementById("MainContent_trLeg1").style.display = '';
                document.getElementById("MainContent_divstartsplitLeft").style.display = '';
                document.getElementById("MainContent_divstartcounty").style.display = 'none';

            }
            else {
                document.getElementById("MainContent_trLeg2").style.display = 'none';
                document.getElementById("MainContent_trLeg1").style.display = 'none';
                document.getElementById("MainContent_divstartsplitLeft").style.display = 'none';
                document.getElementById("MainContent_divstartcounty").style.display = '';
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        function GetDepAirport() {
            var e = document.getElementById("MainContent_ddlDepSetupBy");

            if (e.options[e.selectedIndex].value == 'CT') {
                document.getElementById("MainContent_txtModDepCity").style.display = '';
                document.getElementById("MainContent_txtModDepState").style.display = 'none';
                document.getElementById("MainContent_txtModDepCountry").style.display = 'none';

                document.getElementById("MainContent_txtModDepState").value = '';
                document.getElementById("MainContent_txtModDepCountry").value = '';
            }
            else if (e.options[e.selectedIndex].value == 'CO') {
                document.getElementById("MainContent_txtModDepCity").style.display = 'none';
                document.getElementById("MainContent_txtModDepState").style.display = 'none';
                document.getElementById("MainContent_txtModDepCountry").style.display = '';

                document.getElementById("MainContent_txtModDepCity").value = '';
                document.getElementById("MainContent_txtModDepState").value = '';

            }
            else if (e.options[e.selectedIndex].value == 'ST') {
                document.getElementById("MainContent_txtModDepCity").style.display = 'none';
                document.getElementById("MainContent_txtModDepState").style.display = '';
                document.getElementById("MainContent_txtModDepCountry").style.display = 'none';

                document.getElementById("MainContent_txtModDepCity").value = '';
                document.getElementById("MainContent_txtModDepCountry").value = '';
            }

            document.getElementById('txtStartTime1').value = document.getElementById('MainContent_txtStartTime').value;
            document.getElementById('txtEndTime1').value = document.getElementById('MainContent_txtEndTime').value;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function GetArrivalAirport() {
            var e = document.getElementById("MainContent_ddlArrSetupBy");

            if (e.options[e.selectedIndex].value == 'CT') {
                document.getElementById("MainContent_txtArrAirport").style.display = '';
                document.getElementById("MainContent_txtArrState").style.display = 'none';
                document.getElementById("MainContent_txtArrCountry").style.display = 'none';

                document.getElementById("MainContent_txtArrState").value = '';
                document.getElementById("MainContent_txtArrCountry").value = '';
            }
            else if (e.options[e.selectedIndex].value == 'CO') {
                document.getElementById("MainContent_txtArrAirport").style.display = 'none';
                document.getElementById("MainContent_txtArrState").style.display = 'none';
                document.getElementById("MainContent_txtArrCountry").style.display = '';

                document.getElementById("MainContent_txtArrAirport").value = '';
                document.getElementById("MainContent_txtArrState").value = '';

            }
            else if (e.options[e.selectedIndex].value == 'ST') {
                document.getElementById("MainContent_txtArrAirport").style.display = 'none';
                document.getElementById("MainContent_txtArrState").style.display = '';
                document.getElementById("MainContent_txtArrCountry").style.display = 'none';

                document.getElementById("MainContent_txtArrAirport").value = '';
                document.getElementById("MainContent_txtArrCountry").value = '';
            }


            document.getElementById('txtStartTime1').value = document.getElementById('MainContent_txtStartTime').value;
            document.getElementById('txtEndTime1').value = document.getElementById('MainContent_txtEndTime').value;
        }
    </script>
    <script type="text/javascript">
        function txtCounStartTimefun(object) {
            var txtCounStartTime = document.getElementById('txtCouStartTime1');
            document.getElementById('MainContent_txtCouStartTime').value = txtCounStartTime.value;
        }
        function txtCityStartTimefun(object) {
            var txtCityStartTim = document.getElementById('txtSplitLeg1StartTime1');
            document.getElementById('MainContent_txtSplitLeg1StartTime').value = txtCityStartTim.value;
        }
        function txtLeg1EndTimefun(object) {
            var txtLeg1EndTime = document.getElementById('txtSplitLeg1EndTime1');
            document.getElementById('MainContent_txtSplitLeg1EndTime').value = txtLeg1EndTime.value;
        }



        function txtManualStartTimefun(object) {
            var txtLeg2EndTime = document.getElementById('txtStartTime1');
            document.getElementById('MainContent_txtStartTime').value = txtLeg2EndTime.value;
        }
        function txtManualEndTimefun(object) {
            var txtLeg2EndTime = document.getElementById('txtEndTime1');
            document.getElementById('MainContent_txtEndTime').value = txtLeg2EndTime.value;
        }

        function txtStartTimefun(object) {
            var txtST = document.getElementById('txtManSTime1');
            document.getElementById('MainContent_txtManSTime').value = txtST.value;
        }
        function txtEndTimefun(object) {
            var txtET = document.getElementById('txtMEndTime1');
            document.getElementById('MainContent_txtMEndTime').value = txtET.value;
        }
        
    </script>
    <script type="text/javascript">
        function Split() {
            document.getElementById('txtCouStartTime1').value = document.getElementById('MainContent_txtCouStartTime').value;
            document.getElementById('txtSplitLeg1StartTime1').value = document.getElementById('MainContent_txtSplitLeg1StartTime').value;
            document.getElementById('txtSplitLeg1EndTime1').value = document.getElementById('MainContent_txtSplitLeg1EndTime').value;
        }

       
    </script>
    <script type="text/javascript">
        function Manual() {
            document.getElementById('txtStartTime1').value = document.getElementById('MainContent_txtStartTime').value;
            document.getElementById('txtEndTime1').value = document.getElementById('MainContent_txtEndTime').value;
        }
    </script>
    <style type="text/css">
        #MainContent_gvStates tbody tr td, #MainContent_gvCountry tbody tr td
        {
            padding-top: 0.1rem;
            padding-bottom: 0.1rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row" id="tblGrid" runat="server">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row form-group">
                        <div class="col-lg-3 text-right">
                        </div>
                        <div class="col-lg-6 text-center">
                            <asp:RadioButtonList ID="rblStatus" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                Width="80%" AutoPostBack="true" OnSelectedIndexChanged="ddlstatus_onselectedChanged">
                                <asp:ListItem Text="&nbsp;Add a Manual Trip&nbsp;&nbsp;" Value="Active" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="&nbsp;Modify a  FOS Trip&nbsp;&nbsp;" Value="Booked Trips"></asp:ListItem>
                                <asp:ListItem Text="&nbsp;Add a Manual Leg" Value="Group"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="col-lg-3 text-right" style="padding-right: 2%;">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" OnClick="btnAddOnClick" />
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 boxshadow table-responsive">
                            <asp:GridView ID="gvTailDetails" runat="server" PagerStyle-CssClass="pager" OnSelectedIndexChanged="gvTailDetails_SelectedIndexChanged"
                                AutoGenerateColumns="false" PageSize="100" EmptyDataText="No tail details found"
                                CssClass="table" HeaderStyle-CssClass="thead-dark" AllowPaging="True" Width="100%"
                                OnPageIndexChanging="gvTailDetails_PageIndexChanging" OnRowDataBound="gvTailDetails_OnRowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tail" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                        ItemStyle-Width="5%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTailNo" Text='<%# Eval("TailNo") %>' runat="server"></asp:Label>
                                            <asp:Label ID="lblGroupId" Text='<%# Eval("GroupId") %>' Visible="false" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trip" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                        ItemStyle-Width="5%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltrip" Text='<%# Eval("tripno") %>' Visible="true" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TailName" HeaderText="Tail Name" ItemStyle-HorizontalAlign="Left"
                                        Visible="false" />
                                    <asp:TemplateField HeaderText="Available From" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldate" Text='<%# Eval("AvailbleFrom") %>' Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="Label1" Text='<%# Eval("FromDate","{0:MMMM d, yyyy}") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Available To" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" Text='<%# Eval("Todate","{0:MMMM d, yyyy}") %>' runat="server"></asp:Label>
                                            <asp:Label ID="lbldate1" Text='<%# Eval("AvilbleTo") %>' Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lblleg" runat="server" Text='<%# Eval("leg") %>' Visible="false"> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Airport" ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-HorizontalAlign="center" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstartair" Text='<%# Eval("StartAirport") %>' runat="server"></asp:Label>
                                            <asp:Label ID="lblStartCity" Text='<%# Eval("StartCity") %>' runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Airport" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblendair" Text='<%# Eval("EndAirport") %>' runat="server"></asp:Label>
                                            <asp:Label ID="lblEndCity" Text='<%# Eval("EndCity") %>' runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Time" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstarttime" Text='<%# Eval("StartTime") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Time" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="7%" ItemStyle-Width="7%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndTime" Text='<%# Eval("EndTime") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Flying Hours" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFlyHours" Text='<%# Eval("Flyinghours") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Source" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="7%" ItemStyle-Width="7%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                        Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" Text='<%# Eval("Type") %>' runat="server"></asp:Label>
                                            <asp:Label ID="lblPrice" Text='<%# Eval("CurrentPrice") %>' runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedby" Text='<%# Eval("created") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated On" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedon" Text='<%# Eval("createdon") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <HeaderStyle Width="3%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbView1" runat="server" ImageUrl="~/Images/pencil_Edit.png"
                                                CommandName="Select" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                                CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="tblForm" runat="server">
        <div class="col-xl-12">
            <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                <div class="form-group row">
                    <div class="col-lg-12 text-right" style="padding-right: 2%;">
                        <label class="importantlabels">
                            <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                        </label>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" OnClick="btnSaveOnClick"
                            TabIndex="14" />
                        <asp:Label ID="lblSubScren" runat="server" Text="Add a Manual Trip" Style="color: brown;
                            display: none;"></asp:Label>
                        <asp:Button ID="btnView" runat="server" Text="Back" OnClick="btnViewOnClick" CssClass="btn btn-danger"
                            TabIndex="15" />
                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="S" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                    </div>
                </div>
                <div class="kt-portlet__body p-4">
                    <div class="row" id="tbldata" runat="server" visible="false">
                        <div class="col-xl-1">
                        </div>
                        <div class="col-xl-10">
                            <div class="form-group row" style="margin-bottom: 0rem;">
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    Tail Number
                                </label>
                                <label class="col-1 col-form-label">
                                    <asp:Label ID="lblTailData" runat="server" Style="font-weight: 500;"></asp:Label>
                                </label>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    Start Airport
                                </label>
                                <label class="col-3 col-form-label">
                                    <asp:Label ID="lblStartData" runat="server" Style="font-weight: 500;"></asp:Label>,
                                    <asp:Label ID="lblscitydata" runat="server" Style="font-weight: 500;"></asp:Label>
                                </label>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    Start Time
                                </label>
                                <label class="col-1 col-form-label">
                                    <asp:Label ID="lblStimedata" runat="server" Style="font-weight: 500;"></asp:Label>
                                </label>
                                <label class="col-2 col-form-label" style="max-width: 6%;">
                                    Date
                                </label>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    <asp:Label ID="lblDateData" runat="server" Style="font-weight: 500;"></asp:Label>
                                </label>
                            </div>
                            <div class="form-group row" style="margin-bottom: 0rem;">
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    Trip Number
                                </label>
                                <label class="col-1 col-form-label">
                                    <asp:Label ID="lblTripData" runat="server" Style="font-weight: 500;"></asp:Label>
                                </label>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    End Airport
                                </label>
                                <label class="col-3 col-form-label">
                                    <asp:Label ID="lblendData" runat="server" Style="font-weight: 500;"></asp:Label>
                                    ,<asp:Label ID="lblEcityData" runat="server" Style="font-weight: 500;"></asp:Label>
                                </label>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    End Time
                                </label>
                                <label class="col-1 col-form-label">
                                    <asp:Label ID="lblEtimeData" runat="server" Style="font-weight: 500;"></asp:Label>
                                </label>
                            </div>
                        </div>
                        <div class="col-xl-1">
                        </div>
                    </div>
                    <div class="row" id="trSplit" runat="server">
                        <div class="col-xl-1">
                        </div>
                        <div class="col-xl-10">
                            <div class="form-group row">
                                <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                    padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; width: 40%;
                                    border-bottom: 1px solid #C0C0C0 !important;">
                                    Enter Trip And Date Details
                                </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label" style="max-width: 29%;">
                                    Do you want to breakdown by ?
                                </label>
                                <div class="col-4" style="padding-top: 10px;">
                                    <asp:RadioButtonList ID="rblBreak" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                        Width="55%" onclick="javascript:getCheckedRadio();">
                                        <asp:ListItem Text="City" Value="CT" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Country" Value="CO"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Available From <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-2">
                                    <asp:TextBox class="cusinputuser" runat="server" ID="txtSplitAvaFrom" Placeholder="Date(MM-DD-YYYY)"
                                        onkeypress="return NumericsIphon(event)" TabIndex="4" MaxLength="20" Width="100%"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtSplitAvaFrom"
                                        Format="MM-dd-yyyy" OnClientDateSelectionChanged="CheckDate" Enabled="True">
                                    </asp:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtSplitAvaFrom"
                                        Display="None" ErrorMessage="Available From is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-2 col-form-label">
                                    Available To <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-2">
                                    <asp:TextBox class="cusinputuser" runat="server" ID="txtSplitAvaTo" Placeholder="Date(MM-DD-YYYY)"
                                        onkeypress="return NumericsIphon(event)" TabIndex="5" MaxLength="20" Width="100%"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender4" OnClientDateSelectionChanged="CheckDate"
                                        runat="server" TargetControlID="txtSplitAvaTo" Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtSplitAvaTo"
                                        Display="None" ErrorMessage="Available To is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 23%;">
                                    Show in Landing Page
                                    <asp:CheckBox ID="cbSplitEmpty" TabIndex="7" Checked="true" runat="server" />
                                </label>
                                <div class="col-1" style="padding-top: 17px;">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                    padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; width: 40%;
                                    border-bottom: 1px solid #C0C0C0 !important;">
                                    Enter Departure And Arrival City ( Please enter local time only )
                                </label>
                            </div>
                            <div class="form-group row" id="trLeg1" runat="server" style="display: none;">
                                <label class="col-6 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                    padding-bottom: 15px; width: 40%;">
                                    Airport Details
                                </label>
                                <label class="col-6 col-form-label">
                                    <span id="MainContent_lblmandatory" class="cuslabeluser" style="float: right; font-style: italic;
                                        font-size: 13px;">Note : you can add maximum 3 leg</span>
                                </label>
                            </div>
                            <div class="form-group row" id="divstartcounty" runat="server" style="display: block;">
                                <label class="col-2 col-form-label">
                                    Start Country <span style="color: Red">*</span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="txtcountry" runat="server" MaxLength="50" class="cusinputuser" TabIndex="8"></asp:TextBox>
                                    <asp:PopupControlExtender ID="popupcountry" runat="server" BehaviorID="popupcountry"
                                        Enabled="True" TargetControlID="txtcountry" PopupControlID="pnlcountry" Position="Bottom">
                                    </asp:PopupControlExtender>
                                    <asp:Panel ID="pnlcountry" runat="server" Style="background-color: whitesmoke; border-color: rgb(204, 204, 204);
                                        border-width: 1px; border-style: solid; height: 130px; width: 100%; overflow: auto;
                                        visibility: visible; z-index: 1000; display: none">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:CheckBoxList ID="cblcountry" runat="server" DataTextField="Countryname" DataValueField="countryname"
                                                        TabIndex="6" onchange="GetSelectedClient('MainContent_cblcountry','MainContent_txtcountry')"
                                                        onclick="javascript:CheckCheck();">
                                                    </asp:CheckBoxList>
                                                </td>
                                                <td align="right" valign="top">
                                                    <asp:Image ID="Image3" runat="server" onclick="HidePopupClose();" ToolTip="Close"
                                                        ImageUrl="~/Components/images/delete.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <label class="col-2 col-form-label">
                                    Start Time <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <input style="display: none;" runat="server" id="txtCouStartTime" maxlength="20"
                                        type="text" />
                                    <input class="form-control" id="txtCouStartTime1" tabindex="10" onblur="txtCounStartTimefun(this);"
                                        type="time" style="width: 46%;" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCouStartTime"
                                        Display="None" ErrorMessage="Start Time is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row" id="divstartsplitLeft" runat="server" style="display: none;">
                                <label class="col-2 col-form-label">
                                    Start City <span style="color: Red">*</span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="ddlStartCitySplit" runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                        CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                        CompletionSetCount="1" TargetControlID="ddlStartCitySplit" ID="AutoCompleteExtender3"
                                        runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlStartCitySplit"
                                        Display="None" ErrorMessage=" Start City is required." SetFocusOnError="True"
                                        ValidationGroup="LEG">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-2 col-form-label">
                                    Start Time <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-2">
                                    <asp:TextBox ID="txtSplitLeg1StartTime" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                    <input class="form-control" id="txtSplitLeg1StartTime1" tabindex="10" onblur="txtCityStartTimefun(this);"
                                        type="time" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtSplitLeg1StartTime"
                                        Display="None" ErrorMessage=" Start Time is required." SetFocusOnError="True"
                                        ValidationGroup="LEG">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    End City <span style="color: Red">*</span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="ddlLeg1EndCity" runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                        CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                        CompletionSetCount="1" TargetControlID="ddlLeg1EndCity" ID="AutoCompleteExtender4"
                                        runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="ddlLeg1EndCity"
                                        Display="None" ErrorMessage=" End City is required." SetFocusOnError="True" ValidationGroup="LEG">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-2 col-form-label">
                                    End Time <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-2">
                                    <asp:TextBox ID="txtSplitLeg1EndTime" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                    <input class="form-control" id="txtSplitLeg1EndTime1" tabindex="10" onblur="txtLeg1EndTimefun(this);"
                                        type="time" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtSplitLeg1EndTime"
                                        Display="None" ErrorMessage=" Start Time is required." SetFocusOnError="True"
                                        ValidationGroup="LEG">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Flying Hours <span style="color: Red">*</span>
                                </label>
                                <div class="col-4">
                                    <input class="cusinputuser" id="txtSpliFlyHour" runat="server" tabindex="18" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSpliFlyHour"
                                        Display="None" ErrorMessage="Flying Hours is required." SetFocusOnError="True"
                                        ValidationGroup="LEG">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div id="trLeg2" runat="server" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-12 text-center">
                                        <asp:Button ID="btnAddLeg" runat="server" Text="Add Leg" CssClass="btn btn-danger"
                                            OnClick="btnAddLegOnClick" TabIndex="14" ValidationGroup="LEG" />
                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="LEG" ShowMessageBox="true"
                                            runat="server" ShowSummary="false" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <asp:GridView ID="gvLegDetails" runat="server" PagerStyle-CssClass="pager" AutoGenerateColumns="false"
                                            EmptyDataText="No Leg details found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                            AllowPaging="false" Width="100%" OnRowDeleting="gvLegDetails_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tail" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                                    ItemStyle-Width="5%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTailNo" Text='<%# Eval("TailNo") %>' runat="server"></asp:Label>
                                                        <asp:Label ID="lblTripFlag" Text='<%# Eval("TripFlag") %>' runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trip" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                                    ItemStyle-Width="5%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltrip" Text='<%# Eval("tripno") %>' Visible="true" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Airport" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-HorizontalAlign="center" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                                    HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStartCity" Text='<%# Eval("startCity") %>' runat="server"></asp:Label>&nbsp;-
                                                        &nbsp;
                                                        <asp:Label ID="lblstartair" Text='<%# Eval("start") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Airport" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEndCity" Text='<%# Eval("EndCity") %>' runat="server"></asp:Label>&nbsp;-
                                                        &nbsp;<asp:Label ID="lblendair" Text='<%# Eval("End") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Time" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstarttime" Text='<%# Eval("DTime") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Time" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="7%" ItemStyle-Width="7%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEndTime" Text='<%# Eval("ATime") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Flying Hours" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFlyHours" Text='<%# Eval("FlyinghoursFOS") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                                            CommandName="Delete" CausesValidation="false" AlternateText="Delete Tail details" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1">
                        </div>
                    </div>
                    <div class="row" id="trManualandModiy" runat="server" visible="false">
                        <div class="col-xl-1">
                        </div>
                        <div class="col-xl-10">
                            <div class="form-group row">
                                <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                    padding-bottom: 10px; padding-top: 15px; width: 40%; border-bottom: 1px solid #C0C0C0 !important;">
                                    Enter Trip And Date Details ( Please enter local time only )
                                </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 15%;">
                                    Tail Number <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3" style="max-width: 20%;">
                                    <input class="cusinputuser" runat="server" id="txtTailNo" maxlength="20" type="text"
                                        tabindex="1" />
                                    <asp:RequiredFieldValidator ID="rfvTailNo" runat="server" ControlToValidate="txtTailNo"
                                        Display="None" ErrorMessage="Tail is required." SetFocusOnError="True" ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 16%;">
                                    Available From <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox class="cusinputuser" runat="server" ID="txtAvaFrom" Placeholder="Date(MM-DD-YYYY)"
                                        onkeypress="return NumericsIphon(event)" TabIndex="4" MaxLength="20" Width="100%"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtAvaFrom"
                                        Format="MM-dd-yyyy" OnClientDateSelectionChanged="CheckDate" Enabled="True">
                                    </asp:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAvaFrom"
                                        Display="None" ErrorMessage="Available From is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                                <div class="col-2">
                                    <input class="form-control" runat="server" id="txtStartTime" tabindex="10" maxlength="20"
                                        style="display: none;" />
                                    <input class="form-control" id="txtStartTime1" tabindex="10" maxlength="20" type="time"
                                        onblur="txtManualStartTimefun(this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtStartTime"
                                        Display="None" ErrorMessage=" Start Time is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 15%;">
                                    Trip Number <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3" style="max-width: 20%;">
                                    <input class="cusinputuser" runat="server" id="txttrip" onkeypress="return Numerics(event)"
                                        maxlength="20" type="text" tabindex="2" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txttrip"
                                        Display="None" ErrorMessage="Trip number is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 16%;">
                                    Available To <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-3">
                                    <asp:TextBox class="cusinputuser" runat="server" ID="txtAvaTo" Placeholder="Date(MM-DD-YYYY)"
                                        onkeypress="return NumericsIphon(event)" TabIndex="5" MaxLength="20" Width="100%"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" OnClientDateSelectionChanged="CheckDate"
                                        runat="server" TargetControlID="txtAvaTo" Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAvaTo"
                                        Display="None" ErrorMessage="Available To is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                                <div class="col-2">
                                    <input class="form-control" runat="server" id="txtEndTime" maxlength="20" tabindex="13"
                                        style="display: none;" />
                                    <input class="form-control" id="txtEndTime1" maxlength="20" tabindex="13" type="time"
                                        onblur="txtManualEndTimefun(this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtEndTime"
                                        Display="None" ErrorMessage=" End Time is required." SetFocusOnError="True" ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 15%;">
                                    Flying Hours <span style="color: Red">*</span>
                                </label>
                                <div class="col-3" style="max-width: 20%;">
                                    <input class="cusinputuser" id="txtFlyHours1" runat="server" tabindex="18" style="width: 100%;" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFlyHours1"
                                        Display="None" ErrorMessage="Flying Hours is required." SetFocusOnError="True"
                                        ValidationGroup="S">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 16%;" id="divPricModLeft" runat="server"
                                    visible="true">
                                    Price<span style="color: Red">*</span>
                                </label>
                                <div class="col-3" style="max-width: 22%;">
                                    <div id="divPriceModRight" runat="server" visible="true">
                                        <input class="cusinputuser" runat="server" id="txtNoSeats" maxlength="2" tabindex="19"
                                            width="100%" style="width: 100%; display: none" onkeypress="return Numerics(event)"
                                            type="text" visible="false" />
                                        <input class="cusinputuser" runat="server" id="txtMPrice" maxlength="7" tabindex="19"
                                            style="width: 100%;" onkeypress="return Numerics(event)" type="text" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtNoSeats"
                                            Display="None" ErrorMessage="No of Seats is required." SetFocusOnError="True"
                                            ValidationGroup="S">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <label class="col-3 col-form-label" style="text-align: left; font-size: 12px; color: Black;
                                    font-weight: 600" visible="true">
                                    ( Enter Price in whole number )
                                </label>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Show in Landing Page
                                    <asp:CheckBox ID="chkEmpty" TabIndex="7" Checked="true" runat="server" Visible="false" />
                                </label>
                                <div class="col-2" style="padding-top: 9px;">
                                    <asp:RadioButtonList ID="rblLanding" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="NY"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div id="divManualAirport" runat="server">
                                <div class="form-group row">
                                    <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                        padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; width: 40%;
                                        border-bottom: 1px solid #C0C0C0 !important;">
                                        Enter Departure And Arrival City
                                    </label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" id="divstartcityLeft" visible="true" runat="server"
                                        style="max-width: 15%;">
                                        Start City <span style="color: Red">*</span>
                                    </label>
                                    <div class="col-5">
                                        <div class="cusfielduser-cuscontaineruser" id="divstartcity" runat="server">
                                            <asp:TextBox ID="txtStartCity" runat="server" MaxLength="50" class="cusinputuser"
                                                TabIndex="8"></asp:TextBox>
                                            <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                                CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                                CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                                CompletionSetCount="1" TargetControlID="txtStartCity" ID="AutoCompleteExtender1"
                                                runat="server" FirstRowSelected="false">
                                            </asp:AutoCompleteExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStartCity"
                                                Display="None" ErrorMessage="Start City is required." SetFocusOnError="True"
                                                ValidationGroup="S">*</asp:RequiredFieldValidator>
                                        </div>
                                        <div id="divstartsplit" visible="false" runat="server">
                                            <input class="cusinputuser" runat="server" id="txtstartsplit" maxlength="100" type="text"
                                                tabindex="8" />
                                        </div>
                                    </div>
                                    <div class="col-1" style="padding-top: 10px; padding-left: 0px; max-width: 4%">
                                        <asp:LinkButton ID="lnkSearch" runat="server" Visible="false" OnClick="lnkSearch_Click"
                                            CommandName="Start">
                                    <i class="la la-search" style="color:Blue; font-weight:bold; font-size:25px;"> </i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" style="max-width: 15%;">
                                        End City <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-5">
                                        <div id="divendcity" runat="server">
                                            <asp:TextBox ID="txtEndCity" runat="server" MaxLength="50" class="cusinputuser" TabIndex="9"></asp:TextBox>
                                            <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                                CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                                CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                                CompletionSetCount="1" TargetControlID="txtEndCity" ID="AutoCompleteExtender2"
                                                runat="server" FirstRowSelected="false">
                                            </asp:AutoCompleteExtender>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtEndCity"
                                                Display="None" ErrorMessage="End City is required." SetFocusOnError="True" ValidationGroup="S">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-1" style="padding-top: 10px; padding-left: 0px; max-width: 4%">
                                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="lnkSearch_Click" Visible="false"
                                            CommandName="End">
                                    <i class="la la-search" style="color:Blue; font-weight:bold; font-size:25px;"> </i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div id="divModify" runat="server">
                                <div class="form-group row">
                                    <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                        padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #C0C0C0 !important;">
                                        Enter Departure Airport
                                    </label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" style="max-width: 15%;">
                                        Choose Airport <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-2">
                                        <asp:DropDownList ID="ddlDepSetupBy" runat="server" onclick="javascript:GetDepAirport();">
                                            <asp:ListItem Text="By Airport" Value="CT" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="By State" Value="ST"></asp:ListItem>
                                            <asp:ListItem Text="By Country" Value="CO"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-5">
                                        <asp:TextBox ID="txtModDepCity" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" Style="display: '';" autocomplete="off"></asp:TextBox>
                                        <asp:TextBox ID="txtModDepState" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" Style="display: none;" autocomplete="off"></asp:TextBox>
                                        <asp:TextBox ID="txtModDepCountry" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" Style="display: none;" autocomplete="off"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                            CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                            CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                            CompletionSetCount="1" TargetControlID="txtModDepCity" ID="AutoCompleteExtender6"
                                            runat="server" FirstRowSelected="true">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ServiceMethod="DepState" MinimumPrefixLength="1" CompletionListCssClass="completionList"
                                            CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                            CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtModDepState"
                                            ID="AutoCompleteExtender8" runat="server" FirstRowSelected="true">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ServiceMethod="DepCountry" MinimumPrefixLength="1" CompletionListCssClass="completionList"
                                            CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                            CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtModDepCountry"
                                            ID="AutoCompleteExtender9" runat="server" FirstRowSelected="true">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                    <div class="col-2" style="padding-top: 7px;">
                                        <asp:Button ID="btnAddDeparture" runat="server" Text="Add to List" CssClass="btn btn-danger"
                                            CommandName="Start" OnClick="btnRouteAddDep_Click" TabIndex="14" Style="float: right;
                                            padding-top: 4px; padding-bottom: 4px" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-11">
                                        <asp:GridView ID="gvDepAirport" runat="server" PagerStyle-CssClass="pager" AutoGenerateColumns="false"
                                            EmptyDataText="Departure airport details not found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                            AllowPaging="false" Width="100%" OnRowDeleting="gvDepAirport_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Setup By" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                                    ItemStyle-Width="5%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSetupBy" Text='<%# Eval("SetupBy") %>' Visible="true" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airport" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStartCity" Text='<%# Eval("Airport") %>' runat="server"></asp:Label>
                                                        <asp:Label ID="lblStateCode" Text='<%# Eval("StateCode") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCountryCode" Text='<%# Eval("CountryCode") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblAirportCode" Text='<%# Eval("AirportCode") %>' runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CreatedBy" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedBy" Text='<%# Eval("CreatedBy") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedOn" Text='<%# Eval("CreatedOn") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                                            CommandName="Delete" CausesValidation="false" AlternateText="Delete Tail details" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="form-group row" id="divDepLan" runat="server" visible="false">
                                    <label class="col-4 col-form-label">
                                        How do you want to display in landing page <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-7">
                                        <asp:TextBox ID="txtDepDisLandPage" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                        padding-top: 5px !important; padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #C0C0C0 !important;">
                                        Enter Arrival Airport
                                    </label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" style="max-width: 15%;">
                                        Choose Airport <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-2">
                                        <asp:DropDownList ID="ddlArrSetupBy" runat="server" onclick="javascript:GetArrivalAirport();">
                                            <asp:ListItem Text="By Airport" Value="CT" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="By State" Value="ST"></asp:ListItem>
                                            <asp:ListItem Text="By Country" Value="CO"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-5">
                                        <asp:TextBox ID="txtArrAirport" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" autocomplete="off" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:TextBox ID="txtArrState" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" Style="display: none;" autocomplete="off"></asp:TextBox>
                                        <asp:TextBox ID="txtArrCountry" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" Style="display: none;" autocomplete="off"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                            CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                            CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                            CompletionSetCount="1" TargetControlID="txtArrAirport" ID="AutoCompleteExtender7"
                                            runat="server" FirstRowSelected="true">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ServiceMethod="DepState" MinimumPrefixLength="1" CompletionListCssClass="completionList"
                                            CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                            CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtArrState"
                                            ID="AutoCompleteExtender10" runat="server" FirstRowSelected="true">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ServiceMethod="DepCountry" MinimumPrefixLength="1" CompletionListCssClass="completionList"
                                            CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                            CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtArrCountry"
                                            ID="AutoCompleteExtender11" runat="server" FirstRowSelected="true">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                    <div class="col-2" style="padding-top: 7px;">
                                        <asp:Button ID="btnAddArrival" runat="server" Text="Add to List" CssClass="btn btn-danger"
                                            OnClick="btnRouteAddArr_Click" TabIndex="14" Style="float: right; padding-top: 4px;
                                            padding-bottom: 4px" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-11">
                                        <asp:GridView ID="gvArrivalAirport" runat="server" PagerStyle-CssClass="pager" AutoGenerateColumns="false"
                                            EmptyDataText="Arrival airport details not found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                            AllowPaging="false" Width="100%" OnRowDeleting="gvArrivalAirport_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Setup By" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                                    ItemStyle-Width="5%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSetupBy" Text='<%# Eval("SetupBy") %>' Visible="true" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airport" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStartCity" Text='<%# Eval("Airport") %>' runat="server"></asp:Label>
                                                        <asp:Label ID="lblStateCode" Text='<%# Eval("StateCode") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCountryCode" Text='<%# Eval("CountryCode") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblAirportCode" Text='<%# Eval("AirportCode") %>' runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CreatedBy" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedBy" Text='<%# Eval("CreatedBy") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                                    HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedOn" Text='<%# Eval("CreatedOn") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                                            CommandName="Delete" CausesValidation="false" AlternateText="Delete Tail details" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="form-group row" id="divArrLan" runat="server" visible="false">
                                    <label class="col-4 col-form-label">
                                        How do you want to display in landing page <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-7">
                                        <asp:TextBox ID="txtArrDisLandPage" runat="server" MaxLength="50" class="cusinputuser"
                                            TabIndex="8" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="lblalert" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnconfirmsplit" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpesplit" runat="server" TargetControlID="btnconfirmsplit"
        PopupControlID="pnlsplit" BackgroundCssClass="modalBackground" Y="80" CancelControlID="lnkcancel">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlsplit" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <asp:Label ID="Label6" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btnconfirmdelsplit" runat="server" CssClass="btn btn-danger"
                        OnClick="btnconfirmdelsplitdelete_click" Style="color: rgb(255,255,255); text-decoration: none"
                        CausesValidation="false" Text="Ok" />
                    <asp:LinkButton ID="lnkcancel" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnRoute" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeRoute" runat="server" PopupControlID="pnlRoute" TargetControlID="btnRoute"
        Y="10" CancelControlID="btnRouteClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlRoute" runat="server" role="document" ScrollBars="Auto" Style="display: none;
        width: 40% !important; height: 90%; height: 80%; background-color: rgb(17,24,32);
        color: white !important; padding: 10px;">
        <div class="form-group row" style="margin-bottom: 0rem;">
            <label class="col-lg-12 col-form-label" style="color: White;">
                <asp:Label ID="lblGroupHeader1" runat="server" Text="Choose Airport"></asp:Label>
                <asp:Button ID="btnRouteClose" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="float: right; padding-top: 3px; padding-bottom: 3px" />
            </label>
        </div>
        <div class="row" style="font-size: 14px; font-weight: 400;">
            <label class="col-lg-2 col-form-label" style="color: White !important;">
                Setup By
            </label>
            <div class="col-lg-10 col-form-label">
                <asp:RadioButtonList ID="rblDepartureSetup" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                    AutoPostBack="true" OnSelectedIndexChanged="rblDepartureSetup_SelectedIndexChanged"
                    Width="50%" TabIndex="11" Style="color: White;">
                    <asp:ListItem Text="Airport" Value="AI" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Country" Value="CO"></asp:ListItem>
                    <asp:ListItem Text="State" Value="ST"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="row" id="dvDepartureAirport" runat="server">
            <label class="col-3 col-form-label" style="color: White; padding-top: 16px">
                Choose Airport <span class="MandatoryField">* </span>
            </label>
            <div class="col-9 col-form-label">
                <div class="kt-input-icon kt-input-icon--left">
                    <asp:TextBox ID="txtFromICAO" runat="server" CssClass="form-control m-select2" autocomplete="off"
                        Width="100%" TabIndex="12" placeholder="Search Airport..." />
                </div>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="server" CompletionInterval="100"
                    CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                    CompletionListItemCssClass="listItem" CompletionSetCount="10" EnableCaching="false"
                    DelimiterCharacters="," ShowOnlyCurrentWordInCompletionListItem="true" FirstRowSelected="true"
                    MinimumPrefixLength="1" ServiceMethod="GetCompletionList" TargetControlID="txtFromICAO">
                </asp:AutoCompleteExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtFromICAO"
                    Display="None" ErrorMessage="Start Airport is required." SetFocusOnError="True"
                    ValidationGroup="Route">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group row" id="dvCountry" runat="server" visible="false">
            <label class="col-lg-2 col-form-label" style="color: White;">
                Select Country <span class="MandatoryField">* </span>
            </label>
            <div class="col-lg-4 col-form-label">
                <asp:DropDownList ID="ddlCountry1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry1_SelectedIndexChanged"
                    CssClass="form-control" TabIndex="13">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group row" id="dvDepartureState" runat="server" visible="false">
            <label class="col-lg-2 col-form-label" style="color: White; padding-top: 16px">
                Country <span class="MandatoryField">* </span>
            </label>
            <div class="col-lg-4 col-form-label">
                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"
                    CssClass="form-control" TabIndex="14">
                </asp:DropDownList>
            </div>
            <div class="col-lg-6">
                <asp:Button ID="btnRounte1" runat="server" Text="Submit" CssClass="btn btn-danger"
                    ValidationGroup="Route" CausesValidation="true" TabIndex="2" />
            </div>
            <label class="col-lg-2 col-form-label text-center" style="display: none">
                State <span class="MandatoryField">* </span>
            </label>
            <div class="col-lg-4 col-form-label" style="display: none">
                <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" CssClass="form-control"
                    OnSelectedIndexChanged="ddlState_SelectedIndexChanged" TabIndex="15">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group row" id="dvDepartureState1" runat="server" visible="false">
            <label class="col-lg-3 col-form-label" style="color: White;">
                Choose Airport(s)
            </label>
            <div class="col-lg-9 col-form-label">
                <asp:RadioButtonList ID="rblAirportMap" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                    AutoPostBack="true" OnSelectedIndexChanged="rblAirportMap_SelectedIndexChanged"
                    Width="60%" TabIndex="16" Style="color: White;">
                    <asp:ListItem Text="All Airport(s)" Value="ALL" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Selected Airport(s)" Value="DAT"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form-group row" id="dvDepartureState2" runat="server" visible="false">
            <div class="col-lg-12">
                <asp:GridView ID="gvAirport" runat="server" AutoGenerateColumns="False" Width="100%"
                    Style="margin-top: 1rem;" CssClass="table" HeaderStyle-CssClass="thead-dark"
                    EmptyDataText="No Data Found">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex+1 %>' Style="color: White;"></asp:Label>
                                <asp:Label ID="lblAirportId" runat="server" Text='<%# Bind("AirportId") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblCountry" runat="server" Text='<%# Bind("Country_Name") %>' Style="color: White;"></asp:Label>
                                <asp:Label ID="lblCodeX2" Visible="false" runat="server" Text='<%# Bind("CodeX2") %>' />
                                <asp:Label ID="lblCountry_Code" Visible="false" runat="server" Text='<%# Bind("Country_Code") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblStateName" runat="server" Text='<%# Bind("State_Name") %>' Style="color: White;" />
                                <asp:Label ID="lblState_Code" Visible="false" runat="server" Text='<%# Bind("State_Code") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Airport" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblICAO" runat="server" Text='<%# Bind("ICAO") %>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this,4);" />
                                <asp:Label ID="lnkSelect" runat="server" Text="Select All"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkChoose" runat="server" Height="18px" ImageUrl="~/Images/icon-edit-new.png"
                                    CausesValidation="false" CommandName="select" AlternateText="Edit" ToolTip="Click here to edit." />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="form-group row" id="divCountry" runat="server" visible="false">
            <div class="col-lg-12">
                <asp:GridView ID="gvCountry" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="table" HeaderStyle-CssClass="thead-dark" EmptyDataText="No Data Found">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex+1 %>' Style="color: White;"></asp:Label>
                                <asp:Label ID="lblRow_id" runat="server" Text='<%# Bind("Row_id") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="22%"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblCountryCode" runat="server" Text='<%# Bind("Country_Code") %>'
                                    Style="color: White;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="center" />
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Country Name" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCountryName" runat="server" Text='<%# Bind("Country_Name") %>'
                                    Style="color: White;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-HorizontalAlign="Center" HeaderText="Select">
                            <ItemTemplate>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand" style="padding-top: 17px !important;">
                                    <input type="checkbox" id="chkChoose" runat="server" /><span></span>
                                </label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        <div class="form-group row" id="dvStates" runat="server" visible="false">
            <div class="col-lg-12">
                <asp:GridView ID="gvStates" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="table" HeaderStyle-CssClass="thead-dark" EmptyDataText="No Data Found">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex+1 %>' Style="color: White;"></asp:Label>
                                <asp:Label ID="lblRow_id" runat="server" Text='<%# Bind("Row_id") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="22%"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblState_Code" runat="server" Text='<%# Bind("State_Code") %>' Style="color: White;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="center" />
                            <ItemStyle HorizontalAlign="center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State Name" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblStateName" runat="server" Text='<%# Bind("State_Name") %>' Style="color: White;" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-HorizontalAlign="Center" HeaderText="Select">
                            <ItemTemplate>
                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand" style="padding-top: 17px !important;">
                                    <input type="checkbox" id="chkChoose" runat="server" /><span></span>
                                </label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
            </div>
            <div class="col-lg-4 text-right">
                <asp:Button ID="btnRouteAdd" runat="server" Text="Submit" CssClass="btn btn-danger"
                    ValidationGroup="Route" CausesValidation="true" TabIndex="2" />
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="Route"
                    ShowMessageBox="true" ShowSummary="false" />
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btntrip" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpetrip" runat="server" TargetControlID="btntrip" PopupControlID="pnltrip"
        BackgroundCssClass="modalBackground" Y="50">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnltrip" runat="server" Height="300px" Width="70%" ScrollBars="Auto"
        Style="display: none; background-color: rgb(17,24,32); color: white">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px;
            padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="height: 10px">
                </td>
            </tr>
            <tr>
                <td style="text-align: left; padding-left: 2%; font-size: 15px; font-weight: bold"
                    width="60%" nowrap="nowrap">
                    Trip Number already exists.
                </td>
            </tr>
            <tr>
                <td style="padding-left: 2%; padding-right: 2%">
                    <asp:GridView ID="gv_Child_Tail" runat="server" AutoGenerateColumns="false" Style="width: 100%"
                        EmptyDataText="No trips found." ShowFooter="true" UseAccessibleHeader="true"
                        CssClass="table" HeaderStyle-CssClass="thead-dark">
                        <Columns>
                            <asp:TemplateField HeaderText="Tail Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-Width="8%" HeaderStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TailNo") %>'
                                        Style="color: White;"></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trip Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbltripno" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "tripno") %>' Style="color: White;"></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Airport" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="center" ItemStyle-Width="7%" HeaderStyle-Width="7%"
                                ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartAirport" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Startairport") %>' Style="color: White;"></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Airport" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                ItemStyle-Width="7%" HeaderStyle-Width="7%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblendAirport" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "endairport") %>' Style="color: White;"></asp:Label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("AvailbleFrom", "{0:MMMM dd, yyyy}") %>' Style="color: White;"></asp:Label>
                                    <asp:Label ID="lbldateD" Visible="false" runat="server" Text='<%# Eval("AvailbleFrom") %>' Style="color: White;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Time" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblstarttime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartTime") %>' Style="color: White;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Time" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblendtime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Endtime") %>' Style="color: White;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding-bottom: 10px; padding-top: 10px;">
                    <asp:LinkButton ID="btnsubmit" runat="server" CssClass="btn btn-danger" Text="Close"
                        OnClick="Buttonyes_Click" ValidationGroup="VP" UseSubmitBehavior="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdnTailNo" runat="server" />
    <asp:HiddenField ID="hdnDDate" runat="server" />
    <asp:HiddenField ID="hdnstarttime" runat="server" />
    <asp:HiddenField ID="hdnendtime" runat="server" />
    <asp:HiddenField ID="hdnLeg2EndTime" runat="server" />
    <asp:HiddenField ID="hdnLeg2StartTime" runat="server" />
    <asp:HiddenField ID="hdnstartcity" runat="server" />
    <asp:HiddenField ID="hdnendcity" runat="server" />
    <asp:HiddenField ID="hdnstartAirport" runat="server" />
    <asp:HiddenField ID="hdnendairport" runat="server" />
    <asp:Label ID="lbllegdisplay" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblManEditGroupId" runat="server" Visible="false" Text="0"></asp:Label>
    <asp:Label ID="lblAirportFor" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblbaseflag" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="LBLTRIPNO" Visible="false" runat="server"></asp:Label>
    <asp:Label ID="lbloldtail" Visible="false" runat="server"></asp:Label>
    <asp:Label ID="lblgroupiddelete" Visible="false" runat="server"></asp:Label>
    <asp:Label ID="lblstartdate" runat="server" Style="display: none"></asp:Label>
    <asp:Label ID="lblenddate" runat="server" Style="display: none"></asp:Label>
    <asp:Label ID="lbltailconfirm" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbltripconfirm" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblDateconfirm" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblstarttimeconfirm" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblstartairconfirm" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblendairconfirm" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hdfSCID" runat="server" />
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:Label ID="lblgroupiddisplay" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbltripflagupdate" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblgroup" runat="server" Style='display: none'></asp:Label>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
