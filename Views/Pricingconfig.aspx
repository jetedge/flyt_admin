﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="Pricingconfig.aspx.cs" Inherits="Views_Pricingconfig" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link type="text/css" rel="Stylesheet" href="assets/css/Slider.css" />
    <script type="text/javascript">
        function checkseat() {
            var txtAvaFrom1 = document.getElementById('MainContent_txtNoofSeats');
            var seatval = txtAvaFrom1.value;
            if (seatval > 0) {

            } else {
                document.getElementById('MainContent_lblalert').innerHTML = 'Number of seats should not be 0';
                $find("MainContent_mpealert").show();
                document.getElementById('MainContent_txtNoofSeats').value = '';
            }
        }
    </script>
    <script type="text/javascript">
        function Timecalculation(obj) {

            var strDate = Date(document.getElementById('MainContent_txtLatDDate'));
            var strtime = document.getElementById('MainContent_Text2');
            var Datetime = strDate + strtime;


            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var todaydateTime = date + ' ' + time;

            var diff = Math.abs(todaydateTime - Datetime) / 3600000;

            var strTime = str.value;

            var strtime = document.getElementById('MainContent_Time3');
        }
    </script>
    <script type="text/javascript">
        function MinimumCalculation(obj) {
            var Total = 0;
            var TotalMax = 0;
            var a = 0;
            var b = 0;
            var strflight = document.getElementById('MainContent_txtHourN');
            var strHourly = document.getElementById('MainContent_txthourlyrateN');
            var strfloor = document.getElementById('MainContent_txtfloorN');
            var strceil = document.getElementById('MainContent_txtCielN');
            var strceilval = strceil.value;
            var str1 = strflight.value;
            var str = str1.replace(':', '.');
            a = str;
            b = strHourly.value;
            Total = parseFloat(a) * parseFloat(b);

            if (parseFloat(Total) < parseFloat(strfloor.value)) {
                var Total = strfloor.value;

            }
            var strmin = document.getElementById('MainContent_txtminN');
            strmin.value = parseFloat(Total);

            TotalMax = parseFloat(a) * parseFloat(b);
            var final = 0;
            final = parseFloat(TotalMax) + parseFloat(strceilval);
            var strmax = document.getElementById('MainContent_txtMaxpriceN');
            strmax.value = final.toFixed(2);

            var minbd = 0;
            var maxbd = 0;
            var minprice = 0;
            var maxprice = 0;
            var daytogo = 0;


            var D4 = document.getElementById('MainContent_txtMinbd');
            var D5 = document.getElementById('MainContent_txtMaxBD');
            var M10 = document.getElementById('MainContent_txtMaxpriceN');
            var L10 = document.getElementById('MainContent_txtminN');
            var K10 = document.getElementById('MainContent_txtDayflightN');
            var strk = K10.value;
            var strk10 = strk.replace(':', '.');
            var strd4T = D4.value;
            var strd4FT = strd4T.replace(':', '.');
            var strd5T = D5.value;
            var strd5FT = strd5T.replace(':', '.');

            daytogo = strk10;

            minbd = strd4FT;
            maxbd = strd5FT;
            minprice = L10.value;
            maxprice = M10.value;

            var price = 0;
            price = (((1 - (parseFloat(maxbd) - parseFloat(daytogo)) / (parseFloat(maxbd) - (parseFloat(minbd)))) * (parseFloat(maxprice) - parseFloat(minprice)) + parseFloat(minprice)) / 25) * 25;

            var strprice = document.getElementById('MainContent_txtpriceN');
            strprice.value = price.toFixed(2);
        }
    </script>
    <script type="text/javascript">
        function txtAvaFromfun() {
            document.getElementById('MainContent_txtMinbd').value = document.getElementById('MainContent_txtStartTime1').value;
            document.getElementById('MainContent_txtMaxBD').value = document.getElementById('MainContent_Time1').value;
            document.getElementById('MainContent_txtMinSellHrs').value = document.getElementById('MainContent_Time2').value;
            document.getElementById('MainContent_txtHourN').value = document.getElementById('MainContent_txtHour').value;
        }
    </script>
    <script type="text/javascript">
        function txtAvaFromfunReverse() {

            document.getElementById('MainContent_Time1').value = document.getElementById('MainContent_txtMinbd').value;
            document.getElementById('MainContent_Time2').value = document.getElementById('MainContent_txtMaxBD').value;
            document.getElementById('MainContent_txtMinSellHrs').value = document.getElementById('MainContent_txtMinSellHrs').value;
            document.getElementById('MainContent_txtHour').value = document.getElementById('MainContent_txtHourN').value;
        }
    </script>
    <script type="text/javascript">
        function ComaprisonHourlyRate(obj) {
            var idEPTxt = String(obj.id);

            var idMPTxt = idEPTxt.replace("MainContent_txtfloorN", "MainContent_txthourlyrateN");
            var idMaximum = document.getElementById(idMPTxt);
            var val = (parseFloat(obj.value) > parseFloat(idMaximum.value)) ? 1 : 0;
            if (val == 0) {
                if (obj.value != "" && val == 0 && !isNaN(obj.value)) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'fixed floor should be greater than Hourly Rate';
                    $find("MainContent_mpealert").show();
                    document.getElementById("MainContent_txtfloorN").focus();
                }
                if (isNaN(obj.value) && val == 0) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'Invalid Entry';
                    $find("MainContent_mpealert").show();
                }
                var EpValues = document.getElementById(idEPTxt);
                EpValues.value = "";
            }
        }
    </script>
    <script type="text/javascript">
        function ComaprisoFixedfloor(obj) {
            var idEPTxt = String(obj.id);

            var idMPTxt = idEPTxt.replace("MainContent_txtCielN", "MainContent_txtfloorN");
            var idMaximum = document.getElementById(idMPTxt);
            var val = (parseFloat(obj.value) > parseFloat(idMaximum.value)) ? 1 : 0;
            if (val == 0) {
                if (obj.value != "" && val == 0 && !isNaN(obj.value)) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'Fixed Cieling should be greater than Fixed Floor';
                    $find("MainContent_mpealert").show();
                    document.getElementById("MainContent_txtCielN").focus();
                }
                if (isNaN(obj.value) && val == 0) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'Invalid Entry';
                    $find("MainContent_mpealert").show();
                }
                var EpValues = document.getElementById(idEPTxt);
                EpValues.value = "";
            }
        }
    </script>
    <script type="text/javascript">
        function ComaprisonHourlyRateData(obj) {
            var idEPTxt = String(obj.id);

            var idMPTxt = idEPTxt.replace("MainContent_FixedfloorData", "MainContent_HourlData");
            var idMaximum = document.getElementById(idMPTxt);
            var val = (parseFloat(obj.value) > parseFloat(idMaximum.value)) ? 1 : 0;
            if (val == 0) {
                if (obj.value != "" && val == 0 && !isNaN(obj.value)) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'fixed floor should be greater than Hourly Rate';
                    $find("MainContent_mpealert").show();
                    document.getElementById("MainContent_FixedfloorData").focus();
                }
                if (isNaN(obj.value) && val == 0) {
                    alert("Invalid Entry");
                }
                var EpValues = document.getElementById(idEPTxt);
                EpValues.value = "";
            }
        }
    </script>
    <script type="text/javascript">
        function ComaprisoFixedfloorData(obj) {
            var idEPTxt = String(obj.id);

            var idMPTxt = idEPTxt.replace("MainContent_fixedceilingData", "MainContent_FixedfloorData");
            var idMaximum = document.getElementById(idMPTxt);
            var val = (parseFloat(obj.value) > parseFloat(idMaximum.value)) ? 1 : 0;
            if (val == 0) {
                if (obj.value != "" && val == 0 && !isNaN(obj.value)) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'Fixed Cieling should be greater than Fixed Floor';
                    $find("MainContent_mpealert").show();
                    document.getElementById("MainContent_fixedceilingData").focus();
                }
                if (isNaN(obj.value) && val == 0) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'Invalid Entry';
                    $find("MainContent_mpealert").show();
                }
                var EpValues = document.getElementById(idEPTxt);
                EpValues.value = "";
            }
        }



        function FileDetails1(fupSlder, imgSlder) {
            if (fupSlder.value != '') {
                var a = document.getElementById("MainContent_btnUpload");
                a.click();
            }
            //            if (fupSlder.files && fupSlder.files[0]) {
            //                var reader = new FileReader();
            //                reader.onload = function (e) {
            //                    $(imgSlder).attr('src', e.target.result);
            //                }
            //                reader.readAsDataURL(fupSlder.files[0]);
            //            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="gv_Child_Tail" />
           <%-- <asp:PostBackTrigger ControlID="btnsubmit" />
            <asp:PostBackTrigger ControlID="btncancel" />--%>
        </Triggers>
        <ContentTemplate>
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row" id="tblGrid" runat="server">
                    <div class="col-lg-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body p-4" style="padding-top: 0rem !important;">
                                <div class="row form-group">
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-2 col-form-label" style="max-width: 12%;">
                                        <label>
                                            Tail Number<span class="MandatoryField">*</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtTailFilter" class="cusinputuser" placeholder="Search by tail #"
                                            OnTextChanged="textBox1_TextChanged" runat="server" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                                <div class="row m-0">
                                    <div class="col-lg-12 p-0 boxshadow table-responsive">
                                        <asp:GridView ID="gvpricing" runat="server" PagerStyle-CssClass="pager" EmptyDataText="No tail found"
                                            AutoGenerateColumns="false" PageSize="100" AllowPaging="True" DataKeyNames="TailNo"
                                            CssClass="table" HeaderStyle-CssClass="thead-dark" Width="100%" OnRowCommand="ContactsGridView_RowCommandNew"
                                            OnPageIndexChanging="gvPricingDetails_PageIndexChanging" OnRowDataBound="gvpricing_price_OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tail Number" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                                    ItemStyle-Width="12%" HeaderStyle-Width="12%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTailNo" Text='<%# Eval("TailNo") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Hourly Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                    ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <span>
                                                            <asp:Label ID="lblHourlyRate" Text='<%# String.Format("{0:c0}", Eval("HourlyRate")) %>'
                                                                runat="server"></asp:Label>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fixed Floor" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                    ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFixedFloor" Text='<%# String.Format("{0:c0}", Eval("FixedFloor")) %>'
                                                            runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fixed Ceiling" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" ItemStyle-Width="9%" HeaderStyle-Width="9%"
                                                    ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstart" Text='<%# String.Format("{0:c0}", Eval("FixedCeiling")) %>'
                                                            runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pricing Status" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="8%" HeaderStyle-Width="8%"
                                                    ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" />
                                                        <asp:Label ID="lblprice" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Pricingcompleted") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Gallery" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgGalleryFlag" runat="server" Height="21px" />
                                                        <asp:Label ID="lblGalleryFlag" Text='<%# Eval("GalleryFlag") %>' Visible="false"
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Updated By" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                                    HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcreatedby" Text='<%# Eval("createdby") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Updated On" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                                    HeaderStyle-Width="20%" ItemStyle-Width="20%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcreatedon" Text='<%# Eval("createdon") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Active" HeaderStyle-Width="3%" ItemStyle-Width="3%"
                                                    HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                                    HeaderStyle-Wrap="false">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkactiveM" TabIndex="7" runat="server" Checked='<%# Convert.ToBoolean(Eval("Show")) %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/pencil_Edit.png" HeaderText="Manage"
                                                    HeaderStyle-Width="1%" ItemStyle-Width="1%" HeaderStyle-HorizontalAlign="center"
                                                    ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-4" id="tblForm" runat="server" visible="false">
                    <div class="col-lg-12" style="padding-left: 5%; padding-right: 5%;">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label ">
                                Tail Number&nbsp;&nbsp;&nbsp;<asp:Label ID="lblETail" runat="server" Style="font-weight: bold;"></asp:Label>
                            </label>
                            <div class="col-xl-8 text-right">
                                <label class="importantlabels">
                                    <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                        ID="Label1" Text="Mandatory fields"></asp:Label>
                                </label>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="16"
                                    ValidationGroup="DC" CausesValidation="true" OnClick="btnSave_Click" />
                                <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="DC" ShowMessageBox="true"
                                    runat="server" ShowSummary="false" />
                                &nbsp;
                                <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="17"
                                    OnClick="btnViewOnClick" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-9">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label style="color: brown; font-weight: bold;">
                                            Aircraft Data
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">
                                        Fleet <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-3" style="padding-right: 0px">
                                        <asp:DropDownList ID="ddlFleet" runat="server" class="cusinputuser" TabIndex="1">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="ddlFleet"
                                            Display="None" ErrorMessage="Fleet is required." SetFocusOnError="True" InitialValue="0"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-lg-1" style="padding-top: 12px; max-width: 3%;">
                                        <asp:ImageButton ID="imgAdd" runat="server" ImageUrl="~/Images/Add2.png" OnClick="imgAdd_Click"
                                            ToolTip="click here to add new fleet" TabIndex="1" />
                                    </div>
                                    <div class="col-lg-1" style="padding-top: 9px; max-width: 3%;">
                                        <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/icon-edit-new.png"
                                            OnClick="imgEdit_Click" ToolTip="click here to edit fleet" TabIndex="1" Width="20px" />
                                    </div>
                                    <label class="col-lg-2 col-form-label">
                                        Base Airport
                                    </label>
                                    <div class="col-lg-2">
                                        <input class="cusinputuser" tabindex="3" id="txtBaseAirport" runat="server" style="width: 100%;" />
                                    </div>
                                    <label class="col-lg-1 col-form-label">
                                        Active
                                    </label>
                                    <div class="col-lg-1 col-form-label">
                                        <asp:CheckBox ID="chkActive" TabIndex="5" runat="server" Checked="true" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">
                                        Aircraft Type <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-3" style="padding-right: 0px;">
                                        <asp:DropDownList ID="ddlaircraft" runat="server" class="cusinputuser" TabIndex="2">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="ddlaircraft"
                                            Display="None" ErrorMessage="Aircraft Type is required." SetFocusOnError="True"
                                            InitialValue="None" ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-lg-1" style="padding-top: 12px; max-width: 3%;">
                                    </div>
                                    <div class="col-lg-1" style="padding-top: 12px; max-width: 3%;">
                                    </div>
                                    <label class="col-lg-2 col-form-label">
                                        No of Seats <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-2">
                                        <input class="cusinputuser" runat="server" id="txtNoofseats" onblur="checkseat()"
                                            onkeypress="return Numerics(event)" tabindex="4" maxlength="2" type="text" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtNoofseats"
                                            Display="None" ErrorMessage="No of Seats is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label style="color: brown; font-weight: bold;">
                                            Pricing Setup
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" style="padding-right: 0px;">
                                        Hourly Rate <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-3" style="padding-right: 0px;">
                                        <input class="cusinputuser" runat="server" id="txthourlyrateN" onblur="Amount(this)"
                                            placeholder="Example:3500.00" tabindex="6" onkeypress="return Numerics(event)"
                                            maxlength="8" type="text" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txthourlyrateN"
                                            Display="None" ErrorMessage="Hourly Rate is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <label class="col-lg-5 col-form-label">
                                        Min. time b/w Booking and Departure (Days) <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-2">
                                        <input class="cusinputuser" style="display: none;" runat="server" id="txtMinbd" maxlength="20"
                                            type="text" />
                                        <input class="cusinputuser" id="txtStartTime1" onblur="date1(this)" tabindex="11"
                                            placeholder="Example:0.25" runat="server" maxlength="5" style="margin-top: 2px;
                                            height: 37px;" />
                                        <asp:RequiredFieldValidator ID="rfvRP" runat="server" ControlToValidate="txtStartTime1"
                                            Display="None" ErrorMessage="Min. time b/w Booking and Departure (Days) is required."
                                            SetFocusOnError="True" ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" style="padding-right: 0px;">
                                        Fixed Floor <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-3" style="padding-right: 0px;">
                                        <input class="cusinputuser" runat="server" id="txtfloorN" maxlength="8" onblur="Amount(this)"
                                            placeholder="Example:5000.00" tabindex="7" onkeypress="return Numerics(event)"
                                            onchange="ComaprisonHourlyRate(this)" type="text">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtfloorN"
                                            Display="None" ErrorMessage="Fixed Floor is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <label class="col-lg-5">
                                        Max.time b/w Booking and Departure (Days) <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-2">
                                        <input class="cusinputuser" style="display: none;" runat="server" id="txtMaxBD" maxlength="20"
                                            type="text">
                                        <input class="cusinputuser" id="Time1" placeholder="Example:7.0" onblur=" date1(this)"
                                            maxlength="5" runat="server" onchange="txtAvaFromfun(this);" tabindex="12" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Time1"
                                            Display="None" ErrorMessage="Max. time b/w Booking and Departure (Days) is required."
                                            SetFocusOnError="True" ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" style="padding-right: 0px;">
                                        Fixed Ceiling <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-3" style="padding-right: 0px;">
                                        <input class="cusinputuser" runat="server" id="txtCielN" maxlength="8" tabindex="8"
                                            onchange="Amount(this)" onkeypress="return Numerics(event)" placeholder="Example:10000.00"
                                            type="text" onblur="  ComaprisoFixedfloor(this);MinimumCalculation(this);" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCielN"
                                            Display="None" ErrorMessage="Fixed Ceiling is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <label class="col-lg-5">
                                        Min. Flight Sellabale Hrs
                                    </label>
                                    <div class="col-lg-2">
                                        <input class="cusinputuser" style="display: none;" runat="server" id="txtMinSellHrs"
                                            maxlength="20" type="text">
                                        <input class="cusinputuser" id="Time2" placeholder="Example:0.70" runat="server"
                                            maxlength="5" onblur=" date1(this)" onchange="txtAvaFromfun(this);" tabindex="13" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Time2"
                                            Display="None" ErrorMessage="Min. Flight Sellabale Hrs is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" style="padding-right: 0px;">
                                        DOC(per Hour) <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-3" style="padding-right: 0px;">
                                        <input class="cusinputuser" runat="server" id="txtopcost" tabindex="9" maxlength="8"
                                            type="text" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtopcost"
                                            Display="None" ErrorMessage="DOC(per Hour) is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <label class="col-lg-5">
                                        Runway Length(feet) <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-2">
                                        <input class="cusinputuser" tabindex="14" id="txtrunwaylength" onkeypress="return Numericswithdot(event)"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtrunwaylength"
                                            Display="None" ErrorMessage="Runway Length is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-2" style="padding-right: 0px;">
                                        Avg Speed(in Kts) <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-3" style="padding-right: 0px;">
                                        <input class="cusinputuser" tabindex="10" id="txtAvgspeed" onkeypress="return Numericswithdot(event)"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtAvgspeed"
                                            Display="None" ErrorMessage="Avg Speed(in Kts) is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <label class="col-lg-5">
                                        Yield Revenue <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-2">
                                        <input class="cusinputuser" tabindex="14" id="txtYielldRev" onkeypress="return Numericswithdot(event)"
                                            runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtYielldRev"
                                            Display="None" ErrorMessage="Yield Revenue is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-8 col-form-label">
                                        If FET and segment tax to be charged in invoice&nbsp;&nbsp;<asp:CheckBox ID="chktaxapply"
                                            TabIndex="16" runat="server" Checked="true" />
                                    </label>
                                </div>
                                <div class="form-group row">
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label style="color: brown; font-weight: bold;">
                                            Tail Gallery
                                        </label>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="uplTailGallery" runat="server">
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnUpload" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <div class="form-group row">
                                            <div class="col-lg-12">
                                                <label style="color: black; font-weight: 600;">
                                                    1. Exterior
                                                </label>
                                                <label class="file-upload-new" style="height: 150px;">
                                                    <img src="../assets/img/No_Image_Available.jpg" id="imgSlder1" runat="server" style="border-radius: 5px;
                                                        width: 100%; height: 100%;" alt="" />
                                                    <asp:Label ID="lblSlider1" runat="server" Visible="false" />
                                                    <asp:FileUpload ID="fupSlider1" accept=".png, .jpg, .jpeg" AllowMultiple="false"
                                                        runat="server" ToolTip="Drop files here or click to upload." onchange="FileDetails1(MainContent_fupSlider1, MainContent_imgSlder1)"
                                                        Style="width: 100%; height: 150px; cursor: pointer;"></asp:FileUpload>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-12">
                                                <label style="color: black; font-weight: 600;">
                                                    2. Interior
                                                </label>
                                                <label class="file-upload-new" style="height: 150px;">
                                                    <img src="../assets/img/No_Image_Available.jpg" id="imgSlder2" runat="server" style="border-radius: 5px;
                                                        width: 100%; height: 100%;" alt="" />
                                                    <asp:Label ID="lblSlider2" runat="server" Visible="false" />
                                                    <asp:FileUpload ID="fupSlider2" accept=".png, .jpg, .jpeg" AllowMultiple="false"
                                                        runat="server" ToolTip="Drop files here or click to upload." onchange="FileDetails1(MainContent_fupSlider2, MainContent_imgSlder2)"
                                                        Style="width: 100%; height: 150px; cursor: pointer;"></asp:FileUpload>
                                                </label>
                                            </div>
                                        </div>
                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-danger" Style="display: none;"
                                            TabIndex="1" OnClick="btnUpload_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <div id="divLMBy" runat="server" visible="false">
                                    <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                        font-weight: 600;"></asp:Label>
                                    <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row" runat="server" id="dvformSub">
                            <div class="col-lg-12">
                                <asp:GridView ID="gv_Child_Tail" runat="server" AutoGenerateColumns="false" Style="width: 100%"
                                    EmptyDataText="No trips found." ShowFooter="true" UseAccessibleHeader="true"
                                    CssClass="table" HeaderStyle-CssClass="thead-dark" OnRowDataBound="gv_Child_Tail_OnRowDataBound"
                                    OnSelectedIndexChanged="gvPricingDetailschage_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tail Number" Visible="false" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="left" ItemStyle-Width="2%" HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSubID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "TailNo") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trip Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltripno" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "tripno") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Leg" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblleg" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "leg") %>' />
                                                <asp:Label ID="lbltailflag" runat="server" Text='<%# Eval("Tailflag") %>' Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Airport" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="center" ItemStyle-Width="7%" HeaderStyle-Width="7%"
                                            ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartAirport" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Startairport") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Airport" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                            ItemStyle-Width="7%" HeaderStyle-Width="7%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblendAirport" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "endairport") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                            ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDate" runat="server" Text='<%# Eval("AvailbleFrom", "{0:MMMM d, yyyy}") %>'></asp:Label>
                                                <asp:Label ID="lbldateD" Visible="false" runat="server" Text='<%# Eval("AvailbleFrom") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Time" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstarttime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartTime") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Time" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center"
                                            ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblendtime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Endtime") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pricing Status" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="8%" HeaderStyle-Width="8%"
                                            ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Image ID="Image1" runat="server" />
                                                <asp:Label ID="lblprice" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Pricingcompleted") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Updated By" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                            HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcreatedby" Text='<%# Eval("created") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Updated On" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                            HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcreatedon" Text='<%# Eval("createdonN") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-Width="3%" HeaderStyle-Width="3%">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkActiveG" runat="server" Checked='<%# Convert.ToBoolean(Eval("Show")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/Images/pencil_Edit.png"
                                            HeaderText="Manage" HeaderStyle-Width="1%" ItemStyle-Width="1%" HeaderStyle-HorizontalAlign="center"
                                            ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnprice" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeprice" runat="server" TargetControlID="btnprice" PopupControlID="pnlprice"
        CancelControlID="btncancelpopup" BackgroundCssClass="modalBackground" X="350"
        Y="50">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlprice" runat="server" Width="48%" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,34); padding: 15px; color: rgb(255,255,255)">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px;
            padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr style="border-left: 0px">
                <td align="right">
                    <asp:LinkButton ID="btncancelpopup" runat="server" Style="color: rgb(255,255,255);
                        text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                </td>
            </tr>
            <tr>
                <td style="font-size: 13px; font-weight: bold; letter-spacing: 0.5px;" nowrap="nowrap"
                    align="center" valign="middle">
                    <br />
                    Do you want to configure the same pricing details for all empty legs for the tail
                    <span id="tail" runat="server"></span>?
                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="padding-top: 10px">
                    <asp:LinkButton ID="btnsubmit" runat="server" Class="btn btn-danger" Text="Yes" OnClick="Buttonyes_Click"
                        ValidationGroup="VP" UseSubmitBehavior="false" />
                    &nbsp;
                    <asp:LinkButton ID="btncancel" runat="server" Class="btn btn-danger" Text="No" OnClick="Buttonno_Click"
                        ValidationGroup="VP" UseSubmitBehavior="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnshow" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeData" runat="server" TargetControlID="btnshow" PopupControlID="pnlshow"
        CancelControlID="btncancael" BackgroundCssClass="modalBackground" Y="10">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlshow" runat="server" Width="70%" Style="display: none; background-color: rgb(17,24,34);
        color: rgb(255,255,255)">
        <table width="100%" style="width: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-left: 0px">
                <td style="margin-right: 2%; width: 20%; padding-left: 2%; color: White; font-weight: bold;
                    border-color: none; font-size: larger; vertical-align: middle; color: White"
                    align="left">
                    Pricing setup
                </td>
                <td style="width: 80%; padding-right: 2%; padding-top: 2%" nowrap="nowrap">
                    <table style="width: 100%; padding-left: 10px; margin-left: 5px" id="Table1" runat="server">
                        <tr>
                            <td style="width: 10%; text-align: left">
                                <asp:Label ID="Label2" Style="color: yellow; font-weight: bold" Text="Last Modified by :"
                                    runat="server"></asp:Label>&nbsp&nbsp&nbsp
                            </td>
                            <td style="width: 10%; text-align: left">
                                <asp:Label ID="lbluserdata" Style="color: white; font-weight: bold" runat="server"></asp:Label>&nbsp&nbsp&nbsp
                            </td>
                            <td style="width: 10%; text-align: left">
                                <asp:Label ID="Label51" Text="(on)" Style="color: yellow; font-weight: bold" runat="server"></asp:Label>&nbsp&nbsp&nbsp
                            </td>
                            <td style="width: 60%; text-align: left">
                                <asp:Label ID="lbldate1data" Style="color: white; font-weight: bold" runat="server"></asp:Label>
                            </td>
                            <td style="width: 10%; vertical-align: middle; text-align: left; padding-right: 2%">
                                <asp:LinkButton ID="btncancael" runat="server" Style="color: rgb(255,255,255); text-decoration: none"
                                    CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" colspan="2" style="padding: 2%; padding-bottom: 0px" colspan="2">
                    <table width="100%" style="padding-left: 2%">
                        <tr>
                            <td style="width: 25%">
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    Tail Number</label>
                            </td>
                            <td style="width: 25%">
                                <asp:Label ID="lblTailData" runat="server"></asp:Label>
                            </td>
                            <td style="width: 25%; padding-left: 2%;">
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    Trip Number</label>
                            </td>
                            <td style="width: 25%">
                                <asp:Label ID="lblTripData" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    Start Airport</label>
                            </td>
                            <td>
                                <asp:Label ID="lblStartData" runat="server"></asp:Label>
                            </td>
                            <td style="width: 25%; padding-left: 2%;">
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    End Airport</label>
                            </td>
                            <td>
                                <asp:Label ID="lblendData" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    Start City</label>
                            </td>
                            <td>
                                <asp:Label ID="lblscitydata" runat="server"></asp:Label>
                            </td>
                            <td style="width: 25%; padding-left: 2%">
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    End City</label>
                            </td>
                            <td>
                                <asp:Label ID="lblEcityData" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    Start Time</label>
                            </td>
                            <td>
                                <asp:Label ID="lblStimedata" runat="server" Visible="false"></asp:Label>
                            </td>
                            <td style="width: 25%; padding-left: 2%">
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    End Time</label>
                            </td>
                            <td>
                                <asp:Label ID="lblEtimeData" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="txtFname">
                                    Date</label>
                            </td>
                            <td>
                                <asp:Label ID="lblDateData" runat="server"></asp:Label>
                            </td>
                            <td style="width: 25%; padding-left: 2%">
                                <label class="cuslabeluser" style="color: rgb(255,255,255)" for="chkEmpty">
                                    Enable Trip</label>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkEnableData" TabIndex="7" runat="server" Checked="true" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="49%" style="padding: 16px">
                    <div class="cusformuser-cuscontaineruser" style="padding-bottom: 2px">
                        <div class="cusfielduser-cuscontaineruser" style="padding-bottom: 10px;">
                            <label class="cuslabeluser" for="txtMinbd" style="color: rgb(255,255,255)">
                                Min. time b/w Booking and Departure (Days) <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" id="MintimeMData" onblur="date1(this)" placeholder="Example:0.25"
                                tabindex="12" runat="server" maxlength="5" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="MintimeMData"
                                Display="None" ErrorMessage="Min. time b/w Booking and Departure (Days) is required."
                                SetFocusOnError="True" ValidationGroup="Data">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="cusfielduser-cuscontaineruser" style="padding-bottom: 10px;">
                            <label class="cuslabeluser" for="txtMaxBD" style="color: rgb(255,255,255)">
                                Max.time b/w Booking and Departure (Days) <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" id="MaxtimeMData" placeholder="Example:7.00" onblur=" date1(this)"
                                tabindex="13" maxlength="5" runat="server" onchange="txtAvaFromfun(this);" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="MaxtimeMData"
                                Display="None" ErrorMessage="Max. time b/w Booking and Departure (Days) is required."
                                SetFocusOnError="True" ValidationGroup="Data">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="cusfielduser-cuscontaineruser" style="padding-bottom: 10px;">
                            <label class="cuslabeluser" for="txtMinSellHrs" style="color: rgb(255,255,255)">
                                Min. Flight Sellabale Hrs <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" id="minSellMData" placeholder="Example:0.70" runat="server"
                                tabindex="14" maxlength="5" onblur=" date1(this)" onchange="txtAvaFromfun(this);" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="minSellMData"
                                Display="None" ErrorMessage=" Min. Flight Sellabale Hrs is required." SetFocusOnError="True"
                                ValidationGroup="Data">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="cusformuser-cuscontaineruser" style="padding-bottom: 10px;">
                            <div class="cusfielduser-cuscontaineruser" style="padding-left: 0px">
                                <asp:CheckBox ID="chkActiveData" TabIndex="15" runat="server" Checked="true" />
                                <label class="cuslabeluser" for="chkEmpty" style="color: rgb(255,255,255)">
                                    Active</label>
                            </div>
                        </div>
                    </div>
                </td>
                <td width="49%" style="padding: 16px">
                    <div class="cusformuser-cuscontaineruser" style="padding-bottom: 2px">
                        <div class="cusfielduser-cuscontaineruser" style="padding-bottom: 10px;">
                            <label class="cuslabeluser" for="txtLastName" style="color: rgb(255,255,255)">
                                Hourly Rate <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" runat="server" id="HourlData" placeholder="Example:3500.00"
                                tabindex="16" onblur="Amount(this)" onkeypress="return Numerics(event)" maxlength="8"
                                type="text" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="HourlData"
                                Display="None" ErrorMessage="Hourly Rate is required." SetFocusOnError="True"
                                ValidationGroup="Data">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="cusfielduser-cuscontaineruser" style="padding-bottom: 10px;">
                            <label class="cuslabeluser" for="txtLastName" style="color: rgb(255,255,255)">
                                Fixed Floor <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" runat="server" id="FixedfloorData" placeholder="Example:5000.00"
                                tabindex="17" maxlength="8" onblur="Amount(this)" onkeypress="return Numerics(event)"
                                onchange="ComaprisonHourlyRateData(this)" type="text" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="FixedfloorData"
                                Display="None" ErrorMessage=" Fixed Floor is required." SetFocusOnError="True"
                                ValidationGroup="Data">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="cusfielduser-cuscontaineruser" style="padding-bottom: 10px;">
                            <label class="cuslabeluser" for="txtLastName" style="color: rgb(255,255,255)">
                                Fixed Ceiling <span style="color: Red">*</span></label>
                            <input class="cusinputuserTextbox" runat="server" id="fixedceilingData" tabindex="18"
                                maxlength="8" placeholder="Example:10000.00" onchange="Amount(this)" onkeypress="return Numerics(event)"
                                type="text" onblur="  ComaprisoFixedfloorData(this);MinimumCalculation(this);" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="fixedceilingData"
                                Display="None" ErrorMessage="Fixed Ceiling is required." SetFocusOnError="True"
                                ValidationGroup="Data">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="cusformuser-cuscontaineruser" style="padding-bottom: 10px;">
                            <div class="cusfielduser-cuscontaineruser" style="padding-left: 0px">
                                <asp:CheckBox ID="chkTaxData" runat="server" TabIndex="19" Checked="true" />
                                <label class="cuslabeluser" for="chkEmpty" style="color: rgb(255,255,255)">
                                    If FET and segment tax to be charged in invoice</label>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding-bottom: 2%" colspan="2">
                    <asp:LinkButton ID="btnSavedata" runat="server" CssClass="btn btn-danger" Text="Save"
                        ValidationGroup="Data" OnClick="btnSavedata_Click" TabIndex="20" UseSubmitBehavior="false" />
                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Data" ShowMessageBox="true"
                        runat="server" ShowSummary="false" />
                    <asp:LinkButton ID="btncanceldata" runat="server" TabIndex="21" CssClass="btn btn-danger"
                        Text="Cancel" OnClick="btncanceldata_Click" ValidationGroup="VP" UseSubmitBehavior="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnUserPre" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeUserPref" runat="server" TargetControlID="btnUserPre"
        PopupControlID="pnlUserPref" BackgroundCssClass="modalBackground" Y="80" CancelControlID="PrefClose">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlUserPref" runat="server" Style="display: none; width: 40%; background-color: White;
        padding: 10px">
        <table width="100%" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            <h5 style="margin-left: 0%; font-size: 18px">
                                Add / Edit Fleet</h5>
                        </label>
                        <div class="col-6 text-right">
                            <asp:LinkButton ID="PrefClose" runat="server" Style="color: rgb(255,255,255); float: right;
                                text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                        </div>
                    </div>
                    <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                        <label class="col-3 col-form-label">
                            Fleet Code <span class="MandatoryField">* </span>
                        </label>
                        <div class="col-9">
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtCode" Style="width: 90%;"
                                TabIndex="27" MaxLength="50">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtCode"
                                Display="None" ErrorMessage="Fleet Code is required." SetFocusOnError="True"
                                ValidationGroup="FT">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                        <label class="col-3 col-form-label">
                            Fleet Name <span class="MandatoryField">* </span>
                        </label>
                        <div class="col-9">
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtName" Style="width: 90%;"
                                TabIndex="27" MaxLength="50">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtName"
                                Display="None" ErrorMessage="Fleet Name is required." SetFocusOnError="True"
                                ValidationGroup="FT">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row" style="font-size: 1.15vw; font-weight: 400;">
                        <label class="col-3 col-form-label">
                            Active
                        </label>
                        <div class="col-9" style="padding-top: 8px;">
                            <asp:RadioButtonList ID="rblActive" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-group row">
                    </div>
                    <div class="form-group row">
                        <div class="col-12 text-center">
                            <asp:Button ID="btnAddMore" runat="server" Text="Save" CssClass="btn btn-danger"
                                TabIndex="33" ValidationGroup="FT" OnClick="btnAddMore_Click" />
                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="FT" ShowMessageBox="true"
                                runat="server" ShowSummary="false" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <asp:Label ID="lblalert" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:Label ID="lbltail" runat="server" Visible="false" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
    <asp:Label ID="lblUserId" Text="Email" runat="server" Visible="false" />
    <asp:Label ID="lblEditFleetId" Text="0" runat="server" Visible="false" />
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
