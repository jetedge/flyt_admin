﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Views_EmptyLegSetup_V2 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Empty Leg Details");
            }
            

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "Admin";
            lblSubHeader.InnerText = "Empty Leg Setup";

            BindEmptyLegs();

            hdfSCID.Value = "0";
            tblForm.Visible = false;
            tblGrid.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
            btnAdd.Focus();
        }
    }

    public void BindEmptyLegs()
    {
        string strMangeType = string.Empty;
        string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
        string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

        if (rblStatus.SelectedValue.Trim() == "Active")
        {
            strMangeType = "Active";
            lblSubScren.Text = "Add a Manual Trip";
            btnAdd.Visible = true;
        }
        else if (rblStatus.SelectedValue.Trim() == "Booked Trips")
        {
            strMangeType = "Booked";
            lblSubScren.Text = "Modify a FOS Trip";
            btnAdd.Visible = false;
        }
        else if (rblStatus.SelectedValue.Trim() == "Group")
        {
            strMangeType = "Split";
            lblSubScren.Text = "Add a Manual Leg";
            btnAdd.Visible = false;
        }

        DataTable dtOneWayTrip = BindEmptyLegs_SP(strMangeType, strStartDate, strEndDate, "1", "25");
        dtOneWayTrip.Columns.Add("leg");

        for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
        {
            //            if (dtOneWayTrip.Rows[i]["Type"].ToString() == "FOS DH" && dtOneWayTrip.Rows[i]["Created"].ToString() != "")
            //            {
            //                DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
            //                Where TailNo='" + dtOneWayTrip.Rows[i]["Tailno"].ToString() + @"' and tripno='" + dtOneWayTrip.Rows[i]["tripno"].ToString()
            //                           + @"' and Mod_AvilableFrom='" + Convert.ToDateTime(dtOneWayTrip.Rows[i]["FromDate"].ToString()).ToString("yyyy-MM-dd")
            //                           + @"' and Mod_Starttime='" + Convert.ToDateTime(dtOneWayTrip.Rows[i]["starttime"].ToString()).ToString("HH:mm")
            //                           + @"' and Mod_Endtime='" + Convert.ToDateTime(dtOneWayTrip.Rows[i]["Endtime"].ToString()).ToString("HH:mm")
            //                           + @"' and Mod_StartAirport='" + dtOneWayTrip.Rows[i]["StartAirport"].ToString() + @"'
            //                and Mod_EndAirport='" + dtOneWayTrip.Rows[i]["EndAirport"].ToString() + @"'").Tables[0];

            //                if (dtleg.Rows.Count > 0)
            //                {
            //                    dtOneWayTrip.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
            //                }

            //            }
            //            else
            //            {
            //            DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
            //                Where TailNo='" + dtOneWayTrip.Rows[i]["Tailno"].ToString().Trim() + @"' and tripno='" + dtOneWayTrip.Rows[i]["tripno"].ToString().Trim() 
            //                     + @"' and AvailbleFrom='" + Convert.ToDateTime(dtOneWayTrip.Rows[i]["FromDate"].ToString()).ToString("yyyy-MM-dd")
            //                     + @"' and StartTime='" + Convert.ToDateTime(dtOneWayTrip.Rows[i]["starttime"].ToString()).ToString("HH:mm")
            //                     + @"' and EndTime='" + Convert.ToDateTime(dtOneWayTrip.Rows[i]["Endtime"].ToString()).ToString("HH:mm")
            //                     + @"' and StartAirport='" + dtOneWayTrip.Rows[i]["StartAirport"].ToString() + @"'
            //                and EndAirport='" + dtOneWayTrip.Rows[i]["EndAirport"].ToString() + @"'").Tables[0];

            //            if (dtleg.Rows.Count > 0)
            //            {
            //                dtOneWayTrip.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
            //            }
            //            else
            //            {
            //                dtOneWayTrip.Rows[i]["leg"] = "0";
            //            }
            // }

        }
        if (dtOneWayTrip.Rows.Count > 0)
        {
            gvTailDetails.DataSource = dtOneWayTrip;
            gvTailDetails.DataBind();
        }
        else
        {
            gvTailDetails.DataSource = null;
            gvTailDetails.DataBind();
        }
        gvTailDetails.Visible = true;
    }
    private void Loadcountry()
    {
        try
        {
            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code,Country_Name  from JETEDGE_FUEL..Country where Active='Y' order by Country_Name").Tables[0];

            cblcountry.DataSource = dt;
            cblcountry.DataTextField = "Country_Name";
            cblcountry.DataValueField = "Country_Name";
            cblcountry.DataBind();

            //chkendcountry.DataSource = dt;
            //chkendcountry.DataTextField = "Country_Name";
            //chkendcountry.DataValueField = "Country_Name";
            //chkendcountry.DataBind();
        }
        catch (Exception Ex)
        {

        }
    }


    protected void btnSaveOnClick(object sender, EventArgs e)
    {
        string[] CategoryDetails = new string[70];
        string strMsg = string.Empty;
        string strActive = string.Empty;
        try
        {
            #region For Manual and Modiy Fos

            if (lblgroup.Text != "Group")
            {
                string strBusinessAlert = string.Empty;

                if (txtTailNo.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Tail number is required. <br/>";
                }
                if (txttrip.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Trip number is required. <br/>";
                }
                if (txtFlyHours1.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Flying Hours is required. <br/>";
                }
                if (txtAvaFrom.Text.ToString().Length == 0)
                {
                    strBusinessAlert += "- Available From is required. <br/>";
                }
                if (txtAvaTo.Text.ToString().Length == 0)
                {
                    strBusinessAlert += "- Available To is required. <br/>";
                }
                if (txtMPrice.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Price is required. <br/>";
                }

                if (txtStartTime.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Start Time is required. <br/>";
                }

                if (txtEndTime.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- End Time is required. <br/>";
                }


                if (lblgroup.Text != "Modify")
                {
                    if (gvDepAirport.Rows.Count == 0)
                    {
                        strBusinessAlert += "- Departure Airport is required. <br/>";
                    }

                    if (gvArrivalAirport.Rows.Count == 0)
                    {
                        strBusinessAlert += "- Arrival Airport is required. <br/>";
                    }
                }

                if (gvDepAirport.Rows.Count > 0)
                {
                    if (txtDepDisLandPage.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Departure Landing Page input is required. <br/>";
                    }
                }
                if (gvArrivalAirport.Rows.Count > 0)
                {
                    if (txtArrDisLandPage.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Arrival Landing Page input is required. <br/>";
                    }
                }


                DataTable dtPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select HourlyRate,pricingcompleted  from Tailsetup where tailno='" + txtTailNo.Value.Trim() + "'").Tables[0];

                if (dtPrice.Rows.Count > 0)
                {
                    if (dtPrice.Rows[0]["pricingcompleted"].ToString().Trim() == "0")
                    {
                        strBusinessAlert += "- Please complete the pricing setup for the tail [ " + txtTailNo.Value.Trim() + " ] <br/>";
                    }

                    if (txtMPrice.Value.Trim() != "")
                    {
                        string strHourlyRate = dtPrice.Rows[0]["HourlyRate"].ToString().Trim();

                        if (Convert.ToDecimal(txtMPrice.Value.Trim()) < Convert.ToDecimal(strHourlyRate.Trim()))
                        {
                            strBusinessAlert += "- Please enter price greater than $ " + strHourlyRate + " <br/>";
                        }
                    }
                }
                else
                {
                    strBusinessAlert += "- Tail not exist in pricing setup. <br/>";
                }


                if (strBusinessAlert.Length > 0)
                {
                    lblalert.Text = strBusinessAlert;
                    mpealert.Show();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                    return;
                }
                decimal daystoflight = 0;
                decimal L10_minPrice = 0;
                decimal M10_maxPrice = 0;
                decimal price = 0;

                decimal totalflyingHours = Convert.ToDecimal(txtFlyHours1.Value);

                var HourlyRate = Convert.ToDecimal("0.00"); ;
                var fixedCeiling = Convert.ToDecimal("0.00");
                var fixedFloor = Convert.ToDecimal("0.00");
                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                var MinFLHSellable = Convert.ToDecimal("0.00");
                var finalPricevalidation2 = (decimal)0;


                var dtFlyingDays = Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00";

                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00").Subtract(DateTime.Now);

                var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                string stractive = string.Empty;

                if (chkEmpty.Checked)
                {
                    stractive = "1";
                }
                else
                {
                    stractive = "0";
                }

                if (hdfSCID.Value == "0")
                {
                    CategoryDetails[0] = "I";

                    DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
                    if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
                    {
                        gv_Child_Tail.DataSource = dttailtrip;
                        gv_Child_Tail.DataBind();
                        mpetrip.Show();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                        return;
                    }

                    DataTable dttail = new DataTable();

                    //if (gvDepAirport.Rows.Count > 0)
                    //{
                    //    if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
                    //    {
                    //        string strstartcity2 = txtStartCity.Text.Replace(" - ", "-");
                    //        String[] strliststart = strstartcity2.Split('-');

                    //        string strendcity2 = txtEndCity.Text.Replace(" - ", "-");
                    //        String[] strlistend = strendcity2.Split('-');
                    //        if (strliststart.Length > 1 && strlistend.Length > 1)
                    //        {
                    //            dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + strliststart[1] + "' and EndAirport='" + strlistend[1] + "'").Tables[0];
                    //        }
                    //    }
                    //    else
                    //    {
                    //        dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + txtStartCity.Text.Trim() + "' and EndAirport='" + txtEndCity.Text.Trim() + "'").Tables[0];
                    //    }
                    //}




                    if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
                    {
                        lblalert.Text = "Tail No  already exists.";
                        mpeconfirm.Show();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                        return;
                        txtTailNo.Value = string.Empty;
                    }
                }
                else
                {
                    if (LBLTRIPNO.Text != txttrip.Value)
                    {
                        DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
                        if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
                        {
                            gv_Child_Tail.DataSource = dttailtrip;
                            gv_Child_Tail.DataBind();
                            mpetrip.Show();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                            return;
                        }
                        DataTable dttail = new DataTable();

                        //if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
                        //{
                        //    string strstartcity1 = txtStartCity.Text.Replace(" - ", "-");
                        //    String[] strliststart = strstartcity1.Split('-');

                        //    string strendcity1 = txtEndCity.Text.Replace(" - ", "-");
                        //    String[] strlistend = strendcity1.Split('-');
                        //    dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + strliststart[1] + "' and EndAirport='" + strlistend[1] + "'").Tables[0];
                        //}
                        //else
                        //{
                        //    dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from OneWayTail_Setup where TailNo='" + txtTailNo.Value + "' and TripNo ='" + txttrip.Value + "' and AvailbleFrom='" + Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd") + "' and AvilbleTo='" + Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + txtStartTime.Value + "' and EndTime='" + txtEndTime.Value + "' and StartAirport='" + txtStartCity.Text.Trim() + "' and EndAirport='" + txtEndCity.Text.Trim() + "'").Tables[0];
                        //}

                        if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
                        {
                            lblalert.Text = "Tail No  already exists.";
                            mpeconfirm.Show();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                            return;
                        }
                    }

                    CategoryDetails[0] = "U";
                }
                string atctivetail = string.Empty;
                if (chkEmpty.Checked)
                {
                    atctivetail = "1";
                }
                else
                {
                    atctivetail = "0";
                }

                DataTable dtstatrt = new DataTable();
                DataTable dtend = new DataTable();

                DateTime fromDate = new DateTime();
                DateTime toDate = new DateTime();
                string dateTimeNow = string.Empty;


                DateTime DatefromTZ = new DateTime();
                DateTime DatetoTZ = new DateTime();
                DatefromTZ = Convert.ToDateTime(txtAvaFrom.Text + " " + txtStartTime.Value);
                if (dtstatrt.Rows.Count > 0)
                {
                    fromDate = Convert.ToDateTime(DatefromTZ).AddHours(-Convert.ToInt32(dtstatrt.Rows[0]["Timezone"].ToString()));
                }

                DatetoTZ = Convert.ToDateTime(txtAvaTo.Text + " " + txtEndTime.Value);
                if (dtend.Rows.Count > 0)
                {
                    toDate = Convert.ToDateTime(DatetoTZ).AddHours(-Convert.ToInt32(dtend.Rows[0]["Timezone"].ToString()));
                }


                string strstartdate = fromDate.ToString("yyyy-MM-dd");
                string strstarttime = fromDate.ToString("HH:mm");

                string strenddate = toDate.ToString("yyyy-MM-dd");
                string strendtime = toDate.ToString("HH:mm");
                string strstartcity = string.Empty;
                string strendcity = string.Empty;

                if (hdfSCID.Value == "")
                {
                    CategoryDetails[1] = "0";
                }
                else
                {
                    CategoryDetails[1] = hdfSCID.Value;
                }
                CategoryDetails[2] = txtTailNo.Value;
                CategoryDetails[3] = txtTailNo.Value;

                if (gvDepAirport.Rows.Count > 0)
                {
                    string strAirportCode = string.Empty;
                    string strAirportName = string.Empty;

                    string strStateCode = string.Empty;
                    string strStateName = string.Empty;

                    string strCountryCode = string.Empty;
                    string strCountryName = string.Empty;

                    for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                    {
                        Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                        Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                        if (lblSetupBy.Text.Trim() == "Airport")
                        {
                            strstartcity = lblAirport.Text.Replace(" - ", "-");
                            String[] strliststart = strstartcity.Split('-');

                            strAirportCode += strliststart[1] + "/";
                            strAirportName += strliststart[0] + "/";
                        }
                        if (lblSetupBy.Text.Trim() == "State")
                        {
                            string strState = lblAirport.Text;
                            if (strState.Contains(','))
                            {
                                string[] strSplit = strState.Split(',');

                                if (strSplit.Length > 1)
                                {
                                    strStateName += strSplit[0].ToString().Trim() + "/";

                                    string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                    if (strSTCode.Length > 1)
                                    {
                                        strStateCode += strSTCode[0].ToString().Trim() + "/";
                                    }
                                }
                            }
                        }
                        if (lblSetupBy.Text.Trim() == "Country")
                        {
                            string strCountry = lblAirport.Text;
                            if (strCountry.Contains(','))
                            {
                                string[] strSplit = strCountry.Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strCountryCode += strSplit[1].ToString().Trim() + "/";
                                    strCountryName += strSplit[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }


                    CategoryDetails[4] = (strCountryCode + strStateCode + strAirportCode).TrimEnd('/');
                    CategoryDetails[5] = (strCountryName + strStateName + strAirportName).TrimEnd('/');

                    CategoryDetails[52] = txtDepDisLandPage.Text.Trim();
                }
                else
                {
                    CategoryDetails[4] = lblStartData.Text;
                    CategoryDetails[5] = lblscitydata.Text;

                    CategoryDetails[52] = lblStartData.Text;
                }
                if (gvArrivalAirport.Rows.Count > 0)
                {
                    string strEndAirportCode = string.Empty;
                    string strEndAirportName = string.Empty;

                    string strEndStateCode = string.Empty;
                    string strEndStateName = string.Empty;

                    string strEndCountryCode = string.Empty;
                    string strEndCountryName = string.Empty;
                    for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                    {
                        Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                        Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                        if (lblSetupBy.Text.Trim() == "Airport")
                        {
                            strstartcity = lblAirport.Text.Replace(" - ", "-");
                            String[] strliststart = strstartcity.Split('-');

                            strEndAirportCode += strliststart[1] + "/";
                            strEndAirportName += strliststart[0] + "/";
                        }
                        if (lblSetupBy.Text.Trim() == "State")
                        {
                            string strState = lblAirport.Text;
                            if (strState.Contains(','))
                            {
                                string[] strSplit = strState.Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strEndStateName += strSplit[0].ToString().Trim() + "/";

                                    string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                    if (strSTCode.Length > 1)
                                    {
                                        strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                    }
                                }
                            }
                        }
                        if (lblSetupBy.Text.Trim() == "Country")
                        {
                            string strCountry = lblAirport.Text;
                            if (strCountry.Contains(','))
                            {
                                string[] strSplit = strCountry.Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                    strEndCountryName += strSplit[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }

                    CategoryDetails[7] = (strEndCountryCode + strEndStateCode + strEndAirportCode).TrimEnd('/');
                    CategoryDetails[8] = (strEndCountryName + strEndStateName + strEndAirportName).TrimEnd('/');
                    CategoryDetails[53] = txtArrDisLandPage.Text.Trim();
                }
                else
                {
                    CategoryDetails[7] = lblendData.Text.Trim();
                    CategoryDetails[8] = lblEcityData.Text.Trim();
                    CategoryDetails[53] = lblendData.Text.Trim();
                }



                CategoryDetails[6] = txtStartTime.Value;
                CategoryDetails[9] = txtEndTime.Value;
                CategoryDetails[10] = "";
                CategoryDetails[11] = txtFlyHours1.Value;
                CategoryDetails[12] = Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd");
                CategoryDetails[13] = Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd");

                CategoryDetails[15] = "DN";
                CategoryDetails[41] = strstartdate;
                CategoryDetails[42] = strenddate;
                CategoryDetails[43] = strstarttime;
                CategoryDetails[44] = strendtime;
                if (hdfSCID.Value == "0")
                {
                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' ").Tables[0];
                    if (dttailTable.Rows.Count > 0)
                    {
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                    }

                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *, isnull(noofseats,0) as noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' and pricingcompleted='1'").Tables[0];
                    if (dttailTableEx.Rows.Count > 0)
                    {
                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[35] = "1";
                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                    }
                    else
                    {
                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                        CategoryDetails[35] = "0";
                        CategoryDetails[33] = "1";
                        CategoryDetails[40] = "0";
                        CategoryDetails[14] = "0";
                    }
                }
                else
                {
                    if (lbloldtail.Text != txtTailNo.Value)
                    {
                        DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + lbloldtail.Text + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
                        if (dttail.Rows.Count > 0)
                        {
                            CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
                            CategoryDetails[35] = "1";
                            CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
                        }
                        else
                        {
                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                            CategoryDetails[35] = "0";
                            CategoryDetails[33] = "1";
                        }
                    }
                    else
                    {
                        DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + txtTailNo.Value + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
                        if (dttail.Rows.Count > 0)
                        {
                            CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
                            CategoryDetails[35] = "1";
                            CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
                        }
                        else
                        {
                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                            CategoryDetails[35] = "0";
                            CategoryDetails[33] = "1";
                        }
                    }
                }

                CategoryDetails[16] = "0.00";
                CategoryDetails[16] = "0.00";
                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                if (chkEmpty.Checked)
                {
                    CategoryDetails[19] = "1";
                }
                else
                {
                    CategoryDetails[19] = "0";
                }
                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                TimeSpan ts = TimeSpan.Parse(s);
                CategoryDetails[31] = ts.ToString();
                CategoryDetails[32] = DateTime.Now.Date.ToString();
                if (hdfSCID.Value == "0")
                {
                    CategoryDetails[34] = txttrip.Value;
                }
                else
                {
                    CategoryDetails[34] = txttrip.Value;
                }
                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                CategoryDetails[37] = DateTime.Now.Date.ToString();
                CategoryDetails[38] = LBLTRIPNO.Text;
                CategoryDetails[39] = lbloldtail.Text;

                CategoryDetails[48] = txtMPrice.Value.Trim();

                if (lblgroup.Text == "Modify")
                {
                    CategoryDetails[49] = "MF";
                    CategoryDetails[0] = "CH";
                    if (lbllegdisplay.Text.Trim() == "" || lbllegdisplay.Text.Trim() == "0")
                    {
                        CategoryDetails[51] = "1";
                    }
                    else
                    {
                        CategoryDetails[51] = lbllegdisplay.Text.Trim();
                    }

                    DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from Tailsetup_tripDetails").Tables[0];
                    int ModifyGroupId = 1;
                    if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
                    {
                        if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
                        {
                            ModifyGroupId = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
                        }
                        else
                        {
                            ModifyGroupId = 1;
                        }
                    }

                    CategoryDetails[50] = ModifyGroupId.ToString();
                }
                else
                {
                    CategoryDetails[49] = "ET";
                    CategoryDetails[51] = "1";

                }

                string strReturnVal = saveCategory(CategoryDetails);

                if (strReturnVal != "0")
                {
                    lblalert.Text = "Empty leg details have been saved successfully";
                    mpealert.Show();
                    if (hdfSCID.Value == "0")
                    {
                        lblManEditGroupId.Text = strReturnVal;
                    }
                    else if (hdfSCID.Value != "0" && lblgroup.Text == "Modify")
                    {
                        lblManEditGroupId.Text = strReturnVal;
                    }
                   

                    if (gvDepAirport.Rows.Count > 0)
                    {
                        string strModFlag = string.Empty;
                        if (lblgroup.Text == "Modify")
                        {
                            strModFlag = "MF";
                        }
                        else
                        {
                            strModFlag = "ET";
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");

                        for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                        {
                            Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                            Label lblAir = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");
                            Label lblCreatedBy = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedBy");
                            Label lblCreatedOn = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedOn");

                            Label lblStateCode = (Label)gvDepAirport.Rows[i].FindControl("lblStateCode");
                            Label lblCountryCode = (Label)gvDepAirport.Rows[i].FindControl("lblCountryCode");
                            Label lblAirportCode = (Label)gvDepAirport.Rows[i].FindControl("lblAirportCode");

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Dep', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text + "','" + lblCountryCode.Text + "','" + lblAirportCode.Text + "')");
                        }
                    }

                    if (gvArrivalAirport.Rows.Count > 0)
                    {
                        string strModFlag = string.Empty;
                        if (lblgroup.Text == "Modify")
                        {
                            strModFlag = "MF";
                        }
                        else
                        {
                            strModFlag = "ET";
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");

                        for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                        {
                            Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                            Label lblAir = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");
                            Label lblCreatedBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedBy");
                            Label lblCreatedOn = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedOn");

                            Label lblStateCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateCode");
                            Label lblCountryCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblCountryCode");
                            Label lblAirportCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblAirportCode");

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Arr', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text + "','" + lblCountryCode.Text + "','" + lblAirportCode.Text + "')");
                        }
                    }

                }
                if (hdfSCID.Value != "0")
                {
                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set Active='" + stractive + " ',Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "'  and tripno='" + LBLTRIPNO.Text + "'");
                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update bookeddetails set Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "' and tripno  ='" + LBLTRIPNO.Text + "'  ");
                    DataTable dtPScheck = new DataTable();
                    if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
                    {
                        string strstartcity1 = txtStartCity.Text.Replace(" - ", "-");
                        String[] strliststart = strstartcity1.Split('-');

                        string strendcity1 = txtEndCity.Text.Replace(" - ", "-");
                        String[] strlistend = strendcity1.Split('-');
                        dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "' and Flag='FINAL'").Tables[0];
                        if (dtPScheck.Rows.Count > 0)
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "'  and FLAG='FINAL'");
                        }
                    }

                    else
                    {
                        dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "' and Flag='FINAL'").Tables[0];

                        if (dtPScheck.Rows.Count > 0)
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "'  and FLAG='FINAL'");
                        }
                    }

                }
            }

            #endregion

            #region Split Insert

            else
            {
                string strBusinessAlert = string.Empty;

                if (rblBreak.SelectedValue.Trim() == "CO")
                {
                    if (txtcountry.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Start county is required. <br/>";
                    }
                    if (ddlLeg1EndCity.Text.ToString() == "")
                    {
                        strBusinessAlert += "- End city is required. <br/>";
                    }
                    if (txtCouStartTime.Value.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Start Time is required. <br/>";
                    }
                    if (txtSplitLeg1EndTime.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- End Time is required. <br/>";
                    }
                    if (txtSpliFlyHour.Value.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Flying Hour is required. <br/>";
                    }
                }
                else if (rblBreak.SelectedValue.Trim() == "CT")
                {
                    if (gvLegDetails.Rows.Count == 0)
                    {
                        strBusinessAlert = "Please add leg";
                    }
                }
                if (strBusinessAlert.Length > 0)
                {
                    lblalert.Text = strBusinessAlert;
                    mpealert.Show();
                    return;
                }


                #region Split New Insert

                if (btnSave.Text == "Save")
                {
                    int groupid = 1;
                    decimal daystoflight = 0; decimal L10_minPrice = 0; decimal M10_maxPrice = 0; decimal price = 0;



                    var HourlyRate = Convert.ToDecimal("0.00");
                    var fixedCeiling = Convert.ToDecimal("0.00");
                    var fixedFloor = Convert.ToDecimal("0.00");
                    var D4_MinTimeBD = Convert.ToDecimal("0.0");
                    var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                    var MinFLHSellable = Convert.ToDecimal("0.00");
                    var finalPricevalidation2 = (decimal)0;
                    var _totalDaysK101 = "0";

                    string stractive = string.Empty;
                    stractive = "1";

                    if (hdfSCID.Value == "0")
                    {
                        CategoryDetails[0] = "I";
                    }
                    else
                    {
                        CategoryDetails[0] = "U";
                    }
                    string atctivetail = string.Empty;

                    atctivetail = "1";

                    DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from Tailsetup_tripDetails").Tables[0];

                    if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
                    {
                        if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
                        {
                            groupid = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
                        }
                        else
                        {
                            groupid = 1;
                        }
                    }

                    if (rblBreak.SelectedValue.Trim() == "CO")
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CO',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");

                        if (lblbaseflag.Text.Trim() != "B0")
                        {
                            var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + txtCouStartTime.Value.Trim() + ":00");
                            var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                            var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                            var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value + ":00").Subtract(DateTime.Now);
                            _totalDaysK101 = K10_totalDays1.TotalDays.ToString("0.00");

                            string strstartcity = string.Empty;
                            strstartcity = ddlLeg1EndCity.Text.Replace(" - ", "-");
                            String[] strlistend = strstartcity.Split('-');
                            string startAirport = string.Empty;
                            string startcity = string.Empty;
                            if (strlistend.Length > 1)
                            {
                                startAirport = strlistend[1];
                                startcity = strlistend[0];
                            }

                            CategoryDetails[1] = hdfSCID.Value;
                            CategoryDetails[2] = txtTailNo.Value.Trim();
                            CategoryDetails[3] = txtTailNo.Value.Trim();
                            CategoryDetails[4] = txtcountry.Text;
                            CategoryDetails[5] = "";

                            CategoryDetails[6] = txtCouStartTime.Value.Trim();

                            CategoryDetails[7] = startAirport;
                            CategoryDetails[8] = startcity;
                            CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                            CategoryDetails[10] = "";
                            CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                            CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
                            CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
                            CategoryDetails[15] = "";
                            CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                            CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                            CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                            CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

                            if (hdfSCID.Value == "0")
                            {
                                DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                if (dttailTable.Rows.Count > 0)
                                {

                                }
                                else
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                }

                                DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                if (dttailTableEx.Rows.Count > 0)
                                {
                                    CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                    CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                    CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                    CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                    CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                    CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                    CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
                                    CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[35] = "1";
                                    CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                    CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                    CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                }
                                else
                                {
                                    CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                    CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                    CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                    CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                    CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                    CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                    CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
                                    CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                    CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                    CategoryDetails[35] = "0";
                                    CategoryDetails[33] = "1";
                                    CategoryDetails[40] = "0";
                                    CategoryDetails[14] = "0";
                                }
                            }
                            decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
                            CategoryDetails[16] = "0.00";
                            CategoryDetails[16] = "0.00";
                            CategoryDetails[30] = totalflyingHours.ToString("0.00");
                            if (cbSplitEmpty.Checked)
                            {
                                CategoryDetails[19] = "1";
                            }
                            else
                            {
                                CategoryDetails[19] = "0";
                            }
                            string s1 = DateTime.Now.ToString("HH:mm:ss.fff");

                            TimeSpan ts1 = TimeSpan.Parse(s1);
                            CategoryDetails[31] = ts1.ToString();
                            CategoryDetails[32] = DateTime.Now.Date.ToString();
                            if (hdfSCID.Value == "0")
                            {
                                CategoryDetails[34] = txttrip.Value.Trim();
                            }
                            else
                            {
                                CategoryDetails[34] = txttrip.Value.Trim();
                            }
                            CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                            CategoryDetails[37] = DateTime.Now.Date.ToString();
                            CategoryDetails[38] = txttrip.Value.Trim();
                            CategoryDetails[39] = txtTailNo.Value.Trim();
                            CategoryDetails[45] = "B1";
                            CategoryDetails[46] = groupid.ToString();
                            CategoryDetails[47] = lbllegdisplay.Text;
                            CategoryDetails[48] = rblBreak.SelectedValue.Trim();


                            string strReturnVal1 = saveCategoryGroup(CategoryDetails);
                        }
                        else
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");
                        }
                    }
                    if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");

                        for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                        {
                            if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
                            {
                                string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString();
                                var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + strStartTime.Trim() + ":00");
                                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                                var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                K10_totalDays1.TotalDays.ToString("0.00");

                                #region Leg 2 Insert Breakdown by City

                                var K10_totalDays2 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                var _totalDaysK102 = K10_totalDays2.TotalDays.ToString("0.00");

                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
                                CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
                                CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {

                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }

                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString()).ToString("0.00");
                                if (chkEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s2 = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts2 = TimeSpan.Parse(s2);
                                CategoryDetails[31] = ts2.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString();
                                CategoryDetails[46] = groupid.ToString();
                                CategoryDetails[47] = lbllegdisplay.Text;
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                string strReturnVal2 = saveCategoryGroup(CategoryDetails);

                                #endregion
                            }
                        }
                    }
                }

                #endregion

                #region  Update Split Trip

                else
                {
                    // DataTable dtOutput = (DataTable)ViewState["Tripgrid"];

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + lblgroupiddisplay.Text + "' and Tripflag<>'B0' ");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + lblgroupiddisplay.Text + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");

                    if (lblbaseflag.Text != "B0")
                    {
                        if (rblBreak.SelectedValue.Trim() == "CO")
                        {
                            #region Breakdown by Country

                            DataTable dtexist = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='B1' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

                            if (dtexist.Rows.Count > 0)
                            {
                                decimal daystoflight = 0;
                                decimal L10_minPrice = 0;
                                decimal M10_maxPrice = 0;
                                decimal price = 0;

                                decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
                                var HourlyRate = Convert.ToDecimal("0.00"); ;
                                var fixedCeiling = Convert.ToDecimal("0.00");
                                var fixedFloor = Convert.ToDecimal("0.00");
                                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                var MinFLHSellable = Convert.ToDecimal("0.00");
                                var finalPricevalidation2 = (decimal)0;
                                var _totalDaysK10 = "0";

                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }
                                else
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }


                                string stractive = string.Empty;
                                stractive = "1";
                                CategoryDetails[0] = "U";
                                string atctivetail = string.Empty;
                                atctivetail = "1";

                                string strEndCity = ddlLeg1EndCity.Text.Replace(" - ", "-");
                                String[] strlistend = strEndCity.Split('-');
                                string endAirport = string.Empty;
                                string endCity = string.Empty;
                                if (strlistend.Length > 1)
                                {
                                    endAirport = strlistend[1];
                                    endCity = strlistend[0];
                                }
                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                if (rblBreak.SelectedValue.Trim() == "CT")
                                {
                                    string strstartcity1 = string.Empty;
                                    strstartcity1 = ddlStartCitySplit.Text.Replace(" - ", "-");
                                    String[] strliststart1 = strstartcity1.Split('-');
                                    string startAirport1 = string.Empty;
                                    string startcity1 = string.Empty;
                                    if (strliststart1.Length > 1)
                                    {
                                        startAirport1 = strliststart1[1];
                                        startcity1 = strliststart1[0];

                                        CategoryDetails[4] = startAirport1;
                                        CategoryDetails[5] = startcity1;
                                    }
                                    else
                                    {

                                        CategoryDetails[4] = ddlStartCitySplit.Text;
                                        CategoryDetails[5] = "";
                                    }


                                }
                                else
                                {
                                    CategoryDetails[4] = txtcountry.Text;
                                    CategoryDetails[5] = "";
                                }
                                if (rblBreak.SelectedValue == "CO")
                                {
                                    CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
                                }

                                // CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                CategoryDetails[7] = endAirport;
                                CategoryDetails[8] = endCity;
                                CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                                CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
                                CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
                                //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                if (cbSplitEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts = TimeSpan.Parse(s);
                                CategoryDetails[31] = ts.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = "B1";
                                CategoryDetails[46] = lblgroupiddisplay.Text;
                                CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();


                                string strReturnVal = saveCategoryGroup(CategoryDetails);
                            }
                            else
                            {
                                decimal daystoflight = 0;
                                decimal L10_minPrice = 0;
                                decimal M10_maxPrice = 0;
                                decimal price = 0;
                                decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());

                                var HourlyRate = Convert.ToDecimal("0.00"); ;
                                var fixedCeiling = Convert.ToDecimal("0.00");
                                var fixedFloor = Convert.ToDecimal("0.00");
                                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                var MinFLHSellable = Convert.ToDecimal("0.00");
                                var finalPricevalidation2 = (decimal)0;
                                var _totalDaysK10 = "0";
                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }
                                else
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }


                                string stractive = string.Empty;

                                stractive = "1";

                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[0] = "I";
                                }
                                else
                                {
                                    CategoryDetails[0] = "U";
                                }
                                string atctivetail = string.Empty;

                                atctivetail = "1";
                                string strEndCity = ddlLeg1EndCity.Text.Replace(" - ", "-");
                                String[] strlistend = strEndCity.Split('-');
                                string endAirport = string.Empty;
                                string endCity = string.Empty;
                                if (strlistend.Length > 1)
                                {
                                    endAirport = strlistend[1];
                                    endCity = strlistend[0];
                                }

                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                CategoryDetails[4] = txtcountry.Text.Trim();
                                CategoryDetails[5] = "";
                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
                                }
                                CategoryDetails[7] = endAirport.Trim();
                                CategoryDetails[8] = endCity.Trim();
                                CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                                CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
                                CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
                                //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }

                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                if (cbSplitEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts = TimeSpan.Parse(s);
                                CategoryDetails[31] = ts.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = "B1";
                                CategoryDetails[46] = lblgroupiddisplay.Text;
                                CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                string strReturnVal = saveCategoryGroup(CategoryDetails);
                            }


                            #endregion
                        }

                        #region Breakdown by City

                        else if (rblBreak.SelectedValue.Trim() == "CT")
                        {
                            if (gvLegDetails.Rows.Count > 0)
                            {
                                for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                                {
                                    string strTriplag = gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim();

                                    DataTable dtexist1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='" + strTriplag + "' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

                                    #region Leg2

                                    if (dtexist1.Rows.Count > 0)
                                    {
                                        decimal daystoflight = 0;
                                        decimal L10_minPrice = 0;
                                        decimal M10_maxPrice = 0;
                                        decimal price = 0;

                                        decimal totalflyingHours = Convert.ToDecimal(gvLegDetails.Rows[i].FindControl("lblFlyHours").ToString().Trim());
                                        var HourlyRate = Convert.ToDecimal("0.00"); ;
                                        var fixedCeiling = Convert.ToDecimal("0.00");
                                        var fixedFloor = Convert.ToDecimal("0.00");
                                        var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                        var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                        var MinFLHSellable = Convert.ToDecimal("0.00");
                                        var finalPricevalidation2 = (decimal)0;
                                        string strStartTime = gvLegDetails.Rows[i].FindControl("lblstarttime").ToString().Trim();
                                        var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
                                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                                        string stractive = string.Empty;
                                        stractive = "1";
                                        CategoryDetails[0] = "U";
                                        string atctivetail = string.Empty;
                                        atctivetail = "1";

                                        CategoryDetails[1] = hdfSCID.Value;
                                        CategoryDetails[2] = txtTailNo.Value.Trim();
                                        CategoryDetails[3] = txtTailNo.Value.Trim();
                                        CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                        CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                        CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                        CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                        CategoryDetails[10] = "";
                                        CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
                                        CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
                                        CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
                                        CategoryDetails[15] = "";
                                        CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                        CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                        if (hdfSCID.Value == "0")
                                        {
                                            DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                            if (dttailTable.Rows.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                            }

                                            DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                            if (dttailTableEx.Rows.Count > 0)
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[35] = "1";
                                                CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                                CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                                CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                            }
                                            else
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                                CategoryDetails[35] = "0";
                                                CategoryDetails[33] = "1";
                                                CategoryDetails[40] = "0";
                                                CategoryDetails[14] = "0";
                                            }
                                        }
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                        if (cbSplitEmpty.Checked)
                                        {
                                            CategoryDetails[19] = "1";
                                        }
                                        else
                                        {
                                            CategoryDetails[19] = "0";
                                        }
                                        string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                        TimeSpan ts = TimeSpan.Parse(s);
                                        CategoryDetails[31] = ts.ToString();
                                        CategoryDetails[32] = DateTime.Now.Date.ToString();
                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                        CategoryDetails[37] = DateTime.Now.Date.ToString();
                                        CategoryDetails[38] = txttrip.Value.Trim();
                                        CategoryDetails[39] = txtTailNo.Value.Trim();
                                        CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
                                        CategoryDetails[46] = lblgroupiddisplay.Text;
                                        CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                        CategoryDetails[48] = rblBreak.SelectedValue.Trim();

                                        string strReturnVal = saveCategoryGroup(CategoryDetails);
                                    }
                                    else
                                    {
                                        decimal daystoflight = 0;
                                        decimal L10_minPrice = 0;
                                        decimal M10_maxPrice = 0;
                                        decimal price = 0;
                                        decimal totalflyingHours = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim());

                                        var HourlyRate = Convert.ToDecimal("0.00"); ;
                                        var fixedCeiling = Convert.ToDecimal("0.00");
                                        var fixedFloor = Convert.ToDecimal("0.00");
                                        var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                        var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                        var MinFLHSellable = Convert.ToDecimal("0.00");
                                        var finalPricevalidation2 = (decimal)0;
                                        string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString().Trim();

                                        var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
                                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");


                                        string stractive = string.Empty;

                                        stractive = "1";

                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[0] = "I";
                                        }
                                        else
                                        {
                                            CategoryDetails[0] = "U";
                                        }
                                        string atctivetail = string.Empty;

                                        atctivetail = "1";
                                        CategoryDetails[1] = hdfSCID.Value;
                                        CategoryDetails[2] = txtTailNo.Value.Trim();
                                        CategoryDetails[3] = txtTailNo.Value.Trim();
                                        CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                        CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                        CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                        CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                        CategoryDetails[10] = "";
                                        CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim();
                                        CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
                                        CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
                                        CategoryDetails[15] = "";
                                        CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                        CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                        if (hdfSCID.Value == "0")
                                        {
                                            DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                            if (dttailTable.Rows.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                            }

                                            DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                            if (dttailTableEx.Rows.Count > 0)
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[35] = "1";
                                                CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                                CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                                CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                            }
                                            else
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                                CategoryDetails[35] = "0";
                                                CategoryDetails[33] = "1";
                                                CategoryDetails[40] = "0";
                                                CategoryDetails[14] = "0";
                                            }
                                        }

                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                        if (chkEmpty.Checked)
                                        {
                                            CategoryDetails[19] = "1";
                                        }
                                        else
                                        {
                                            CategoryDetails[19] = "0";
                                        }
                                        string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                        TimeSpan ts = TimeSpan.Parse(s);
                                        CategoryDetails[31] = ts.ToString();
                                        CategoryDetails[32] = DateTime.Now.Date.ToString();
                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                        CategoryDetails[37] = DateTime.Now.Date.ToString();
                                        CategoryDetails[38] = txttrip.Value.Trim();
                                        CategoryDetails[39] = txtTailNo.Value.Trim();
                                        CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
                                        CategoryDetails[46] = lblgroupiddisplay.Text;
                                        CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                        CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                        string strReturnVal = saveCategoryGroup(CategoryDetails);
                                    }

                                    #endregion
                                }
                            }
                        }

                        #endregion
                    }
                }

                #endregion
            }

            #endregion

            #region Livepricecalculation
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set flag='OLD'");
            string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
            string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", false);
            saveCategoryParam[0].Value = strStartDate;
            saveCategoryParam[1].Value = strEndDate;
            DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", saveCategoryParam).Tables[0];
            DataTable dtMerge = dtOneWayTrip.Clone();
            if (dtOneWayTrip.Rows.Count > 0)
            {
                for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                {
                    decimal daystoflightF = 0;
                    decimal L10_minPriceF = 0;
                    decimal M10_maxPriceF = 0;
                    decimal priceF = 0;

                    string tailNo = dtOneWayTrip.Rows[i]["TailNo"].ToString();
                    string noOfSeats = dtOneWayTrip.Rows[i]["NoOFSeats"].ToString();
                    var flyingDate = dtOneWayTrip.Rows[i]["DDate"].ToString();
                    var flyingtoDate = dtOneWayTrip.Rows[i]["ADate"].ToString();
                    var strtTime = dtOneWayTrip.Rows[i]["DTime"].ToString();

                    var endTime = dtOneWayTrip.Rows[i]["ATime"].ToString();
                    var flyingHours = dtOneWayTrip.Rows[i]["FH"].ToString();
                    //  var flyingHours = "1:18";
                    string startCity = dtOneWayTrip.Rows[i]["StartCity"].ToString();
                    string endCity = dtOneWayTrip.Rows[i]["EndCity"].ToString();
                    string startAirport = dtOneWayTrip.Rows[i]["Start"].ToString();
                    string endAirport = dtOneWayTrip.Rows[i]["End"].ToString();

                    var HourlyRateF = dtOneWayTrip.Rows[i]["HourlyRate"].ToString();
                    var fixedCeilingF = dtOneWayTrip.Rows[i]["FixedCeiling"].ToString();
                    var fixedFloorF = dtOneWayTrip.Rows[i]["FixedFloor"].ToString();
                    var D4_MinTimeBDF = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MinTimeBD"].ToString());
                    var D5_MaxTimeBDF = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MaxTimeBD"].ToString());
                    var MinFLHSellableF = dtOneWayTrip.Rows[i]["MinFLHSellable"].ToString();
                    var applytax = dtOneWayTrip.Rows[i]["applytax"].ToString();
                    var tripno = dtOneWayTrip.Rows[i]["tripno"].ToString();
                    var enabletrip = dtOneWayTrip.Rows[i]["enabletrip"].ToString();
                    var showtrip = dtOneWayTrip.Rows[i]["Active"].ToString();
                    var TailFlag = dtOneWayTrip.Rows[i]["TailFlag"].ToString();
                    var createdby = dtOneWayTrip.Rows[i]["createdby"].ToString();
                    var createdon = dtOneWayTrip.Rows[i]["createdon"].ToString();
                    var tripflag = dtOneWayTrip.Rows[i]["tripflag"].ToString();
                    var groupid = dtOneWayTrip.Rows[i]["groupid"].ToString();
                    var FosModifiedFlag = dtOneWayTrip.Rows[i]["ModifyFlag"].ToString();
                    var leg = dtOneWayTrip.Rows[i]["leg"].ToString();

                    var NewStartAirport = dtOneWayTrip.Rows[i]["NewStart"].ToString();
                    var NewEndAirport = dtOneWayTrip.Rows[i]["NewEnd"].ToString();

                    if (leg.ToString() == "" || leg.ToString() == "0")
                    {
                        leg = "1";
                    }
                    var splitype = dtOneWayTrip.Rows[i]["Splittype"].ToString();

                    if (dtOneWayTrip.Rows[i]["HourlyRate"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedCeiling"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedFloor"].ToString() != "0.00")
                    {
                        var dtFlyingDaysF = Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00";

                        var K10_totalDaysF = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00").Subtract(DateTime.Now);

                        DateTime startTime = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00");
                        DateTime endtime1 = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + endTime + ":00");
                        var duration = endtime1.Subtract(startTime);
                        var _totalDaysK10F = K10_totalDaysF.TotalDays.ToString("0.00");
                        decimal totalhrs = 0;
                        if (flyingHours != "")
                        {
                            totalhrs = Convert.ToDecimal(flyingHours);
                        }


                        long durationTicks = Math.Abs(duration.Ticks / TimeSpan.TicksPerMillisecond);
                        long hours = durationTicks / (1000 * 60 * 60);
                        long minutes = (durationTicks - (hours * 60 * 60 * 1000)) / (1000 * 60);

                        string CBPFF = string.Empty;
                        string exactFlyinghrsF = hours.ToString("00") + ":" + minutes.ToString("00");

                        L10_minPriceF = (Convert.ToDecimal(HourlyRateF) * Convert.ToDecimal(totalhrs));

                        if (L10_minPriceF < Convert.ToDecimal(fixedFloorF))
                        {
                            L10_minPriceF = Convert.ToDecimal(fixedFloorF);
                            CBPFF = "FIXEDCEILING";
                        }

                        M10_maxPriceF = (Convert.ToDecimal(HourlyRateF) * Convert.ToDecimal(totalhrs)) + Convert.ToDecimal(fixedCeilingF);


                        var finalPricevalidation2F = (decimal)0;

                        var finalPriceF = Convert.ToInt32(((1 - (D5_MaxTimeBDF - Convert.ToDecimal(_totalDaysK10F)) / (D5_MaxTimeBDF - Convert.ToDecimal(D4_MinTimeBDF))) * (M10_maxPriceF - L10_minPriceF) + L10_minPriceF) / 25) * 25;

                        int mul1F = 0;
                        if (Convert.ToDecimal(_totalDaysK10F) < D4_MinTimeBDF)
                        {
                            mul1F = 0;
                        }
                        else
                            mul1F = 1;

                        string mul2F = "0";
                        if (Convert.ToDecimal(Convert.ToDecimal(totalhrs)) >= Convert.ToDecimal(MinFLHSellableF))
                        {
                            mul2F = "1";
                        }
                        else
                            mul2F = "ERROR";



                        if (mul2F != "ERROR")
                        {
                            finalPricevalidation2F = Convert.ToDecimal(finalPriceF) * Convert.ToDecimal(mul2F) * Convert.ToDecimal(mul1F);
                        }

                        string Specialflag = string.Empty;
                        DataTable dtspecialpricing = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from SpeicalPricing").Tables[0];
                        DataRow[] drspecial = dtspecialpricing.Select("#" + flyingDate + "# >= StartDate AND EndDate >= #" + flyingDate + "#" + "and startAirport='" + startAirport + "' and endairport ='" + endAirport + "'");
                        DataTable dtselection = new DataTable();
                        if (drspecial.Length > 0)
                        {
                            dtselection = drspecial.CopyToDataTable();
                            if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "H")
                            {
                                finalPricevalidation2F = Convert.ToDecimal(flyingHours) * Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                                Specialflag = "(Special Price)";
                            }
                            else if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "D")
                            {
                                finalPricevalidation2F = Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                                Specialflag = "(Special Price)";
                            }
                        }


                        if (FosModifiedFlag.ToString() == "ET")
                        {
                            DataTable dtManualPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Mprice from OneWayTail_Setup where TailNO='" + tailNo.ToString().Trim() + "' and TripNo='" + tripno.ToString().Trim() + "' and startAirport='" + startAirport.ToString().Trim() + "' and EndAirport='" + endAirport.ToString().Trim() + "' ").Tables[0];

                            if (dtManualPrice.Rows.Count > 0)
                            {
                                if (Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString()) > 3500)
                                {
                                    finalPricevalidation2F = Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString());
                                }

                            }
                        }
                        if (FosModifiedFlag.ToString() == "MF")
                        {
                            DataTable dtEditPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Mod_Price from Tailsetup_tripDetails where TailNO='" + tailNo.ToString().Trim() + "' and TripNo='" + tripno.ToString().Trim() + "' and GroupID='" + groupid + "' and GroupID<>0").Tables[0];

                            if (dtEditPrice.Rows.Count > 0)
                            {
                                if (Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString()) > 3500)
                                {
                                    finalPricevalidation2F = Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString());
                                }
                            }
                        }
                        string sF = DateTime.Now.ToString("HH:mm:ss.fff");
                        TimeSpan tsF = TimeSpan.Parse(sF);
                        string time = tsF.ToString();
                        string Date = DateTime.Now.Date.ToString();
                        if (Specialflag == "(Special Price)")
                        {
                            DataTable dtspecialCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, " select * from LivePricingCaculation Where flag='FINAL' and Priceflag='(Special Price)' and TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' ").Tables[0];

                            if (dtspecialCheck.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  LivePricingCaculation values (" +
                                                                                                                                                  " '" + tailNo + "'," +
                                                                                                                                                   " '" + flyingDate + "'," +
                                                                                                                                                    " '" + flyingtoDate + "'," +
                                                                                                                                                     " '" + strtTime + "'," +
                                                                                                                                                      " '" + endTime + "'," +
                                                                                                                                                       " '" + startAirport + "'," +
                                                                                                                                                        " '" + startCity.Replace("'", "''") + "'," +
                                                                                                                                                          " '" + endAirport + "'," +
                                                                                                                                                            " '" + endCity.Replace("'", "''") + "'," +
                                                                                                                                                             " '" + noOfSeats + "'," +
                                                                                                                                                              " '" + totalhrs.ToString() + "'," +
                                                                                                                                                                " '" + totalhrs.ToString() + "'," +
                                                                                                                                                                " '" + HourlyRateF + "'," +
                                                                                                                                                                  " '" + fixedFloorF + "'," +
                                                                                                                                                                  " '" + fixedCeilingF + "'," +
                                                                                                                                                                     " '" + D4_MinTimeBDF + "'," +
                                                                                                                                                                       " '" + D5_MaxTimeBDF + "'," +
                                                                                                                                                                        " '" + MinFLHSellableF + "'," +
                                                                                                                                                                            " '" + _totalDaysK10F + "'," +

                                                                                                                                                                 " '" + L10_minPriceF + "'," +
                                                                                                                                                                  " '" + M10_maxPriceF + "'," +
                                                                                                                                                                  " '" + finalPricevalidation2F + "'," +
                                                                                                                                                                    " '" + time + "'," +
                                                                                                                                                                     " '" + Date + "'," +
                                                                                                                                                                       " 'FINAL'," +
                                                                                                                                                                         " '" + CBPFF + "','0','" + applytax + "','" + tripno + "','" + enabletrip + "','" + showtrip + "','" + Specialflag + "','" + TailFlag + "','" + createdby + "','" + createdon + "','" + tripflag + "','" + groupid + "','" + FosModifiedFlag + "','" + leg + "','" + splitype + "','" + NewStartAirport.Replace("'", "''") + "','" + NewEndAirport.Replace("'", "''") + "') ");

                                if (finalPricevalidation2F > 0)
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='NEW' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                                }
                                else
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='EXP' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                                }
                            }

                        }
                        else
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  LivePricingCaculation values (" +
                                                                                                                                              " '" + tailNo + "'," +
                                                                                                                                               " '" + flyingDate + "'," +
                                                                                                                                                " '" + flyingtoDate + "'," +
                                                                                                                                                 " '" + strtTime + "'," +
                                                                                                                                                  " '" + endTime + "'," +
                                                                                                                                                   " '" + startAirport + "'," +
                                                                                                                                                    " '" + startCity.Replace("'", "''") + "'," +
                                                                                                                                                      " '" + endAirport + "'," +
                                                                                                                                                        " '" + endCity.Replace("'", "''") + "'," +
                                                                                                                                                         " '" + noOfSeats + "'," +
                                                                                                                                                          " '" + totalhrs.ToString() + "'," +
                                                                                                                                                            " '" + totalhrs.ToString() + "'," +
                                                                                                                                                            " '" + HourlyRateF + "'," +
                                                                                                                                                              " '" + fixedFloorF + "'," +
                                                                                                                                                              " '" + fixedCeilingF + "'," +
                                                                                                                                                                 " '" + D4_MinTimeBDF + "'," +
                                                                                                                                                                   " '" + D5_MaxTimeBDF + "'," +
                                                                                                                                                                    " '" + MinFLHSellableF + "'," +
                                                                                                                                                                        " '" + _totalDaysK10F + "'," +

                                                                                                                                                             " '" + L10_minPriceF + "'," +
                                                                                                                                                              " '" + M10_maxPriceF + "'," +
                                                                                                                                                              " '" + finalPricevalidation2F + "'," +
                                                                                                                                                                " '" + time + "'," +
                                                                                                                                                                 " '" + Date + "'," +
                                                                                                                                                                   " 'FINAL'," +
                                                                                                                                                                     " '" + CBPFF + "','0','" + applytax + "','" + tripno + "','" + enabletrip + "','" + showtrip + "','" + Specialflag + "','" + TailFlag + "','" + createdby + "','" + createdon + "','" + tripflag + "','" + groupid + "','" + FosModifiedFlag + "','" + leg + "','" + splitype + "','" + NewStartAirport.Replace("'", "''") + "','" + NewEndAirport.Replace("'", "''") + "') ");

                            if (finalPricevalidation2F > 0)
                            {
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='NEW' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                            }
                            else
                            {
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='EXP' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                            }
                        }
                    }
                }
            }
            #endregion

            lblalert.Text = "Empty leg details have been saved successfully";
            mpealert.Show();
            hdfSCID.Value = "0";
            BindEmptyLegs();
            tblForm.Visible = false;
            tblGrid.Visible = true;
            rblStatus.Visible = true;
            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void gvTailDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ViewState["dtLegDetails"] = null;
            gvLegDetails.DataSource = null;
            gvLegDetails.DataBind();

            ViewState["dtDepAirport"] = null;
            gvDepAirport.DataSource = null;
            gvDepAirport.DataBind();

            ViewState["dtArrAirport"] = null;
            gvArrivalAirport.DataSource = null;
            gvArrivalAirport.DataBind();

            trSplit.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            btnAdd.Visible = false;
            rblStatus.Visible = false;
            trManualandModiy.Visible = false;
            divModify.Visible = true;
            divManualAirport.Visible = true;
            Label TailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
            Label lbltrip = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
            Label lblType = (Label)gvTailDetails.SelectedRow.FindControl("lblType");
            Label lblGroupId = (Label)gvTailDetails.SelectedRow.FindControl("lblGroupId");
            Loadcountry();

            #region

            if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text == "0")
            {
                tbldata.Visible = true;
                divPriceModRight.Visible = true;
                divPricModLeft.Visible = true;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                lblSubScren.Text = "Modify a FOS Trip";
                trManualandModiy.Visible = true;
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                LBLTRIPNO.Text = lbltripnoG.Text.Trim();

                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                lbllegdisplay.Text = lblleg.Text;

                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lblTripData.Text;
                txtFlyHours1.Value = lblFlyingHours.Text;
                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");

                txtStartTime.Value = lblStarttime.Text;
                txtEndTime.Value = lblEndTime.Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:GetDepAirport();", true);
                txtstartsplit.Value = lblstartcity.Text + " - " + lblstart.Text;

                txtEndCity.Text = lblEndcity.Text + " - " + lblend.Text;

                txtStartCity.Text = lblstartcity.Text + " - " + lblstart.Text;

                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                txtAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");

                //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
                //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

                //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
                //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

                lblgroup.Text = "Modify";
                txtTailNo.Disabled = true;
                txttrip.Disabled = true;
                tbldata.Visible = true;
                divstartcity.Visible = true;
                divstartcityLeft.Visible = true;
                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplit.Visible = false;
                divstartsplitLeft.Attributes.Add("style", "display:none");
                txtstartsplit.Disabled = true;
                divendcity.Visible = true;
                btnSave.Visible = true;
                tblForm.Visible = true;
                tblGrid.Visible = false;
            }

            #endregion

            #region Edited Split

            else if (lblType.Text.Trim() == "Split" && lblGroupId.Text != "0")
            {
                lblSubScren.Text = "Add a Manual Leg";
                divPriceModRight.Visible = false;
                divPricModLeft.Visible = false;
                trManualandModiy.Visible = false;
                DataTable dtPS = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag<>'B0' Order by groupid").Tables[0];

                lblgroupiddisplay.Text = lblGroupId.Text.Trim();

                trSplit.Visible = true;
                tbldata.Visible = true;
                divstartcounty.Attributes.Add("style", "display:''");
                divstartsplitLeft.Attributes.Add("style", "display:none");
                trLeg1.Attributes.Add("style", "display:none");
                trLeg2.Attributes.Add("style", "display:none");
                if (dtPS != null && dtPS.Rows.Count > 0)
                {
                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");


                    //  lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;

                    //lblupdatedon.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text;


                    txtSpliFlyHour.Value = lblFlyingHours.Text;
                    txtNoSeats.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                    txtSpliFlyHour.Value = lblFlyingHours.Text;
                    txtMPrice.Value = dtPS.Rows[0]["CurrentPrice"].ToString();

                    lblTailData.Text = lblTailNo.Text;
                    lblTripData.Text = lbltripnoG.Text;
                    lblStartData.Text = lblstart.Text;
                    lblendData.Text = lblend.Text;
                    lblscitydata.Text = lblstartcity.Text;
                    lblEcityData.Text = lblEndcity.Text;
                    lblStimedata.Text = lblStarttime.Text;
                    lblEtimeData.Text = lblEndTime.Text;
                    lblDateData.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MMM dd, yyyy").ToUpper();
                    lblstartdate.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MM-dd-yyyy");
                    lblenddate.Text = Convert.ToDateTime(dtPS.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
                    lbllegdisplay.Text = lblleg.Text;

                    txtTailNo.Value = lblTailNo.Text;
                    txttrip.Value = lbltripnoG.Text;
                    LBLTRIPNO.Text = txttrip.Value.Trim();

                    txtcountry.Text = string.Empty;
                    DataTable dtbase2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
                    if (dtbase2.Rows.Count > 0)
                    {
                        if (dtbase2.Rows[0]["tripflag"].ToString() == "B1")
                        {

                            DataTable dtPS2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag='B1' Order by groupid").Tables[0];
                            // txtStartCity.Text = dtPS2.Rows[0]["Endcity"].ToString() + " - " + dtPS2.Rows[0]["End"].ToString();
                            string strSpeed = string.Empty;
                            DataTable dtPS1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup Where TailNo='" + txtTailNo.Value + "'").Tables[0];
                            if (dtPS1.Rows.Count > 0)
                            {
                                strSpeed = dtPS1.Rows[0]["AvgSpeed"].ToString();
                            }
                            DataTable dtResul = new DataTable();
                            try
                            {

                                dtResul = AirRouteCalculation(dtPS2.Rows[0]["End"].ToString(), lblendData.Text, strSpeed);
                            }
                            catch (Exception ex)
                            {

                            }

                            if (dtResul.Rows.Count > 0)
                            {
                                txtSpliFlyHour.Value = (Math.Round(Convert.ToDecimal(dtResul.Rows[0]["flight_time_min"].ToString()) / 60, 2)).ToString();
                            }
                        }
                    }

                    DataTable dtpreference = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select TailNo as TailName,   TailNo, convert(varchar(12),DDate,109) DDate ,convert(varchar(12),ADate,109) ADate ,DDate [DateFormat],  CONVERT(VARCHAR(5), StartTime, 108) DTime,
		                                                CONVERT(VARCHAR(5), EndTime, 108)  ATime,Start+'-->'+[End] [routes], Start, StartCity, [End], EndCity, isnull(NoOFSeats,0) NoOFSeats,
		                                                FlyingHoursFOS, CalcualtedFlyingHrs FLTTIMEMINS,TripNo,
		                                                DaysToFlight,tripflag,Groupid,splittype from LivePricingCaculation where Groupid is not null and 
                                                        Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL'  Order by groupid").Tables[0];

                    if (dtpreference.Rows.Count > 0)
                    {
                        dtpreference.Columns.Add("leg");

                        for (int i = 0; i < dtpreference.Rows.Count; i++)
                        {
                            DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + dtpreference.Rows[i]["Tailno"].ToString() + @"' and tripno='" + dtpreference.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(dtpreference.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(dtpreference.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(dtpreference.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + lblstart.Text + @"'
                and EndAirport='" + lblend.Text + @"'").Tables[0];
                            if (dtleg.Rows.Count > 0)
                            {
                                dtpreference.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                            }
                        }

                        for (int i = 0; i < dtpreference.Rows.Count; i++)
                        {
                            if (i > 0)
                            {
                                dtpreference.Rows[i]["leg"] = dtpreference.Rows[0]["leg"].ToString();
                            }
                        }

                        ViewState["Tripgrid"] = dtpreference;

                        DataTable dt = new DataTable();
                        dtpreference.DefaultView.Sort = "Tripflag asc";
                        dt = dtpreference.DefaultView.ToTable();
                        rblBreak.SelectedValue = "CO";
                        DataRow[] result1 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag='B1' and splittype='CO'");

                        if (result1.Length > 0)
                        {
                            rblBreak.SelectedValue = "CO";
                            DataTable dttemp1 = result1.CopyToDataTable();
                            hdfSCID.Value = "0";
                            LBLTRIPNO.Text = dttemp1.Rows[0]["tripno"].ToString();
                            lbloldtail.Text = dttemp1.Rows[0]["tailno"].ToString();
                            txttrip.Value = dttemp1.Rows[0]["tripno"].ToString();
                            txtTailNo.Value = dttemp1.Rows[0]["TailNo"].ToString();
                            txtCouStartTime.Value = dttemp1.Rows[0]["DTime"].ToString();
                            txtSplitLeg1EndTime.Text = dttemp1.Rows[0]["ATime"].ToString();
                            txtSpliFlyHour.Value = dttemp1.Rows[0]["FlyinghoursFOS"].ToString().Replace(":", ".");

                            DataTable dtTake = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select Mod_AvailableTo,Mod_AvilableFrom,Mod_EndAirport,Mod_Endcity,Mod_StartAirport,Mod_Startcity from Tailsetup_tripDetails where Groupid='" + lblGroupId.Text.Trim() + "' and TripFlag='B1' and  ModifyFlag='SP'  and Mod_AvilableFrom!='1900-01-01 00:00:00.000'").Tables[0];

                            if (dtTake.Rows.Count > 0)
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");
                                ddlLeg1EndCity.Text = dtTake.Rows[0]["Mod_Endcity"].ToString() + " - " + dtTake.Rows[0]["Mod_EndAirport"].ToString();
                            }
                            else
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dttemp1.Rows[0]["DDate"].ToString()).ToString("MM-dd-yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dttemp1.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
                            }

                            txtNoSeats.Value = dttemp1.Rows[0]["NoOFSeats"].ToString();
                            txtcountry.Text = dttemp1.Rows[0]["Start"].ToString();
                            divstartcounty.Attributes.Add("style", "display:''");

                        }
                        DataRow[] result2 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag in ('B1','B2','B3') and splittype='CT' ");

                        if (result2.Length > 0)
                        {
                            divstartcounty.Attributes.Add("style", "display:none");
                            divstartsplitLeft.Attributes.Add("style", "display:''");
                            trLeg1.Attributes.Add("style", "display:''");
                            trLeg2.Attributes.Add("style", "display:''");
                            rblBreak.SelectedValue = "CT";


                            ddlStartCitySplit.Text = string.Empty;
                            ddlLeg1EndCity.Text = string.Empty;
                            txtSpliFlyHour.Value = string.Empty;
                            txtSplitLeg1StartTime.Text = string.Empty;
                            txtSplitLeg1EndTime.Text = string.Empty;

                            DataTable dttemp1 = result2.CopyToDataTable();
                            ViewState["dtLegDetails"] = dttemp1;
                            gvLegDetails.DataSource = dttemp1;
                            gvLegDetails.DataBind();
                            if (gvLegDetails.Rows.Count == 3)
                            {
                                btnAddLeg.Visible = false;
                            }
                            else
                            {
                                btnAddLeg.Visible = true;
                            }
                            hdfSCID.Value = "0";
                        }
                        else
                        {
                            ViewState["dtLegDetails"] = null;
                            gvLegDetails.DataSource = null;
                            gvLegDetails.DataBind();
                        }
                    }
                }
                DataTable dtbase = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
                if (dtbase.Rows.Count > 0)
                {
                    if (dtbase.Rows[0]["tripflag"].ToString() == "B0")
                    {
                        lblbaseflag.Text = "B1";
                    }
                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B1")
                    {
                        lblbaseflag.Text = "B2";
                    }
                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B2")
                    {
                        lblbaseflag.Text = "B3";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Split();", true);
                btnSave.Text = "Update";
                tblForm.Visible = true;
                tblGrid.Visible = false;
                lblgroup.Text = "Group";
            }

            #endregion

            #region New Split

            else if ((lblType.Text.Trim() == "Split" && lblGroupId.Text == "0"))
            {
                trSplit.Visible = true;
                divPriceModRight.Visible = false;
                divPricModLeft.Visible = false;
                lblSubScren.Text = "Add a Manual Leg";
                trManualandModiy.Visible = false;

                txtcountry.Text = string.Empty;
                rblBreak.SelectedValue = "CT";
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                LBLTRIPNO.Text = lbltripnoG.Text.Trim();
                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                lbllegdisplay.Text = lblleg.Text;

                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lbltripnoG.Text;
                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                hdnstarttime.Value = lblStarttime.Text;
                hdnendtime.Value = lblEndTime.Text;
                ddlStartCitySplit.Text = lblstartcity.Text + " - " + lblstart.Text;
                ddlStartCitySplit.Enabled = false;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
                // txtstartsplit.Value = lblstartcity.Text + " - " + lblstart.Text;
                // txtEndCity.Text = lblEndcity.Text + " - " + lblend.Text;
                //txtStartCity.Text = lblstartcity.Text + " - " + lblstart.Text;
                //txtLeng2EndCity.Text = lblEndcity.Text + " - " + lblend.Text;
                //ddlLeg2EndCity.SelectedItem.Text = lblEndcity.Text + " - " + lblend.Text;
                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                tbldata.Visible = true;


                btnSave.Visible = true;

                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplitLeft.Attributes.Add("style", "display:''");
                trLeg1.Attributes.Add("style", "display:''");
                trLeg2.Attributes.Add("style", "display:''");

                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
                saveCategoryParam[0].Value = 1;//MANAGETYPE
                saveCategoryParam[1].Value = 25;//Rowid
                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
                saveCategoryParam[3].Value = strEndDate;//Rowid
                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];

                DataTable dtAdvSearch = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM Airportmaster").Tables[0];
                if (dtOneWayTrip.Rows.Count > 0)
                {
                    for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtAdvSearch.Rows.Count; j++)
                        {
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["Start"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["End"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["End"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtOneWayTrip.Rows[i]["End"].ToString())
                            {
                                string strk = dtOneWayTrip.Rows[i]["routes"].ToString().Replace("-->", @"@");
                                String[] strlist = strk.Split(new[] { '@' });
                                dtOneWayTrip.Rows[i]["Start"] = strlist[0];
                                dtOneWayTrip.Rows[i]["End"] = strlist[1];
                            }
                        }
                    }
                }



                DataView dv = dtOneWayTrip.DefaultView;

                DataTable sortedDT = dv.ToTable();
                sortedDT.Columns.Add("leg");
                for (int i = 0; i < sortedDT.Rows.Count; i++)
                {
                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
                    String[] strlist = routes.Split(new[] { ',' });
                    string startD = strlist[0];
                    string EndD = strlist[1];
                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
                and EndAirport='" + EndD + @"'").Tables[0];
                    if (dtleg.Rows.Count > 0)
                    {
                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                    }
                }

                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
                DataTable dt = new DataTable();
                if (rowsFilteredConditionAND.Length > 0)
                {
                    dt = rowsFilteredConditionAND.CopyToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    ViewState["tripBase"] = dt;

                    lblbaseflag.Text = "B1";

                    lblgroup.Text = "Group";
                }

                tblForm.Visible = true;
                tblGrid.Visible = false;
                ddlLeg1EndCity.Text = string.Empty;
                txtSpliFlyHour.Value = string.Empty;
            }

            #endregion

            #region Modified Split

            else if (lblType.Text == "MSplit" && lblGroupId.Text != "0")
            {
                trSplit.Visible = true;
                divPriceModRight.Visible = false;
                divPricModLeft.Visible = false;
                lblSubScren.Text = "Add a Manual Leg";
                trManualandModiy.Visible = false;

                txtcountry.Text = string.Empty;
                rblBreak.SelectedValue = "CO";
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("Label1");
                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label2");
                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label1");

                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                if (lblleg.Text == "" || lblleg.Text == "0")
                {
                    lbllegdisplay.Text = "1";
                }
                else
                {
                    lbllegdisplay.Text = lblleg.Text;
                }


                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lbltripnoG.Text;
                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                LBLTRIPNO.Text = txttrip.Value.Trim();

                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                tbldata.Visible = true;


                btnSave.Visible = true;

                divstartcounty.Attributes.Add("style", "display:''");
                divstartsplitLeft.Attributes.Add("style", "display:none");
                trLeg1.Attributes.Add("style", "display:none");
                trLeg2.Attributes.Add("style", "display:none");

                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
                saveCategoryParam[0].Value = 1;//MANAGETYPE
                saveCategoryParam[1].Value = 25;//Rowid
                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
                saveCategoryParam[3].Value = strEndDate;//Rowid
                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];

                DataTable dtAdvSearch = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM Airportmaster").Tables[0];
                if (dtOneWayTrip.Rows.Count > 0)
                {
                    for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtAdvSearch.Rows.Count; j++)
                        {
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["Start"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["End"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["End"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtOneWayTrip.Rows[i]["End"].ToString())
                            {
                                string strk = dtOneWayTrip.Rows[i]["routes"].ToString().Replace("-->", @"@");
                                String[] strlist = strk.Split(new[] { '@' });
                                dtOneWayTrip.Rows[i]["Start"] = strlist[0];
                                dtOneWayTrip.Rows[i]["End"] = strlist[1];
                            }
                        }
                    }
                }



                DataView dv = dtOneWayTrip.DefaultView;

                DataTable sortedDT = dv.ToTable();
                sortedDT.Columns.Add("leg");
                for (int i = 0; i < sortedDT.Rows.Count; i++)
                {
                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
                    String[] strlist = routes.Split(new[] { ',' });
                    string startD = strlist[0];
                    string EndD = strlist[1];
                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
                and EndAirport='" + EndD + @"'").Tables[0];
                    if (dtleg.Rows.Count > 0)
                    {
                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                    }
                }

                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
                DataTable dt = new DataTable();
                if (rowsFilteredConditionAND.Length > 0)
                {
                    dt = rowsFilteredConditionAND.CopyToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    ViewState["tripBase"] = dt;


                }
                lblbaseflag.Text = "B1";

                lblgroup.Text = "Group";
                tblForm.Visible = true;
                tblGrid.Visible = false;
            }

            #endregion

            #region Modiied FOS
            else if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text != "0")
            {
                lblgroup.Text = "Modify";
                lblSubScren.Text = "Modify a FOS Trip";
                trManualandModiy.Visible = true;
                trSplit.Visible = false;
                tbldata.Visible = true;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  TailNO,TripNo,NoOfseats, Mod_AvilableFrom, Mod_AvailableTo, Mod_StartAirport, Mod_Startcity,Mod_EndAirport,Mod_Endcity,Mod_Starttime,Mod_Endtime,Mod_FlyingHours,Mod_Price,CreatedOn,Active,New_StartAirport,New_EndAirport  FROM  Tailsetup_tripDetails  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and GroupId='" + lblGroupId.Text.Trim() + "'").Tables[0];
                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
                {
                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                    lbllegdisplay.Text = lblleg.Text;
                    string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                    string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
                    DataTable dtGetData = GET_LIVEDATA("", strStartDate, strEndDate, lblTailNo.Text, lbltrip.Text);

                    if (dtGetData.Rows.Count > 0)
                    {
                        lblTailData.Text = dtGetData.Rows[0]["TailNo"].ToString();
                        lblTripData.Text = dtGetData.Rows[0]["TripNo"].ToString();
                        lblStartData.Text = dtGetData.Rows[0]["StartAirport"].ToString();
                        lblendData.Text = dtGetData.Rows[0]["EndAirport"].ToString();
                        lblscitydata.Text = dtGetData.Rows[0]["StartCity"].ToString();
                        lblEcityData.Text = dtGetData.Rows[0]["EndCity"].ToString();
                        lblStimedata.Text = dtGetData.Rows[0]["StartTime"].ToString();
                        lblEtimeData.Text = dtGetData.Rows[0]["Endtime"].ToString();
                        lblDateData.Text = Convert.ToDateTime(dtGetData.Rows[0]["AvailbleFrom"].ToString()).ToString("MMM dd, yyyy").ToUpper();
                    }

                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Dep'").Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        gvDepAirport.DataSource = dt;
                        gvDepAirport.DataBind();
                        ViewState["dtDepAirport"] = dt;
                        divDepLan.Visible = true;
                        txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
                    }
                    else
                    {
                        divDepLan.Visible = false;
                    }

                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Arr'").Tables[0];

                    if (dtArr.Rows.Count > 0)
                    {
                        gvArrivalAirport.DataSource = dtArr;
                        gvArrivalAirport.DataBind();
                        ViewState["dtArrAirport"] = dtArr;
                        txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
                        divArrLan.Visible = true;
                    }
                    else
                    {
                        divArrLan.Visible = false;
                    }

                    txtTailNo.Value = lblTailNo.Text;
                    txttrip.Value = lbltripnoG.Text;
                    txtFlyHours1.Value = lblFlyingHours.Text;
                    txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                    txtFlyHours1.Value = lblFlyingHours.Text;

                    txtStartTime.Value = lblStarttime.Text;
                    txtEndTime.Value = lblEndTime.Text;

                    hdfSCID.Value = "1";
                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["TripNo"].ToString();
                    lbloldtail.Text = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    txttrip.Value = dtOneWayTSel.Rows[0]["TripNo"].ToString();
                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();

                    if (dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() != "")
                    {
                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    }
                    else
                    {
                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    }
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
                    if (dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() != "")
                    {
                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    }
                    else
                    {
                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    }

                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Mod_FlyingHours"].ToString().Replace(":", ".");
                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");

                    //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
                    //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

                    //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
                    //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOfseats"].ToString();

                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    hdnDDate.Value = dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString();
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString();
                    hdnendcity.Value = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString();
                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    hdnendairport.Value = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();
                    txtMPrice.Value = dtOneWayTSel.Rows[0]["Mod_Price"].ToString();
                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
                    {
                        chkEmpty.Checked = true;
                    }
                    else
                    {
                        chkEmpty.Checked = false;
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                }
                tbldata.Visible = true;
                tblForm.Visible = true;
                tblGrid.Visible = false;
            }

            #endregion

            #region Manual Edit

            else
            {
                trManualandModiy.Visible = true;
                trSplit.Visible = false;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                lblSubScren.Text = "Add a Manual Trip";
                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours,  AvailbleFrom,AvilbleTo,NoOFSeats,DecrementType,DecrementAmount,EndAirport, EndCity,Active,TRIPNO,createdby,createdon,MPrice  FROM  OneWayTail_Setup  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "'").Tables[0];
                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
                {
                    hdfSCID.Value = "1";
                    lblManEditGroupId.Text = dtOneWayTSel.Rows[0]["rowid"].ToString();
                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["tripno"].ToString();
                    lbloldtail.Text = dtOneWayTSel.Rows[0]["tailno"].ToString();
                    txttrip.Value = dtOneWayTSel.Rows[0]["tripno"].ToString();
                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    txtStartCity.Text = dtOneWayTSel.Rows[0]["StartCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["StartAirport"].ToString();
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();

                    txtEndCity.Text = dtOneWayTSel.Rows[0]["EndCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["EndAirport"].ToString();
                    txtEndTime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Flyinghours"].ToString().Replace(":", ".");
                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString()).ToString("MM-dd-yyyy");
                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvilbleTo"].ToString()).ToString("MM-dd-yyyy");
                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOFSeats"].ToString();
                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    hdnDDate.Value = dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString();
                    hdnstarttime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();
                    hdnendtime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["StartCity"].ToString();
                    hdnendcity.Value = dtOneWayTSel.Rows[0]["EndCity"].ToString();
                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["StartAirport"].ToString();
                    hdnendairport.Value = dtOneWayTSel.Rows[0]["EndAirport"].ToString();
                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();
                    txtMPrice.Value = dtOneWayTSel.Rows[0]["MPrice"].ToString();
                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
                    {
                        chkEmpty.Checked = true;
                    }
                    else
                    {
                        chkEmpty.Checked = false;
                    }

                    DataTable dtGroup = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT New_StartAirport,New_EndAirport  FROM  Tailsetup_tripDetails  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and GroupId='0'").Tables[0];

                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Dep'").Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        gvDepAirport.DataSource = dt;
                        gvDepAirport.DataBind();
                        ViewState["dtDepAirport"] = dt;
                        divDepLan.Visible = true;
                        txtDepDisLandPage.Text = dtGroup.Rows[0]["New_StartAirport"].ToString();
                    }
                    else
                    {
                        divDepLan.Visible = false;
                    }

                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Arr'").Tables[0];

                    if (dtArr.Rows.Count > 0)
                    {
                        gvArrivalAirport.DataSource = dtArr;
                        gvArrivalAirport.DataBind();
                        ViewState["dtArrAirport"] = dtArr;
                        txtArrDisLandPage.Text = dtGroup.Rows[0]["New_EndAirport"].ToString();
                        divArrLan.Visible = true;
                    }
                    else
                    {
                        divArrLan.Visible = false;
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

                }

                tblForm.Visible = true;
                tblGrid.Visible = false;
            }
            #endregion
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public string saveCategory(string[] arrCategory)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_V1]", false);
        // DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  OneWayTail_Setup.rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours, EndAirport, EndCity  FROM  OneWayTail_Setup Inner join TailPricing_Configuration on TailPricing_Configuration.TailNo=OneWayTail_Setup.TailNo ").Tables[0];

        saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
        saveCategoryParam[1].Value = arrCategory[1];//Rowid
        saveCategoryParam[2].Value = arrCategory[2];//TailId
        saveCategoryParam[3].Value = arrCategory[3];//
        saveCategoryParam[4].Value = arrCategory[4];//
        saveCategoryParam[5].Value = arrCategory[5];//
        saveCategoryParam[6].Value = arrCategory[6];//
        saveCategoryParam[7].Value = arrCategory[7];//
        saveCategoryParam[8].Value = arrCategory[8];//
        saveCategoryParam[9].Value = arrCategory[9];//
        saveCategoryParam[10].Value = arrCategory[10];//
        saveCategoryParam[11].Value = arrCategory[11];//
        saveCategoryParam[12].Value = arrCategory[12];//
        saveCategoryParam[13].Value = arrCategory[13];//
        saveCategoryParam[14].Value = arrCategory[14];//
        // saveCategoryParam[15].Value = arrCategory[15];//
        // saveCategoryParam[16].Value = arrCategory[16];//
        saveCategoryParam[19].Value = arrCategory[19];//

        saveCategoryParam[20].Value = arrCategory[20];//
        saveCategoryParam[21].Value = arrCategory[21];//
        saveCategoryParam[22].Value = arrCategory[22];//
        saveCategoryParam[23].Value = arrCategory[23];//
        saveCategoryParam[24].Value = arrCategory[24];//
        saveCategoryParam[25].Value = arrCategory[25];//
        saveCategoryParam[26].Value = arrCategory[26];//
        saveCategoryParam[27].Value = arrCategory[27];//
        saveCategoryParam[28].Value = arrCategory[28];//
        saveCategoryParam[29].Value = arrCategory[29];//
        saveCategoryParam[30].Value = arrCategory[30];//
        saveCategoryParam[31].Value = arrCategory[31];//
        saveCategoryParam[32].Value = arrCategory[32];//
        saveCategoryParam[33].Value = arrCategory[33];//
        saveCategoryParam[34].Value = arrCategory[34];//
        saveCategoryParam[35].Value = arrCategory[35];//
        saveCategoryParam[36].Value = arrCategory[36];//
        saveCategoryParam[37].Value = Convert.ToDateTime(arrCategory[37]);//
        saveCategoryParam[38].Value = arrCategory[38];//
        saveCategoryParam[39].Value = arrCategory[39];//

        saveCategoryParam[41].Value = arrCategory[41];//
        saveCategoryParam[42].Value = arrCategory[42];//
        saveCategoryParam[43].Value = arrCategory[43];//
        saveCategoryParam[44].Value = arrCategory[44];//
        saveCategoryParam[46].Value = arrCategory[48];//
        saveCategoryParam[47].Value = arrCategory[49];//
        saveCategoryParam[48].Value = arrCategory[50];
        saveCategoryParam[49].Value = arrCategory[51];//


        saveCategoryParam[50].Value = arrCategory[52];//
        saveCategoryParam[51].Value = arrCategory[53];//



        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_V1]", saveCategoryParam).ToString();
        return obj.ToString();
    }
    public string saveCategoryGroup(string[] arrCategory)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_GROUP]", false);
        // DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  OneWayTail_Setup.rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours, EndAirport, EndCity  FROM  OneWayTail_Setup Inner join TailPricing_Configuration on TailPricing_Configuration.TailNo=OneWayTail_Setup.TailNo ").Tables[0];

        saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
        saveCategoryParam[1].Value = arrCategory[1];//Rowid
        saveCategoryParam[2].Value = arrCategory[2];//TailId
        saveCategoryParam[3].Value = arrCategory[3];//
        saveCategoryParam[4].Value = arrCategory[4];//
        saveCategoryParam[5].Value = arrCategory[5];//
        saveCategoryParam[6].Value = arrCategory[6];//
        saveCategoryParam[7].Value = arrCategory[7];//
        saveCategoryParam[8].Value = arrCategory[8];//
        saveCategoryParam[9].Value = arrCategory[9];//
        saveCategoryParam[10].Value = arrCategory[10];//
        saveCategoryParam[11].Value = arrCategory[11];//
        saveCategoryParam[12].Value = arrCategory[12];//
        saveCategoryParam[13].Value = arrCategory[13];//
        saveCategoryParam[14].Value = arrCategory[14];//
        // saveCategoryParam[15].Value = arrCategory[15];//
        // saveCategoryParam[16].Value = arrCategory[16];//
        saveCategoryParam[19].Value = arrCategory[19];//

        saveCategoryParam[20].Value = arrCategory[20];//
        saveCategoryParam[21].Value = arrCategory[21];//
        saveCategoryParam[22].Value = arrCategory[22];//
        saveCategoryParam[23].Value = arrCategory[23];//
        saveCategoryParam[24].Value = arrCategory[24];//
        saveCategoryParam[25].Value = arrCategory[25];//
        saveCategoryParam[26].Value = arrCategory[26];//
        saveCategoryParam[27].Value = arrCategory[27];//
        saveCategoryParam[28].Value = arrCategory[28];//
        saveCategoryParam[29].Value = arrCategory[29];//
        saveCategoryParam[30].Value = arrCategory[30];//
        saveCategoryParam[31].Value = arrCategory[31];//
        saveCategoryParam[32].Value = arrCategory[32];//
        saveCategoryParam[33].Value = arrCategory[33];//
        saveCategoryParam[34].Value = arrCategory[34];//
        saveCategoryParam[35].Value = arrCategory[35];//
        saveCategoryParam[36].Value = arrCategory[36];//
        saveCategoryParam[37].Value = Convert.ToDateTime(arrCategory[37]);//
        saveCategoryParam[38].Value = arrCategory[38];//
        saveCategoryParam[39].Value = arrCategory[39];//

        saveCategoryParam[41].Value = arrCategory[41];//
        saveCategoryParam[42].Value = arrCategory[42];//
        saveCategoryParam[43].Value = arrCategory[43];//
        saveCategoryParam[44].Value = arrCategory[44];//
        saveCategoryParam[45].Value = arrCategory[45];//
        saveCategoryParam[46].Value = arrCategory[46];//
        saveCategoryParam[47].Value = arrCategory[47];//
        saveCategoryParam[48].Value = arrCategory[48];//


        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_OneWayTail_Setup_INS_GROUP]", saveCategoryParam).ToString();
        return obj.ToString();
    }

    protected void ddlstatus_onselectedChanged(object sender, EventArgs e)
    {
        BindEmptyLegs();
        gvTailDetails.PageIndex = 0;
        gvTailDetails.DataBind();
    }
    protected void btnconfirmdelsplitdelete_click(object sender, EventArgs e)
    {
        try
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  EmptyLegAirport_Child  where GroupId = '" + lblgroupiddelete.Text.Trim() + "' ");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + lblgroupiddelete.Text + "' and tripflag<>'B0'");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set Groupid=0,ModifyFlag='',Mod_AvilableFrom=null,Mod_AvailableTo=null,Mod_Starttime=null,Mod_Endtime=null,Mod_FlyingHours=null,Mod_Startcity='',Mod_Endcity='',Mod_StartAirport='',Mod_EndAirport='',Createdby='',CreatedOn=null,Splittype='',New_StartAirport='',New_EndAirport='' Where Groupid='" + lblgroupiddelete.Text + "'");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set flag='OLD',groupid=0,Mod_Flag='',splittype='',New_StartAirport='',New_EndAirport=''");
            string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
            string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", false);
            saveCategoryParam[0].Value = strStartDate;
            saveCategoryParam[1].Value = strEndDate;
            DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", saveCategoryParam).Tables[0];
            DataTable dtMerge = dtOneWayTrip.Clone();
            if (dtOneWayTrip.Rows.Count > 0)
            {
                for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                {
                    decimal daystoflight = 0;
                    decimal L10_minPrice = 0;
                    decimal M10_maxPrice = 0;
                    decimal price = 0;

                    string tailNo = dtOneWayTrip.Rows[i]["TailNo"].ToString();
                    string noOfSeats = dtOneWayTrip.Rows[i]["NoOFSeats"].ToString();
                    var flyingDate = dtOneWayTrip.Rows[i]["DDate"].ToString();
                    var flyingtoDate = dtOneWayTrip.Rows[i]["ADate"].ToString();
                    var strtTime = dtOneWayTrip.Rows[i]["DTime"].ToString();

                    var endTime = dtOneWayTrip.Rows[i]["ATime"].ToString();
                    var flyingHours = dtOneWayTrip.Rows[i]["FH"].ToString();
                    //  var flyingHours = "1:18";
                    string startCity = dtOneWayTrip.Rows[i]["StartCity"].ToString();
                    string endCity = dtOneWayTrip.Rows[i]["EndCity"].ToString();
                    string startAirport = dtOneWayTrip.Rows[i]["Start"].ToString();
                    string endAirport = dtOneWayTrip.Rows[i]["End"].ToString();

                    var HourlyRate = dtOneWayTrip.Rows[i]["HourlyRate"].ToString();
                    var fixedCeiling = dtOneWayTrip.Rows[i]["FixedCeiling"].ToString();
                    var fixedFloor = dtOneWayTrip.Rows[i]["FixedFloor"].ToString();
                    var D4_MinTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MinTimeBD"].ToString());
                    var D5_MaxTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MaxTimeBD"].ToString());
                    var MinFLHSellable = dtOneWayTrip.Rows[i]["MinFLHSellable"].ToString();
                    //  string DecFlag = dtOneWayTrip.Rows[i]["DecFlag"].ToString();
                    //  var DecPerHour = dtOneWayTrip.Rows[i]["DecPerHour"].ToString();
                    var applytax = dtOneWayTrip.Rows[i]["applytax"].ToString();
                    var tripno = dtOneWayTrip.Rows[i]["tripno"].ToString();
                    var enabletrip = dtOneWayTrip.Rows[i]["enabletrip"].ToString();
                    var showtrip = dtOneWayTrip.Rows[i]["Active"].ToString();
                    var TailFlag = dtOneWayTrip.Rows[i]["TailFlag"].ToString();
                    var createdby = dtOneWayTrip.Rows[i]["createdby"].ToString();
                    if (createdby == "")
                    {
                        createdby = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                    }
                    var createdon = dtOneWayTrip.Rows[i]["createdon"].ToString();
                    var tripflag = dtOneWayTrip.Rows[i]["tripflag"].ToString();
                    var groupid = dtOneWayTrip.Rows[i]["groupid"].ToString();
                    var FosModifiedFlag = dtOneWayTrip.Rows[i]["ModifyFlag"].ToString();
                    var leg = dtOneWayTrip.Rows[i]["leg"].ToString();
                    var splitype = dtOneWayTrip.Rows[i]["Splittype"].ToString();
                    var NewStartAirport = dtOneWayTrip.Rows[i]["NewStart"].ToString();
                    var NewEndAirport = dtOneWayTrip.Rows[i]["NewEnd"].ToString();

                    if (dtOneWayTrip.Rows[i]["HourlyRate"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedCeiling"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedFloor"].ToString() != "0.00")
                    {
                        var dtFlyingDays = Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00";

                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00").Subtract(DateTime.Now);

                        DateTime startTime = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00");
                        DateTime endtime1 = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + endTime + ":00");
                        var duration = endtime1.Subtract(startTime);
                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                        decimal totalhrs = 0;
                        if (flyingHours != "")
                        {
                            totalhrs = Convert.ToDecimal(flyingHours);
                        }

                        //decimal totalhrs = Convert.ToDecimal(TimeSpan.Parse(flyingHours).TotalHours);

                        //  var calculatedhour = "00:00";
                        //if (totalhrs.ToString().Contains(".") == false)
                        //{
                        //    calculatedhour = TimeSpan.Parse(flyingHours).TotalHours + ":00";
                        //}
                        //else
                        //    calculatedhour = TimeSpan.Parse(flyingHours).TotalHours.ToString("00.00").Replace(".", ":");


                        long durationTicks = Math.Abs(duration.Ticks / TimeSpan.TicksPerMillisecond);
                        long hours = durationTicks / (1000 * 60 * 60);
                        long minutes = (durationTicks - (hours * 60 * 60 * 1000)) / (1000 * 60);

                        string CBPF = string.Empty;
                        string exactFlyinghrs = hours.ToString("00") + ":" + minutes.ToString("00");

                        L10_minPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs));

                        if (L10_minPrice < Convert.ToDecimal(fixedFloor))
                        {
                            L10_minPrice = Convert.ToDecimal(fixedFloor);
                            CBPF = "FIXEDCEILING";
                        }

                        M10_maxPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs)) + Convert.ToDecimal(fixedCeiling);


                        //  price = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs)) + Convert.ToDecimal(HourlyRate);

                        // var ss=Convert.ToInt32(((1-(7-1.99)/(7-(0.25)))*(14900-5000)+5000)/25)*25;
                        //=MAX(INT(((1-(D5-K10)/(D5-(D4)))*(M10-L10)+L10)/25)*25,L10)*IF(K10<(D4),0,1)*IF(I10>=D6,1,"ERROR")

                        var finalPricevalidation2 = (decimal)0;

                        var finalPrice = Convert.ToInt32(((1 - (D5_MaxTimeBD - Convert.ToDecimal(_totalDaysK10)) / (D5_MaxTimeBD - Convert.ToDecimal(D4_MinTimeBD))) * (M10_maxPrice - L10_minPrice) + L10_minPrice) / 25) * 25;

                        int mul1 = 0;
                        if (Convert.ToDecimal(_totalDaysK10) < D4_MinTimeBD)
                        {
                            mul1 = 0;
                        }
                        else
                            mul1 = 1;

                        string mul2 = "0";
                        if (Convert.ToDecimal(Convert.ToDecimal(totalhrs)) >= Convert.ToDecimal(MinFLHSellable))
                        {
                            mul2 = "1";
                        }
                        else
                            mul2 = "ERROR";



                        if (mul2 != "ERROR")
                        {
                            finalPricevalidation2 = Convert.ToDecimal(finalPrice) * Convert.ToDecimal(mul2) * Convert.ToDecimal(mul1);
                        }
                        string Specialflag = string.Empty;
                        DataTable dtspecialpricing = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from SpeicalPricing").Tables[0];
                        DataRow[] drspecial = dtspecialpricing.Select("#" + flyingDate + "# >= StartDate AND EndDate >= #" + flyingDate + "#" + "and startAirport='" + startAirport + "' and endairport ='" + endAirport + "'");
                        DataTable dtselection = new DataTable();
                        if (drspecial.Length > 0)
                        {
                            dtselection = drspecial.CopyToDataTable();
                            if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "H")
                            {
                                finalPricevalidation2 = Convert.ToDecimal(flyingHours) * Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                                Specialflag = "(Special Price)";
                            }
                            else if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "D")
                            {
                                finalPricevalidation2 = Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                                Specialflag = "(Special Price)";
                            }
                        }

                        if (FosModifiedFlag.ToString() == "ET")
                        {
                            DataTable dtManualPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Mprice from OneWayTail_Setup where TailNO='" + tailNo.ToString().Trim() + "' and TripNo='" + tripno.ToString().Trim() + "' and startAirport='" + startAirport.ToString().Trim() + "' and EndAirport='" + endAirport.ToString().Trim() + "' ").Tables[0];

                            if (dtManualPrice.Rows.Count > 0)
                            {
                                if (Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString()) > 3500)
                                {
                                    finalPricevalidation2 = Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString());
                                }

                            }
                        }
                        if (FosModifiedFlag.ToString() == "MF")
                        {
                            DataTable dtEditPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Mod_Price from Tailsetup_tripDetails where TailNO='" + tailNo.ToString().Trim() + "' and TripNo='" + tripno.ToString().Trim() + "' and GroupID='" + groupid + "' and GroupID<>0").Tables[0];

                            if (dtEditPrice.Rows.Count > 0)
                            {
                                if (Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString()) > 3500)
                                {
                                    finalPricevalidation2 = Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString());
                                }
                            }
                        }

                        string s = DateTime.Now.ToString("HH:mm:ss.fff");
                        TimeSpan ts = TimeSpan.Parse(s);
                        string time = ts.ToString();
                        string Date = DateTime.Now.Date.ToString();
                        if (Specialflag == "(Special Price)")
                        {
                            //  DataTable dtspecialCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, " select * from LivePricingCaculation Where flag='FINAL' and Priceflag='(Special Price)' and TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' and currentprice='" + finalPricevalidation2 + "'").Tables[0];
                            DataTable dtspecialCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, " select * from LivePricingCaculation Where flag='FINAL' and Priceflag='(Special Price)' and TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' ").Tables[0];
                            if (dtspecialCheck.Rows.Count > 0)
                            {
                                //  SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update LivePricingCaculation Set currentprice='" + finalPricevalidation2 + "' where flag='FINAL' and Priceflag='(Special Price)' and TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' ");


                            }
                            else
                            {
                                // SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update LivePricingCaculation Set Flag='OLD' where TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' ");
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  LivePricingCaculation values (" +
                                                                                                                                                  " '" + tailNo + "'," +
                                                                                                                                                   " '" + flyingDate + "'," +
                                                                                                                                                    " '" + flyingtoDate + "'," +
                                                                                                                                                     " '" + strtTime + "'," +
                                                                                                                                                      " '" + endTime + "'," +
                                                                                                                                                       " '" + startAirport + "'," +
                                                                                                                                                        " '" + startCity.Replace("'", "''") + "'," +
                                                                                                                                                          " '" + endAirport + "'," +
                                                                                                                                                            " '" + endCity.Replace("'", "''") + "'," +
                                                                                                                                                             " '" + noOfSeats + "'," +
                                                                                                                                                              " '" + totalhrs.ToString() + "'," +
                                                                                                                                                                " '" + totalhrs.ToString() + "'," +
                                                                                                                                                                " '" + HourlyRate + "'," +
                                                                                                                                                                  " '" + fixedFloor + "'," +
                                                                                                                                                                  " '" + fixedCeiling + "'," +
                                                                                                                                                                     " '" + D4_MinTimeBD + "'," +
                                                                                                                                                                       " '" + D5_MaxTimeBD + "'," +
                                                                                                                                                                        " '" + MinFLHSellable + "'," +
                                                                                                                                                                            " '" + _totalDaysK10 + "'," +

                                                                                                                                                                 " '" + L10_minPrice + "'," +
                                                                                                                                                                  " '" + M10_maxPrice + "'," +
                                                                                                                                                                  " '" + finalPricevalidation2 + "'," +
                                                                                                                                                                    " '" + time + "'," +
                                                                                                                                                                     " '" + Date + "'," +
                                                                                                                                                                       " 'FINAL'," +
                                                                                                                                                                         " '" + CBPF + "','0','" + applytax + "','" + tripno + "','" + enabletrip + "','" + showtrip + "','" + Specialflag + "','" + TailFlag + "','" + createdby + "','" + createdon + "','" + tripflag + "','" + groupid + "','" + FosModifiedFlag + "','" + leg + "','" + splitype + "','" + NewStartAirport + "','" + NewEndAirport + "') ");

                                if (finalPricevalidation2 > 0)
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='NEW' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                                }
                                else
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='EXP' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                                }
                            }
                        }
                        else
                        {
                            //  SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update LivePricingCaculation Set Flag='OLD' where TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' ");
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  LivePricingCaculation values (" +
                                                                                                                                              " '" + tailNo + "'," +
                                                                                                                                               " '" + flyingDate + "'," +
                                                                                                                                                " '" + flyingDate + "'," +
                                                                                                                                                 " '" + strtTime + "'," +
                                                                                                                                                  " '" + endTime + "'," +
                                                                                                                                                   " '" + startAirport + "'," +
                                                                                                                                                    " '" + startCity.Replace("'", "''") + "'," +
                                                                                                                                                      " '" + endAirport + "'," +
                                                                                                                                                        " '" + endCity.Replace("'", "''") + "'," +
                                                                                                                                                         " '" + noOfSeats + "'," +
                                                                                                                                                          " '" + totalhrs.ToString() + "'," +
                                                                                                                                                            " '" + totalhrs.ToString() + "'," +
                                                                                                                                                            " '" + HourlyRate + "'," +
                                                                                                                                                              " '" + fixedFloor + "'," +
                                                                                                                                                              " '" + fixedCeiling + "'," +
                                                                                                                                                                 " '" + D4_MinTimeBD + "'," +
                                                                                                                                                                   " '" + D5_MaxTimeBD + "'," +
                                                                                                                                                                    " '" + MinFLHSellable + "'," +
                                                                                                                                                                        " '" + _totalDaysK10 + "'," +

                                                                                                                                                             " '" + L10_minPrice + "'," +
                                                                                                                                                              " '" + M10_maxPrice + "'," +
                                                                                                                                                              " '" + finalPricevalidation2 + "'," +
                                                                                                                                                                " '" + time + "'," +
                                                                                                                                                                 " '" + Date + "'," +
                                                                                                                                                                   " 'FINAL'," +
                                                                                                                                                                     " '" + CBPF + "','0','" + applytax + "','" + tripno + "','" + enabletrip + "','" + showtrip + "','" + Specialflag + "','" + TailFlag + "','" + createdby + "','" + createdon + "','" + tripflag + "','" + groupid + "','" + FosModifiedFlag + "','" + leg + "','" + splitype + "','" + NewStartAirport + "','" + NewEndAirport + "') ");
                        }
                    }
                }

            }

            tblForm.Visible = false;
            tblGrid.Visible = true;
            BindEmptyLegs();
            hdfSCID.Value = "0";
            lblalert.Text = "Tail setup details has been deleted successfully";
            mpealert.Show();
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Tail setup details has been deleted successfully ');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            //ImageButton imbExport = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            // Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            Label lblTailNo = (Label)gvrow.FindControl("lblTailNo");
            Label lbltrip = (Label)gvrow.FindControl("lbltrip");
            Label lbldate = (Label)gvrow.FindControl("lbldate");
            Label lblstarttime = (Label)gvrow.FindControl("lblstarttime");
            Label lblstartair = (Label)gvrow.FindControl("lblstartair");
            Label lblendair = (Label)gvrow.FindControl("lblendair");
            Label lblGroupId = (Label)gvrow.FindControl("lblGroupId");
            Label lblType = (Label)gvrow.FindControl("lblType");

            if (lblType.Text.Trim() == "Manual DH")
            {
                lbltailconfirm.Text = lblTailNo.Text;
                lbltripconfirm.Text = lbltrip.Text;
                lblDateconfirm.Text = lbldate.Text;
                lblstarttimeconfirm.Text = lblstarttime.Text;
                lblstartairconfirm.Text = lblstartair.Text;
                lblendairconfirm.Text = lblendair.Text;

                mpeconfirm.Show();
            }
            else
            {


                Label lblgroupid = (Label)gvrow.FindControl("lblgroupid");


                lblgroupiddelete.Text = lblgroupid.Text;

                mpesplit.Show();
                //lbllegnoP.Text = lblName.Text;
            }




        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {


            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  EmptyLegAirport_Child  where GroupId = '" + lblgroupiddelete.Text.Trim() + "' ");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  OneWayTail_Setup  where tailno = '" + lbltailconfirm.Text.Trim() + "' and tripno = '" + lbltripconfirm.Text.Trim() + "'");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  Tailsetup_tripDetails  where tailno = '" + lbltailconfirm.Text.Trim() + "' and AvailbleFrom='" + lblDateconfirm.Text + "' and starttime='" + lblstarttimeconfirm.Text + "' and StartAirport='" + lblstartairconfirm.Text + "' and [EndAirport]='" + lblendairconfirm.Text + "'");

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update LivePricingCaculation Set Flag='OLD',GroupId='0',Mod_Flag='',New_StartAirport='',New_EndAirport='' where TailNo='" + lbltailconfirm.Text.Trim() + "' and Start='" + lblstartairconfirm.Text + "' and [End]='" + lblendairconfirm.Text + "' and DDate='" + lblDateconfirm.Text + "' ");

            DataTable dtPS1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where tailno = '" + lbltailconfirm.Text.Trim() + "'").Tables[0];

            if (dtPS1.Rows.Count > 0)
            {

            }
            else
            {
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  Tailsetup  where tailno = '" + lbltailconfirm.Text.Trim() + "'");
            }
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set flag='OLD'");
            string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
            string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
            SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", false);
            saveCategoryParam[0].Value = strStartDate;
            saveCategoryParam[1].Value = strEndDate;
            DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[SP_FUTURETRIPDATA_WITHSETUP_V1]", saveCategoryParam).Tables[0];
            DataTable dtMerge = dtOneWayTrip.Clone();
            if (dtOneWayTrip.Rows.Count > 0)
            {
                for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                {
                    decimal daystoflight = 0;
                    decimal L10_minPrice = 0;
                    decimal M10_maxPrice = 0;
                    decimal price = 0;

                    string tailNo = dtOneWayTrip.Rows[i]["TailNo"].ToString();
                    string noOfSeats = dtOneWayTrip.Rows[i]["NoOFSeats"].ToString();
                    var flyingDate = dtOneWayTrip.Rows[i]["DDate"].ToString();
                    var flyingtoDate = dtOneWayTrip.Rows[i]["ADate"].ToString();
                    var strtTime = dtOneWayTrip.Rows[i]["DTime"].ToString();

                    var endTime = dtOneWayTrip.Rows[i]["ATime"].ToString();
                    var flyingHours = dtOneWayTrip.Rows[i]["FH"].ToString();
                    //  var flyingHours = "1:18";
                    string startCity = dtOneWayTrip.Rows[i]["StartCity"].ToString();
                    string endCity = dtOneWayTrip.Rows[i]["EndCity"].ToString();
                    string startAirport = dtOneWayTrip.Rows[i]["Start"].ToString();
                    string endAirport = dtOneWayTrip.Rows[i]["End"].ToString();

                    var HourlyRate = dtOneWayTrip.Rows[i]["HourlyRate"].ToString();
                    var fixedCeiling = dtOneWayTrip.Rows[i]["FixedCeiling"].ToString();
                    var fixedFloor = dtOneWayTrip.Rows[i]["FixedFloor"].ToString();
                    var D4_MinTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MinTimeBD"].ToString());
                    var D5_MaxTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MaxTimeBD"].ToString());
                    var MinFLHSellable = dtOneWayTrip.Rows[i]["MinFLHSellable"].ToString();
                    var applytax = dtOneWayTrip.Rows[i]["applytax"].ToString();
                    var tripno = dtOneWayTrip.Rows[i]["tripno"].ToString();
                    var enabletrip = dtOneWayTrip.Rows[i]["enabletrip"].ToString();
                    var showtrip = dtOneWayTrip.Rows[i]["Active"].ToString();
                    var TailFlag = dtOneWayTrip.Rows[i]["TailFlag"].ToString();
                    var createdby = dtOneWayTrip.Rows[i]["createdby"].ToString();
                    if (createdby == "")
                    {
                        createdby = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                    }
                    var createdon = dtOneWayTrip.Rows[i]["createdon"].ToString();
                    var tripflag = dtOneWayTrip.Rows[i]["tripflag"].ToString();
                    var groupid = dtOneWayTrip.Rows[i]["groupid"].ToString();
                    var FosModifiedFlag = dtOneWayTrip.Rows[i]["ModifyFlag"].ToString();
                    var leg = dtOneWayTrip.Rows[i]["leg"].ToString();
                    var splitype = dtOneWayTrip.Rows[i]["Splittype"].ToString();
                    var NewStartAirport = dtOneWayTrip.Rows[i]["NewStart"].ToString();
                    var NewEndAirport = dtOneWayTrip.Rows[i]["NewEnd"].ToString();
                    if (dtOneWayTrip.Rows[i]["HourlyRate"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedCeiling"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedFloor"].ToString() != "0.00")
                    {
                        var dtFlyingDays = Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00";

                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00").Subtract(DateTime.Now);

                        DateTime startTime = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00");
                        DateTime endtime1 = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + endTime + ":00");
                        var duration = endtime1.Subtract(startTime);
                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                        decimal totalhrs = Convert.ToDecimal(flyingHours);

                        long durationTicks = Math.Abs(duration.Ticks / TimeSpan.TicksPerMillisecond);
                        long hours = durationTicks / (1000 * 60 * 60);
                        long minutes = (durationTicks - (hours * 60 * 60 * 1000)) / (1000 * 60);

                        string CBPF = string.Empty;
                        string exactFlyinghrs = hours.ToString("00") + ":" + minutes.ToString("00");

                        L10_minPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs));

                        if (L10_minPrice < Convert.ToDecimal(fixedFloor))
                        {
                            L10_minPrice = Convert.ToDecimal(fixedFloor);
                            CBPF = "FIXEDCEILING";
                        }

                        M10_maxPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs)) + Convert.ToDecimal(fixedCeiling);

                        var finalPricevalidation2 = (decimal)0;

                        var finalPrice = Convert.ToInt32(((1 - (D5_MaxTimeBD - Convert.ToDecimal(_totalDaysK10)) / (D5_MaxTimeBD - Convert.ToDecimal(D4_MinTimeBD))) * (M10_maxPrice - L10_minPrice) + L10_minPrice) / 25) * 25;

                        int mul1 = 0;
                        if (Convert.ToDecimal(_totalDaysK10) < D4_MinTimeBD)
                        {
                            mul1 = 0;
                        }
                        else
                            mul1 = 1;

                        string mul2 = "0";
                        if (Convert.ToDecimal(Convert.ToDecimal(totalhrs)) >= Convert.ToDecimal(MinFLHSellable))
                        {
                            mul2 = "1";
                        }
                        else
                            mul2 = "ERROR";



                        if (mul2 != "ERROR")
                        {
                            finalPricevalidation2 = Convert.ToDecimal(finalPrice) * Convert.ToDecimal(mul2) * Convert.ToDecimal(mul1);
                        }
                        string Specialflag = string.Empty;
                        DataTable dtspecialpricing = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from SpeicalPricing").Tables[0];
                        DataRow[] drspecial = dtspecialpricing.Select("#" + flyingDate + "# >= StartDate AND EndDate >= #" + flyingDate + "#" + "and startAirport='" + startAirport + "' and endairport ='" + endAirport + "'");
                        DataTable dtselection = new DataTable();
                        if (drspecial.Length > 0)
                        {
                            dtselection = drspecial.CopyToDataTable();
                            if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "H")
                            {
                                finalPricevalidation2 = Convert.ToDecimal(flyingHours) * Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                                Specialflag = "(Special Price)";
                            }
                            else if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "D")
                            {
                                finalPricevalidation2 = Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                                Specialflag = "(Special Price)";
                            }
                        }

                        if (FosModifiedFlag.ToString() == "ET")
                        {
                            DataTable dtManualPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Mprice from OneWayTail_Setup where TailNO='" + tailNo.ToString().Trim() + "' and TripNo='" + tripno.ToString().Trim() + "' and startAirport='" + startAirport.ToString().Trim() + "' and EndAirport='" + endAirport.ToString().Trim() + "' ").Tables[0];

                            if (dtManualPrice.Rows.Count > 0)
                            {
                                if (Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString()) > 3500)
                                {
                                    finalPricevalidation2 = Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString());
                                }

                            }
                        }
                        if (FosModifiedFlag.ToString() == "MF")
                        {
                            DataTable dtEditPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Mod_Price from Tailsetup_tripDetails where TailNO='" + tailNo.ToString().Trim() + "' and TripNo='" + tripno.ToString().Trim() + "' and GroupID='" + groupid + "' and GroupID<>0").Tables[0];

                            if (dtEditPrice.Rows.Count > 0)
                            {
                                if (Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString()) > 3500)
                                {
                                    finalPricevalidation2 = Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString());
                                }
                            }
                        }

                        string s = DateTime.Now.ToString("HH:mm:ss.fff");
                        TimeSpan ts = TimeSpan.Parse(s);
                        string time = ts.ToString();
                        string Date = DateTime.Now.Date.ToString();
                        if (Specialflag == "(Special Price)")
                        {
                            DataTable dtspecialCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, " select * from LivePricingCaculation Where flag='FINAL' and Priceflag='(Special Price)' and TailNo='" + tailNo + "' and Start='" + startAirport + "' and [End]='" + endAirport + "' and DDate='" + flyingDate + "' ").Tables[0];
                            if (dtspecialCheck.Rows.Count > 0)
                            {


                            }
                            else
                            {
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  LivePricingCaculation values (" +
                                                                                                                                                  " '" + tailNo + "'," +
                                                                                                                                                   " '" + flyingDate + "'," +
                                                                                                                                                    " '" + flyingtoDate + "'," +
                                                                                                                                                     " '" + strtTime + "'," +
                                                                                                                                                      " '" + endTime + "'," +
                                                                                                                                                       " '" + startAirport + "'," +
                                                                                                                                                        " '" + startCity.Replace("'", "''") + "'," +
                                                                                                                                                          " '" + endAirport + "'," +
                                                                                                                                                            " '" + endCity.Replace("'", "''") + "'," +
                                                                                                                                                             " '" + noOfSeats + "'," +
                                                                                                                                                              " '" + totalhrs.ToString() + "'," +
                                                                                                                                                                " '" + totalhrs.ToString() + "'," +
                                                                                                                                                                " '" + HourlyRate + "'," +
                                                                                                                                                                  " '" + fixedFloor + "'," +
                                                                                                                                                                  " '" + fixedCeiling + "'," +
                                                                                                                                                                     " '" + D4_MinTimeBD + "'," +
                                                                                                                                                                       " '" + D5_MaxTimeBD + "'," +
                                                                                                                                                                        " '" + MinFLHSellable + "'," +
                                                                                                                                                                            " '" + _totalDaysK10 + "'," +

                                                                                                                                                                 " '" + L10_minPrice + "'," +
                                                                                                                                                                  " '" + M10_maxPrice + "'," +
                                                                                                                                                                  " '" + finalPricevalidation2 + "'," +
                                                                                                                                                                    " '" + time + "'," +
                                                                                                                                                                     " '" + Date + "'," +
                                                                                                                                                                       " 'FINAL'," +
                                                                                                                                                                         " '" + CBPF + "','0','" + applytax + "','" + tripno + "','" + enabletrip + "','" + showtrip + "','" + Specialflag + "','" + TailFlag + "','" + createdby + "','" + createdon + "','" + tripflag + "','" + groupid + "','" + FosModifiedFlag + "','" + leg + "','" + splitype + "','" + NewStartAirport.Replace("'", "''") + "','" + NewEndAirport.Replace("'", "''") + "') ");

                                if (finalPricevalidation2 > 0)
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='NEW' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                                }
                                else
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update  Tailsetup_tripDetails set Showpriceflag='EXP' Where Tailsetup_tripDetails.tailno='" + tailNo + "' and Tailsetup_tripDetails.tripno='" + tripno + "' and AvailbleFrom='" + flyingDate + "'");
                                }
                            }
                        }
                        else
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  LivePricingCaculation values (" +
                                                                                                                                              " '" + tailNo + "'," +
                                                                                                                                               " '" + flyingDate + "'," +
                                                                                                                                                " '" + flyingtoDate + "'," +
                                                                                                                                                 " '" + strtTime + "'," +
                                                                                                                                                  " '" + endTime + "'," +
                                                                                                                                                   " '" + startAirport + "'," +
                                                                                                                                                    " '" + startCity.Replace("'", "''") + "'," +
                                                                                                                                                      " '" + endAirport + "'," +
                                                                                                                                                        " '" + endCity.Replace("'", "''") + "'," +
                                                                                                                                                         " '" + noOfSeats + "'," +
                                                                                                                                                          " '" + totalhrs.ToString() + "'," +
                                                                                                                                                            " '" + totalhrs.ToString() + "'," +
                                                                                                                                                            " '" + HourlyRate + "'," +
                                                                                                                                                              " '" + fixedFloor + "'," +
                                                                                                                                                              " '" + fixedCeiling + "'," +
                                                                                                                                                                 " '" + D4_MinTimeBD + "'," +
                                                                                                                                                                   " '" + D5_MaxTimeBD + "'," +
                                                                                                                                                                    " '" + MinFLHSellable + "'," +
                                                                                                                                                                        " '" + _totalDaysK10 + "'," +

                                                                                                                                                             " '" + L10_minPrice + "'," +
                                                                                                                                                              " '" + M10_maxPrice + "'," +
                                                                                                                                                              " '" + finalPricevalidation2 + "'," +
                                                                                                                                                                " '" + time + "'," +
                                                                                                                                                                 " '" + Date + "'," +
                                                                                                                                                                   " 'FINAL'," +
                                                                                                                                                                     " '" + CBPF + "','0','" + applytax + "','" + tripno + "','" + enabletrip + "','" + showtrip + "','" + Specialflag + "','" + TailFlag + "','" + createdby + "','" + createdon + "','" + tripflag + "','" + groupid + "','" + FosModifiedFlag + "','" + leg + "','" + splitype + "','" + NewStartAirport.Replace("'", "''") + "','" + NewEndAirport.Replace("'", "''") + "') ");
                        }
                    }
                }

            }

            tblForm.Visible = false;
            tblGrid.Visible = true;
            BindEmptyLegs();
            hdfSCID.Value = "0";
            lblalert.Text = "Tail setup details has been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnAddOnClick(object sender, EventArgs e)
    {
        lblgroup.Text = string.Empty;
        divPricModLeft.Visible = true;
        divPriceModRight.Visible = true;
        tblForm.Visible = true;
        tblGrid.Visible = false;
        txtTailNo.Disabled = false;
        txttrip.Disabled = false;
        divstartcity.Visible = true;
        divModify.Visible = true;
        divManualAirport.Visible = false;
        divstartcityLeft.Visible = true;
        divstartcounty.Attributes.Add("style", "display:none");
        btnSave.Text = "Save";
        lblSubScren.Text = "Add a Manual Trip";
        clearform();
        txtTailNo.Focus();
        trSplit.Visible = false;
        rblStatus.Visible = false;
        rblStatus.Visible = true;
        btnSave.Visible = true;
        btnView.Visible = true;
        tbldata.Visible = false;
        trManualandModiy.Visible = true;
        ViewState["dtLegDetails"] = null;
        gvLegDetails.DataSource = null;
        gvLegDetails.DataBind();

        ViewState["dtDepAirport"] = null;
        gvDepAirport.DataSource = null;
        gvDepAirport.DataBind();

        ViewState["dtArrAirport"] = null;
        gvArrivalAirport.DataSource = null;
        gvArrivalAirport.DataBind();
    }
    protected void btnViewOnClick(object sender, EventArgs e)
    {
        BindEmptyLegs();
        tblForm.Visible = false;
        tblGrid.Visible = true;
        tbldata.Visible = false;
        rblStatus.Visible = true;
    }
    protected void gvTailDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindEmptyLegs();
        gvTailDetails.PageIndex = e.NewPageIndex;
        gvTailDetails.DataBind();
    }
    protected void gvTailDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;

                Label lblGroupID = e.Row.FindControl("lblGroupID") as Label;

                Label lblType = e.Row.FindControl("lblType") as Label;

                ImageButton imgDelete = e.Row.FindControl("imbDelete") as ImageButton;

                if (lblType.Text.Trim() == "FOS DH")
                {
                    lblType.ForeColor = System.Drawing.ColorTranslator.FromHtml("#1B5E20");
                    lblType.Font.Bold = true;
                }
                else if (lblType.Text.Trim() == "Manual DH")
                {
                    lblType.ForeColor = System.Drawing.ColorTranslator.FromHtml("#d32f2f");
                    lblType.Font.Bold = true;
                }
                if (lblGroupID.Text.Trim() == "0" && lblType.Text.Trim() == "FOS DH")
                {
                    imgDelete.Visible = false;
                }
                else if (lblGroupID.Text.Trim() == "0" && lblType.Text.Trim() == "Split")
                {
                    imgDelete.Visible = false;
                }
                else if (lblGroupID.Text.Trim() != "0" && lblType.Text.Trim() == "Split")
                {
                    imgDelete.Visible = true;
                }
                else
                {
                    imgDelete.Visible = true;
                }

                if (lblcreatedon.Text.Trim() == "1/1/1900 12:00:00 AM")
                {
                    lblcreatedon.Text = null;
                }

                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //  lblMsg.Text = ex.Message;
        }
    }

    protected void Buttonyes_Click(object sender, EventArgs e)
    {
        mpetrip.Hide();
        tblForm.Visible = true;
        tblGrid.Visible = false;
    }
    public DataTable BindEmptyLegs_SP(string strManaeType, string strFromDate, string strToDate, string strPageINdex, string strPageSize)
    {
        SqlParameter[] Get_CostSetDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", false);
        Get_CostSetDetails[0].Value = strManaeType;
        Get_CostSetDetails[1].Value = strFromDate;
        Get_CostSetDetails[2].Value = strToDate;
        Get_CostSetDetails[3].Value = strPageINdex;
        Get_CostSetDetails[4].Value = strPageSize;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", Get_CostSetDetails).Tables[0];
    }
    public DataTable GET_LIVEDATA(string strManaeType, string strFromDate, string strToDate, string strTail, string strTrip)
    {
        SqlParameter[] Get_CostSetDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", false);
        Get_CostSetDetails[0].Value = "DATA";
        Get_CostSetDetails[1].Value = strFromDate;
        Get_CostSetDetails[2].Value = strToDate;
        Get_CostSetDetails[5].Value = strTail;
        Get_CostSetDetails[6].Value = strTrip;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_OneWayTail_Setup_SEL_V1", Get_CostSetDetails).Tables[0];
    }
    void clearform()
    {
        hdfSCID.Value = "0";
        txtTailNo.Value = "";
        txtStartCity.Text = "";
        txtStartTime.Value = "";
        txtEndCity.Text = "";
        txtEndTime.Value = "";
        txtFlyHours1.Value = "";
        txtAvaFrom.Text = "";
        txtAvaTo.Text = "";
        txtNoSeats.Value = "";
        txttrip.Value = "";
        txtMPrice.Value = "";
    }


    protected void btnAddLegOnClick(object sender, EventArgs e)
    {
        trLeg2.Attributes.Add("style", "display:block");
        trLeg1.Attributes.Add("style", "display:block");
        divstartsplitLeft.Attributes.Add("style", "display:''");
        divstartcounty.Attributes.Add("style", "display:none");
        DataRow drMappingDetails;
        DataTable dtMapping = new DataTable();
        try
        {
            if (ViewState["dtLegDetails"] != null)
            {
                dtMapping = (DataTable)ViewState["dtLegDetails"];
            }
            else
            {
                dtMapping.Columns.Clear();
                dtMapping.Columns.Add("TripFlag");
                dtMapping.Columns.Add("TailNo");
                dtMapping.Columns.Add("tripno");
                dtMapping.Columns.Add("Start");
                dtMapping.Columns.Add("startCity");
                dtMapping.Columns.Add("End");
                dtMapping.Columns.Add("EndCity");
                dtMapping.Columns.Add("DTime");
                dtMapping.Columns.Add("ATime");
                dtMapping.Columns.Add("FlyinghoursFOS");
            }


            drMappingDetails = dtMapping.NewRow();
            if (gvLegDetails.Rows.Count == 0)
            {
                drMappingDetails["TripFlag"] = "B1";
            }
            else if (gvLegDetails.Rows.Count == 1)
            {
                drMappingDetails["TripFlag"] = "B2";
            }
            else if (gvLegDetails.Rows.Count == 2)
            {
                drMappingDetails["TripFlag"] = "B3";
            }

            drMappingDetails["TailNo"] = lblTailData.Text;
            drMappingDetails["tripno"] = lblTripData.Text;

            string[] strStartCitySplit = ddlStartCitySplit.Text.Split('-');

            if (strStartCitySplit.Length >= 1)
            {
                drMappingDetails["start"] = strStartCitySplit[1].ToString();
                drMappingDetails["startCity"] = strStartCitySplit[0].ToString();
            }

            string[] strEndCitySplit = ddlLeg1EndCity.Text.Split('-');
            if (strEndCitySplit.Length >= 1)
            {
                drMappingDetails["End"] = strEndCitySplit[1].ToString();
                drMappingDetails["EndCity"] = strEndCitySplit[0].ToString();
            }

            drMappingDetails["DTime"] = txtSplitLeg1StartTime.Text;
            drMappingDetails["ATime"] = txtSplitLeg1EndTime.Text;
            drMappingDetails["FlyinghoursFOS"] = txtSpliFlyHour.Value.Trim();
            dtMapping.Rows.Add(drMappingDetails);


            ViewState["dtLegDetails"] = dtMapping;
            gvLegDetails.DataSource = dtMapping;
            gvLegDetails.DataBind();

            ddlStartCitySplit.Text = ddlLeg1EndCity.Text.Trim();
            //ddlStartCitySplit.Text = string.Empty;
            ddlLeg1EndCity.Text = string.Empty;
            txtSpliFlyHour.Value = "";

            if (gvLegDetails.Rows.Count == 3)
            {
                btnAddLeg.Visible = false;
            }
            else
            {
                btnAddLeg.Visible = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvLegDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtLegDetails"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtLegDetails"] = dt;
            gvLegDetails.EditIndex = -1;
            gvLegDetails.DataSource = ViewState["dtLegDetails"];
            gvLegDetails.DataBind();

            if (gvLegDetails.Rows.Count == 3)
            {
                btnAddLeg.Visible = false;
            }
            else
            {
                btnAddLeg.Visible = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvArrivalAirport_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtArrAirport"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtArrAirport"] = dt;
            gvArrivalAirport.EditIndex = -1;
            gvArrivalAirport.DataSource = ViewState["dtArrAirport"];
            gvArrivalAirport.DataBind();

            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }


    protected void gvDepAirport_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtDepAirport"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtDepAirport"] = dt;
            gvDepAirport.EditIndex = -1;
            gvDepAirport.DataSource = ViewState["dtDepAirport"];
            gvDepAirport.DataBind();

            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            genclass.FillControl(ddlState, "Select Row_ID,State_Name from State where Active = 'Y' and Country_ID='" + ddlCountry.SelectedValue + "' order by State_Name", "ALL");
            ddlCountry.Focus();

            if (rblDepartureSetup.SelectedValue == "ST")
            {
                dvStates.Visible = true;

                DataTable dt = genclass.GetDataTable("select Row_ID, State_Code,State_Name from  JETEDGE_FUEL..State where Country_ID='" + ddlCountry.SelectedValue + "'");
                gvStates.DataSource = dt;
                gvStates.DataBind();
            }
            else
            {
                dvStates.Visible = false;
                gvStates.DataSource = null;
                gvStates.DataBind();
            }

            mpeRoute.Show();
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

        }
    }
    protected void ddlCountry1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();

            BindAirport_Country();

            foreach (GridViewRow row in gvAirport.Rows)
            {
                CheckBox chkChoose = ((CheckBox)row.FindControl("chkChoose"));
                chkChoose.Checked = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);

        }
    }
    protected void rblDepartureSetup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();

            dvDepartureAirport.Visible = false;
            dvDepartureState.Visible = false;
            dvCountry.Visible = false;

            rblAirportMap.SelectedIndex = 0;

            if (rblDepartureSetup.SelectedValue == "AI")
            {
                dvDepartureAirport.Visible = true;

                dvStates.Visible = false;
                gvStates.DataSource = null;
                gvStates.DataBind();
                divCountry.Visible = false;
            }
            else if (rblDepartureSetup.SelectedValue == "ST")
            {
                dvDepartureState.Visible = true;

                dvStates.Visible = true;
                DataTable dt = genclass.GetDataTable("select Row_ID, State_Code,State_Name from  JETEDGE_FUEL..State where Country_ID='" + ddlCountry.SelectedValue + "'");
                gvStates.DataSource = dt;
                gvStates.DataBind();

                divCountry.Visible = false;
            }
            else if (rblDepartureSetup.SelectedValue == "CO")
            {
                dvCountry.Visible = false;

                dvStates.Visible = false;
                gvStates.DataSource = null;
                gvStates.DataBind();
                dvDepartureState.Visible = false;
                divCountry.Visible = true;
                //dvDepartureState2.Visible = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    public void BindCountryGrid()
    {
        DataTable dt = genclass.GetDataTable("Select Row_ID,Country_Name,Country_Code from JETEDGE_FUEL..Country where Active = 'Y' order by Country_Name");

        if (dt.Rows.Count > 0)
        {
            gvCountry.DataSource = dt;
            gvCountry.DataBind();
        }
        else
        {
            gvCountry.DataSource = null;
            gvCountry.DataBind();
        }
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();
            BindAirport();

            if (ddlState.SelectedIndex > 0)
            {
                dvDepartureState1.Visible = false;
            }

            foreach (GridViewRow row in gvAirport.Rows)
            {
                CheckBox chkChoose = ((CheckBox)row.FindControl("chkChoose"));
                chkChoose.Checked = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Clear_Popup();
            BindCountryGrid();
            ddlCountry_SelectedIndexChanged(sender, e);
            ddlCountry1_SelectedIndexChanged(sender, e);
            btnRouteAdd.Text = "Submit";
            if (lnkSearch.CommandName == "Start" || btnAddDeparture.CommandName == "Start")
            {
                lblAirportFor.Text = "Start";
            }
            else if (lnkSearch.CommandName == "End" || btnAddArrival.CommandName == "Start")
            {
                lblAirportFor.Text = "End";
            }
            dvStates.Visible = false;
            divCountry.Visible = false;
            mpeRoute.Show();
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    private void Clear_Popup()
    {
        rblDepartureSetup.ClearSelection();
        rblDepartureSetup.SelectedIndex = 0;
        txtFromICAO.Text = string.Empty;

        dvDepartureAirport.Visible = true;
        dvDepartureState.Visible = false;
        dvDepartureState1.Visible = false;
        dvDepartureState2.Visible = false;
        dvCountry.Visible = false;

        BindCountry();
    }
    private void BindCountry()
    {
        genclass.FillControl(ddlCountry, "Select Row_ID, LTRIM(RTRIM(Country_Name)) Country_Name from JETEDGE_FUEL..Country where Active = 'Y' order by Country_Name");
        genclass.FillControl(ddlCountry1, "Select Row_ID, LTRIM(RTRIM(Country_Name)) Country_Name from JETEDGE_FUEL..Country where Active = 'Y' order by Country_Name");

        ddlCountry.SelectedValue = "1";
        ddlCountry1.SelectedValue = "1";

        ddlState.Items.Clear();
        ddlState.Items.Insert(0, new ListItem("Not Available", "0"));
    }


    protected void btnRouteAddDep_Click(object sender, EventArgs e)
    {
        string strAirport = string.Empty;
        string strCode = string.Empty;
        DataRow drDepAirport;
        DataTable dtDepAIrport = new DataTable();
        try
        {
            if (ViewState["dtDepAirport"] != null)
            {
                dtDepAIrport = (DataTable)ViewState["dtDepAirport"];
            }
            else
            {
                dtDepAIrport.Columns.Clear();
                dtDepAIrport.Columns.Add("SetupBy");
                dtDepAIrport.Columns.Add("Airport");
                dtDepAIrport.Columns.Add("StateCode");
                dtDepAIrport.Columns.Add("CountryCode");
                dtDepAIrport.Columns.Add("AirportCode");
                dtDepAIrport.Columns.Add("CreatedBy");
                dtDepAIrport.Columns.Add("CreatedOn");
            }

            drDepAirport = dtDepAIrport.NewRow();

            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                drDepAirport["SetupBy"] = "Airport";
                drDepAirport["Airport"] = txtModDepCity.Text.Trim();

                if (txtModDepCity.Text.Contains("-"))
                {
                    string strstartcity2 = txtModDepCity.Text.Replace(" - ", "-");
                    String[] strliststart = strstartcity2.Split('-');
                    strCode = strliststart[1].ToString();
                }

                drDepAirport["AirportCode"] = strCode.Trim();

            }
            else if (ddlDepSetupBy.SelectedValue == "CO")
            {
                drDepAirport["SetupBy"] = "Country";
                drDepAirport["Airport"] = txtModDepCountry.Text.Trim();

                if (txtModDepCountry.Text.Contains(','))
                {
                    string[] strSplit = txtModDepCountry.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        strCode = strSplit[1].ToString().Trim();
                    }
                }

                drDepAirport["CountryCode"] = strCode.Trim();
            }
            else if (ddlDepSetupBy.SelectedValue == "ST")
            {
                drDepAirport["SetupBy"] = "State";
                drDepAirport["Airport"] = txtModDepState.Text.Trim();

                if (txtModDepState.Text.Length > 0)
                {
                    string[] strSplit = txtModDepState.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                        if (strSTCode.Length > 1)
                        {
                            strCode = strSTCode[0].ToString().Trim();
                        }
                    }
                }

                drDepAirport["StateCode"] = strCode.Trim();

            }

            drDepAirport["CreatedBy"] = lblUser.Text;
            drDepAirport["CreatedOn"] = DateTime.Now.ToString();
            dtDepAIrport.Rows.Add(drDepAirport);

            ViewState["dtDepAirport"] = dtDepAIrport;
            gvDepAirport.DataSource = dtDepAIrport;
            gvDepAirport.DataBind();

            if (gvDepAirport.Rows.Count > 0)
            {
                string strAirportCode = string.Empty;
                string strAirportName = string.Empty;

                string strStateCode = string.Empty;
                string strStateName = string.Empty;

                string strCountryCode = string.Empty;
                string strCountryName = string.Empty;

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strAirportName += lblAirport.Text + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Split(',');

                            if (strSplit.Length > 1)
                            {
                                strStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strCountryCode += strSplit[1].ToString().Trim() + "/";
                                strCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }

                divDepLan.Visible = true;

                txtDepDisLandPage.Text = (strCountryName + strStateCode + strAirportName).TrimEnd('/');
            }
            else
            {
                divDepLan.Visible = false;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:GetDepAirport();", true);
            txtModDepCity.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;
            txtModDepState.Text = string.Empty;


        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnRouteAddArr_Click(object sender, EventArgs e)
    {
        string strAirport = string.Empty;
        string strCode = string.Empty;
        DataRow drArrAirport;
        DataTable dtArrAIrport = new DataTable();
        try
        {
            if (ViewState["dtArrAirport"] != null)
            {
                dtArrAIrport = (DataTable)ViewState["dtArrAirport"];
            }
            else
            {
                dtArrAIrport.Columns.Clear();
                dtArrAIrport.Columns.Add("SetupBy");
                dtArrAIrport.Columns.Add("Airport");
                dtArrAIrport.Columns.Add("StateCode");
                dtArrAIrport.Columns.Add("CountryCode");
                dtArrAIrport.Columns.Add("AirportCode");
                dtArrAIrport.Columns.Add("CreatedBy");
                dtArrAIrport.Columns.Add("CreatedOn");

            }
            drArrAirport = dtArrAIrport.NewRow();

            if (ddlArrSetupBy.SelectedValue == "CT")
            {
                drArrAirport["SetupBy"] = "Airport";
                drArrAirport["Airport"] = txtArrAirport.Text;
                if (txtArrAirport.Text.Contains("-"))
                {
                    string strstartcity2 = txtArrAirport.Text.Replace(" - ", "-");
                    String[] strliststart = strstartcity2.Split('-');
                    strCode = strliststart[1];
                }

                drArrAirport["AirportCode"] = strCode.Trim();
            }
            else if (ddlArrSetupBy.SelectedValue == "CO")
            {
                drArrAirport["SetupBy"] = "Country";
                drArrAirport["Airport"] = txtArrCountry.Text;
                if (txtArrCountry.Text.Contains(','))
                {
                    string[] strSplit = txtArrCountry.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        strCode = strSplit[1].ToString().Trim();
                    }
                }

                drArrAirport["CountryCode"] = strCode.Trim();
            }
            else if (ddlArrSetupBy.SelectedValue == "ST")
            {
                drArrAirport["SetupBy"] = "State";
                drArrAirport["Airport"] = txtArrState.Text;
                if (txtArrState.Text.Length > 0)
                {
                    string[] strSplit = txtArrState.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                        if (strSTCode.Length > 1)
                        {
                            strCode = strSTCode[0].ToString().Trim();
                        }
                    }
                }
                drArrAirport["StateCode"] = strCode.Trim();
            }

            drArrAirport["CreatedBy"] = lblUser.Text;
            drArrAirport["CreatedOn"] = DateTime.Now.ToString();
            dtArrAIrport.Rows.Add(drArrAirport);

            ViewState["dtArrAirport"] = dtArrAIrport;
            gvArrivalAirport.DataSource = dtArrAIrport;
            gvArrivalAirport.DataBind();

            if (gvArrivalAirport.Rows.Count > 0)
            {
                string strEndAirportCode = string.Empty;
                string strEndAirportName = string.Empty;

                string strEndStateCode = string.Empty;
                string strEndStateName = string.Empty;

                string strEndCountryCode = string.Empty;
                string strEndCountryName = string.Empty;
                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strEndAirportName += lblAirport.Text.Trim() + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                strEndCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }
                divArrLan.Visible = true;
                txtArrDisLandPage.Text = (strEndCountryName + strEndStateCode + strEndAirportName).TrimEnd('/');
            }
            else
            {
                divArrLan.Visible = false;
            }

            txtArrAirport.Text = string.Empty;
            txtArrCountry.Text = string.Empty;
            txtArrState.Text = string.Empty;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:GetArrivalAirport();", true);



        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void rblAirportMap_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeRoute.Show();
            dvDepartureState2.Visible = false;

            if (rblAirportMap.SelectedValue == "ALL")
            {
                dvDepartureState2.Visible = false;
            }
            else
            {
                dvDepartureState2.Visible = true;
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    private void BindAirport_Country()
    {
        DataTable dtAirport = BindAirport_Country(ddlCountry1.SelectedValue);
        if (dtAirport.Rows.Count > 0)
        {
            gvAirport.DataSource = dtAirport;
            gvAirport.DataBind();
        }
        else
        {
            gvAirport.DataSource = null;
            gvAirport.DataBind();
        }
    }
    private void BindAirport()
    {
        DataTable dtAirport = BindAirport(ddlCountry.SelectedValue, ddlState.SelectedValue);
        if (dtAirport.Rows.Count > 0)
        {
            gvAirport.DataSource = dtAirport;
            gvAirport.DataBind();
        }
        else
        {
            gvAirport.DataSource = null;
            gvAirport.DataBind();
        }
    }
    public DataTable BindAirport(string CountryId, string StateId)
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", false);
        GetLegDetails[0].Value = "SEL";
        GetLegDetails[1].Value = CountryId;
        GetLegDetails[2].Value = StateId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", GetLegDetails).Tables[0];
    }

    public DataTable BindAirport_Country(string CountryId)
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", false);
        GetLegDetails[0].Value = "SELC";
        GetLegDetails[1].Value = CountryId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_Get_Airport_Dropdown", GetLegDetails).Tables[0];
    }

    public DataTable AirRouteCalculation(string StartAirport, string strEndAirport, string strSpeed)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
        string strRoot = StartAirport + "-" + strEndAirport;

        string WEBSERVICE_URL = "https://greatcirclemapper.p.rapidapi.com/airports/route/";
        WEBSERVICE_URL = WEBSERVICE_URL + strRoot + @"/" + strSpeed;

        var jsonResponse = "";

        var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
        if (webRequest != null)
        {
            webRequest.Method = "GET";
            webRequest.Timeout = 20000;
            webRequest.ContentType = "text/html;charset=UTF-8"; //"application/json";

            webRequest.Headers.Add("X-RapidAPI-Host", "greatcirclemapper.p.rapidapi.com");
            webRequest.Headers.Add("X-RapidAPI-Key", SecretsBLL.RapidAPIKey.ToString());
            webRequest.Headers.Add("vary", "Accept-Encoding");

            //webRequest.Headers.Add("content-type", "text/html;charset=UTF-8");

            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                {
                    jsonResponse = sr.ReadToEnd();
                }
            }
        }
        LegDetails8 leg = new LegDetails8();
        List<LegDetails8> RouteListLeg = new List<LegDetails8>();

        JavaScriptSerializer js = new JavaScriptSerializer();
        dynamic d = js.Deserialize<dynamic>(jsonResponse.ToString());

        dynamic[] arrlegs = d["legs"];

        for (int i = 0; i < arrlegs.Length; i++)
        {
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(arrlegs[i]);

            dynamic dInnerLeg = js.Deserialize<dynamic>(json);

            leg = new LegDetails8();

            leg.legNo = Convert.ToString(i + 1);
            leg.startICAO = dInnerLeg["origin"]["ident"];
            leg.endICAO = dInnerLeg["destination"]["ident"];
            leg.distance_km = Convert.ToString(arrlegs[i]["distance_km"]);
            leg.distance_nm = Convert.ToString(arrlegs[i]["distance_nm"]);
            leg.flight_time_min = Convert.ToString(arrlegs[i]["flight_time_min"]);
            leg.heading_deg = Convert.ToString(arrlegs[i]["heading_deg"]);
            leg.heading_compass = Convert.ToString(arrlegs[i]["heading_compass"]);

            RouteListLeg.Add(leg);
        }

        DataTable dtResul = ToDataTable(RouteListLeg);

        return dtResul;

    }
    public DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }

        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }

        //put a breakpoint here and check datatable
        return dataTable;

    }
    public DataTable RapidAPIDatatable()
    {
        DataTable dtRapid = new DataTable();
        dtRapid.Columns.Add("legNo");
        dtRapid.Columns.Add("startICAO");
        dtRapid.Columns.Add("endICAO");
        dtRapid.Columns.Add("distance_km");
        dtRapid.Columns.Add("distance_nm");
        dtRapid.Columns.Add("flight_time_min");
        dtRapid.Columns.Add("heading_deg");
        dtRapid.Columns.Add("heading_compass");

        DataRow row = dtRapid.NewRow();
        row["legNo"] = 1;
        row["startICAO"] = "KVNY";
        row["endICAO"] = "KSNA";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        row = dtRapid.NewRow();
        row["legNo"] = 2;
        row["startICAO"] = "KSNA";
        row["endICAO"] = "KVNY";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        return dtRapid;


    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText)
    {
        return AutoFillProducts(prefixText);

    }


    private static List<string> AutoFillProducts(string prefixText)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {
                com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;


            }

        }
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCity(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepState(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select S.State_Name+' , '+S.State_Code+ ' - '+C.Country_Name as State from JETEDGE_FUEL..State S Left Join JETEDGE_FUEL..Country C on C.Row_ID=S.Country_ID where " + "State_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["State"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCountry(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct Country_Name+ ' , '+Country_Code as Country from JETEDGE_FUEL..Country where " + "Country_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["Country"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }
}


public class LegDetails8
{
    public string legNo { get; set; }
    public string startICAO { get; set; }
    public string endICAO { get; set; }
    public string distance_km { get; set; }
    public string distance_nm { get; set; }
    public string flight_time_min { get; set; }
    public string heading_deg { get; set; }
    public string heading_compass { get; set; }
}