﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="AirportGroup.aspx.cs" Inherits="Views_AirportGroup" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        #MainContent_gvGroup tbody tr td, #MainContent_gvGroupArrival tbody tr td
        {
            padding-top: 5px;
            padding-bottom: 5px;
        }
        #MainContent_gvGroup tbody tr td tbody tr td, #MainContent_gvGroupArrival tbody tr td tbody tr td
        {
            padding-top: 1px;
            padding-bottom: 1px;
            border: none !important;
            padding-left: 7px;
        }
        #MainContent_gvGroup tbody tr th, #MainContent_gvGroupArrival tbody tr th
        {
            padding-top: 0.1rem;
            padding-bottom: 0.1rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="form-group row">
                            <div class="col-xl-12 text-right">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" TabIndex="1"
                                    OnClick="btnAddonClick" />
                                <label id="lblMan" runat="server" class="importantlabels" visible="false">
                                    <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                        ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                                </label>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                    ValidationGroup="SP" OnClick="btnDepCreateGroup" />
                                <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                                    OnClick="btnViewonClick" />
                                <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                    runat="server" ShowSummary="false" />
                            </div>
                        </div>
                        <div class="row" id="tblGrid" runat="server">
                            <div class="col-xl-12 table-responsive boxshadow">
                                <asp:GridView ID="gvGroupArrival" runat="server" AutoGenerateColumns="False" Width="100%"
                                    CssClass="table" HeaderStyle-CssClass="thead-dark" EmptyDataText="No Data Found"
                                    OnSelectedIndexChanged="gvGroup_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                                                <asp:Label ID="lblAirportId" runat="server" Text='<%# Bind("RowID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Group Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGroupName" runat="server" Text='<%# Bind("GroupName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Width="25%" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <table width="100%" style="border: 0px;">
                                                    <tr>
                                                        <td valign="top" align="center" style="width: 20% !important; text-align: left">
                                                            <asp:Label ID="Head1" runat="server" Text="Setup By"></asp:Label>
                                                        </td>
                                                        <td valign="top" align="center" style="width: 30% !important; text-align: left">
                                                            <asp:Label ID="Head2" runat="server" Text="Airport Setup By"></asp:Label>
                                                        </td>
                                                        <td valign="top" align="center" style="width: 50% !important; text-align: left">
                                                            <asp:Label ID="Head3" runat="server" Text="Airport"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:DataList ID="rptAirports" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                                    Width="100%" Style="border: none;">
                                                    <ItemTemplate>
                                                        <table width="100%" style="border: none !important;">
                                                            <tr>
                                                                <td valign="top" style="border: none !important; width: 20%; text-align: left">
                                                                    <asp:Label ID="lblDepSetupBy" runat="server" Text='<%# Bind("DepSetupByText") %>'></asp:Label>
                                                                </td>
                                                                <td valign="top" style="border: none !important; width: 30%; text-align: left">
                                                                    <asp:Label ID="lblSetupBy" runat="server" Text='<%# Bind("SetupBy") %>'></asp:Label>
                                                                </td>
                                                                <td valign="top" style="border: none !important; width: 50%; text-align: left">
                                                                    <asp:Label ID="lblAirports" runat="server" Text='<%# Bind("Airport") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="4%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblActive" runat="server" Text='<%# Bind("ActiveFlag") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreatedBy" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Bind("CreatedBy") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="9%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedON" runat="server" Text='<%# Bind("createdOn") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbView1" runat="server" TabIndex="2" ImageUrl="~/Images/pencil_Edit.png"
                                                    CommandName="Select" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbDelete" runat="server" TabIndex="3" ImageUrl="~/Images/icon_delete.png"
                                                    Style="color: White;" CausesValidation="false" AlternateText="Delete group details"
                                                    OnClick="imbGroupDepDeleteOnclick" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div id="tblForm" runat="server" visible="false">
                            <div class="row">
                                <div class="col-xl-2">
                                </div>
                                <div class="col-xl-8">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">
                                            Group Name <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-9">
                                            <asp:TextBox ID="txtGname" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtGname"
                                                Display="None" ErrorMessage="Group Name is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">
                                            Choose Airport <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-3">
                                            <asp:DropDownList ID="ddlDepSetupBy" runat="server" OnSelectedIndexChanged="ddlDepSetupBy_SelectedIndexChanged"
                                                AutoPostBack="true">
                                                <asp:ListItem Text="By Airport" Value="CT" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="By State" Value="ST"></asp:ListItem>
                                                <asp:ListItem Text="By Country" Value="CO"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-6">
                                            <asp:TextBox ID="txtMod_DepCity" runat="server" MaxLength="50" class="cusinputuser"
                                                TabIndex="8" autocomplete="off" AutoCompleteType="Disabled" Style="text-transform: uppercase;" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ControlToValidate="txtMod_DepCity"
                                                Display="None" ErrorMessage="Airport is required." SetFocusOnError="True" ValidationGroup="DepAdd" />
                                            <asp:TextBox ID="txtModDepState" runat="server" MaxLength="50" class="cusinputuser"
                                                TabIndex="8" Visible="false" autocomplete="off" AutoCompleteType="Disabled" Style="text-transform: uppercase;" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ControlToValidate="txtModDepState"
                                                Display="None" ErrorMessage="Airport is required." SetFocusOnError="True" ValidationGroup="DepAdd" />
                                            <asp:TextBox ID="txtModDepCountry" runat="server" MaxLength="50" class="cusinputuser"
                                                TabIndex="8" Visible="false" autocomplete="off" AutoCompleteType="Disabled" Style="text-transform: uppercase;" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="txtModDepCountry"
                                                Display="None" ErrorMessage="Airport is required." SetFocusOnError="True" ValidationGroup="DepAdd" />
                                            <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                                CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                                CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                                CompletionSetCount="1" TargetControlID="txtMod_DepCity" ID="AutoCompleteExtender6"
                                                runat="server" FirstRowSelected="true">
                                            </asp:AutoCompleteExtender>
                                            <asp:AutoCompleteExtender ServiceMethod="DepState" MinimumPrefixLength="1" CompletionListCssClass="completionList"
                                                CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                                CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtModDepState"
                                                ID="AutoCompleteExtender8" runat="server" FirstRowSelected="true">
                                            </asp:AutoCompleteExtender>
                                            <asp:AutoCompleteExtender ServiceMethod="DepCountry" MinimumPrefixLength="1" CompletionListCssClass="completionList"
                                                CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem"
                                                CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtModDepCountry"
                                                ID="AutoCompleteExtender9" runat="server" FirstRowSelected="true">
                                            </asp:AutoCompleteExtender>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-3 col-form-label">
                                            Setup By
                                        </label>
                                        <div class="col-9 col-form-label">
                                            <asp:RadioButtonList ID="rblSetupBy" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Departure" Value="DEP"></asp:ListItem>
                                                <asp:ListItem Text="Arrival" Value="ARR"></asp:ListItem>
                                                <asp:ListItem Text="All (Departure + Arrival)" Value="ALL" Selected="True"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label">
                                            Active
                                        </label>
                                        <div class="col-4 col-form-label">
                                            <asp:RadioButtonList ID="rblActive" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div class="col-5 text-right">
                                            <asp:Button ID="btnAddDeparture" runat="server" Text="Add to List" CssClass="btn btn-danger"
                                                CommandName="Start" OnClick="btnRouteAddDep_Click" TabIndex="14" ValidationGroup="DepAdd" />
                                            <asp:ValidationSummary ID="ValidationSummary7" ValidationGroup="DepAdd" ShowMessageBox="true"
                                                runat="server" ShowSummary="false" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <asp:GridView ID="gvDepAirport" runat="server" PagerStyle-CssClass="pager" AutoGenerateColumns="false"
                                                EmptyDataText="Airport details not found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                                AllowPaging="false" Width="100%" OnRowDeleting="gvDepAirport_RowDeleting">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Setup By" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="7%"
                                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDepSetupBy" Text='<%# Eval("DepSetupBy") %>' Visible="false" runat="server" />
                                                            <asp:Label ID="lblDepSetupByText" Text='<%# Eval("DepSetupByText") %>' runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Airport Setup By" ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-Width="5%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSetupBy" Text='<%# Eval("SetupBy") %>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Airport" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false"
                                                        HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="8%" HeaderStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStartCity" Text='<%# Eval("Airport") %>' runat="server"></asp:Label>
                                                            <asp:Label ID="lblStateCode" Text='<%# Eval("StateCode") %>' runat="server" Visible="false" />
                                                            <asp:Label ID="lblCountryCode" Text='<%# Eval("CountryCode") %>' runat="server" Visible="false" />
                                                            <asp:Label ID="lblAirportCode" Text='<%# Eval("AirportCode") %>' runat="server" Visible="false" />
                                                            <asp:Label ID="lblStateId" Text='<%# Eval("StateID") %>' runat="server" Visible="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Created By/On" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false"
                                                        HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="8%" HeaderStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCreatedBy" Text='<%# Eval("CreatedBy") %>' runat="server" />
                                                            <br />
                                                            <asp:Label ID="lblCreatedOn" Text='<%# Eval("CreatedOn") %>' runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                                                CommandName="Delete" CausesValidation="false" AlternateText="Delete Airport details"
                                                                OnClientClick="return confirm('Are you sure you want to delete this Airport details?');" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                        <div class="col-12">
                                            <div style="float: left; padding-top: 10px; padding-left: 15px;">
                                                <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                                    font-weight: 600;"></asp:Label>
                                                <asp:Label ID="lblLMby" runat="server" Style="font-weight: 600;"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" OnClick="btnConfirmDelete" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="lblrowDeleteId" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
