﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Net.Mime;
using Microsoft.Exchange.WebServices.Data;

public partial class Views_Customer : System.Web.UI.Page
{
    #region Global Declaration

    public int linenum = 0;
    public string MethodName = string.Empty;

    protected BusinessLayer.CreateCustomerBLL objMember = new BusinessLayer.CreateCustomerBLL();

    string STARTICAO = string.Empty;
    string STARTStateCode = string.Empty;
    string STARTCountryCode = string.Empty;
    string STARTCity = string.Empty;
    string STARTIATA = string.Empty;


    string ENDICAO = string.Empty;
    string ENDStateCode = string.Empty;
    string ENDCountryCode = string.Empty;
    string ENDCity = string.Empty;
    string ENDIATA = string.Empty;


    #endregion

    #region Pageload

    /// <summary>
    /// 1. Checks the cookie values and redirects to session Expired page
    /// 2. Bind the Page header menu name
    /// 3. Assign the Cookies values in hidden labels
    /// 4. Bind Country and Phone code dropdown
    /// 5. Get the List of airports and Bind the customer details.
    /// </summary>
    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                // Put user code to initialize the page here
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Customer");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "General Setup";
                lblSubHeader.InnerText = "Customer";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                Session["ICAOC"] = null;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;

                GetAirport();

                genclass.Bind_Country_Dropdown(ddlCountry);
                genclass.Bind_PhoneCode_Dropdown(ddlPhoneCode, "0");

                List();

                CalendarExtender1.StartDate = DateTime.Now;
                CalendarExtender4.StartDate = DateTime.Now;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {

        this.Load += new System.EventHandler(this.Page_Load);
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnAddPref.Click += new System.EventHandler(this.btnAddPref_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);

        //this.rblIsBroker.SelectedIndexChanged += new System.EventHandler(this.rblIsBroker_SelectedIndexChanged);
        this.rblflytime1.SelectedIndexChanged += new System.EventHandler(this.rblflytime1_OnSelectedIndexChanged);
        this.txtusername.TextChanged += new System.EventHandler(this.txtusername_changed);
        this.btnAddMore.Click += new System.EventHandler(this.btnSaveprefrernceOnClick);
        this.btnReset.Click += new System.EventHandler(this.btnResetClick);
        this.chkCopy.CheckedChanged += new System.EventHandler(this.chkChoose_CheckedChanged);
    }

    #endregion

    #region Button Events

    /// <summary>
    /// 1. Export the customer details in Excel format
    /// </summary>
    private void btnExportExcel_Click(object sender, System.EventArgs e)
    {
        try
        {
            string strName = string.Empty;

            string strQry = string.Empty;
            strQry += "SELECT Title, FirstName as [First Name], LastName as [Last Name], Email, CASE WHEN BrokerFlag = 'Y' then 'Broker' else 'Customer' end as [User Type], ISNULL(Code_Phone,'') + '' + Replace(PhoneNumber,'.','') as [Phone Number], ";
            strQry += "[Address] as [Address 1], Address2 as [Address 2], ";
            strQry += "City, [state] as [State], Country, ZipCode, CASE WHEN MustMoveMailFlag ='Y' THEN 'Yes' ELSE 'No' END as [Must Move Mail], FORMAT(USR_CreatedOn,'MMMM d, yyyy HH:mm') as [Created On] ";
            strQry += "FROM UserMasterCreate ";
            strQry += "WHERE [Role] = 'User' ORDER BY FirstName ";

            DataTable dtCustomer = genclass.GetDataTable(strQry);

            strName = "Customer_Details_" + DateTime.Now.ToString("ddMMMyyyy");

            objMember.Exportexcel(dtCustomer, strName, DateTime.Now.ToString("MMM/dd/yyyy"), lblUser.Text, "Customer Details");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. Based on the user type displays the Broker details
    /// </summary>
    public void ddlUserType_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            lblCurrentUserType.Text = ddlUserType.SelectedValue;

            if (ddlUserType.SelectedValue == "Broker")
            {
                dvCopy.Visible = true;
                trBrokerAdress.Visible = true;
                lblPre.Text = "you can add 10 more preferences";

                if (gv_Child.Rows.Count == 10)
                {
                    btnAddMore.Visible = false;
                }
                else
                {
                    int c = 10 - gv_Child.Rows.Count;

                    lblPre.Text = "you can add " + c.ToString() + " more preferences";
                }
            }
            else
            {
                dvCopy.Visible = false;
                trBrokerAdress.Visible = false;
                lblPre.Text = "you can add 3 more preferences";

                if (gv_Child.Rows.Count == 3)
                {
                    btnAddMore.Visible = false;
                }
                else
                {
                    int c = 3 - gv_Child.Rows.Count;

                    lblPre.Text = "you can add " + c.ToString() + " more preferences";
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. Copy the Customer details to Broker Details
    /// </summary>
    private void chkChoose_CheckedChanged(object sender, System.EventArgs e)
    {
        try
        {
            if (chkCopy.Checked == true)
            {
                txtBrokerName.Text = txtFname.Text.Trim();
                txtBLName.Text = txtLastName.Text.Trim();
                txtManger.Text = txtFname.Text.Trim();
                txtBrokerAdd1.Text = txtAddress.Text.Trim();
                txtBrokerCIty.Text = txtCity.Text.Trim();
                txtBrokerState.Text = txtState.Text.Trim();
                txtBrokerCountry.Text = ddlCountry.SelectedItem.Text.Trim();
                txtBrokerZipCode.Text = txt_Zip.Text;
            }
            else
            {
                txtBrokerName.Text = string.Empty;
                txtBLName.Text = string.Empty;
                txtManger.Text = string.Empty;
                txtBrokerAdd1.Text = string.Empty;
                txtBrokerCIty.Text = string.Empty;
                txtBrokerState.Text = string.Empty;
                txtBrokerCountry.Text = string.Empty;
                txtBrokerZipCode.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. Checks the Email Exists or Not
    /// 2. Validate the Allowed pay methods
    /// 3. Assign the variables
    /// 4. Insert the customer details in database
    /// 5. Based on the User Type assign the Coupon Details
    /// 6. Insert / Update the Preferences
    /// 7. Send the Login details to the customer
    /// 8. Clear the Form Filds
    /// </summary>
    private void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            string strReturnVal1 = string.Empty;

            #region Email Address Exists

            DataTable dt = objMember.AlreadyExist(txtEmail.Text.Trim(), hdfPSRID.Value);
            if (dt.Rows.Count > 0)
            {
                txtEmail.Focus();
                lblalert.Text = "The email address [ " + txtEmail.Text + " ] has already been registered for a FLYT account";
                mpealert.Show();
                return;
            }

            #endregion

            #region Allowed Pay Methods Validation

            int PayMethodCount = 0;
            for (int j = 0; j < rblPayMethod.Items.Count; j++)
            {
                if (rblPayMethod.Items[j].Selected == true)
                {
                    PayMethodCount += 1;
                }
            }
            if (PayMethodCount == 0)
            {
                lblalert.Text = "Please choose atleast one Allowed Pay Methods.";
                mpealert.Show();
                return;
            }

            #endregion

            string autoGenPwd = string.Empty;
            autoGenPwd = genclass.RandomString(genclass.RandomNumber(8, 15));

            #region Variable Assignment

            if (btnSave.Text == "Save")
            {
                objMember.ManageType = "I";

                #region Password Criteria

                bool blValidated = genclass.ValidatePassword(txtNew_Password.Text);
                if (blValidated == false)
                {
                    lblalert.Text += "Password Criteria not matched. Password must contain <br /><br /> ";
                    lblalert.Text += "<span style='text-align: left; float: left; line-height: 30px;'> 1. At least one lower case letter <br /> ";
                    lblalert.Text += "2. At least one upper case letter<br />  ";
                    lblalert.Text += "3. Not include special character <br /> ";
                    lblalert.Text += "4. At least one number <br /> ";
                    lblalert.Text += "5. At least 8 characters length </span> ";

                    mpealert.Show();

                    return;
                }

                #endregion
            }
            else
            {
                objMember.ManageType = "U";
            }

            objMember.BrokerState = txtBrokerState.Text.Trim();
            objMember.BrokerCountry = txtBrokerCountry.Text.Trim();
            objMember.RowID = hdfPSRID.Value;
            objMember.Title = ddlgender.SelectedItem.Text.Trim();
            objMember.firstName = txtFname.Text.Trim();
            objMember.lastName = txtLastName.Text.Trim();
            objMember.Email = txtEmail.Text.Trim();

            string strPCode = genclass.GetPhoneCode(ddlCountry.SelectedValue.Trim());
            objMember.PhoneCode = strPCode.Trim();
            objMember.Phone = txtPhone.Value.Trim().Replace(".", "");
            objMember.Address1 = txtAddress.Text.Trim();
            objMember.Address2 = txtAddress2.Text.Trim();
            objMember.City = txtCity.Text.Trim();
            objMember.State = txtState.Text.Trim();
            objMember.Country = ddlCountry.SelectedItem.Text;
            objMember.ZipCode = txt_Zip.Text.Trim();
            objMember.Password = (genclass.Encrypt(txtNew_Password.Text));
            objMember.UserType = ddlUserType.SelectedValue.Trim(); //"User";
            lblCurrentUserType.Text = ddlUserType.SelectedValue.Trim();

            if (chkActive.Checked == true)
            {
                objMember.Active = "1";
            }
            else
            {
                objMember.Active = "0";
            }

            objMember.BrokerFlag = ddlUserType.SelectedValue.Trim() == "Broker" ? "Y" : "N";
            objMember.BrokerFirstName = txtBrokerName.Text.Trim();
            objMember.BrokerLastName = txtBLName.Text.Trim();
            objMember.Manger = txtManger.Text.Trim();
            objMember.BrokerAddress = txtBrokerAdd1.Text.Trim();
            objMember.BrokerCity = txtBrokerCIty.Text.Trim();

            objMember.BrokerZipCode = txtBrokerZipCode.Text.Trim();
            objMember.NumberDays = txtDays.Text.Trim();
            objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
            objMember.MustMoveMailFlag = rblMustMoveMail.SelectedValue.Trim();
            objMember.Countryid = ddlCountry.SelectedValue.Trim();

            string strPayMethod = string.Empty;
            for (int i = 0; i < rblPayMethod.Items.Count; i++)
            {
                if (rblPayMethod.Items[i].Selected == true)
                {
                    strPayMethod += rblPayMethod.Items[i].Value.Trim() + ",";
                }
            }
            objMember.PayMehod = strPayMethod.Trim().TrimEnd(',');

            #endregion

            string retVal = objMember.AddnewMember(objMember);
            if (retVal != "-1")
            {
                if (ddlUserType.SelectedValue != "JEUser")
                {
                    if (lblPreviousUserType.Text != lblCurrentUserType.Text)
                    {
                        string strSyncid = objMember.Sync_CouponDiscount(retVal);
                    }
                }
                else if (ddlUserType.SelectedValue == "JERetail")
                {
                    if (lblPreviousUserType.Text != lblCurrentUserType.Text)
                    {
                        string strSyncid = objMember.Sync_CouponDiscount_Delete(retVal);
                    }
                }
                else
                {
                    if (lblPreviousUserType.Text != lblCurrentUserType.Text)
                    {
                        string strSyncid = objMember.Sync_CouponDiscount_Delete(retVal);
                    }
                }

                #region Prefence

                DataTable dtprefence = new DataTable();
                dtprefence = (DataTable)ViewState["EmployeeName"];
                objMember.DeletePreference(retVal);

                if (dtprefence != null)
                {
                    for (int i = 0; i < dtprefence.Rows.Count; i++)
                    {
                        objMember.PreManageType = "I";
                        objMember.PreRowID = "0";
                        objMember.StartAirport = dtprefence.Rows[i]["Startairport"].ToString();
                        objMember.EndAirport = dtprefence.Rows[i]["endaiport"].ToString();
                        objMember.RowID = retVal;
                        objMember.StartDate = Convert.ToDateTime(dtprefence.Rows[i]["StartDate"].ToString()).ToString("yyyy-MM-dd");
                        objMember.Time = dtprefence.Rows[i]["time"].ToString();
                        objMember.EmailFlag = dtprefence.Rows[i]["Emailflag"].ToString();
                        objMember.StartCity = dtprefence.Rows[i]["startcity"].ToString();
                        objMember.EndCity = dtprefence.Rows[i]["endcity"].ToString();
                        if (dtprefence.Rows[i]["EndDate"].ToString() == string.Empty || dtprefence.Rows[i]["EndDate"].ToString() == "")
                        {
                            objMember.EndDate = null;
                        }
                        else
                        {
                            objMember.EndDate = Convert.ToDateTime(dtprefence.Rows[i]["EndDate"].ToString()).ToString("yyyy-MM-dd");
                        }

                        objMember.TimeFlag = dtprefence.Rows[i]["Timeflag"].ToString();
                        objMember.SMSFlag = dtprefence.Rows[i]["SMSFlag"].ToString();
                        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

                        objMember.StartICAODisp = dtprefence.Rows[i]["StartICAODisp"].ToString();
                        objMember.EndICAODisp = dtprefence.Rows[i]["EndICAODisp"].ToString();

                        strReturnVal1 = objMember.AddPreference(objMember);
                    }
                }

                DataTable dtgrid = new DataTable();
                dtgrid.Columns.Add("slno", System.Type.GetType("System.String"));
                dtgrid.Columns.Add("Startairport", System.Type.GetType("System.String"));
                dtgrid.Columns.Add("endairport", System.Type.GetType("System.String"));
                dtgrid.Columns.Add("date", System.Type.GetType("System.String"));
                dtgrid.Columns.Add("price", System.Type.GetType("System.String"));
                dtgrid.Columns.Add("tailno", System.Type.GetType("System.String"));
                DataRow dr1;
                for (int i = 0; i < gvSimpleSearch.Rows.Count; i++)
                {
                    CheckBox CheckBox1 = ((CheckBox)gvSimpleSearch.Rows[i].FindControl("CheckBox1"));

                    if (CheckBox1.Checked == true)
                    {
                        dr1 = dtgrid.NewRow();
                        dr1["slno"] = ((Label)gvSimpleSearch.Rows[i].FindControl("lblindex")).Text.Trim();
                        dr1["Startairport"] = ((Label)gvSimpleSearch.Rows[i].FindControl("Label2")).Text.Trim();
                        dr1["endairport"] = ((Label)gvSimpleSearch.Rows[i].FindControl("Label3")).Text.Trim();
                        dr1["date"] = ((Label)gvSimpleSearch.Rows[i].FindControl("Label4")).Text.Trim();
                        dr1["price"] = ((Label)gvSimpleSearch.Rows[i].FindControl("Label7")).Text.Trim();
                        dr1["tailno"] = ((Label)gvSimpleSearch.Rows[i].FindControl("lbltailno")).Text.Trim();
                        dtgrid.Rows.Add(dr1);
                    }
                }

                #endregion

                string strResult = string.Empty;
                string strResult11 = string.Empty;

                mpealert.Show();
                if (btnSave.Text == "Save")
                {
                    lblalert.Text = "Customer [ " + txtFname.Text + " " + txtLastName.Text + " ] details has been saved successfully";
                    strResult = SendEmail_logincreate(txtEmail.Text, txtFname.Text + " " + txtLastName.Text, autoGenPwd);

                    if (dtgrid.Rows.Count > 0)
                    {
                        strResult11 = SendEmail(txtEmail.Text, txtNew_Password.Text, txtFname.Text + " " + txtLastName.Text, retVal, dtgrid);
                    }
                }
                else
                {
                    lblalert.Text = "Customer [ " + txtFname.Text + " " + txtLastName.Text + " ] details has been updated successfully";
                }

                btnViewOnClick(sender, e);

                Clear();
            }
            else
            {
                //lblMessage.Value = "An error has occured while processing your request";
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. Clear the Form Fields and ViewState
    /// 2. Display the forms view
    /// </summary>
    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();

            ddlgender.Focus();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            btnReset.Visible = false;
            dvFormHeader.Visible = true;
            dvGridHeader.Visible = false;
            gv_Child.DataSource = null;
            gv_Child.DataBind();
            gv_Child.Visible = true;
            trPAsword.Visible = true;
            tripblock.Visible = false;
            divReason.Visible = false;
            divUnSub.Visible = false;

            ViewState["DisplayIndex"] = null;
            ViewState["EmployeeName"] = null;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. Display the Preference popup
    /// 2. Clear the Form Fields and ViewState
    /// </summary>
    private void btnAddPref_Click(object sender, System.EventArgs e)
    {
        try
        {
            mpeUserPref.Show();
            txtScity1.Text = string.Empty;
            txtEcity1.Text = string.Empty;
            txtDate1.Text = string.Empty;
            txtToDate1.Text = string.Empty;
            rblflytime1.SelectedIndex = 0;
            rblEmailFlag.SelectedIndex = 0;
            rblSMSNotifi.SelectedIndex = 0;
            txtTime1.Text = string.Empty;
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            trtime.Visible = false;
            ViewState["DisplayIndex"] = null;
            //ViewState["EmployeeName"] = null;
            btnAddMore.Text = "Add to List";

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. List all the Customer in Gridview
    /// 2. Based on the Customer Data display the forms and Buttons
    /// </summary>
    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();

            if (gvpricing.Rows.Count > 0)
            {
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
                btnReset.Visible = false;
                dvFormHeader.Visible = false;
                dvGridHeader.Visible = true;
            }
            else
            {
                tblForm.Visible = true;
                tblGrid.Visible = false;
                btnAdd.Visible = false;
                btnSave.Visible = true;
                btnView.Visible = true;
                btnReset.Visible = false;
                dvFormHeader.Visible = true;
                dvGridHeader.Visible = false;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    /// <summary>
    /// 1. Assign the Preference details in View State
    /// </summary>
    protected void btnSaveprefrernceOnClick(object sender, EventArgs e)
    {
        string startairport = string.Empty;
        string endairport = string.Empty;
        string startcity = string.Empty;
        string endcity = string.Empty;
        DataTable dtOutput = new DataTable();
        DataRow dr;

        try
        {
            string strstartcity = string.Empty;
            strstartcity = txtScity1.Text.Replace(",", "-").Replace(" ", "");
            String[] strliststart = strstartcity.Split('-');

            string strendcity = string.Empty;
            strendcity = txtEcity1.Text.Replace(",", "-").Replace(" ", "");
            String[] strlistend = strendcity.Split('-');

            if (strliststart.Length > 1)
            {
                startairport = strliststart[0].Trim();
                startcity = strliststart[1].Trim();
            }
            else
            {
                startcity = txtScity1.Text.Trim();
                startairport = "";
            }
            if (strlistend.Length > 1)
            {
                endairport = strlistend[0].Trim();
                endcity = strlistend[1].Trim();
            }
            else
            {
                endcity = txtEcity1.Text.Trim();
                endairport = "";
            }

            string emailflag = string.Empty;
            if (rblEmailFlag.SelectedIndex == 0)
            {
                emailflag = "Yes";
            }
            else
            {
                emailflag = "No";
            }
            string timeflag = string.Empty;
            if (rblflytime1.SelectedIndex == 0)
            {
                timeflag = "Anytime";
            }
            else
            {
                timeflag = "Specific Time";
            }

            string SMSFlag = string.Empty;
            if (rblSMSNotifi.SelectedIndex == 0)
            {
                SMSFlag = "No";
            }
            else
            {
                SMSFlag = "Yes";
            }



            if (ViewState["EmployeeName"] != null)
            {
                dtOutput = (DataTable)ViewState["EmployeeName"];
            }
            else
            {
                dtOutput.Columns.Add("slno", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("StartShow", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("Startcity", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("StartAirport", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("Endshow", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("Endcity", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("EndAiport", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("StartDate", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("EndDate", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("Timeflag", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("Emailflag", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("Time", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("SMSflag", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("StartICAODisp", System.Type.GetType("System.String"));
                dtOutput.Columns.Add("EndICAODisp", System.Type.GetType("System.String"));
            }
            if (btnAddMore.Text == "Add to List")
            {
                dr = dtOutput.NewRow();
                dr["slno"] = "0";
                dr["StartShow"] = startcity + " (" + startairport + ")";
                dr["Startcity"] = startcity;
                dr["StartAirport"] = startairport;
                dr["Endshow"] = endcity + " (" + endairport + ")";
                dr["Endcity"] = endcity;
                dr["EndAiport"] = endairport;
                if (txtDate1.Text == "")
                {
                    dr["StartDate"] = DateTime.Now.ToString("MMM dd,yyyy");
                }
                else
                {
                    dr["StartDate"] = Convert.ToDateTime(txtDate1.Text).ToString("MMM dd,yyyy");
                }
                if (txtToDate1.Text == "")
                {
                    dr["EndDate"] = null;
                }
                else
                {
                    dr["EndDate"] = Convert.ToDateTime(txtToDate1.Text).ToString("MMM dd,yyyy");
                }

                dr["Timeflag"] = timeflag;
                dr["Emailflag"] = emailflag;
                dr["Time"] = txtTime1.Text;
                dr["SMSflag"] = SMSFlag;
                dr["StartICAODisp"] = txtScity1.Text;
                dr["EndICAODisp"] = txtEcity1.Text;

                dtOutput.Rows.Add(dr);
            }
            else
            {
                if (ViewState["DisplayIndex"] != null)
                {
                    int Rows = Convert.ToInt32(ViewState["DisplayIndex"]);
                    dtOutput.Rows[Rows]["slno"] = "0";
                    dtOutput.Rows[Rows]["StartShow"] = startcity + " (" + startairport + ")";
                    dtOutput.Rows[Rows]["Startcity"] = startcity;
                    dtOutput.Rows[Rows]["StartAirport"] = startairport;
                    dtOutput.Rows[Rows]["Endshow"] = endcity + " (" + endairport + ")";
                    dtOutput.Rows[Rows]["Endcity"] = endcity;
                    dtOutput.Rows[Rows]["EndAiport"] = endairport;

                    if (txtDate1.Text == "")
                    {
                        dtOutput.Rows[Rows]["StartDate"] = DateTime.Now.ToString("MMM dd,yyyy");
                    }
                    else
                    {
                        dtOutput.Rows[Rows]["StartDate"] = Convert.ToDateTime(txtDate1.Text).ToString("MMM dd,yyyy");
                    }
                    if (txtToDate1.Text == "")
                    {

                        dtOutput.Rows[Rows]["EndDate"] = null;
                    }
                    else
                    {
                        dtOutput.Rows[Rows]["EndDate"] = Convert.ToDateTime(txtToDate1.Text).ToString("MMM dd,yyyy");
                    }

                    dtOutput.Rows[Rows]["Timeflag"] = timeflag;
                    dtOutput.Rows[Rows]["Emailflag"] = emailflag;
                    dtOutput.Rows[Rows]["Time"] = txtTime1.Text;
                    dtOutput.Rows[Rows]["SMSflag"] = SMSFlag;
                    dtOutput.Rows[Rows]["StartICAODisp"] = txtScity1.Text;
                    dtOutput.Rows[Rows]["EndICAODisp"] = txtEcity1.Text;

                    dtOutput.AcceptChanges();
                }
            }

            ViewState["EmployeeName"] = dtOutput;


            if (dtOutput.Rows.Count > 0)
            {
                gv_Child.DataSource = dtOutput;
                gv_Child.DataBind();
                gv_Child.Visible = true;

                if (ddlUserType.SelectedValue == "Broker")
                {
                    lblPre.Text = "you can add 10 more preferences";

                    if (gv_Child.Rows.Count >= 10)
                    {
                        btnAddPref.Visible = false;
                        lblPre.Text = "you can add 10 preferences only";
                    }
                    else
                    {
                        int c = 10 - gv_Child.Rows.Count;
                        if (c.ToString().Contains("-"))
                        {
                            lblPre.Text = "you can add 10 preferences only";
                        }
                        else
                        {
                            lblPre.Text = "you can add " + c.ToString() + " more preferences";
                        }
                    }

                }
                else
                {
                    lblPre.Text = "you can add 3 more preferences";
                    if (gv_Child.Rows.Count >= 3)
                    {
                        btnAddPref.Visible = false;
                        lblPre.Text = "you can add 3 preferences only";
                    }
                    else
                    {
                        int c = 3 - gv_Child.Rows.Count;

                        if (c.ToString().Contains("-"))
                        {
                            lblPre.Text = "you can add 3 preferences only";
                        }
                        else
                        {
                            lblPre.Text = "you can add " + c.ToString() + " more preferences";
                        }

                    }
                }
            }
            if (txtDate1.Text == "")
            {
                BindEmptyLegs(1, startairport, endairport, DateTime.Now.ToString("yyyy-MM-dd"));
            }
            else
            {
                BindEmptyLegs(1, startairport, endairport, Convert.ToDateTime(txtDate1.Text).ToString("yyyy-MM-dd"));
            }

            txtScity1.Text = string.Empty;
            txtEcity1.Text = string.Empty;
            txtDate1.Text = string.Empty;
            txtToDate1.Text = string.Empty;
            txtTime1.Text = string.Empty;
            rblEmailFlag.SelectedIndex = 0;
            rblflytime1.SelectedIndex = 0;
            trtime.Visible = false;
            if (ddlUserType.SelectedValue == "Broker")
            {
                trBrokerAdress.Visible = true;
                lblPre.Text = "you can add 10 more preferences";

                if (gv_Child.Rows.Count == 10)
                {
                    btnAddMore.Visible = false;
                }
                else
                {
                    int c = 10 - gv_Child.Rows.Count;

                    lblPre.Text = "you can add " + c.ToString() + " more preferences";
                }

            }
            else
            {
                trBrokerAdress.Visible = false;
                lblPre.Text = "you can add 3 more preferences";

                if (gv_Child.Rows.Count == 3)
                {
                    btnAddMore.Visible = false;
                }
                else
                {
                    int c = 3 - gv_Child.Rows.Count;

                    lblPre.Text = "you can add " + c.ToString() + " more preferences";
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. Display the time filed based on the flag
    /// </summary>
    public void rblflytime1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        mpeUserPref.Show();

        if (txtTime1.Text != "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
        }

        if (rblflytime1.SelectedIndex == 0)
        {
            trtime.Visible = false;
        }
        else
        {
            trtime.Visible = true;
        }
    }

    /// <summary>
    /// 1. Get the RowId from the Selected Row
    /// 2. Based on the RowId get and Bind the Customer Details
    /// 3. Bind the Preference Grid
    /// 4. Display the Form view
    /// </summary>
    protected void gvPricingDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvpricing.SelectedRow.FindControl("lblrowid");

            DataSet dtPS = objMember.Edit(lblrowid.Text.Trim());
            if (dtPS != null && dtPS.Tables[0].Rows.Count > 0)
            {
                trPAsword.Visible = false;
                hdfPSRID.Value = lblrowid.Text;
                ddlgender.SelectedIndex = ddlgender.Items.IndexOf(ddlgender.Items.FindByValue(dtPS.Tables[0].Rows[0]["title"].ToString()));

                ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(ddlUserType.Items.FindByValue(dtPS.Tables[0].Rows[0]["Role"].ToString()));
                ddlUserType_SelectedIndexChanged(sender, e);

                lblPreviousUserType.Text = dtPS.Tables[0].Rows[0]["Role"].ToString();
                lblCurrentUserType.Text = dtPS.Tables[0].Rows[0]["Role"].ToString();

                for (int j = 0; j < rblPayMethod.Items.Count; j++)
                {
                    rblPayMethod.Items[j].Selected = false;
                }

                string[] strPayMethod = dtPS.Tables[0].Rows[0]["PayMethod"].ToString().Trim().Split(',');
                if (strPayMethod.Length >= 1)
                {
                    for (int i = 0; i < strPayMethod.Length; i++)
                    {
                        for (int j = 0; j < rblPayMethod.Items.Count; j++)
                        {
                            if (rblPayMethod.Items[j].Value.Trim() == strPayMethod[i].ToString().Trim())
                            {
                                rblPayMethod.Items[j].Selected = true;
                            }
                        }
                    }
                }


                txtFname.Text = dtPS.Tables[0].Rows[0]["FirstName"].ToString();
                txtLastName.Text = dtPS.Tables[0].Rows[0]["LastName"].ToString();
                txtEmail.Text = dtPS.Tables[0].Rows[0]["Email"].ToString();

                txtPhone.Value = genclass.Format_Phone(dtPS.Tables[0].Rows[0]["PhoneNumber"].ToString());

                txtAddress2.Text = dtPS.Tables[0].Rows[0]["Address2"].ToString();
                txtAddress.Text = dtPS.Tables[0].Rows[0]["Address"].ToString();
                txtCity.Text = dtPS.Tables[0].Rows[0]["City"].ToString();
                txtState.Text = dtPS.Tables[0].Rows[0]["State"].ToString();
                ddlPhoneCode.SelectedIndex = ddlPhoneCode.Items.IndexOf(ddlPhoneCode.Items.FindByValue(dtPS.Tables[0].Rows[0]["Code_Phone"].ToString()));
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(dtPS.Tables[0].Rows[0]["CountryId"].ToString()));
                txt_Zip.Text = dtPS.Tables[0].Rows[0]["ZipCode"].ToString();

                this.txtNew_Password.Attributes.Add("value", this.txtNew_Password.Text);
                this.txtNew_Password.Attributes.Add("value", this.txtNew_Password.Text);

                if (dtPS.Tables[0].Rows[0]["Active"].ToString() == "1")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }

                string strCreatedBy = dtPS.Tables[0].Rows[0]["CreatedBYN"].ToString();
                if (strCreatedBy != "" && strCreatedBy != "as as" && dtPS.Tables[0].Rows[0]["USR_CreatedOn"].ToString() != "")
                {
                    divLMBy.Visible = true;
                    lblLMby.Text = strCreatedBy + " ( on ) " + Convert.ToDateTime(dtPS.Tables[0].Rows[0]["USR_CreatedOn"]).ToString("MMM dd, yyyy hh:mm tt");
                }
                else
                {
                    divLMBy.Visible = false;
                }

                txtBrokerName.Text = dtPS.Tables[0].Rows[0]["BrokerName"].ToString();
                txtManger.Text = dtPS.Tables[0].Rows[0]["Manager"].ToString();
                txtBrokerAdd1.Text = dtPS.Tables[0].Rows[0]["BrokerAddress1"].ToString();
                txtBLName.Text = dtPS.Tables[0].Rows[0]["BrokerAddress2"].ToString();
                txtBrokerCIty.Text = dtPS.Tables[0].Rows[0]["BrokerCity"].ToString();
                txtBrokerState.Text = dtPS.Tables[0].Rows[0]["BrokerState"].ToString();
                txtBrokerCountry.Text = dtPS.Tables[0].Rows[0]["BrokerCountry"].ToString();
                txtBrokerZipCode.Text = dtPS.Tables[0].Rows[0]["BrokerZipCode"].ToString();
                txtDays.Text = dtPS.Tables[0].Rows[0]["PayTermDays"].ToString();

                lblReason.Text = dtPS.Tables[0].Rows[0]["UnSubReason"].ToString();

                if (lblReason.Text.Length > 0)
                {
                    divReason.Visible = true;
                }
                else
                {
                    divReason.Visible = false;
                }

                rblMustMoveMail.SelectedIndex = rblMustMoveMail.Items.IndexOf(rblMustMoveMail.Items.FindByValue(dtPS.Tables[0].Rows[0]["MustMoveMailFlag"].ToString()));
                if (rblMustMoveMail.SelectedIndex == -1)
                {
                    rblMustMoveMail.SelectedValue = "Y";
                }

                if (rblMustMoveMail.SelectedValue == "Y")
                {
                    spnSubscribe.Text = "Subscribed By";
                }
                else
                {
                    spnSubscribe.Text = "Unsubscribed By";
                }

                string strUnSubBy = dtPS.Tables[0].Rows[0]["UnSubBy"].ToString();
                string strUnSubon = dtPS.Tables[0].Rows[0]["UnSubOn"].ToString();

                if (strUnSubBy.Length > 0 && strUnSubon.Length > 0)
                {
                    lblUnSubBy.Text = strUnSubBy + " (on) " + strUnSubon;
                    divUnSub.Visible = true;
                }
                else
                {
                    divUnSub.Visible = false;
                }


                if (dtPS.Tables[0].Rows[0]["Userflag"].ToString() == "ADMINUSER")
                {
                    userflag.Text = "ADMINUSER";

                    DataTable dtpreference = dtPS.Tables[1];

                    if (dtpreference != null && dtpreference.Rows.Count > 0)
                    {
                        ViewState["EmployeeName"] = dtpreference;
                        gv_Child.DataSource = dtpreference;
                        gv_Child.DataBind();
                    }
                    else
                    {
                        gv_Child.DataSource = null;
                        gv_Child.DataBind();
                    }
                }
                else
                {
                    userflag.Text = "USER";

                    DataTable dtpreference = dtPS.Tables[1];

                    if (dtpreference != null && dtpreference.Rows.Count > 0)
                    {
                        ViewState["EmployeeName"] = dtpreference;

                        gv_Child.DataSource = dtpreference;
                        gv_Child.DataBind();
                    }
                    else
                    {
                        gv_Child.DataSource = null;
                        gv_Child.DataBind();
                    }
                }

                for (int i = 0; i < gv_Child.Rows.Count; i++)
                {
                    string startairport = ((Label)gv_Child.Rows[i].FindControl("lblstartcity2")).Text.Trim();
                    string endairport = ((Label)gv_Child.Rows[i].FindControl("lblendcity2")).Text.Trim();
                    string date = ((Label)gv_Child.Rows[i].FindControl("lblstartdate")).Text.Trim();
                    BindEmptyLegs(1, startairport, endairport, Convert.ToDateTime(date).ToString("yyyy-MM-dd"));
                }
            }

            if (ddlUserType.SelectedValue == "Broker")
            {
                trBrokerAdress.Visible = true;
                lblPre.Text = "you can add 10 more preferences";

                if (gv_Child.Rows.Count >= 10)
                {
                    btnAddPref.Visible = false;
                    lblPre.Text = "you can add 10 preferences only";
                }
                else
                {
                    int c = 10 - gv_Child.Rows.Count;
                    if (c.ToString().Contains("-"))
                    {
                        btnAddPref.Visible = false;
                        lblPre.Text = "you can add 10 preferences only";
                    }
                    else
                    {
                        lblPre.Text = "you can add " + c.ToString() + " more preferences";
                        btnAddPref.Visible = true;
                    }
                }
            }
            else
            {
                trBrokerAdress.Visible = false;
                lblPre.Text = "you can add 3 more preferences";

                if (gv_Child.Rows.Count >= 3)
                {
                    btnAddPref.Visible = false;
                    lblPre.Text = "you can add 3 preferences only";
                }
                else
                {
                    int c = 3 - gv_Child.Rows.Count;

                    if (c.ToString().Contains("-"))
                    {
                        btnAddPref.Visible = false;
                        lblPre.Text = "you can add 3 preferences only";
                    }
                    else
                    {
                        lblPre.Text = "you can add " + c.ToString() + " more preferences";
                        btnAddPref.Visible = true;
                    }

                }
            }


            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            trPAsword.Visible = false;
            btnReset.Visible = true;
            dvFormHeader.Visible = true;
            dvGridHeader.Visible = false;
            ViewState["DisplayIndex"] = null;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }


    /// <summary>
    /// 1. Based on the Filtered Textbox bind the customer List
    /// </summary>
    public void txtusername_changed(object sender, EventArgs e)
    {
        try
        {
            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    /// <summary>
    /// 1. Display the password Reset popup
    /// </summary>
    protected void btnResetClick(object sender, EventArgs e)
    {
        try
        {
            mpetime.Show();
            txtMPass.Value = string.Empty;
            txtMConfirmPass.Value = string.Empty;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    /// <summary>
    /// 1. Validate the Password criteria
    /// 2. Update the customer popup
    /// 3. Send the Updated password to the customer
    /// </summary>
    protected void btnUpdatePasswordClick(object sender, EventArgs e)
    {
        try
        {
            #region Password Criteria

            bool blValidated = genclass.ValidatePassword(txtMPass.Value.Trim());
            if (blValidated == false)
            {
                lblAlertNew.Text += "Password Criteria not matched. Password must contain <br /><br /> ";
                lblAlertNew.Text += "<span style='text-align: left; float: left; line-height: 30px;'> 1. At least one lower case letter <br /> ";
                lblAlertNew.Text += "2. At least one upper case letter<br />  ";
                lblAlertNew.Text += "3. Not include special character <br /> ";
                lblAlertNew.Text += "4. At least one number <br /> ";
                lblAlertNew.Text += "5. At least 8 characters length </span> ";

                mpeAlertNew.Show();

                return;
            }
            else if (blValidated == true)
            {
                blValidated = genclass.ValidatePassword(txtMConfirmPass.Value.Trim());
                if (blValidated == false)
                {
                    lblAlertNew.Text += "Password Criteria not matched. Password must contain <br /><br /> ";
                    lblAlertNew.Text += "<span style='text-align: left; float: left; line-height: 30px;'> 1. At least one lower case letter <br /> ";
                    lblAlertNew.Text += "2. At least one upper case letter<br />  ";
                    lblAlertNew.Text += "3. Not include special character <br /> ";
                    lblAlertNew.Text += "4. At least one number <br /> ";
                    lblAlertNew.Text += "5. At least 8 characters length </span> ";

                    mpeAlertNew.Show();

                    return;
                }
            }

            #endregion

            if (txtMPass.Value.Trim() != txtMConfirmPass.Value.Trim())
            {
                lblalert.Text = "Password and Confirm Password doest not match";
                mpetime.Show();
                mpealert.Show();
                return;
            }
            else
            {
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set PAssword='" + (genclass.Encrypt(txtMPass.Value.Trim())) + "' where rowid='" + hdfPSRID.Value.Trim() + "' ");

                lblalert.Text = "Password has been updated successfully";
                mpealert.Show();

                StringBuilder sbEmailBody = new StringBuilder();

                string subject = "Welcome to Jet Edge !";

                string strURL = "https://www.flyjetedge.net/FLYT/LandingPage.aspx";

                sbEmailBody.Append("Dear " + txtFname.Text + " " + txtLastName.Text + ", <img height=50 width=150 style='float:right;padding-right:5%' src= https://www.flyjetedge.net/JETEDGE/Images/Jet_Edge_logo.jpg /><br><br>");
                sbEmailBody.Append("Jet Edge has been updated password for your account in Jet Edge FLYT.<br><br>");
                sbEmailBody.Append("Your account details are as follows:<br><br>");


                sbEmailBody.Append("Login ID : " + txtEmail.Text.Trim() + "<br><br>");
                sbEmailBody.Append("Password : " + txtMPass.Value.Trim() + " <br><br>");


                sbEmailBody.Append("<br>");
                sbEmailBody.Append("To log into your Jet Edge FLYT account, please use the below URL and save it in browser favorites for quick access in the future. <br><br>");

                sbEmailBody.Append("URL : <a href='" + strURL + "'>" + strURL + " </a><br><br>");

                sbEmailBody.Append("If you have questions, please contact us at flyt@flyjetedge.com<br><br><br>");


                sbEmailBody.Append("Thank you.<br><br>Jet Edge FLYT<br>");
                sbEmailBody.Append("<h3><u>NOTE: Replies to this email will not be accepted</u></h3><br>");

                try
                {
                    string strRes = SendEmail_Office365(txtEmail.Text.Trim(), "", subject, sbEmailBody.ToString());
                    if (strRes.ToLower().Trim() != "yes")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Email sending failed.');", true);
                    }
                }
                catch (Exception ex)
                {
                    linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                    MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    /// <summary>
    /// 1. Display the preference popup  based on the selected prefernece details
    /// 2. Bind the Details and Changed the button Text
    /// </summary>
    protected void gv_Child_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            btnAddMore.Text = "Update";
            mpeUserPref.Show();
            ViewState["DisplayIndex"] = gv_Child.SelectedRow.RowIndex.ToString();

            string strStartAirport = ((Label)gv_Child.SelectedRow.FindControl("lblstartcity2")).Text;
            string strEndAirport = ((Label)gv_Child.SelectedRow.FindControl("lblendcity2")).Text;

            DataTable dtSPstart = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select city,StateName from JETEDGE_FUEL..st_airport Where icao='" + strStartAirport.Trim() + "'").Tables[0];


            txtScity1.Text = strStartAirport.Trim() + " , " + dtSPstart.Rows[0]["city"].ToString() + " - " + dtSPstart.Rows[0]["StateName"].ToString();

            DataTable dtSPend = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select city,StateName from JETEDGE_FUEL..st_airport Where icao='" + strEndAirport.Trim() + "'").Tables[0];

            txtEcity1.Text = strEndAirport.Trim() + " , " + dtSPend.Rows[0]["city"].ToString() + " - " + dtSPend.Rows[0]["StateName"].ToString();

            txtDate1.Text = ((Label)gv_Child.SelectedRow.FindControl("lblstartdate")).Text;

            if (txtDate1.Text.Length > 0)
            {
                txtDate1.Text = Convert.ToDateTime(txtDate1.Text.Trim()).ToString("MM-dd-yyyy");
            }

            txtToDate1.Text = ((Label)gv_Child.SelectedRow.FindControl("lblenddate")).Text;

            if (txtToDate1.Text.Length > 0)
            {
                txtToDate1.Text = Convert.ToDateTime(txtToDate1.Text.Trim()).ToString("MM-dd-yyyy");
            }

            if (((Label)gv_Child.SelectedRow.FindControl("lbltime1")).Text.Trim() == "Anytime")
            {
                rblflytime1.SelectedIndex = 0;
            }
            else
            {
                rblflytime1.SelectedIndex = 1;
            }

            rblflytime1_OnSelectedIndexChanged(sender, e);

            txtTime1.Text = ((Label)gv_Child.SelectedRow.FindControl("lbltime2")).Text;

            if (txtTime1.Text != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:NewSplit();", true);
            }

            if (((Label)gv_Child.SelectedRow.FindControl("lblemail")).Text == "Yes")
            {
                rblEmailFlag.SelectedIndex = 0;
            }
            else
            {
                rblEmailFlag.SelectedIndex = 1;
            }
            if (((Label)gv_Child.SelectedRow.FindControl("lblSMs")).Text == "Yes")
            {
                rblSMSNotifi.SelectedIndex = 1;
            }
            else
            {
                rblSMSNotifi.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    /// <summary>
    /// 1. Delete the selected Preference and Rebind the Preference Grid
    /// </summary>
    protected void gv_Child_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["EmployeeName"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["EmployeeName"] = dt;
            gv_Child.EditIndex = -1;
            gv_Child.DataSource = ViewState["EmployeeName"];
            gv_Child.DataBind();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    #region  User Defined Function

    /// <summary>
    /// 1. List all the Customers
    /// 2. based on the Broker Flag, displays the Broker Name
    /// </summary>
    public void List()
    {
        DataTable dt = objMember.UserList(txtusername.Text);
        if (dt.Rows.Count > 0)
        {
            gvpricing.DataSource = dt;
            gvpricing.DataBind();

            foreach (GridViewRow item in gvpricing.Rows)
            {
                Label lblphone = (Label)item.FindControl("lblphone");
                lblphone.Text = genclass.Format_Phone(lblphone.Text.Trim().Replace(".", ""));

                Label lblBrokerNamw = (Label)item.FindControl("lblBrokerNamw");
                Label lblUserType = (Label)item.FindControl("lblUserType");
                lblBrokerNamw.Visible = false;
                if (lblUserType.Text == "Broker")
                {
                    lblBrokerNamw.Visible = true;
                }
            }
        }
        else
        {
            gvpricing.DataSource = null;
            gvpricing.DataBind();
        }
    }

    /// <summary>
    /// 1. Clear the FOrm Fields
    /// </summary>
    public void Clear()
    {
        try
        {
            divLMBy.Visible = false;
            userflag.Text = "ADMINUSER";
            btnSave.Text = "Save";
            hdfPSRID.Value = "0";
            userflag.Text = string.Empty;
            ddlgender.ClearSelection();
            txtFname.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPhone.Value = string.Empty;
            txtAddress.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtState.Text = string.Empty;
            ddlCountry.ClearSelection();
            txt_Zip.Text = string.Empty;
            txtScity1.Text = string.Empty;
            txtEcity1.Text = string.Empty;
            txtDate1.Text = string.Empty;
            trBrokerAdress.Visible = false;
            txtBrokerName.Text = string.Empty;
            txtManger.Text = string.Empty;
            txtBrokerAdd1.Text = string.Empty;
            txtBLName.Text = string.Empty;
            txtBrokerCIty.Text = string.Empty;
            txtBrokerState.Text = string.Empty;
            txtBrokerCountry.Text = string.Empty;
            txtBrokerZipCode.Text = string.Empty;
            txtDays.Text = string.Empty;
            chkActive.Checked = true;
            txtAddress2.Text = string.Empty;
            ddlUserType.ClearSelection();
            ddlUserType.SelectedIndex = 0;
            ddlPhoneCode.ClearSelection();
            txtNew_Password.Text = string.Empty;
            txtConformPass.Text = string.Empty;
            rblMustMoveMail.SelectedValue = "Y";
            ViewState["EmployeeName"] = null;

            for (int j = 0; j < rblPayMethod.Items.Count; j++)
            {
                rblPayMethod.Items[j].Selected = true;
            }

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    /// <summary>
    /// 1. Get the List of airport for Preference
    /// </summary>
    public void GetAirport()
    {
        DataTable dt = genclass.GetZoomAirports("YES");

        if (dt.Rows.Count > 0)
        {
            Session["ICAOC"] = dt;
        }
        else
        {
            Session["ICAOC"] = null;
        }

    }

    public void BindEmptyLegs(int pageindex, string startairport, string endairport, string Date)
    {
        string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
        string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

        DataTable dtOneWayTrip = objMember.GetEmptyTrips(pageindex.ToString(), "25", strStartDate, strEndDate, "");
        DataView dv = dtOneWayTrip.DefaultView;
        DataTable sortedDT = dv.ToTable();
        //sortedDT.Columns.Add("leg");
        for (int i = 0; i < sortedDT.Rows.Count; i++)
        {
            string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
            String[] strlist = routes.Split(new[] { ',' });
            string startD = strlist[0];
            string EndD = strlist[1];
            DataTable dtleg = objMember.GetLeg(sortedDT.Rows[i]["Tailno"].ToString(), sortedDT.Rows[i]["tripno"].ToString(), Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd"), Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm"), Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm"), startD, EndD);

            if (dtleg.Rows.Count > 0)
            {
                sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
            }
        }

        sortedDT.Columns.Add("SNo", typeof(int));
        for (int count = 0; count < sortedDT.Rows.Count; count++)
        {
            sortedDT.Rows[count]["SNo"] = count + 1;
        }
        for (int i = 0; i < sortedDT.Rows.Count; i++)
        {
            sortedDT.Rows[i]["SNo"] = Convert.ToInt32(sortedDT.Rows[i]["SNo"].ToString()) - 1;
        }

        string finalroutes = startairport + "-->" + endairport;
        DataRow[] result1 = sortedDT.Select("Dateformat='" + Date + "' and routes='" + finalroutes + "'");
        DataTable dt1 = new DataTable();
        DataTable dtfinal = new DataTable();
        if (result1.Length > 0)
        {
            dt1 = result1.CopyToDataTable();
        }
        if (gvSimpleSearch.Rows.Count == 0)
        {
            dtfinal = dt1;
            ViewState["Tripgrid"] = dt1;
        }
        else
        {
            dt1.Merge((DataTable)ViewState["Tripgrid"]);
            dtfinal = dt1;
            ViewState["Tripgrid"] = dt1;
        }
        if (dtfinal.Columns.Contains("IsSelected"))
        {

        }
        else
        {
            dtfinal.Columns.Add("IsSelected");
        }

        for (int i = 0; i < dtfinal.Rows.Count; i++)
        {
            dtfinal.Rows[i]["IsSelected"] = false;
        }
        gvSimpleSearch.DataSource = dtfinal;
        gvSimpleSearch.DataBind();

        if (dtfinal.Rows.Count > 0)
        {
            tripblock.Visible = true;
        }
        else
        {
            tripblock.Visible = false;
        }
    }

    /// <summary>
    /// 1. Send welcome mail, when a customer is created
    /// </summary>
    public string SendEmail(string strEmail, string strPassword, string strName, string userid, DataTable dttrip)
    {

        StringBuilder sbEmailBody = new StringBuilder();

        string subject = "Welcome to Jet Edge Flyt !";
        string strURL = SecretsBLL.MailLoginURL.ToString().Trim();

        sbEmailBody.Append("Dear " + strName + ", <img height=50 width=150 style='float:right;padding-right:5%' src= https://www.flyjetedge.net/JETEDGE/Images/Jet_Edge_logo.jpg /><br><br>");
        sbEmailBody.Append("Jet Edge has created an user account for you in Jet Edge Zoom.<br><br>");
        sbEmailBody.Append("Your account details are as follows:<br><br>");

        sbEmailBody.Append("Login ID : " + strEmail + "<br><br>");
        sbEmailBody.Append("Password : " + strPassword + " <br><br>");

        sbEmailBody.Append("Based on your request please find the trip details below. Please click on BOOK to book the trip. <br><br>");
        sbEmailBody.Append("<div style='width:100%;padding-left:10%'>");
        sbEmailBody.Append("<table border='3px' cellpadding='5' cellspacing='0' bgcolor='White' style='font-family:Verdana; font-size:13px;width:70%;'>");

        sbEmailBody.Append("<tr style='height:50px;background-color:black;color:white'>");

        sbEmailBody.Append("<td  style='width:15%;text-align:center;Padding-top:10px;padding-bottom:5px'>");
        sbEmailBody.Append("<b>Date</b>");
        sbEmailBody.Append("</td>");
        sbEmailBody.Append("<td  style='width:30%;text-align:center;'>");
        sbEmailBody.Append("<b>Departure    -     Arrival</b>");
        sbEmailBody.Append("</td>");

        sbEmailBody.Append("<td  style='text-align:center;width:15%'>");
        sbEmailBody.Append("<b>Price</b>");
        sbEmailBody.Append("</td>");
        sbEmailBody.Append("<td  style='text-align:center;width:15%'>");
        sbEmailBody.Append("<b>Book</b>");
        sbEmailBody.Append("</td>");

        sbEmailBody.Append("</tr>");

        for (int i = 0; i < dttrip.Rows.Count; i++)
        {
            if (i % 2 == 0)
            {
                sbEmailBody.Append("<tr  style='height:50px;background-color:black;color:white'>");

                sbEmailBody.Append("<td style='text-align:center;width:15%;Padding-top:10px;padding-bottom:5px'>");
                sbEmailBody.Append(string.Format("{0:MMMM dd, yyyy}", Convert.ToDateTime(dttrip.Rows[i]["date"].ToString())));
                sbEmailBody.Append("</td >");
                sbEmailBody.Append("<td style='text-align:center;width:30%'>");
                sbEmailBody.Append(dttrip.Rows[i]["startairport"].ToString() + "   -    " + dttrip.Rows[i]["endairport"].ToString());
                sbEmailBody.Append("</td >");

                sbEmailBody.Append("<td style='text-align:center;width:15%'>");
                sbEmailBody.Append(dttrip.Rows[i]["price"].ToString());
                sbEmailBody.Append("</td >");
                sbEmailBody.Append("<td nowrap='nowrap' style='text-align:center;width:15%' >");
                sbEmailBody.Append("<a style='background-color:#b72025;padding-right:3%;padding-left:3%;text-decoration:none;color:white' href=" + " https://www.flyjetedge.net/Zoom/simplesearchbooking.aspx?Flag=L&userid=" + userid + "&userflag=ADMINUSER&Index=" + dttrip.Rows[i]["slno"] + "&startairport=" + dttrip.Rows[i]["startairport"].ToString() + "&endairport=" + dttrip.Rows[i]["endairport"].ToString() + "&flydate=" + Convert.ToDateTime(dttrip.Rows[i]["date"].ToString()).ToString("yyyy-MM-dd") + "&Tailno=" + dttrip.Rows[i]["tailno"].ToString() + "" + ">Book</a>");
                sbEmailBody.Append("</td >");

                sbEmailBody.Append("</tr >");
            }
            else
            {
                sbEmailBody.Append("<tr  style='height:50px;background-color:grey;color:white'>");

                sbEmailBody.Append("<td style='text-align:center;width:15%;Padding-top:10px;padding-bottom:5px'>");
                sbEmailBody.Append(string.Format("{0:MMMM dd, yyyy}", Convert.ToDateTime(dttrip.Rows[i]["date"].ToString())));
                sbEmailBody.Append("</td >");
                sbEmailBody.Append("<td style='text-align:center;width:15%'>");
                sbEmailBody.Append(dttrip.Rows[i]["startairport"].ToString() + "   -    " + dttrip.Rows[i]["endairport"].ToString());
                sbEmailBody.Append("</td >");

                sbEmailBody.Append("<td style='text-align:center;width:15%'>");
                sbEmailBody.Append(dttrip.Rows[i]["price"].ToString());
                sbEmailBody.Append("</td >");
                sbEmailBody.Append("<td nowrap='nowrap' style='text-align:center;width:15%' >");
                sbEmailBody.Append("<a style='background-color:#b72025;padding-right:3%;padding-left:3%;text-decoration:none;color:white' href=" + " https://www.flyjetedge.net/Zoom/simplesearchbooking.aspx?Flag=L&userid=" + userid + "&userflag=ADMINUSER&Index=" + dttrip.Rows[i]["slno"] + "&startairport=" + dttrip.Rows[i]["startairport"].ToString() + "&endairport=" + dttrip.Rows[i]["endairport"].ToString() + "&flydate=" + Convert.ToDateTime(dttrip.Rows[i]["date"].ToString()).ToString("yyyy-MM-dd") + "&Tailno=" + dttrip.Rows[i]["tailno"].ToString() + "" + ">Book</a>");
                sbEmailBody.Append("</td >");


                sbEmailBody.Append("</tr >");
            }

        }

        sbEmailBody.Append("</table>");
        sbEmailBody.Append("</div>");

        sbEmailBody.Append("<br>");
        sbEmailBody.Append("<br>");

        sbEmailBody.Append("To log into your Zoom account, please use the below URL and save it in browser favorites for quick access in the future. <br><br>");

        sbEmailBody.Append("URL : <a href='" + strURL + "'>" + strURL + " </a><br><br>");

        sbEmailBody.Append("If you have questions, please contact us at zoom@flyjetedge.com<br><br><br>");

        sbEmailBody.Append("Thank you.<br><br>Jet Edge Zoom Team<br>");
        sbEmailBody.Append("<h3><u>NOTE: Replies to this email will not be accepted</u></h3><br>");

        string strRes = genclass.SendEmail_Office365(strEmail, "", subject, sbEmailBody.ToString());
        return strRes;
    }

    /// <summary>
    /// 1. Builds the Mail Body for customer
    /// </summary>
    private string Mail_Body(string OTP, string username, string userEmail)
    {
        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + userEmail + "'").Tables[0];
        string id = string.Empty;
        if (dt.Rows.Count > 0)
        {
            id = dt.Rows[0]["Rowid"].ToString();
        }

        string strURL = SecretsBLL.MailLoginURL.ToString().Trim() + "?verified=y&id=" + id + "";
        //                    <img src='https://www.flyjetedge.net/ZoomCust/assets/img/Jet_Edge_logo.jpg'  id='img1' alt='' style='float:right' width='55px' height='55px'/>

        string str = @" <div>
        <center>

            <div style='border: 1px solid #a3a29e; width:550px'>
                <div style='text-align: right;padding-right:8%'>

 
                </div>

                <div style='text-align: right;'>
<table> <tr><td style='vertical-align:middle; width:70%;padding-right:8%'> WELCOME TO JET EDGE FLYT  </td> <td><img  id='img1' alt='' height=50 width=150 style='float:right;padding-right:1%' src= https://www.flyjetedge.net/JETEDGE/Images/Jet_Edge_logo.jpg /></td></tr></table>
                  
                </div>

                <div style='border-top:1px solid #a3a29e;padding-top:10px;padding-bottom:10px;text-align:left;padding-left:5%'>
                  Thank you for creating a Jet Edge Flyt account.  Your token is below.  Please click on it within the next 30 minutes to verify your registration.  If you did not create an account, please disregard this e-mail.
                </div>

                <div style='padding-top:20px;padding-bottom:20px'>
                   <a style='background-color:#b72025;padding-right:30%;padding-left:30%;padding-top:10px;text-decoration:none;font-size:25px;padding-bottom:10px;width:100%;color:white' >" + OTP + @"</a>
                </div>

                 <div style='padding-top:6px;font-size:11px;padding-bottom:6px;display:none'>
                   Your token will expire in 30 minutes
                </div>

                 <div style='padding-top:10px;border-top:1px solid #a3a29e;text-align:left;padding-left:5%'>
                    If you have any difficulty verifying your account, please contact us at flyt@flyjetedge.com.  Replies to this e-mail will not be accepted.  
                 </div>
         
                 <div style='padding-top:20px;padding-bottom:10px;text-align:left;padding-left:5%'>
                    We hope you enjoy FLYT!
                    <br/>
                    <br/>
                    Jet Edge Flyt Team
                </div>
            </div>

        </center>
        </div>";

        return str;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText, int count)
    {
        return AutoFillProducts_V1(prefixText);
    }

    private static List<string> AutoFillProducts_V1(string prefixText)
    {
        DataTable dtNew = new DataTable();
        DataTable dt = new DataTable();
        DataRow[] drNew = null;
        if (HttpContext.Current.Session["ICAOC"] == null)
        {
            dt = genclass.GetZoomAirports("YES");

            HttpContext.Current.Session["ICAOC"] = dt;
        }

        dt = (DataTable)HttpContext.Current.Session["ICAOC"];

        if (dt.Rows.Count > 0)
        {
            if (prefixText.Length < 5)
            {
                drNew = dt.Select("ICAO like '%' + '" + prefixText + "' + '%'");
            }
            else
            {
                drNew = dt.Select("StateName like  '" + prefixText + "' + '%' OR City like '" + prefixText + "' + '%'");
            }

            if (drNew.Length > 0)
            {
                dtNew = drNew.CopyToDataTable();
            }
        }
        List<string> sEmail = new List<string>();
        for (int i = 0; i < drNew.Length; i++)
        {
            sEmail.Add(drNew[i]["ICAOName"].ToString());
        }

        //foreach (DataRow row in dtNew.Rows)
        //{
        //    sEmail.Add(row[0].ToString());
        //}
        return sEmail;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> CompleteStartCity(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;

            using (SqlCommand com = new SqlCommand())
            {
                com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "icao like  @Search + '%' AND Access=1";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> CompleteEndCity(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;

            using (SqlCommand com = new SqlCommand())
            {
                com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "icao like  @Search + '%' AND Access=1";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    #endregion

    public string SendEmail_logincreate(string email, string username, string OTP)
    {
        string subject = "WELCOME TO JET EDGE FLYT";
        string strMailBody = Mail_Body(OTP, username, email);

        string strRes = SendEmail_Office365(email, "", subject, strMailBody);
        return strRes;
    }

    public static string SendEmail_Office365(string strEmail, string strCC, string subject, string EmailBody)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);

        ExchangeService myservice = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
        myservice.Credentials = new WebCredentials(SecretsBLL.SenderEmailid, SecretsBLL.Password);

        string serviceUrl = SecretsBLL.Office365WebserivceURL;
        myservice.Url = new Uri(serviceUrl);
        EmailMessage emailMessage = new EmailMessage(myservice);

        emailMessage.Subject = subject;
        emailMessage.Body = new MessageBody(EmailBody);
        emailMessage.ToRecipients.Add(strEmail);
        if (strCC.Length > 0)
        {
            emailMessage.CcRecipients.Add(strCC);
        }

        emailMessage.BccRecipients.Add("premkumar@sankarasoftware.com");
        emailMessage.BccRecipients.Add("venkatesh.kuppuraj@sankarasoftware.com");

        emailMessage.Send();

        return "Yes";
    }
}