﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Newtonsoft.Json;
using DocuSign.eSign.Client;
using DocuSign.eSign.Api;
using DocuSign.eSign.Model;

public partial class Views_BookedReport : System.Web.UI.Page
{

    #region Global Declaration

    public int linenum = 0;
    public string MethodName = string.Empty;

    protected BusinessLayer.BookedReportBLL objMember = new BusinessLayer.BookedReportBLL();
    protected BusinessLayer.ExceptionReportBLL objMemberRep = new BusinessLayer.ExceptionReportBLL();
    //Zendesk_Ticket objZendesk_Ticket = new Zendesk_Ticket();

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Reports", "Booking Report");
                }


                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Reports";
                lblSubHeader.InnerText = "Booked Report";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                txtfromdate.Text = DateTime.Now.AddDays(-7).ToString("MM-dd-yyyy");
                txttodate.Text = DateTime.Now.ToString("MM-dd-yyyy");
                BindEmptyLegs();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    public void BindEmptyLegs()
    {
        lblNoTrips.Visible = false;

        DataTable dtOneWayTrip = objMember.Select(Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd"), Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd"), txtTailFilter.Text, txttripno.Text.Trim());
        if (dtOneWayTrip.Rows.Count > 0)
        {
            dlCurrent.DataSource = dtOneWayTrip;
            dlCurrent.DataBind();

            foreach (DataListItem item in dlCurrent.Items)
            {
                Label lblRowID = (Label)item.FindControl("lblRowID");

                ImageButton imgSignedFile = (ImageButton)item.FindControl("imgSignedFile");
                Label lbleSignStatus = (Label)item.FindControl("lbleSignStatus");
                Label lblSignedBy = (Label)item.FindControl("lblSignedBy");
                Label lblSignedOn = (Label)item.FindControl("lblSignedOn");
                HtmlGenericControl dvSignedBy = (HtmlGenericControl)item.FindControl("dvSignedBy");

                Label lblFromTime = (Label)item.FindControl("lblFromTime");
                if (lblFromTime.Text.Length > 0)
                {
                    lblFromTime.Text = genclass.Long_Date_Time(Convert.ToDateTime(lblFromTime.Text));
                }

                Label lblToTime = (Label)item.FindControl("lblToTime");
                if (lblToTime.Text.Length > 0)
                {
                    lblToTime.Text = genclass.Long_Date_Time(Convert.ToDateTime(lblToTime.Text));
                }

                ImageButton imbInvDownload = (ImageButton)item.FindControl("imbInvDownload");
                Button btnSendMail = (Button)item.FindControl("btnSendMail");
                LinkButton btnCheckStatus = (LinkButton)item.FindControl("btnCheckStatus");

                imgSignedFile.Visible = false;
                btnCheckStatus.Visible = true;

                Label lblExpiryTime = (Label)item.FindControl("lblExpiryTime");
                lblExpiryTime.Text = Convert.ToDateTime(dtOneWayTrip.Rows[0]["TransEndTime"].ToString()).ToString("MMMM dd, yyyy HH:mm tt");

                if (Convert.ToDateTime(dtOneWayTrip.Rows[0]["TransEndTime"].ToString()) < DateTime.Now)
                {
                    lbleSignStatus.Text = "Expired";
                    lbleSignStatus.ForeColor = Color.Red;
                    btnCheckStatus.Visible = false;
                    btnSendMail.Visible = false;
                }

                DataTable dtDocuSign = genclass.GetDataTable("Select * FROM BookedDetails_DocuSign where BookingId = '" + lblRowID.Text + "'");
                if (dtDocuSign.Rows.Count > 0)
                {
                    if (dtDocuSign.Rows[0]["SignedBy"].ToString().Length > 0)
                    {
                        lbleSignStatus.Text = "Completed";
                        lbleSignStatus.ForeColor = Color.Green;

                        lblSignedBy.Text = dtDocuSign.Rows[0]["SignedBy"].ToString();
                        lblSignedOn.Text = Convert.ToDateTime(dtDocuSign.Rows[0]["SignedOn"].ToString()).ToString("MMM dd, yyyy HH:mm tt").ToUpper();

                        dvSignedBy.Visible = true;
                        imgSignedFile.Visible = true;

                        btnCheckStatus.Visible = false;
                        btnSendMail.Visible = false;
                        imbInvDownload.Visible = false;
                    }
                }
            }
        }
        else
        {
            lblNoTrips.Visible = true;
        }
    }

    protected void ddlstatus_onselectedChanged(object sender, EventArgs e)
    {
        try
        {
            BindEmptyLegs();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            dvGrid.Visible = true;
            //dvheader.Visible = true;
            //dvheader1.Visible = true;
            //gvTailDetails.Columns[9].Visible = false;
            //gvTailDetails.Columns[10].Visible = true;
            //gvTailDetails.Columns[11].Visible = false;
            //gvTailDetails.Columns[12].Visible = true;
            Response.ClearContent();
            Response.AppendHeader("content-disposition", "attachment;filename=BookedReports" + Convert.ToDateTime(txtfromdate.Text).ToString("MM-dd-yyyy") + "_" + Convert.ToDateTime(txttodate.Text).ToString("MM-dd-yyyy") + ".xls");
            Response.Charset = "";
            Response.ContentType = ("application/vnd.xls;charset=iso-8859-1");
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dvGrid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    { }
    public void txtfromdate_changed(object sender, EventArgs e)
    {
        try
        {
            BindEmptyLegs();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public void txtto_changed(object sender, EventArgs e)
    {
        try
        {
            BindEmptyLegs();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public void textBox1_TextChanged(object sender, EventArgs e)
    {
        try
        {
            BindEmptyLegs();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public void txttripno_TextChanged(object sender, EventArgs e)
    {
        try
        {
            BindEmptyLegs();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void gvTailDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            BindEmptyLegs();
            //gvTailDetails.PageIndex = e.NewPageIndex;
            //gvTailDetails.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }

    }
    protected void gvpricing_price_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label Label2 = e.Row.FindControl("Label2") as Label;
                if (Label2.Text.Length > 0)
                {
                    if (Convert.ToDateTime(Label2.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        Label2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        Label2.Font.Bold = true;
                    }
                    else
                    {
                        Label2.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void gvTailDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //mpetrans.Hide();
            //LinkButton Invoicenumber = (LinkButton)gvTailDetails.SelectedRow.FindControl("lnkbtn");
            //string str = @"D:\jetedgefiles\EMPTYLEG\" + Invoicenumber.Text + ".pdf";
            //funFileDownload(str);
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void lblInvoicenumber_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;

            DataTable dtPS = objMemberRep.Booking(btn.Text);
            if (dtPS.Rows.Count > 0)
            {
                lblbookedname.Text = dtPS.Rows[0]["CFirstname"].ToString() + " " + dtPS.Rows[0]["CLastname"].ToString();
                lblbookeddate.Text = dtPS.Rows[0]["Transdate"].ToString();
                lbltransaction.Text = dtPS.Rows[0]["Paymentstatus"].ToString();
                lbltransid.Text = dtPS.Rows[0]["transid"].ToString();
                lblamount.Text = Convert.ToDecimal(dtPS.Rows[0]["totalamount"].ToString()).ToString("c2");
                lblmblno.Text = dtPS.Rows[0]["CPhone"].ToString();

                lblInvoiceNumber.Text = dtPS.Rows[0]["Invoicenumber"].ToString();
                lblTransRefNumber.Text = dtPS.Rows[0]["TransactionReference"].ToString();
            }
            else
            {
                lblbookedname.Text = string.Empty;
                lblbookeddate.Text = string.Empty;
                lbltransaction.Text = string.Empty;
                lbltransid.Text = string.Empty;
                lblamount.Text = string.Empty;
                lblmblno.Text = string.Empty;

                lblInvoiceNumber.Text = string.Empty;
                lblTransRefNumber.Text = string.Empty;
            }

            mpetrans.Show();

        }
        catch (Exception ex)
        {
            ;
        }
    }
    void funFileDownload(string Location)
    {
        try
        {
            FileInfo file = new FileInfo(Location);
            if (file.Exists)
            {
                Response.Buffer = false;
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(Location));
                Response.TransmitFile(Location);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void dlCurrent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LinkButton Invoicenumber = (LinkButton)dlCurrent.SelectedItem.FindControl("lblInvoicenumber");
            string str = @"D:\jetedgefiles\EMPTYLEG\02Invoice\" + Invoicenumber.Text.Trim() + ".pdf";

            funFileDownload(str);
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void btnCheckStatus_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;
            DataListItem item = (DataListItem)btn.NamingContainer;

            Label lblRowID = (Label)item.FindControl("lblRowID");
            LinkButton lblInvoiceNumber = (LinkButton)item.FindControl("lblInvoicenumber");

            Label lbleSignStatus = (Label)item.FindControl("lbleSignStatus");
            Label lblSignedBy = (Label)item.FindControl("lblSignedBy");
            Label lblSignedOn = (Label)item.FindControl("lblSignedOn");

            HtmlGenericControl dvSignedBy = (HtmlGenericControl)item.FindControl("dvSignedBy");

            ImageButton imgSignedFile = (ImageButton)item.FindControl("imgSignedFile");
            LinkButton btnCheckStatus = (LinkButton)item.FindControl("btnCheckStatus");

            ImageButton imbInvDownload = (ImageButton)item.FindControl("imbInvDownload");
            Button btnSendMail = (Button)item.FindControl("btnSendMail");


            imgSignedFile.Visible = false;
            btnCheckStatus.Visible = true;
            dvSignedBy.Visible = false;

            DataTable dtDocuSign = genclass.GetDataTable("Select * FROM BookedDetails_DocuSign where BookingId = '" + lblRowID.Text + "'");
            if (dtDocuSign.Rows.Count > 0)
            {
                string EnvelopId = dtDocuSign.Rows[0]["EnvelopeId"].ToString();
                string DocuId = dtDocuSign.Rows[0]["DocumentNo"].ToString();
                string FilePath = SecretsBLL.DocuSignUrl.ToString().Trim();

                var config = new Configuration(new ApiClient(SecretsBLL.basePath));
                config.AddDefaultHeader("Authorization", "Bearer " + genclass.Authorization_Code_Grant());
                EnvelopesApi envelopesApi = new EnvelopesApi(config);

                Envelope EvRsults = envelopesApi.GetEnvelope(SecretsBLL.accountId, EnvelopId);

                Recipients EvRecep = envelopesApi.ListRecipients(SecretsBLL.accountId, EnvelopId);
                List<Signer> signer = EvRecep.Signers;
                if (EvRsults.Status.ToLower() == "completed")
                {
                    imgSignedFile.Visible = true;
                    dvSignedBy.Visible = true;

                    imbInvDownload.Visible = false;
                    btnCheckStatus.Visible = false;
                    btnSendMail.Visible = false;

                    if (signer.Count > 0)
                    {
                        lblEnvelopBy.Text = signer[0].Name;
                        lblEnvelopOn.Text = Convert.ToDateTime(signer[0].SignedDateTime).ToString("MMM dd, yyyy HH:mm tt").ToUpper();

                        lbleSignStatus.Text = "Completed";
                        lblSignedBy.Text = signer[0].Name;
                        lblSignedOn.Text = Convert.ToDateTime(signer[0].SignedDateTime).ToString("MMM dd, yyyy HH:mm tt").ToUpper();

                        lbleSignStatus.ForeColor = Color.Green;
                    }

                    DateTime dtDocSignedOn = Convert.ToDateTime(EvRsults.StatusChangedDateTime);

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update JE_ZOOM..BookedDetails_DocuSign set SignedBy = '" + lblEnvelopBy.Text + "', SignedOn = '" + lblEnvelopOn.Text + "' where BookingId = '" + lblRowID.Text + "'");

                    EnvelopeDocumentsNew envelopeDocuments = DoWork(genclass.Authorization_Code_Grant(), SecretsBLL.basePath, SecretsBLL.accountId, EnvelopId);
                    List<EnvelopeDocItemNew> documents = envelopeDocuments.Documents;

                    //EnvelopeFormData EFDresults = envelopesApi.GetFormData(accountId, EnvelopId);
                    //dynamic dn = JsonConvert.SerializeObject(EFDresults, Formatting.Indented);

                    DoWork_GetSignedFile(genclass.Authorization_Code_Grant(), SecretsBLL.basePath, SecretsBLL.accountId,
                                         EnvelopId, documents, DocuId, FilePath, lblEnvelopBy.Text, lblEnvelopOn.Text, lblInvoiceNumber.Text);
                }
                else
                {
                    mpealert.Show();
                    lblalert.Text = "eSignature for the Trip is not Signed.";
                }
            }
            else
            {
                mpealert.Show();
                lblalert.Text = "eSignature for the Trip is not Signed.";
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void btnSendMail_Click(object sender, EventArgs e)
    {
        try
        {
            Button btn = (Button)sender;
            DataListItem item = (DataListItem)btn.NamingContainer;

            Label lblRowID = (Label)item.FindControl("lblRowID");

            ResendEnvelope(lblRowID.Text);
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    public void ResendEnvelope(string hdnBookingId)
    {
        try
        {
            string Qry = "Select *,CAST( FromTime as datetime ) FromTime1 , CAST( ToTime as datetime ) ToTime1 ";
            Qry += "FROM BookedDetails ";
            Qry += "INNER JOIN BookedDetails_DocuSign on BookedDetails_DocuSign.BookingId = BookedDetails.RowID ";
            Qry += "where BookedDetails.RowID = '" + hdnBookingId + "' ";

            DataTable dtBookedDetails = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, Qry).Tables[0];
            if (dtBookedDetails.Rows.Count > 0)
            {
                string CFirstName = dtBookedDetails.Rows[0]["CFirstName"].ToString();
                string CLastName = dtBookedDetails.Rows[0]["CLastName"].ToString();
                string strEmail = dtBookedDetails.Rows[0]["CEmail"].ToString();

                string strEnvelopeId = dtBookedDetails.Rows[0]["EnvelopeId"].ToString();
                string recipientId = dtBookedDetails.Rows[0]["recipientId"].ToString();

                Signer signer = new Signer();
                signer.Email = strEmail;
                signer.Name = CFirstName + " " + CLastName;

                // instantiation of recipients as per https://stackoverflow.com/questions/21565765/resend-docusign-emails
                Recipients recipients = new Recipients
                {
                    Signers = new List<Signer>()
            {
                    new Signer
                    {
                        RecipientId = recipientId,
                        Email = signer.Email,
                        Name = signer.Name,
                    },
                }
                };

                var config = new DocuSign.eSign.Client.Configuration(new ApiClient(SecretsBLL.basePath));
                config.AddDefaultHeader("Authorization", "Bearer " + genclass.Authorization_Code_Grant());
                EnvelopesApi envelopesApi = new EnvelopesApi(config);
                EnvelopesApi.UpdateRecipientsOptions options = new EnvelopesApi.UpdateRecipientsOptions();
                options.resendEnvelope = "true";

                RecipientsUpdateSummary recipientsUpdateSummary =
                    envelopesApi.UpdateRecipients(
                        SecretsBLL.accountId,
                        strEnvelopeId,
                        recipients,
                        new EnvelopesApi.UpdateRecipientsOptions { resendEnvelope = "true" });

                //RecipientsUpdateSummary summary = api.UpdateRecipients(objZendesk_Ticket.accountId, EnvelopeId, recipients, options);

                lblalert.Text = "Email has been resent to [ " + strEmail + " ].";
                mpealert.Show();

                var responses = recipientsUpdateSummary.RecipientUpdateResults.ToList<RecipientUpdateResponse>();
                var errors = responses.Select(rs => rs.ErrorDetails).ToList();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            //LogWriteFail(ex.Message.ToString() + " " + linenum.ToString(), "Errorbill");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void imgSignedFile_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton btn = (ImageButton)sender;
            DataListItem item = (DataListItem)btn.NamingContainer;

            Label lblRowID = (Label)item.FindControl("lblRowID");
            LinkButton lblInvoiceNumber = (LinkButton)item.FindControl("lblInvoicenumber");

            string FilePath = SecretsBLL.DocuSignUrl.ToString().Trim() + lblInvoiceNumber.Text + @"\";
            string FileName = FilePath + lblInvoiceNumber.Text + "_Signed.pdf";

            if (Directory.Exists(FilePath))
            {
                if (File.Exists(FileName))
                {
                    funFileDownload(FileName);
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #region Dicu Sign

    private EnvelopeDocumentsNew DoWork(string accessToken, string basePath, string accountId, string envelopeId)
    {
        var config = new Configuration(new ApiClient(basePath));
        config.AddDefaultHeader("Authorization", "Bearer " + accessToken);

        EnvelopesApi envelopesApi = new EnvelopesApi(config);
        EnvelopeDocumentsResult results = envelopesApi.ListDocuments(accountId, envelopeId);

        List<EnvelopeDocItemNew> envelopeDocItems = new List<EnvelopeDocItemNew>
            {
                new EnvelopeDocItemNew { Name = "Combined", Type = "content", DocumentId = "combined" },
                new EnvelopeDocItemNew { Name = "Zip archive", Type = "zip", DocumentId = "archive" }
            };

        foreach (EnvelopeDocument doc in results.EnvelopeDocuments)
        {
            envelopeDocItems.Add(new EnvelopeDocItemNew
            {
                DocumentId = doc.DocumentId,
                Name = doc.DocumentId == "certificate" ? "Certificate of completion" : doc.Name,
                Type = doc.Type
            });
        }

        EnvelopeDocumentsNew envelopeDocuments = new EnvelopeDocumentsNew
        {
            EnvelopeId = envelopeId,
            Documents = envelopeDocItems
        };

        return envelopeDocuments;
    }
    private void DoWork_GetSignedFile(string accessToken, string basePath, string accountId, string envelopeId,
                                     List<EnvelopeDocItemNew> documents, string docSelect, string FilePath,
                                     string SignedBy, string SignedOn, string lblInvoiceNumber)
    {
        var config = new Configuration(new ApiClient(basePath));
        config.AddDefaultHeader("Authorization", "Bearer " + accessToken);
        EnvelopesApi envelopesApi = new EnvelopesApi(config);

        // Step 1. EnvelopeDocuments::get.
        // Exceptions will be caught by the calling function
        System.IO.Stream results = envelopesApi.GetDocument(accountId,
                        envelopeId, docSelect);

        // Step 2. Look up the document from the list of documents 
        EnvelopeDocItemNew docItem = documents.FirstOrDefault(d => docSelect.Equals(d.DocumentId));

        string docName = lblInvoiceNumber;

        // Process results. Determine the file name and mimetype
        bool hasPDFsuffix = docName.ToUpper().EndsWith(".PDF");
        bool pdfFile = hasPDFsuffix;
        // Add .pdf if it's a content or summary doc and doesn't already end in .pdf
        string docType = docItem.Type;
        if (("content".Equals(docType) || "summary".Equals(docType)) && !hasPDFsuffix)
        {
            docName += ".pdf";
            pdfFile = true;
        }

        // Add .zip as appropriate
        if ("zip".Equals(docType))
        {
            docName += ".zip";
        }

        string mimetype;
        if (pdfFile)
        {
            mimetype = "application/pdf";
        }
        else if ("zip".Equals(docType))
        {
            mimetype = "application/zip";
        }
        else
        {
            mimetype = "application/octet-stream";
        }

        Write_StreamtoFile(results, mimetype, docName, FilePath);

    }
    private void Write_StreamtoFile(Stream results, string mimetype, string docName, string path)
    {
        if (!path.EndsWith(@"\"))
        {
            path += @"\";
        }

        if (!Directory.Exists(path))
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            Directory.CreateDirectory(path);
        }

        if (File.Exists(Path.Combine(path, docName)))
        {
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(Path.Combine(path, docName));
        }

        using (FileStream fs = new FileStream(Path.Combine(path, docName), FileMode.CreateNew, FileAccess.Write))
        {
            CopyStream(results, fs);
        }

        results.Close();
        results.Flush();
    }
    public static void CopyStream(Stream input, Stream output)
    {
        byte[] buffer = new byte[8 * 1024];
        int len;
        while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, len);
        }
    }

    #endregion
}

public class EnvelopeDocumentsNew
{
    public string EnvelopeId { get; set; }
    public List<EnvelopeDocItemNew> Documents { get; set; }
}

public class EnvelopeDocItemNew
{
    public string Name { get; set; }
    public string Type { get; set; }
    public string DocumentId { get; set; }
}