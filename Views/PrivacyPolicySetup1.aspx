﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="PrivacyPolicySetup1.aspx.cs" Inherits="Views_PrivacyPolicySetup1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        h3, .h3
        {
            font-size: 1.2rem !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="kt-portlet">
                    <div class="form-group row">
                        <div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
                        </div>
                        <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                            <div class="form-group row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                                    <asp:Button ID="btnRequest" runat="server" CssClass="btn btn-danger" Text="Request Details"
                                        TabIndex="1" ToolTip="Click here to View request Details" PostBackUrl="~/Views/PrivacyPolicyRequest.aspx" />
                                    &nbsp;
                                    <asp:Button ID="btnMailSetup" runat="server" CssClass="btn btn-danger" Text="Add New Email"
                                        TabIndex="1" ToolTip="Click here to add" OnClick="btnMailSetup_Click" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:GridView ID="gvEmail" runat="server" AutoGenerateColumns="False" Width="100%"
                                        CssClass="table" HeaderStyle-CssClass="thead-dark" DataKeyNames="PID" EmptyDataText="No Data Found">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                                                    <asp:Label ID="lblPID" Visible="false" runat="server" Text='<%# Bind("PID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="First Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-Wrap="true" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-Wrap="true" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email Id" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-Wrap="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmailID" runat="server" Text='<%# Bind("EmailID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                                        OnClientClick="if (!confirm('Are you sure you want delete Privacy Policy Email Setup?')) return false;"
                                                        CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:ModalPopupExtender ID="mpePrivacy" runat="server" PopupControlID="pnlPrivacy"
        CancelControlID="LinkButton1" TargetControlID="btnMailSetup" Y="20" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlPrivacy" runat="server" role="document" Style="display: none; overflow-y: auto;
        overflow: auto; width: 40% !important; overflow-x: hidden;">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="background: white;">
                <div class="modal-header">
                    <h5 class="modal-title" style="color: Black; font-size: 1.3rem; font-weight: 600;">
                        Add New Email
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">
                            First Name
                        </label>
                        <div class="col-lg-8">
                            <asp:TextBox ID="txtFirstName" CssClass="form-control" TabIndex="1" runat="server"
                                MaxLength="50" ToolTip="Enter First Name and limit is upto 50" Width="95%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">
                            Last Name
                        </label>
                        <div class="col-lg-8">
                            <asp:TextBox ID="txtLastName" CssClass="form-control" TabIndex="2" runat="server"
                                MaxLength="50" ToolTip="Enter Last Name and limit is upto 50" Width="95%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">
                            Email Id
                        </label>
                        <div class="col-lg-8">
                            <asp:TextBox ID="txtEmail" CssClass="form-control" TabIndex="3" runat="server" MaxLength="50"
                                ToolTip="Enter Email and limit is upto 50" Width="95%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 text-center mt-6">
                            <asp:Button ID="btnAddToList" runat="server" CssClass="btn btn-danger" Text="Add to list"
                                TabIndex="5" ToolTip="Click here to add" OnClick="btnAddToList_Click" />&nbsp;&nbsp;
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" Text="Close"
                                CausesValidation="false" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdPPSID" runat="server" />
    <asp:HiddenField ID="lblMessage" runat="server" />
</asp:Content>
