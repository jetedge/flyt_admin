﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="AirportMaster.aspx.cs" Inherits="Views_AirportMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row" id="tblGrid" runat="server">
        <div class="col-xl-12" style="padding-left: 3%; padding-right: 4%;">
            <div class="kt-portlet">
                <div class="kt-portlet__body p-0">
                    <div class="row form-group">
                        <div class="col-lg-12 text-right">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" TabIndex="1" />
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 p-0 boxshadow table-responsive">
                            <asp:GridView ID="gvAirport" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                PagerStyle-CssClass="pager" GridLines="Both" OnSelectedIndexChanged="gvPricingDetails_SelectedIndexChanged"
                                OnRowDataBound="gvTailDetails_OnRowDataBound" Width="100%" AutoGenerateColumns="false"
                                PageSize="25" AllowPaging="True" OnPageIndexChanging="gvPricingDetails_PageIndexChanging"
                                TabIndex="7">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rowid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrowid" Text='<%# Eval("RowID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCountry" Text='<%# Eval("Country") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblicaocity" Text='<%# Eval("ICAOCity") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ICAO" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblicao" Text='<%# Eval("ICAO") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                        HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                        HeaderStyle-Wrap="false" Visible="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkactiveM" TabIndex="7" runat="server" Checked='<%# Convert.ToBoolean(Eval("ActiveAirport")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Show in Emptyleg" HeaderStyle-Width="10%" ItemStyle-Width="10%"
                                        HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                        HeaderStyle-Wrap="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblicao" Text="Landings" runat="server"></asp:Label>
                                            <%--<br />
                                            <asp:Label ID="Label1" Text="(Check if applicable)" Style="font-size: 11px; background: rgb(236, 234, 234);"
                                                runat="server"></asp:Label>--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkshow" TabIndex="7" Enabled="false" runat="server" Checked='<%# Convert.ToBoolean(Eval("ShowinEmptylegshow")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedby" Text='<%# Eval("Created") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated On" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                        ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedon" Text='<%# Eval("createdon") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%"
                                        ItemStyle-Width="8%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbView1" runat="server" TabIndex="4" ImageUrl="~/Images/pencil_Edit.png"
                                                CommandName="Select" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%"
                                        ItemStyle-Width="8%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" TabIndex="4" ImageUrl="~/Images/icon_delete.png"
                                                CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="tblForm" runat="server" visible="false">
        <div class="col-xl-1">
        </div>
        <div class="col-xl-10">
            <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                <div class="form-group row">
                    <div class="col-xl-12 text-right">
                        <label class="importantlabels" id="dvMand" runat="server" visible="false">
                            <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                        </label>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                            ValidationGroup="SP" />
                        <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                            OnClick="btnViewOnClick" />
                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                        <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="kt-portlet__body p-4">
                    <div class="row" style="width: 100%;">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 10%; padding-right: 0px;">
                                    Country <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="txtcountry" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                        ToolTip="Enter all characters. Length is limited to 100" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcountry"
                                        Display="None" ErrorMessage="Country is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 20%;">
                                    City
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="txtcity" runat="server" CssClass="form-control" TabIndex="11" autocomplete="off"
                                        ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtcity"
                                        Display="None" ErrorMessage="City is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 10%;">
                                    ICAO <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="txtICAO" runat="server" CssClass="form-control" TabIndex="6" MaxLength="100"
                                        autocomplete="off" ToolTip="Enter all characters. Length is limited to 100" onkeypress="return Alphabets(event)"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtICAO"
                                        Display="None" ErrorMessage="ICAO is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-3 col-form-label" style="max-width: 20%;">
                                    Runway Length(feet)
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="txtrunwaylength" runat="server" CssClass="form-control" TabIndex="12"
                                        autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtrunwaylength"
                                        Display="None" ErrorMessage="Runway length is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label" style="max-width: 28%;">
                                    Landings(Check if applicable)
                                </label>
                                <div class="col-2" style="padding-top: 15px;">
                                    <asp:CheckBox ID="chkEmpty" Checked="true" TabIndex="9" runat="server" />
                                </div>
                            </div>
                            <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                <div style="float: left; padding-top: 10px;">
                                    <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                        font-weight: 600;"></asp:Label>
                                    <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
</asp:Content>
