﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;

public partial class Views_AirportMaster : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.AirportMasterBLL objMember = new BusinessLayer.AirportMasterBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Airport Master");
            }


            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Airport Master";


            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();


            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
            hdfPSRID.Value = "0";
            btnAdd.Focus();
            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    #region Button Events

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            if (btnSave.Text == "Save")
            {
                DataTable dt = objMember.AlreadyExist(txtICAO.Text.Trim());
                if (dt.Rows.Count > 0)
                {
                    txtICAO.Focus();
                    lblalert.Text = txtICAO.Text + " is already exist";
                    mpealert.Show();
                    return;
                }
            }

            AddAirport();

            int retVal = objMember.Save(objMember);

            if (retVal > 0)
            {
                lblalert.Text = "Airport Details have been saved successfully";
                mpealert.Show();
                btnAdd.Focus();
                btnViewOnClick(sender, e);
                Clear();
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request";
                mpealert.Show();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            txtcountry.Focus();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            dvMand.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();

            if (gvAirport.Rows.Count > 0)
            {
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
                dvMand.Visible = false;
            }
            else
            {
                tblForm.Visible = true;
                tblGrid.Visible = false;
                btnAdd.Visible = false;
                btnSave.Visible = true;
                btnView.Visible = true;
                dvMand.Visible = true;
            }

            btnAdd.Focus();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            objMember.DeletUser(lblrowtodelete.Text.Trim());
            List();
            lblalert.Text = "User Details have been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvPricingDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvAirport.SelectedRow.FindControl("lblrowid");

            DataTable dtPS = objMember.Edit(lblrowid.Text.Trim());

            if (dtPS != null && dtPS.Rows.Count > 0)
            {
                hdfPSRID.Value = lblrowid.Text;

                txtcountry.Text = dtPS.Rows[0]["Country"].ToString();
                txtICAO.Text = dtPS.Rows[0]["ICAO"].ToString();
                txtcity.Text = dtPS.Rows[0]["ICAOCity"].ToString();
                txtrunwaylength.Text = dtPS.Rows[0]["length"].ToString();


                if (dtPS.Rows[0]["ShowinEmptyleg"].ToString() == "1")
                {
                    chkEmpty.Checked = true;
                }
                else
                {
                    chkEmpty.Checked = false;
                }
            }
            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            dvMand.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            lblrowtodelete.Text = lblrowid.Text;
            mpeconfirm.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }


    protected void gvTailDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;

                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //  lblMsg.Text = ex.Message;
        }
    }

    protected void gvPricingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvAirport.PageIndex = e.NewPageIndex;
            List();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }
    #endregion

    #region  User Defined Function

    public void AddAirport()
    {
        if (btnSave.Text == "Save")
        {
            objMember.ManageType = "I";
        }
        else
        {
            objMember.ManageType = "U";
        }
        objMember.RowId = hdfPSRID.Value;
        objMember.Country = txtcountry.Text.Trim();
        objMember.ICAO = txtICAO.Text.Trim();
        objMember.City = txtcity.Text.Trim();
        objMember.RunWayLength = txtrunwaylength.Text.Trim();
        if (chkEmpty.Checked == true)
        {
            objMember.Active = "1";
        }
        else
        {
            objMember.Active = "0";
        }
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString().Trim();
        //
    }

    public void List()
    {
        DataTable dt = objMember.Select();
        if (dt.Rows.Count > 0)
        {
            gvAirport.DataSource = dt;
            gvAirport.DataBind();
        }
        else
        {
            gvAirport.DataSource = null;
            gvAirport.DataBind();
        }
    }

    public void Clear()
    {
        try
        {
            hdfPSRID.Value = "0";

            txtcountry.Text = string.Empty;
            txtICAO.Text = string.Empty;
            txtcity.Text = string.Empty;
            txtrunwaylength.Text = string.Empty;
            chkEmpty.Checked = true;

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }


    #endregion
}