﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;


public partial class Views_Pricingconfig : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.PricingConfigBLL objMember = new BusinessLayer.PricingConfigBLL();
    EmptyLegPriceCal objEmptyLegPriceCal = new EmptyLegPriceCal();

    #endregion

    public int linenum = 0;
    public string MethodName = string.Empty;

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            try
            {
                // Put user code to initialize the page here

                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Pricing Setup");
                }


                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Admin";
                lblSubHeader.InnerText = "Pricing Setup";


                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblUserId.Text = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                btnSave.Visible = false;
                btnView.Visible = false;

                LoadFleet();
                
                LoadAircrafttype();

                List();
            }
            catch (Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }
    }

    #endregion

    #region Button Events
    public void textBox1_TextChanged(object sender, EventArgs e)
    {
        try
        {
            BindFilter_Grid();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void imgAdd_Click(object sender, EventArgs e)
    {
        try
        {
            mpeUserPref.Show();
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            btnAddMore.Text = "Save";
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    public void imgEdit_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlFleet.SelectedValue == "0")
            {
                lblalert.Text = "Fleet Name is required";
                mpealert.Show();
                return;
            }
            else
            {
                DataTable dtFleet = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from FleetTypeMaster WHere RowId='" + ddlFleet.SelectedValue.Trim() + "' ").Tables[0];

                if (dtFleet.Rows.Count > 0)
                {
                    txtCode.Text = dtFleet.Rows[0]["FleetCode"].ToString();
                    txtName.Text = dtFleet.Rows[0]["FleetName"].ToString();
                    rblActive.SelectedIndex = rblActive.Items.IndexOf(rblActive.Items.FindByValue(dtFleet.Rows[0]["Active"].ToString().Trim()));
                    btnAddMore.Text = "Update";
                    lblEditFleetId.Text = dtFleet.Rows[0]["RowID"].ToString();
                }
                else
                {
                    btnAddMore.Text = "Save";
                    lblEditFleetId.Text = "0";
                }

                mpeUserPref.Show();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnAddMore_Click(object sender, EventArgs e)
    {
        try
        {
            if (btnAddMore.Text == "Save")
            {
                DataTable dtFleet = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from FleetTypeMaster WHere FleetName='" + txtName.Text.Trim() + "' ").Tables[0];

                if (dtFleet.Rows.Count > 0)
                {
                    lblalert.Text = "Fleet Name [ " + txtName.Text + " ] already exist";
                    mpeUserPref.Show();
                    mpealert.Show();
                    return;
                }

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO FleetTypeMaster (FleetCode,FleetName,Active,Createdby,CreatedOn) VALUES ('" + txtCode.Text.Trim() + "','" + txtName.Text.Trim() + "','" + rblActive.SelectedValue.Trim() + "','" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "','" + DateTime.Now + "')");
                lblalert.Text = "Fleet Name [ " + txtName.Text + " ] saved successfully";
            }
            else
            {
                DataTable dtFleet = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from FleetTypeMaster WHere FleetName='" + txtName.Text.Trim() + "' and RowID='" + lblEditFleetId.Text.Trim() + "' ").Tables[0];

                if (dtFleet.Rows.Count > 0)
                {
                    lblalert.Text = "Fleet Name [ " + txtName.Text + " ] already exist";
                    mpeUserPref.Show();
                    mpealert.Show();
                    return;
                }
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE FleetTypeMaster set FleetCode='" + txtCode.Text.Trim() + "',FleetName='" + txtName.Text.Trim() + "',Active='" + rblActive.SelectedValue.Trim() + "',Createdby='" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "',CreatedOn='" + DateTime.Now + "' WHERE ROWID='" + lblEditFleetId.Text.Trim() + "'");
                lblalert.Text = "Fleet Name [ " + txtName.Text + " ] updated successfully";
            }
            lblEditFleetId.Text = "0";
            btnAddMore.Text = "Save";
            LoadFleet();

            mpealert.Show();

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    private void BindFilter_Grid()
    {
        DataTable dtDiscountCoupon = new DataTable();
        dtDiscountCoupon = objMember.FilterGrid(txtTailFilter.Text);

        if (dtDiscountCoupon != null && dtDiscountCoupon.Rows.Count > 0)
        {
            gvpricing.DataSource = dtDiscountCoupon;
            gvpricing.DataBind();
        }
        else
        {
            gvpricing.DataSource = null;
            gvpricing.DataBind();
        }
    }

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            mpeprice.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void Buttonyes_Click(object sender, EventArgs e)
    {
        try
        {
            Saveyes();
            mpeprice.Hide();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void Buttonno_Click(object sender, EventArgs e)
    {
        try
        {
            Saveno();
            mpeprice.Hide();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    public void Saveyes()
    {
        string strActive = string.Empty;
        string strenabletrip = string.Empty;
        string strapplytax = string.Empty;

        if (chkActive.Checked == true)
        {
            strActive = "1";
        }
        else
        {
            strActive = "0";
        }

        if (chktaxapply.Checked == true)
        {
            strapplytax = "1";
        }
        else
        {
            strapplytax = "0";
        }

        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup Set FleetId='" + ddlFleet.SelectedValue.Trim() + "', minbd='" + txtStartTime1.Value + "',maxbd='" + Time1.Value + "',minsellable='" + Time2.Value + "',HourlyRate='" + txthourlyrateN.Value + "',Fixedfloor='" + txtfloorN.Value + "',fixedceiling='" + txtCielN.Value + "',Active='" + strActive + "',applytax='" + strapplytax + "',pricingcompleted='1',createdby='" + Request.Cookies["JETRIPSADMIN"]["userid"].ToString() + "',createdon='" + DateTime.Now.ToString() + "',length='" + txtrunwaylength.Value + "',Aircraftype='" + ddlaircraft.SelectedValue + "',AvgSpeed='" + txtAvgspeed.Value + "',Operationalcost='" + txtopcost.Value + "',BaseAirport='" + txtBaseAirport.Value + "',Noofseats='" + txtNoofseats.Value + "',YieldRevenue='" + txtYielldRev.Value.Trim() + "' where TailNo='" + lbltail.Text + "' ");
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup_tripDetails Set minbd='" + txtStartTime1.Value + "',maxbd='" + Time1.Value + "',minsellable='" + Time2.Value + "',HourlyRate='" + txthourlyrateN.Value + "',Fixedfloor='" + txtfloorN.Value + "',fixedceiling='" + txtCielN.Value + "',Active='" + strActive + "',applytax='" + strapplytax + "',pricingcompleted='1',createdby='" + Request.Cookies["JETRIPSADMIN"]["userid"].ToString() + "',createdon='" + DateTime.Now.ToString() + "' where TailNo='" + lbltail.Text + "' ");

        objEmptyLegPriceCal.FunEmptyPriceCal();
        List();

        tblForm.Visible = false;
        tblGrid.Visible = true;
        btnSave.Visible = false;
        btnView.Visible = false;

        lblalert.Text = "Pricing setup details have been saved successfully";
        mpealert.Show();

    }
    protected void btnUpload_Click(object sender, System.EventArgs e)
    {
        try
        {
            string FileURL = SecretsBLL.TailGallery + lbltail.Text + @"\";
            bool exists = Directory.Exists(FileURL.ToString());
            if (!exists)
            {
                Directory.CreateDirectory(FileURL.ToString());
            }

            string strInterior = string.Empty;
            string strExterior = string.Empty;

            if (fupSlider1.HasFile)
            {
                fupSlider1.SaveAs(FileURL + removeSpecialChar(fupSlider1.FileName));
                strInterior = removeSpecialChar(fupSlider1.FileName);
            }
            else if (lblSlider1.Text.Length > 0)
            {
                strInterior = removeSpecialChar(lblSlider1.Text);
            }

            if (fupSlider2.HasFile)
            {
                fupSlider2.SaveAs(FileURL + removeSpecialChar(fupSlider2.FileName));
                strExterior = removeSpecialChar(fupSlider2.FileName);
            }
            else if (lblSlider2.Text.Length > 0)
            {
                strExterior = removeSpecialChar(lblSlider2.Text);
            }

            string TailImagesId = objMember.Insert_TailImages(lbltail.Text, FileURL, strInterior, strExterior, lblUserId.Text);

            Select_TailImages(lbltail.Text);

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    public static string removeSpecialChar(string input)
    {
        return Regex.Replace(input, "[^0-9A-Za-z.-]", "_");
    }
    public void Saveno()
    {
        try
        {
            string strActive = string.Empty;
            string strenabletrip = string.Empty;
            string strapplytax = string.Empty;
            if (chkActive.Checked == true)
            {
                strActive = "1";
            }
            else
            {
                strActive = "0";

            }

            if (chktaxapply.Checked == true)
            {
                strapplytax = "1";
            }
            else
            {
                strapplytax = "0";
            }
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup Set minbd='" + txtStartTime1.Value + "',maxbd='" + Time1.Value + "',minsellable='" + Time2.Value + "',HourlyRate='" + txthourlyrateN.Value + "',Fixedfloor='" + txtfloorN.Value + "',fixedceiling='" + txtCielN.Value + "',Active='" + strActive + "',applytax='" + strapplytax + "',pricingcompleted='1',createdby='" + Request.Cookies["JETRIPSADMIN"]["userid"].ToString() + "',createdon='" + DateTime.Now.ToString() + "',length='" + txtrunwaylength.Value + "',Aircraftype='" + ddlaircraft.SelectedValue + "',AvgSpeed='" + txtAvgspeed.Value + "',Operationalcost='" + txtopcost.Value + "' ,BaseAirport='" + txtBaseAirport.Value + "',Noofseats='" + txtNoofseats.Value + "',YieldRevenue='" + txtYielldRev.Value.Trim() + "' where TailNo='" + lbltail.Text + "' ");
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set flag='OLD'");

            objEmptyLegPriceCal.FunEmptyPriceCal();

            lblalert.Text = "Pricing setup details have been saved successfully";
            mpealert.Show();
            DataTable dtPS2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *,CASE WHEN Tailsetup.active='1'  THEN 'TRUE' WHEN Tailsetup.active = '0' THEN 'FALSE' ELSE 'FALSE' END AS Show,FirstName +' '+ LastName as created,createdon from Tailsetup left join UserMasterCreate on Convert(int,Tailsetup.Createdby)=UserMasterCreate.RowID Where TailNo='" + lbltail.Text + "' ").Tables[0];
            if (dtPS2.Rows.Count > 0)
            {
                //lblupdatedby.Text = Convert.ToString(dtPS2.Rows[0]["created"]);
                //lblupdatedon.Text = Convert.ToString(dtPS2.Rows[0]["createdon"]);
            }
            else
            {
                //lblupdatedby.Text = "";
                //lblupdatedon.Text = "";
            }
            hdfPSRID.Value = "0";
            List();
            tblForm.Visible = false;
            dvformSub.Visible = false;
            tblGrid.Visible = true;
            //dvFilter.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            if (txtTailFilter.Text.Length > 0)
            {
                BindFilter_Grid();
            }
            else
            {
                List();
            }

            tblForm.Visible = false;
            tblGrid.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void btnSavedata_Click(object sender, EventArgs e)
    {


    }
    protected void ContactsGridView_RowCommandNew(Object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int index = Convert.ToInt32(e.CommandArgument);

            GridView childGrid = (GridView)sender;

            object str1 = childGrid.DataKeys[index].Value;
            string SUID = str1.ToString();

            if (((Label)gvpricing.Rows[index].FindControl("lblcreatedby")).Text.Length > 0)
            {
                divLMBy.Visible = true;
                lblLMby.Text = ((Label)gvpricing.Rows[index].FindControl("lblcreatedby")).Text + " ( on ) " + ((Label)gvpricing.Rows[index].FindControl("lblcreatedon")).Text;
            }



            DataTable dtPS = objMember.Edit(SUID.Trim());
            if (dtPS.Rows.Count > 0)
            {
                DataTable dtPS1 = objMember.TailDetails(SUID.Trim());
                if (dtPS1.Rows.Count > 0)
                {
                    gv_Child_Tail.DataSource = dtPS1;
                    gv_Child_Tail.DataBind();
                }
                else
                {
                    gv_Child_Tail.DataSource = null;
                    gv_Child_Tail.DataBind();
                }

                lblETail.Text = dtPS.Rows[0]["TailNo"].ToString();
                lbltail.Text = dtPS.Rows[0]["TailNo"].ToString();
                tail.InnerText = dtPS.Rows[0]["TailNo"].ToString();

                Select_TailImages(lbltail.Text);

                if (Convert.ToDecimal(dtPS.Rows[0]["MINBD"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // txtStartTime1.Value = string.Empty;
                }
                else
                {
                    txtStartTime1.Value = dtPS.Rows[0]["MINBD"].ToString();
                }

                if (Convert.ToDecimal(dtPS.Rows[0]["MAXBD"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // Time1.Value = string.Empty;
                }
                else
                {
                    Time1.Value = dtPS.Rows[0]["MAXBD"].ToString();
                }

                if (Convert.ToDecimal(dtPS.Rows[0]["MinSellable"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // Time2.Value = "";
                }
                else
                {
                    Time2.Value = Convert.ToString(dtPS.Rows[0]["MinSellable"]);
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["HourlyRate"].ToString()) == Convert.ToDecimal(0.00))
                {
                    ///   txthourlyrateN.Value = "";
                }
                else
                {
                    txthourlyrateN.Value = dtPS.Rows[0]["HourlyRate"].ToString();
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["Fixedfloor"].ToString()) == Convert.ToDecimal(0.00))
                {
                    //  txtfloorN.Value = string.Empty;
                }
                else
                {
                    txtfloorN.Value = dtPS.Rows[0]["Fixedfloor"].ToString();
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["Fixedceiling"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // txtCielN.Value = string.Empty;
                }
                else
                {
                    txtCielN.Value = dtPS.Rows[0]["Fixedceiling"].ToString();
                }
                txtYielldRev.Value = dtPS.Rows[0]["YieldRevenue"].ToString();
                //txtStartTime1.Value = Convert.ToString(dtPS.Rows[0]["MINBD"]);
                //Time1.Value = Convert.ToString(dtPS.Rows[0]["MAXBD"]);
                //Time2.Value = Convert.ToString(dtPS.Rows[0]["MinSellable"]);
                //  txthourlyrateN.Value = Convert.ToString(dtPS.Rows[0]["HourlyRate"]);
                //  txtfloorN.Value = Convert.ToString(dtPS.Rows[0]["Fixedfloor"]);
                //  txtCielN.Value = Convert.ToString(dtPS.Rows[0]["Fixedceiling"]);
                if (dtPS.Rows[0]["length"].ToString() == "0")
                {
                    // txtCielN.Value = string.Empty;
                }
                else
                {
                    txtrunwaylength.Value = dtPS.Rows[0]["length"].ToString();
                }
                if (dtPS.Rows[0]["AvgSpeed"].ToString() == "0")
                {
                    // txtCielN.Value = string.Empty;
                }
                else
                {
                    txtAvgspeed.Value = dtPS.Rows[0]["AvgSpeed"].ToString();
                }

                if (dtPS.Rows[0]["Operationalcost"].ToString() == "0")
                {
                    // txtCielN.Value = string.Empty;
                }
                else
                {
                    txtopcost.Value = dtPS.Rows[0]["Operationalcost"].ToString();
                }
                if (dtPS.Rows[0]["Baseairport"].ToString() == "0")
                {
                    // txtCielN.Value = string.Empty;
                }
                else
                {
                    txtBaseAirport.Value = dtPS.Rows[0]["Baseairport"].ToString();
                }
                if (dtPS.Rows[0]["noofseats"].ToString() == "0")
                {
                    // txtCielN.Value = string.Empty;
                }
                else
                {
                    txtNoofseats.Value = dtPS.Rows[0]["noofseats"].ToString();
                }
                ddlaircraft.SelectedIndex = ddlaircraft.Items.IndexOf(ddlaircraft.Items.FindByText(dtPS.Rows[0]["Aircraftype"].ToString().Trim()));
                ddlFleet.SelectedIndex = ddlFleet.Items.IndexOf(ddlFleet.Items.FindByValue(dtPS.Rows[0]["FleetId"].ToString().Trim()));

                //   ddlaircraft.SelectedValue = dtPS.Rows[0]["Aircraftype"].ToString();

                if (dtPS.Rows[0]["Show"].ToString() == "TRUE")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }
                if (dtPS.Rows[0]["applytax"].ToString() == "1")
                {
                    chktaxapply.Checked = true;
                }
                else
                {
                    chktaxapply.Checked = false;
                }
            }



            tblForm.Visible = true;
            dvformSub.Visible = true;
            tblGrid.Visible = false;
            //dvFilter.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            txtStartTime1.Focus();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }


    }
    protected void gvPricingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvpricing.PageIndex = e.NewPageIndex;
            List();
            //dvFilter.Visible = true;
            tblForm.Visible = false;
            tblGrid.Visible = true;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void gvpricing_price_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Image imgctrl = e.Row.FindControl("Image1") as Image;
                Label lblprice = e.Row.FindControl("lblprice") as Label;

                if (lblprice.Text == "1")
                {
                    imgctrl.ImageUrl = "~/Components/images/tick_yes.png";
                }
                else
                {
                    imgctrl.ImageUrl = "~/Components/images/tick_no.png";
                }

                Image imgGalleryFlag = e.Row.FindControl("imgGalleryFlag") as Image;
                Label lblGalleryFlag = e.Row.FindControl("lblGalleryFlag") as Label;
                if (lblGalleryFlag.Text == "Y")
                {
                    imgGalleryFlag.ImageUrl = "~/Images/gallery-icon.jpg";
                }

                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;
                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void gv_Child_Tail_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() != "admin@demo.com")
                {
                    e.Row.Cells[2].Visible = true;
                }
                else
                {
                    e.Row.Cells[2].Visible = true;
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Image imgctrl = e.Row.FindControl("Image1") as Image;
                Label lblprice = e.Row.FindControl("lblprice") as Label;
                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;
                Label lblleg = e.Row.FindControl("lblleg") as Label;
                Label lbltailflag = e.Row.FindControl("lbltailflag") as Label;
                // Label lbltripno = e.Row.FindControl("lbltripno") as Label;
                if (lblprice.Text == "1")
                {
                    //Get the Image here
                    imgctrl.ImageUrl = "~/Components/images/tick_yes.png";
                }
                else
                {
                    imgctrl.ImageUrl = "~/Components/images/tick_no.png";
                }

                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        //gvinvoice.Rows[i].Cells[6].Font.Bold = true;
                    }
                }
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() == "admin@demo.com")
                {
                    if (lbltailflag.Text == "EMPTY")
                    {
                        e.Row.BackColor = System.Drawing.Color.WhiteSmoke;
                    }
                }
                else
                {
                    if (lbltailflag.Text == "EMPTY")
                    {
                        e.Row.BackColor = System.Drawing.Color.WhiteSmoke;
                    }
                }
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() == "admin@demo.com")
                {
                    if (lblleg.Text == "0")
                    {
                        e.Row.Cells[2].Text = "";
                    }
                }
                else
                {
                    if (lblleg.Text == "0")
                    {
                        e.Row.Cells[2].Text = "";
                    }

                }
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() != "admin@demo.com")
                {

                    e.Row.Cells[2].Visible = true;
                }
                else
                {

                    e.Row.Cells[2].Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void gvPricingDetailschage_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblSubID = (Label)gv_Child_Tail.SelectedRow.FindControl("lblSubID");
            Label lbltripno = (Label)gv_Child_Tail.SelectedRow.FindControl("lbltripno");
            Label lbldateD = (Label)gv_Child_Tail.SelectedRow.FindControl("lbldateD");
            Label lblstarttime = (Label)gv_Child_Tail.SelectedRow.FindControl("lblstarttime");

            DataTable dtPS = objMember.PopDetails(lbltripno.Text, lblSubID.Text, lbldateD.Text, lblstarttime.Text);
            if (dtPS.Rows.Count > 0)
            {
                lblTailData.Text = Convert.ToString(dtPS.Rows[0]["TailNo"]);
                lblTripData.Text = Convert.ToString(dtPS.Rows[0]["TripNO"]);
                lblDateData.Text = Convert.ToDateTime(dtPS.Rows[0]["AvailbleFrom"]).ToString("MM-dd-yyyy");
                lblStartData.Text = Convert.ToString(dtPS.Rows[0]["StartAirport"]);
                lblendData.Text = Convert.ToString(dtPS.Rows[0]["EndAirport"]);
                lblscitydata.Text = Convert.ToString(dtPS.Rows[0]["startcity"]);
                lblEcityData.Text = Convert.ToString(dtPS.Rows[0]["Endcity"]);
                lblStimedata.Text = Convert.ToString(dtPS.Rows[0]["starttime"]);
                lblEtimeData.Text = Convert.ToString(dtPS.Rows[0]["endtime"]);
                lbldate1data.Text = dtPS.Rows[0]["createdon"].ToString();
                lbluserdata.Text = dtPS.Rows[0]["created"].ToString();

                ddlFleet.SelectedIndex = ddlFleet.Items.IndexOf(ddlFleet.Items.FindByValue(dtPS.Rows[0]["FleetId"].ToString()));

                if (Convert.ToDecimal(dtPS.Rows[0]["MINBD"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // txtStartTime1.Value = string.Empty;
                }
                else
                {
                    MintimeMData.Value = Convert.ToString(dtPS.Rows[0]["MINBD"]);
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["MAXBD"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // Time1.Value = string.Empty;

                }
                else
                {
                    MaxtimeMData.Value = Convert.ToString(dtPS.Rows[0]["MAXBD"]);
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["MinSellable"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // Time2.Value = "";


                }
                else
                {
                    minSellMData.Value = Convert.ToString(dtPS.Rows[0]["MinSellable"]);
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["HourlyRate"].ToString()) == Convert.ToDecimal(0.00))
                {
                    ///   txthourlyrateN.Value = "";


                }
                else
                {
                    HourlData.Value = Convert.ToString(dtPS.Rows[0]["HourlyRate"]);
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["Fixedfloor"].ToString()) == Convert.ToDecimal(0.00))
                {
                    //  txtfloorN.Value = string.Empty;


                }
                else
                {
                    FixedfloorData.Value = Convert.ToString(dtPS.Rows[0]["Fixedfloor"]);
                }
                if (Convert.ToDecimal(dtPS.Rows[0]["Fixedceiling"].ToString()) == Convert.ToDecimal(0.00))
                {
                    // txtCielN.Value = string.Empty;


                }
                else
                {
                    fixedceilingData.Value = Convert.ToString(dtPS.Rows[0]["Fixedceiling"]);
                }

                if (dtPS.Rows[0]["Active"].ToString() == "1")
                {
                    chkActiveData.Checked = true;
                }
                else
                {
                    chkActiveData.Checked = false;
                }
                if (dtPS.Rows[0]["applytax"].ToString() == "1")
                {
                    chkTaxData.Checked = true;
                }
                else
                {
                    chkTaxData.Checked = false;
                }
                if (dtPS.Rows[0]["EnableTrip"].ToString() == "1")
                {
                    chkEnableData.Checked = true;
                }
                else
                {
                    chkEnableData.Checked = false;
                }
            }

            mpeData.Show();
            MintimeMData.Focus();

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    #region  User Defined Function

    public void List()
    {
        DataTable dtDiscountCoupon = objMember.Select();
        if (dtDiscountCoupon != null && dtDiscountCoupon.Rows.Count > 0)
        {
            gvpricing.DataSource = dtDiscountCoupon;
            gvpricing.DataBind();
        }
        else
        {
            gvpricing.DataSource = null;
            gvpricing.DataBind();
        }

    }

    public void Select_TailImages(string Tailno)
    {
        lblSlider1.Text = string.Empty;
        lblSlider2.Text = string.Empty;

        imgSlder1.Src = "../assets/img/No_Image_Available.jpg";
        imgSlder2.Src = "../assets/img/No_Image_Available.jpg";

        string FileURL = SecretsBLL.TailImageURL + Tailno + @"\";

        DataTable dtTailImages = objMember.Select_TailImages(Tailno);
        if (dtTailImages != null && dtTailImages.Rows.Count > 0)
        {
            if (dtTailImages.Rows[0]["InteriorImage"].ToString().Length > 0)
            {
                lblSlider1.Text = dtTailImages.Rows[0]["InteriorImage"].ToString();
                imgSlder1.Src = FileURL + dtTailImages.Rows[0]["InteriorImage"].ToString();
            }

            if (dtTailImages.Rows[0]["ExteriorImage"].ToString().Length > 0)
            {
                lblSlider2.Text = dtTailImages.Rows[0]["ExteriorImage"].ToString();
                imgSlder2.Src = FileURL + dtTailImages.Rows[0]["ExteriorImage"].ToString();
            }
        }
    }

    public void SaveData()
    {
        try
        {
            string strActive = string.Empty;
            string strenabletrip = string.Empty;
            string strapplytax = string.Empty;
            if (chkActiveData.Checked == true)
            {
                strActive = "1";
            }
            else
            {
                strActive = "0";

            }

            if (chktaxapply.Checked == true)
            {
                strapplytax = "1";
            }
            else
            {
                strapplytax = "0";
            }

            if (chkEnableData.Checked == true)
            {
                strenabletrip = "1";
            }
            else
            {
                strenabletrip = "0";
            }
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup_tripDetails Set minbd='" + MintimeMData.Value + "',maxbd='" + MaxtimeMData.Value + "',minsellable='" + minSellMData.Value + "',HourlyRate='" + HourlData.Value + "',Fixedfloor='" + FixedfloorData.Value + "',fixedceiling='" + fixedceilingData.Value + "',Enabletrip='" + strenabletrip + "',Active='" + strActive + "',applytax='" + strapplytax + "',pricingcompleted='1',createdby='" + Request.Cookies["JETRIPSADMIN"]["userid"].ToString() + "',createdon='" + DateTime.Now.ToString() + "' where TailNo='" + lblTailData.Text + "' and TripNo='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text).ToString("yyyy-MM-dd") + "' and StartTime='" + lblStimedata.Text + "'");
            mpealert.Show();
            DataTable dtPS = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *,CASE WHEN Tailsetup_tripDetails.active='1'  THEN 'TRUE' WHEN Tailsetup_tripDetails.active = '0' THEN 'FALSE' ELSE 'FALSE' END AS Show,FirstName +' '+ LastName as created,createdon from Tailsetup_tripDetails left join UserMasterCreate on Convert(int,Tailsetup_tripDetails.Createdby)=UserMasterCreate.RowID Where TailNo='" + lbltail.Text + "' and  Showpriceflag='NEW' and Activeflag='ACTIVE' and Bookflag='0' order by AvailbleFrom ,StartTime ").Tables[0];
            if (dtPS.Rows.Count > 0)
            {
                gv_Child_Tail.DataSource = dtPS;
                gv_Child_Tail.DataBind();
            }
            else
            {
                gv_Child_Tail.DataSource = null;
                gv_Child_Tail.DataBind();
            }
            hdfPSRID.Value = "0";
            List();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btncanceldata_Click(object sender, EventArgs e)
    {
        mpeData.Hide();
        MintimeMData.Value = "0.0";
        MaxtimeMData.Value = "0.0";
        minSellMData.Value = "0.0";
        HourlData.Value = "0.00";
        FixedfloorData.Value = "0.00";
        fixedceilingData.Value = "0.00";

        chkEnableData.Checked = true;
        chkActiveData.Checked = true;
        chkTaxData.Checked = true;

    }
    public void LoadAircrafttype()
    {
        try
        {
            DataTable dtaircraft = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select distinct  Aircraft_Name from JETEDGE_FUEL..[ST-TailSetup] Inner join JETEDGE_FUEL..AircraftSubType on JETEDGE_FUEL..AircraftSubType.Row_ID=JETEDGE_FUEL..[ST-TailSetup].AirCarftSubType Inner join JETEDGE_FUEL..AircraftType on JETEDGE_FUEL..AircraftType.Row_ID=JETEDGE_FUEL..[ST-TailSetup].AircraftType ").Tables[0];
            if (dtaircraft != null && dtaircraft.Rows.Count > 0)
            {
                ddlaircraft.DataSource = dtaircraft;
                ddlaircraft.DataValueField = "Aircraft_Name";
                ddlaircraft.DataTextField = "Aircraft_Name";
                ddlaircraft.DataBind();
                ddlaircraft.Items.Insert(0, new ListItem("Please Select", "None"));
            }
            else
            {
                ddlaircraft.DataSource = null;
                ddlaircraft.DataBind();
                ddlaircraft.Items.Insert(0, new ListItem("Please Select", "None"));
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }
    public void LoadFleet()
    {
        try
        {
            DataTable dtFleet = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select distinct RowId, FleetName from FleetTypeMaster order by FleetName ").Tables[0];
            if (dtFleet != null && dtFleet.Rows.Count > 0)
            {
                ddlFleet.DataSource = dtFleet;
                ddlFleet.DataValueField = "RowId";
                ddlFleet.DataTextField = "FleetName";
                ddlFleet.DataBind();
                ddlFleet.Items.Insert(0, new ListItem("Please Select", "0"));
            }
            else
            {
                ddlFleet.DataSource = null;
                ddlFleet.DataBind();
                ddlFleet.Items.Insert(0, new ListItem("Please Select", "0"));
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }



    public void Clear()
    {
        try
        {
            hdfPSRID.Value = "0";
            hdfPSRID.Value = "0";
            txthourlyrateN.Value = "0.00";
            txtfloorN.Value = "0.00";
            txtCielN.Value = "0.00";
            txtMaxBD.Value = "";
            txtMaxBD.Value = "";
            txtMinSellHrs.Value = "";
            txthourlyrateN.Value = string.Empty;
            txtfloorN.Value = string.Empty;
            txtCielN.Value = string.Empty;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText, int count)
    {
        return AutoFillProducts(prefixText);
    }

    private static List<string> AutoFillProducts(string prefixText)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {
                com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;


            }

        }
    }

    #endregion
}