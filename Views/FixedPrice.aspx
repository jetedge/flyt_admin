﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="FixedPrice.aspx.cs" Inherits="Views_FixedPrice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" language="javascript">
        function ConfirmDF() {
            if (confirm('Are you sure to delete?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function txtPriceFromfun(object) {
            var txtAvaFrom1 = document.getElementById('txtPriceFrom1');
            document.getElementById('txtPriceFrom').value = txtPriceFrom1.value;

            if (document.getElementById("txtPriceFrom1").value != '' && document.getElementById("txtPriceTo1").value != '') {
                var endDate = new Date(document.getElementById("txtPriceFrom1").value);
                var startDate = new Date(document.getElementById("txtPriceTo1").value);

                if (startDate > endDate) {
                    document.getElementById('lblalert').innerHTML = 'Please ensure that the Date To is greater than or equal to the Date From.';
                    $find("mpealert").show();
                    // alert("Please ensure that the Date To is greater than or equal to the Date From.");
                    sender._textbox.set_Value('')
                    return false;
                }
            }
        }
        function txtPriceTofun(object) {
            var txtAvaTo1 = document.getElementById('txtPriceTo1');
            document.getElementById('txtPriceTo').value = txtPriceTo1.value;

            if (document.getElementById("txtPriceFrom1").value != '' && document.getElementById("txtPriceTo1").value != '') {
                var endDate = new Date(document.getElementById("txtPriceTo1").value);
                var startDate = new Date(document.getElementById("txtPriceFrom1").value);

                if (startDate > endDate) {
                    document.getElementById('lblalert').innerHTML = 'Please ensure that the Date To is greater than or equal to the Date From.';
                    $find("mpealert").show();
                    sender._textbox.set_Value('')
                    return false;
                }
            }
        }
        function datetimeupdate() {
            debugger;
            document.getElementById('txtPriceFrom1').value = document.getElementById('txtPriceFrom').value;
            document.getElementById('txtPriceTo1').value = document.getElementById('txtPriceTo').value;


        }
        function CheckDate(sender, args) {
        }
        function isNumberKey(evt, id) {
            try {
                var charCode = (evt.which) ? evt.which : event.keyCode;

                if (charCode == 46) {
                    var txt = document.getElementById(id).value;
                    if (!(txt.indexOf(".") > -1)) {
                        return true;
                    }
                }
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            } catch (w) {
                alert(w);
            }
        }
        function isNumberKeyNoDot(evt, id) {
            try {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if ((charCode > 31) && (charCode < 48 || charCode > 57 || charCode == 46))
                    return false;
            } catch (w) {
                alert(w);
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        function Alphabets(evt) {
            var keyCode = (evt.which) ? evt.which : evt.keyCode
            if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32 && keyCode != 9 && keyCode != 8 && keyCode != 46)
                return false;
            return true;
        }
    </script>
    <script type="text/javascript" language="javascript">
        function Amount(inputField) {
            var isValid = /^\d{0,6}(\.\d{1,2})?$/.test(inputField.value);
            if (isValid) {
            }
            else {
                var number = inputField.value;
                var n = parseFloat(number).toFixed(2);
                inputField.value = n;
            }
            return isValid;
        }
    </script>
    <script type="text/javascript" language="javascript">
        function CheckDate(sender, args) {

            //            var today = new Date();
            //            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            //            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            //            var dateTime = date + ' ' + time;

            //            var today1 = sender._selectedDate;
            //            var date1 = today1.getFullYear() + '-' + (today1.getMonth() + 1) + '-' + today1.getDate();
            //            var time1 = today1.getHours() + ":" + today1.getMinutes() + ":" + today1.getSeconds();
            //            var dateTime1 = date1 + ' ' + time1;

            //            var dateOne = new Date(today.getFullYear(), (today.getMonth() + 1), today.getDate()); //Year, Month, Date    
            //            var dateTwo = new Date(today1.getFullYear(), (today1.getMonth() + 1), today1.getDate()); //Year, Month, Date    

            //            if (dateOne > dateTwo) {
            //                document.getElementById('MainContent_lblalert').innerHTML = 'You cannot select past date!';
            //                $find("MainContent_mpealert").show();
            //                // alert("You cannot select past date!");
            //                sender._selectedDate = new Date();
            //                sender._textbox.set_Value('')
            //                return;
            //            }

            if (document.getElementById("MainContent_txtPriceFrom").value != '' && document.getElementById("MainContent_txtPriceTo").value != '') {
                var endDate = new Date(document.getElementById("MainContent_txtPriceTo").value);
                var startDate = new Date(document.getElementById("MainContent_txtPriceFrom").value);

                if (startDate > endDate) {
                    document.getElementById('MainContent_lblalert').innerHTML = 'Please ensure that the date to is greater than or equal to the date from.';
                    $find("MainContent_mpealert").show();

                    sender._textbox.set_Value('')
                    return false;
                }
            }
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row" id="tblGrid" runat="server">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row form-group">
                        <div class="col-xl-4 text-right">
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group row" id="dvGridHeader" runat="server" visible="true">
                                <label class="col-3 col-form-label">
                                    Status
                                </label>
                                <div class="col-8">
                                    <asp:DropDownList ID="ddlstatus" OnSelectedIndexChanged="ddlstatus_onselectedChanged"
                                        Width="80%" runat="server" AutoPostBack="true">
                                        <asp:ListItem Selected="true" Value="Active" Text="Active Pricing"></asp:ListItem>
                                        <asp:ListItem Value="Expire" Text="Expired Pricing"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 text-right" style="padding-right: 3%;">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" TabIndex="1" />
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 boxshadow table-responsive">
                            <asp:GridView ID="gvSpecialPricing" runat="server" EmptyDataText="No Fixed Price Details found"
                                OnSelectedIndexChanged="gvSpecialPricing_SelectedIndexChanged" AutoGenerateColumns="false"
                                OnRowDataBound="gvTailDetails_OnRowDataBound" AllowPaging="True" PageSize="25"
                                CssClass="table" HeaderStyle-CssClass="thead-dark" Width="100%">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrowid" Text='<%# Eval("rowid") %>' runat="server" />
                                            <asp:Label ID="lblTail" Text='<%# Eval("TailNo") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="1%" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="StartDate" HeaderText="Date From" DataFormatString="{0:MMM d, yyyy}"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" HeaderStyle-Width="10%" />
                                    <asp:BoundField DataField="EndDate" HeaderText="Date To" DataFormatString="{0:MMM d, yyyy}"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" HeaderStyle-Width="10%" />
                                    <asp:BoundField DataField="SpecialPriceDescription" HeaderText="Pricing Option" HeaderStyle-Wrap="false"
                                        ItemStyle-Wrap="false" HeaderStyle-Width="10%" />
                                    <asp:BoundField DataField="PriceDetails" HeaderText="Amount" DataFormatString="{0:c2}"
                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="5%" HeaderStyle-Wrap="false"
                                        ItemStyle-Wrap="false" />
                                    <asp:TemplateField HeaderText="Start Airport" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartDisp" Text='<%# Eval("StartDisp") %>' runat="server" />
                                            <asp:Label ID="lblStartAirport" Text='<%# Eval("StartAirport") %>' runat="server"
                                                Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Airport" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndDisp" Text='<%# Eval("EndDisp") %>' runat="server" />
                                            <asp:Label ID="lblEndAirPort" Text='<%# Eval("EndAirPort") %>' runat="server" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated By" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="12%" ItemStyle-Width="15%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedby" Text='<%# Eval("created") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated On" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="13%" ItemStyle-Width="15%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedon" Text='<%# Eval("createdonN") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <HeaderStyle Width="3%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbView1" runat="server" TabIndex="4" ImageUrl="~/Images/pencil_Edit.png"
                                                CommandName="Select" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <HeaderStyle Width="5%" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" TabIndex="4" ImageUrl="~/Images/icon_delete.png"
                                                CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="tblForm" runat="server" visible="false">
        <div class="col-xl-1">
        </div>
        <div class="col-xl-10">
            <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                <div class="form-group row">
                    <div class="col-xl-12 text-right" style="padding-right: 3%;">
                        <label class="importantlabels" id="dvMan" runat="server">
                            <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                ID="lblmandatory" Text="Mandatory fields" />
                        </label>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                            ValidationGroup="SP" />
                        <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                            OnClick="btnViewOnClick" />
                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                        <asp:Label ID="lblrowtodelete" runat="server" Visible="false" />
                    </div>
                </div>
                <div class="kt-portlet__body p-4">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Date From <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox runat="server" ID="txtPriceFrom" Placeholder="MMM d, yyyy" onkeypress="return false;"
                                        TabIndex="2" MaxLength="20" />
                                    <asp:CalendarExtender ID="CalendarExtender2" OnClientDateSelectionChanged="CheckDate"
                                        CssClass="cal_Theme1" runat="server" TargetControlID="txtPriceFrom" Format="MMM d, yyyy"
                                        Enabled="True">
                                    </asp:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="rfvPriceFrom" runat="server" ControlToValidate="txtPriceFrom"
                                        Display="None" ErrorMessage="Date From is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-2 col-form-label">
                                    Amount <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <input id="txtAmount" onblur="Amount(this)" onkeypress="return isNumberKey(event,this.id)"
                                        runat="server" maxlength="20" type="text" tabindex="5" />
                                    <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                                        Display="None" ErrorMessage="Amount is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Date To <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox runat="server" ID="txtPriceTo" TabIndex="2" Placeholder="MMM d, yyyy"
                                        onkeypress="return false;" MaxLength="20" />
                                    <asp:CalendarExtender ID="CalendarExtender1" OnClientDateSelectionChanged="CheckDate"
                                        CssClass="cal_Theme1" runat="server" TargetControlID="txtPriceTo" Format="MMM d, yyyy"
                                        Enabled="True">
                                    </asp:CalendarExtender>
                                    <asp:RequiredFieldValidator ID="rfvPriceTo" runat="server" ControlToValidate="txtPriceTo"
                                        Display="None" ErrorMessage="Date To is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-2 col-form-label">
                                    Start Airport <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="txtSAirport" runat="server" MaxLength="50" Style="text-transform: uppercase;"
                                        TabIndex="6" />
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                        CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                        CompletionSetCount="1" TargetControlID="txtSAirport" ID="AutoCompleteExtender1"
                                        runat="server" FirstRowSelected="true">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSAirport"
                                        Display="None" ErrorMessage="Start Airport is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">
                                    Pricing Option
                                </label>
                                <div class="col-4">
                                    <asp:DropDownList ID="rblPricing" runat="server" Css RepeatColumns="2" TabIndex="4"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Value="H">Per Hour</asp:ListItem>
                                        <asp:ListItem Value="D">Fixed Pricing</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label class="col-2 col-form-label">
                                    End Airport <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <asp:TextBox ID="txtEAirport" runat="server" MaxLength="50" Style="text-transform: uppercase;"
                                        TabIndex="7" />
                                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                                        CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                                        CompletionSetCount="1" TargetControlID="txtEAirport" ID="AutoCompleteExtender2"
                                        runat="server" FirstRowSelected="true">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEAirport"
                                        Display="None" ErrorMessage="End Airport is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                <div style="float: left; padding-top: 10px;">
                                    <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                        font-weight: 600;" />
                                    <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
        </div>
    </div>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr>
                <td style="color: rgb(255,255,255); letter-spacing: 0.5px; vertical-align: middle"
                    align="center">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    &nbsp;
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfSPRID" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
</asp:Content>
