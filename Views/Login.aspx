﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Views_Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>JetEdge | FLYT | Admin</title>
    <meta name="description" content="Latest updates and statistic charts" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="login-signup-modal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false"
        data-backdrop="static">
        <div id="divlog" runat="server">
            <div role="document" style="width: 52%; margin-top: 6%;">
                <table width="100%">
                    <tr>
                        <td width="100%">
                            <div class="modal-content" id="login-modal-content" style="background-color: rgb(17, 24, 32)">
                                <table width="100%">
                                    <tr>
                                        <td width="48%" style="padding-top: 0px; vertical-align: top; padding-top: 1%">
                                            <div>
                                                <h5 style="align: center; margin-left: 5%; font-family: Gotham; font-weight: bold;
                                                    color: rgb(255, 255, 255);">
                                                    <br />
                                                    <b>RETURNING CUSTOMERS</b></h5>
                                            </div>
                                            <div>
                                                <h6 style="float: center; margin-left: 5%; font-family: Gotham; display: none; color: rgb(255, 255, 255);">
                                                    Sign in for a quick and easy checkout process</h6>
                                            </div>
                                            <div class="modal-body" style="border-bottom: none; padding-bottom: 0px">
                                                <div id="login" runat="server" role="form">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon" style="background-color: rgb(153,153,154); border: 1px solid #ccc">
                                                                <span class="glyphicon glyphicon-envelope"></span>
                                                            </div>
                                                            <input name="email" id="txtLoginEmail" autocompletetype="Disabled" runat="server"
                                                                style="padding-top: 25px; background-color: rgb(153,153,154); color: rgb(255,255,255);
                                                                border: 1px solid #ccc; padding-bottom: 25px; width: 100%; font-family: Gotham;
                                                                font-weight: 400; font-size: 1.15vw" class="form-control input-lg" tabindex="1"
                                                                placeholder="Enter Email" required data-parsley-type="email">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtLoginEmail"
                                                                Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="MP">*</asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon" style="background-color: rgb(153,153,154); border: 1px solid #ccc">
                                                                <span class="glyphicon glyphicon-lock"></span>
                                                            </div>
                                                            <input name="password" id="txtPassword" tabindex="2" runat="server" autocompletetype="Disabled"
                                                                style="padding-top: 25px; background-color: rgb(153,153,154); color: rgb(255,255,255);
                                                                border: 1px solid #ccc; font-family: Gotham; padding-bottom: 25px; font-weight: 400;
                                                                font-size: 1.15vw; width: 100%" type="password" class="form-control input-lg"
                                                                placeholder="Enter Password" required data-parsley-length="[6, 10]" data-parsley-trigger="keyup">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPassword"
                                                                Display="None" ErrorMessage="Password is required." SetFocusOnError="True" ValidationGroup="MP">*</asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="width: 100%; float: left; margin-bottom: 47px">
                                                        <asp:LinkButton ID="lnkforgot" OnClick="btnforgetPassOnClick" Style="text-decoration: underline;
                                                            color: rgb(255, 255, 255); font-family: Gotham; font-weight: bold;" UseSubmitBehavior="false"
                                                            runat="server" Text="Forgot Password?" />
                                                    </div>
                                                    <center>
                                                        <asp:Button ID="btnLogin" TabIndex="3" class="buttonLogin" Width="75%" OnClick="logged_Click"
                                                            UseSubmitBehavior="false" runat="server" Text="SIGN IN" />
                                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="MP" ShowMessageBox="true"
                                                            runat="server" ShowSummary="false" />
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="modal-footer" style="border-top: none">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="100%" style="align: left">
                                                            <asp:Label ID="lblforgot" runat="server" Visible="false" Style="font-family: Gotham;
                                                                color: rgb(255,255,255); align: center"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <tr>
                                                            <td style="height: 5px">
                                                            </td>
                                                        </tr>
                                                        <td>
                                                            <table width="100%">
                                                                <tr class="hoverforlogin">
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td width="2%" style="border-left: 1px Solid rgb(153,153,154)">
                                        </td>
                                        <td width="48%">
                                            <div>
                                                <table style="width: 100%">
                                                    <tr style="height: 40px">
                                                        <td style="width: 92%; vertical-align: middle">
                                                            <h5 style="align: center; margin-left: 5%; font-family: Gotham; font-weight: bold;
                                                                color: rgb(255, 255, 255)">
                                                                <br />
                                                                <b>NEW CUSTOMERS </b>
                                                            </h5>
                                                        </td>
                                                        <td style="width: 8%; vertical-align: bottom; padding-top: 5%; padding-right: 5%">
                                                            <asp:LinkButton ID="ImageButton1" runat="server" Style="color: rgb(255,255,255);
                                                                text-decoration: none" CssClass="btncommonclose" Text="Close" OnClick="btncloseLogin"
                                                                CausesValidation="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div>
                                                <h6 style="align: center; margin-left: 5%; display: none; font-family: Gotham; color: Black">
                                                    Proceed to checkout as guest</h6>
                                            </div>
                                            <div style="padding-top: 50px; text-align: center;">
                                                <asp:Button ID="btnCheckout" class="buttonLogin" Width="60%" UseSubmitBehavior="false"
                                                    runat="server" Text="CHECKOUT AS A GUEST" />
                                            </div>
                                            <div>
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                                <h6 style="align: center; margin-left: 5%; font-family: Gotham; color: rgb(255, 255, 255);">
                                                    If you don’t have a Zoom account, you can create one now.
                                                </h6>
                                            </div>
                                            <div style="padding-top: 50px; text-align: center;">
                                                <asp:LinkButton ID="btnregister" Width="60%" class="buttonLogin" Style="text-decoration: none;
                                                    color: rgb(255,255,255)" OnClick="ShowCreateUserPopup" UseSubmitBehavior="false"
                                                    runat="server" Text="SIGNUP NOW!" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="50%">
                        </td>
                        <td>
                            <div class="modal-content" id="forgot-password-modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" style="float: left">
                                        <span class="glyphicon glyphicon-lock"></span>Recover Password!</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <div method="post" id="Forgot" runat="server" role="form">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-envelope"></span>
                                                </div>
                                                <input class="form-control input-lg" id="resetemail" runat="server" placeholder="E-mail">
                                            </div>
                                        </div>
                                        <asp:Button ID="Button3" class="btn btn-success btn-block btn-lg" OnClick="logged_Click"
                                            UseSubmitBehavior="false" runat="server" Text="Submit" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <p>
                                    </p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="signup" style="width: 50%; margin-top: 2%">
            <%--class="modal-content" role="document" signup-modal-content--%>
            <div class="modal-header" style="background: #000000; border-top-left-radius: 0px;
                border-top-right-radius: 0px; border: none">
                <table width="100%">
                    <tr>
                        <td style="width: 50%; text-align: left">
                        </td>
                        <td style="width: 50%; text-align: right">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-body" style="background: #eee">
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <asp:Button ID="btncookie" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpecookie" runat="server" TargetControlID="btncookie"
        PopupControlID="pnlcookie" X="950" Y="30" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlcookie" runat="server" Width="23%" ScrollBars="Auto" Style="display: none;
        background-color: #111820; align: right;">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px;
            padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 18px; padding-top: 15px; padding-left: 20px; color: rgb(255,255,255);
                    font-weight: bold">
                    Cookies
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr style="margin-left: 10%; border-right: 0px; border-left: 0px">
                <td style="color: black; font-size: 11px; padding-left: 20px; vertical-align: middle"
                    align="left">
                    <table width="100%">
                        <tr>
                            <td style="padding-bottom: 8px; font-family: Gotham; font-weight: 400; color: rgb(255,255,255)"
                                nowrap="nowrap">
                                This site uses cookies:
                                <asp:LinkButton ID="LinkButton10" Style="text-decoration: underline; color: White;
                                    font-size: 15px; font-weight: bold" runat="server" CausesValidation="false" Text="Find out more"
                                    OnClick="lnkcookesshow_click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center" nowrap="nowrap">
                    <asp:LinkButton ID="lnkReject" Style="text-decoration: none; display: none; color: White"
                        runat="server" CssClass="Enablered" CausesValidation="false" Text="Reject" OnClick="lnkReject_Onclick" />
                    <asp:LinkButton ID="lnkAllow" Style="text-decoration: none; color: White" OnClick="lnkAllow_Onclick"
                        runat="server" CssClass="Enablered" CausesValidation="false" Text="Accept" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btncookieframe" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpefrmaecookie" runat="server" TargetControlID="btncookieframe"
        PopupControlID="pnlcookieframe" Y="30" BackgroundCssClass="modalBackground" CancelControlID="ImageButton2">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlcookieframe" runat="server" Width="30%" Style="display: none; background-color: #111820;
        align: right;">
        <table width="100%">
            <tr>
                <td style="vertical-align: middle; text-align: left; padding-top: 10px;">
                    <span style="text-align: left; padding-left: 20px; font-size: 18px; font-weight: bold;
                        font-family: Gotham; color: rgb(255,255,255)">Cookies </span>
                </td>
                <td style="vertical-align: bottom; float: right; padding-right: 20px; padding-top: 20px">
                    <asp:LinkButton ID="ImageButton2" runat="server" CssClass="btncommonclose" Style="color: rgb(255,255,255);
                        text-decoration: none" Text="Close" CausesValidation="false" />
                </td>
            </tr>
        </table>
        <%--  <iframe id="framecookies" width="100%" runat="server" name="mainFrame" frameborder="0">
        </iframe>--%>
    </asp:Panel>
    <asp:Button ID="btnloginpopup" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpelogin" runat="server" TargetControlID="btnloginpopup"
        PopupControlID="pnllogin" Y="30" BackgroundCssClass="modalBackground" CancelControlID="btncancellogin2">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnllogin" runat="server" Width="27%" ScrollBars="Auto" Style="display: none;
        background-color: white; align: right;">
        <table width="100%">
            <tr>
                <td width="100%">
                    <div id="Div2">
                        <table width="100%">
                            <tr>
                                <td width="100%">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <td style="vertical-align: top; text-align: center">
                                                    <span style="text-align: center; font-size: 22px; font-family: Gotham, sans-serif;
                                                        color: #b72025">
                                                        <br />
                                                        Thank You
                                                        <br />
                                                        Your account is ready</span>
                                                </td>
                                                <td style="vertical-align: middle; padding-right: 5px">
                                                    <asp:ImageButton ID="btncancellogin2" runat="server" Style="float: right" Height="20px"
                                                        Width="20px" ImageUrl="assets/img/close.png" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="modal-body" style="border-bottom: none">
                                        <div id="Div3" runat="server" role="form">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-envelope"></span>
                                                    </div>
                                                    <input name="email" id="txtemail1" runat="server" style="padding-top: 25px; padding-bottom: 25px;
                                                        width: 100%" class="form-control input-lg" tabindex="1" placeholder="Enter Email"
                                                        required data-parsley-type="email">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtemail1"
                                                        Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="VG">*</asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-lock"></span>
                                                    </div>
                                                    <input name="password" id="txtpassword1" tabindex="2" runat="server" style="padding-top: 25px;
                                                        padding-bottom: 25px; width: 100%" type="password" class="form-control input-lg"
                                                        placeholder="Enter Password" required data-parsley-length="[6, 10]" data-parsley-trigger="keyup">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtpassword1"
                                                        Display="None" ErrorMessage="Password is required." SetFocusOnError="True" ValidationGroup="VG">*</asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group" style="width: 100%; text-align: right; margin-top: 40px">
                                                <asp:LinkButton ID="LinkButton6" OnClick="btnforgetPassOnClick_newuser" Style="text-decoration: underline;
                                                    color: Black; font-family: Gotham;" UseSubmitBehavior="false" runat="server"
                                                    Text="Forgot Password?" />
                                            </div>
                                            <div class="form-group" style="width: 100%; text-align: right; margin-top: 30px">
                                                <asp:Button ID="btnlogin2" TabIndex="3" class="buttonLogin" Width="100%" OnClick="btnlogin2_click"
                                                    UseSubmitBehavior="false" runat="server" Text="SIGN IN" ValidationGroup="VG" />
                                                <asp:ValidationSummary ID="ValidationSummary5" ValidationGroup="VG" ShowMessageBox="true"
                                                    runat="server" ShowSummary="false" />
                                            </div>
                                            <div class="form-group" style="width: 100%; text-align: left; margin-top: 30px">
                                                <asp:Label ID="lblforgotnew" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnotpmodal" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeotp" runat="server" TargetControlID="btnotpmodal"
        PopupControlID="pnlotp" Y="70" BackgroundCssClass="modalBackground" CancelControlID="imgcancelotp">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlotp" runat="server" Width="29%" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); align: right;">
        <table width="100%">
            <tr>
                <td width="100%">
                    <div>
                        <table width="100%">
                            <tr>
                                <td style="vertical-align: top; width: 97%; padding-left: 6%; text-align: left;">
                                    <span style="text-align: center; font-size: 20px; font-family: Gotham; color: rgb(255,255,255)">
                                        <br />
                                        JET EDGE ZOOM </span>
                                </td>
                                <td style="vertical-align: bottom; padding-right: 5px; width: 3%; padding-top: 20px;
                                    padding-right: 22px">
                                    <%--  <asp:ImageButton ID="imgcancelotp" runat="server" Style="float: right" Height="20px"
                                                                        Width="20px" ImageUrl="assets/img/close.png" />--%>
                                    <asp:LinkButton ID="imgcancelotp" runat="server" Style="color: rgb(255,255,255);
                                        text-decoration: none" CssClass="btncommonclose" Text="Close" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-body" style="border-bottom: none; padding-bottom: 10px">
                        <div id="Div5" runat="server" role="form">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon" style="background-color: rgb(153,153,154);">
                                        <i class="fa fa-key" aria-hidden="true"></i>
                                        <%--  <span class="glyphicon glyphicon-envelope"></span>--%>
                                    </div>
                                    <input name="email" id="txtotp" runat="server" style="padding-top: 25px; padding-bottom: 25px;
                                        background-color: rgb(153,153,154); color: rgb(255,255,255); font-family: Gotham;
                                        border: 1px solid #ccc; font-size: 1.15vw; font-weight: 400; width: 100%" class="form-control input-lg"
                                        tabindex="1" placeholder="Enter Token" required data-parsley-type="email">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtotp"
                                        Display="None" ErrorMessage="token is required." SetFocusOnError="True" ValidationGroup="OG">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group" style="width: 100%; text-align: right; margin-top: 40px">
                                <asp:LinkButton ID="btnresendotp" OnClick="btnresendotp_Click" Style="text-decoration: underline;
                                    color: rgb(255,255,255); font-family: Gotham;" UseSubmitBehavior="false" runat="server"
                                    Text="Resend Token" />
                            </div>
                            <div class="form-group" style="width: 100%; text-align: right; margin-top: 40px">
                                <asp:Button ID="btnOTP" TabIndex="3" class="buttonLogin" Width="100%" OnClick="btnOTP_click"
                                    UseSubmitBehavior="false" runat="server" Text="SIGN IN" />
                                <%--ValidationGroup="OG"--%>
                                <asp:ValidationSummary ID="ValidationSummary6" ValidationGroup="OG" ShowMessageBox="true"
                                    runat="server" ShowSummary="false" />
                            </div>
                            <div class="form-group" style="width: 100%; text-align: center; margin-top: 30px"
                                id="alertOTp" runat="server" visible="false">
                                <asp:Label ID="lblOTPalert" Style="font-family: Gotham; color: rgb(255,255,255);
                                    font-weight: bold" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnotpmodalForget" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeotpforget" runat="server" TargetControlID="btnotpmodalForget"
        PopupControlID="pnlotpForget" Y="70" BackgroundCssClass="modalBackground" CancelControlID="imgcancelotpForget">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlotpForget" runat="server" Width="29%" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); align: right;">
        <table width="100%">
            <tr>
                <td width="100%">
                    <div>
                        <table width="100%">
                            <tr>
                                <td style="vertical-align: top; width: 97%; padding-left: 6%; text-align: left;">
                                    <span style="text-align: center; font-size: 20px; font-family: Gotham; color: rgb(255,255,255)">
                                        <br />
                                        JET EDGE ZOOM </span>
                                </td>
                                <td style="vertical-align: bottom; padding-right: 5px; width: 3%; padding-top: 20px;
                                    padding-right: 22px">
                                    <asp:LinkButton ID="imgcancelotpForget" runat="server" Style="color: rgb(255,255,255);
                                        text-decoration: none" CssClass="btncommonclose" Text="Close" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-body" style="border-bottom: none; padding-bottom: 10px">
                        <div id="Div4" runat="server" role="form">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon" style="background-color: rgb(153,153,154);">
                                        <i class="fa fa-key" aria-hidden="true"></i>
                                        <%--  <span class="glyphicon glyphicon-envelope"></span>--%>
                                    </div>
                                    <input name="email" id="txtotpForget" runat="server" style="padding-top: 25px; padding-bottom: 25px;
                                        background-color: rgb(153,153,154); color: rgb(255,255,255); font-family: Gotham;
                                        border: 1px solid #ccc; font-size: 1.15vw; font-weight: 400; width: 100%" class="form-control input-lg"
                                        tabindex="1" placeholder="Enter Token" required data-parsley-type="email">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtotpForget"
                                        Display="None" ErrorMessage="token is required." SetFocusOnError="True" ValidationGroup="OGF">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group" style="width: 100%; text-align: right; margin-top: 40px">
                                <asp:LinkButton ID="btnresendotpForget" OnClick="btnresendotpForget_Click" Style="text-decoration: underline;
                                    color: rgb(255,255,255); font-family: Gotham;" UseSubmitBehavior="false" runat="server"
                                    Text="Resend Token" />
                            </div>
                            <div class="form-group" style="width: 100%; text-align: right; margin-top: 40px">
                                <asp:Button ID="btnOTPForget" TabIndex="3" class="buttonLogin" Width="100%" OnClick="btnOTP_clickForget"
                                    UseSubmitBehavior="false" runat="server" Text="SIGN IN" />
                                <%--ValidationGroup="OG"--%>
                                <asp:ValidationSummary ID="ValidationSummary7" ValidationGroup="OGF" ShowMessageBox="true"
                                    runat="server" ShowSummary="false" />
                            </div>
                            <div class="form-group" style="width: 100%; text-align: center; margin-top: 30px"
                                id="alertOTpForget" runat="server" visible="false">
                                <asp:Label ID="lblOTPalertForget" Style="font-family: Gotham; color: rgb(255,255,255);
                                    font-weight: bold" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlalertprice" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalertprice" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lnkalertprice" runat="server" OnClick="lnkalertprice_Click" CssClass="btncommonclose"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalertBook" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealertBook" runat="server" TargetControlID="btnalertBook"
        PopupControlID="pnlalertBook" BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalertBook" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalertBook" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lnkalertBook" runat="server" OnClick="lnkalertBook_Click" CssClass="btncommonclose"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalertfortime" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealertfortime" runat="server" TargetControlID="btnalertfortime"
        PopupControlID="pnlalertfortime" BackgroundCssClass="modalBackground" Y="50">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalertfortime" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="lblalertfortime" Style="line-height: 20px; letter-spacing: 0.5px;"
                        runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton11" runat="server" CssClass="btncommonclose" OnClick="btnok_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnchangepassword" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpechange" runat="server" TargetControlID="btnchangepassword"
        PopupControlID="pnlchangepassword" BackgroundCssClass="modalBackground" Y="10">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlchangepassword" runat="server" Width="30%" ScrollBars="Auto" Style="display: none;
        color: rgb(255,255,255); background-color: rgb(17,24,32)">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px;
            padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr style="border-left: 0px">
                <td style="height: 10px; margin-right: 2%; padding-right: 3%; padding-left: 3%; padding-top: 3%;
                    color: White; font-weight: bold; border-color: none; font-size: larger; vertical-align: middle"
                    align="left">
                    Change Password
                </td>
                <td style="height: 30px; margin-right: 2%; padding-right: 3%; color: White; font-weight: bold;
                    border-color: none; font-size: larger; vertical-align: middle" align="right"
                    colspan="5">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 3%; padding-right: 3%">
                    <div class="cusfielduser-cuscontaineruser" style="text-align: left; padding-top: 20px;
                        display: none">
                        <label class="cuslabeluser " for="txtOldPassword" style="text-align: left; color: rgb(255,255,255)">
                            Old Password <span style="color: Red">*</span></label>
                        <input class="cusinputuserTextbox" style="width: 100%" runat="server" id="txtOldPassword"
                            maxlength="20" type="password">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtOldPassword"
                            Display="None" ErrorMessage="Old Password is required." SetFocusOnError="True"
                            ValidationGroup="TP">*</asp:RequiredFieldValidator>
                    </div>
                    <div class="cusfielduser-cuscontaineruser" style="text-align: left">
                        <label class="cuslabeluser" for="txtNewPassword" style="text-align: left; color: rgb(255,255,255)">
                            New Password <span style="color: Red">*</span></label>
                        <input class="cusinputuserTextbox" style="width: 100%" runat="server" tabindex="1"
                            id="txtNewPassword" maxlength="20" type="password" onfocus="return showlist()"
                            onblur="return passvalidate(this)" onkeypress="return show(this)" onkeyup="return show(this)">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtNewPassword"
                            Display="None" ErrorMessage="New Password is required." SetFocusOnError="True"
                            ValidationGroup="TP">*</asp:RequiredFieldValidator>
                        <asp:TextBox runat="server" ID="txtUserName" Style="display: none"></asp:TextBox>
                    </div>
                    <div id="divShow" runat="server" style="width: 240px; left: 65%; top: 26%; z-index: 9999999;
                        position: fixed; color: rgb(17,24,32); display: none;" class="hint">
                        <div class="hint-pointer">
                        </div>
                        <div>
                            <div>
                                <table>
                                    <tr>
                                        <td style="font-weight: bold; text-align: center">
                                            Password criteria:
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgyes1" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno1" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                        </td>
                                        <td>
                                            At least 8 characters long
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgyes2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno2" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                        </td>
                                        <td>
                                            Start with a letter
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgyes3" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno3" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                        </td>
                                        <td>
                                            Include a lower case letter
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgyes4" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno4" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                        </td>
                                        <td>
                                            Include an upper case letter
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgyes5" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno5" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                        </td>
                                        <td>
                                            Include a number
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imgyes7" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                            <asp:Image ID="imgno7" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                        </td>
                                        <td>
                                            No special characters
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="cusfielduser-cuscontaineruser" style="text-align: left">
                        <label class="cuslabeluser" for="txtConformPass" style="text-align: left; color: rgb(255,255,255)">
                            Confirm Password <span style="color: Red">*</span></label>
                        <input class="cusinputuserTextbox" runat="server" id="txtConformPass" tabindex="2"
                            style="width: 100%" maxlength="20" type="password" onfocus="return showlistpass()"
                            onblur="checkpassmatch(this)" onkeypress="validatepass(this)" onkeyup="validatepass(this)">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNewPassword"
                            Display="None" ErrorMessage="Confirm Password is required." SetFocusOnError="True"
                            ValidationGroup="TP">*</asp:RequiredFieldValidator>
                    </div>
                    <div id="divshowpassmatch" runat="server" style="width: 240px; color: rgb(17,24,32);
                        left: 65%; top: 36%; z-index: 9999999; position: fixed; display: none;" class="hint">
                        <div class="hint-pointer">
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <td style="font-weight: bold; text-align: center">
                                        Password criteria:
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Image ID="Imgpassyes" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                        <asp:Image ID="Imgpassno" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                    </td>
                                    <td>
                                        Password Matches
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding-top: 10px" colspan="5">
                    <asp:LinkButton ID="LinkButton4" runat="server" Style="width: 150px" OnClick="btnSubmit_Click"
                        Text="Submit" CssClass="btncommonjet" TabIndex="3" />
                    <asp:LinkButton ID="btnClose" runat="server" CssClass="btncommonjet" TabIndex="4"
                        Text="Close" OnClick="btnClose_Click" CausesValidation="false" />
                    <asp:ValidationSummary ID="ValidationSummary4" ValidationGroup="TP" ShowMessageBox="true"
                        runat="server" ShowSummary="false" />
                    <asp:Label ID="lblMessage" runat="server" Text="Last four passwords cannot be used for security reasons"
                        Visible="false" CssClass="mandatory"></asp:Label>
                    <input type='hidden' id='txthidden' runat="server" name='txthidden' />
                    <input type='hidden' id='txthidden1' runat="server" name='txthidden1' />
                    <asp:HiddenField ID="txtTelephony" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding-bottom: 10px; padding-top: 10px" colspan="5">
                    <div align="center" style="font-size: 15px">
                        <div id="div1" runat="server" class="content" visible="false">
                            <p>
                                Your password has been changed successfully. Please use your new password to <a href="simplesearchBooking.aspx"
                                    target="_parent" class="highlight">Login here</a>.</p>
                        </div>
                    </div>
                    <div align="center">
                        <div id="divEnd" runat="server" class="content" visible="false">
                            <p style="font-size: 18px">
                                Your password has been changed successfully. Please use your new password to <a href="simplesearchBooking.aspx"
                                    target="_parent" class="highlight" style="font-size: 22px">Login here</a>.</p>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnShow" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpetime" runat="server" TargetControlID="btnShow" PopupControlID="pnltime"
        BackgroundCssClass="modalBackground" Y="50">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnltime" runat="server" Width="50%" DefaultButton="btnsubmit" ScrollBars="Auto"
        Style="display: none; background-color: rgb(17,24,32); font-family: Gotham; font-weight: 400">
        <table style="width: 100%; height: 100%; padding-left: 0px; color: rgb(255,255,255);
            border-right: 0px; padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr style="border-left: 0px">
                <td style="height: 40px; margin-right: 2%; padding-right: 3%; color: White; font-weight: bold;
                    border-color: none; font-size: larger; vertical-align: bottom" align="right"
                    colspan="5">
                    <asp:LinkButton ID="btncancelpopup" OnClick="btncancelpopup_Click" runat="server"
                        Style="color: rgb(255,255,255); text-decoration: none" CssClass="btncommonclose"
                        Text="Close" CausesValidation="false" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr id="tdmodifyflagDate" runat="server" visible="false">
                <td style="text-align: left; padding-left: 3%; padding-bottom: 20px; font-size: 15px;
                    font-weight: bold; width: 55%" nowrap="nowrap">
                    Select the Date you want to fly
                    <br />
                    <asp:Label ID="Label27" runat="server" Style="font-size: 11px; padding-left: 0px;
                        padding-right: 0px; font-weight: normal; color: rgb(17,24,32); vertical-align: baseline;"
                        Text=""></asp:Label>
                    <asp:Label ID="Label28" runat="server" Style="font-size: 11px; padding-left: 0px;
                        display: none; font-weight: normal; vertical-align: baseline;" Text="(Please enter time in 24 hours format)"></asp:Label>
                </td>
                <td style="text-align: left; width: 25%; padding-left: 2%">
                    <asp:TextBox class="cusinputuserTextbox" runat="server" onkeypress="return NumericsIphon(event)"
                        Placeholder="Date(MM-DD-YYYY)" ID="txtDeptdate" TabIndex="2" MaxLength="20" />
                    <asp:CalendarExtender ID="CalenderDate" runat="server" TargetControlID="txtDeptdate"
                        Format="MM-dd-yyyy" Enabled="True">
                    </asp:CalendarExtender>
                </td>
            </tr>
            <tr id="tdtimeflag" runat="server">
                <td style="text-align: left; padding-left: 3%; padding-bottom: 20px; font-size: 15px;
                    font-weight: bold; width: 55%" nowrap="nowrap">
                    Enter time you want to fly on <span id="date" runat="server"></span>
                    <br />
                    <asp:Label ID="Label24" runat="server" Style="font-size: 11px; padding-left: 0px;
                        padding-right: 0px; font-weight: normal; color: rgb(17,24,32); vertical-align: baseline;"
                        Text=""></asp:Label>
                    <asp:Label ID="lbl" runat="server" Style="font-size: 11px; padding-left: 0px; display: none;
                        font-weight: normal; vertical-align: baseline;" Text="(Please enter time in 24 hours format)"></asp:Label>
                </td>
                <td style="text-align: left; width: 22%; padding-left: 2%">
                    <asp:DropDownList ID="ddlTimeFrom" class="cusinputuserTextbox" onfocusin="Open_Dropdownlist()"
                        onfocusout="Close_Dropdownlist()" runat="server">
                    </asp:DropDownList>
                    <input class="cusinputuser" style="display: none;" runat="server" id="txtStartTime"
                        maxlength="20" type="text" />
                    <input class="cusinputuserTextbox" id="txtStartTime1" value="08:30" step="1800" style="margin-top: 2px;
                        display: none; height: 37px;" tabindex="10" onblur="txtStartTimefun(this);" type="time" />
                    <asp:DropDownList ID="ddlHours" Width="70%" Style="display: none" runat="server"
                        class="cusinputuserTextbox">
                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: center; padding-left: 5px; padding-right: 5px; font-size: 15px;
                    width: 1%; font-weight: bold" nowrap="nowrap">
                </td>
                <td style="text-align: left; width: 22%;">
                    <asp:DropDownList ID="ddlMin" Width="70%" runat="server" Style="display: none" class="cusinputuserTextbox">
                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="text-align: left; padding-left: 20px; width: 0%; display: none">
                    <asp:DropDownList ID="ddltimetype" runat="server" Width="80%" Style="z-index: 9999999"
                        class="cusinputuser">
                        <asp:ListItem Value="AM" Selected="True" Text="AM"></asp:ListItem>
                        <asp:ListItem Value="PM" Text="PM"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trcityflag" visible="false" runat="server">
                <td style="text-align: left; padding-left: 3%; font-size: 15px; padding-bottom: 20px;
                    font-weight: bold; width: 55%" nowrap="nowrap">
                    Choose airport you want to fly to
                </td>
                <td style="text-align: left; width: 45%; padding-left: 2%" colspan="4">
                    <asp:TextBox ID="txtcountryModify" runat="server" class="cusinputuserTextbox" onkeyup="SetContextKey2()"
                        Width="92%" TabIndex="8" />
                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                        UseContextKey="true" CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                        CompletionSetCount="1" TargetControlID="txtcountryModify" ID="AutoCompleteExtender3"
                        runat="server" FirstRowSelected="false">
                    </asp:AutoCompleteExtender>
                </td>
            </tr>
            <tr id="trcity" visible="false" runat="server">
                <td style="text-align: left; padding-left: 3%; font-size: 15px; padding-bottom: 20px;
                    font-weight: bold; width: 55%" nowrap="nowrap">
                    Choose airport you want to fly to
                </td>
                <td style="text-align: left; width: 45%;" colspan="4">
                    <asp:DropDownList ID="ddlendcity" runat="server" Width="92%" Style="z-index: 999999"
                        class="cusinputuserTextbox">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trcountry" visible="false" runat="server">
                <td style="text-align: left; padding-left: 3%; font-size: 15px; padding-bottom: 20px;
                    font-weight: bold; width: 55%" nowrap="nowrap">
                    Choose airport you want to fly from
                </td>
                <td style="text-align: left; width: 45%;" colspan="4">
                    <asp:TextBox ID="ddlcountry" runat="server" class="cusinputuserTextbox" onkeyup="SetContextKey()"
                        Width="92%" TabIndex="8" />
                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                        UseContextKey="true" CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                        CompletionSetCount="1" TargetControlID="ddlcountry" ID="AutoCompleteExtender1"
                        runat="server" FirstRowSelected="false">
                    </asp:AutoCompleteExtender>
                    <asp:Label ID="lblcamp" runat="server" Visible="false"></asp:Label>
                    <div style="width: 100%; display: none">
                        <asp:PopupControlExtender ID="popupcountry" runat="server" BehaviorID="popupcountry"
                            Enabled="True" ExtenderControlID="" TargetControlID="ddlcountry" PopupControlID="pnlcountry"
                            OffsetY="40">
                        </asp:PopupControlExtender>
                        <asp:Panel ID="pnlcountry" runat="server" Style="background-color: whitesmoke; border-color: rgb(204, 204, 204);
                            border-width: 1px; border-style: solid; height: 60px; width: 42%; overflow: auto;
                            position: fixed; visibility: visible; display: none">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:CheckBoxList ID="cblcountry" runat="server" Style="color: black" DataTextField="ICAOCity"
                                            DataValueField="ICAOCity" TabIndex="6" onchange="GetSelectedClient('cblcountry','ddlcountry')"
                                            onclick="javascript:CheckCheck();">
                                        </asp:CheckBoxList>
                                    </td>
                                    <td align="right" valign="top">
                                        <asp:Image ID="Image3" runat="server" onclick="body.click();" ToolTip="Close" ImageUrl="~/Components/images/delete.gif" />
                                        <asp:Button ID="Button5" runat="server" Style="display: none;" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
            </tr>
            <tr id="trendcountry" visible="false" runat="server">
                <td style="text-align: left; padding-left: 3%; font-size: 15px; padding-bottom: 20px;
                    font-weight: bold; width: 55%" nowrap="nowrap">
                    Choose airport you want to fly to
                </td>
                <td style="text-align: left; width: 45%;" colspan="4">
                    <asp:TextBox ID="txtendcountry" runat="server" class="cusinputuserTextbox" onkeyup="SetContextKey2()"
                        Width="92%" TabIndex="8" />
                    <asp:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="1"
                        UseContextKey="true" CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                        CompletionListItemCssClass="listItem" CompletionInterval="10" EnableCaching="false"
                        CompletionSetCount="1" TargetControlID="txtendcountry" ID="AutoCompleteExtender2"
                        runat="server" FirstRowSelected="false">
                    </asp:AutoCompleteExtender>
                    <asp:Label ID="Label25" runat="server" Visible="false"></asp:Label>
                    <div style="width: 100%; display: none">
                        <asp:PopupControlExtender ID="popupendcountry" runat="server" BehaviorID="popupendcountry"
                            Enabled="True" ExtenderControlID="" TargetControlID="txtendcountry" PopupControlID="pnlendcountry"
                            OffsetY="40">
                        </asp:PopupControlExtender>
                        <asp:Panel ID="pnlendcountry" runat="server" Style="background-color: whitesmoke;
                            border-color: rgb(204, 204, 204); border-width: 1px; border-style: solid; height: 60px;
                            width: 42%; overflow: auto; position: fixed; visibility: visible; display: none">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:CheckBoxList ID="cblendcountry" runat="server" Style="color: black" DataTextField="ICAOCity"
                                            DataValueField="ICAOCity" TabIndex="6" onchange="GetSelectedClient('cblendcountry','txtendcountry')"
                                            onclick="javascript:CheckCheckend();">
                                        </asp:CheckBoxList>
                                    </td>
                                    <td align="right" valign="top">
                                        <asp:Image ID="Image1" runat="server" onclick="body.click();" ToolTip="Close" ImageUrl="~/Components/images/delete.gif" />
                                        <asp:Button ID="Button6" runat="server" Style="display: none;" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
            </tr>
            <tr style="padding-bottom: 10px;">
                <td style="text-align: center; padding-bottom: 0px; margin-bottom: 0px" colspan="5">
                    <asp:LinkButton ID="btnsubmit" runat="server" CssClass="btncommonjet" Text="Submit"
                        OnClick="Buttonsubmit_Click" UseSubmitBehavior="false" />
                    <%--ValidationGroup="VP"--%>
                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="VP" ShowMessageBox="true"
                        runat="server" ShowSummary="false" />
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding-top: 10px; padding-bottom: 15px; font-family: Gotham;
                    font-weight: 400; color: rgb(255,255,255); padding-left: 3%; padding-right: 3%"
                    colspan="5">
                    <asp:Label ID="lblAvail" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnsignup" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpesignup" runat="server" TargetControlID="btnsignup"
        PopupControlID="pnlsignup" BackgroundCssClass="modalBackground" Y="30">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlsignup" DefaultButton="Button2" runat="server" Width="53%" ScrollBars="Auto"
        Style="display: none; background-color: rgb(17,24,32)">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px;
            padding-right: 0px; padding: 10px" cellpadding="0" cellspacing="0">
            <tr style="background-color: rgb(17,24,32); border-left: 0px; height: 35px">
                <td style="vertical-align: middle; padding-top; 10px">
                    <h4 class="modal-title" style="float: left; padding-left: 6%; padding-top: 10px;
                        font-family: Gotham; font-weight: bold; color: White">
                        <span class="glyphicon glyphicon-lock" style="text-align: left"></span>&nbspSignup
                        Now!</h4>
                </td>
                <td style="height: 30px; margin-right: 2%; padding-right: 5%; color: White; font-weight: bold;
                    padding-top: 20px; border-color: none; font-size: larger; vertical-align: bottom"
                    align="right" colspan="5">
                    <asp:LinkButton ID="LinkButton2" runat="server" Style="color: rgb(255,255,255); text-decoration: none"
                        CssClass="btncommonclose" Text="Close" OnClick="ImageButton1_Click" CausesValidation="false" />
                    <%-- <asp:LinkButton ID="LinkButton1" OnClick="ImageButton1_Click" runat="server"> <img  src="assets/img/close.png" width="25px" height="25px" alt="bokknow"  /> </asp:LinkButton>--%>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="padding-left: 4%; padding-right: 6%; padding-bottom: 20px" colspan="2">
                    <div id="Signin" role="form" style="align: center">
                        <div class="form-group">
                            <div class="input-group">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 18%; padding-right: 2%" nowrap="nowrap">
                                            <span style="color: rgb(17,24,32)">*</span>
                                            <asp:DropDownList ID="ddlTitle" class="cusinputuserTextbox" Width="100%" runat="server">
                                                <asp:ListItem>Mr</asp:ListItem>
                                                <asp:ListItem>Mrs</asp:ListItem>
                                                <asp:ListItem>Miss</asp:ListItem>
                                                <asp:ListItem>Sir</asp:ListItem>
                                                <asp:ListItem>Dr</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 41%; padding-right: 2%" nowrap="nowrap">
                                            <span style="color: Red">*</span>
                                            <input class="cusinputuserTextbox" width="48%" runat="server" id="txtFirstName" type="text"
                                                maxlength="50" onkeypress="return Alphabets(event)" placeholder="First Name">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                                                Display="None" ErrorMessage="First Name is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td style="width: 41%" nowrap="nowrap">
                                            <span style="color: Red">*</span>
                                            <input class="cusinputuserTextbox" width="48%" runat="server" id="txtLastName" type="text"
                                                maxlength="50" onkeypress="return Alphabets(event)" placeholder="Last Name">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                                                Display="None" ErrorMessage="Last Name is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="width: 100%">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%; padding-left: 2%" nowrap="nowrap">
                                            <table width="100%">
                                                <tr>
                                                    <td width="20%">
                                                        <input class="cusinputuserTextbox" placeholder="country code" width="90%" runat="server"
                                                            id="txtPhoneCode" maxlength="4" tabindex="5" onkeypress="return Numericswithspecial(event);">
                                                    </td>
                                                    <td width="1%">
                                                    </td>
                                                    <td>
                                                        <input class="cusinputuserTextbox" width="98%" runat="server" id="txtphone" placeholder="Phone#"
                                                            maxlength="15" onkeypress="return Numerics(event)">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtphone"
                                                            Display="None" ErrorMessage="Phone # is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 50%;" nowrap="nowrap">
                                            <span style="color: Red">*</span>
                                            <%-- <asp:textbox id="txtemail" runat="server" class="cusinputuserTextbox" width="98%" autocomplete="off" autocompletetype="None" xmlns:asp="#unknown"> </asp:textbox>
                                            --%>
                                            <input class="cusinputuserTextbox" width="98%" runat="server" id="txtemail" placeholder="E-mail"
                                                maxlength="200" autocomplete="off" onchange="ValidateEmail(this)">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtemail"
                                                Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%; padding-right: 2%;" nowrap="nowrap">
                                            <span style="color: red">*</span>
                                            <input class="cusinputuserTextbox" runat="server" id="txtpassword1C" autocomplete="off"
                                                maxlength="20" type="password" placeholder="Password" onfocus="return showlist1()"
                                                onblur="return passvalidate1(this)" onkeypress="return show1(this)" onkeyup="return show1(this)">
                                            <%----%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtpassword1C"
                                                Display="None" ErrorMessage="New Password is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td style="width: 50%" nowrap="nowrap">
                                            <span style="color: red">*</span>
                                            <input class="cusinputuserTextbox" runat="server" id="txtpassword2C" maxlength="20"
                                                type="password" onfocus="return showlistpass1()" onblur="checkpassmatch1(this)"
                                                onkeypress="validatepass1(this)" onkeyup="validatepass1(this)" placeholder="Confirm Password">
                                            <%--onblur="return Validate()" --%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtpassword2C"
                                                Display="None" ErrorMessage="Confirm Password is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                            <%--    <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="txtpassword1C"
                                                        ControlToValidate="txtpassword2C" ErrorMessage="Your passwords do not match up!"
                                                        Display="Dynamic" />--%>
                                            <div id="divshowpassC" runat="server" style="width: 250px; left: 55%; top: 38%; z-index: 9999999;
                                                position: fixed; display: none;" class="hint">
                                                <div class="hint-pointer">
                                                </div>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td style="font-weight: bold; text-align: center">
                                                                Password criteria:
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="imgyespassC1" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                                <asp:Image ID="imgnopassC1" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                                            </td>
                                                            <td>
                                                                Password Matches
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="divshowuserC" runat="server" style="width: 240px; left: 35%; top: 38%; z-index: 9999999;
                                                position: fixed; display: none;" class="hint">
                                                <div class="hint-pointer">
                                                </div>
                                                <div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td style="font-weight: bold; text-align: center">
                                                                    Password criteria:
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Image ID="imgyes1C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                                    <asp:Image ID="imgno1C" runat="server" Style="display: block" ImageUrl="~/Components/images/tick_no.png" />
                                                                </td>
                                                                <td>
                                                                    At least 8 characters long
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Image ID="imgyes2C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                                    <asp:Image ID="imgno2C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                                                </td>
                                                                <td>
                                                                    Start with a letter
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Image ID="imgyes3C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                                    <asp:Image ID="imgno3C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                                                </td>
                                                                <td>
                                                                    Include a lower case letter
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Image ID="imgyes4C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                                    <asp:Image ID="imgno4C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                                                </td>
                                                                <td>
                                                                    Include an upper case letter
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Image ID="imgyes5C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                                    <asp:Image ID="imgno5C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                                                </td>
                                                                <td>
                                                                    Include a number
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Image ID="imgyes6C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_yes.png" />
                                                                    <asp:Image ID="imgno6C" runat="server" Style="display: none" ImageUrl="~/Components/images/tick_no.png" />
                                                                </td>
                                                                <td>
                                                                    No special characters
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="width: 100%">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 100%;" nowrap="nowrap">
                                            <span style="color: rgb(17,24,32)">*</span>
                                            <input class="cusinputuserTextbox" width="98%" runat="server" id="txtAddress" placeholder="Address1"
                                                maxlength="200">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="width: 100%">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 100%;" nowrap="nowrap">
                                            <span style="color: rgb(17,24,32)">*</span>
                                            <input class="cusinputuserTextbox" width="98" runat="server" id="txtAddress2" placeholder="Address2"
                                                maxlength="200">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%; padding-right: 2%;" nowrap="nowrap">
                                            <span style="color: rgb(17,24,32)">*</span>
                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtCity" placeholder="City"
                                                maxlength="200">
                                        </td>
                                        <td style="width: 50%" nowrap="nowrap">
                                            <span style="color: rgb(17,24,32)">*</span>
                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtstate" placeholder="State"
                                                maxlength="200">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%; padding-right: 2%;" nowrap="nowrap">
                                            <span style="color: rgb(17,24,32)">*</span>
                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtcountry" placeholder="Country"
                                                maxlength="200">
                                        </td>
                                        <td style="width: 50%" nowrap="nowrap">
                                            <span style="color: rgb(17,24,32)">*</span>
                                            <input class="cusinputuserTextbox" width="50%" runat="server" id="txtZipcode" placeholder="Zip Code"
                                                maxlength="6">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <div class="input-group">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 58%; padding-right: 2%; padding-left: 2%" nowrap="nowrap">
                                            <asp:CheckBox ID="chkEmpty" onclick="sameasdata(this)" Style="vertical-align: text-bottom"
                                                Checked="false" TabIndex="7" runat="server" />
                                            <label style="font-family: 'Roboto', sans-serif; font-size: 15px; vertical-align: middle"
                                                for="chkEmpty">
                                                I agree to
                                                <asp:LinkButton ID="lnk" runat="server" Style="color: blue; font-weight: bold" Text="privacy & security policy."
                                                    OnClick="OpenTermsandConditions2"></asp:LinkButton>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <center>
                            <asp:Button ID="Button2" class="Enablered" OnClick="btnCreateLoginOnClick" UseSubmitBehavior="false"
                                runat="server" Width="50%" Text="CREATE AN ACCOUNT" />
                            <%--ValidationGroup="SP"--%>
                            <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                runat="server" ShowSummary="false" />
                            <input type='hidden' id='hdnpassword1' runat="server" name='hdnpassword1' />
                            <input type='hidden' id='hdnpassword2' runat="server" name='hdnpassword2' />
                            <asp:HiddenField ID="hdnpassword3" runat="server" />
                        </center>
                        <%--    <button type="submit" class="btn btn-success btn-block btn-lg">
                            CREATE ACCOUNT!</button>--%>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnThank" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpethank" runat="server" TargetControlID="btnThank" PopupControlID="pnlthank"
        BackgroundCssClass="modalBackground" Y="10">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlthank" runat="server" Width="33%" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); font-family: Gotham; font-weight: 400">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px;
            padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr style="margin-left: 10%; border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-family: Gotham; font-weight: 400; font-size: 15px;
                    vertical-align: middle" align="center">
                    User details have been saved successfully.<br />
                    <br />
                    Token has been sent to your registered email address.
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lnkthank" runat="server" CssClass="btncommonjet" CausesValidation="false"
                        Text="Ok" OnClick="btnOkThank_onclick" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnTerms" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeterms" runat="server" TargetControlID="btnTerms" PopupControlID="pnlTerms"
        CancelControlID="imgCloseterms" Y="15" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlTerms" runat="server" Width="60%" ScrollBars="Auto" Style="display: none;
        background-color: #525659f5;">
        <table width="100%">
            <tr style="background-color: rgb(17,24,32); margin-left: 2%; margin-top: 5px; color: White">
                <td align="left" style="vertical-align: middle;">
                    <h4 style="margin-left: 2%; margin-top: 5px; color: White; font-size: 15px">
                    </h4>
                </td>
                <td align="right" style="vertical-align: middle; padding-right: 10px">
                    <asp:LinkButton ID="imgCloseterms" runat="server" CssClass="btncommonclose" Style="color: rgb(255,255,255);
                        text-decoration: none" Text="Close" CausesValidation="false" />
                </td>
            </tr>
            <tr style="margin-top: 50px">
                <td style="width: 100%; padding: 0px" colspan="2">
                    <center>
                        <%-- <iframe id="iframeterms" runat="server" frameborder="0" width="100%" style="background-color: white">
                        </iframe>--%>
                    </center>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnterms2" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeterms2" runat="server" TargetControlID="btnTerms2"
        PopupControlID="pnlTerms2" Y="15" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlTerms2" runat="server" Width="90%" ScrollBars="Auto" Style="display: none;
        background-color: #525659f5;">
        <table width="100%">
            <tr style="background-color: #000000; margin-left: 2%; margin-top: 5px; color: White">
                <td align="left" style="vertical-align: middle;">
                    <h4 style="margin-left: 2%; margin-top: 5px; color: White; font-size: 15px">
                        Terms and Conditions
                    </h4>
                </td>
                <td align="right" style="vertical-align: middle">
                    <asp:LinkButton ID="LinkButton5" OnClick="Closempepopup" runat="server"> <img width="25px" height="25px" src="assets/img/close.png" alt="bokknow"  /> </asp:LinkButton>
                </td>
            </tr>
            <tr style="margin-top: 50px">
                <td style="width: 100%; padding: 0px" colspan="2">
                    <center>
                        <%-- <iframe id="iframeterms1" runat="server" frameborder="0" width="100%" 
                                        style="background-color: white"></iframe>--%>
                        <asp:Literal ID="ltterms2" runat="server" />
                    </center>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalertprice" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealertprice" runat="server" TargetControlID="btnalertprice"
        PopupControlID="pnlalertprice" BackgroundCssClass="modalBackground" Y="80">
    </asp:ModalPopupExtender>
    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkalertprice_Click" CssClass="btncommonclose"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Button ID="btnThankForget" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpethankForget" runat="server" TargetControlID="btnThankForget"
        PopupControlID="pnlthankForget" BackgroundCssClass="modalBackground" Y="10">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlthankForget" runat="server" Width="33%" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); font-family: Gotham; font-weight: 400">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px;
            padding-right: 0px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr style="margin-left: 10%; border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-family: Gotham; font-weight: 400; font-size: 15px;
                    vertical-align: middle" align="center">
                    A Token has been sent to your registered email address to reset your password.
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="lnkthankForget" OnClick="btnOkThankForget_onclick" runat="server"
                        CssClass="btncommonjet" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btncommonclose" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lbltailNumber" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbldate" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblfromtime" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbltotoime" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblstartAiport" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblendAirport" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcount" runat="server" Text="0" Visible="false"></asp:Label>
    <asp:Label ID="lblHcity" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hdnendairport" runat="server" />
    <asp:HiddenField ID="hdnstartairport" runat="server" />
    <asp:HiddenField ID="hdnlength" runat="server" />
    <asp:Label ID="lblendfortime" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbltripnotime" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblTailflagtime" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblHrs" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblPricingFinal" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblworkhours" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblindex" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbllegCookie" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblsplitflag" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="loggedflag" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="BookedFlag" runat="server" Visible="false"></asp:Label>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3gFKhVywUkygSxQEBdGVrI5-ZRrdjueA"></script>
    <script id="INLINE_PEN_JS_ID" type="text/javascript">
        jQuery(document).ready(function ($) {
            var $form_modal = $('.cd-user-modal'),
  $form_login = $form_modal.find('#cd-login'),
  $form_signup = $form_modal.find('#cd-signup'),
  $form_forgot_password = $form_modal.find('#cd-reset-password'),
  $form_modal_tab = $('.cd-switcher'),
  $tab_login = $form_modal_tab.children('li').eq(0).children('a'),
  $tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
  $forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
  $back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
  $main_nav = $('.main-nav');

            //open modal
            $main_nav.on('click', function (event) {

                if ($(event.target).is($main_nav)) {
                    // on mobile open the submenu
                    $(this).children('ul').toggleClass('is-visible');
                } else {
                    // on mobile close submenu
                    $main_nav.children('ul').removeClass('is-visible');
                    //show modal layer
                    $form_modal.addClass('is-visible');
                    //show the selected form
                    $(event.target).is('.cd-signup') ? signup_selected() : login_selected();
                }

            });

            //close modal
            $('.cd-user-modal').on('click', function (event) {
                if ($(event.target).is($form_modal) || $(event.target).is('.cd-close-form')) {
                    $form_modal.removeClass('is-visible');
                }
            });
            //close modal when clicking the esc keyboard button
            $(document).keyup(function (event) {
                if (event.which == '27') {
                    $form_modal.removeClass('is-visible');
                }
            });

            //switch from a tab to another
            $form_modal_tab.on('click', function (event) {
                event.preventDefault();
                $(event.target).is($tab_login) ? login_selected() : signup_selected();
            });

            //hide or show password
            $('.hide-password').on('click', function () {
                var $this = $(this),
    $password_field = $this.prev('input');

                'password' == $password_field.attr('type') ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
                'Hide' == $this.text() ? $this.text('Show') : $this.text('Hide');
                //focus and move cursor to the end of input field
                $password_field.putCursorAtEnd();
            });

            //show forgot-password form 
            $forgot_password_link.on('click', function (event) {
                event.preventDefault();
                forgot_password_selected();
            });

            //back to login from the forgot-password form
            $back_to_login_link.on('click', function (event) {
                event.preventDefault();
                login_selected();
            });

            function login_selected() {
                $form_login.addClass('is-selected');
                $form_signup.removeClass('is-selected');
                $form_forgot_password.removeClass('is-selected');
                $tab_login.addClass('selected');
                $tab_signup.removeClass('selected');
            }

            function signup_selected() {
                $form_login.removeClass('is-selected');
                $form_signup.addClass('is-selected');
                $form_forgot_password.removeClass('is-selected');
                $tab_login.removeClass('selected');
                $tab_signup.addClass('selected');
            }

            function forgot_password_selected() {
                $form_login.removeClass('is-selected');
                $form_signup.removeClass('is-selected');
                $form_forgot_password.addClass('is-selected');
            }

            //REMOVE THIS - it's just to show error messages 
            $form_login.find('input[type="submit"]').on('click', function (event) {
                event.preventDefault();
                $form_login.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
            });
            $form_signup.find('input[type="submit"]').on('click', function (event) {
                event.preventDefault();
                $form_signup.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
            });


            //IE9 placeholder fallback
            //credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
            if (!Modernizr.input.placeholder) {
                $('[placeholder]').focus(function () {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                }).blur(function () {
                    var input = $(this);
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                }).blur();
                $('[placeholder]').parents('form').submit(function () {
                    $(this).find('[placeholder]').each(function () {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                            input.val('');
                        }
                    });
                });
            }

        });


        //credits https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
        jQuery.fn.putCursorAtEnd = function () {
            return this.each(function () {
                // If this function exists...
                if (this.setSelectionRange) {
                    // ... then use it (Doesn't work in IE)
                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
                    var len = $(this).val().length * 2;
                    this.setSelectionRange(len, len);
                } else {
                    // ... otherwise replace the contents with itself
                    // (Doesn't work in Google Chrome)
                    $(this).val($(this).val());
                }
            });
        };

        jQuery('#cody-info ul li').eq(1).on('click', function () {
            $('#cody-info').hide();
        });
        //# sourceURL=pen.js
    </script>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({
                html: true,
                content: function () {
                    return $('#Preferences').html();
                }
            });

            $('html').on('click', function (e) {
                $('[data-toggle="popover"]').each(function () {
                    //the 'is' for buttons that trigger popups
                    //the 'has' for icons within a button that triggers a popup
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });


            $(".dropdown").click(function (e) {
                e.preventDefault();
                var $this = $(this).children(".dropdown-content");
                $(".dropdown-content:visible").not($this).slideToggle(200); //Close submenu already opened
                $this.slideToggle(200); //Open the new submenu
            });
        });


        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({
                html: true,
                content: function () {
                    return $('#OrderHistory').html();
                }
            });

            $('html').on('click', function (e) {
                $('[data-toggle="popover"]').each(function () {
                    //the 'is' for buttons that trigger popups
                    //the 'has' for icons within a button that triggers a popup
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });


            $(".dropdown").click(function (e) {
                e.preventDefault();
                var $this = $(this).children(".dropdown-content");
                $(".dropdown-content:visible").not($this).slideToggle(200); //Close submenu already opened
                $this.slideToggle(200); //Open the new submenu
            });
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({
                html: true,
                content: function () {
                    return $('#profile').html();
                }
            });

            $('html').on('click', function (e) {
                $('[data-toggle="popover"]').each(function () {
                    //the 'is' for buttons that trigger popups
                    //the 'has' for icons within a button that triggers a popup
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });


            $(".dropdown").click(function (e) {
                e.preventDefault();
                var $this = $(this).children(".dropdown-content");
                $(".dropdown-content:visible").not($this).slideToggle(200); //Close submenu already opened
                $this.slideToggle(200); //Open the new submenu
            });
        });


    
      
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#Login-Form').parsley();
            $('#Signin-Form').parsley();
            $('#Forgot-Password-Form').parsley();

            $('#signupModal').click(function () {
                $('#login-modal-content').fadeOut('fast', function () {
                    $('#signup').fadeIn('fast');
                });
            });

            $('#loginModal').click(function () {
                $('#signup').fadeOut('fast', function () {
                    $('#login-modal-content').fadeIn('fast');
                });
            });

            $('#FPModal').click(function () {
                $('#login-modal-content').fadeOut('fast', function () {
                    $('#forgot-password-modal-content').fadeIn('fast');
                });
            });

            $('#loginModal1').click(function () {
                $('#forgot-password-modal-content').fadeOut('fast', function () {
                    $('#login-modal-content').fadeIn('fast');
                });
            });

        });

        function Open() {

            $('#login-signup-modal').modal('show');

            $('#signup').fadeOut('fast', function () {
                $('#forgot-password-modal-content').fadeOut('fast');
                $('#login-modal-content').fadeIn('fast');

            });

        }
        function OpenLogout() {

            $('#cd-signup').modal('show');
            $('##cd-signup').remove("modalBackground");


            $('#signup').fadeOut('fast', function () {
                $('#forgot-password-modal-content').fadeOut('fast');
                $('#login-modal-content').FadIn('fast');


            });

        }
        function OpenMytrip() {

            $('#cd-signup').modal('show');


        }
        function Close() {

            $('#login-signup-modal').modal('hide');
            $('#login-signup-modal').remove("modalBackground");



        }
        function Openforgot() {

            $('#login-signup-modal').modal('show');
            //            var string = 'We will send a token to your email to reset your password'
            //            document.getElementById("lblforgot").innerHTML = string;
            //            //            var forgot = document.getElementById('lblforgot');
            //            //            forgot.Value = string;
            //            document.getElementById('lblforgot').style.display = 'block';
            $('#login-modal-content').fadeIn('fast', function () {
                $('#signup').fadeOut('fast');
                $('#forgot-password-modal-content').fadeOut('fast');
            });


        }
        function OpenSignUp() {

            $('#login-signup-modal').modal('show');
            $('#login-modal-content').fadeIn('fast', function () {

                document.getElementById('divlog').style.display = 'None';
                $('#signup').fadeIn('fast');
                $('#login-modal-content').fadeIn('fast');

            });

        }

        function CloseSignup() {

            $('#login-signup-modal').modal('hide');
            $('#login-modal-content').fadeOut('fast', function () {

                document.getElementById('divlog').style.display = 'None';
                $('#signup').fadeOut('fast');
                $('#login-modal-content').fadeOut('fast');

            });

        }

        function ShowPopup() {

            $("[id*=mpeuser]").modal("show");
            document.getElementById('pnluer').style.display = "block";
            document.getElementById("topcontent").style.width = '1500px';
            document.getElementById("divfooyrt").style.width = '1500px';

        }
        function Hidepopup() {
            $("[id*=mpeuser]").modal("hide");
            document.getElementById('pnluer').style.display = "none";
            document.getElementById("topcontent").style.width = '1500px';
            document.getElementById("divfooyrt").style.width = '1500px';

        }

        $('document').ready(function (e) {
            $('.collapse').collapse({ toggle: false });
        });


       
    </script>
    </form>
</body>
</html>
