﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class Views_Exception : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.ExceptionBLL objExceptionBLL = new BusinessLayer.ExceptionBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here
            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Exception");
            }

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Exception";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
         
    }

    #endregion

    #region Button Events


    protected void gvpricing_price_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Image imgctrl = e.Row.FindControl("Image1") as Image;
                Label lblprice = e.Row.FindControl("lblprice") as Label;
                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;

                if (lblprice.Text == "1")
                {
                    //Get the Image here
                    imgctrl.ImageUrl = "~/Components/images/tick_yes.png";

                }
                else
                {
                    imgctrl.ImageUrl = "~/Components/images/tick_no.png";
                }

                if (lblcreatedon.Text.Length > 0)
                {


                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void gvPricingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvpricing.PageIndex = e.NewPageIndex;
            List();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }


    #endregion

    #region  User Defined Function

    public void List()
    {
        DataTable dt = objExceptionBLL.Select();
        if (dt.Rows.Count > 0)
        {
            gvpricing.DataSource = dt;
            gvpricing.DataBind();
        }
        else
        {
            gvpricing.DataSource = null;
            gvpricing.DataBind();
        }
    }



    #endregion
}