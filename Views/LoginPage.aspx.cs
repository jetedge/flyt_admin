﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Web;
using System.Globalization;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Net.Mime;
using BusinessLayer;

public partial class Views_LoginPage : System.Web.UI.Page
{
    HttpCookie EMPTYTRIPADMINTRANSID = new HttpCookie("EMPTYTRIPADMINTRANSID");

    string userInfo = string.Empty;
    string autoGen = string.Empty;
    string strUserID = string.Empty;
    string userRole = string.Empty;
    string ZendeskId = string.Empty;

    public int linenum = 0;
    public string MethodName = string.Empty;

    //Zendesk_Ticket objZendesk_Ticket = new Zendesk_Ticket();
    //LoginBAL objLoginBAL = new LoginBAL();
    CreateAdminBLL objCreateAdminBLL = new CreateAdminBLL();

    public enum UserInfo { Incorrect, Invalid, LoggedIn, Locked, LogOutProblem, InActive }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                DataTable dtGlobalConfig = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM GlobalConfig").Tables[0];
                if (dtGlobalConfig.Rows.Count > 0)
                {
                    DBMailFlag.Text = dtGlobalConfig.Rows[0]["MailFlag"].ToString();
                }

                txtLoginEmail.Attributes.Add("autocomplete", "off");
                txtPassword.Attributes.Add("autocomplete", "false");
                txtPhoneCode.Attributes.Add("autocomplete", "off");
                txtphone.Attributes.Add("autocomplete", "off");
                txtFirstName.Attributes.Add("autocomplete", "off");
                txtLastName.Attributes.Add("autocomplete", "off");
                txtemail.Attributes.Add("autocomplete", "off");
                txtpassword1C.Attributes.Add("autocomplete", "off");
                txtpassword2C.Attributes.Add("autocomplete", "off");

                if (Request.QueryString["FROM"] != null)
                {
                    if (Request.QueryString["FROM"].ToString().Length > 0)
                    {
                        lblFromFlag.Text = Request.QueryString["FROM"].ToString();

                        if (Request.QueryString["Flag"] != null)
                        {
                            if (Request.Cookies["JETRIPSADMIN"]["Username"] != null)
                            {
                                if (Request.Cookies["JETRIPSADMIN"]["Username"].ToString().ToLower() == "guest")
                                {
                                    btnCheckout.Attributes.Add("style", "opacity: 0.3; pointer-events: none;  cursor: not-allowed;");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }


        //HtmlControl iframeterms = (HtmlControl)Parent.FindControl("iframeterms");

        //if (iframeterms != null)
        //    iframeterms.Attributes.Add("style", "height: calc(40vh);border-radius: 5px;");
    }

    protected void lnkSignupClose_Click(object sender, EventArgs e)
    {
        try
        {
            divSIgnUp.Visible = false;
            divLogin.Visible = true;
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void imgcancelotpForget_Click(object sender, EventArgs e)
    {
        try
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "SetWidgetName_Login();", true);
            mpeOTPForgot.Show();
            divLogin.Visible = true;
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void logged_Click(object sender, EventArgs e)
    {
        LoginDirect();
    }

    protected void btncloseLogin(object sender, EventArgs e)
    {
        if (BookedFlag.Text == "B")
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Update Booking_Log set Bookflag='0'  where  tailno='" + Request.Cookies["JETRIPSPAY"]["tailno"].ToString() + "' and tripno='" + Request.Cookies["JETRIPSPAY"]["Tripno"].ToString() + "' and [Date]='" + Convert.ToDateTime(Request.Cookies["JETRIPSPAY"]["BDate"].ToString()) + "' and Starttime='" + Request.Cookies["JETRIPSPAY"]["starttime"].ToString() + "' and Endtime='" + Request.Cookies["JETRIPSPAY"]["Endtime"].ToString() + "' and [Start]='" + Request.Cookies["JETRIPSPAY"]["start"].ToString() + "' and [End]='" + Request.Cookies["JETRIPSPAY"]["End"].ToString() + "' and AlerId ='" + Request.Cookies["JETIDFORALERT"]["AlertId"].ToString() + "' ");
        }

        if (Request.Cookies["JETRIPSADMIN"] != null)
        {
            if (Request.Cookies["JETRIPSADMIN"]["Username"] != null)
            {
                if (Request.Cookies["JETRIPSADMIN"]["Username"].ToString().ToLower() == "guest")
                {
                    if (lblFromFlag.Text == "BH")
                    {
                        Response.Redirect("BookingHome.aspx?Flag=L&Flag=Available", false);
                    }
                    else if (lblFromFlag.Text == "DT")
                    {
                        Response.Redirect("DepartureTime.aspx?Flag=L&FMSC=LN", false);
                    }
                    else if (lblFromFlag.Text == "LN")
                    {
                        Response.Redirect("LandingPage.aspx?Flag=L&FROM=G", false);
                    }
                }
                else
                {
                    Response.Redirect("LandingPage.aspx", false);
                }
            }
            else
            {
                Response.Redirect("LandingPage.aspx", false);
            }
        }
        else
        {
            Response.Redirect("LandingPage.aspx", false);
        }
    }

    protected void CheckGuest(object sender, EventArgs e)
    {
        Response.Cookies.Clear();

        HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
        Response.Cookies.Remove("JETRIPSADMIN");
        Response.Cookies.Add(JETRIPSADMIN);
        JETRIPSADMIN.Values.Add("Username", "Guest");
        JETRIPSADMIN.Values.Add("brokerflag", "N");

        Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);
        loggedflag.Text = "L";

        if (BookedFlag.Text.Length > 0)
        {

        }
        else
        {
            if (lblFromFlag.Text == "BH")
            {
                Response.Redirect("BookingHome.aspx?Flag=L&Flag=Available", false);
            }
            else if (lblFromFlag.Text == "DT" || lblFromFlag.Text == "BK")
            {
                Response.Redirect("DepartureTime.aspx?Flag=L&FMSC=LN", false);
            }
            else if (lblFromFlag.Text == "LN")
            {
                Response.Redirect("LandingPage.aspx?Flag=L&FROM=G", false);
            }
        }
    }

    protected void ShowCreateUserPopup(object sender, EventArgs e)
    {
        divLogin.Visible = false;
        divSIgnUp.Visible = true;
    }

    #region Cookies
    protected void ImageButton2_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "SetWidgetName_Forgot();", true);

        tblcookie.Visible = true;
        mpefrmaecookie.Visible = false;
    }
    protected void lnkcookesshow_click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "SetWidgetName_Forgot();", true);

        tblcookie.Visible = false;
        mpefrmaecookie.Visible = true;
    }
    protected void lnkAllow_Onclick(object sender, EventArgs e)
    {
        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Usecookies='1' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");
        Response.Redirect("LandingPage.aspx?Flag=L", false);
    }
    #endregion

    protected void btnOTP_click(object sender, EventArgs e)
    {
        string strBusinessAlert = string.Empty;
        if (txtotp.Value.ToString().Length == 0)
        {
            strBusinessAlert += "- Token is required. <br/>";
        }

        if (strBusinessAlert.Length > 0)
        {
            lblalert.Text = strBusinessAlert;
            mpealert.Show();
            return;
        }

        if (txtotp.Value.Length > 0)
        {
            DataTable dtuser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];
            if (dtuser.Rows.Count > 0)
            {
                string OTP = dtuser.Rows[0]["OTP"].ToString();
                DateTime date1 = Convert.ToDateTime(dtuser.Rows[0]["OTPTime"].ToString());
                DateTime date2 = DateTime.Now;
                TimeSpan ts = date2 - date1;
                if (txtotp.Value == OTP)
                {
                    if (Convert.ToInt32(ts.TotalMinutes) > 30)
                    {
                        lblOTPalert.Visible = true;
                        lblOTPalert.Text = "Your token has expired. Please click Resend Token to get new one";
                        alertOTp.Visible = true;
                    }
                    else
                    {
                        string Query = "Update UserMasterCreate Set OTPflag='1'  where  Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'";
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, Query);

                        mpecookie.Show();
                        mpeToken.Hide();

                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "SetWidgetName_Forgot();", true);
                    }
                }
                else
                {
                    lblalert.Text = "Token is invalid – please try again.";
                    txtotp.Value = string.Empty;

                    mpealert.Show();
                }
            }
        }
    }

    protected void lnkReject_Onclick(object sender, EventArgs e)
    {
        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Usecookies='0' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");
        Response.Redirect("LandingPage.aspx?Flag=L", false);
    }

    void LoginDirect()
    {
        try
        {
            string strBusinessAlert = string.Empty;
            string URL = string.Empty;

            if (txtLoginEmail.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- E-mail <br/>";
            }
            if (txtPassword.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- Password <br/>";
            }

            if (strBusinessAlert.Length > 0)
            {
                lblalert.Text = "- The below required items are missing:<br/><br/>" + strBusinessAlert;
                mpealert.Show();
                return;
            }
            userInfo = AuthenticateUser(txtLoginEmail.Value, txtPassword.Value);
            if (userInfo == UserInfo.Incorrect.ToString()) // Login failed...
            {
                lblalert.Text = "Incorrect Password";
                mpealert.Show();

                return;
            }
            else if (userInfo == UserInfo.Invalid.ToString()) // Login failed...
            {
                lblalert.Text = "Invalid username or password.Please try again.";
                mpealert.Show();

                return;
            }
            else if (userInfo == "ResetOTP")
            {
                mpealert.Show();
                lblalert.Text = "Entered token has been expired.Please click 'Forgot Password?' to get new token";

                return;
            }
            else // Login success...
            {
                string strval = string.Empty;

                DataTable dt = objCreateAdminBLL.Select_User_Login(txtLoginEmail.Value.Trim(), "SEL");

                HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
                Response.Cookies.Remove("JETRIPSADMIN");
                Response.Cookies.Add(JETRIPSADMIN);
                JETRIPSADMIN.Values.Add("Username", dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
                JETRIPSADMIN.Values.Add("FirstName", dt.Rows[0]["FirstName"].ToString());
                JETRIPSADMIN.Values.Add("LastName", dt.Rows[0]["LastName"].ToString());
                JETRIPSADMIN.Values.Add("Title", dt.Rows[0]["Title"].ToString());
                JETRIPSADMIN.Values.Add("Email", dt.Rows[0]["Email"].ToString());
                JETRIPSADMIN.Values.Add("PhoneNumber", dt.Rows[0]["PhoneNumber"].ToString());
                JETRIPSADMIN.Values.Add("Phonecode", dt.Rows[0]["Code_Phone"].ToString());
                JETRIPSADMIN.Values.Add("Address", dt.Rows[0]["Address"].ToString());
                JETRIPSADMIN.Values.Add("Country", dt.Rows[0]["Country"].ToString());
                JETRIPSADMIN.Values.Add("City", dt.Rows[0]["City"].ToString());
                JETRIPSADMIN.Values.Add("ZipCode", dt.Rows[0]["ZipCode"].ToString());
                JETRIPSADMIN.Values.Add("Role", dt.Rows[0]["Role"].ToString());
                JETRIPSADMIN.Values.Add("UserId", dt.Rows[0]["RowId"].ToString());
                JETRIPSADMIN.Values.Add("brokerflag", dt.Rows[0]["UserbrokerfLAG"].ToString());

                Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);

                NameValueCollection ipAddress = Request.ServerVariables;
                EMPTYTRIPADMINTRANSID.Values.Add("TrasId", UserLoggedIn(dt.Rows[0]["RowId"].ToString(), ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value));
                Response.Cookies.Add(EMPTYTRIPADMINTRANSID);

                Response.Redirect("pricecharter.aspx", false);
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public string AuthenticateUser(string userName, string password)
    {
        string retMessage = string.Empty;
        DataTable dtUser = objCreateAdminBLL.Select_User_Login(userName, "SEL");
        if (dtUser.Rows.Count > 0)
        {
            string pass1 = genclass.Decrypt(dtUser.Rows[0]["Password"].ToString());

            if (pass1 == password)
            {
                if (dtUser.Rows[0]["Active"].ToString() != "1")
                {
                    retMessage = UserInfo.InActive.ToString();
                }
                else if (dtUser.Rows[0]["MustChangePassword"].ToString() == "1")
                {
                    if (dtUser.Rows[0]["role"].ToString().ToLower() != "admin")
                    {
                        if (dtUser.Rows[0]["userflag"].ToString().ToUpper() != "ADMINUSER")
                        {
                            DateTime date1 = Convert.ToDateTime(dtUser.Rows[0]["ForgotPassTime"].ToString());
                            DateTime date2 = DateTime.Now;
                            TimeSpan ts = date2 - date1;

                            if (Convert.ToInt32(ts.TotalMinutes) > 30)
                            {
                                retMessage = "ResetOTP";
                            }
                            else
                            {
                                retMessage = string.Empty;
                            }
                        }
                        else
                        {
                            if (dtUser.Rows[0]["ForgotPassTime"].ToString().Length > 0)
                            {
                                DateTime date1 = Convert.ToDateTime(dtUser.Rows[0]["ForgotPassTime"].ToString());
                                DateTime date2 = DateTime.Now;
                                TimeSpan ts = date2 - date1;

                                if (Convert.ToInt32(ts.TotalMinutes) > 30)
                                {
                                    retMessage = "ResetOTP";
                                }
                                else
                                {
                                    retMessage = string.Empty;
                                }

                            }
                            else
                            {
                                retMessage = "Changepassword";
                            }
                        }

                    }
                    else
                    {
                        retMessage = string.Empty;

                    }
                }
                else
                {
                    retMessage = string.Empty;
                }
            }
            else
            {
                retMessage = UserInfo.Incorrect.ToString();
            }
        }
        else
        {
            retMessage = UserInfo.Invalid.ToString();
        }

        return retMessage;
    }

    public static string UserLoggedIn(string userName, string ipAddress, string BName, string BVersion, string userInfo, string PBXType)
    {
        string TRansID = string.Empty;

        string CurrentDate = string.Empty;
        CurrentDate = System.DateTime.Now.ToString();

        string Query = "Update UserMasterCreate Set LastLoginIP='" + ipAddress + "'  where  Rowid='" + userName + "'";
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, Query);


        string strQuery = "insert into Logaudit(Userid,loggedin,loggedout,IPAddress,LoginDate,[Loggedout(sec)],logoutReason,[browserName],[browserVersion]) values('" + userName + "', GETDATE(), NULL, '" + ipAddress + "','" + DateTime.Now.Date + "','0',NULL, '" + BName + "', '" + BVersion + "'); SELECT SCOPE_IDENTITY() ;";
        TRansID = Convert.ToString(SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, strQuery));



        DataTable dtInfo = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select top 1 rowid,loggedout from Logaudit where Userid='" + userName + "' " +
                           " and CONVERT(char,loggedin,101)=CONVERT(char,GETDATE(),101) and isnull(loggedout,'')<>'' and rowid <'" + TRansID + "' order by loggedin desc ").Tables[0];
        if (dtInfo.Rows.Count > 0)
        {
            if (dtInfo.Rows[0]["rowid"].ToString() != TRansID)
            {
                strQuery = "Update Logaudit Set [Loggedout(sec)]= DATEDIFF(ss,'" + dtInfo.Rows[0]["loggedout"].ToString() + "','" + CurrentDate + "') where  Logaudit.Userid='" + userName + "' and Logaudit.rowid='" + dtInfo.Rows[0]["rowid"].ToString() + "'";
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, strQuery);
            }
        }
        return TRansID;
    }

    protected void btnforgetPassOnClick(object sender, EventArgs e)
    {
        try
        {
            string autoGenPwd = string.Empty;
            autoGenPwd = genclass.RandomStringNumbers(6);
            DataTable dt1 = new DataTable();
            string strName = string.Empty;
            string strResult = string.Empty;

            //if (txtLoginEmail.Value.Length > 0)
            //{
            //    dt1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtLoginEmail.Value + "'").Tables[0];
            //    if (dt1.Rows.Count > 0)
            //    {
            //        strName = dt1.Rows[0]["firstName"].ToString() + " " + dt1.Rows[0]["LastName"].ToString();
            //        strResult = objLoginBAL.SendEmailForget(txtLoginEmail.Value, autoGenPwd, strName);

            //        if (strResult.ToLower().Trim() != "yes")
            //        {
            //            lblalert.Text = "Email sending failed.";
            //            mpealert.Show();
            //            return;
            //        }
            //        else
            //        {
            //            lblalert.Text = "We will send a token to your email to reset your password";
            //        }

            //        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Password ='" + genclass.Encrypt(autoGenPwd) + "',MustChangePassword='1',ForgotPassTime='" + DateTime.Now + "' Where Email='" + txtLoginEmail.Value + "'");

            //        HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
            //        Response.Cookies.Remove("JETRIPSADMIN");
            //        Response.Cookies.Add(JETRIPSADMIN);
            //        JETRIPSADMIN.Values.Add("Username", dt1.Rows[0]["FirstName"].ToString() + " " + dt1.Rows[0]["LastName"].ToString());
            //        JETRIPSADMIN.Values.Add("FirstName", dt1.Rows[0]["FirstName"].ToString());
            //        JETRIPSADMIN.Values.Add("LastName", dt1.Rows[0]["LastName"].ToString());
            //        JETRIPSADMIN.Values.Add("Title", dt1.Rows[0]["Title"].ToString());
            //        JETRIPSADMIN.Values.Add("Email", dt1.Rows[0]["Email"].ToString());
            //        JETRIPSADMIN.Values.Add("PhoneNumber", dt1.Rows[0]["PhoneNumber"].ToString());
            //        JETRIPSADMIN.Values.Add("Address", dt1.Rows[0]["Address"].ToString());
            //        JETRIPSADMIN.Values.Add("Country", dt1.Rows[0]["Country"].ToString());
            //        JETRIPSADMIN.Values.Add("City", dt1.Rows[0]["City"].ToString());
            //        JETRIPSADMIN.Values.Add("ZipCode", dt1.Rows[0]["ZipCode"].ToString());
            //        JETRIPSADMIN.Values.Add("Role", dt1.Rows[0]["Role"].ToString());
            //        JETRIPSADMIN.Values.Add("UserId", dt1.Rows[0]["RowId"].ToString());

            //        Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);

            //        mpeThankForgot.Show();

            //    }
            //    else
            //    {
            //        mpealert.Show();
            //        lblalert.Text = "Not a registered user!";
            //    }
            //}
            //else
            //{
            //    lblalert.Text = "- The below required items are missing:<br/><br/>" + "- Email";
            //    mpealert.Show();
            //}
        }
        catch (Exception ex)
        {
            lblalert.Text = "Email sending failed.";
            mpealert.Show();
        }
    }

    protected void btnOkThankForget_onclick(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "SetWidgetName_ThankForget();", true);

        if (loggedflag.Text == "L" && BookedFlag.Text == "B")
        {
            mpeThankForgot.Hide();
            mpeOTPForgot.Show();
        }
        else
        {
            mpeThankForgot.Hide();
            mpeOTPForgot.Show();
        }
    }

    protected void btnresendotpForget_Click(object sender, EventArgs e)
    {
        try
        {
            //string autoGenPwd = string.Empty;
            //autoGenPwd = genclass.RandomStringNumbers(6);
            //DataTable dt = new DataTable();
            //string strName = string.Empty;
            //string strResult = string.Empty;

            //if (txtLoginEmail.Value.Length > 0)
            //{
            //    dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtLoginEmail.Value + "'").Tables[0];
            //    if (dt.Rows.Count > 0)
            //    {
            //        strName = dt.Rows[0]["firstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
            //        strResult = objLoginBAL.SendEmailForget(txtLoginEmail.Value, autoGenPwd, strName);

            //        if (strResult.ToLower().Trim() != "yes")
            //        {
            //            lblalert.Text = "Email sending failed.";
            //            mpealert.Show();
            //            return;
            //        }
            //        else
            //        {
            //            SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Password ='" + genclass.Encrypt(autoGenPwd) + "',MustChangePassword='1',ForgotPassTime='" + DateTime.Now + "' Where Email='" + txtLoginEmail.Value + "'");
            //            lblalert.Text = "Token has been resent to your registered email address.";
            //            mpealert.Show();
            //        }
            //    }
            //    else
            //    {
            //        lblalert.Text = "Not a registered user!";
            //        mpealert.Show();
            //    }
            //}
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnTokClose(object sender, EventArgs e)
    {
        divLogin.Visible = true;
        mpeToken.Hide();
    }

    protected void btnresendotp_Click(object sender, EventArgs e)
    {

        //string autoGenPwd = string.Empty;
        //autoGenPwd = genclass.RandomStringNumbers(6);

        //DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];

        //if (dtUser.Rows.Count > 0)
        //{
        //    string username = dtUser.Rows[0]["firstname"].ToString() + " " + dtUser.Rows[0]["lastname"].ToString();
        //    string email = dtUser.Rows[0]["email"].ToString();
        //    string Query = "Update UserMasterCreate Set OTP='" + autoGenPwd + "',OTPflag='0',OTPTime='" + DateTime.Now + "'  where  email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'";
        //    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, Query);

        //    objLoginBAL.SendEmail_logincreate(email, username, autoGenPwd);
        //    //mpeotp.Show();
        //    lblOTPalert.Text = "Token has been resent to your registered email address.";
        //    alertOTp.Visible = true;
        //}
    }

    protected void btnCreateLoginOnClick(object sender, EventArgs e)
    {
        string[] CategoryDetails = new string[40];
        string strMsg = string.Empty;
        string strActive = string.Empty;
        string strBusinessAlert = string.Empty;
        string autoGenPwd = string.Empty;
        string strResult = string.Empty;

        autoGenPwd = genclass.RandomStringNumbers(6);

        try
        {
            if (txtFirstName.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>- First Name <br/>";
            }
            if (txtLastName.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>- Last Name <br/>";
            }
            if (txtemail.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>- Email <br/>";
            }
            if (txtpassword1C.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>-  Password  <br/>";
            }
            if (txtpassword2C.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>-  Confirm Password <br/>";
            }
            if (strBusinessAlert.Length > 0)
            {
                lblalert.Text = "- The below required items are missing:<br/><br/>" + strBusinessAlert;
                mpealert.Show();
                return;
            }

            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtemail.Value + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                lblalert.Text = txtemail.Value + " is already exist";
                mpealert.Show();
                return;
            }

            CategoryDetails[0] = "I";
            CategoryDetails[1] = "0";
            CategoryDetails[2] = "";
            CategoryDetails[3] = txtemail.Value;
            CategoryDetails[4] = txtphone.Value;
            CategoryDetails[5] = txtAddress.Value;
            CategoryDetails[6] = txtCity.Value;
            CategoryDetails[7] = txtZipcode.Value;
            CategoryDetails[8] = "User";
            CategoryDetails[9] = (genclass.Encrypt(txtpassword1C.Value));
            CategoryDetails[10] = txtFirstName.Value;
            CategoryDetails[11] = txtLastName.Value;
            CategoryDetails[12] = txtAddress2.Value;
            CategoryDetails[13] = txtstate.Value;
            CategoryDetails[14] = txtcountry.Value;
            CategoryDetails[15] = autoGenPwd;
            CategoryDetails[16] = "0";
            CategoryDetails[17] = txtPhoneCode.Value;

            string strReturnVal = saveCategory(CategoryDetails);
            DataTable dtCD = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  CouponDiscount ").Tables[0];
            if (dtCD.Rows.Count > 0)
            {
                for (int i = 0; i < dtCD.Rows.Count; i++)
                {
                    if (dtCD.Rows[i]["UserFlag"].ToString() == "A")
                    {
                        string couponcode = dtCD.Rows[i]["CouponCode"].ToString();
                        string users = dtCD.Rows[i]["CouponDescription"].ToString();
                        if (users.Length > 0)
                        {
                            users = users + "," + strReturnVal;
                        }
                        else
                        {
                            users = strReturnVal;
                        }
                        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update CouponDiscount set CouponDescription ='" + users + "' Where CouponCode='" + couponcode + "'");
                    }
                }
            }

            //strResult = objLoginBAL.SendEmail_logincreate(txtemail.Value, txtFirstName.Value + " " + txtLastName.Value, autoGenPwd);

            if (strResult.ToLower().Trim() != "yes")
            {
                lblalert.Text = "Email sending failed.";
                mpealert.Show();
                return;
            }
            else
            {
                HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
                Response.Cookies.Remove("JETRIPSADMIN");
                Response.Cookies.Add(JETRIPSADMIN);

                JETRIPSADMIN.Values.Add("Username", txtFirstName.Value + " " + txtLastName.Value);
                JETRIPSADMIN.Values.Add("FirstName", txtFirstName.Value);
                JETRIPSADMIN.Values.Add("LastName", txtLastName.Value);
                JETRIPSADMIN.Values.Add("Email", txtemail.Value);
                JETRIPSADMIN.Values.Add("PhoneNumber", txtphone.Value);
                JETRIPSADMIN.Values.Add("Phonecode", txtPhoneCode.Value);
                JETRIPSADMIN.Values.Add("Address", txtAddress.Value);
                JETRIPSADMIN.Values.Add("Address2", txtAddress2.Value);
                JETRIPSADMIN.Values.Add("City", txtCity.Value);
                JETRIPSADMIN.Values.Add("ZipCode", txtZipcode.Value);
                JETRIPSADMIN.Values.Add("Role", "User");
                JETRIPSADMIN.Values.Add("UserId", strReturnVal);
                JETRIPSADMIN.Values.Add("Brokerflag", "N");

                Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);
                NameValueCollection ipAddress = Request.ServerVariables;
                EMPTYTRIPADMINTRANSID.Values.Add("TrasId", UserLoggedIn(strReturnVal, ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value));
                Response.Cookies.Add(EMPTYTRIPADMINTRANSID);

                loggedflag.Text = "L";
                if (strReturnVal != "0")
                {
                    mpethank.Show();
                    divSIgnUp.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public string saveCategory(string[] arrCategory)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_UserMaster_INS]", false);
        // DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  OneWayTail_Setup.rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours, EndAirport, EndCity  FROM  OneWayTail_Setup Inner join TailPricing_Configuration on TailPricing_Configuration.TailNo=OneWayTail_Setup.TailNo ").Tables[0];

        saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
        saveCategoryParam[1].Value = arrCategory[1];//Rowid
        saveCategoryParam[2].Value = arrCategory[2];//Username
        saveCategoryParam[3].Value = arrCategory[10];//Role
        saveCategoryParam[4].Value = arrCategory[11];//Role
        saveCategoryParam[6].Value = arrCategory[3];//Email
        saveCategoryParam[7].Value = arrCategory[4];//Phone Number
        saveCategoryParam[8].Value = arrCategory[5];//Address
        saveCategoryParam[9].Value = arrCategory[14];//Address2
        saveCategoryParam[10].Value = arrCategory[6];//Country
        saveCategoryParam[11].Value = arrCategory[7];//Zipcode
        saveCategoryParam[12].Value = arrCategory[8];//Role
        saveCategoryParam[13].Value = arrCategory[9];//Role

        saveCategoryParam[15].Value = arrCategory[13];//Address2
        saveCategoryParam[16].Value = "0";//Role
        saveCategoryParam[17].Value = arrCategory[12];//Address2

        saveCategoryParam[18].Value = "0";//Address2
        saveCategoryParam[19].Value = "1";//active
        saveCategoryParam[20].Value = arrCategory[15];//Address2
        saveCategoryParam[21].Value = arrCategory[16];//Address2
        saveCategoryParam[23].Value = arrCategory[17];//Address2



        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_UserMaster_INS]", saveCategoryParam).ToString();
        return obj.ToString();
    }

    protected void btnOkThank_onclick(object sender, EventArgs e)
    {
        try
        {
            mpeToken.Show();
            mpethank.Hide();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "SetWidgetName_Login();", true);
    }

    protected void btnOTP_clickForget(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "SetWidgetName_ChangePassword();", true);
        try
        {
            string strBusinessAlert = string.Empty;
            if (txtotpForget.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- Token is required. <br/>";
            }

            if (strBusinessAlert.Length > 0)
            {
                lblalert.Text = strBusinessAlert;
                mpealert.Show();
                mpeOTPForgot.Show();
                return;
            }
            userInfo = AuthenticateUser(txtLoginEmail.Value, txtotpForget.Value);
            if (userInfo == UserInfo.Incorrect.ToString()) // Login failed...
            {
                lblalert.Text = "Token is invalid – please try again.";
                mpealert.Show();
                mpeOTPForgot.Show();
                return;
            }
            else if (userInfo == UserInfo.InActive.ToString()) // Login failed...
            {
                lblalert.Text = "User is Inactive. Please contact Zoom Support Team.";
                mpealert.Show();
                return;
            }
            else if (userInfo == UserInfo.Invalid.ToString()) // Login failed...
            {
                lblalert.Text = "Invalid username or password";
                mpealert.Show();
                return;
            }
            else if (userInfo == "ResetOTP")
            {
                lblalert.Text = "Entered token has been expired.Please click 'Forgot Password?' to get new token";
                mpealert.Show();
                return;
            }

            else // Login success...
            {
                DataTable dtUserRole = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtLoginEmail.Value + "'").Tables[0];
                if (dtUserRole.Rows.Count > 0)
                {
                    if (dtUserRole.Rows[0]["MustChangePassword"].ToString() == "1")
                    {
                        mpeOTPForgot.Hide();
                        mpeChangePassword.Show();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        try
        {
            string strBusinessAlert = string.Empty;

            if (txtNewPassword.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- New Password <br/>";
            }
            if (txtConformPass.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- Confirm Password <br/>";
            }

            if (strBusinessAlert.Length > 0)
            {
                lblalert.Text = "The below required items are missing:<br><br>" + strBusinessAlert;
                mpealert.Show();

                return;
            }

            string result = string.Empty;
            string result1 = string.Empty;

            SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Password ='" + genclass.Encrypt(txtNewPassword.Value.Trim()) + "',MustChangePassword='0' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");

            string strErr = "";
            string strval = string.Empty;

            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *, isnull(BrokerFlag,'N') as UserbrokerfLAG from UserMasterCreate where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];
            HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
            Response.Cookies.Remove("JETRIPSADMIN");
            Response.Cookies.Add(JETRIPSADMIN);
            JETRIPSADMIN.Values.Add("Username", dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
            JETRIPSADMIN.Values.Add("FirstName", dt.Rows[0]["FirstName"].ToString());
            JETRIPSADMIN.Values.Add("LastName", dt.Rows[0]["LastName"].ToString());
            JETRIPSADMIN.Values.Add("Title", dt.Rows[0]["Title"].ToString());
            JETRIPSADMIN.Values.Add("Email", dt.Rows[0]["Email"].ToString());
            JETRIPSADMIN.Values.Add("PhoneNumber", dt.Rows[0]["PhoneNumber"].ToString());
            JETRIPSADMIN.Values.Add("Phonecode", dt.Rows[0]["Code_Phone"].ToString());
            JETRIPSADMIN.Values.Add("Address", dt.Rows[0]["Address"].ToString());
            JETRIPSADMIN.Values.Add("Country", dt.Rows[0]["Country"].ToString());
            JETRIPSADMIN.Values.Add("City", dt.Rows[0]["City"].ToString());
            JETRIPSADMIN.Values.Add("ZipCode", dt.Rows[0]["ZipCode"].ToString());
            JETRIPSADMIN.Values.Add("Role", dt.Rows[0]["Role"].ToString());
            JETRIPSADMIN.Values.Add("UserId", dt.Rows[0]["RowId"].ToString());
            JETRIPSADMIN.Values.Add("Brokerflag", dt.Rows[0]["UserbrokerfLAG"].ToString());

            Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);

            if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().Length > 0)
            {
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString() != "guest@demo.com")
                {
                    DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];

                    if (dtUser.Rows[0]["MustChangePassword"].ToString() != "1")
                    {
                        if (dtUser.Rows.Count > 0)
                        {
                            if (dtUser.Rows[0]["role"].ToString().ToLower() == "admin")
                            {
                                string URL = SecretsBLL.AdminHomePage.ToString() + "?UName=" + dt.Rows[0]["Email"].ToString();
                                Response.Redirect(URL, false);
                            }
                            else
                            {
                                loggedflag.Text = "L";
                                if (loggedflag.Text == "L" && BookedFlag.Text == "B")
                                {

                                }
                                else
                                {
                                    if (dt.Rows[0]["userflag"].ToString().ToUpper() != "ADMINUSER")
                                    {
                                        Response.Redirect("LandingPage.aspx?Flag=L", false);
                                    }
                                    else
                                    {
                                        Response.Redirect("LandingPage.aspx", false);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        try
        {
            mpeChangePassword.Hide();
            divLogin.Visible = true;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
}

