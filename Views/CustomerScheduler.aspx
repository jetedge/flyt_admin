﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true" CodeFile="CustomerScheduler.aspx.cs" Inherits="Views_CustomerScheduler" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<script type="text/javascript">
    function datetimeupdate() {
        document.getElementById('txtExpTime1').value = document.getElementById('MainContent_txtExpTime').value;
        document.getElementById('txtStartTime1').value = document.getElementById('MainContent_txtStartTime').value;
    }

    </script>
    <script type="text/javascript">
        function checkAll(objRef) {
            var GridView = document.getElementById("MainContent_gvpAddress");
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function txtStartTimefun1(object) {
            var txtFlyHours1 = document.getElementById('txtStartTime1');
            document.getElementById('MainContent_txtStartTime').value = txtFlyHours1.value;
        }

        function txtEndTimefun(object) {
            var txtFlyHours2 = document.getElementById('txtExpTime1');
            document.getElementById('MainContent_txtExpTime').value = txtFlyHours2.value;
        }
        function ConfirmDeleteATA() {
            if (confirm('Are you sure want to delete ?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <style type="text/css">
        input[type="radio"], input[type="checkbox"]
        {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            margin-right: 5px;
            vertical-align: middle;
            margin-left: 6px;
            margin-bottom: 3px;
            height: 15px;
            width: 14px;
        }
        .form-group row
        {
            margin-bottom: 0rem !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="form-group row" id="dvFormView" runat="server">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-10">
                    <div class="form-group row text-right">
                        <div class="col-12">
                            <asp:Button ID="ButSubmit" runat="server" CssClass="btn btn-danger" OnClick="ButSubmit_Click"
                                TabIndex="17" Text="Save" ToolTip="Click here to Save." ValidationGroup="Scheduler" />
                            <asp:ValidationSummary ID="vldSumCountry" runat="server" ShowMessageBox="True" ShowSummary="False"
                                ValidationGroup="Scheduler" />
                            &nbsp;
                            <asp:Button ID="btnClear" runat="server" CausesValidation="False" CssClass="btn btn-danger"
                                OnClick="btnClear_Click" Style="position: static; display: none" TabIndex="18"
                                Text="Clear" ToolTip="Click here to clear." Width="65px" />
                            &nbsp;
                            <asp:Button ID="btnFileUpload" runat="server" CausesValidation="False" CssClass="btn btn-danger"
                                OnClick="btnBack_Click" Style="position: static; float: right;" TabIndex="18"
                                Text="Back" ToolTip="Click here to go to Grid View." />
                            <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="btn btn-danger"
                                OnClick="btnBack_Click" Style="position: static; float: right; display: none;"
                                TabIndex="18" Text="Back" ToolTip="Click here to back." />
                        </div>
                    </div>
                    <div id="tdStatus" runat="server" visible="false">
                        <div class="form-group row">
                            <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                padding: 7px 12px; border-bottom: 1px solid #C0C0C0 !important;">
                                Scheduler Status
                            </label>
                        </div>
                        <div class="form-group row align-items-center" style="border-bottom: 1px solid #C0C0C0;
                            padding-bottom: 0.5rem;">
                            <label class="col-3 col-form-label">
                                Current Status :
                                <asp:Label ID="lblCurrentStatus" Text="N/A" Font-Bold="true" runat="server"></asp:Label>
                                &nbsp;
                                <asp:Image ID="imgTick" ImageUrl="~/Components/images/tick_yes.png" Height="18px"
                                    runat="server" />
                            </label>
                            <label class="col-7 col-form-label">
                                <span style="color: #e4685d; font-weight: 600;">Last Modified by :</span>
                                <asp:Label ID="lblStatusBy" Text="N/A" runat="server" Style="font-weight: 600;"></asp:Label>
                            </label>
                            <div class="col-2 text-right">
                                <asp:Button ID="btnRunScheduler" runat="server" CssClass="btn btn-danger" TabIndex="17"
                                    Style="padding: 3px 12px;" Text="Run" ToolTip="Click here to Run Scheduler."
                                    CausesValidation="false" OnClick="btnRunScheduler_Click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnStopScheduler" runat="server" CssClass="btn btn-danger" TabIndex="17"
                                    Text="Stop" ToolTip="Click here to Stop Scheduler." CausesValidation="false"
                                    OnClick="btnStopScheduler_Click" Style="padding: 3px 12px;" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3 col-form-label" style="max-width: 13%;">
                            Effective From <span class="MandatoryField">*</span>
                        </label>
                        <div class="col-3">
                            <asp:TextBox ID="txtEffFrom" runat="server" TabIndex="10" Width="100%" onkeypress="return false;"
                                placeholder="MMM d, yyyy"></asp:TextBox>
                            <asp:Label ID="lblScheduleFrom" runat="server" Visible="false"></asp:Label>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="cal_Theme1"
                                Format="MMM d, yyyy" TargetControlID="txtEffFrom">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEffFrom"
                                SetFocusOnError="true" Display="None" ErrorMessage="Effective From is required!"
                                ValidationGroup="Scheduler">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-6">
                            <label style="line-height: 2.7;">
                                [ will receive the previous day created customers list ]</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3 col-form-label" style="max-width: 13%;">
                            Schedule Time <span class="MandatoryField">*</span>
                        </label>
                        <div class="col-1" style="padding-right: 0rem;">
                            <asp:DropDownList ID="ddlHour" runat="server">
                                <asp:ListItem Text="01" Value="01" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-1" style="padding-right: 0rem; padding-left: 0.2rem;">
                            <asp:DropDownList ID="ddlMin" runat="server">
                                <asp:ListItem Text="00" Value="00" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                <asp:ListItem Text="45" Value="45"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-1" style="padding-left: 0.2rem;">
                            <asp:DropDownList ID="ddlAMPM" runat="server">
                                <asp:ListItem Text="AM" Value="AM" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-6">
                            <label style="line-height: 2.7;">
                                [ daily run at the given time ]</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3 col-form-label" style="max-width: 13%;">
                            Schedule Days
                        </label>
                        <div class="col-9" style="padding-top: 10px;">
                            <asp:CheckBoxList ID="cblDays" runat="server" RepeatDirection="Horizontal" RepeatColumns="7"
                                Style="width: 100%;">
                                <asp:ListItem Text="Sunday" Value="sun"></asp:ListItem>
                                <asp:ListItem Text="Monday" Value="mon"></asp:ListItem>
                                <asp:ListItem Text="Tuesday" Value="tue"></asp:ListItem>
                                <asp:ListItem Text="Wednesday" Value="wed"></asp:ListItem>
                                <asp:ListItem Text="Thursday" Value="thu"></asp:ListItem>
                                <asp:ListItem Text="Friday" Value="fri"></asp:ListItem>
                                <asp:ListItem Text="Saturday" Value="sat"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="form-group row" id="tdSignature" visible="false" runat="server">
                        <label class="col-3 col-form-label" style="max-width: 13%;">
                            Email Signature <span class="MandatoryField">*</span>
                        </label>
                        <div class="col-7">
                            <asp:DropDownList ID="ddlSignature" runat="server" TabIndex="9" Width="85%">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlSignature"
                                SetFocusOnError="true" InitialValue="0" Display="None" ErrorMessage="Signature Email is required!"
                                ValidationGroup="Scheduler">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 p-0" style="border-bottom: 1px solid #C0C0C0 !important;">
                            <label style="font-size: 16px; text-align: left; font-weight: bold; padding: 7px 10px;">
                                Receive Email Alerts <span class="MandatoryField">*</span>
                            </label>
                            <asp:Button ID="btnAddAddress" Style="float: right; padding: 3px 12px;" runat="server"
                                CssClass="btn btn-danger" TabIndex="17" Text="Add Users" ToolTip="Click here to Add Users."
                                CausesValidation="false" OnClick="btnAddAddress_Click" />
                        </div>
                        <div class="col-12" style="padding: 10px 10px 0px 10px;">
                            <asp:GridView ID="gvToAddress" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                                EmptyDataText="No Receive Email Alerts Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                Width="100%" OnRowDeleting="gvToAddress_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                            <asp:Label ID="lblUserID" runat="server" Visible="false" Text='<%#Bind("UserID")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Bind("Name")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmailID" runat="server" Text='<%#Bind("EmailID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Role">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserType" runat="server" Text='<%#Bind("RoleName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" Visible="true">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDel" runat="server" CommandName="Delete" ImageUrl="~/Images/icon_delete.png"
                                                CausesValidation="false" AlternateText="Delete" ToolTip="Click here to Delete"
                                                CommandArgument="<%# Container.DataItemIndex + 1 %>" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 p-0" style="border-bottom: 1px solid #C0C0C0 !important;">
                            <label style="font-size: 16px; text-align: left; font-weight: bold; padding: 7px 10px;">
                                Add External User
                            </label>
                            <asp:Button ID="btnAddExtUser" Style="float: right; padding: 3px 12px;" runat="server"
                                CssClass="btn btn-danger" TabIndex="17" Text="Add Users" ToolTip="Click here to Add External User."
                                CausesValidation="false" OnClick="btnAddExtUser_Click" />
                        </div>
                        <div class="col-12" style="padding: 10px 10px 0px 10px;">
                            <asp:GridView ID="gvExtUser" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                                EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                OnRowDeleting="gvextUser_RowDeleting" DataKeyNames="ExtUserid" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                            <asp:Label ID="lblExtUserID" runat="server" Visible="false" Text='<%#Bind("ExtUserid")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Bind("Name")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmailID" runat="server" Text='<%#Bind("EmailID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Role">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserType" runat="server" Text='<%#Bind("UserRole")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="20%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDel" runat="server" CommandName="Delete" ImageUrl="~/Images/icon_delete.png"
                                                CausesValidation="false" AlternateText="Delete" ToolTip="Click here to Delete"
                                                CommandArgument="<%# Container.DataItemIndex + 1 %>" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="form-group row" id="trModify" runat="server" visible="false" style="display: none;">
                        <div class="col-12">
                            <label>
                                Last Modified by :
                                <asp:Label ID="lblModify" Style="font-weight: 600;" runat="server"></asp:Label>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
        </div>
        <div class="form-group row" id="dvGridView" runat="server">
            <div class="col-lg-12 text-right" style="padding-right: 4%; padding-left: 4%;">
                <asp:Button ID="btnAddScheduler" runat="server" CausesValidation="False" CssClass="btn btn-danger"
                    OnClick="btnAddScheduler_Click" Style="float: right; margin-bottom: 10px;" TabIndex="18"
                    Text="Add Scheduler" ToolTip="Click here to Add Scheduler." />&nbsp;&nbsp;
                <asp:Button ID="btnGoReport" runat="server" CausesValidation="False" CssClass="btn btn-danger"
                    PostBackUrl="~/Views/Customer.aspx" Style="float: right; margin-bottom: 10px;
                    margin-right: 5px;" TabIndex="18" Text="Go to Customer" ToolTip="Click here to go Customer Details." />
            </div>
            <div class="col-lg-12" style="padding-right: 4%; padding-left: 4%;">
                <asp:GridView ID="gvScheduler" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                    CssClass="table" HeaderStyle-CssClass="thead-dark" EmptyDataText="No Data Found"
                    Width="100%" OnSelectedIndexChanged="gvScheduler_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                                <asp:Label ID="lblScheID" runat="server" Text='<%#Bind("ScheID")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblRunFlag" runat="server" Visible="false" Text='<%#Bind("RunFlag")%>'></asp:Label>
                                <asp:Label ID="lblSignature" runat="server" Visible="false" Text='<%#Bind("Signature")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="2%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Effective From">
                            <ItemTemplate>
                                <asp:Label ID="lblEffDate" runat="server" Text='<%#Bind("EffDate")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Next Posting Time">
                            <ItemTemplate>
                                <asp:Label ID="lblEffTime" runat="server" Text='<%#Bind("NextPostingTime")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Style="font-weight: 600;"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Modified by">
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedby" runat="server" Text='<%#Bind("Createdby")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" Width="12%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Modified on">
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedon" runat="server" Text='<%#Bind("Createdon")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:ImageButton ID="imbEdit" runat="server" CommandName="Select" ImageUrl="~/Images/pencil_Edit.png"
                                    CausesValidation="false" Height="20px" AlternateText="Delete" ToolTip="Click here to Delete"
                                    CommandArgument="<%# Container.DataItemIndex + 1 %>" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle Width="5%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:Button ID="btnSavedPopup" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeATC" runat="server" PopupControlID="pnlATC" TargetControlID="btnSavedPopup"
        Y="30" CancelControlID="btnAdditionalClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlATC" runat="server" CssClass="modalPopupnew" Style="display: none;
        overflow-x: hidden; background-color: rgb(17,24,32); width: 65%; height: 80%;
        padding: 10px" ScrollBars="Auto">
        <div class="form-group row">
            <label class="col-9 col-form-label" style="color: White;">
                <h5 style="color: White; margin-left: 0%; font-size: 18px">
                    Add Receive Email Alerts</h5>
            </label>
            <div class="col-lg-3 text-right">
                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-danger" OnClick="btnAdd_Click"
                    TabIndex="17" Text="Submit" ToolTip="Click here to ADD." CausesValidation="false"
                    Style="padding: 3px 12px;" />
                &nbsp;&nbsp;
                <asp:Button ID="btnAdditionalClose" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="padding: 3px 12px;" ToolTip="Close Popup"></asp:Button>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-12">
                <asp:GridView ID="gvpAddress" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                    EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                    DataKeyNames="UserID" Width="100%" OnSelectedIndexChanged="gvToAddress_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' Style="color: White;"></asp:Label>
                                <asp:Label ID="lblUserID" runat="server" Text='<%#Bind("UserID")%>' Visible="false" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="1%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Bind("Name")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email ID">
                            <ItemTemplate>
                                <asp:Label ID="lblEmailID" runat="server" Text='<%#Bind("EmailID")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="15%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User Role">
                            <ItemTemplate>
                                <asp:Label ID="lblUserType" runat="server" Text='<%#Bind("RoleName")%>' Style="color: White;"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                <asp:Label ID="lnkSelect" runat="server" Text="Select All"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkStatus" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnAdEditExtUser" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeExtUser" runat="server" PopupControlID="pnlExtUser"
        TargetControlID="btnAdEditExtUser" Y="70" CancelControlID="btnCloseExtUser" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlExtUser" runat="server" CssClass="modalPopupnew" Style="display: none;
        background-color: rgb(17,24,32); width: 40%; padding: 10px">
        <div class="form-group row">
            <label class="col-6 col-form-label" style="color: White;">
                <h5 style="color: White; margin-left: 0%; font-size: 18px">
                    Add / Edit External User</h5>
            </label>
        </div>
        <div class="form-group row">
            <label class="col-3 col-form-label" style="color: White;">
                Name <span class="MandatoryField">* </span>
            </label>
            <div class="col-lg-9">
                <asp:TextBox ID="txtName" runat="server" MaxLength="100" ToolTip="Enter Alpha numeric characters and length is minited to 100."
                    TabIndex="1" Style="margin: 0; background-color: rgb(153,153,154); color: rgb(255,255,255);
                    border: 1px solid #ccc; padding: 10px; font-family: inherit; font-size: 16px;"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Rfv" runat="server" ControlToValidate="txtName" Display="None"
                    ErrorMessage="Name is required." SetFocusOnError="True" ValidationGroup="External">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-3 col-form-label" style="color: White;">
                Email Id <span class="MandatoryField">* </span>
            </label>
            <div class="col-lg-9">
                <asp:TextBox ID="txtEmailId" runat="server" Style="margin: 0; background-color: rgb(153,153,154);
                    color: rgb(255,255,255); border: 1px solid #ccc; padding: 10px; font-family: inherit;
                    font-size: 16px;" MaxLength="100" TabIndex="2" ToolTip="Enter a valis Email Id and length is minited to 100."></asp:TextBox>
                <asp:RequiredFieldValidator ID="Rfv3" runat="server" ControlToValidate="txtEmailId"
                    Display="None" ErrorMessage="EmailId is required." SetFocusOnError="True" ValidationGroup="External">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailId"
                    Display="None" ErrorMessage="Please enter valid email address." ValidationGroup="External"
                    SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-3 col-form-label" style="color: White;">
                User Role
            </label>
            <div class="col-lg-9">
                <asp:TextBox ID="txtUserRole" runat="server" Style="margin: 0; background-color: rgb(153,153,154);
                    color: rgb(255,255,255); border: 1px solid #ccc; padding: 10px; font-family: inherit;
                    font-size: 16px;" MaxLength="100" TabIndex="3" ToolTip="Enter Alpha numeric characters and length is minited to 100."></asp:TextBox>
            </div>
        </div>
        <br />
        <div class="form-group row">
            <div class="col-lg-12 text-center">
                <asp:Button ID="btnSaveExtUser" runat="server" CssClass="btn btn-danger" Text="Save"
                    TabIndex="4" CausesValidation="true" ToolTip="click here to save external user"
                    OnClick="btnSaveExtUser_Click" ValidationGroup="External" />
                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
                    ShowSummary="False" ValidationGroup="External" />
                <asp:Button ID="btnClearExtUser" runat="server" CssClass="btn btn-danger" Text="Clear"
                    TabIndex="5" ToolTip="click here to clear" OnClick="btnClearExtUser_Click" />
                <asp:Button ID="btnCloseExtUser" runat="server" CssClass="btn btn-danger" TabIndex="6"
                    Text="Close" ToolTip="Click here to close popup." CausesValidation="false" />
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32);border: 2px solid red; min-width: 25%; padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32); border: 2px solid red; min-width: 25%;
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <asp:Label ID="lblalert" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblSchId" Text="0" runat="server" Visible="false" />
    <asp:Label ID="lblRowIndexId" Text="0" runat="server" Visible="false" />
    <asp:Label ID="lblDeleteFor" Text="0" runat="server" Visible="false" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserId" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblExtUserId" runat="server" Visible="false"></asp:Label>
</asp:Content>

