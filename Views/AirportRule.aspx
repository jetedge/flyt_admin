﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="AirportRule.aspx.cs" Inherits="AirportRule" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .table th, .table td
        {
            padding: 0.5rem;
            vertical-align: middle;
            border: 0;
            border-top: 1px solid #EBEDF3;
        }
        .rowheader
        {
            font-weight: bold !important;
            color: #b21e28 !important;
            font-size: 1.1rem !important;
            letter-spacing: 0.5px !important;
        }
        .form-group
        {
            margin-bottom: 1rem;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function GetSelectedClient(checkboxlist, textbox) {

            var appendvalue = "";
            var cbTripType = document.getElementById(checkboxlist.toString());
            var txtClient = document.getElementById(textbox.toString());
            var checkbox = cbTripType.getElementsByTagName("input");
            var label = cbTripType.getElementsByTagName("label");
            var splitval = 0;
            for (var i = 0; i < checkbox.length; i++) {

                if (checkbox[i].checked) {

                    //appendvalue += label[i].innerHTML + ",";
                    if (appendvalue.length == 0) {
                        appendvalue += label[i].innerHTML;
                    }
                    else {
                        appendvalue += "," + label[i].innerHTML;
                    }
                    splitval++;
                }

            }
            txtClient.value = appendvalue;
            return false;
        }

        function disnone() {
            var bh = $find("cl");
            bh.hidePopup();
        }

        function checkUnCheckAll(id, cblist, textbox) {

            var cbClientCheckAll = document.getElementById(id.toString());
            var cbid1 = document.getElementById(cblist.toString());
            var txtTextBoxID = document.getElementById(textbox.toString());

            if (cbClientCheckAll.checked == true) {
                txtTextBoxID.value = '';
                txtTextBoxID.value = 'All';
                CheckAll(cbid1, txtTextBoxID);
            }
            else {
                UnCheckAll(cbid1, txtTextBoxID);
                txtTextBoxID.value = '';
            }
        }

        function CheckAll(cbListOrg, txtBoxOrg) {
            var checkbox = cbListOrg.getElementsByTagName("input");
            for (var i = 0; i < checkbox.length; i++) {
                checkbox[i].checked = true;
            }
        }

        function UnCheckAll(cbListOrg, txtBoxOrg) {
            var checkbox = cbListOrg.getElementsByTagName("input");
            for (var i = 0; i < checkbox.length; i++) {
                checkbox[i].checked = false;
            }
        }

        function GetRDBValue() {
            debugger;

            var radio = document.getElementsByName('MainContent_rblDepSetup');
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    alert(radio[i].value);
                }
            }

        }

    </script>
    <script type="text/javascript">
        window.onload = function () {
            var rbl = document.getElementById("MainContent_rblArrvSetup");
            var radio = rbl.getElementsByTagName("INPUT");
            for (var i = 0; i < radio.length; i++) {
                radio[i].onchange = function () {
                    var radio = this;
                    var label = radio.parentNode.getElementsByTagName("LABEL")[0];
                    if (radio.value == "S") {
                        document.getElementById("MainContent_dvArrvCountry").style.display = 'table-row';
                    } else {
                        document.getElementById("MainContent_dvArrvCountry").style.display = 'none';
                    }

                };
            }
        };

        function GetConfirmation() {
            var reply = confirm("Ary you sure you want to delete the selected Airport Rule?");
            if (reply) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="form-group row">
                    <div class="col-xl-1">
                    </div>
                    <div class="col-xl-5">
                        <div class="form-group row">
                            <label class="col-12 col-form-label rowheader">
                                <i class='fa fa-plane-departure' style="color: #b21e28;"></i>&nbsp;Departure Airport
                            </label>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">
                                Setup By
                            </label>
                            <div class="col-9">
                                <asp:RadioButtonList runat="server" ID="rblDepSetup" RepeatColumns="3" AutoPostBack="true"
                                    OnSelectedIndexChanged="rblDepSetup_SelectedIndexChanged" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Country" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="State" Value="S"></asp:ListItem>
                                    <asp:ListItem Text="Airport" Value="A"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group row" id="dvDepCountry" runat="server">
                            <label class="col-3 col-form-label">
                                Country
                            </label>
                            <div class="col-9">
                                <asp:DropDownList ID="ddlDepCountry" Width="80%" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlDepCountry_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row" id="dvDepState" runat="server" visible="false">
                            <label class="col-3 col-form-label">
                                Choose State
                            </label>
                            <div class="col-9">
                                <asp:TextBox ID="txtDepState" placeholder="Select Departure State." class="form-controlf"
                                    Width="80%" runat="server" TabIndex="6" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:HiddenField ID="hdnProcessHidden" Value="0" runat="server"></asp:HiddenField>
                                <ajax:PopupControlExtender ID="clientpopup" runat="server" BehaviorID="cl" Enabled="True"
                                    ExtenderControlID="" TargetControlID="txtDepState" PopupControlID="Panel1Client"
                                    OffsetY="42">
                                </ajax:PopupControlExtender>
                                <asp:Panel ID="Panel1Client" runat="server" Height="300px" Width="75%" BorderStyle="Solid"
                                    BorderColor="#cccccc" BorderWidth="1px" Direction="LeftToRight" ScrollBars="Auto"
                                    BackColor="White" Style="display: none">
                                    <table width="100%">
                                        <tr class="tr">
                                            <td align="left">
                                                <asp:CheckBox ID="cbDepStateALL" runat="server" Text="All" onclick="checkUnCheckAll (this.id,'MainContent_cbDepState','MainContent_txtDepState')" />
                                            </td>
                                            <td align="right">
                                                <asp:Image ID="Image1" runat="server" Style="width: 15px; height: 15px;" onclick="disnone()"
                                                    ToolTip="Close" ImageUrl="~/Images/close.png" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" width="100%">
                                                <asp:CheckBoxList ID="cbDepState" ForeColor="Black" runat="server" Height="10%" Width="100%"
                                                    onchange="GetSelectedClient('MainContent_cbDepState','MainContent_txtDepState')"
                                                    RepeatDirection="Vertical" RepeatColumns="0">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="form-group row" id="dvDepAirports" runat="server" visible="false">
                            <label class="col-3 col-form-label">
                                Choose Airport
                            </label>
                            <div class="col-9">
                                <asp:TextBox ID="txtDepAirport" placeholder="Choose Departure Airport" class="form-controlf"
                                    Style="width: 80%; text-transform: uppercase;" runat="server" TabIndex="6" AutoCompleteType="Disabled" />
                                <ajax:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="0"
                                    CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                    CompletionListItemCssClass="listItem" CompletionInterval="5" EnableCaching="false"
                                    CompletionSetCount="1" TargetControlID="txtDepAirport" ID="AutoCompleteExtender1"
                                    runat="server" FirstRowSelected="true" DelimiterCharacters="," ShowOnlyCurrentWordInCompletionListItem="true">
                                </ajax:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="form-group row">
                            <label class="col-12 col-form-label rowheader">
                                <i class='fa fa-plane-arrival' style="color: #b21e28;"></i>&nbsp;Arrival Airport
                            </label>
                        </div>
                        <div class="form-group row">
                            <label class="col-3 col-form-label">
                                Setup By
                            </label>
                            <div class="col-9">
                                <asp:RadioButtonList runat="server" ID="rblArrvSetup" RepeatColumns="3" AutoPostBack="true"
                                    OnSelectedIndexChanged="rblArrvSetup_SelectedIndexChanged" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Country" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="State" Value="S"></asp:ListItem>
                                    <asp:ListItem Text="Airport" Value="A"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group row" id="dvArrvCountry" runat="server">
                            <label class="col-3 col-form-label">
                                Country
                            </label>
                            <div class="col-9">
                                <asp:DropDownList ID="ddlArrvCountry" Width="80%" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlArrvCountry_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group row" id="dvArrvState" visible="false" runat="server">
                            <label class="col-3 col-form-label">
                                Choose State
                            </label>
                            <div class="col-9">
                                <asp:TextBox ID="txtArrvState" placeholder="Select Arrival State." class="form-controlf"
                                    Width="80%" runat="server" TabIndex="6" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:HiddenField ID="HiddenField1" Value="0" runat="server" />
                                <ajax:PopupControlExtender ID="PopupControlExtender1" runat="server" BehaviorID="cl"
                                    Enabled="True" ExtenderControlID="" TargetControlID="txtArrvState" PopupControlID="pnlArrvState"
                                    OffsetY="42">
                                </ajax:PopupControlExtender>
                                <asp:Panel ID="pnlArrvState" runat="server" Height="300px" Width="75%" BorderStyle="Solid"
                                    BorderColor="#cccccc" BorderWidth="1px" Direction="LeftToRight" ScrollBars="Auto"
                                    BackColor="White" Style="display: none">
                                    <table width="100%">
                                        <tr class="tr">
                                            <td align="left">
                                                <asp:CheckBox runat="server" Text="All" ID="chkArrvAll" onclick="checkUnCheckAll (this.id,'MainContent_cblArrvState','MainContent_txtArrvState')" />
                                            </td>
                                            <td align="right">
                                                <asp:Image ID="Image2" runat="server" Style="width: 15px; height: 15px;" onclick="disnone()"
                                                    ToolTip="Close" ImageUrl="~/Images/close.png" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" width="100%">
                                                <asp:CheckBoxList ID="cblArrvState" ForeColor="Black" runat="server" Height="10%"
                                                    Width="100%" onchange="GetSelectedClient('MainContent_cblArrvState','MainContent_txtArrvState')"
                                                    RepeatDirection="Vertical" RepeatColumns="0">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="form-group row" id="dvArrvAirports" runat="server" visible="false">
                            <label class="col-3 col-form-label">
                                Choose Airport
                            </label>
                            <div class="col-9">
                                <asp:TextBox ID="txtArrvAirport" placeholder="Choose Arrival Airport" class="form-controlf"
                                    Style="width: 80%; text-transform: uppercase;" runat="server" TabIndex="6" AutoCompleteType="Disabled" />
                                <ajax:AutoCompleteExtender ServiceMethod="GetCompletionList" MinimumPrefixLength="0"
                                    CompletionListCssClass="completionList" CompletionListHighlightedItemCssClass="itemHighlighted"
                                    CompletionListItemCssClass="listItem" CompletionInterval="5" EnableCaching="false"
                                    CompletionSetCount="1" TargetControlID="txtArrvAirport" ID="AutoCompleteExtender2"
                                    runat="server" FirstRowSelected="true" DelimiterCharacters="," ShowOnlyCurrentWordInCompletionListItem="true">
                                </ajax:AutoCompleteExtender>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3">
                    </div>
                    <div class="col-xl-6 text-center">
                        <asp:Button ID="btnAdd" OnClick="btnAdd_Clilck" runat="server" CssClass="btn btn-danger"
                            Text="Add to List" />
                        &nbsp;
                        <asp:Button ID="btnCancel" OnClick="btnCancel_Clilck" runat="server" CssClass="btn btn-danger"
                            Text="Cancel" />
                    </div>
                    <div class="col-xl-3 text-center" style="align-self: center;">
                        <asp:LinkButton ID="lnkRefreshData" runat="server" Font-Bold="true" Text="Refresh Data in Landing Page"
                            ForeColor="Blue" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xl-12 text-center p-6">
                        <asp:GridView ID="gvAirportRule" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                            PagerStyle-CssClass="pager" GridLines="Both" Width="100%" AutoGenerateColumns="false"
                            OnRowCreated="gvAirportRule_RowCreated" PageSize="25" AllowPaging="false">
                            <Columns>
                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                        <asp:Label ID="lblRuleId" Text='<%# Eval("RuleId") %>' runat="server" Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Setup By" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%"
                                    HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDepSetupBy" Text='<%# Eval("DepSetupBy") %>' runat="server" />
                                        <asp:Label ID="lblDepSetupByFlag" Text='<%# Eval("DepSetupByFlag") %>' runat="server"
                                            Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Airport Rule" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDepRuleCode" Text='<%# Eval("DepRuleCode") %>' runat="server" Visible="false" />
                                        <asp:Label ID="lblDepRuleName" Text='<%# Eval("DepRuleName") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Setup By" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblArrSetupBy" runat="server" Text='<%# Eval("ArrSetupBy") %>' />
                                        <asp:Label ID="lblArrSetupByFlag" Text='<%# Eval("ArrSetupByFlag") %>' runat="server"
                                            Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Airport Rule" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblArrRuleCode" runat="server" Text='<%# Eval("ArrRuleCode") %>' Visible="false" />
                                        <asp:Label ID="lblArrRuleName" runat="server" Text='<%# Eval("ArrRuleName") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Modified By/On" HeaderStyle-Width="13%" HeaderStyle-HorizontalAlign="left"
                                    ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblModifiedBy" Text='<%# Eval("ModifiedBy") %>' runat="server" />
                                        <br />
                                        <asp:Label ID="lblModifiedOn" Text='<%# Eval("ModifiedOn") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="~/Images/icon-edit-new.png"
                                            Height="22px" ToolTip="Click to Edit selected Airport Rule details" AlternateText="Edit Airport Rule details"
                                            CausesValidation="false" OnClick="imbEdit_click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                            ToolTip="Click to Delete selected Airport Rule details" AlternateText="Delete Airport Rule details"
                                            CausesValidation="false" OnClientClick="return GetConfirmation();" OnClick="imbDelete_click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <ajax:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert"
        PopupControlID="pnlalert" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </ajax:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <asp:Label ID="lblalert" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="Button1" runat="server" Style="display: none" Text="Show Modal Popup" />
    <ajax:ModalPopupExtender ID="mpeRefreshData" runat="server" TargetControlID="lnkRefreshData"
        PopupControlID="pnlRefreshData" BackgroundCssClass="modalBackground" Y="80" CancelControlID="lnkRefreshNo">
    </ajax:ModalPopupExtender>
    <asp:Panel ID="pnlRefreshData" runat="server" ScrollBars="Auto" Style="display: none;
        background-color: rgb(17,24,32); padding: 15px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); letter-spacing: 0.5px;" align="center" valign="middle">
                    Do you want to refresh the data in Landing page?
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="lnkRefresh" OnClick="lnkRefresh_Click" runat="server" CssClass="btn btn-danger"
                        CausesValidation="false" Text="Yes" />
                    &nbsp;
                    <asp:Button ID="lnkRefreshNo" runat="server" CssClass="btn btn-danger" CausesValidation="false"
                        Text="No" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="hdnRowId" runat="server" Text="0" Visible="false" />
    <asp:Label ID="lblUserId" runat="server" Visible="false" />
</asp:Content>
