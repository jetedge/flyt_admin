﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BusinessLayer;
using System.Web.UI.HtmlControls;

public partial class Views_DiscountandCoupon : System.Web.UI.Page
{
    protected DiscountandCouponBLL objDiscount = new DiscountandCouponBLL();
    public enum ButtonName { Save, Update };

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Home", "Discount & Coupon");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Home";
                lblSubHeader.InnerText = "Discount & Coupon";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                Bind_CheckBox_Users();

                BindGrid();

                dvForm.Visible = false;
                dvFormHeader.Visible = false;

                dvGrid.Visible = true;
                dvGridHeader.Visible = true;

                btnAdd.Focus();
                lblRowId.Text = "0";

                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;

                CalendarExtender1.StartDate = DateTime.Now;
                CalendarExtender2.StartDate = DateTime.Now;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    #region Button Events

    protected void txtEDate_TextChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow item in gvCustomerMapping.Rows)
            {
                AjaxControlToolkit.CalendarExtender ceStartDate = item.FindControl("ceStartDate") as AjaxControlToolkit.CalendarExtender;
                AjaxControlToolkit.CalendarExtender ceEndDate = item.FindControl("ceEndDate") as AjaxControlToolkit.CalendarExtender;

                if (txtstartdate.Text.Length > 0)
                {
                    ceStartDate.StartDate = Convert.ToDateTime(txtstartdate.Text);
                    ceEndDate.StartDate = Convert.ToDateTime(txtstartdate.Text);
                }

                if (txtEDate.Text.Length > 0)
                {
                    ceStartDate.EndDate = Convert.ToDateTime(txtEDate.Text);
                    ceEndDate.EndDate = Convert.ToDateTime(txtEDate.Text);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void txtusernameF_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Bind_CheckBox_Users();

            //DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FourSeasonsConnectionString, CommandType.Text, "select distinct rowid,(FirstName+' '+LastName)+' - '+Email RegisteredUser, Email from UserMasterCreate where FirstName is not null and Role IN ( 'User' , 'Broker')  AND Active = 1 and  FirstName+ ' '+ LastName like '%" + txtusernameF.Text + "%' OR BrokerName like '%" + txtusernameF.Text + "%'  order by (FirstName+' '+LastName)+' - '+Email").Tables[0];

            //gvCustomerMapping.DataSource = dtUser;
            //gvCustomerMapping.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void txtEmailF_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Bind_CheckBox_Users();

            //DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FourSeasonsConnectionString, CommandType.Text, "select distinct rowid,(FirstName+' '+LastName)+' - '+Email RegisteredUser, Email from UserMasterCreate where FirstName is not null and Role IN ( 'User' , 'Broker')  AND Active = 1 and FirstName like '" + txtusernameF.Text + "%' and Email like '%" + txtEmailF.Text + "%'  order by (FirstName+' '+LastName)+' - '+Email").Tables[0];

            //gvCustomerMapping.DataSource = dtUser;
            //gvCustomerMapping.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void rblApplyFlag_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Control c = sender as Control;
            GridViewRow selectedRow = c.NamingContainer as GridViewRow;  //find the current gridview row
            if (selectedRow != null)
            {
                int selectRowIndex = selectedRow.RowIndex;
                RadioButtonList rblApplyFlag = selectedRow.FindControl("rblApplyFlag") as RadioButtonList;
                TextBox txtStartDate = selectedRow.FindControl("txtStartDate") as TextBox;
                TextBox txtEndDate = selectedRow.FindControl("txtEndDate") as TextBox;

                if (rblApplyFlag.SelectedValue == "PE")
                {
                    txtStartDate.Visible = true;
                    txtEndDate.Visible = true;
                }
                else
                {
                    txtStartDate.Visible = false;
                    txtEndDate.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void rblAutoFlag_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DiscountType();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnAddOnClick(object sender, EventArgs e)
    {
        dvForm.Visible = true;
        dvFormHeader.Visible = true;

        dvGrid.Visible = false;
        dvGridHeader.Visible = false;

        btnAdd.Visible = false;
        btnSave.Visible = true;
        btnView.Visible = true;

        Clear();
        txtCouponCd.Focus();
    }

    protected void btnSaveOnClick(object sender, EventArgs e)
    {
        string strMsg = string.Empty;
        string strActive = string.Empty;

        try
        {
            if (hdfCDRID.Value == "0")
            {
                objDiscount.ManageType = "INS";

                DataTable dt = objDiscount.AlreadyExist(txtCouponCd.Value.Trim());
                if (dt.Rows.Count > 0)
                {
                    txtCouponCd.Focus();
                    return;
                }
            }
            else
            {
                objDiscount.ManageType = "UPD";
            }

            string strCam = string.Empty;
            int count1 = 0;
            foreach (GridViewRow item in gvCustomerMapping.Rows)
            {
                Label lblRowID = (Label)item.FindControl("lblRowID");
                Label lblRegisteredUser = (Label)item.FindControl("lblRegisteredUser");
                RadioButtonList rblApplyFlag = (RadioButtonList)item.FindControl("rblApplyFlag");
                CheckBox chkuser = (CheckBox)item.FindControl("chkuser");

                if (chkuser.Checked == true)
                {
                    if (strCam.Length == 0)
                        strCam = lblRowID.Text;
                    else
                        strCam += "," + lblRowID.Text;

                    count1++;
                }
            }
            strCam = strCam.TrimEnd(',').Trim();

            string ApplicableFor = string.Empty;
            for (int count = 0; count < chkApplicable.Items.Count; count++)
            {
                if (chkApplicable.Items[count].Selected)
                {
                    if (ApplicableFor.Length == 0)
                        ApplicableFor = chkApplicable.Items[count].Value;
                    else
                        ApplicableFor += "," + chkApplicable.Items[count].Value;

                    count1++;
                }
            }
            ApplicableFor = ApplicableFor.TrimEnd(',').Trim();

            if (gvCustomerMapping.Rows.Count == count1)
            {
                lbluserflag.Value = "A";
            }
            else
            {
                lbluserflag.Value = "P";
            }

            objDiscount.RowID = hdfCDRID.Value;
            objDiscount.CouponCode = txtCouponCd.Value.Trim();
            objDiscount.CouponDescription = strCam;

            objDiscount.StartDate = txtstartdate.Text.Trim();
            objDiscount.ExpiryDate = txtEDate.Text.Trim();

            if (rblAutoFlag.SelectedValue == "J")
            {
                objDiscount.CouponValue = txtRetailRedeemD.Value.Trim();
                objDiscount.CouponPercentage = txtRetailRedeemP.Value.Trim();
            }
            else
            {
                objDiscount.CouponValue = ddlDiscounttype.SelectedValue == "$" ? txtRAmount.Value.Trim() : "0.00";
                objDiscount.CouponPercentage = ddlDiscounttype.SelectedValue == "%" ? txtRAmount.Value.Trim() : "0.00";
            }

            objDiscount.Discountflag = ddlDiscounttype.SelectedValue.Trim();

            objDiscount.Description = txtcouponDesc.Value.Trim();
            objDiscount.UserFlag = lbluserflag.Value;
            objDiscount.Active = "1";
            objDiscount.AutoApply = rblAutoFlag.SelectedValue;
            objDiscount.ApplicableFor = ApplicableFor;

            objDiscount.Createdby = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
            objDiscount.CreatedOn = DateTime.Now.ToString();
            objDiscount.Modifiedby = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
            objDiscount.ModifiedOn = DateTime.Now.ToString();

            int retVal = objDiscount.SaveDiscountandCoupon(objDiscount);
            if (retVal > 0)
            {
                mpealert.Show();
                lblalert.Text = "Discount and Coupon details have been saved successfully.";

                objDiscount.ManageType = "DEL";
                objDiscount.RowID = retVal.ToString();
                int retVal11 = objDiscount.DeleteCoupon_Users(objDiscount);

                foreach (GridViewRow item in gvCustomerMapping.Rows)
                {
                    Label lblRowID = (Label)item.FindControl("lblRowID");
                    Label lblRegisteredUser = (Label)item.FindControl("lblRegisteredUser");
                    RadioButtonList rblApplyFlag = (RadioButtonList)item.FindControl("rblApplyFlag");
                    CheckBox chkuser = (CheckBox)item.FindControl("chkuser");

                    TextBox txtGridStartDate = (TextBox)item.FindControl("txtStartDate");
                    TextBox txtGridEndDate = (TextBox)item.FindControl("txtEndDate");

                    if (chkuser.Checked == true)
                    {
                        objDiscount.ManageType = "INS";
                        objDiscount.RowID = retVal.ToString();
                        objDiscount.UserId = lblRowID.Text;
                        objDiscount.ApplicableFlag = rblApplyFlag.SelectedValue;

                        if (rblApplyFlag.SelectedValue == "OT")
                        {
                            objDiscount.StartDate = null;
                            objDiscount.EndDate = null;
                        }
                        else
                        {
                            objDiscount.StartDate = txtGridStartDate.Text.Length > 0 ? txtGridStartDate.Text : txtstartdate.Text.Trim();
                            objDiscount.EndDate = txtGridEndDate.Text.Length > 0 ? txtGridEndDate.Text : txtEDate.Text.Trim();
                        }

                        objDiscount.Modifiedby = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                        objDiscount.ModifiedOn = DateTime.Now.ToString();

                        int retValUser = objDiscount.SaveCoupon_Users(objDiscount);
                    }

                }

                BindGrid();

                lblRowId.Text = "0";

                dvForm.Visible = false;
                dvFormHeader.Visible = false;

                dvGrid.Visible = true;
                dvGridHeader.Visible = true;

                btnSave.Visible = false;
                btnView.Visible = false;
                btnAdd.Visible = true;
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request";
                mpealert.Show();
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            BindGrid();

            dvForm.Visible = false;
            dvFormHeader.Visible = false;

            dvGrid.Visible = true;
            dvGridHeader.Visible = true;

            btnAdd.Focus();

            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        ImageButton lb = (ImageButton)sender;

        GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
        Label lblrowid = (Label)gvrow.FindControl("lblrowid");

        lblrowtodelete.Text = lblrowid.Text;
        mpeconfirm.Show();
    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "delete from  CouponDiscount  where rowid = '" + lblrowtodelete.Text.Trim() + "'");
        dvForm.Visible = false;
        dvGrid.Visible = true;
        dvGridHeader.Visible = true;
        dvFormHeader.Visible = false;
        BindGrid();
        hdfCDRID.Value = "0";
        lblalert.Text = "Discount and coupon details has been deleted successfully";
        mpealert.Show();
    }

    protected void ddlstatus_onselectedChanged(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void ddlDiscounttype_selectedindexchaged(object sender, EventArgs e)
    {
        //if (ddlDiscounttype.SelectedValue == "$")
        //{
        //    doller.Visible = true;
        //    doller1.Visible = true;

        //    divpercent.Visible = false;
        //    divpercent1.Visible = false;
        //}
        //else
        //{
        //    doller.Visible = false;
        //    doller1.Visible = false;

        //    divpercent.Visible = true;
        //    divpercent1.Visible = true;
        //}

        ddlDiscounttype.Focus();
    }

    #endregion

    #region Gridview Events

    protected void gvTailDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;

                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
                if (e.Row.Cells[7].Text.ToString() == "$0.00")
                {
                    e.Row.Cells[7].Text = "-";
                }
                else
                {
                    e.Row.Cells[7].Text = e.Row.Cells[7].Text.ToString();
                }

                if (e.Row.Cells[8].Text.ToString() == "0.00")
                {
                    e.Row.Cells[8].Text = "-";
                }
                else
                {
                    e.Row.Cells[8].Text = e.Row.Cells[8].Text.ToString();
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void gvCustomerMapping_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtStartDate = e.Row.FindControl("txtStartDate") as TextBox;
                TextBox txtEndDate = e.Row.FindControl("txtEndDate") as TextBox;

                AjaxControlToolkit.CalendarExtender ceStartDate = e.Row.FindControl("ceStartDate") as AjaxControlToolkit.CalendarExtender;
                AjaxControlToolkit.CalendarExtender ceEndDate = e.Row.FindControl("ceEndDate") as AjaxControlToolkit.CalendarExtender;

                if (txtstartdate.Text.Length > 0)
                {
                    ceStartDate.StartDate = Convert.ToDateTime(txtstartdate.Text);
                    ceEndDate.StartDate = Convert.ToDateTime(txtstartdate.Text);
                }

                if (txtEDate.Text.Length > 0)
                {
                    ceStartDate.EndDate = Convert.ToDateTime(txtEDate.Text);
                    ceEndDate.EndDate = Convert.ToDateTime(txtEDate.Text);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void gvdiscount_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvdiscount.SelectedRow.FindControl("lblrowid");

            DataTable dtEdit = objDiscount.EditDiscountandCoupon(lblrowid.Text);
            if (dtEdit.Rows.Count > 0)
            {
                hdfCDRID.Value = lblrowid.Text;
                string struser = dtEdit.Rows[0]["CouponDescription"].ToString();
                string[] k = struser.Split(',');

                txtCouponCd.Value = dtEdit.Rows[0]["CouponCode"].ToString();

                ddlDiscounttype.SelectedIndex = ddlDiscounttype.Items.IndexOf(ddlDiscounttype.Items.FindByText(dtEdit.Rows[0]["Discountflag"].ToString()));
                txtEDate.Text = Convert.ToDateTime(dtEdit.Rows[0]["ExpiryDate"].ToString()).ToString("MMM d, yyyy");
                txtstartdate.Text = Convert.ToDateTime(dtEdit.Rows[0]["startdate"].ToString()).ToString("MMM d, yyyy");

                txtcouponDesc.Value = dtEdit.Rows[0]["Description"].ToString();

                rblAutoFlag.SelectedIndex = rblAutoFlag.Items.IndexOf(rblAutoFlag.Items.FindByValue(dtEdit.Rows[0]["AutoApply"].ToString()));
                rblAutoFlag_SelectedIndexChanged(sender, e);

                ddlDiscounttype.SelectedIndex = ddlDiscounttype.Items.IndexOf(ddlDiscounttype.Items.FindByText(dtEdit.Rows[0]["Discountflag"].ToString()));
                if (rblAutoFlag.SelectedValue == "J")
                {
                    txtRetailRedeemD.Value = dtEdit.Rows[0]["CouponValue"].ToString();
                    txtRetailRedeemP.Value = dtEdit.Rows[0]["CouponPercentage"].ToString();
                }
                else
                {
                    if (ddlDiscounttype.SelectedIndex == 0)
                    {
                        txtRAmount.Value = dtEdit.Rows[0]["CouponValue"].ToString();
                    }
                    else
                    {
                        txtRAmount.Value = dtEdit.Rows[0]["CouponPercentage"].ToString();
                    }
                }

                DiscountType();

                string ApplicableFor = dtEdit.Rows[0]["ApplicableFor"].ToString();
                string[] arrApplicable = ApplicableFor.Split(',');
                if (arrApplicable.Length > 0)
                {
                    for (int i = 0; i < arrApplicable.Length; i++)
                    {
                        SelectCheckBoxList(arrApplicable[i].ToString());
                    }
                }

                tbllast.Visible = true;
                lblupdatedby.Text = ((Label)gvdiscount.SelectedRow.FindControl("lblcreatedby")).Text;
                lblupdatedon.Text = dtEdit.Rows[0]["createdon"].ToString();

                if (lblupdatedon.Text.Length > 1)
                {
                    lblupdatedon.Text = Convert.ToDateTime(lblupdatedon.Text).ToString("MMM d, yyyy hh:mm tt").ToUpper();
                }

                Bind_MappedUsers(hdfCDRID.Value);

                dvForm.Visible = true;
                dvGrid.Visible = false;
                dvGridHeader.Visible = false;
                dvFormHeader.Visible = true;
                btnSave.Visible = true;
                btnView.Visible = true;
                btnAdd.Visible = false;
                txtCouponCd.Focus();

                btnSave.Text = ButtonName.Update.ToString();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void Bind_MappedUsers(string strRowId)
    {
        DataTable dtCoupounUsers = objDiscount.SelectCoupon_Users(strRowId);
        if (dtCoupounUsers.Rows.Count > 0)
        {
            for (int i = 0; i < dtCoupounUsers.Rows.Count; i++)
            {
                foreach (GridViewRow item in gvCustomerMapping.Rows)
                {
                    Label lblRowID = (Label)item.FindControl("lblRowID");
                    Label lblRegisteredUser = (Label)item.FindControl("lblRegisteredUser");
                    RadioButtonList rblApplyFlag = (RadioButtonList)item.FindControl("rblApplyFlag");
                    CheckBox chkuser = (CheckBox)item.FindControl("chkuser");

                    TextBox txtStartDate = item.FindControl("txtStartDate") as TextBox;
                    TextBox txtEndDate = item.FindControl("txtEndDate") as TextBox;

                    if (lblRowID.Text.Trim() == dtCoupounUsers.Rows[i]["UserId"].ToString())
                    {
                        chkuser.Checked = true;
                        rblApplyFlag.SelectedValue = dtCoupounUsers.Rows[i]["ApplicableFlag"].ToString().Trim();

                        if (dtCoupounUsers.Rows[i]["ApplicableFlag"].ToString().Trim() == "PE")
                        {
                            txtStartDate.Visible = true;
                            txtEndDate.Visible = true;

                            txtStartDate.Text = Convert.ToDateTime(dtCoupounUsers.Rows[i]["StartDate"]).ToString("MM-dd-yyyy");
                            txtEndDate.Text = Convert.ToDateTime(dtCoupounUsers.Rows[i]["EndDate"]).ToString("MM-dd-yyyy");
                        }
                        else
                        {
                            txtStartDate.Visible = false;
                            txtEndDate.Visible = false;
                        }
                    }
                    else
                    {
                        if (rblApplyFlag.SelectedValue == "PE")
                        {
                            txtStartDate.Visible = true;
                            txtEndDate.Visible = true;
                        }
                        else
                        {
                            txtStartDate.Visible = false;
                            txtEndDate.Visible = false;
                        }
                    }
                }
            }
        }
        else
        {
            foreach (GridViewRow item in gvCustomerMapping.Rows)
            {
                CheckBox chkuser = (CheckBox)item.FindControl("chkuser");
                RadioButtonList rblApplyFlag = (RadioButtonList)item.FindControl("rblApplyFlag");

                chkuser.Checked = true;

                if (rblAutoFlag.SelectedValue == "A")
                {
                    rblApplyFlag.SelectedValue = "AB";
                }
            }
        }
    }

    #endregion

    #region User Defined Events

    private void DiscountType()
    {
        foreach (ListItem item in chkApplicable.Items)
        {
            item.Selected = false;
            item.Enabled = true;
        }

        if (rblAutoFlag.SelectedValue == "P")
        {
            SelectCheckBoxList("C");
            SelectCheckBoxList("B");
            SelectCheckBoxList("J");

            EnableCheckBoxList("C");
            EnableCheckBoxList("B");
            EnableCheckBoxList("J");
            DisableCheckBoxList("G");
            DisableCheckBoxList("R");
        }
        else if (rblAutoFlag.SelectedValue == "A")
        {
            SelectCheckBoxList("C");
            SelectCheckBoxList("B");
            SelectCheckBoxList("G");

            EnableCheckBoxList("C");
            EnableCheckBoxList("B");
            DisableCheckBoxList("J");
            EnableCheckBoxList("G");
            EnableCheckBoxList("R");
        }
        else if (rblAutoFlag.SelectedValue == "J")
        {
            UnSelectCheckBoxList("C");
            UnSelectCheckBoxList("B");
            UnSelectCheckBoxList("J");
            UnSelectCheckBoxList("G");
            SelectCheckBoxList("R");

            DisableCheckBoxList("C");
            DisableCheckBoxList("B");
            DisableCheckBoxList("J");
            DisableCheckBoxList("G");
            EnableCheckBoxList("R");
        }

        if (rblAutoFlag.SelectedValue == "J")
        {
            dvRetailRedeem.Visible = true;
            dvOtherRedeem.Visible = false;
        }
        else
        {
            dvRetailRedeem.Visible = false;
            dvOtherRedeem.Visible = true;
        }

        Bind_CheckBox_Users();
    }

    private void Clear()
    {
        lblRowId.Text = "0";
        hdfCDRID.Value = "0";
        txtCouponCd.Value = string.Empty;
        ddlDiscounttype.ClearSelection();
        txtcouponDesc.Value = string.Empty;

        rblAutoFlag.SelectedIndex = 0;
        foreach (ListItem item in chkApplicable.Items)
        {
            item.Selected = false;
        }

        DiscountType();

        foreach (GridViewRow item in gvCustomerMapping.Rows)
        {
            Label lblRowID = (Label)item.FindControl("lblRowID");
            Label lblRegisteredUser = (Label)item.FindControl("lblRegisteredUser");
            RadioButtonList rblApplyFlag = (RadioButtonList)item.FindControl("rblApplyFlag");
            CheckBox chkuser = (CheckBox)item.FindControl("chkuser");
            TextBox txtStartDate = item.FindControl("txtStartDate") as TextBox;
            TextBox txtEndDate = item.FindControl("txtEndDate") as TextBox;

            txtStartDate.Visible = false;
            txtEndDate.Visible = false;

            rblApplyFlag.SelectedIndex = 0;
            chkuser.Checked = true;

            AjaxControlToolkit.CalendarExtender ceStartDate = item.FindControl("ceStartDate") as AjaxControlToolkit.CalendarExtender;
            AjaxControlToolkit.CalendarExtender ceEndDate = item.FindControl("ceEndDate") as AjaxControlToolkit.CalendarExtender;

            ceStartDate.StartDate = DateTime.Now;
            ceEndDate.StartDate = DateTime.Now;
        }

        txtstartdate.Text = string.Empty;
        txtEDate.Text = string.Empty;

        txtRAmount.Value = string.Empty;
        txtRetailRedeemD.Value = string.Empty;
        txtRetailRedeemP.Value = string.Empty;

        btnSave.Text = ButtonName.Save.ToString();

        tbllast.Visible = false;
        lblupdatedby.Text = string.Empty;
        lblupdatedon.Text = string.Empty;
    }

    private void BindGrid()
    {
        DataTable dt = objDiscount.ListDiscountandCoupon(ddlstatus.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            gvdiscount.DataSource = dt;
            gvdiscount.DataBind();
        }
        else
        {
            gvdiscount.DataSource = null;
            gvdiscount.DataBind();
        }
    }

    private void Bind_CheckBox_Users()
    {
        DataTable dtUser = objDiscount.List_RegisteredUser("REG", txtusernameF.Text, txtEmailF.Text, rblAutoFlag.SelectedValue);

        gvCustomerMapping.DataSource = dtUser;
        gvCustomerMapping.DataBind();

        Bind_MappedUsers(hdfCDRID.Value);
    }

    /// <summary>
    /// Select Checkbox.
    /// </summary>
    private void SelectCheckBoxList(string valueToSelect)
    {
        ListItem listItem = this.chkApplicable.Items.FindByValue(valueToSelect);
        if (listItem != null) listItem.Selected = true;
    }

    /// <summary>
    /// Unselect the Checkbox.
    /// </summary>
    private void UnSelectCheckBoxList(string valueToSelect)
    {
        ListItem listItem = this.chkApplicable.Items.FindByValue(valueToSelect);
        if (listItem != null) listItem.Selected = false;
    }

    /// <summary>
    /// Disable the Checkbox list Item
    /// </summary>
    private void DisableCheckBoxList(string valueToDisable)
    {
        ListItem listItem = this.chkApplicable.Items.FindByValue(valueToDisable);
        if (listItem != null) listItem.Enabled = false;
    }

    /// <summary>
    /// Enable the Checkbox list Item
    /// </summary>
    private void EnableCheckBoxList(string valueToEnable)
    {
        ListItem listItem = this.chkApplicable.Items.FindByValue(valueToEnable);
        if (listItem != null) listItem.Enabled = true;
    }


    #endregion
}