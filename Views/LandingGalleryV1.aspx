﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="LandingGalleryV1.aspx.cs" Inherits="Views_LandingGallery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link type="text/css" rel="Stylesheet" href="assets/css/Slider.css" />
    <script type="text/javascript">
        function DeleteItem() {
            if (confirm("Are you sure you want to delete?")) {
                return true;
            }
            return false;
        }

        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }

        function DropFiles(fileUpload) {
            if (fileUpload.value != '') {
                var a = document.getElementById("MainContent_btnSave");
                a.click();
            }
        }

        function FileDetails1(fupSlder, imgSlder) {
            //            if (fupSlder.files && fupSlder.files[0]) {
            //                var reader = new FileReader();
            //                reader.onload = function (e) {
            //                    $(imgSlder).attr('src', e.target.result);
            //                }
            //                reader.readAsDataURL(fupSlder.files[0]);
            //            }


            //            var fi = document.getElementById('MainContent_fuFile');
            //            document.getElementById('MainContent_listofuploadedfilesCount1').innerHTML = '';
            //            document.getElementById('MainContent_listofuploadedfiles1').innerHTML = '';

            //            if (fi.files.length > 0) {
            //                if (fi.files.length > 4) {
            //                    document.getElementById('MainContent_lblalert').innerHTML = 'Max 4 file(s) allowed to upload.';
            //                    $find("mpealert").show();
            //                    return;
            //                }
            //                else {

            //                    document.getElementById('MainContent_listofuploadedfilesCount1').innerHTML =
            //                            'Total Files : ' + fi.files.length;

            //                    for (var i = 0; i <= fi.files.length - 1; i++) {

            //                        var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
            //                        var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.

            //                        document.getElementById('MainContent_listofuploadedfiles1').innerHTML =
            //                                document.getElementById('MainContent_listofuploadedfiles1').innerHTML + '<br />' +
            //                                     fname + ' ( <b>' + bytesToSize(fsize) + '</b> ) ';
            //                    }
            //                }
            //            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="uplLandingGallery" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnPreview" />
        </Triggers>
        <ContentTemplate>
            <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
                <div class="d-flex flex-column-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-10">
                                <div class="form-group row">
                                    <div class="col-xl-12 text-right">
                                        <asp:Button ID="btnPreview" runat="server" Text="View Preview" CssClass="btn btn-danger"
                                            TabIndex="1" OnClick="btnPreview_Click" />
                                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="FORM" ShowMessageBox="true"
                                            runat="server" ShowSummary="false" />
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-danger" Style="display: none;"
                                            TabIndex="1" OnClick="btnSave_Click" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xl-12 text-right">
                                        <label class="file-upload-new">
                                            <div class="kt-dropzone__msg dz-message needsclick">
                                                <h3 class="kt-dropzone__msg-title">
                                                    Drop file here or click to upload.</h3>
                                            </div>
                                            <asp:Label ID="lblImageName" runat="server" Visible="false" />
                                            <asp:FileUpload ID="fupImageUpload" accept=".png, .jpg, .jpeg" AllowMultiple="false"
                                                runat="server" ToolTip="Drop files here or click to upload." onchange="DropFiles(MainContent_fupImageUpload)"
                                                Style="width: 100%; height: 160px; cursor: pointer;"></asp:FileUpload>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-3 col-form-label">
                                        <label>
                                            Slider 1</label>
                                        <asp:LinkButton ID="SliderDelete1" runat="server" CommandName="SLD1" OnClick="lnkSliderDelete_Click"
                                            Visible="false" ToolTip="Click to Delete" Style="float: right;" OnClientClick="return DeleteItem()"> 
                                            <i class="fa fa-trash" style="color: #b72025;"></i> </asp:LinkButton>
                                        <label class="file-upload-new">
                                            <img src="../assets/img/No_Image_Available.jpg" id="imgSlder1" runat="server" style="border-radius: 5px;
                                                width: 100%; height: 100%;" alt="" />
                                            <asp:Label ID="lblSlider1" runat="server" Visible="false" />
                                            <asp:Label ID="lblSlider1RowId" runat="server" Visible="false" />
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-form-label">
                                        <label>
                                            Slider 2
                                        </label>
                                        <asp:LinkButton ID="SliderDelete2" runat="server" CommandName="SLD2" OnClick="lnkSliderDelete_Click"
                                            Visible="false" ToolTip="Click to Delete" Style="float: right;" OnClientClick="return DeleteItem()"> 
                                            <i class="fa fa-trash" style="color: #b72025;"></i> </asp:LinkButton>
                                        <label class="file-upload-new">
                                            <img src="../assets/img/No_Image_Available.jpg" id="imgSlder2" runat="server" style="border-radius: 5px;
                                                width: 100%; height: 100%;" alt="" />
                                            <asp:Label ID="lblSlider2" runat="server" Visible="false" />
                                            <asp:Label ID="lblSlider2RowId" runat="server" Visible="false" />
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-form-label">
                                        <label>
                                            Slider 3</label>
                                        <asp:LinkButton ID="SliderDelete3" runat="server" CommandName="SLD3" OnClick="lnkSliderDelete_Click"
                                            Visible="false" ToolTip="Click to Delete" Style="float: right;" OnClientClick="return DeleteItem()"> 
                                            <i class="fa fa-trash" style="color: #b72025;"></i> </asp:LinkButton>
                                        <label class="file-upload-new">
                                            <img src="../assets/img/No_Image_Available.jpg" id="imgSlder3" runat="server" style="border-radius: 5px;
                                                width: 100%; height: 100%;" alt="" />
                                            <asp:Label ID="lblSlider3" runat="server" Visible="false" />
                                            <asp:Label ID="lblSlider3RowId" runat="server" Visible="false" />
                                        </label>
                                    </div>
                                    <div class="col-lg-3 col-form-label">
                                        <label>
                                            Slider 4</label>
                                        <asp:LinkButton ID="SliderDelete4" runat="server" CommandName="SLD4" OnClick="lnkSliderDelete_Click"
                                            Visible="false" ToolTip="Click to Delete" Style="float: right;" OnClientClick="return DeleteItem()"> 
                                            <i class="fa fa-trash" style="color: #b72025;"></i> </asp:LinkButton>
                                        <label class="file-upload-new">
                                            <img src="../assets/img/No_Image_Available.jpg" id="imgSlder4" runat="server" style="border-radius: 5px;
                                                width: 100%; height: 100%;" alt="" />
                                            <asp:Label ID="lblSlider4" runat="server" Visible="false" />
                                            <asp:Label ID="lblSlider4RowId" runat="server" Visible="false" />
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12 col-form-label" style="line-height: 30px;">
                                        <span class="MandatoryField">Instructions </span>
                                        <br />
                                        1. Upload [ .png, .jpg, .jpeg ] File only and Max size is 20MB.<br />
                                        2. Upload the file without special characters /[<>'+\"\/;`%@!#$^&*()={}?,]/
                                    </label>
                                </div>
                                <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                    <div style="float: left; padding-top: 10px; padding-left: 15px;">
                                        <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;"></asp:Label>
                                        <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="btnTerms" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpePreview" runat="server" TargetControlID="btnTerms"
                PopupControlID="pnlPreview" CancelControlID="imgCloseterms" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlPreview" runat="server" Height="90%" Width="97%" ScrollBars="Auto"
                Style="display: none; background-color: #525659f5;">
                <div class="modal-dialog" role="document" style="height: 100%;">
                    <div class="modal-content" style="height: 100%;">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Landing Page Gallery
                            </h5>
                            <asp:LinkButton ID="imgCloseterms" runat="server" CssClass="btn btn-danger" Style="float: right;"
                                Text="Close" CausesValidation="false" />
                        </div>
                        <div class="modal-body">
                            <!-- Slideshow container -->
                            <div class="slideshow-container">
                                <!-- Full-width images with number and caption text -->
                                <div class="mySlides fade">
                                    <img id="imgSlider1" runat="server" style="width: 100%; height: 569px;" alt="" />
                                </div>
                                <div class="mySlides fade">
                                    <img id="imgSlider2" runat="server" style="width: 100%; height: 569px;" alt="" />
                                </div>
                                <div class="mySlides fade">
                                    <img id="imgSlider3" runat="server" style="width: 100%; height: 569px;" alt="" />
                                </div>
                                <div class="mySlides fade">
                                    <img id="imgSlider4" runat="server" style="width: 100%; height: 569px;" alt="" />
                                </div>
                                <!-- Next and previous buttons -->
                                <a class="prev" onclick="plusSlides(-1)"><i class="flaticon2-back"></i></a><a class="next"
                                    onclick="plusSlides(1)"><i class="flaticon2-next"></i></a>
                            </div>
                            <br>
                            <!-- The dots/circles -->
                            <div class="dvsliderdot">
                                <span class="dot" onclick="currentSlide(1)"></span><span class="dot" onclick="currentSlide(2)">
                                </span><span class="dot" onclick="currentSlide(3)"></span><span class="dot" onclick="currentSlide(4)">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpealert" BehaviorID="mpealert" runat="server" TargetControlID="btnalert"
                PopupControlID="pnlalert" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
                padding: 13px">
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="middle" align="center">
                            <asp:Label ID="lblalert" ForeColor="White" Font-Size="13.5px" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" ToolTip="Close Popup"
                                CausesValidation="false" Text="Ok" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <script type="text/javascript">
                var slideIndex = 1;
                showSlides(slideIndex);

                function plusSlides(n) {
                    showSlides(slideIndex += n);
                }

                function currentSlide(n) {
                    showSlides(slideIndex = n);
                }

                function showSlides(n) {
                    debugger;

                    var i;
                    var slides = document.getElementsByClassName("mySlides");
                    var dots = document.getElementsByClassName("dot");
                    if (n == undefined) { n = ++slideIndex }
                    if (n > slides.length) { slideIndex = 1 }
                    if (n < 1) { slideIndex = slides.length }
                    for (i = 0; i < slides.length; i++) {
                        slides[i].style.display = "none";
                    }
                    for (i = 0; i < dots.length; i++) {
                        dots[i].className = dots[i].className.replace(" active", "");
                    }
                    slides[slideIndex - 1].style.display = "block";
                    dots[slideIndex - 1].className += " active";

                    setTimeout(showSlides, 5000);
                }
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Label ID="lblUserId" Text="UserName" runat="server" Visible="false" />
</asp:Content>
