﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class Views_LandingGallery : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.LandingGalleryBLL objLandingGallery = new BusinessLayer.LandingGalleryBLL();

    public int linenum = 0;
    public string MethodName = string.Empty;

    #endregion

    #region Pageload

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            try
            {
                // Put user code to initialize the page here
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Landing Page Gallery");
                }


                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "General Setup";
                lblSubHeader.InnerText = "Landing Page Gallery";

                lblUserId.Text = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

                List();
            }
            catch (Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }
    }

    #endregion

    #region  User Defined Function

    private void List()
    {
        DataTable dt = objLandingGallery.ListGallery("");
        if (dt.Rows.Count > 0)
        {
            divLMBy.Visible = true;
            lblLMby.Text = dt.Rows[0]["UserName"].ToString() + " ( on ) " + dt.Rows[0]["UploadedOn"].ToString();

            string strPath = SecretsBLL.LandingImageURL;

            if (dt.Rows.Count == 1)
            {
                #region Count 1
                if (dt.Rows[0]["Image1"].ToString().Length > 0)
                {
                    imgSlder1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                    lblSlider1.Text = dt.Rows[0]["Image1"].ToString();
                    lblSlider1RowId.Text = dt.Rows[0]["RowId"].ToString();
                    SliderDelete1.Visible = true;

                    imgSlider1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                }
                #endregion
            }

            if (dt.Rows.Count == 2)
            {
                #region Count 2
                if (dt.Rows[0]["Image1"].ToString().Length > 0)
                {
                    imgSlder1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                    lblSlider1.Text = dt.Rows[0]["Image1"].ToString();
                    lblSlider1RowId.Text = dt.Rows[0]["RowId"].ToString();
                    SliderDelete1.Visible = true;

                    imgSlider1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                }

                if (dt.Rows[1]["Image1"].ToString().Length > 0)
                {
                    imgSlder2.Src = strPath + dt.Rows[1]["Image1"].ToString();
                    lblSlider2.Text = dt.Rows[1]["Image1"].ToString();
                    lblSlider2RowId.Text = dt.Rows[1]["RowId"].ToString();
                    SliderDelete2.Visible = true;

                    imgSlider2.Src = strPath + dt.Rows[1]["Image1"].ToString();
                }
                #endregion
            }

            if (dt.Rows.Count == 3)
            {
                #region Count 3
                if (dt.Rows[0]["Image1"].ToString().Length > 0)
                {
                    imgSlder1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                    lblSlider1.Text = dt.Rows[0]["Image1"].ToString();
                    lblSlider1RowId.Text = dt.Rows[0]["RowId"].ToString();
                    SliderDelete1.Visible = true;

                    imgSlider1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                }

                if (dt.Rows[1]["Image1"].ToString().Length > 0)
                {
                    imgSlder2.Src = strPath + dt.Rows[1]["Image1"].ToString();
                    lblSlider2.Text = dt.Rows[1]["Image1"].ToString();
                    lblSlider2RowId.Text = dt.Rows[1]["RowId"].ToString();
                    SliderDelete2.Visible = true;

                    imgSlider2.Src = strPath + dt.Rows[1]["Image1"].ToString();
                }

                if (dt.Rows[2]["Image1"].ToString().Length > 0)
                {
                    imgSlder3.Src = strPath + dt.Rows[2]["Image1"].ToString();
                    lblSlider3.Text = dt.Rows[2]["Image1"].ToString();
                    lblSlider3RowId.Text = dt.Rows[2]["RowId"].ToString();
                    SliderDelete3.Visible = true;

                    imgSlider3.Src = strPath + dt.Rows[2]["Image1"].ToString();
                }
                #endregion
            }

            if (dt.Rows.Count == 4)
            {
                #region Count 4
                if (dt.Rows[0]["Image1"].ToString().Length > 0)
                {
                    imgSlder1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                    lblSlider1.Text = dt.Rows[0]["Image1"].ToString();
                    lblSlider1RowId.Text = dt.Rows[0]["RowId"].ToString();
                    SliderDelete1.Visible = true;

                    imgSlider1.Src = strPath + dt.Rows[0]["Image1"].ToString();
                }

                if (dt.Rows[1]["Image1"].ToString().Length > 0)
                {
                    imgSlder2.Src = strPath + dt.Rows[1]["Image1"].ToString();
                    lblSlider2.Text = dt.Rows[1]["Image1"].ToString();
                    lblSlider3RowId.Text = dt.Rows[1]["RowId"].ToString();
                    SliderDelete2.Visible = true;

                    imgSlider2.Src = strPath + dt.Rows[1]["Image1"].ToString();
                }

                if (dt.Rows[2]["Image1"].ToString().Length > 0)
                {
                    imgSlder3.Src = strPath + dt.Rows[2]["Image1"].ToString();
                    lblSlider3.Text = dt.Rows[2]["Image1"].ToString();
                    lblSlider3RowId.Text = dt.Rows[2]["RowId"].ToString();
                    SliderDelete3.Visible = true;

                    imgSlider3.Src = strPath + dt.Rows[2]["Image1"].ToString();
                }

                if (dt.Rows[3]["Image1"].ToString().Length > 0)
                {
                    imgSlder4.Src = strPath + dt.Rows[3]["Image1"].ToString();
                    lblSlider4.Text = dt.Rows[3]["Image1"].ToString();
                    lblSlider4RowId.Text = dt.Rows[3]["RowId"].ToString();
                    SliderDelete4.Visible = true;

                    imgSlider4.Src = strPath + dt.Rows[3]["Image1"].ToString();
                }
                #endregion
            }
        }
        else
        {
            divLMBy.Visible = false;
            lblLMby.Text = string.Empty;
        }
    }

    #endregion

    #region Button Events

    public void btnPreview_Click(object sender, System.EventArgs e)
    {
        try
        {
            mpePreview.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void lnkSliderDelete_Click(object sender, System.EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            string strCommand = lnk.CommandName;

            string strRowId = string.Empty;
            string strFileName = string.Empty;

            if (strCommand == "SLD1")
            {
                strRowId = lblSlider1RowId.Text;
                strFileName = lblSlider1.Text;
            }
            else if (strCommand == "SLD2")
            {
                strRowId = lblSlider2RowId.Text;
                strFileName = lblSlider2.Text;
            }
            else if (strCommand == "SLD3")
            {
                strRowId = lblSlider3RowId.Text;
                strFileName = lblSlider3.Text;
            }
            else if (strCommand == "SLD4")
            {
                strRowId = lblSlider4RowId.Text;
                strFileName = lblSlider4.Text;
            }

            string retVal = objLandingGallery.DeleteGallery(strRowId);

            if (retVal != null)
            {
                string FileURL = SecretsBLL.LandingGallery;
                bool exists = Directory.Exists(FileURL.ToString());
                if (exists)
                {
                    bool existsfile = File.Exists(FileURL.ToString() + strFileName);
                    if (existsfile)
                    {
                        File.Delete(FileURL.ToString() + strFileName);
                    }
                }

                lblalert.Text = "Landing page Image Gallery has been deleted successfully";
                mpealert.Show();
            }

            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            DataTable dt = objLandingGallery.ListGallery("");
            if (dt.Rows.Count == 4)
            {
                lblalert.Text = "Allowed to upload 4 Slides";
                mpealert.Show();
                return;
            }

            string strFileName = string.Empty;
            string FileURL = SecretsBLL.LandingGallery;

            bool exists = Directory.Exists(FileURL.ToString());
            if (!exists)
            {
                Directory.CreateDirectory(FileURL.ToString());
            }

            if (fupImageUpload.HasFile)
            {
                fupImageUpload.SaveAs(FileURL + fupImageUpload.FileName);
                objLandingGallery.Image1 = removeSpecialChar(fupImageUpload.FileName);
            }
            else if (lblImageName.Text.Length > 0)
            {
                objLandingGallery.Image1 = removeSpecialChar(lblImageName.Text);
            }

            objLandingGallery.ManageType = "INS";

            objLandingGallery.Notes = "";
            objLandingGallery.Status = "Y";
            objLandingGallery.UploadedBy = lblUserId.Text;
            objLandingGallery.ImagePath = FileURL;

            int retVal = objLandingGallery.SaveGallery(objLandingGallery);

            if (retVal > 0)
            {
                lblalert.Text = "Landing page Image Gallery has been uploaded successfully";
                mpealert.Show();
            }

            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public static string removeSpecialChar(string input)
    {
        return Regex.Replace(input, "[^0-9A-Za-z.-]", "_");
    }

    #endregion
}