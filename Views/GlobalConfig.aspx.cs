﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using DataLayer;
using Newtonsoft.Json.Linq;

public partial class Views_TaxSetup : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.TaxSetupBLL objTaxSetupBLL = new BusinessLayer.TaxSetupBLL();
    protected BusinessLayer.ExternalUserGlobalBLL objGlobal = new BusinessLayer.ExternalUserGlobalBLL();

    RapidAPI objRapidAPI = new RapidAPI();
    GlobalDAL objGlobalDAL = new GlobalDAL();

    public int linenum = 0;
    public string MethodName = string.Empty;

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            try
            {
                // Put user code to initialize the page here
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Global Config");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "General Setup";
                lblSubHeader.InnerText = "Global Config";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                genclass.FillControl(ddlTail, "select ROWID,TailNo from Tailsetup order by TailNo");
                bindValues();
                rblFlightHours_SelectedIndexChanged(sender, e);

                BindFLeet();

                BindFFTail();

                Bind_CustomFLYT();

                GetValueFOreFlight();

                ExtUserList();

                List_CrewDutyFilter();
            }
            catch (Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                mpealert.Show();
                lblalert.Text = genclass.LogException_New(ex, linenum, MethodName);

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }
    }

    #endregion

    #region Web Form Designer generated code


    #endregion

    #region Button Events

    //public void btnSave_Click(object sender, EventArgs e)
    //{
    //    try
    //    {

    //        SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into Enablepayment ([EnablePayment] , [Updatedby] , [Updatedon]  ) values ('" + rblPaymentgateway.SelectedValue + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

    //        string MailFlag = string.Empty;
    //        string SmsFlag = string.Empty;
    //        string FlightHours = string.Empty;
    //        MailFlag = rblMailConfig.SelectedValue;
    //        SmsFlag = rblEnableSMS.SelectedValue;
    //        FlightHours = rblFlightHours.SelectedValue;

    //        DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
    //        if (dtGlobalConfig.Rows.Count > 0)
    //        {
    //            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [MailFlag] = '" + MailFlag + "' , [SmsFlag] = '" + SmsFlag + "',[FlightHourFlag]='" + FlightHours + "', [UpdatedBy]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [UpdatedOn]= '" + DateTime.Now + "'");
    //        }
    //        else
    //        {
    //            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [UpdatedBy], [UpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
    //        }

    //        string EventName = string.Empty;
    //        EventName = rblMailConfig.SelectedValue == "S" ? "Mail Config : Zendesk to SMTP" : "Mail Config : SMTP to Zendesk";
    //        SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

    //        EventName = string.Empty;
    //        EventName = rblEnableSMS.SelectedValue == "Y" ? "Enable SMS : No to Yes" : "Enable SMS : Yes to No";
    //        SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");


    //        EventName = string.Empty;
    //        EventName = "Flight Hours Changed";
    //        SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");



    //        lblalert.Text = "Global config has been updated sucessfully.";
    //        mpealert.Show();
    //        bindValues();
    //    }
    //    catch (Exception ex)
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
    //    }
    //}

    protected void ddlForeFlightTail_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindProfiles();
            mpeForeFlight.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void rblSetupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeForeFlight.Show();

            if (rblSetupBy.SelectedValue == "SPE")
            {
                dvFleet.Visible = true;
            }
            else
            {
                dvFleet.Visible = false;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnPaymentGatewaySave_Click(object sender, EventArgs e)
    {
        try
        {
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into Enablepayment ([EnablePayment] , [Updatedby] , [Updatedon]  ) values ('" + rblPaymentgateway.SelectedValue + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

            lblalert.Text = "Global config [ Payment (Authorize .Net) ] details has been updated sucessfully.";
            mpealert.Show();

            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void rblFlightHours_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (rblFlightHours.SelectedValue == "FF")
            {
                divForFlight.Visible = true;

            }
            else
            {
                divForFlight.Visible = false;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void lnkAddNew_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnkAddNew = sender as LinkButton;
            if (lnkAddNew.CommandName == "FOREFLYT")
            {
                GetAircraft.getAircraft();
                BindFFTail();
                BindProfiles();

                mpeForeFlight.Show();
                ddlForeFlightTail.ClearSelection();
                ddlForeFlightTail_SelectedIndexChanged(sender, e);

                rblSetupBy.SelectedValue = "All";
                rblSetupBy_SelectedIndexChanged(sender, e);

                btnSaveFleet.Text = "Save";
                lblForeFLightID.Text = "0";

                ddlProfiles.ClearSelection();

                ddlwindModel.ClearSelection();
                txtfixedWindComponent.Text = string.Empty;
                txtfixedIsaDeviation.Text = string.Empty;
                txthistoricalProbability.Text = string.Empty;
            }
            else
            {
                mpeExtUser.Show();
                ClearExternalUser();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void gvForeFlight_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GetAircraft.getAircraft();
            BindFFTail();

            lblForeFLightID.Text = ((Label)gvForeFlight.SelectedRow.FindControl("lblForeFlightID")).Text;

            ddlForeFlightTail.SelectedIndex =
                ddlForeFlightTail.Items.IndexOf(ddlForeFlightTail.Items.FindByText(((Label)gvForeFlight.SelectedRow.FindControl("lblName")).Text));
            ddlForeFlightTail_SelectedIndexChanged(sender, e);

            ddlProfiles.SelectedIndex =
                ddlProfiles.Items.IndexOf(ddlProfiles.Items.FindByValue(((Label)gvForeFlight.SelectedRow.FindControl("lblCruiseProfiles")).Text));

            ddlwindModel.SelectedIndex = ddlwindModel.Items.IndexOf(ddlwindModel.Items.FindByValue(((Label)gvForeFlight.SelectedRow.FindControl("lblwindModel")).Text.Trim()));
            txtfixedWindComponent.Text = ((Label)gvForeFlight.SelectedRow.FindControl("lblfixedWindComponent")).Text;
            txtfixedIsaDeviation.Text = ((Label)gvForeFlight.SelectedRow.FindControl("lblfixedIsaDeviation")).Text;
            txthistoricalProbability.Text = ((Label)gvForeFlight.SelectedRow.FindControl("lblhistoricalProbability")).Text;

            rblSetupBy.SelectedIndex = rblSetupBy.Items.IndexOf(rblSetupBy.Items.FindByText(((Label)gvForeFlight.SelectedRow.FindControl("lblSetupBy")).Text.Trim()));
            rblSetupBy_SelectedIndexChanged(sender, e);

            DataTable dtFore = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "Select * FROM Global_ForeFlightFleet WHERE ForeFlightID='" + lblForeFLightID.Text.Trim() + "'").Tables[0];
            if (dtFore.Rows.Count > 0)
            {
                if (gvFleet.Rows.Count > 0)
                {
                    for (int i = 0; i < gvFleet.Rows.Count; i++)
                    {
                        Label lblFOreid = (Label)gvFleet.Rows[i].FindControl("lblFleetID");

                        CheckBox cb = (CheckBox)gvFleet.Rows[i].FindControl("chkStatus");
                        cb.Checked = false;
                        for (int j = 0; j < dtFore.Rows.Count; j++)
                        {
                            if (lblFOreid.Text == dtFore.Rows[j]["FleetID"].ToString().Trim())
                            {
                                cb.Checked = true;
                            }
                        }
                    }

                }
            }
            btnSaveFleet.Text = "Update";

            mpeForeFlight.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void gvForeFlight_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)gvForeFlight.Rows[e.RowIndex];
            lblForeFLightID.Text = ((Label)row.FindControl("lblForeFlightID")).Text;

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete FROM Global_ForeFlightFleet WHERE ForeFlightID='" + lblForeFLightID.Text.Trim() + "'");
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete FROM Global_ForeFlight WHERE RowID='" + lblForeFLightID.Text.Trim() + "'");
            lblalert.Text = "Flight Hours has been deleted sucessfully.";
            mpealert.Show();
            GetValueFOreFlight();

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void GetValueFOreFlight()
    {
        DataTable dtFleet = objGlobal.Bind_ForFlight();
        if (dtFleet.Rows.Count > 0)
        {
            gvForeFlight.DataSource = dtFleet;
            gvForeFlight.DataBind();
        }
        else
        {
            gvForeFlight.DataSource = null;
            gvForeFlight.DataBind();
        }
    }

    public void btnSaveFleet_Click(object sender, EventArgs e)
    {
        string strRes = string.Empty;
        try
        {
            if (btnSaveFleet.Text == "Save")
            {
                object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO Global_ForeFlight ( TailId, SetupBy, CruiseProfiles, windModel, fixedWindComponent, fixedIsaDeviation, historicalProbability, CreatedBy, CreatedON ) VALUES ('" + ddlForeFlightTail.SelectedItem.Text.Trim() + "','" + rblSetupBy.SelectedValue.Trim() + "', '" + ddlProfiles.SelectedValue.Trim() + "', '" + ddlwindModel.SelectedValue.Trim() + "', '" + txtfixedWindComponent.Text.Trim() + "', '" + txtfixedIsaDeviation.Text.Trim() + "', '" + txthistoricalProbability.Text.Trim() + "','" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "','" + DateTime.Now + "') select SCOPE_IDENTITY()");
                strRes = obj.ToString();
            }
            else
            {
                object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE Global_ForeFlight set TailId ='" + ddlForeFlightTail.SelectedItem.Text.Trim() + "', SetupBy='" + rblSetupBy.SelectedValue.Trim() + "', CruiseProfiles='" + ddlProfiles.SelectedValue.Trim() + "', windModel='" + ddlwindModel.SelectedValue.Trim() + "', fixedWindComponent = '" + txtfixedWindComponent.Text.Trim() + "', fixedIsaDeviation = '" + txtfixedIsaDeviation.Text.Trim() + "', historicalProbability = '" + txthistoricalProbability.Text.Trim() + "', CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "',CreatedON='" + DateTime.Now + "' where RowID='" + lblForeFLightID.Text.Trim() + "'");
                strRes = lblForeFLightID.Text.Trim();
            }

            if (strRes != "0")
            {
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From Global_ForeFlightFleet WHERE ForeFlightID='" + strRes.Trim() + "'");

                if (rblSetupBy.SelectedValue.Trim() == "All")
                {
                    for (int i = 0; i < gvFleet.Rows.Count; i++)
                    {
                        Label lblFleetID = (Label)gvFleet.Rows[i].FindControl("lblFleetID");
                        SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO Global_ForeFlightFleet (ForeFlightID,FleetID) VALUES ('" + strRes + "','" + lblFleetID.Text + "')");
                    }
                }
                else
                {
                    if (gvFleet.Rows.Count > 0)
                    {
                        for (int i = 0; i < gvFleet.Rows.Count; i++)
                        {
                            CheckBox cb = (CheckBox)gvFleet.Rows[i].FindControl("chkStatus");
                            Label lblFleetID = (Label)gvFleet.Rows[i].FindControl("lblFleetID");

                            if (cb.Checked == true)
                            {
                                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO Global_ForeFlightFleet (ForeFlightID,FleetID) VALUES ('" + strRes + "','" + lblFleetID.Text + "')");
                            }
                        }
                    }
                }
            }

            GetValueFOreFlight();

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            dvFleet.Visible = false;
            ddlForeFlightTail.ClearSelection();
            ddlProfiles.ClearSelection();

            ddlwindModel.ClearSelection();
            txtfixedWindComponent.Text = string.Empty;
            txtfixedIsaDeviation.Text = string.Empty;
            txthistoricalProbability.Text = string.Empty;

            rblSetupBy.SelectedIndex = 0;

            mpeForeFlight.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnFHSave_Click(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;
            FlightHours = rblFlightHours.SelectedValue;

            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [FlightHourFlag]='" + FlightHours + "', [FHUpdatedBY]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [FHUpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [FHUpdatedBY], [FHUpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = "Flight Hours Changed";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

            lblalert.Text = "Flight Hours has been updated sucessfully.";
            mpealert.Show();
            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnFFApply_Click(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;
            FlightHours = "FF";

            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [FlightHourFlag]='" + FlightHours + "', [FHUpdatedBY]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [FHUpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [FHUpdatedBY], [FHUpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = "Flight Hours Changed";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

            lblalert.Text = "Flight Hours has been updated sucessfully.";
            mpealert.Show();
            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnMNApply_Click(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;
            FlightHours = "MN";

            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [FlightHourFlag]='" + FlightHours + "', [FHUpdatedBY]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [FHUpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [FHUpdatedBY], [FHUpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = "Flight Hours Changed";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

            lblalert.Text = "Flight Hours has been updated sucessfully.";
            mpealert.Show();
            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnGCApply_Click(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;
            FlightHours = "GC";

            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [FlightHourFlag]='" + FlightHours + "', [FHUpdatedBY]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [FHUpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [FHUpdatedBY], [FHUpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = "Flight Hours Changed";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

            lblalert.Text = "Flight Hours has been updated sucessfully.";
            mpealert.Show();
            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnGFApply_Click(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;
            FlightHours = "GF";

            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [FlightHourFlag]='" + FlightHours + "', [FHUpdatedBY]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [FHUpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [FHUpdatedBY], [FHUpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = "Flight Hours Changed";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");

            lblalert.Text = "Flight Hours has been updated sucessfully.";
            mpealert.Show();
            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnSMSSave_Click(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;
            SmsFlag = rblEnableSMS.SelectedValue;

            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [SMSFLag]='" + rblEnableSMS.SelectedValue + "', [SMSUpdatedBY]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [SMSUpdatedOn]= '" + DateTime.Now + "'");


            lblalert.Text = "Enable SMS has been updated sucessfully.";
            mpealert.Show();
            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnDocSignSave_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  DocuSignExpiry='" + txtDocExp.Text + "',DocSignEffFrom='" + txtEffFrom.Text + "', [DocSignUpdatedBy]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [DocSignUpdatedOn]= '" + DateTime.Now + "'");

            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO GlobalConfig (DocuSignExpiry,DocSignEffFrom,DocSignUpdatedBy,DocSignUpdatedOn)  VALUES ('" + txtDocExp.Text + "','" + txtEffFrom.Text + "','" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', '" + DateTime.Now + "'");


            }

            DataTable dtHistory = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig_DocSignHistory WHERE DocSignEffFrom='" + Convert.ToDateTime(txtEffFrom.Text) + "'");

            if (dtHistory.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE GlobalConfig_DocSignHistory SET DocSignExpiry='" + txtDocExp.Text + "',DocSignEffFrom='" + txtEffFrom.Text + "',DocSignUpdatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "',DocSignUpdatedOn='" + DateTime.Now + "' WHERE DSHID='" + dtHistory.Rows[0]["DSHID"].ToString() + "'");
            }

            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO GlobalConfig_DocSignHistory (DocSignExpiry,DocSignEffFrom,DocSignUpdatedBy,DocSignUpdatedOn)  VALUES ('" + txtDocExp.Text + "','" + txtEffFrom.Text + "','" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', '" + DateTime.Now + "')");
            }

            lblalert.Text = "Docu Sign Expiry has been updated sucessfully.";
            mpealert.Show();
            bindValues();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    //public void btnMLSave_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [MailFlag]='" + rblMailConfig.SelectedValue + "', [UpdatedBY]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [UpdatedOn]= '" + DateTime.Now + "'");


    //        lblalert.Text = "Mail Configuration has been updated sucessfully.";
    //        mpealert.Show();
    //        bindValues();
    //    }
    //    catch (Exception ex)
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
    //    }
    //}


    public void btnCalc_Click(object sender, EventArgs e)
    {
        try
        {
            dvSearch.Visible = false;
            txtDepartDate.Text = System.DateTime.Now.ToString("MMM dd, yyyy");

            txtStartTime.Value = "10:00";
            txtStartAirport.Text = string.Empty;
            txtEndAirport.Text = string.Empty;
            txtAvgSpeed.Text = "420";

            if (rblFlightHours.SelectedValue == "MN")
            {
                btnFFApply.Visible = true;
                btnMNApply.Visible = false;
                btnGCApply.Visible = true;
            }
            else if (rblFlightHours.SelectedValue == "GC")
            {
                btnFFApply.Visible = true;
                btnMNApply.Visible = true;
                btnGCApply.Visible = false;
            }
            else
            {

                btnFFApply.Visible = false;
                btnMNApply.Visible = true;
                btnGCApply.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            mpeCalculation.Show();

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            dvSearch.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            string strStartAirport = txtStartAirport.Text;
            string strEndAirport = txtEndAirport.Text;
            string StrdepartTime = Convert.ToDateTime(txtDepartDate.Text).Date.ToString("MM/dd/yyyy") + ' ' + txtStartTime.Value;

            //Great Circle API
            DataTable dtGC = objRapidAPI.FlightHourCalculation(strStartAirport, strEndAirport, ddlTail.SelectedItem.Text, StrdepartTime, txtAvgSpeed.Text, "GC");

            //FOre Flight API
            DataTable dtFF = FlightHourCalculation_Test(strStartAirport, strEndAirport, ddlTail.SelectedItem.Text, StrdepartTime, txtAvgSpeed.Text, "FF");

            //Great Circle Mapper Formula
            DataTable dtGCF = objRapidAPI.FlightHourCalculation(strStartAirport, strEndAirport, ddlTail.SelectedItem.Text, StrdepartTime, txtAvgSpeed.Text, "GF");

            //Distance Formula
            DataTable dtMN = objRapidAPI.FlightHourCalculation(strStartAirport, strEndAirport, ddlTail.SelectedItem.Text, StrdepartTime, txtAvgSpeed.Text, "MN");

            if (dtGC.Rows.Count > 0)
            {
                string StrFlightHours = (Convert.ToInt32(dtGC.Rows[0]["flight_time_min"].ToString()) / 60).ToString() + ":" +
                 (Convert.ToInt32(dtGC.Rows[0]["flight_time_min"].ToString()) % 60).ToString();
                decimal hour = Convert.ToDecimal(StrFlightHours.Split(':')[0]);
                decimal min = Convert.ToDecimal(StrFlightHours.Split(':')[1]);
                lblGCFlightTimeMin.Text = (((hour * 60) + min) / 60).ToString("0.0");

                lblGCDepartureTime.Text = Convert.ToDateTime(StrdepartTime).ToString("MM/dd/yyyy HH:mm");
                lblGCArrivalTime.Text = FindEndDatetime_SameAirport(strStartAirport, strEndAirport, StrdepartTime, StrFlightHours);
                lblGCDistance.Text = dtGC.Rows[0]["distance_nm"].ToString();
                lblGCFlightTimehour.Text = StrFlightHours;
            }


            if (dtFF.Rows.Count > 0)
            {
                //double mini = Convert.ToDouble(dtFF.Rows[0]["flight_time_min"].ToString());
                //TimeSpan spWorkMin = TimeSpan.FromMinutes(mini);
                //string strHourMinute = spWorkMin.ToString(@"hh\:mm");

                string strHourMinute = (Convert.ToInt32(dtFF.Rows[0]["flight_time_min"].ToString()) / 60).ToString() + ":" +
              (Convert.ToInt32(dtFF.Rows[0]["flight_time_min"].ToString()) % 60).ToString();
                decimal hour = Convert.ToDecimal(strHourMinute.Split(':')[0]);
                decimal min = Convert.ToDecimal(strHourMinute.Split(':')[1]);
                lblFFFlightTimeMin.Text = (((hour * 60) + min) / 60).ToString("0.0");

                lblFFDepartureTime.Text = Convert.ToDateTime(StrdepartTime).ToString("MM/dd/yyyy HH:mm");
                lblFFArrivalTime.Text = FindEndDatetime_SameAirport(strStartAirport, strEndAirport, StrdepartTime, strHourMinute);
                lblFFDistance.Text = dtFF.Rows[0]["distance_nm"].ToString();
                lblFFFlightTimehour.Text = strHourMinute;

            }


            if (dtGCF.Rows.Count > 0)
            {
                //double mini = Convert.ToDouble(dtMN.Rows[0]["flight_time_min"].ToString());
                //TimeSpan spWorkMin = TimeSpan.FromMinutes(mini);
                //string strHourMinute = spWorkMin.ToString(@"hh\:mm");

                string strHourMinute = (Convert.ToInt32(dtGCF.Rows[0]["flight_time_min"].ToString()) / 60).ToString() + ":" +
           (Convert.ToInt32(dtGCF.Rows[0]["flight_time_min"].ToString()) % 60).ToString();
                decimal hour = Convert.ToDecimal(strHourMinute.Split(':')[0]);
                decimal min = Convert.ToDecimal(strHourMinute.Split(':')[1]);
                lblFlightTimeMin.Text = (((hour * 60) + min) / 60).ToString("0.0");


                lblDepartureTime.Text = Convert.ToDateTime(StrdepartTime).ToString("MM/dd/yyyy HH:mm");
                lblArrivalTime.Text = FindEndDatetime_SameAirport(strStartAirport, strEndAirport, StrdepartTime, strHourMinute);
                lblDistance.Text = Convert.ToDecimal(dtGCF.Rows[0]["distance_nm"]).ToString("0.00");
                lblFlightTimehour.Text = strHourMinute;
            }


            if (dtMN.Rows.Count > 0)
            {
                //double mini = Convert.ToDouble(dtMN.Rows[0]["flight_time_min"].ToString());
                //TimeSpan spWorkMin = TimeSpan.FromMinutes(mini);
                //string strHourMinute = spWorkMin.ToString(@"hh\:mm");

                string strHourMinute = (Convert.ToInt32(dtMN.Rows[0]["flight_time_min"].ToString()) / 60).ToString() + ":" + (Convert.ToInt32(dtMN.Rows[0]["flight_time_min"].ToString()) % 60).ToString();
                decimal hour = Convert.ToDecimal(strHourMinute.Split(':')[0]);
                decimal min = Convert.ToDecimal(strHourMinute.Split(':')[1]);
                lblDFFlightTimeMin.Text = (((hour * 60) + min) / 60).ToString("0.0");


                lblDFDepartureTime.Text = Convert.ToDateTime(StrdepartTime).ToString("MM/dd/yyyy HH:mm");
                lblDFArrivalTime.Text = FindEndDatetime_SameAirport(strStartAirport, strEndAirport, StrdepartTime, strHourMinute);
                lblDFDistance.Text = Convert.ToDecimal(dtMN.Rows[0]["distance_nm"]).ToString("0.00");
                lblDFFlightTimehour.Text = strHourMinute;
            }




            mpeCalculation.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    #region  User Defined Function

    public void BindFFTail()
    {
        ddlForeFlightTail.Items.Clear();
        DataTable dtFleet = genclass.GetDataTable("select distinct AircraftReg,AircraftReg AircraftReg1 from JE_QUOTE..AircraftProfiles order by AircraftReg");
        if (dtFleet != null && dtFleet.Rows.Count > 0)
        {
            ddlForeFlightTail.DataSource = dtFleet;
            ddlForeFlightTail.DataValueField = "AircraftReg";
            ddlForeFlightTail.DataTextField = "AircraftReg";
            ddlForeFlightTail.DataBind();
            ddlForeFlightTail.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Please Select", "0"));
        }
        else
        {
            ddlForeFlightTail.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Not Available", "0"));
        }
    }
    public void BindProfiles()
    {
        ddlProfiles.Items.Clear();
        DataTable dtFleet = genclass.GetDataTable("select distinct uuid,profileName  from JE_QUOTE..AircraftProfiles where AircraftReg='" + ddlForeFlightTail.SelectedValue + "' order by profileName");
        if (dtFleet != null && dtFleet.Rows.Count > 0)
        {
            ddlProfiles.DataSource = dtFleet;
            ddlProfiles.DataValueField = "uuid";
            ddlProfiles.DataTextField = "profileName";
            ddlProfiles.DataBind();
            ddlProfiles.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Please Select", "0"));
        }
        else
        {
            ddlProfiles.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Not Available", "0"));
        }
    }

    public void BindFLeet()
    {
        DataTable dtFleet = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from  FleetTypeMaster  where Active='Y' order by FleetName").Tables[0];

        if (dtFleet.Rows.Count > 0)
        {
            gvFleet.DataSource = dtFleet;
            gvFleet.DataBind();

        }
        else
        {
            gvFleet.DataSource = null;
            gvFleet.DataBind();
        }
    }

    public DataSet getTimeZone(string startAirport, string strEndAiport)
    {
        SqlParameter[] getTripsLegParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", false);
        getTripsLegParm[0].Value = "TIMEZONE";
        getTripsLegParm[4].Value = startAirport;
        getTripsLegParm[5].Value = strEndAiport;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_PERSZOOM_02_DEPARTTIME", getTripsLegParm);
    }

    public string FindEndDatetime_SameAirport(string strStartAirport, string strEndAirport,
       string strStartDatetime, string workingHours)
    {
        string strStartTimeZone = string.Empty;
        string strEndTimeZone = string.Empty;
        string APIJsonstring = string.Empty;
        string StrFlightHours = workingHours.Replace(".", ":");
        string EndDatetime = string.Empty;

        DataSet dsTimeZoom = getTimeZone(strStartAirport.Trim(), strEndAirport.Trim());

        if (dsTimeZoom.Tables[0].Rows.Count > 0)
        {
            strStartTimeZone = dsTimeZoom.Tables[0].Rows[0]["TimeZone"].ToString();
        }
        if (dsTimeZoom.Tables[1].Rows.Count > 0)
        {
            strEndTimeZone = dsTimeZoom.Tables[1].Rows[0]["TimeZone"].ToString();
        }
        int offsetDiff = Convert.ToInt32(strEndTimeZone) - Convert.ToInt32(strStartTimeZone);

        decimal Chour = Convert.ToDecimal(StrFlightHours.Split(':')[0]);
        decimal Cmin = Convert.ToDecimal(StrFlightHours.Split(':')[1]);
        int hour = Convert.ToInt32(StrFlightHours.Split(':')[0]);
        int minutes = Convert.ToInt32(StrFlightHours.Split(':')[1]);

        if (strStartDatetime != string.Empty)
        {
            //find Entime and Zulu
            DateTime dtZuluStart = Convert.ToDateTime(strStartDatetime).AddHours((-1) * Convert.ToInt32(strStartTimeZone));
            DateTime dtZuluEnd = dtZuluStart.AddHours(Convert.ToInt32(Chour)).AddMinutes(Convert.ToInt32(Cmin));
            EndDatetime = dtZuluEnd.AddHours(Convert.ToInt32(strEndTimeZone)).ToString();
        }

        if (strStartDatetime != string.Empty)
        {
            EndDatetime = Convert.ToDateTime(strStartDatetime).AddHours(offsetDiff).AddHours(Convert.ToInt32(Chour)).AddMinutes(Convert.ToInt32(Cmin)).ToString();
        }

        return Convert.ToDateTime(EndDatetime).ToString("MM/dd/yyyy HH:mm");

    }

    public void Clear()
    {
        hdfCDRID.Value = "0";
        rblPaymentgateway.SelectedIndex = 0;
        //   rblMailConfig.SelectedIndex = 0;
        rblFlightHours.SelectedIndex = 0;
        rblEnableSMS.SelectedIndex = 0;

        btnPGSave.Text = "Save";
        // btnMLSave.Text = "Save";
        btnSMSSave.Text = "Save";
        btnFHSave.Text = "Save";
        btnDocSignSave.Text = "Save";
    }

    public void bindValues()
    {
        Clear();

        DataSet ds = objGlobalDAL.SELData("SEL");

        DataTable dtpayment = ds.Tables[0];
        if (dtpayment != null && dtpayment.Rows.Count > 0)
        {
            rblPaymentgateway.SelectedValue = dtpayment.Rows[0]["EnablePayment"].ToString();
            if (dtpayment.Rows[0]["UserName"].ToString() != string.Empty)
            {
                lblPGLMby.Text = dtpayment.Rows[0]["UserName"].ToString() + " ( on ) " + genclass.Long_Date_Time(Convert.ToDateTime(dtpayment.Rows[0]["Updatedon"]));
                divPGLMBy.Visible = true;
                btnPGSave.Text = "Update";
            }
        }

        DataTable dtGlobalConfig = ds.Tables[1];
        if (dtGlobalConfig.Rows.Count > 0)
        {
            if (dtGlobalConfig.Rows[0]["SmsFlag"].ToString().Length > 0)
            {
                rblEnableSMS.SelectedValue = dtGlobalConfig.Rows[0]["SmsFlag"].ToString();
                if (dtGlobalConfig.Rows[0]["SMSUpdatedBY"].ToString() != string.Empty)
                {
                    lblSMSLMby.Text = dtGlobalConfig.Rows[0]["SMSUpdatedBY"].ToString() + " ( on ) " + genclass.Long_Date_Time(Convert.ToDateTime(dtGlobalConfig.Rows[0]["SMSUpdatedOn"]));
                    divSMSLMBy.Visible = true;
                    btnSMSSave.Text = "Update";
                }
            }
            if (dtGlobalConfig.Rows[0]["FlightHourFlag"].ToString().Length > 0)
            {
                rblFlightHours.SelectedValue = dtGlobalConfig.Rows[0]["FlightHourFlag"].ToString();
                if (dtGlobalConfig.Rows[0]["FHUpdatedBY"].ToString() != string.Empty)
                {
                    lblFHLMby.Text = dtGlobalConfig.Rows[0]["FHUpdatedBY"].ToString() + " ( on ) " + genclass.Long_Date_Time(Convert.ToDateTime(dtGlobalConfig.Rows[0]["FHUpdatedOn"]));
                    divFHLMBy.Visible = true;
                    btnFHSave.Text = "Update";
                }
            }

            if (dtGlobalConfig.Rows[0]["DocuSignExpiry"].ToString().Length > 0)
            {
                txtDocExp.Text = dtGlobalConfig.Rows[0]["DocuSignExpiry"].ToString();
                txtEffFrom.Text = Convert.ToDateTime(dtGlobalConfig.Rows[0]["DocSignEffFrom"]).ToString("MMM dd, yyyy");
                if (dtGlobalConfig.Rows[0]["DocUpdatedBy"].ToString() != string.Empty)
                {
                    lblDocUp.Text = dtGlobalConfig.Rows[0]["DocUpdatedBy"].ToString() + " ( on ) " + genclass.Long_Date_Time(Convert.ToDateTime(dtGlobalConfig.Rows[0]["DocSignUpdatedOn"]));
                    divDOC.Visible = true;
                    btnDocSignSave.Text = "Update";
                }
            }
        }
    }

    #endregion

    #region Custom FLYT

    public void btnCustomSave_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtExists = genclass.GetDataTable("Select * FROM GlobalConfig_CustomFLYT_History where EffectiveFrom = '" + Convert.ToDateTime(txtCustomEffec.Text).ToString("yyyy-MM-dd") + "' ");
            if (dtExists.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig_CustomFLYT_History SET  [PriceCap1] ='" + txtPriceCap1.Text + "', [PriceCap2]= '" + txtPriceCap2.Text + "', IncStopsPrice = '" + txtIncrementalStops.Text + "', [EffectiveFrom] = '" + Convert.ToDateTime(txtCustomEffec.Text).ToString("yyyy-MM-dd") + "', [ModifiedBy]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [ModifiedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_CustomFLYT_History ( [PriceCap1], [PriceCap2], [IncStopsPrice], [EffectiveFrom], [ModifiedBy], [ModifiedOn] ) values ('" + txtPriceCap1.Text + "','" + txtPriceCap2.Text + "','" + txtIncrementalStops.Text + "' , '" + Convert.ToDateTime(txtCustomEffec.Text).ToString("yyyy-MM-dd") + "', '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', '" + DateTime.Now + "' ) ");
            }

            lblalert.Text = "Global config [ Custom FLYT ] details have been updated sucessfully.";
            mpealert.Show();

            Clear_CustomFLYT();
            Bind_CustomFLYT();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void lnkCustomHistory_Click(object sender, EventArgs e)
    {
        try
        {
            string Qry = "Select [GlobalConfig_CustomFLYT_History].RowId,PriceCap1, PriceCap2, IncStopsPrice, ModifiedBy, ";
            Qry += "ModifiedOn, UserMasterCreate.FirstName + ' ' + UserMasterCreate.LastName as [UserName] ";
            Qry += "from [GlobalConfig_CustomFLYT_History] ";
            Qry += "LEFT JOIN UserMasterCreate on UserMasterCreate.RowID = ModifiedBy ";
            Qry += "order by EffectiveFrom desc";

            DataTable dtCustomFLYTHis = genclass.GetDataTable(Qry);
            if (dtCustomFLYTHis.Rows.Count > 0)
            {
                mpeCFLYTHistory.Show();

                gvCustomFLYT.DataSource = dtCustomFLYTHis;
                gvCustomFLYT.DataBind();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    private void Bind_CustomFLYT()
    {
        DataTable dtCustomFLYT = objGlobal.Bind_CustomFLYT();
        if (dtCustomFLYT.Rows.Count > 0)
        {
            btnCustomSave.Text = "Update";

            txtPriceCap1.Text = dtCustomFLYT.Rows[0]["PriceCap1"].ToString();
            txtPriceCap2.Text = dtCustomFLYT.Rows[0]["PriceCap2"].ToString();
            txtIncrementalStops.Text = Convert.ToDecimal(dtCustomFLYT.Rows[0]["IncStopsPrice"]).ToString("0");
            txtCustomEffec.Text = Convert.ToDateTime(dtCustomFLYT.Rows[0]["EffectiveFrom"]).ToString("MMM dd, yyyy");

            dvCFLYtModified.Visible = true;
            lblCFLYtModifiedBy.Text = dtCustomFLYT.Rows[0]["UserName"].ToString() + " ( on ) " + genclass.Long_Date_Time(Convert.ToDateTime(dtCustomFLYT.Rows[0]["ModifiedOn"].ToString()));
        }
    }

    private void Clear_CustomFLYT()
    {
        btnCustomSave.Text = "Save";
        txtPriceCap1.Text = string.Empty;
        txtPriceCap2.Text = string.Empty;
        txtIncrementalStops.Text = string.Empty;

        dvCFLYtModified.Visible = false;
    }

    #endregion

    #region External Users

    protected void gvextUser_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow row = (GridViewRow)gvExtUser.Rows[e.RowIndex];
            lblForeFLightID.Text = ((Label)row.FindControl("lblExtUserID")).Text;

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete FROM Booking_ExternalUsers WHERE ExtId='" + lblForeFLightID.Text.Trim() + "'");

            lblalert.Text = "External User has been deleted sucessfully.";
            mpealert.Show();
            ExtUserList();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvextUser_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvExtUser.SelectedRow.FindControl("lblExtUserID");

            DataTable dtPS = objGlobal.Edit(lblrowid.Text.Trim());
            if (dtPS != null && dtPS.Rows.Count > 0)
            {
                lblExtUserId.Text = lblrowid.Text;
                txtName.Text = dtPS.Rows[0]["UserName"].ToString();
                txtEmailId.Text = dtPS.Rows[0]["EmailId"].ToString();
                txtUserRole.Text = dtPS.Rows[0]["UserRole"].ToString();
            }

            btnSaveExtUser.Text = "Update";
            mpeExtUser.Show();

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }

    public void ExtUserList()
    {
        DataTable dt = objGlobal.UserList();
        if (dt.Rows.Count > 0)
        {
            gvExtUser.DataSource = dt;
            gvExtUser.DataBind();
        }
        else
        {
            gvExtUser.DataSource = null;
            gvExtUser.DataBind();
        }
    }

    protected void btnSaveExtUser_Click(object sender, EventArgs e)
    {
        try
        {

            if (btnSaveExtUser.Text == "Save")
            {
                objGlobal.ManageTye = "I";
            }
            else
            {
                objGlobal.ManageTye = "U";
            }

            objGlobal.ExtId = lblExtUserId.Text.Trim();
            objGlobal.UserName = txtName.Text.Trim();
            objGlobal.EmailId = txtEmailId.Text.Trim();
            objGlobal.UserRole = txtUserRole.Text.Trim();
            objGlobal.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

            string retVal = objGlobal.Save(objGlobal);
            if (retVal != "0")
            {
                if (btnSaveExtUser.Text == "Save")
                {
                    lblalert.Text = "External User [ " + txtName.Text.Trim() + " ] details has been saved successfully";

                }
                else
                {
                    lblalert.Text = "External User [ " + txtName.Text.Trim() + " ] details has been updated successfully";
                }

                mpealert.Show();
                btnSaveExtUser.Focus();
                ClearExternalUser();
                ExtUserList();
            }
            else
            {
                lblMessage.Value = "An error has occured while processing your request";
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnClearExtUser_Click(object sender, EventArgs e)
    {
        try
        {
            ClearExternalUser();
            mpeExtUser.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    void ClearExternalUser()
    {
        btnSaveExtUser.Text = "Save";
        lblExtUserId.Text = "0";

        txtName.Text = string.Empty;
        txtEmailId.Text = string.Empty;
        txtUserRole.Text = string.Empty;
    }

    #endregion

    #region Crew / Day Duty Filter

    protected void btnCrewDutySave_Click(object sender, EventArgs e)
    {
        try
        {
            if (lblCrewDayId.Text == "0")
            {
                objGlobal.ManageTye = "I";
                lblalert.Text = "Crew / Day Duty Filter details has been saved successfully";
            }
            else
            {
                objGlobal.ManageTye = "U";
                lblalert.Text = "Crew / Day Duty Filter details has been updated successfully";
            }

            objGlobal.DutyId = lblCrewDayId.Text;
            objGlobal.BLKHours = txtBLKHours.Text.Trim();
            objGlobal.CrewDutyFilter = rblCrewDutyFilter.SelectedValue.Trim();

            objGlobal.CrewDuty = txtCrewHours.Text.Trim();
            objGlobal.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

            string retVal = objGlobal.Save_DayDutyFilter(objGlobal);

            if (retVal != "0")
            {
                mpealert.Show();

                List_CrewDutyFilter();
            }
            else
            {
                lblMessage.Value = "An error has occured while processing your request";
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    public void List_CrewDutyFilter()
    {
        DataTable dtCrewDutyFilter = objGlobal.List_CrewDutyFilter();
        if (dtCrewDutyFilter.Rows.Count > 0)
        {
            txtBLKHours.Text = Convert.ToInt32(dtCrewDutyFilter.Rows[0]["BLKHrs"]).ToString();
            txtCrewHours.Text = Convert.ToInt32(dtCrewDutyFilter.Rows[0]["CrewHrs"]).ToString();
            lblCrewDayId.Text = dtCrewDutyFilter.Rows[0]["DutyId"].ToString();
            rblCrewDutyFilter.SelectedValue = dtCrewDutyFilter.Rows[0]["EnableFlag"].ToString();

            btnCrewDutySave.Text = "Update";

            div3.Visible = true;
            Label10.Text = dtCrewDutyFilter.Rows[0]["ModifiedBy"].ToString() + " ( on ) " + dtCrewDutyFilter.Rows[0]["ModifiedOn"].ToString();
        }
        else
        {
            txtBLKHours.Text = string.Empty;
            txtCrewHours.Text = string.Empty;
            lblCrewDayId.Text = "0";
            rblCrewDutyFilter.ClearSelection();
            rblCrewDutyFilter.SelectedIndex = 0;

            btnCrewDutySave.Text = "Save";

            div3.Visible = false;
            Label10.Text = string.Empty;
        }
    }

    #endregion

    public void btnDocSignHistory_Click(object sender, EventArgs e)
    {
        try
        {
            mpeDHistoru.Show();
            DataSet ds = objGlobalDAL.SELData("SEL");
            DataTable dtHistry = ds.Tables[2];
            if (dtHistry.Rows.Count > 0)
            {
                gvDocHistory.DataSource = dtHistry;
                gvDocHistory.DataBind();
            }
            else
            {
                gvDocHistory.DataSource = null;
                gvDocHistory.DataBind();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public string ConvertDegree(string input)
    {
        //"77 2 00.000W"; Sample Input from textBox1
        double sd = 0.0;
        double min = 0.0;
        double sec = 0.0;
        double deg = 0.0;
        string direction = input.Substring((input.Length - 1), 1);
        string sign = "";

        if ((direction.ToUpper() == "S") || (direction.ToUpper() == "W"))
        {
            sign = "-";
        }

        string[] arr = input.Split(new char[] { ' ' });
        min = Convert.ToDouble(arr[1]);
        string[] arr1 = arr[2].Split(new char[] { '.' });
        sec = Convert.ToDouble(arr1[0]);
        deg = Convert.ToDouble(arr[0]);
        min = min / ((double)60);
        sec = sec / ((double)3600);
        sd = deg + min + sec;

        if (!(string.IsNullOrEmpty(sign)))
        {
            sd = sd * (-1);
        }

        sd = Math.Round(sd, 6);
        string sdnew = Convert.ToString(sd);
        string sdnew1 = "";

        sdnew1 = string.Format("{0:0.000000}", sd);

        return sdnew1;
        //EXPECTED OUTPUT -77.03333
    }

    public DataTable FlightHourCalculation_Test(string StartAirport, string strEndAirport, string strTailNo, string TimeOfDeparture,
        string strSpeed, string strFlag)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);

        DataTable dtResul = new DataTable();
        LegDetails leg = new LegDetails();
        List<LegDetails> RouteListLeg = new List<LegDetails>();

        try
        {
            #region Fore Flight API

            string ForFlightID = "NTEST";
            string profileUUID = string.Empty;

            string windModel = string.Empty;
            string fixedWindComponent = string.Empty;
            string fixedIsaDeviation = string.Empty;
            string historicalProbability = string.Empty;

            DataTable dt = objRapidAPI.getFFTail(strTailNo);
            if (dt.Rows.Count > 0)
            {
                ForFlightID = dt.Rows[0]["TailID"].ToString();
                profileUUID = dt.Rows[0]["CruiseProfiles"].ToString();

                windModel = dt.Rows[0]["windModel"].ToString();
                fixedWindComponent = dt.Rows[0]["fixedWindComponent"].ToString();
                fixedIsaDeviation = dt.Rows[0]["fixedIsaDeviation"].ToString();
                historicalProbability = dt.Rows[0]["historicalProbability"].ToString();
            }


            string APIJsonstring = string.Empty;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://public-api.foreflight.com/public/api/flights/performance");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("x-api-key", "pT5PFZxv21jmN8lzZDhwPUDNHhpLwBZdiWz5NY72iR8=");

            string json = string.Empty;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //2020-09-10T16:40:17.776Z
                if (profileUUID == string.Empty)
                {
                    json = "{ \"flight\": { \"departure\": \"" + StartAirport + "\", \"destination\": \"" + strEndAirport + "\", \"scheduledTimeOfDeparture\": \"" + TimeOfDeparture + "\",  \"aircraftRegistration\": \"" + ForFlightID + "\" } }";
                }
                else
                {
                    json = "{ \"flight\": { \"departure\": \"" + StartAirport + "\", \"destination\": \"" + strEndAirport + "\", \"scheduledTimeOfDeparture\": \"" + TimeOfDeparture + "\",  \"aircraftRegistration\": \"" + ForFlightID + "\", \"cruiseProfileUUID\": \"" + profileUUID + "\", \"windOptions\": { \"windModel\": \"" + windModel + "\", \"fixedWindComponent\": " + fixedWindComponent + ", \"fixedIsaDeviation\": " + fixedIsaDeviation + ", \"historicalProbability\": " + historicalProbability + " } } }";
                }

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                APIJsonstring = streamReader.ReadToEnd();
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            JObject jObjPerf = JObject.Parse(APIJsonstring);
            string strPerform = jObjPerf["performance"].ToString();

            dynamic d = js.Deserialize<dynamic>(strPerform.ToString());

            var x = JObject.Parse(strPerform);
            var success = x["times"];
            double txnId = Convert.ToDouble(x["times"]["timeToDestinationMinutes"]); //totalTimeMinutes

            var success1 = x["distances"];
            double Distancenm = Convert.ToDouble(x["distances"]["destination"]);


            TimeSpan spWorkMin = TimeSpan.FromMinutes(txnId);
            string workHours = spWorkMin.ToString(@"hh\:mm");

            leg = new LegDetails();

            leg.legNo = " 0";
            leg.startICAO = StartAirport;
            leg.endICAO = strEndAirport;
            leg.distance_km = "";
            leg.distance_nm = Distancenm.ToString();
            leg.flight_time_min = txnId.ToString();
            leg.heading_deg = "";
            leg.heading_compass = "";

            RouteListLeg.Add(leg);

            dtResul = objRapidAPI.ToDataTable(RouteListLeg);

            #endregion
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

        return dtResul;
    }
}

public class GetAircraft
{
    public static DataTable getAircraft()
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);


        string APIJsonstring = string.Empty;
        var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://public-api.foreflight.com/public/api/aircraft");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "GET";
        httpWebRequest.Headers.Add("x-api-key", "pT5PFZxv21jmN8lzZDhwPUDNHhpLwBZdiWz5NY72iR8=");

        using (System.IO.Stream s = httpWebRequest.GetResponse().GetResponseStream())
        {
            using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
            {
                APIJsonstring = sr.ReadToEnd();
            }
        }




        JArray jsonArray = JArray.Parse(APIJsonstring);

        DataTable ParentData = new DataTable();
        ParentData.Columns.Add("aircraftRegistration");
        ParentData.Columns.Add("aircraftModelCode");
        ParentData.Columns.Add("cruiseProfiles");
        DataTable ChildData = new DataTable();
        ChildData.Columns.Add("aircraftRegistration");
        ChildData.Columns.Add("aircraftModelCode");
        ChildData.Columns.Add("uuid");
        ChildData.Columns.Add("profileName");



        foreach (var row in jsonArray)
        {
            var datarow = ParentData.NewRow();
            foreach (var jToken in row)
            {
                var jProperty = jToken as JProperty;
                if (jProperty == null) continue;
                datarow[jProperty.Name] = jProperty.Value.ToString();
            }
            ParentData.Rows.Add(datarow);
        }


        for (int z = 0; z < ParentData.Rows.Count; z++)
        {

            JArray jsonArray1 = JArray.Parse(ParentData.Rows[z][2].ToString());

            foreach (var row in jsonArray1)
            {
                var datarow = ChildData.NewRow();
                datarow[0] = ParentData.Rows[z][0].ToString();
                datarow[1] = ParentData.Rows[z][1].ToString();
                foreach (var jToken in row)
                {
                    var jProperty = jToken as JProperty;
                    if (jProperty == null) continue;
                    datarow[jProperty.Name] = jProperty.Value.ToString();
                }
                ChildData.Rows.Add(datarow);
            }
        }
        for (int i = 0; i < ChildData.Rows.Count; i++)
        {
            SaveAircraft(ChildData.Rows[i]["aircraftRegistration"].ToString(),
                ChildData.Rows[i]["aircraftModelCode"].ToString(), ChildData.Rows[i]["uuid"].ToString(), ChildData.Rows[i]["profileName"].ToString());

        }

        return ChildData;

    }

    public static void SaveAircraft(string AircraftReg, string AircraftModule, string uuid, string profileName)
    {
        SqlParameter[] SaveAircraftParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.QuoteConnectionString, "Aircraftprofiles_INS", false);
        SaveAircraftParm[0].Value = "INS";
        SaveAircraftParm[1].Value = AircraftReg;
        SaveAircraftParm[2].Value = AircraftModule;
        SaveAircraftParm[3].Value = uuid;
        SaveAircraftParm[4].Value = profileName;
        SqlHelper.ExecuteNonQuery(SqlHelper.QuoteConnectionString, CommandType.StoredProcedure, "Aircraftprofiles_INS", SaveAircraftParm);
    }

}