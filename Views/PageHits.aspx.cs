﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BusinessLayer;
using System.Data;
using System.IO;
using System.Web.Services;

public partial class Views_PageHits : System.Web.UI.Page
{
    public int linenum = 0;
    public string MethodName = string.Empty;

    protected BookedReportBLL objMember = new BookedReportBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Reports", "Page Hits");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Reports";
                lblSubHeader.InnerText = "Page Hits (Beta)";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                txtfromdate.Text = DateTime.Now.ToString("MMM d, yyyy");
                txttodate.Text = DateTime.Now.ToString("MMM d, yyyy");

                CalendarExtender1.EndDate = DateTime.Now;
                CalendarExtender2.EndDate = DateTime.Now;

                BindPageHits();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            BindPageHits();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    private void BindPageHits()
    {
        ClearData();

        string strRepFromDate = Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd");
        string strRepToDate = Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd");

        DataSet dtPageHit = objMember.Select_PageHit(strRepFromDate, strRepToDate);
        if (dtPageHit.Tables != null)
        {
            if (dtPageHit.Tables[0].Rows.Count > 0)
            {
                gvPageHits.DataSource = dtPageHit.Tables[0];
                gvPageHits.DataBind();

                btnExport.Visible = true;
            }

            if (dtPageHit.Tables[1].Rows.Count > 0)
            {
                lblTotalHits.Text = dtPageHit.Tables[1].Rows[0]["TotalHits"].ToString();
                lblIPHits.Text = dtPageHit.Tables[1].Rows[0]["IPHits"].ToString();
                lblRegionHits.Text = dtPageHit.Tables[1].Rows[0]["RegionHits"].ToString();
                lblUserCreated.Text = dtPageHit.Tables[1].Rows[0]["UserCreated"].ToString();
                lblNoofBookings.Text = dtPageHit.Tables[1].Rows[0]["NoofBookings"].ToString();
                lblTotalAmount.Text = "$" + Convert.ToDecimal(dtPageHit.Tables[1].Rows[0]["BookingAmount"].ToString()).ToString("N2");
            }

            Session["ChartData"] = dtPageHit.Tables[2];
        }

        DataTable dtBookings = objMember.PageHit_TopBookings(strRepFromDate, strRepToDate);
        if (dtBookings.Rows.Count > 0)
        {
            dlRecentBookings.Visible = true;
            lblNoTrips.Visible = false;

            dlRecentBookings.DataSource = dtBookings;
            dlRecentBookings.DataBind();
        }
        else
        {
            dlRecentBookings.Visible = false;
            lblNoTrips.Visible = true;
        }
    }

    private void ClearData()
    {
        gvPageHits.DataSource = null;
        gvPageHits.DataBind();

        lblTotalHits.Text = "0";
        lblIPHits.Text = "0";
        lblRegionHits.Text = "0";
        lblUserCreated.Text = "0";
        lblNoofBookings.Text = "0";
        lblTotalAmount.Text = "$" + Convert.ToDecimal("0").ToString("N2");

        btnExport.Visible = false;
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string strName = string.Empty;
            strName = "PageHits_" + DateTime.Now.ToString("ddMMMyyyy");

            string[] arrlblPageHits = { "lblSNo", "lblcountry", "lblregionName", "lblcity", "lbltimezone", "lblTotalHits", "lblUniqIPHits" };

            objMember.Exportexcel(gvPageHits, strName, arrlblPageHits, DateTime.Now.ToString("MMM/dd/yyyy"), lblUser.Text, "Page Hits");

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }

    protected void gvPageHits_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblcountry = (Label)gvPageHits.SelectedRow.FindControl("lblcountry");
            Label lblregionName = (Label)gvPageHits.SelectedRow.FindControl("lblregionName");
            Label lblcity = (Label)gvPageHits.SelectedRow.FindControl("lblcity");

            lblPopupHits.Text = ((Label)gvPageHits.SelectedRow.FindControl("lblTotalHits")).Text;
            lblPopUniqueHits.Text = ((Label)gvPageHits.SelectedRow.FindControl("lblUniqIPHits")).Text;

            string strRepFromDate = Convert.ToDateTime(txtfromdate.Text).ToString("yyyy-MM-dd");
            string strRepToDate = Convert.ToDateTime(txttodate.Text).ToString("yyyy-MM-dd");

            string country = lblcountry.Text.Split('(')[1].Replace(")", "").Trim();
            string regionName = lblregionName.Text.Split('(')[1].Replace(")", "").Trim();

            DataTable dtPopup = objMember.PageHit_GetPopup_Details(country, regionName, lblcity.Text, strRepFromDate, strRepToDate);
            if (dtPopup.Rows.Count > 0)
            {
                mpeDetailed.Show();

                gvDetailed.DataSource = dtPopup;
                gvDetailed.DataBind();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    [WebMethod]
    public static string GetSummary()  //Flight Hours
    {
        DataTable dtChartData = (DataTable)HttpContext.Current.Session["ChartData"];

        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;

        foreach (DataRow dr in dtChartData.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dtChartData.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }

        return serializer.Serialize(rows);
    }
}