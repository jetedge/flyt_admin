﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Configuration;

public partial class Views_TermsAndCondition : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.TermsAndConditionBLL objMember = new BusinessLayer.TermsAndConditionBLL();

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Tax Setup");
            }

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Terms And Condition";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();
            Clear();
            List();
        }
    }

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnHist.Click += new System.EventHandler(this.BtnHist_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
        this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    private void btnAdd_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            tblGrid.Visible = false;
            tblMain.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }
    private void btnBack_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            List();
            tblGrid.Visible = true;
            tblMain.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        string FilePath = string.Empty;
        try
        {
            AddTerms();
            string retVal = objMember.Save(objMember);
            if (retVal != "-1")
            {
                string fileUrl = CommanClass.GetUrl(HttpContext.Current.Request.Url.Host) + "/EMPTYLEG/TermsAndCondition/" + @"/" + retVal + @"/";

                FilePath = fileUrl;

                if (FilePath != "" && FilePath != string.Empty)
                {
                    bool exists = Directory.Exists(FilePath.ToString());
                  
                    if (!exists)
                    {
                        Directory.CreateDirectory(FilePath.ToString());
                    }
                    
                    if (fuFile.HasFile)
                    {
                        fuFile.SaveAs(FilePath + @"\" + fuFile.FileName.ToString().Replace("(", "").Replace(")", "").Trim());

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE TermsAndCondition SET FileName='" + fuFile.FileName.ToString().Replace("(", "").Replace(")", "").Trim() + "',FilePath='" + FilePath.ToString().Trim() + "' WHERE RowId='" + retVal.ToString().Trim() + "' ");
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE TermsAndCondition SET FileName='" + lblFileName.Text.ToString().Replace("(", "").Replace(")", "").Trim() + "',FilePath='" + lblFilePath.Text.ToString().Trim() + "' WHERE RowId='" + retVal.ToString().Trim() + "' ");
                    }
                }
                lblalert.Text = "Terms and Condition details have been saved successfully";
                mpealert.Show();
                Clear();
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request";
                mpealert.Show();
            }
            List();
            tblGrid.Visible = true;
            tblMain.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }
    protected void imgFUF206Download1_Click(object sender, EventArgs e)
    {
        try
        {
            string strExtension = System.IO.Path.GetExtension(lblFilePath.Text + @"/" + lblFileName.Text);
            if (strExtension == ".pdf")
            {
                System.IO.FileInfo file = new System.IO.FileInfo(lblFilePath.Text + @"/" + lblFileName.Text);
                if (file.Exists)
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                    HttpContext.Current.Response.ContentType = "application/ms-excel";
                    HttpContext.Current.Response.WriteFile(file.FullName);
                    HttpContext.Current.Response.End();

                }
            }
            else
            {
                System.IO.FileInfo file = new System.IO.FileInfo(lblFilePath.Text + @"/" + lblFileName.Text);
                if (file.Exists)
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                    HttpContext.Current.Response.ContentType = "application/ms-excel";
                    HttpContext.Current.Response.WriteFile(file.FullName);
                    HttpContext.Current.Response.End();
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }

    }
    protected void img206Form_Delete(object sender, EventArgs e)
    {
        try
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update TermsAndCondition SET FilePath='', FileName='' WHERE Rowid='" + hdfPSRID.Value.Trim() + "'");


            bool exists = Directory.Exists(lblFilePath.Text + @"/" + lblFileName.Text);
            if (exists)
            {
                Directory.CreateDirectory(lblFilePath.Text + @"/" + lblFileName.Text);
            }
            if (lblFilePath.Text != null && lblFilePath.Text != string.Empty)
            {
                File.Delete(lblFilePath.Text + @"/" + lblFileName.Text);
            }
            imgFUF206Download1.Visible = false;
            img206Form.Visible = false;
            lblFileName.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void BtnHist_Click(object sender, EventArgs e)
    {
        try
        {
            mpeHistory.Show();
            History();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public void List()
    {
        DataSet dt = objMember.Select();
        if (dt.Tables[0].Rows.Count > 0)
        {
            hdfPSRID.Value = dt.Tables[0].Rows[0]["RowID"].ToString();
            txtVersionNo.Text = dt.Tables[0].Rows[0]["Version"].ToString();
            lblVersion.Text = dt.Tables[0].Rows[0]["Version"].ToString();
            txtEffFrom.Text = dt.Tables[0].Rows[0]["EffFrom"].ToString();
            lblEfFrom.Text = dt.Tables[0].Rows[0]["EffFrom"].ToString();

            if (lblEfFrom.Text.Length > 0)
            {
                lblEfFrom.Text = Convert.ToDateTime(lblEfFrom.Text).ToString("MMM dd, yyyy");
            }
            txtNotes.Text = dt.Tables[0].Rows[0]["Notes"].ToString();
            lblNotes.Text = dt.Tables[0].Rows[0]["Notes"].ToString();

            string strFileName = dt.Tables[0].Rows[0]["FileName"].ToString();
            string strFilePath = dt.Tables[0].Rows[0]["FilePath"].ToString();


            string strCreatedBy = dt.Tables[0].Rows[0]["CreatedByName"].ToString();
            string strcreatdOn = dt.Tables[0].Rows[0]["CreatedOn"].ToString();

            if (strFileName.Length > 0 && strFilePath.Length > 0)
            {

                lblFilePath.Text = dt.Tables[0].Rows[0]["FilePath"].ToString();
                lblFileName.Text = dt.Tables[0].Rows[0]["FileName"].ToString();
                imgFUF206Download1.Visible = true;
                img206Form.Visible = true;
                lblFileName.Visible = true;
                int Width = Request.Browser.ScreenPixelsWidth * 2 - 100;
                int Height = Request.Browser.ScreenPixelsHeight * 2 - 370;
                string ACDocPathView = SecretsBLL.TCUrl.ToString();
                pdfIFrame.Attributes.Add("src", ACDocPathView + hdfPSRID.Value + @"\" + lblFileName.Text + "#view=FitH");
                pdfIFrame.Attributes.Add("style", "height:" + Height + "px; width: 100%; background: white; border: 0;");
            }
            else
            {

                imgFUF206Download1.Visible = false;
                img206Form.Visible = false;
                lblFileName.Visible = false;

            }
            if (strCreatedBy.Length > 0 && strcreatdOn.Length > 0)
            {
                divLMBy.Visible = true;
                lblLMby.Text = strCreatedBy + " ( on ) " + Convert.ToDateTime(strcreatdOn).ToString("MMM dd, yyyy hh:mm tt").ToUpper();
            }
            else
            {
                divLMBy.Visible = false;
            }
        }
        else
        {

            imgFUF206Download1.Visible = false;
            img206Form.Visible = false;
            lblFileName.Visible = false;
            divLMBy.Visible = false;
            lblVersion.Text = "N/A";
            lblEfFrom.Text = "N/A";
            lblNotes.Text = "N/A";
        }
    }
    protected void imgDownload_onClick(object sender, EventArgs e)
    {
        try
        {
            ImageButton imb = (ImageButton)sender;
            GridViewRow row = (GridViewRow)imb.Parent.Parent;

            string filePAth = ((Label)row.FindControl("lblFilePath")).Text.Trim();

            string fileName = ((Label)row.FindControl("lblFileName")).Text.Trim();

            System.IO.FileInfo file = new System.IO.FileInfo(filePAth + fileName);
            if (file.Exists)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                HttpContext.Current.Response.WriteFile(file.FullName);
                HttpContext.Current.Response.End();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void History()
    {
        DataTable dtHist = objMember.History("INTAX", "");
        if (dtHist.Rows.Count > 0)
        {
            gvHistory.DataSource = dtHist;
            gvHistory.DataBind();
        }
        else
        {
            gvHistory.DataSource = null;
            gvHistory.DataBind();
        }
    }
    public void AddTerms()
    {
        objMember.ManageType = "I";
        objMember.EffectiveFrom = txtEffFrom.Text.Trim();
        objMember.RowId = hdfPSRID.Value;
        objMember.Version = txtVersionNo.Text.Trim();
        objMember.Notes = txtNotes.Text.Trim();
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"];
    }

    public void Clear()
    {
        hdfPSRID.Value = "0";
        txtVersionNo.Text = string.Empty;
        txtEffFrom.Text = string.Empty;
        txtNotes.Text = string.Empty;
        lblFileName.Text = string.Empty;
        lblFilePath.Text = string.Empty;

        imgFUF206Download1.Visible = false;
        img206Form.Visible = false;
        lblFileName.Visible = false;
        divLMBy.Visible = false;
    }
}