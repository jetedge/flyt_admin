﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    Debug="true" CodeFile="BookedReportV1.aspx.cs" Inherits="Views_BookedReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-xl-12" style="border-bottom: 6px solid #f2f3f7;">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row m-0">
                        <div class="col-lg-12 p-0">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 11%; text-align: center">
                                    Tail Number
                                </label>
                                <div class="col-2" style="max-width: 11%;">
                                    <asp:TextBox ID="txtTailFilter" class="cusinputuser" placeholder="Enter tail #" runat="server"
                                        Width="100%" AutoPostBack="true" OnTextChanged="textBox1_TextChanged"></asp:TextBox>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    Trip Number
                                </label>
                                <div class="col-2" style="max-width: 11%;">
                                    <asp:TextBox ID="txttripno" class="cusinputuser" placeholder="Enter trip #" runat="server"
                                        Width="100%" AutoPostBack="true" OnTextChanged="txttripno_TextChanged"></asp:TextBox>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 10%;">
                                    From Date<span style="color: Red">*</span>
                                </label>
                                <div class="col-2" style="max-width: 12%;">
                                    <asp:TextBox ID="txtfromdate" class="cusinputuser" Placeholder="Date(MM-DD-YYYY)"
                                        onkeypress="return NumericsIphon(event)" runat="server" Width="98%" AutoPostBack="true"
                                        OnTextChanged="txtfromdate_changed"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtfromdate"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 8%;">
                                    To Date<span style="color: Red">*</span>
                                </label>
                                <div class="col-2" style="max-width: 12%;">
                                    <asp:TextBox ID="txttodate" class="cusinputuser" Placeholder="Date(MM-DD-YYYY)" onkeypress="return NumericsIphon(event)"
                                        runat="server" Width="98%" AutoPostBack="true" OnTextChanged="txtto_changed"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttodate"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                </div>
                                <div class="col-2" style="padding-top: 5px; max-width: 12%;">
                                    <asp:Button ID="Button2" runat="server" Style="display: none;" Text="Export To Excel"
                                        CssClass="btn btn-danger" OnClick="btnExport_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="dvGrid" runat="server">
        <div class="col-lg-12">
            <div class="card card-custom card-stretch gutter-b">
                <div class="card-body p-3">
                    <asp:DataList ID="dlCurrent" RepeatDirection="Horizontal" RepeatColumns="1" runat="server"
                        Width="100%" FooterStyle-HorizontalAlign="left" OnSelectedIndexChanged="dlCurrent_SelectedIndexChanged">
                        <ItemTemplate>
                            <div class="kt-widget5__item">
                                <div class="row m-0 pb-2 mb-2" style="border-bottom: 1px dashed #9E9E9E;">
                                    <div class="col-lg-2 col-md-3 col-xs-12 col-sm-12 p-0">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                <span class="kt-widget5__title">Tail Number : </span><span class="kt-font-info">
                                                    <asp:Label ID="lblTailNo" runat="server" Text='<%# Eval("TailNo") %>' />
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                <span class="kt-widget5__title">Trip Number : </span><span class="kt-font-info">
                                                    <asp:Label ID="lblTripno" runat="server" Text='<%# Eval("Tripno") %>' />
                                                </span>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label">
                                                Trip Date : <span class="kt-font-info">
                                                    <asp:Label ID="Label15" runat="server" Font-Size="14px" Text='<%# Eval("FromDate") %>' />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 p-0">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                <span class="kt-widget5__title">Trip Reference Number : </span><span class="kt-font-info">
                                                    <asp:Label ID="Label1" Font-Bold="true" Style="letter-spacing: 0.5px;" runat="server"
                                                        Text='<%# Eval("TransactionReference") %>' /></span>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                Booked For : <span class="kt-font-info">
                                                    <asp:Label ID="lblCFirstName" runat="server" Text='<%# Eval("CFirstName") %>' />
                                                </span>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label">
                                                Booking Placed : <span class="kt-font-info">
                                                    <asp:Label ID="lblBookingDate" runat="server" Font-Size="14px" Text='<%# Eval("TransDate", "{0:MMMM dd, yyyy}") %>' />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 p-0">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                Total : <span class="kt-font-info">
                                                    <asp:Label ID="Label10" runat="server" Text='<%# "$ " + Eval("TotalAmount","{0:N2}") %>' />
                                                </span>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                Departure : <span class="kt-font-info">
                                                    <asp:Label ID="lblFromICAODisp" runat="server" Text='<%# Eval("FromICAODisp") %>' />
                                                    <asp:Label ID="lblFromTime" runat="server" Text='<%# Eval("FromTime") %>' />
                                                </span>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                Arrival : <span class="kt-font-info">
                                                    <asp:Label ID="lblToICAODisp" runat="server" Text='<%# Eval("ToICAODisp") %>' />
                                                    <asp:Label ID="lblToTime" runat="server" Text='<%# Eval("ToTime") %>' />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 p-0" style="align-items: center;
                                        align-self: center;">
                                        <div class="row" style="width: 100%;">
                                            <div class="p-0 col-lg-3 col-md-3 col-xs-12 col-sm-12 col-form-label text-center" style="align-items: center;
                                                align-self: center;">
                                                <asp:ImageButton ImageUrl="~/Images/pdf-icon.png" Style="height: 60px; width: 60px;"
                                                    data-toggle="tooltip" data-placement="right" data-container="body" data-boundary="window"
                                                    title="Click to Download Invoice" CommandName="Select" ID="imbInvDownload" runat="server" />
                                                <asp:ImageButton ImageUrl="~/Images/pdf-icon-signed.png" Style="height: 60px; width: 60px;"
                                                    data-toggle="tooltip" data-placement="right" data-container="body" data-boundary="window"
                                                    title="Click to Download Signed Invoice" Visible="false" ID="imgSignedFile" runat="server"
                                                    OnClick="imgSignedFile_Click" />
                                            </div>
                                            <div class="p-0  col-lg-9 col-md-9 col-xs-12 col-sm-12 col-form-label" style="align-self: center;">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                                        <label class="mb-0">
                                                            eSign Status :
                                                            <asp:Label ID="lbleSignStatus" Font-Bold="true" ForeColor="Red" runat="server" Text="Pending" />
                                                        </label>
                                                        <asp:LinkButton ID="btnCheckStatus" OnClick="btnCheckStatus_Click" runat="server"
                                                            Style="background: transparent; margin-left: 1rem;" ToolTip="Click to check eSign Status"
                                                            CausesValidation="false" CssClass="btn btn-light p-0">
                                                                            <i class="flaticon2-refresh" style="color: Black;"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 col-form-label" style="letter-spacing: 0;">
                                                        <span class="kt-widget5__title">Expiry Time : </span><span class="kt-font-info">
                                                            <asp:Label ID="lblExpiryTime" runat="server" />
                                                            <asp:Label ID="lblRowID" Visible="false" runat="server" Text='<%# Eval("RowID") %>' />
                                                            <asp:LinkButton ID="lblInvoicenumber" OnClick="lblInvoicenumber_Click" Font-Bold="true"
                                                                runat="server" Text='<%# Eval("Invoicenumber") %>' Visible="false" /></span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" id="dvSignedBy" runat="server"
                                                        visible="false">
                                                        <label>
                                                            Signed By :
                                                            <asp:Label ID="lblSignedBy" runat="server" Text="NA" />
                                                        </label>
                                                        <br />
                                                        <label>
                                                            Signed On :
                                                            <asp:Label ID="lblSignedOn" runat="server" Text="NA" />
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-right">
                                                        <asp:Button ID="btnSendMail" OnClick="btnSendMail_Click" runat="server" Text="Click to ReSend Mail"
                                                            ToolTip="Click to ReSend Mail" CssClass="btn btn-danger" CausesValidation="false">
                                                        </asp:Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                    <label id="lblNoTrips" runat="server" visible="false" style="font-size: 1.2rem;">
                        No Trips details found.
                    </label>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btntrans" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="mpetrans" runat="server" TargetControlID="btntrans" PopupControlID="pnltrans"
        BackgroundCssClass="modalBackground" Y="50" CancelControlID="btncancelpopup">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnltrans" runat="server" CssClass="modalPopup" BackColor="#111820"
        Style="display: none; float: right; align: right; color: White; width: 40%; padding: 12px;
        position: fixed">
        <table style="width: 100%;">
            <tr style="padding-bottom: 20px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td style="color: White; font-size: 16px; vertical-align: middle">
                    Booking Details
                </td>
                <td style="margin-right: 2%; padding-right: 2%; color: White; font-weight: bold;
                    padding-bottom: 10px; padding-top: 10px; border-color: none; font-size: larger;
                    vertical-align: middle" align="right" colspan="5">
                    <asp:LinkButton ID="btncancelpopup" runat="server" Style="color: rgb(255,255,255);
                        text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label5" Text="Invoice Number" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblInvoiceNumber" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label11" Text="Trip Reference Number" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblTransRefNumber" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label7" Text="Booked For" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblbookedname" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label8" Text="Booked Date" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblbookeddate" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label3" Text="Transaction Status" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lbltransaction" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label9" Text="Transaction ID" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lbltransid" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label4" Text="Amount" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblamount" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label10" Text="Mobile Number" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblmblno" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" BehaviorID="mpealert" runat="server" TargetControlID="btnalert"
        PopupControlID="pnlalert" BackgroundCssClass="modalBackground" Y="95" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 14px">
        <table style="width: 100%;">
            <tr>
                <td valign="middle" align="left">
                    <asp:Label ID="lblalert" runat="server" Style="line-height: 25px; letter-spacing: 0.5px;
                        color: White; font-size: 15px;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Button ID="btncancelalert" runat="server" CssClass="btn btn-danger" CausesValidation="false"
                        Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblEnvelopBy" runat="server" Visible="false" />
    <asp:Label ID="lblEnvelopOn" runat="server" Visible="false" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
</asp:Content>
