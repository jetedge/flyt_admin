﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;

public partial class Views_TaxSetup : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.TaxSetupBLL objMember = new BusinessLayer.TaxSetupBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Tax Setup");
            }
            

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Tax Setup";



            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
            hdfPSRID.Value = "0";
            btnAdd.Focus();
            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnAddAlaska.Click += new System.EventHandler(this.btnAlaskaNew_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    #region Button Events

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            AddTax();
            int retVal = objMember.Save(objMember);
            if (retVal > 0)
            {
                lblalert.Text = "Tax Details have been saved successfully";
                mpealert.Show();
                btnAdd.Focus();
                btnViewOnClick(sender, e);
                Clear();
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request";
                mpealert.Show();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            txtcode.Focus();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            hdfTaxFlag.Value = "TAX";
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnAlaskaNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            txtcode.Focus();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            hdfTaxFlag.Value = "INTAX";
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();

            if (gvdomestic.Rows.Count > 0)
            {
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
            }
            else
            {
                tblForm.Visible = true;
                tblGrid.Visible = false;
                btnAdd.Visible = false;
                btnSave.Visible = true;
                btnView.Visible = true;
            }

            btnAdd.Focus();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }


    protected void gvdiscount_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvdomestic.SelectedRow.FindControl("lblrowid");

            DataTable dtCD = objMember.Edit(lblrowid.Text.Trim());

            if (dtCD != null && dtCD.Rows.Count > 0)
            {
                hdfCDRID.Value = lblrowid.Text;
                txtcode.Text = dtCD.Rows[0]["Code"].ToString();
                txtName.Text = dtCD.Rows[0]["Name"].ToString();
                txtdesc.Text = dtCD.Rows[0]["Description"].ToString();
                txtEDate.Text = Convert.ToDateTime(dtCD.Rows[0]["Effectivefrom"].ToString()).ToString("yyyy-MM-dd"); ;
                txtnotes.Text = dtCD.Rows[0]["notes"].ToString();
                if (dtCD.Rows[0]["Active"].ToString() == "1")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }
                if (dtCD.Rows[0]["pflag"].ToString() == "$")
                {
                    ddlDiscounttype.SelectedIndex = 0;
                    txtRPercent.Text = dtCD.Rows[0]["fees"].ToString();
                }
                else
                {
                    ddlDiscounttype.SelectedIndex = 1;
                    txtRPercent.Text = dtCD.Rows[0]["Tax"].ToString();
                }
            }
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            hdfTaxFlag.Value = "TAX";
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvdiscountInternal_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvinterntional.SelectedRow.FindControl("lblrowid");

            DataTable dtCD = objMember.Edit(lblrowid.Text.Trim());

            if (dtCD != null && dtCD.Rows.Count > 0)
            {
                hdfCDRID.Value = lblrowid.Text;
                txtcode.Text = dtCD.Rows[0]["Code"].ToString();
                txtName.Text = dtCD.Rows[0]["Name"].ToString();
                txtdesc.Text = dtCD.Rows[0]["Description"].ToString();
                txtEDate.Text = Convert.ToDateTime(dtCD.Rows[0]["Effectivefrom"].ToString()).ToString("yyyy-MM-dd"); ;
                txtnotes.Text = dtCD.Rows[0]["notes"].ToString();
                if (dtCD.Rows[0]["Active"].ToString() == "1")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }
                if (dtCD.Rows[0]["pflag"].ToString() == "$")
                {
                    ddlDiscounttype.SelectedIndex = 0;
                    txtRPercent.Text = dtCD.Rows[0]["Tax"].ToString();
                }
                else
                {
                    ddlDiscounttype.SelectedIndex = 1;
                    txtRPercent.Text = dtCD.Rows[0]["Tax"].ToString();
                }
            }
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            hdfTaxFlag.Value = "INTAX";
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void imbHistoryIntl(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblCode = (Label)gvrow.FindControl("lblCode");

            DataTable dtHist = objMember.History("INTAX", lblCode.Text);

            if (dtHist.Rows.Count > 0)
            {
                gvHistory.DataSource = dtHist;
                gvHistory.DataBind();
            }
            else
            {
                gvHistory.DataSource = null;
                gvHistory.DataBind();
            }
            mpeHistory.Show();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void imbHistoryDomestic(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblCode = (Label)gvrow.FindControl("lblCode");

            DataTable dtHist = objMember.History("TAX", lblCode.Text);

            if (dtHist.Rows.Count > 0)
            {
                gvHistory.DataSource = dtHist;
                gvHistory.DataBind();
            }
            else
            {
                gvHistory.DataSource = null;
                gvHistory.DataBind();
            }

            mpeHistory.Show();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion

    #region  User Defined Function

    public void AddTax()
    {
        if (hdfCDRID.Value == "0")
        {
            objMember.ManageType = "I";
        }
        else
        {
            objMember.ManageType = "I";
        }
        objMember.EffectiveFrom = txtEDate.Text.Trim();
        objMember.RowId = hdfCDRID.Value;
        objMember.Code = txtcode.Text.Trim();
        objMember.Name = txtName.Text.Trim();
        objMember.Description = txtdesc.Text.Trim();
        objMember.Notes = txtnotes.Text.Trim();
        if (ddlDiscounttype.SelectedIndex == 0)
        {
            objMember.Tax = txtRPercent.Text.Trim();
            objMember.PercentageSymbol = ddlDiscounttype.SelectedValue;
        }
        else
        {
            objMember.Tax = txtRPercent.Text.Trim();

            objMember.PercentageSymbol = ddlDiscounttype.SelectedValue;
        }
        if (chkActive.Checked == true)
        {
            objMember.Active = "1";
        }
        else
        {
            objMember.Active = "0";
        }
        objMember.TaxFlag = hdfTaxFlag.Value.Trim();
    }

    public void List()
    {
        DataSet dt = objMember.Select();
        if (dt.Tables[0].Rows.Count > 0)
        {
            gvdomestic.DataSource = dt.Tables[0];
            gvdomestic.DataBind();
        }
        else
        {
            gvdomestic.DataSource = null;
            gvdomestic.DataBind();
        }

        if (dt.Tables[1].Rows.Count > 0)
        {
            gvinterntional.DataSource = dt.Tables[1];
            gvinterntional.DataBind();
        }
        else
        {
            gvinterntional.DataSource = null;
            gvinterntional.DataBind();
        }
    }

    public void Clear()
    {
        hdfCDRID.Value = "0";
        txtcode.Text = string.Empty;
        txtdesc.Text = string.Empty;
        txtName.Text = string.Empty;
        txtEDate.Text = string.Empty;
        txtRPercent.Text = string.Empty;
        txtnotes.Text = string.Empty;
        hdfCDRID.Value = "0";
    }


    #endregion
}