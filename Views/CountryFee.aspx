﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="CountryFee.aspx.cs" Inherits="Views_CountryFee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style>
        #MainContent_gvCountryFee tbody tr td
        {
            padding-top: 7px;
            padding-bottom: 12px;
        }
        #MainContent_gvCountryFee tbody tr td tbody tr td
        {
            padding-top: 1px;
            padding-bottom: 5px;
            border: none !important;
            padding-left: 7px;
        }
        #MainContent_gvCountryFee tbody tr th
        {
            padding-top: 0.1rem;
            padding-bottom: 0.1rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="kt-portlet">
                            <div class="form-group row">
                                <div class="col-xl-12 text-right" style="padding-right: 2%;">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add New" CssClass="btn btn-danger" TabIndex="1" />
                                    <label id="lblMan" runat="server" class="importantlabels" visible="false">
                                        <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                            ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                                    </label>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                        ValidationGroup="SP" />
                                    <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                                        OnClick="btnViewOnClick" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                        runat="server" ShowSummary="false" />
                                </div>
                            </div>
                            <div class="row m-0" id="tblGrid" runat="server">
                                <div class="col-xl-12 table-responsive boxshadow">
                                    <asp:GridView ID="gvCountryFee" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                        Width="100%" AutoGenerateColumns="false" OnSelectedIndexChanged="gvPricingDetails_SelectedIndexChanged"
                                        PageSize="50" AllowPaging="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    <asp:Label ID="lblrowid" Text='<%# Eval("RowID") %>' runat="server" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                                ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCountry" Text='<%# Eval("Country_Name") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    <table width="100%" style="border: 0px;">
                                                        <tr>
                                                            <td align="center" style="width: 20% !important; text-align: left">
                                                                <asp:Label ID="Head1" runat="server" Text="Fee Name"></asp:Label>
                                                            </td>
                                                            <td align="center" style="width: 15% !important; text-align: left">
                                                                <asp:Label ID="Head2" runat="server" Text="Amount"></asp:Label>
                                                            </td>
                                                            <td align="center" style="width: 25% !important; text-align: left">
                                                                <asp:Label ID="Label2" runat="server" Text="Apply For"></asp:Label>
                                                            </td>
                                                            <td align="center" style="width: 20% !important; text-align: center">
                                                                <asp:Label ID="Label3" runat="server" Text="Effective From"></asp:Label>
                                                            </td>
                                                            <td align="center" style="width: 20% !important; text-align: center">
                                                                <asp:Label ID="Label4" runat="server" Text="Discontinue Date"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:DataList ID="rptFeeDetails" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                                        Width="100%" Style="border: none;">
                                                        <ItemTemplate>
                                                            <table width="100%" style="border: none !important;">
                                                                <tr>
                                                                    <td style="border: none !important; width: 20%; text-align: left">
                                                                        <asp:Label ID="lblSetupBy" runat="server" Text='<%# Bind("FeeName") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="border: none !important; width: 15%; text-align: left">
                                                                        <asp:Label ID="lblAirports" runat="server" Text='<%# Eval("Amount","{0:C0}") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="border: none !important; width: 25%; text-align: left">
                                                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("ApplyForFlag") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="border: none !important; width: 20%; text-align: center">
                                                                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("EffFrom") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="border: none !important; width: 20%; text-align: center">
                                                                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("DiscontinueDate") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="7%"
                                                ItemStyle-Width="7%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActive" Text='<%# Eval("Active") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                                ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreatedby" Text='<%# Eval("CreatedBy") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%"
                                                ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreatedOn" Text='<%# Eval("CreatedOn") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                                ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbView1" runat="server" TabIndex="2" ImageUrl="~/Images/pencil_Edit.png"
                                                        CommandName="Select" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="3%"
                                                ItemStyle-Width="3%" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbDelete" runat="server" TabIndex="3" ImageUrl="~/Images/icon_delete.png"
                                                        CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div id="tblForm" runat="server" visible="false">
                                <div class="row">
                                    <div class="col-xl-1">
                                    </div>
                                    <div class="col-xl-10">
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label" style="max-width: 11%;">
                                                Country <span class="MandatoryField">* </span>
                                            </label>
                                            <div class="col-3">
                                                <asp:DropDownList ID="ddlCountry" runat="server" TabIndex="4" class="cusinputuser">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlCountry"
                                                    InitialValue="0" Display="None" ErrorMessage="Country is required." SetFocusOnError="True"
                                                    ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                            </div>
                                            <label class="col-2 col-form-label" style="max-width: 11%;">
                                                Active
                                            </label>
                                            <div class="col-3" style="padding-top: 9px;">
                                                <asp:RadioButtonList ID="rblActive" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div class="col-2 text-right">
                                                <asp:Button ID="btnAddFee" runat="server" Text="Add Fee" CssClass="btn btn-danger"
                                                    TabIndex="1" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-1">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-1">
                                    </div>
                                    <div class="col-xl-10">
                                        <asp:GridView ID="gvFeeList" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                            OnSelectedIndexChanged="gvFeeList_SelectedIndexChanged" Width="100%" AutoGenerateColumns="false"
                                            PageSize="50" AllowPaging="True" OnRowDeleting="gvFeeList_RowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fee Name" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%"
                                                    ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFeeName" Text='<%# Eval("FeeName") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                                    ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" Text='<%# Eval("Amount","{0:N0}") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Apply For" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                                    ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblApplyFor" Text='<%# Eval("ApplyFor") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblApplyForFlag" Text='<%# Eval("ApplyForFlag") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Effective From" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="12%" ItemStyle-Width="12%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEFrom" Text='<%# Eval("EffFrom") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Discontinue Date" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="12%" ItemStyle-Width="12%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDisDate" Text='<%# Eval("DiscontinueDate") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                                    ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbView1" runat="server" TabIndex="2" ImageUrl="~/Images/pencil_Edit.png"
                                                            CommandName="Select" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="~/Images/icon_delete.png"
                                                            CommandName="Delete" CausesValidation="false" AlternateText="Delete Tail details" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="col-xl-1">
                                    </div>
                                </div>
                                <div class="form-group row">
                                </div>
                                <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                    <div class="col-xl-1">
                                    </div>
                                    <div class="col-xl-10">
                                        <div style="float: left; padding-top: 10px; padding-left: 15px">
                                            <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                                font-size: 15px; font-weight: 600;"></asp:Label>
                                            <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600; font-size: 15px"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-xl-1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnUserPre" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeFeeDetails" runat="server" TargetControlID="btnUserPre"
        PopupControlID="pnlFeeDetails" BackgroundCssClass="modalBackground" Y="80" CancelControlID="PrefClose">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlFeeDetails" runat="server" Style="display: none; background-color: rgb(17,24,32);
        width: 50%; padding: 10px">
        <table width="100%" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="form-group row">
                        <label class="col-6 col-form-label" style="color: White;">
                            <h5 style="color: White; margin-left: 0%; font-size: 18px">
                                Fee Details</h5>
                        </label>
                        <div class="col-6 text-right">
                            <asp:LinkButton ID="PrefClose" runat="server" Style="color: rgb(255,255,255); float: right;
                                text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4 col-form-label" style="color: White;">
                            Fee Name <span class="MandatoryField">* </span>
                        </label>
                        <div class="col-8">
                            <asp:TextBox ID="txtFeeName" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFeeName"
                                Display="None" ErrorMessage="Fee Name is required." SetFocusOnError="True" ValidationGroup="TR">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4 col-form-label" style="color: White;">
                            Amount <span class="MandatoryField">* </span>
                        </label>
                        <div class="col-8">
                            <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" TabIndex="6" MaxLength="100"
                                autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FT" runat="server" TargetControlID="txtAmount" ValidChars="0123456789.">
                            </asp:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAmount"
                                Display="None" ErrorMessage="Amount is required." SetFocusOnError="True" ValidationGroup="TR">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-4 col-form-label" style="color: White;">
                            Apply For
                        </label>
                        <div class="col-8" style="padding-top: 9px;">
                            <asp:RadioButtonList ID="rblApplyFor" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
                                Style="color: White;">
                                <asp:ListItem Text="Both" Value="B" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Departure" Value="D"></asp:ListItem>
                                <asp:ListItem Text="Arrival" Value="A"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4 col-form-label" style="padding-right: 0px; color: White;">
                            Effective From <span class="MandatoryField">* </span>
                        </label>
                        <div class="col-8">
                            <asp:TextBox ID="txtEffFrom" runat="server" CssClass="form-control" TabIndex="7"
                                autocomplete="off" MaxLength="100" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEffFrom"
                                Format="MM-dd-yyyy" Enabled="True">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEffFrom"
                                Display="None" ErrorMessage="Effective From is required." SetFocusOnError="True"
                                ValidationGroup="TR">*</asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-4 col-form-label" style="color: White;">
                            Discontinue Date
                        </label>
                        <div class="col-8">
                            <asp:TextBox ID="txtDisContinueDate" runat="server" CssClass="form-control" TabIndex="8"
                                MaxLength="3" autocomplete="off" ToolTip="Enter all characters. Length is limited to 3"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDisContinueDate"
                                Format="MM-dd-yyyy" Enabled="True">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                    <div class="form-group row">
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <asp:Button ID="btnAddtoList" runat="server" Text="Add to List" CssClass="btn btn-danger"
                                TabIndex="1" ValidationGroup="TR" OnClick="btnAddLegOnClick" />
                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="TR" ShowMessageBox="true"
                                runat="server" ShowSummary="false" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="lblMessage" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
