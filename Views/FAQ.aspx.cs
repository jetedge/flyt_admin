﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class Views_FAQ : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.FAQBLL objMember = new BusinessLayer.FAQBLL();

    #endregion

    #region Pageload
    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "FAQ");
            }


            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "Admin";
            lblSubHeader.InnerText = "FAQ";


            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();


            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
            hdfPSRID.Value = "0";
            btnAdd.Focus();
            GetTopic();
            List();
            tblGrid.Visible = true;
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.btnSaveExternal.Click += new System.EventHandler(this.btnSaveTopic_Click);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    #region Button Events

    protected void gvFAQ_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFAQ.PageIndex = e.NewPageIndex;
            List();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            AddAirport();

            string retVal = objMember.Save(objMember);

            if (retVal != "0" && retVal != "-1")
            {
                //string strSQ = "0";

                //DataTable dt = genclass.GetDataTable("SELECT * FROM FAQ where  Rowid!='" + retVal.ToString().Trim() + "' and Topic='" + ddlTopic.SelectedValue.Trim() + "' order by DisplaySequence asc");
                //int m = 1;
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    strSQ = (Convert.ToInt32(strSQ) + 1).ToString();
                //    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE FAQ  set DisplaySequence='" + strSQ + "' where Rowid='" + dt.Rows[i]["Rowid"].ToString() + "' and Topic='" + ddlTopic.SelectedValue.Trim() + "'");
                //    strSQ = strSQ;
                //}

                lblalert.Text = "FAQ Details have been saved successfully";
                mpealert.Show();
                btnAdd.Focus();
                btnViewOnClick(sender, e);
                Clear();
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request";
                mpealert.Show();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnSaveTopic_Click(object sender, System.EventArgs e)
    {
        try
        {
            AddTopic();

            int retVal = objMember.SaveTopic(objMember);

            if (retVal > 0)
            {
                lblalert.Text = "Topic Details have been saved successfully";
                mpealert.Show();
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request";
                mpealert.Show();
            }

            GetTopic();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            dvMand.Visible = true;
            DataTable dt = genclass.GetDataTable("select ISNULL(Max(DisplaySequence),0) +1  as SEQ from FAQ");
            if (dt.Rows.Count > 0)
            {
                txtSeq.Text = dt.Rows[0]["SEQ"].ToString();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();

            if (gvFAQ.Rows.Count > 0)
            {
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
                dvMand.Visible = false;
            }
            else
            {
                tblForm.Visible = true;
                tblGrid.Visible = false;
                btnAdd.Visible = false;
                btnSave.Visible = true;
                btnView.Visible = true;
                dvMand.Visible = true;
            }

            btnAdd.Focus();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From FAQ WHERE Rowid='" + lblrowtodelete.Text.Trim() + "'");
            List();
            lblalert.Text = "Faq Details has been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvFAQ_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            hdfPSRID.Value = ((Label)gvFAQ.SelectedRow.FindControl("lblrowid")).Text;

            ddlTopic.SelectedIndex = ddlTopic.Items.IndexOf(ddlTopic.Items.FindByText(((Label)gvFAQ.SelectedRow.FindControl("lblTopic")).Text.Trim()));

            txtSeq.Text = ((Label)gvFAQ.SelectedRow.FindControl("lblRowNumber")).Text;

            txtQuestion.Text = ((Label)gvFAQ.SelectedRow.FindControl("lblQuestion")).Text;

            EditorMessage.Content = ((Label)gvFAQ.SelectedRow.FindControl("lblAnswer")).Text;

            rblActive.SelectedIndex = rblActive.Items.IndexOf(rblActive.Items.FindByText(((Label)gvFAQ.SelectedRow.FindControl("lblActive")).Text.Trim()));

            string strCreatedBy = ((Label)gvFAQ.SelectedRow.FindControl("lblcreatedby")).Text;

            string strCreatedOn = ((Label)gvFAQ.SelectedRow.FindControl("lblcreatedon")).Text;

            if (strCreatedBy != "" && strCreatedOn != "")
            {
                lblLMby.Text = strCreatedBy + " ( on ) " + strCreatedOn;
                divLMBy.Visible = true;
            }
            else
            {
                divLMBy.Visible = false;
            }

            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            dvMand.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            lblrowtodelete.Text = lblrowid.Text;
            mpeconfirm.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvTailDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;

                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //  lblMsg.Text = ex.Message;
        }
    }

    protected void gvPricingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFAQ.PageIndex = e.NewPageIndex;
            List();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void imbEditTopic_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (ddlTopic.SelectedIndex == 0)
            {
                lblalert.Text = "Please choose topic name";
                mpealert.Show();
            }
            else
            {
                mpetime.Show();
                txtTopic.Text = ddlTopic.SelectedItem.Text;
                btnSaveExternal.Text = "Update";
                hdTopId.Value = ddlTopic.SelectedValue.Trim();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void imbAddTopic_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            mpetime.Show();
            txtTopic.Text = string.Empty;
            rblActive.SelectedValue = "Y";
            btnSaveExternal.Text = "Save";
            hdTopId.Value = "0";
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }
    #endregion

    #region  User Defined Function

    public static string Get_TermsadnCondition()
    {
        string URL = string.Empty;
        URL = SecretsBLL.TCUrl.ToString().Trim();

        DataTable dtTC = genclass.GetDataTable_Mgnt("Select * FROM JE_ZOOM..TermsAndCondition_Version where IsCurrent = 1");
        if (dtTC.Rows.Count > 0)
        {
            URL += "JE" + dtTC.Rows[0]["RowId"].ToString() + @"\Terms_And_Condition.pdf";
        }

        return URL;
    }

    public void AddAirport()
    {
        if (btnSave.Text == "Save")
        {
            objMember.ManageType = "I";
        }
        else
        {
            objMember.ManageType = "U";
        }
        objMember.RowId = hdfPSRID.Value;
        objMember.Topic = ddlTopic.SelectedValue.Trim();
        objMember.Question = txtQuestion.Text.Trim();
        objMember.DisSeq = txtSeq.Text;
        objMember.Answer = EditorMessage.Content.ToString().Trim();
        objMember.Active = rblActive.SelectedValue.Trim();
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString().Trim();
    }

    public void AddTopic()
    {
        if (btnSaveExternal.Text == "Save")
        {
            objMember.ManageType = "IT";
        }
        else
        {
            objMember.ManageType = "UT";
        }
        objMember.RowId = hdTopId.Value;
        objMember.Topic = txtTopic.Text.Trim();
        objMember.Active = rblTActive.SelectedValue.Trim();
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString().Trim();
    }

    public void List()
    {
        DataTable dt = objMember.Select();
        if (dt.Rows.Count > 0)
        {
            gvFAQ.DataSource = dt;
            gvFAQ.DataBind();

            string URL = Get_TermsadnCondition();

            foreach (GridViewRow item in gvFAQ.Rows)
            {
                Label lblAnswer = (Label)item.FindControl("lblAnswer");

                if (lblAnswer.Text.Contains("https://www.flyjetedge.net/FLYT/uploads/Generate_Quote/Charter_Master_Agreement_20201120.pdf"))
                {
                    lblAnswer.Text = lblAnswer.Text.Replace("https://www.flyjetedge.net/FLYT/uploads/Generate_Quote/Charter_Master_Agreement_20201120.pdf", URL);
                }
            }
        }
        else
        {
            gvFAQ.DataSource = null;
            gvFAQ.DataBind();
        }
    }


    public void GetTopic()
    {
        DataTable dt = objMember.SelectTopic();
        if (dt.Rows.Count > 0)
        {
            ddlTopic.DataSource = dt;

            ddlTopic.DataTextField = "TopicName";
            ddlTopic.DataValueField = "RowId";
            ddlTopic.DataBind();
            ddlTopic.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        else
        {
            ddlTopic.Items.Insert(0, new ListItem("Please Select", "None"));
        }
    }

    public void Clear()
    {

        hdfPSRID.Value = "0";
        hdTopId.Value = "0";
        txtTopic.Text = string.Empty;
        txtQuestion.Text = string.Empty;
        rblActive.SelectedValue = "Y";
        txtSeq.Text = string.Empty;
        EditorMessage.Content = "";
        divLMBy.Visible = false;
        btnSave.Text = "Save";
        ddlTopic.ClearSelection();
    }


    #endregion
}