﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class Views_CountryFee : System.Web.UI.Page
{

    #region Global Declaration

    protected BusinessLayer.CountryFeeBLL objMember = new BusinessLayer.CountryFeeBLL();

    #endregion


    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Country Setup");
            }

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Country  Fee Setup";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            CalendarExtender1.StartDate = DateTime.Now;
            CalendarExtender2.StartDate = DateTime.Now;

            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
            GetCountry();
            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.btnAddFee.Click += new System.EventHandler(this.btnAddFee_Click);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    #region Button Events

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        AddCountryFee();

        string retVal = objMember.Save(objMember);

        if (retVal != "-1")
        {
            if (btnSave.Text == "Save")
            {
                lblalert.Text = "Country Fee has been saved successfully";
            }
            else
            {
                lblalert.Text = "Country Fee has been updated successfully";
            }

            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From CountryFee Where CountryID='" + retVal + "'");

            if (gvFeeList.Rows.Count > 0)
            {
                string strDiscDate = string.Empty;
                for (int i = 0; i < gvFeeList.Rows.Count; i++)
                {
                    Label lblFeeName = (Label)gvFeeList.Rows[i].FindControl("lblFeeName");
                    Label lblAmount = (Label)gvFeeList.Rows[i].FindControl("lblAmount");
                    Label lblApplyFor = (Label)gvFeeList.Rows[i].FindControl("lblApplyFor");
                    Label lblEffFrom = (Label)gvFeeList.Rows[i].FindControl("lblEFrom");
                    Label lblDiscDate = (Label)gvFeeList.Rows[i].FindControl("lblDisDate");

                    if (lblDiscDate.Text.Length > 0)
                    {
                        strDiscDate = Convert.ToDateTime(lblDiscDate.Text.Trim()).ToString("yyyy-MM-dd");
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "insert INTO  CountryFee (CountryID,FeeName,Amount,ApplyFor,EffFrom,DiscontinueDate) VALUES ('" + retVal + "','" + lblFeeName.Text + "','" + lblAmount.Text.Replace(",", "") + "','" + lblApplyFor.Text + "','" + Convert.ToDateTime(lblEffFrom.Text).ToString("yyyy-MM-dd") + "','" + strDiscDate + "')");

                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "insert INTO  CountryFee (CountryID,FeeName,Amount,ApplyFor,EffFrom,DiscontinueDate) VALUES ('" + retVal + "','" + lblFeeName.Text + "','" + lblAmount.Text.Replace(",", "") + "','" + lblApplyFor.Text + "','" + Convert.ToDateTime(lblEffFrom.Text).ToString("yyyy-MM-dd") + "',null)");
                    }


                }
            }

            mpealert.Show();
            btnAdd.Focus();
            btnViewOnClick(sender, e);
            Clear();
        }
        else
        {
            lblMessage.Value = "An error has occured while processing your request";
        }
    }
    private void btnAddFee_Click(object sender, System.EventArgs e)
    {
        mpeFeeDetails.Show();

        txtFeeName.Text = string.Empty;
        txtAmount.Text = string.Empty;
        txtEffFrom.Text = string.Empty;
        txtDisContinueDate.Text = string.Empty;
        rblApplyFor.SelectedValue = "B";
        btnAddtoList.Text = "Add to List";
        ViewState["DisplayIndex"] = null;
    }
    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            lblMan.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void gvFeeList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            mpeFeeDetails.Show();
            ViewState["DisplayIndex"] = gvFeeList.SelectedRow.RowIndex.ToString();
            txtFeeName.Text = ((Label)gvFeeList.SelectedRow.FindControl("lblFeeName")).Text;
            txtEffFrom.Text = Convert.ToDateTime(((Label)gvFeeList.SelectedRow.FindControl("lblEFrom")).Text).ToString("MM-dd-yyyy");
            if (((Label)gvFeeList.SelectedRow.FindControl("lblDisDate")).Text.Length > 0)
            {
                txtDisContinueDate.Text = Convert.ToDateTime(((Label)gvFeeList.SelectedRow.FindControl("lblDisDate")).Text).ToString("MM-dd-yyyy");
            }

            txtAmount.Text = ((Label)gvFeeList.SelectedRow.FindControl("lblAmount")).Text.Replace(",", "");
            rblApplyFor.SelectedIndex = rblApplyFor.Items.IndexOf(rblApplyFor.Items.FindByValue(((Label)gvFeeList.SelectedRow.FindControl("lblApplyFor")).Text));
            btnAddtoList.Text = "Update";
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();

            if (gvCountryFee.Rows.Count > 0)
            {
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
                lblMan.Visible = false;
            }
            else
            {
                tblForm.Visible = true;
                tblGrid.Visible = false;
                btnAdd.Visible = false;
                btnSave.Visible = true;
                btnView.Visible = true;
                lblMan.Visible = true;
            }

            btnAdd.Focus();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            objMember.DeletUser(lblrowtodelete.Text.Trim());
            List();
            lblalert.Text = "User Details have been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvPricingDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvCountryFee.SelectedRow.FindControl("lblrowid");

            Label lblCountry = (Label)gvCountryFee.SelectedRow.FindControl("lblCountry");

            hdfPSRID.Value = lblrowid.Text;

            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText(lblCountry.Text.Trim()));

            Label lblCreatedBy = (Label)gvCountryFee.SelectedRow.FindControl("lblCreatedby");

            Label lblCreatedOn = (Label)gvCountryFee.SelectedRow.FindControl("lblCreatedOn");

            if (lblCreatedBy.Text != "" && lblCreatedOn.Text != "")
            {
                lblLMby.Text = lblCreatedBy.Text + " ( on  ) " + Convert.ToDateTime(lblCreatedOn.Text).ToString("MMM dd, yyyy hh:mm tt").ToUpper();
                divLMBy.Visible = true;
            }
            else
            {
                divLMBy.Visible = false;
            }


            DataTable dtPS = objMember.Child(lblrowid.Text.Trim());

            if (dtPS != null && dtPS.Rows.Count > 0)
            {
                ViewState["dtFee"] = dtPS;

                gvFeeList.DataSource = dtPS;
                gvFeeList.DataBind();
            }
            else
            {
                ViewState["dtFee"] = null;

                gvFeeList.DataSource = null;
                gvFeeList.DataBind();
            }


            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            lblMan.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }



    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            lblrowtodelete.Text = lblrowid.Text;
            mpeconfirm.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnAddLegOnClick(object sender, EventArgs e)
    {
        DataRow drMappingDetails;
        DataTable dtMapping = new DataTable();
        try
        {
            if (ViewState["dtFee"] != null)
            {
                dtMapping = (DataTable)ViewState["dtFee"];
            }
            else
            {
                dtMapping.Columns.Clear();
                dtMapping.Columns.Add("FeeName");
                dtMapping.Columns.Add("Amount");
                dtMapping.Columns.Add("ApplyFor");
                dtMapping.Columns.Add("EffFrom");
                dtMapping.Columns.Add("DiscontinueDate");
                dtMapping.Columns.Add("ApplyForFlag");
            }
            if (btnAddtoList.Text == "Add to List")
            {
                drMappingDetails = dtMapping.NewRow();
                drMappingDetails["FeeName"] = txtFeeName.Text.Trim();
                drMappingDetails["Amount"] = Convert.ToDecimal(txtAmount.Text.Trim()).ToString("0");
                drMappingDetails["ApplyFor"] = rblApplyFor.SelectedValue.Trim();
                drMappingDetails["EffFrom"] = Convert.ToDateTime(txtEffFrom.Text).ToString("MMM dd,yyyy");
                drMappingDetails["DiscontinueDate"] = txtDisContinueDate.Text.ToString().Length > 0 ? Convert.ToDateTime(txtDisContinueDate.Text).ToString("MMM dd,yyyy") : null;

                drMappingDetails["ApplyForFlag"] = rblApplyFor.SelectedItem.Text;
                dtMapping.Rows.Add(drMappingDetails);
            }
            else
            {
                if (ViewState["DisplayIndex"] != null)
                {
                    int Rows = Convert.ToInt32(ViewState["DisplayIndex"]);
                    dtMapping.Rows[Rows]["FeeName"] = txtFeeName.Text;
                    dtMapping.Rows[Rows]["Amount"] = Convert.ToDecimal(txtAmount.Text.Trim()).ToString("0");
                    dtMapping.Rows[Rows]["ApplyFor"] = rblApplyFor.SelectedValue.Trim();
                    dtMapping.Rows[Rows]["EffFrom"] = Convert.ToDateTime(txtEffFrom.Text).ToString("MMM dd,yyyy");
                    dtMapping.Rows[Rows]["DiscontinueDate"] = txtDisContinueDate.Text.ToString().Length > 0 ? Convert.ToDateTime(txtDisContinueDate.Text).ToString("MMM dd,yyyy") : null;
                    dtMapping.Rows[Rows]["ApplyForFlag"] = rblApplyFor.SelectedItem.Text;
                    dtMapping.AcceptChanges();
                }
            }

            ViewState["dtFee"] = dtMapping;

            gvFeeList.DataSource = dtMapping;
            gvFeeList.DataBind();

            txtFeeName.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtEffFrom.Text = string.Empty;
            txtDisContinueDate.Text = string.Empty;
            rblApplyFor.SelectedValue = "B";


        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvFeeList_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtFee"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtFee"] = dt;
            gvFeeList.EditIndex = -1;
            gvFeeList.DataSource = ViewState["dtFee"];
            gvFeeList.DataBind();
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    #region  User Defined Function

    public void AddCountryFee()
    {
        if (btnSave.Text == "Save")
        {
            objMember.ManageType = "I";
        }
        else
        {
            objMember.ManageType = "U";
        }
        objMember.RowID = hdfPSRID.Value;
        objMember.Countryid = ddlCountry.SelectedValue.Trim();
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"];
        objMember.Active = rblActive.SelectedValue.Trim();
    }

    public void List()
    {
        DataTable dt = objMember.Select();
        if (dt.Rows.Count > 0)
        {
            gvCountryFee.DataSource = dt;
            gvCountryFee.DataBind();

            for (int i = 0; i < gvCountryFee.Rows.Count; i++)
            {
                Label lblRowId = (Label)gvCountryFee.Rows[i].FindControl("lblrowid");

                DataList dsScreen = (DataList)gvCountryFee.Rows[i].FindControl("rptFeeDetails");

                DataTable dtChild = objMember.Child(lblRowId.Text);

                if (dtChild.Rows.Count > 0)
                {
                    dsScreen.DataSource = dtChild;
                    dsScreen.DataBind();
                }
                else
                {
                    dsScreen.DataSource = null;
                    dsScreen.DataBind();
                }

            }
        }
        else
        {
            gvCountryFee.DataSource = null;
            gvCountryFee.DataBind();

        }
    }

    public void GetCountry()
    {
        DataTable dt = genclass.GetDataTable("select * from JETEDGE_FUEL..Country where Active='Y' order by Country_Name ");
        if (dt.Rows.Count > 0)
        {
            ddlCountry.DataSource = dt;

            ddlCountry.DataTextField = "Country_Name";
            ddlCountry.DataValueField = "Row_ID";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Please Select", "0"));
        }
        else
        {
            ddlCountry.Items.Insert(0, new ListItem("Please Select", "None"));
        }
    }

    public void Clear()
    {
        try
        {
            hdfPSRID.Value = "0";
            btnSave.Text = "Save";
            rblActive.SelectedValue = "Y";
            divLMBy.Visible = false;
            ddlCountry.ClearSelection();
            txtFeeName.Text = string.Empty;
            txtAmount.Text = string.Empty;
            txtEffFrom.Text = string.Empty;
            txtDisContinueDate.Text = string.Empty;
            rblApplyFor.SelectedValue = "B";
            ViewState["dtFee"] = null;

            gvFeeList.DataSource = null;
            gvFeeList.DataBind();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    #endregion

    #endregion

}