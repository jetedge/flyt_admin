﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

public partial class Views_Pricecharter : System.Web.UI.Page
{
    public int linenum = 0;
    public string MethodName = string.Empty;

    #region Global Declaration

    protected BusinessLayer.PriceCharterBLL objMember = new BusinessLayer.PriceCharterBLL();
    ZoomCalculatinDAL objZoomCalculatinDAL = new ZoomCalculatinDAL();
    #endregion

    #region Pageload

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Home");
                }


                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Admin";
                lblSubHeader.InnerText = "Home";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();
                BindEmptyLegs();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    #endregion

    #region Gridview Events

    protected void lnkPersons_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lb = (LinkButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;

            Label Label12 = (Label)gvrow.FindControl("Label12");
            Label lbltripno = (Label)gvrow.FindControl("lbltripno");
            Label lblTailNo = (Label)gvrow.FindControl("lblTailNo");
            Label lblstart = (Label)gvrow.FindControl("lblstart");
            Label lblEcity = (Label)gvrow.FindControl("lblEcity");
            Label lblscity = (Label)gvrow.FindControl("lblscity");
            Label lblleg = (Label)gvrow.FindControl("lblleg");

            Label lbltodate = (Label)gvrow.FindControl("lbltodate");
            if (lbltodate.Visible == true)
            {
                lblPopDate.Text = Label12.Text + " - " + lbltodate.Text;
            }
            else
            {
                lblPopDate.Text = Label12.Text;
            }


            lblPopTail.Text = lblTailNo.Text;
            lblPopTrip.Text = lbltripno.Text;
            lblPopLeg.Text = lblleg.Text;
            lblPopDeparture.Text = lblscity.Text;
            lblPopArrival.Text = lblEcity.Text;

            DataTable dtbooklog = Get_PersonViewing(lblTailNo.Text, lbltripno.Text, lblstart.Text);
            if (dtbooklog.Rows.Count > 0)
            {
                mpePersonViewing.Show();

                gvPersonViewing.DataSource = dtbooklog;
                gvPersonViewing.DataBind();
            }
            else
            {
                gvPersonViewing.DataSource = null;
                gvPersonViewing.DataBind();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void gvSimpleSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblAircraftType.Text = ((Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblTailNo")).Text;
            Label lblAircraft = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblAircraft");
            lblStartTIme.Text = ((Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblDtime")).Text;
            lblEndTime1.Text = ((Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblAtime")).Text;
            FromAirport.Text = ((Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblstart")).Text;
            lblToAirport.Text = ((Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblEnd")).Text;
            Label lblstartcity = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblstartcity");
            Label lblEndcity = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblEndcity");
            Label lblavilfrom = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblDDate1");
            Label lblpricing = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("Label7");
            lblTimeintheair.Text = ((Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblFlyingHours")).Text;
            Label lblNoSeats = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblNoSeats");
            Label lblroutes = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblroutes");
            lbltripno.Text = ((Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lbltripno")).Text;
            Label lblleg = (Label)gvSimpleSearch.Rows[gvSimpleSearch.SelectedIndex].FindControl("lblleg");
            
            string strk = lblroutes.Text.Replace("-->", @"@");
            String[] strlist = strk.Split(new[] { '@' });
            string startD = strlist[0];
            string EndD = strlist[1];

            DataTable dtLeg = objZoomCalculatinDAL.Get_EmptyTripSetupTrip(lbltripno.Text, lblleg.Text).Tables[1];
            //lblActualStartICAO.Text = lblStartData.Text + " " + lblscitydata.Text;
            // lblActualEndICAO.Text = lblendData.Text + " " + lblEcityData.Text;
            if (dtLeg.Rows.Count > 0)
            {
                lblStartTIme.Text = dtLeg.Rows[0]["StartTime"].ToString();
                lblEndTime1.Text = dtLeg.Rows[0]["EndTime"].ToString();

                //if (dtLeg.Rows[0]["Departure"].ToString() != string.Empty)
                //    lblActualStartICAO.Text = dtLeg.Rows[0]["Departure"].ToString();
                //if (dtLeg.Rows[0]["Arraival"].ToString() != string.Empty)
                //    lblActualEndICAO.Text = dtLeg.Rows[0]["Arraival"].ToString();

            }


            ModalPopupExtender1.Show();
            bindCharter(lblAircraftType.Text.Trim(), lblavilfrom.Text, startD, EndD);
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void gvSimpleSearch_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() != "admin@demo.com")
                {
                    e.Row.Cells[0].Visible = true;
                    e.Row.Cells[1].Visible = true;
                    e.Row.Cells[2].Visible = true;
                }
                else
                {
                    e.Row.Cells[0].Visible = true;
                    e.Row.Cells[1].Visible = true;

                    e.Row.Cells[2].Visible = true;
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label Label2 = e.Row.FindControl("Label2") as Label;
                Label Label3 = e.Row.FindControl("Label3") as Label;
                Label lbltailflag = e.Row.FindControl("lbltailflag") as Label;
                Label lblleg = e.Row.FindControl("lblleg") as Label;
                Label Label1 = e.Row.FindControl("Label1") as Label;
                Label lblSpType = e.Row.FindControl("lblSplitType") as Label;
                Label lblModFlag = e.Row.FindControl("lblModFlag") as Label;
                Label lblTripFlag = e.Row.FindControl("lblTripFlag") as Label;
                Label lblTailNo = e.Row.FindControl("lblTailNo") as Label;
                Label lbltripno = e.Row.FindControl("lbltripno") as Label;
                Label lbltosplit = e.Row.FindControl("lbltosplit") as Label;
                Label lblCDFlag = e.Row.FindControl("lblCDFlag") as Label;
                Label lblDontshow = e.Row.FindControl("lblDontshow") as Label;
                LinkButton lnkPersons = e.Row.FindControl("lnkPersons") as LinkButton;

                if (lnkPersons.Text == "0")
                {
                    lnkPersons.Text = string.Empty;
                    lnkPersons.Visible = false;
                }

                if (lblCDFlag.Text == "N")
                {
                    lblDontshow.Text = "No";
                    e.Row.Cells[9].BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblDontshow.Text = "Yes";
                }



                if (Label1.Text == "(Special Price)")
                {
                    Label1.Text = "(Fixed Price)";
                }

                if ((Label2.Text == "Hawaii" || Label2.Text == "Alaska" || Label2.Text == "Puerto Rico"))
                    Label2.ForeColor = System.Drawing.Color.Blue;

                if ((Label3.Text == "Hawaii" || Label3.Text == "Alaska" || Label3.Text == "Puerto Rico"))
                    Label3.ForeColor = System.Drawing.Color.Blue;

                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() == "admin@demo.com")
                {
                    if (lbltailflag.Text == "EMPTY")
                    {
                        e.Row.BackColor = System.Drawing.Color.WhiteSmoke;
                    }
                }
                else
                {
                    if (lbltailflag.Text == "EMPTY")
                    {
                        e.Row.BackColor = System.Drawing.Color.WhiteSmoke;
                    }

                }
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() == "admin@demo.com")
                {
                    if (lblleg.Text == "0")
                    {
                        e.Row.Cells[2].Text = "";
                    }
                }
                else
                {
                    if (lblleg.Text == "0")
                    {
                        e.Row.Cells[2].Text = "";
                    }
                }
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().ToLower() != "admin@demo.com")
                {
                    e.Row.Cells[0].Visible = true;
                    e.Row.Cells[1].Visible = true;
                    e.Row.Cells[2].Visible = true;
                }
                else
                {
                    e.Row.Cells[0].Visible = true;
                    e.Row.Cells[1].Visible = true;
                    e.Row.Cells[2].Visible = true;
                }

                if (lblSpType.Text == "CO" && lblModFlag.Text == "SP" && lbltailflag.Text == "AUTO" && lblTripFlag.Text == "B0")
                {
                    e.Row.Visible = false;
                }

                Label lbltodate = e.Row.FindControl("lbltodate") as Label;
                Label lblDateFlag = e.Row.FindControl("lblDateFlag") as Label;
                if (lblDateFlag.Text == "DF")
                {
                    lbltosplit.Visible = true;
                    lbltodate.Visible = true;
                }
                else
                {
                    lbltosplit.Visible = false;
                    lbltodate.Visible = false;
                }
                Label Label12 = e.Row.FindControl("Label12") as Label;
                if (Label12.Text.Length > 0)
                {
                    Label12.Text = Convert.ToDateTime(Label12.Text).ToString("MMM d");
                }
            }
        }

        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }
    protected void gvCharter_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblpriceflag = e.Row.FindControl("lblpriceflag") as Label;
                if (lblpriceflag.Text == "(Special Price)")
                {
                    lblpriceflag.Text = "(Fixed Price)";
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    #region User Defined Function

    public DataTable Get_PersonViewing(string strTailNo, string strTripNo, string strStart)
    {
        SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_PersonView_SEL]", false);

        getAllTripParm[0].Value = "SEL";
        getAllTripParm[1].Value = strTailNo;
        getAllTripParm[2].Value = strTripNo;
        getAllTripParm[3].Value = strStart;

        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.StoredProcedure, "[SP_PersonView_SEL]", getAllTripParm).Tables[0];
    }

    public void BindEmptyLegs()
    {
        DataTable sortedDT = objMember.Select();

        if (sortedDT.Rows.Count > 0)
        {
            gvSimpleSearch.DataSource = sortedDT;
            gvSimpleSearch.DataBind();


        }
        else
        {
            gvSimpleSearch.DataSource = null;
            gvSimpleSearch.DataBind();
        }

    }

    public void bindCharter(string tailNo, string availblefrom, string strstart, string strend)
    {
        DataTable dt1 = objMember.BindCharterPrice(tailNo, availblefrom, strstart, strend);

        if (dt1.Rows.Count > 0)
        {
            gvCharter.DataSource = dt1;
            gvCharter.DataBind();
        }
        else
        {
            gvCharter.DataSource = null;
            gvCharter.DataBind();

        }
    }

    #endregion
}