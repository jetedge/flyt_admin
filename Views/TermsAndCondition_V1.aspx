﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="TermsAndCondition_V1.aspx.cs" Inherits="Views_TermsAndCondition_V1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .kt-dropzone__msg
        {
            font-weight: 600;
            color: #673ab7;
        }
        
        .dropzone .dz-clickable
        {
            cursor: pointer;
        }
        .dropzone
        {
            border: 2px dashed #ebedf2;
        }
        
        .dropzone
        {
            border-radius: 4px;
            padding: 10px;
            text-align: center;
            cursor: pointer;
            min-height: 150px;
        }
        
        .dz-message
        {
            text-align: center;
            margin: 1em 0;
        }
        /* File Upload Design  */
        .file-upload-new
        {
            display: inline-block;
            overflow: hidden;
            position: relative;
            text-align: center;
            vertical-align: middle;
            background: white;
            color: black;
            border-radius: 5px;
            -moz-border-radius: 6px;
            font-weight: 600;
            border: 2px dashed lightgray; /*min-height: 100px;*/
            padding: 10px;
            margin-bottom: 0px;
            cursor: pointer;
        }
        
        .file-upload-new:hover
        {
            background: white;
        }
        
        /* The button size */
        /* .file-upload-new
        {
            height: 45px;
        } */
        .file-upload-new, .file-upload-new span
        {
            width: 97%;
        }
        
        .file-upload-new input
        {
            position: absolute;
            top: 0;
            left: 0;
            margin: 0;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        
        .file-upload-new strong
        {
            font: normal 13.5px sans-serif;
            font-weight: 600;
            vertical-align: middle;
        }
        
        .file-upload-new span
        {
            position: absolute;
            top: 4px;
            left: 4px; /* display: inline-block; */ /* padding-top: 1.15em; */ /* font-size: 0rem; */
            padding: 4px;
            text-align: left;
            font-size: 0;
        }
    </style>
    <style type="text/css">
        h3, .h3
        {
            font-size: 1.2rem !important;
        }
        iframe
        {
            padding: 10px;
            padding-right: 0px;
        }
        .note-editor.note-frame .panel-heading.note-toolbar
        {
            background: #F3F6F9;
            border-bottom: 1px solid rgb(153,154,154) !important;
            border-radius: 0 !important;
        }
        .dropdown-item
        {
            display: block;
            width: 100%;
            padding: 0.4rem !important;
            clear: both;
            font-weight: 400;
            color: #181C32;
            text-align: inherit;
            white-space: nowrap;
            background-color: transparent;
            border: 0;
        }
        .card-header
        {
            padding: 2rem 2.25rem;
            margin-bottom: 0;
            background-color: #ffffff;
            border-bottom: 1px solid rgb(153,154,154) !important;
            border-radius: 0 !important;
        }
        .note-popover .popover-content .note-dropdown-menu.note-check a i, .note-editor .note-toolbar .note-dropdown-menu.note-check a i
        {
            color: #00bfff;
            visibility: hidden;
            align-self: center;
        }
    </style>
    <script type="text/javascript">
        function FileDetails1() {
            var fi = document.getElementById('MainContent_fuFile');
            document.getElementById('MainContent_listofuploadedfilesCount1').innerHTML = '';
            document.getElementById('MainContent_listofuploadedfiles1').innerHTML = '';

            if (fi.files.length > 0) {

                document.getElementById('MainContent_listofuploadedfilesCount1').innerHTML =
                'Total Files : ' + fi.files.length;

                for (var i = 0; i <= fi.files.length - 1; i++) {

                    var fname = fi.files.item(i).name;      // THE NAME OF THE FILE.
                    var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.

                    document.getElementById('MainContent_listofuploadedfiles1').innerHTML =
                    document.getElementById('MainContent_listofuploadedfiles1').innerHTML + '<br />' +
                         fname + ' ( <b>' + fsize + '</b> bytes ) ';
                }
            }
        }

        function AddNew() {
            document.getElementById('cpWheelsUp_btnAddNew').click();
        }
        function View() {
            document.getElementById('cpWheelsUp_btnView').click();
        }
        function Save() {
            document.getElementById('cpWheelsUp_btnSumbit').click();
        }
        function Clear() {
            document.getElementById('cpWheelsUp_btnClear').click();
        }

        function ConfirmDelete() {
            if (confirm('Are you sure want to delete ?')) {
                return true;
            }
            else {
                return false;
            }
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional" ViewStateMode="Enabled">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAddVers" />
            <asp:PostBackTrigger ControlID="btnBackVer" />
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="gvVersion" />
            <%--<asp:PostBackTrigger ControlID="btnSwitch" />
            <asp:PostBackTrigger ControlID="btnSwitchhtml" />--%>
            <asp:PostBackTrigger ControlID="lnkSigningFile" />
        </Triggers>
        <ContentTemplate>
            <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
                <div class="d-flex flex-column-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="kt-portlet">
                                    <div class="form-group row">
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <div class="col-lg-12 text-right" id="divAction" runat="server" visible="false">
                                                    <a href="#" class="btn btn-label-brand btn-sm  btn-bold dropdown-toggle" data-toggle="dropdown"
                                                        style="background-color: #716aca; color: White;">Action </a>
                                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right">
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__item" id="liAddNew" runat="server"><a href="#" class="kt-nav__link"
                                                                onclick="AddNew()"><i class="kt-nav__link-icon flaticon2-add"></i><span class="kt-nav__link-text"
                                                                    style="font-weight: 500; font-size: 1.1rem;">Add New</span> </a></li>
                                                            <li class="kt-nav__item" id="liView" runat="server"><a href="#" class="kt-nav__link"
                                                                onclick="View()"><i class="kt-nav__link-icon flaticon-reply"></i><span class="kt-nav__link-text"
                                                                    style="font-weight: 500; font-size: 1.1rem;">Back</span> </a></li>
                                                            <li class="kt-nav__item" id="liSave" runat="server"><a href="#" class="kt-nav__link"
                                                                onclick="Save()"><i class="kt-nav__link-icon flaticon2-edit"></i><span class="kt-nav__link-text"
                                                                    style="font-weight: 500; font-size: 1.1rem;">
                                                                    <asp:Label ID="lblSave" runat="server" Text="Save"></asp:Label></span> </a>
                                                            </li>
                                                            <li class="kt-nav__item" id="liClear" runat="server"><a href="#" class="kt-nav__link"
                                                                onclick="Clear()"><i class="kt-nav__link-icon flaticon2-refresh"></i><span class="kt-nav__link-text"
                                                                    style="font-weight: 500; font-size: 1.1rem;">Clear</span> </a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 text-right" id="divButton" runat="server">
                                                    <div id="tblModified" runat="server" visible="false" style="float: left;">
                                                        Last Modified By :&nbsp;
                                                        <asp:Label ID="lblVerModifiedBy" runat="server" Style="font-weight: 600;"></asp:Label>
                                                    </div>
                                                    <asp:Button ID="btnAddVers" runat="server" CssClass="btn btn-danger" Text="Add New"
                                                        Visible="false" TabIndex="1" ToolTip="Click here to add new version" OnClick="btnAddVer_Click" />&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnBackVer" runat="server" CssClass="btn btn-danger" Text="Back"
                                                        TabIndex="2" ToolTip="Click here to version list" OnClick="btnBackVer_Click"
                                                        Visible="false" />
                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-danger" Text="Update"
                                                        Visible="false" TabIndex="3" ValidationGroup="VER" ToolTip="Click here to update"
                                                        OnClick="btnUpdateVersion_Click" />&nbsp;&nbsp;
                                                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-danger" Text="Save" Visible="false"
                                                        TabIndex="4" ValidationGroup="VER" ToolTip="Click here to save version" OnClick="btnSaveVersionNew_Click" />
                                                </div>
                                            </div>
                                            <div class="form-group row" id="tblMainGrid" runat="server">
                                                <div class="col-xl-12 table-responsive boxshadow">
                                                    <asp:GridView ID="gvVersion" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CssClass="table" HeaderStyle-CssClass="thead-dark" OnSelectedIndexChanged="gvVersion_SelectedIndexChanged"
                                                        DataKeyNames="Rowid" EmptyDataText="No Data Found">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="1%" Visible="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                                                                    <asp:Label ID="lblVersionId" Visible="false" runat="server" Text='<%# Bind("Rowid") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Version" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true"
                                                                HeaderStyle-Width="8%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblVersion" runat="server" Text='<%# Bind("Version") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Effective From" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center" ItemStyle-Wrap="true" HeaderStyle-Width="7%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEffFrom" runat="server" Text='<%# Bind("EffFrom") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Notes" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Wrap="true" HeaderStyle-Width="15%" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNotes" runat="server" Text='<%# Bind("Notes") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-Wrap="true" HeaderStyle-Width="5%" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblActive" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Wrap="true" HeaderStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Bind("CreatedBy") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                                                ItemStyle-Wrap="true" HeaderStyle-Width="10%" Visible="true">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Bind("CreatedOn") %>' Visible="true"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Edit" HeaderStyle-Width="2%" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imbView" runat="server" Height="20px" ImageUrl="~/Images/icon-edit-new.png"
                                                                        CausesValidation="false" CommandName="select" AlternateText="Edit" ToolTip="Click here to edit." />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="1%" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imbDelet" runat="server" Height="18px" ImageUrl="~/Images/delete.png"
                                                                        CausesValidation="false" CommandName="delete" OnClick="btnDeletVers_Click" OnClientClick="javascript:return ConfirmDelete();"
                                                                        AlternateText="delete" ToolTip="Click here to delete." />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="row" id="tblGrid" runat="server" visible="false">
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-lg-2 col-md-2 col-sm-6 col-xs-6 col-form-label">
                                                    Version <span class="MandatoryField">* </span>
                                                </label>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                                    <asp:TextBox ID="txtVersion" runat="server" CssClass="form-control" MaxLength="100"
                                                        Width="95%" TabIndex="6" ToolTip="Allows all type of characters. length is limited to 100"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtVersion"
                                                        Display="None" ErrorMessage="Version is required." SetFocusOnError="True" ValidationGroup="VER" />
                                                </div>
                                                <label class="col-lg-2 col-md-2 col-sm-6 col-xs-6 col-form-label">
                                                    Effective From <span class="MandatoryField">* </span>
                                                </label>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                    <asp:TextBox ID="txtEffFrom" runat="server" CssClass="form-control" MaxLength="100"
                                                        Style="text-transform: uppercase;" onkeypress="return false;" Width="70%" TabIndex="7"
                                                        ToolTip="Choose Effective From"></asp:TextBox>
                                                    <asp:CalendarExtender ID="calendar12" runat="server" TargetControlID="txtEffFrom"
                                                        Format="MMM d, yyyy" PopupPosition="TopRight">
                                                    </asp:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEffFrom"
                                                        Display="None" ErrorMessage="Effective From is required." SetFocusOnError="True"
                                                        ValidationGroup="VER">*</asp:RequiredFieldValidator>
                                                    <asp:RadioButtonList ID="rblActive" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                                        TabIndex="8" Style="display: none;">
                                                        <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="display: none;">
                                                <label class="col-lg-2 col-md-2 col-sm-5 col-xs-5 col-form-label">
                                                    Notes
                                                </label>
                                                <div class="col-lg-10 col-md-10 col-sm-7 col-xs-7">
                                                    <asp:TextBox ID="txtNotes" runat="server" CssClass="form-control" MaxLength="100"
                                                        Width="96%" TabIndex="9" ToolTip="Allows all type of characters. length is limited to 100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <label style="font-weight: bold;">
                                                        1. Terms and Condition <span class="MandatoryField">* </span>
                                                    </label>
                                                    <asp:LinkButton ID="btnSwitch" OnClick="btnSwitch_Click" runat="server" Text="PDF Preview"
                                                        Style="color: Blue; float: right; font-weight: 600; margin-left: 2rem;"></asp:LinkButton>
                                                    <asp:LinkButton ID="btnSwitchhtml" OnClick="btnSwitchHTML_Click" runat="server" Text="HTML Preview"
                                                        Style="color: Blue; float: right; font-weight: 600;"></asp:LinkButton>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <%--<textarea class="summernote" id="EditorMessage" runat="server" />--%>
                                                    <cc1:Editor ID="EditorMessage" runat="server" Height="450px" TabIndex="13" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="VER" SetFocusOnError="true"
                                                        ErrorMessage="Terms and Condition Message is Required.!" runat="server" ControlToValidate="EditorMessage"
                                                        Display="None"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <label style="line-height: 25px;">
                                                        <span style="color: red;">Instructions : </span>
                                                        <br />
                                                        1. Copy and paste the text from word document without <b>ACKNOWLEDGEMENT AND ACCEPTANCE.
                                                        </b>
                                                        <br />
                                                        2. Use <b>"Calibri" </b>font family as default and for Headers use font size as
                                                        (<b>18pt</b>) and paragraph use (<b>12pt</b>).
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <label style="font-weight: bold;">
                                                        2. DocuSign Signing PDF <span class="MandatoryField">* </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <%-- <div class="form-group row" id="dvFileUpload" runat="server">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <label class="file-upload-new" id="lblfuFile" runat="server" style="margin-bottom: 0.5rem;
                                                        width: 100%;">
                                                        <div class="kt-dropzone__msg dz-message needsclick">
                                                            <h3 class="kt-dropzone__msg-title" style="color: Green">
                                                                Drop Signing PDF or click to upload.</h3>
                                                        </div>
                                                        <asp:FileUpload ID="fuFile" accept="application/pdf" AllowMultiple="false" runat="server"
                                                            ToolTip="Drop files here or click to upload." onchange="FileDetails1()" Style="width: 100%;
                                                            min-height: 73px; cursor: pointer;"></asp:FileUpload>
                                                    </label>
                                                    <br />
                                                    <asp:Label ID="listofuploadedfilesCount1" Style="line-height: 25px; color: #a72224;
                                                        font-weight: 600;" runat="server" />
                                                    <asp:Label ID="listofuploadedfiles1" Style="line-height: 25px;" runat="server" />
                                                </div>
                                            </div>--%>
                                            <div class="form-group row" id="dvFileView" runat="server">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblSigningFile" Text="Terms_And_Condition_Sign.pdf" Visible="false"
                                                        runat="server" />
                                                    <asp:LinkButton ID="lnkSigningFile" ForeColor="Blue" Font-Bold="true" Text="Terms_And_Condition_Sign.pdf"
                                                        ToolTip="Click to download file" OnClick="lnkSigningFile_Click" runat="server" />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:LinkButton ID="lnkPreview" ForeColor="#9e0101" Font-Bold="true" Text="Preview"
                                                        ToolTip="Click to view Preview" OnClick="lnkPreview_Click" runat="server" />
                                                    <asp:LinkButton ID="lnkDelete" ForeColor="Red" ToolTip="Click to Delete file" OnClick="lnkDelete_Click"
                                                        Visible="false" OnClientClick="javascript:return ConfirmDelete();" runat="server"><i class="fa fa-trash MandatoryField" ></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
            <asp:ModalPopupExtender ID="mpeRoute" runat="server" PopupControlID="pnlRoute" TargetControlID="Button1"
                Y="10" CancelControlID="Button2" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlRoute" runat="server" role="document" Style="display: none; width: 90% !important;
                background-color: #525659f5; left: 110px">
                <div class="modal-dialog" role="document" style="height: 100% !important;">
                    <div class="modal-content" style="height: 100% !important; background-color: White !important;">
                        <div class="modal-header">
                            <div class="form-group row" style="width: 100%;">
                                <label class="col-6 col-form-label">
                                    <h5 style="margin-left: 0%; font-size: 18px">
                                        Terms And Conditions</h5>
                                </label>
                                <div class="col-6 text-right">
                                    <asp:LinkButton ID="Button2" runat="server" Style="color: rgb(255,255,255); float: right;
                                        text-decoration: none" CssClass="btn btn-danger" Text="Close" CausesValidation="false" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 13%;">
                                    Group Header <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-6">
                                    <asp:TextBox ID="txtHeader" runat="server" CssClass="form-control" MaxLength="500"
                                        Width="100%" TabIndex="11" ToolTip="Allows all type of characters. length is limited to 500"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtHeader"
                                        Display="None" ErrorMessage="Group Header is required." SetFocusOnError="True"
                                        ValidationGroup="FS">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 12%;">
                                    Order Sequence <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-1">
                                    <asp:TextBox ID="txtOrdSeq" runat="server" CssClass="form-control" MaxLength="10"
                                        Enabled="false" Width="80%" TabIndex="12" ToolTip="Allows numerics only. length is limited to 10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOrdSeq"
                                        Display="None" ErrorMessage="Order Sequence is required." SetFocusOnError="True"
                                        ValidationGroup="FS">*</asp:RequiredFieldValidator>
                                </div>
                                <label class="col-1 col-form-label">
                                </label>
                            </div>
                            <div class="form-group row">
                            </div>
                            <div class="form-group row">
                                <label class="col-6 col-form-label">
                                    <div id="trModify" runat="server" visible="false" style="padding-top: 10px;">
                                        <span style="color: #673ab7; font-size: 13px; padding-left: 5px">Last Modified by :&nbsp;</span>
                                        <asp:Label ID="lblModify" Style="font-size=13px; color: Black; font-weight: 500"
                                            runat="server"></asp:Label>
                                    </div>
                                </label>
                                <div class="col-6 text-right">
                                    <asp:Button ID="btnSumbit" runat="server" CssClass="btn btn-danger" Text="Save" TabIndex="14"
                                        ValidationGroup="FS" ToolTip="Click here to save" OnClick="btnSumbit_Click" Visible="false" />&nbsp;&nbsp;
                                    <asp:Button ID="btnClear" runat="server" CssClass="btn btn-danger" Text="Clear" ToolTip="Click here to clear"
                                        TabIndex="15" OnClick="btnClear_Click" Visible="false" />
                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-danger" Text="View Setup"
                                        TabIndex="15" ToolTip="Click here to view." OnClick="btnView_Click" Visible="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Button ID="btnPreview" runat="server" Text="Button" Style="display: none" />
            <asp:ModalPopupExtender ID="mpePreview" runat="server" PopupControlID="pnlPreview"
                TargetControlID="btnPreview" Y="15" BackgroundCssClass="modalBackground" CancelControlID="LinkButton1">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlPreview" runat="server" role="document" ScrollBars="Auto" Style="display: none;
                width: 60%; height: 96%; background-color: White">
                <div class="modal-dialog m-0" role="document">
                    <div class="modal-content m-0" style="background: white;">
                        <div class="modal-header">
                            <h5 class="modal-title" style="color: Black; font-size: 1.3rem; font-weight: 600;">
                                Terms and Conditions</h5>
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" Text="Close"
                                Style="float: right;" CausesValidation="false" />
                        </div>
                        <div class="modal-body p-4" style="border: 1px solid #9E9E9E; margin: 0.5rem;">
                            <asp:Label ID="lblhtmlView" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Button ID="btnTerms" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpeterms" runat="server" TargetControlID="btnTerms" PopupControlID="pnlTerms"
                CancelControlID="imgCloseterms" Y="15" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlTerms" runat="server" Height="96%" ScrollBars="Auto" Style="display: none;
                width: 65%; background-color: rgb(31 30 46);">
                <div class="modal-dialog m-0" role="document">
                    <div class="modal-content m-0" style="background: white;">
                        <div class="modal-header">
                            <h5 class="modal-title" style="color: Black; font-size: 1.3rem; font-weight: 600;">
                                Terms and Conditions</h5>
                            <asp:LinkButton ID="imgCloseterms" runat="server" CssClass="btn btn-danger" Style="float: right;"
                                Text="Close" CausesValidation="false" />
                        </div>
                        <div class="modal-body p-0">
                            <iframe id="pdfIFrame" runat="server" frameborder="0" width="100%"></iframe>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
            <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
                BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
                padding: 10px">
                <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
                    cellpadding="0" cellspacing="0">
                    <tr style="border-right: 0px; border-left: 0px">
                        <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                            <asp:Label ID="lblalert" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                                text-decoration: none" CausesValidation="false" Text="Ok" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Label ID="lblUserName" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblUserId" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblCustomerId" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblUserEmail" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblTermId" runat="server" Visible="false" Text="0"></asp:Label>
            <asp:Label ID="lblVersionid" runat="server" Visible="false" Text="0"></asp:Label>
            <asp:Label ID="lblChangePosValue" runat="server" Visible="false" Text="0"></asp:Label>
            <asp:Label ID="lblChangeSeq" runat="server" Visible="false" Text="0"></asp:Label>
            <asp:Label ID="lblApplyChangeSeq" runat="server" Visible="false" Text="0"></asp:Label>
            <asp:Label ID="lblScreenName" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblScreenId" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblButtonRights" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblModuleName" runat="server" Visible="false"></asp:Label>
            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="FS" runat="server"
                ShowMessageBox="true" ShowSummary="false" />
            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="VER" runat="server"
                ShowMessageBox="true" ShowSummary="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
