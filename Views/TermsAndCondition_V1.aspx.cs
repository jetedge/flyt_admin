﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Xml;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.html;
using iTextSharp.tool.xml;
using System.Text.RegularExpressions;

public partial class Views_TermsAndCondition_V1 : System.Web.UI.Page
{
    string strCustomerAP = string.Empty;
    public int linenum = 0;
    public string MethodName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {

                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Non Zoom Customer");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Home";
                lblSubHeader.InnerText = "Terms And Condition";

                lblUserName.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();
                lblUserId.Text = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

                BindVersionGrid();

                tblMainGrid.Visible = true;

                tblGrid.Visible = false;
                btnSwitch.Visible = false;
                btnSwitchhtml.Visible = false;

                btnAddVers.Focus();

                if (lblButtonRights.Text == "V")
                {
                    btnAddVers.Visible = false;
                }
                else
                {
                    btnAddVers.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void BindVersionGrid()
    {
        DataTable dtVersion = BindVerGrid();

        if (dtVersion != null && dtVersion.Rows.Count > 0)
        {
            gvVersion.DataSource = dtVersion;
            gvVersion.DataBind();
        }
        else
        {
            gvVersion.DataSource = null;
            gvVersion.DataBind();
        }
    }

    protected void btnSumbit_Click(object sender, EventArgs e)
    {
        string strSQ = string.Empty;
        string[] FetSetup = new string[15];
        string status = string.Empty;
        string result = string.Empty;
        string Message = string.Empty;
        string StrEvent = string.Empty;

        try
        {
            if (lblTermId.Text == "0")
            {
                FetSetup[0] = "I";
                Message = "has been saved";
                StrEvent = "Insert";
            }
            else
            {
                FetSetup[0] = "U";
                Message = "has been updated";
                StrEvent = "Update";
            }

            FetSetup[1] = Convert.ToInt32(lblTermId.Text.Trim()).ToString();
            FetSetup[2] = "Terms And Condition";
            FetSetup[3] = EditorMessage.Content;
            FetSetup[4] = "1";
            FetSetup[5] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
            FetSetup[6] = lblVersionid.Text;
            FetSetup[7] = "Terms_And_Condition_Sign.pdf";

            //if (fuFile.HasFile)
            //{
            //    FetSetup[7] = removeSpecialChar(fuFile.FileName);
            //}
            //else if (lblSigningFile.Text.Length > 0)
            //{
            //    FetSetup[7] = lblSigningFile.Text;
            //}
            //else
            //{
            //    FetSetup[7] = "Terms_And_Condition_Top.pdf";
            //}

            FetSetup[8] = EditorMessage.Content.Replace("pt;", "px;").Replace("12.0pt", "12px").Replace("15.0pt", "14px").Replace("18.0pt", "16px").Replace("24.0pt", "18px").Replace("12pt", "12px").Replace("15pt", "14px").Replace("18pt", "16px").Replace("24pt", "18px");

            result = SaveTermsAndConditions(FetSetup);

            if (result != "0" || result != "")
            {
                if (lblChangePosValue.Text == "B")
                {
                    strSQ = lblApplyChangeSeq.Text.Trim();

                    if (strSQ == "0")
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE TermsAndConditions_V1  set OrderSequence=1 where TermsId='" + result.ToString() + "' and VersionID='" + lblVersionid.Text + "'");
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE TermsAndConditions_V1  set OrderSequence='" + txtOrdSeq.Text.Trim() + "' where TermsId='" + result.ToString() + "' and VersionID='" + lblVersionid.Text + "'");
                    }

                    DataTable dt = genclass.GetDataTable("SELECT * FROM TermsAndConditions_V1 where VersionID='" + lblVersionid.Text + "' and OrderSequence >='" + txtOrdSeq.Text + "' and Termsid!='" + result.ToString().Trim() + "' order by OrderSequence asc");
                    int m = 1;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strSQ = (Convert.ToInt32(strSQ) + 1).ToString();
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE TermsAndConditions_V1  set OrderSequence='" + strSQ + "' where TermsId='" + dt.Rows[i]["Termsid"].ToString() + "' and VersionID='" + lblVersionid.Text + "'");
                        strSQ = strSQ;
                    }
                }
                if (lblChangePosValue.Text == "A")
                {
                    strSQ = lblApplyChangeSeq.Text.Trim();

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE TermsAndConditions_V1  set OrderSequence='" + txtOrdSeq.Text.Trim() + "' where TermsId='" + result.ToString() + "' and VersionID='" + lblVersionid.Text + "'");

                    DataTable dt = genclass.GetDataTable("SELECT * FROM TermsAndConditions_V1 where VersionID='" + lblVersionid.Text + "' and OrderSequence >='" + txtOrdSeq.Text + "' and Termsid!='" + result.ToString().Trim() + "' order by OrderSequence asc");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strSQ = (Convert.ToInt32(strSQ) + 1).ToString();
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE TermsAndConditions_V1  set OrderSequence='" + strSQ + "' where TermsId='" + dt.Rows[i]["Termsid"].ToString() + "' and VersionID='" + lblVersionid.Text + "'");
                        strSQ = strSQ;
                    }
                }
            }

            CreatePDF_NEW(lblVersionid.Text, lblUserId.Text, lblUserName.Text);

            clear();

            tblGrid.Visible = true;

            lblalert.Text = "Terms And Conditions details " + Message + " successfully";
            mpealert.Show();

            GetTerms(lblVersionid.Text);

            GethtmlView();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            clear();
            mpeRoute.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    void clear()
    {
        txtHeader.Text = string.Empty;
        EditorMessage.Content = string.Empty;
        txtOrdSeq.Text = string.Empty;
        btnSumbit.Text = "Save";
        lblTermId.Text = "0";
        lblSave.Text = "Save";

    }

    void clearVersion()
    {
        txtVersion.Text = string.Empty;
        txtEffFrom.Text = string.Empty;
        txtNotes.Text = string.Empty;
        rblActive.SelectedValue = "Y";

        lblVersionid.Text = "0";

    }

    public void GetTerms(string strVerionId)
    {
        try
        {
            DataTable dtApproverGrid = BindGrid("S", strVerionId);

            if (dtApproverGrid.Rows.Count > 0)
            {
                lblTermId.Text = dtApproverGrid.Rows[0]["TermsId"].ToString();
                txtHeader.Text = dtApproverGrid.Rows[0]["Header"].ToString();
                EditorMessage.Content = dtApproverGrid.Rows[0]["Description"].ToString();
                txtOrdSeq.Text = dtApproverGrid.Rows[0]["orderSequence"].ToString();

                lblSigningFile.Text = dtApproverGrid.Rows[0]["SignatureDescPDF"].ToString();
                lnkSigningFile.Text = dtApproverGrid.Rows[0]["SignatureDescPDF"].ToString();

                lblModify.Text = dtApproverGrid.Rows[0]["CreatedBy"].ToString() + " (on) " + dtApproverGrid.Rows[0]["CreatedOn"].ToString();
                btnSumbit.Text = "Update";
                lblSave.Text = "Update";

                txtHeader.Focus();
                lblChangePosValue.Text = "0";
                lblChangeSeq.Text = "0";
                lblApplyChangeSeq.Text = "0";



                GetPDFView();
                GethtmlView();

                btnSwitchhtml.Visible = true;
                btnSwitch.Visible = true;

                //dvFileUpload.Visible = false;
                //dvFileView.Visible = true;

            }
            else
            {
                pdfIFrame.Visible = false;
                btnSwitchhtml.Visible = false;
                btnSwitch.Visible = false;
                btnSumbit.Text = "Save";
                lblSave.Text = "Save";
                lblTermId.Text = "0";
                lblChangePosValue.Text = "0";
                lblChangeSeq.Text = "0";
                lblApplyChangeSeq.Text = "0";
                DataTable dt = genclass.GetDataTable("SELECT MAX(OrderSequence) as Orders FROM TermsAndConditions_V1 WHERE Versionid='" + lblVersionid.Text.Trim() + "'");
                if (dt.Rows.Count > 0 && dt != null)
                {
                    if (dt.Rows[0]["Orders"].ToString() != "")
                    {
                        txtOrdSeq.Text = (Convert.ToInt32(dt.Rows[0]["Orders"].ToString()) + 1).ToString();
                    }
                    else
                    {
                        txtOrdSeq.Text = "1";
                    }
                }
                else
                {
                    txtOrdSeq.Text = "1";
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void gvVersion_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            tblMainGrid.Visible = false;
            tblGrid.Visible = true;

            btnBackVer.Visible = true;
            btnAddVers.Visible = false;
            if (lblButtonRights.Text == "V")
            {
                btnUpdate.Visible = false;
            }
            else
            {
                btnUpdate.Visible = true;
            }

            txtVersion.Focus();
            tblModified.Visible = true;
            lblVersionid.Text = ((Label)gvVersion.SelectedRow.FindControl("lblVersionId")).Text;
            txtVersion.Text = ((Label)gvVersion.SelectedRow.FindControl("lblVersion")).Text;
            txtEffFrom.Text = Convert.ToDateTime(((Label)gvVersion.SelectedRow.FindControl("lblEffFrom")).Text).ToString("MMM d, yyyy");
            txtNotes.Text = ((Label)gvVersion.SelectedRow.FindControl("lblNotes")).Text;

            lblVerModifiedBy.Text = ((Label)gvVersion.SelectedRow.FindControl("lblCreatedBy")).Text + " ( on ) " + ((Label)gvVersion.SelectedRow.FindControl("lblCreatedOn")).Text;

            string strActive = ((Label)gvVersion.SelectedRow.FindControl("lblActive")).Text;
            if (strActive == "Yes")
            {
                rblActive.SelectedValue = "Y";
            }
            else if (strActive == "No")
            {
                rblActive.SelectedValue = "N";
            }
            else
            {
                rblActive.SelectedValue = "Y";
            }

            GetTerms(lblVersionid.Text);

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        try
        {
            clear();
            trModify.Visible = false;
            //tblFormView.Visible = true;
            tblGrid.Visible = true;
            txtHeader.Focus();
            btnView.Visible = false;
            btnSumbit.Visible = true;
            btnClear.Visible = true;


            lblChangePosValue.Text = "0";
            lblChangeSeq.Text = "0";
            lblApplyChangeSeq.Text = "0";
            txtHeader.Focus();

            mpeRoute.Show();
            DataTable dt = genclass.GetDataTable("SELECT MAX(OrderSequence) as Orders FROM TermsAndConditions_V1 WHERE Versionid='" + lblVersionid.Text.Trim() + "'");

            if (dt.Rows.Count > 0 && dt != null)
            {
                if (dt.Rows[0]["Orders"].ToString() != "")
                {
                    txtOrdSeq.Text = (Convert.ToInt32(dt.Rows[0]["Orders"].ToString()) + 1).ToString();
                }
                else
                {
                    txtOrdSeq.Text = "1";
                }

            }
            else
            {
                txtOrdSeq.Text = "1";
            }

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void lnkPreview_Click(object sender, EventArgs e)
    {
        try
        {
            string strFilePath = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\Terms_And_Condition_Sign.pdf";

            pdfIFrame.Attributes.Add("style", "width: 100%; border: 0; background-color: white; height: 88vh");

            string NewPath = strFilePath.Replace(@"D:\", SecretsBLL.InvoiceNewPath);
            pdfIFrame.Attributes.Add("src", null);
            pdfIFrame.Attributes.Add("src", NewPath + "#view=FitH");

            mpeterms.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void lnkSigningFile_Click(object sender, EventArgs e)
    {
        try
        {
            string strFilePath = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\Terms_And_Condition_Sign.pdf";
            funFileDownload(strFilePath);
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            string strFilePath = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\JE" + lblVersionid.Text + @"\" + lnkSigningFile.Text;

            if (File.Exists(strFilePath))
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                File.Delete(strFilePath);

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update TermsAndConditions_V1 set SignatureDescPDF = NULL where VersionId = '" + lblVersionid.Text + "'");

                //dvFileUpload.Visible = true;
                //dvFileView.Visible = false;

                lnkSigningFile.Text = string.Empty;
                lblSigningFile.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    void funFileDownload(string Location)
    {
        try
        {
            FileInfo file = new FileInfo(Location);
            if (file.Exists)
            {
                Response.Buffer = false;
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(Location));
                Response.TransmitFile(Location);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnAddVer_Click(object sender, EventArgs e)
    {
        try
        {
            clearVersion();
            txtVersion.Focus();
            btnUpdate.Visible = false;
            btnSwitch.Visible = false;
            btnSwitchhtml.Visible = false;
            tblMainGrid.Visible = false;
            tblGrid.Visible = true;
            //tblFormView.Visible = false;
            btnBackVer.Visible = true;
            tblModified.Visible = false;
            btnAddVers.Visible = false;
            //imgNoFile.Visible = true;
            pdfIFrame.Visible = false;
            btnSave.Visible = true;
            //btnSaveNew.Text = "Save";
            btnSwitch.Visible = false;
            //gvTerms.DataSource = null;
            //gvTerms.DataBind();
            clear();
            btnSumbit.Text = "Save";
            lblChangePosValue.Text = "0";
            lblChangeSeq.Text = "0";
            lblApplyChangeSeq.Text = "0";
            lblTermId.Text = "0";

            //dvFileUpload.Visible = true;
            //dvFileView.Visible = false;

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        try
        {
            clear();

            tblGrid.Visible = true;
            btnBackVer.Visible = true;
            btnUpdate.Visible = true;
            btnSwitch.Visible = true;
            btnSwitchhtml.Visible = true;

            //dvFileUpload.Visible = true;
            //dvFileView.Visible = false;
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnBackVer_Click(object sender, EventArgs e)
    {
        try
        {
            BindVersionGrid();
            tblMainGrid.Visible = true;
            tblGrid.Visible = false;
            btnSave.Visible = false;
            if (lblButtonRights.Text == "V")
            {
                btnAddVers.Visible = false;
                btnBackVer.Visible = false;
                btnUpdate.Visible = false;
            }
            else
            {
                btnAddVers.Visible = true;
                btnBackVer.Visible = false;
                btnUpdate.Visible = false;
            }

            //dvFileUpload.Visible = true;
            //dvFileView.Visible = false;

            tblModified.Visible = false;

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnSaveVersionNew_Click(object sender, EventArgs e)
    {
        string[] FetSetup = new string[15];

        string[] FetSetup1 = new string[15];
        string status = string.Empty;
        string result = string.Empty;
        string result123 = string.Empty;
        string Message = string.Empty;

        try
        {
            DataTable dtExist = genclass.GetDataTable("SELECT * From TermsAndCondition_Version WHERE VERSION='" + txtVersion.Text.Trim() + "' AND EffectiveFrom='" + txtEffFrom.Text + "'");

            if (dtExist.Rows.Count > 0)
            {
                lblalert.Text = "Version " + txtVersion.Text + " already available for the effective from " + txtEffFrom.Text + "";
                mpealert.Show();
                return;
            }

            FetSetup[0] = "I";
            Message = "has been saved";
            FetSetup[1] = Convert.ToInt32(0).ToString();
            FetSetup[2] = txtVersion.Text.Trim();
            FetSetup[3] = txtEffFrom.Text.Trim();
            FetSetup[4] = rblActive.SelectedValue.Trim();
            FetSetup[5] = txtNotes.Text.Trim();
            FetSetup[6] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

            result = SaveVersion(FetSetup);

            if (result != "0")
            {
                lblVersionid.Text = result.ToString().Trim();
                btnSumbit_Click(sender, e);
            }

            BindVersionGrid();

            //BindGrid(lblVersionid.Text);

            tblMainGrid.Visible = false;
            tblGrid.Visible = true;

            //imgNoFile.Visible = true;
            pdfIFrame.Visible = false;

            btnAddVers.Visible = false;
            btnBackVer.Visible = true;
            btnUpdate.Visible = true;
            btnSwitch.Visible = true;
            btnSwitchhtml.Visible = true;
            btnSave.Visible = false;

            lblalert.Text = "Version [ " + txtVersion.Text + " ] has been saved successfully";
            mpealert.Show();

        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnUpdateVersion_Click(object sender, EventArgs e)
    {
        string[] FetSetup = new string[15];
        string status = string.Empty;
        string result = string.Empty;
        string Message = string.Empty;

        try
        {
            FetSetup[0] = "U";
            Message = "have been saved";
            FetSetup[1] = Convert.ToInt32(lblVersionid.Text.Trim()).ToString();
            FetSetup[2] = txtVersion.Text.Trim();
            FetSetup[3] = txtEffFrom.Text.Trim();
            FetSetup[4] = rblActive.SelectedValue.Trim();
            FetSetup[5] = txtNotes.Text.Trim();
            FetSetup[6] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();

            result = SaveVersion(FetSetup);
            if (result != "0")
            {
                lblVersionid.Text = result.ToString().Trim();
                btnSumbit_Click(sender, e);
            }

            lblalert.Text = "Version [ " + txtVersion.Text + " ] has been updated successfully";
            clearVersion();
            tblMainGrid.Visible = true;

            tblGrid.Visible = false;
            tblModified.Visible = false;

            btnUpdate.Visible = false;
            btnSave.Visible = false;
            btnBackVer.Visible = false;

            btnAddVers.Visible = true;

            BindVersionGrid();

            mpealert.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public string SaveVersion(string[] arrVersion)
    {
        SqlParameter[] saveOpeningBalanceParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "Version_Setup_INS", false);
        saveOpeningBalanceParam[0].Value = arrVersion[0];//Manage Type
        saveOpeningBalanceParam[1].Value = arrVersion[1];//Version Id
        saveOpeningBalanceParam[2].Value = arrVersion[2];//Version
        saveOpeningBalanceParam[3].Value = arrVersion[3];//Eff From
        saveOpeningBalanceParam[4].Value = arrVersion[4];//Active
        saveOpeningBalanceParam[5].Value = arrVersion[5];//Notes
        saveOpeningBalanceParam[6].Value = arrVersion[6];//Created By
        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "Version_Setup_INS", saveOpeningBalanceParam).ToString();
        return obj.ToString();
    }

    public DataTable BindVerGrid()
    {
        DataTable dtresult = new DataTable();
        string result = string.Empty;
        object Existes = 0;
        SqlParameter[] savePaymentParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "Version_Setup_INS", false);
        savePaymentParam[0].Value = "S";//Manage Type
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.StoredProcedure, "Version_Setup_INS", savePaymentParam).Tables[0];
    }

    public void CreatePDF_NEW(string strReqID, string strUserID, string strUserName)
    {
        try
        {
            string fileUrl = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\JE" + strReqID + @"\";
            string fileNameTC = "Terms_And_Condition_Top.pdf";

            if (!Directory.Exists(fileUrl))
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                Directory.CreateDirectory(fileUrl);
            }

            if (File.Exists(fileUrl + fileNameTC))
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                File.Delete(fileUrl + fileNameTC);
            }

            bool exists = File.Exists(fileUrl + @"\Terms_And_Condition_Sign.pdf");
            if (exists)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                File.Delete(fileUrl + @"\Terms_And_Condition_Sign.pdf");
            }

            File.Copy(@"D:\jetedgefiles\EMPTYLEG\Termsandcondition\Terms_And_Condition_Sign.pdf", fileUrl + @"\Terms_And_Condition_Sign.pdf");

            //if (fuFile.HasFile)
            //{
            //    bool exists = Directory.Exists(fileUrl + fuFile.FileName);
            //    if (exists)
            //    {
            //        System.GC.Collect();
            //        System.GC.WaitForPendingFinalizers();

            //        File.Delete(fileUrl + fuFile.FileName);
            //    }

            //    fuFile.SaveAs(fileUrl + removeSpecialChar(fuFile.FileName));
            //    lblSigningFile.Text = fuFile.FileName;
            //}

            Save_PDFCHTR(fileUrl, fileNameTC, lblSigningFile.Text, strReqID, strUserID, strUserName);
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    public static string removeSpecialChar(string input)
    {
        return Regex.Replace(input, "[^0-9A-Za-z.-]", "_");
    }
    private void Save_PDFCHTR(string strPath, string fileNameTC, string fileNameSign, string strReqID, string struserID, string strUserName)
    {
        try
        {
            DataTable dtPDFMAster = genclass.GetDataTable("SELECT  * FROM TermsAndConditions_V1 where Versionid='" + strReqID + "' order by OrderSequence asc");

            string htmlText = dtPDFMAster.Rows[0]["DescriptionPDF"].ToString().Replace(@"""", "\"").Replace("<br>", "<br />");

            #region Html File
            string filename = "Terms_And_Condition.html";
            StreamWriter swXLS = new StreamWriter(strPath + filename);
            if (dtPDFMAster.Rows.Count > 0)
            {
                string strHTML = dtPDFMAster.Rows[0]["Description"].ToString() + dtPDFMAster.Rows[0]["SignatureDesc"].ToString();
                swXLS.Write(strHTML);
                swXLS.Close();
            }
            #endregion

            #region TC Top Section

            Document doc = new Document(iTextSharp.text.PageSize.A4, 10f, 20f, 10f, 20f);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
                writer.PageEvent = new PDFFooter();
                doc.Open();

                iTextSharp.text.Font fontTinyItalic = FontFactory.GetFont("Calibri", 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                PdfPTable ParentTable4 = new PdfPTable(1);
                ParentTable4.WidthPercentage = 100;
                ParentTable4.DefaultCell.MinimumHeight = 812f;
                ParentTable4.DefaultCell.BorderWidth = 1;

                PdfPTable SubTable4 = new PdfPTable(1);
                SubTable4.WidthPercentage = 100;
                SubTable4.DefaultCell.BorderWidth = 1;

                //First Row
                PdfPCell cellP9 = new PdfPCell();

                string CSS_STYLE = "span { font-size: 13pt; } MsoNormal { margin: 16px 13px; }"
                    + "p { font-size: 13pt; margin: 16px 13px;  }";

                using (StringReader sr = new StringReader(htmlText))
                {
                    ElementList elements = XMLWorkerHelper.ParseToElementList(htmlText, CSS_STYLE);
                    if (htmlText.Length > 0)
                    {
                        if (elements.Count > 0)
                        {
                            foreach (IElement e in elements)
                            {
                                //Add those elements to the paragraph
                                cellP9.AddElement(e);
                                cellP9.Border = 0;
                                cellP9.Padding = 5;
                                cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
                            }
                        }
                        else
                        {
                            cellP9 = new PdfPCell(new Phrase(htmlText));
                            cellP9.Border = 0;
                            cellP9.Padding = 5;
                            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
                        }
                    }
                }

                SubTable4.AddCell(cellP9);

                ParentTable4.AddCell(SubTable4);
                doc.Add(ParentTable4);
                doc.Close();

                byte[] bytes1 = memoryStream.ToArray();

                File.WriteAllBytes(strPath + @"\" + fileNameTC, bytes1);
            }

            #endregion

            if (File.Exists(strPath + @"\Terms_And_Condition_Sign.pdf"))
            {
                File.Delete(strPath + @"\Terms_And_Condition_Sign.pdf");
            }

            File.Copy(@"D:\jetedgefiles\EMPTYLEG\Termsandcondition\Terms_And_Condition_Sign.pdf", strPath + @"\Terms_And_Condition_Sign.pdf");
            fileNameSign = "Terms_And_Condition_Sign.pdf";

            //if (fileNameSign.Length > 0)
            //{
            //    if (!File.Exists(strPath + @"\" + fileNameSign))
            //    {
            //        File.Copy(@"D:\jetedgefiles\EMPTYLEG\Termsandcondition\Terms_And_Condition_Sign.pdf", strPath + @"\" + fileNameSign);
            //    }
            //}
            //else
            //{
            //    File.Copy(@"D:\jetedgefiles\EMPTYLEG\Termsandcondition\Terms_And_Condition_Sign.pdf", strPath + @"\Terms_And_Condition_Sign.pdf");
            //    fileNameSign = "Terms_And_Condition_Sign.pdf";
            //}

            MergePDF(strPath + @"\" + fileNameTC, strPath + @"\" + fileNameSign, strPath, "Terms_And_Condition.pdf");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    private static void MergePDF(string File1, string File2, string OutFilePath, string OutFileName)
    {
        string[] fileArray = new string[3];
        fileArray[0] = File1;
        fileArray[1] = File2;

        PdfReader reader = null;
        iTextSharp.text.Document sourceDocument = null;
        PdfCopy pdfCopyProvider = null;
        PdfImportedPage importedPage;
        string outputPdfPath = OutFilePath + OutFileName;

        sourceDocument = new iTextSharp.text.Document();
        pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));

        //output file Open  
        sourceDocument.Open();


        //files list wise Loop  
        for (int f = 0; f < fileArray.Length - 1; f++)
        {
            int pages = TotalPageCount(fileArray[f]);

            reader = new PdfReader(fileArray[f]);
            //Add pages in new file  
            for (int i = 1; i <= pages; i++)
            {
                importedPage = pdfCopyProvider.GetImportedPage(reader, i);
                pdfCopyProvider.AddPage(importedPage);
            }

            reader.Close();
        }
        //save the output file  
        sourceDocument.Close();
    }
    private static int TotalPageCount(string file)
    {
        using (StreamReader sr = new StreamReader(System.IO.File.OpenRead(file)))
        {
            Regex regex = new Regex(@"/Type\s*/Page[^s]");
            MatchCollection matches = regex.Matches(sr.ReadToEnd());

            return matches.Count;
        }
    }

    protected void btnDeletVers_Click(object sender, EventArgs e)
    {
        ImageButton lnkFileName = (ImageButton)sender;
        GridViewRow gvFleet = (GridViewRow)lnkFileName.Parent.Parent;
        Label lblRowid = (Label)gvFleet.FindControl("lblVersionId");

        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete from TermsAndConditions_V1 WHERE VersionID='" + lblRowid.Text + "'");

        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete from TermsAndCondition_Version WHERE Rowid='" + lblRowid.Text + "'");

        string strFilePath = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\JE" + lblRowid.Text + @"\" + "Terms_And_Condition.pdf"; ;

        string strFilePathtml = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\JE" + lblRowid.Text + @"\" + "Terms_And_Condition.html"; ;

        if (File.Exists(strFilePath))
        {
            File.Delete(strFilePath);
        }
        if (File.Exists(strFilePathtml))
        {
            File.Delete(strFilePathtml);
        }
        BindVersionGrid();
    }

    private void GetPDFView()
    {
        pdfIFrame.Attributes.Add("style", "width: 100%; border: 0; background-color: white; height: 88vh");
        string strFilePath = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\JE" + lblVersionid.Text + @"\" + "Terms_And_Condition.pdf";
        string NewPath = strFilePath.Replace(@"D:\", SecretsBLL.InvoiceNewPath);
        pdfIFrame.Attributes.Add("src", null);
        pdfIFrame.Attributes.Add("src", NewPath + "#view=FitH");
    }
    protected void btnSwitch_Click(object sender, EventArgs e)
    {
        try
        {
            string strPath = @"D:\jetedgefiles\EMPTYLEG\TermsAndCondition\JE" + lblVersionid.Text + @"\";
            string fileNameTC = "Terms_And_Condition_Top.pdf";
            string htmlView = string.Empty;

            DataTable dtSignatureHtml = genclass.GetDataTable("Select RowId, SignatureHtml FROm TermsAndConditions_Signature");
            if (dtSignatureHtml.Rows.Count > 0)
            {
                htmlView = dtSignatureHtml.Rows[0]["SignatureHtml"].ToString();
            }

            #region Html File
            string filename = "Terms_And_Condition.html";
            StreamWriter swXLS = new StreamWriter(strPath + filename);

            string strHTML = EditorMessage.Content + "<br/> <br/>" + htmlView;
            swXLS.Write(strHTML);
            swXLS.Close();

            #endregion

            #region TC Top Section

            string htmlText = EditorMessage.Content.Replace("pt;", "px;").Replace("12.0pt", "12px").Replace("15.0pt", "14px").Replace("18.0pt", "16px").Replace("24.0pt", "18px").Replace("12pt", "12px").Replace("15pt", "14px").Replace("18pt", "16px").Replace("24pt", "18px");
            Document doc = new Document(iTextSharp.text.PageSize.A4, 10f, 20f, 10f, 20f);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
                writer.PageEvent = new PDFFooter();
                doc.Open();

                iTextSharp.text.Font fontTinyItalic = FontFactory.GetFont("Calibri", 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                PdfPTable ParentTable4 = new PdfPTable(1);
                ParentTable4.WidthPercentage = 100;
                ParentTable4.DefaultCell.MinimumHeight = 812f;
                ParentTable4.DefaultCell.BorderWidth = 1;

                PdfPTable SubTable4 = new PdfPTable(1);
                SubTable4.WidthPercentage = 100;
                SubTable4.DefaultCell.BorderWidth = 1;

                //First Row
                PdfPCell cellP9 = new PdfPCell();

                string CSS_STYLE = "span { font-size: 13pt; } MsoNormal { margin: 16px 13px; }"
                    + "p { font-size: 13pt; margin: 16px 13px;  }";

                using (StringReader sr = new StringReader(htmlText))
                {
                    ElementList elements = XMLWorkerHelper.ParseToElementList(htmlText, CSS_STYLE);
                    foreach (IElement e1 in elements)
                    {
                        //Add those elements to the paragraph
                        cellP9.AddElement(e1);

                        cellP9.Border = 0;
                        cellP9.Padding = 5;
                        cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
                    }
                }

                SubTable4.AddCell(cellP9);

                ParentTable4.AddCell(SubTable4);
                doc.Add(ParentTable4);
                doc.Close();

                byte[] bytes1 = memoryStream.ToArray();

                File.WriteAllBytes(strPath + @"\" + fileNameTC, bytes1);
            }

            #endregion

            MergePDF(strPath + @"\" + fileNameTC, strPath + @"\" + lblSigningFile.Text, strPath, "Terms_And_Condition.pdf");

            GetPDFView();

            mpeterms.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public void GethtmlView()
    {
        DataTable dtPDFMAster = genclass.GetDataTable("SELECT  * FROM TermsAndConditions_V1 where Versionid='" + lblVersionid.Text.Trim() + "' order by OrderSequence asc");
        string htmlView = string.Empty;

        if (dtPDFMAster.Rows.Count > 0)
        {
            htmlView = dtPDFMAster.Rows[0]["Description"].ToString() + "<br/> <br/>" + dtPDFMAster.Rows[0]["SignatureDesc"].ToString();
        }

        lblhtmlView.Text = htmlView;
    }
    protected void btnSwitchHTML_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtSignatureHtml = genclass.GetDataTable("Select RowId, SignatureHtml FROm TermsAndConditions_Signature");
            string htmlView = string.Empty;

            if (dtSignatureHtml.Rows.Count > 0)
            {
                htmlView = dtSignatureHtml.Rows[0]["SignatureHtml"].ToString();
            }

            lblhtmlView.Text = EditorMessage.Content + "<br/> <br/>" + htmlView;

            mpePreview.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public string Terms_Copy(string TailCopyFrom, string TailCopyTo, string TailIdCopyTo, string UserId)
    {
        string result = string.Empty;

        SqlParameter[] ScreenRightsParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[QUOTE_TERMS_Copy]", false);

        ScreenRightsParam[0].Value = TailCopyFrom;  //@TailCopyFrom
        ScreenRightsParam[1].Value = TailCopyTo;    //@TailCopyTo
        ScreenRightsParam[2].Value = TailIdCopyTo;  //@TailIdCopyTo
        ScreenRightsParam[3].Value = UserId;        //@UserId

        result = SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, "[QUOTE_TERMS_Copy]", ScreenRightsParam).ToString();

        return result.ToString();

    }

    public string SaveTermsAndConditions(string[] arrsaveFetSetup)
    {
        SqlParameter[] saveOpeningBalanceParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "TermsAndCondition_Setup_Ins", false);

        saveOpeningBalanceParam[0].Value = arrsaveFetSetup[0];//Manage Type
        saveOpeningBalanceParam[1].Value = arrsaveFetSetup[1];//Terms Id
        saveOpeningBalanceParam[2].Value = arrsaveFetSetup[2];//Header
        saveOpeningBalanceParam[3].Value = arrsaveFetSetup[3];//Description
        saveOpeningBalanceParam[4].Value = arrsaveFetSetup[4];//Order Sequence
        saveOpeningBalanceParam[5].Value = arrsaveFetSetup[5];//Creatd By
        saveOpeningBalanceParam[6].Value = arrsaveFetSetup[6];//Version id
        saveOpeningBalanceParam[7].Value = arrsaveFetSetup[7];//Signing Description
        saveOpeningBalanceParam[8].Value = arrsaveFetSetup[8];//DescriptionPDF

        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "TermsAndCondition_Setup_Ins", saveOpeningBalanceParam).ToString();
        return obj.ToString();
    }

    public DataTable BindGrid(string strManageType, string strVersionId)
    {
        DataTable dtresult = new DataTable();
        string result = string.Empty;
        object Existes = 0;
        SqlParameter[] savePaymentParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "TermsAndCondition_Setup_Sel", false);
        savePaymentParam[0].Value = strManageType;
        savePaymentParam[1].Value = strVersionId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.StoredProcedure, "TermsAndCondition_Setup_Sel", savePaymentParam).Tables[0];
    }

    public override void VerifyRenderingInServerForm(Control control)
    {

    }

    public class PDFFooter : PdfPageEventHelper
    {
        // write on end of each page
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);

            PdfPTable SubTable4 = new PdfPTable(10);
            SubTable4.TotalWidth = 575F;
            PdfPCell cellP8 = new PdfPCell(SubTable4);
            Font CalibriBoldRight = FontFactory.GetFont("Calibri", 8, Font.NORMAL);

            //First Row
            PdfPCell cellP9 = new PdfPCell(new Phrase("(Jet Edge FLYT Invoice - Rev.1.0.0.0)", FontFactory.GetFont("Calibri", 7, Font.NORMAL)));
            cellP9.Border = 0;
            cellP9.Colspan = 10;
            cellP9.Rowspan = 5;
            cellP9.HorizontalAlignment = Element.ALIGN_LEFT;
            SubTable4.AddCell(cellP9);

            //cellP9 = new PdfPCell(new Phrase("Jet Edge FLYT", FontFactory.GetFont("Calibri", 8, Font.NORMAL)));
            //cellP9.Border = 0;
            //cellP9.Colspan = 3;
            //cellP9.Rowspan = 5;
            //cellP9.HorizontalAlignment = Element.ALIGN_CENTER;
            //SubTable4.AddCell(cellP9);

            //cellP9 = new PdfPCell(new Phrase("Printed : " + genclass.Long_Date_Time(DateTime.Now) + "", FontFactory.GetFont("Calibri", 8, Font.NORMAL)));
            //cellP9.Border = 0;
            //cellP9.Colspan = 3;
            //cellP9.Rowspan = 5;
            //cellP9.HorizontalAlignment = Element.ALIGN_RIGHT;
            //SubTable4.AddCell(cellP9);

            SubTable4.WriteSelectedRows(0, -1, 10, document.Bottom, writer.DirectContent);
        }

        //write on close of document
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
        }
    }
}