﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Net;
using Microsoft.Exchange.WebServices.Data;

public partial class Views_PrivacyPolicySetup1 : System.Web.UI.Page
{

    #region Global Declaration

    protected BusinessLayer.PrivacyEmailBLL objPrivacy = new BusinessLayer.PrivacyEmailBLL();

    #endregion

    public int linenum = 0;
    public string MethodName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            try
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "PrivacyPolicy Setup");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Admin";
                lblSubHeader.InnerText = "Privacy Policy Email Setup";

                btnMailSetup.Visible = true;

                List();
            }
            catch (System.Exception ex)
            {
                linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
            }
        }

    }

    protected void btnMailSetup_Click(object sender, EventArgs e)
    {

        mpePrivacy.Show();

    }

    protected void btnAddToList_Click(object sender, EventArgs e)
    {
        try
        {


            if (btnAddToList.Text == "Add to list")
            {
                objPrivacy.ManageTye = "I";
            }

            objPrivacy.PID = hdPPSID.Value;
            objPrivacy.FirstName = txtFirstName.Text.Trim();
            objPrivacy.LastName = txtLastName.Text.Trim();
            objPrivacy.EmailID = txtEmail.Text.Trim();


            objPrivacy.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();


            int retVal = objPrivacy.Save(objPrivacy);

            if (retVal > 0)
            {
                if (btnAddToList.Text == "Add to list")
                {
                    lblalert.Text = "PrivacyPolicy Setup Details has been added successfully";
                    mpealert.Show();

                }
                else
                {
                    lblalert.Text = "An error has occured while processing your request";
                    mpealert.Show();
                }

                mpePrivacy.Show();
                List();
                Clear();
            }
            else
            {
                lblMessage.Value = "An error has occured while processing your request";
            }
        }
        catch (System.Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    public void List()
    {
        gvEmail.DataSource = objPrivacy.Select();
        gvEmail.DataBind();

        if (gvEmail.Rows.Count > 0)
        {
            gvEmail.Visible = true;
        }
        else
        {
            gvEmail.Visible = false;
            Clear();
        }
    }

    public void Clear()
    {
        try
        {
            hdPPSID.Value = "0";
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;

            Label lblPID = (Label)gvrow.FindControl("lblPID");

            objPrivacy.DeletUser(lblPID.Text);

            lblalert.Text = "Privacy Policy Setup Details has been deleted successfully";
            mpealert.Show();

            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

}