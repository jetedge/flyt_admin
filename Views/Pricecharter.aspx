﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="Pricecharter.aspx.cs" Inherits="Views_Pricecharter" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .buttongrid
        {
            display: inline-block;
            text-align: center;
            vertical-align: middle;
            padding: 5px 5px;
            border: 1px solid rgb(178, 30, 40);
            border-radius: 2px;
            background-color: rgb(178, 30, 40);
            font-size: 12px;
            color: #FFFFFF;
            text-decoration: none;
            font-weight: bold;
        }
        
        #MainContent_gvSimpleSearch_bokknow:hover
        {
            color: white;
        }
        
        #MainContent_gvCharter tbody tr td
        {
            padding-top: 0.2rem;
            padding-bottom: 0.2rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="kt-portlet" id="tblGrid" runat="server">
        <div class="kt-portlet__body p-2">
            <div class="form-group row">
                <div class="col-xl-3">
                </div>
                <div class="col-xl-6 text-center">
                    <div class="row m-0">
                        <div class="col-xl-3 text-center">
                            <label>
                                Display By
                            </label>
                        </div>
                        <div class="col-xl-9">
                            <asp:RadioButtonList ID="rblDisplay" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Customer Landing Trips" Value="C"></asp:ListItem>
                                <asp:ListItem Text="All Trips" Value="A" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                </div>
            </div>
            <div class="row m-0">
                <div class="col-xl-12 p-0 boxshadow table-responsive">
                    <asp:GridView runat="server" ID="gvSimpleSearch" AutoGenerateColumns="false" Width="100%"
                        CssClass="table" HeaderStyle-CssClass="thead-dark" role="grid" Style="padding-left: 0px;
                        margin-left: 0px" OnSelectedIndexChanged="gvSimpleSearch_SelectedIndexChanged"
                        OnRowDataBound="gvSimpleSearch_OnRowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Date" HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Wrap="false" HeaderStyle-Width="7%" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" Style="text-transform: uppercase  !important;" runat="server"
                                        Text='<%# Eval("DateFormat") %>'> </asp:Label>
                                    <asp:Label ID="lbltosplit" runat="server" Visible="false" Text="-"> </asp:Label>
                                    <asp:Label ID="lbltodate" Style="text-transform: uppercase  !important;" runat="server"
                                        Visible="false" Text='<%# Eval("ADate", "{0: d}") %>'> </asp:Label>
                                    <asp:Label ID="lblDateFrom" runat="server" Visible="false" Text='<%# Eval("DateFormat") %>' />
                                    <asp:Label ID="lblDateTo" runat="server" Visible="false" Text='<%# Eval("ADate") %>' />
                                    <asp:Label ID="lblDDate1" runat="server" Visible="false" Text='<%# Eval("DDate1") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tail #" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                HeaderStyle-Width="7%" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblTailNo" runat="server" Text='<%# Eval("TailNo") %>'> </asp:Label>
                                    <asp:Label ID="lblCDFlag" runat="server" Text='<%# Eval("CDFlag") %>' Visible="false"> </asp:Label>
                                    <asp:Label ID="lblDtime" runat="server" Text='<%# Eval("FOSTIME") %>' Visible="false"> </asp:Label>
                                    <asp:Label ID="lblAtime" runat="server" Text='<%# Eval("ATime") %>' Visible="false"> </asp:Label>
                                    <asp:Label ID="lblstartcity" runat="server" Text='<%# Eval("StartCity") %>' Visible="false"> </asp:Label>
                                    <asp:Label ID="lblEndcity" runat="server" Text='<%# Eval("EndCity") %>' Visible="false"> </asp:Label>
                                    <asp:Label ID="lblDDate" runat="server" Text='<%# Eval("DDate", "{0:MMM d yyyy}") %>'
                                        Visible="false"> </asp:Label>
                                    <asp:Label ID="lblstart" Style="font-size: 16pt" runat="server" Text='<%# Eval("Start") %>'
                                        Visible="false"> </asp:Label>
                                    <asp:Label ID="lblEnd" Style="font-size: 16pt" runat="server" Text='<%# Eval("End") %>'
                                        Visible="false"> </asp:Label>
                                    <asp:Label ID="lblFlyingHours" runat="server" Visible="false" Text='<%# Eval("FLTTIMEMINS") %>'></asp:Label>
                                    <asp:Label ID="lblroutes" runat="server" Visible="false" Text='<%# Eval("routes") %>'></asp:Label>
                                    <asp:Label ID="lbltailflag" runat="server" Visible="false" Text='<%# Eval("tailflag") %>'></asp:Label>
                                    <asp:Label ID="lblTripFlag" runat="server" Visible="false" Text='<%# Eval("tripflag") %>'></asp:Label>
                                    <asp:Label ID="lblSplitType" runat="server" Visible="false" Text='<%# Eval("Splittype") %>'></asp:Label>
                                    <asp:Label ID="lblModFlag" runat="server" Visible="false" Text='<%# Eval("Mod_flag") %>'></asp:Label>
                                    <asp:Label ID="lblDateFlag" runat="server" Visible="false" Text='<%# Eval("DateFlag") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trip #" HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="6%" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lbltripno" runat="server" Text='<%# Eval("tripno") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Leg" HeaderStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-Width="6%" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblleg" runat="server" Text='<%# Eval("leg") %>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="40%">
                                <HeaderTemplate>
                                    <div class="row">
                                        <div class="col-lg-45 col-md-45 col-xs-12 col-sm-12 p-0" id="dvDeparture" runat="server">
                                            Departure
                                        </div>
                                        <div class="col-lg-100 col-md-100 col-xs-12 col-sm-12 text-center" id="dvSeperator"
                                            runat="server">
                                            -
                                        </div>
                                        <div class="col-lg-45 col-md-45 col-xs-12 col-sm-12 p-0" id="dvArrival" runat="server">
                                            Arrival
                                        </div>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row" style="align-items: center;">
                                        <div class="col-lg-45 col-md-45 col-xs-12 col-sm-12 p-0" id="dvDeparture" runat="server">
                                            <asp:Label ID="lblscity" runat="server" Style="text-transform: uppercase  !important;"
                                                Text='<%# Eval("StartCityShow11") %>' />
                                            <asp:Label ID="lblstairport" Style="text-transform: uppercase !important;" runat="server"
                                                Visible="false" Text='<%# Eval("startshow") %>' />
                                            <asp:Label ID="Label2" Style="font-size: 14px" runat="server" Text='<%# Eval("Start") %>'
                                                Visible="false" />
                                        </div>
                                        <div class="col-lg-100 col-md-100 col-xs-12 col-sm-12 text-center" id="dvSeperator"
                                            runat="server">
                                            -
                                        </div>
                                        <div class="col-lg-45 col-md-45 col-xs-12 col-sm-12 p-0" id="dvArrival" runat="server">
                                            <asp:Label ID="lblEcity" Style="text-transform: uppercase !important;" runat="server"
                                                Text='<%# Eval("EndCityShow11") %>' />
                                            <asp:Label ID="lblendairport" Style="text-transform: uppercase !important;" runat="server"
                                                Text='<%# Eval("Endshow") %>' Visible="false" />
                                            <asp:Label ID="Label3" Style="font-size: 14px" runat="server" Visible="false" Text='<%# Eval("End") %>' />
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fleet" HeaderStyle-Width="13%" HeaderStyle-Wrap="true"
                                ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblAircraftSubName" runat="server" Text='<%# Eval("AircraftSubName") %>'
                                        Visible="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aircraft Capacity <br/> (Seats)" ItemStyle-Wrap="false"
                                HeaderStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblNoSeats" runat="server" Text='<%# Eval("NoOFSeats") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Earliest Start <br/> Date time" ItemStyle-Wrap="true"
                                HeaderStyle-Width="13%" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblEDepTime" runat="server" Text='<%# Eval("DepartNotes") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Allowed Booking" HeaderStyle-Width="9%" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDontshow" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="14%">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" Font-Size="14px" Style="text-align: right" runat="server"
                                        Text='<%# "$" + Eval("Pricing","{0:N0}") %>' />
                                    <asp:Label ID="Label1" Font-Size="14px" Style="text-align: center" runat="server"
                                        Text='<%# Eval("Priceflag") %>' Visible="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price" HeaderStyle-Width="12%" ItemStyle-Width="12%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="bokknow" CommandName="Select" runat="server" Text="Price charter"
                                        class="buttongrid"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Persons Viewing" HeaderStyle-Width="6%" ItemStyle-Width="6%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPersons" CommandName="Persons" OnClick="lnkPersons_Click"
                                        ToolTip="Click to view Persons looking in to this leg" Style="text-decoration: underline;"
                                        ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# Eval("PersonView") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="Button1" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="mpePersonViewing" runat="server" TargetControlID="Button1"
        Y="30" PopupControlID="pnlPersonViewing" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlPersonViewing" runat="server" BackColor="White" Width="75%" Style="display: none;
        background-color: rgb(17,24,32); color: rgb(255,255,255)">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid #EBEDF3;">
                <h5 class="modal-title" id="exampleModalLabel">
                    Persons Viewing</h5>
                <button type="button" class="btn btn-secondary" id="btnClose" data-dismiss="modal">
                    Close</button>
            </div>
            <div class="modal-body p-3">
                <div class="form-group row">
                    <div class="col-lg-3">
                        Date :
                        <asp:Label ID="lblPopDate" runat="server" />
                    </div>
                    <div class="col-lg-3">
                        Tail :
                        <asp:Label ID="lblPopTail" runat="server" />
                    </div>
                    <div class="col-lg-3">
                        Trip # :
                        <asp:Label ID="lblPopTrip" runat="server" />
                    </div>
                    <div class="col-lg-3">
                        Leg :
                        <asp:Label ID="lblPopLeg" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        Route :
                        <asp:Label ID="lblPopDeparture" runat="server" />
                        -
                        <asp:Label ID="lblPopArrival" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-12">
                        <asp:GridView ID="gvPersonViewing" runat="server" HeaderStyle-CssClass="thead-dark"
                            RowStyle-ForeColor="White" PagerStyle-CssClass="pager" AutoGenerateColumns="false"
                            PageSize="500" CssClass="table" AllowPaging="True" Width="100%" EmptyDataText="No Persons Viewing found.">
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-Width="11%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserName" Text='<%# Eval("UserName") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User Type" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-Width="7%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRole" Text='<%# Eval("Role") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Book Start Time" HeaderStyle-HorizontalAlign="left"
                                    ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedOn" Text='<%# Eval("CreatedOn") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TC Accepted On" HeaderStyle-HorizontalAlign="left"
                                    ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTCAcceptedOn" Text='<%# Eval("TCAcceptedOn") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FLYT Requested On" HeaderStyle-HorizontalAlign="left"
                                    HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTransdate" Text='<%# Eval("Transdate") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btnShowPopup"
        PopupControlID="pnlpopup1" CancelControlID="btncancelpopup" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlpopup1" runat="server" BackColor="White" Width="50%" Style="display: none;
        background-color: rgb(17,24,32); color: rgb(255,255,255)">
        <table width="100%" style="width: 100%; height: 100%; padding-left: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr>
                <td style="color: White; font-weight: bold; font-size: larger; vertical-align: middle;
                    padding-left: 20px" align="left">
                    Price Charter
                </td>
                <td align="right" style="padding-right: 20px; padding-top: 5px; vertical-align: middle">
                    <asp:LinkButton ID="btncancelpopup" runat="server" Style="color: rgb(255,255,255);
                        padding-top: 3px; padding-bottom: 3px; text-decoration: none" CssClass="btn btn-danger"
                        Text="Close" CausesValidation="false" />
                </td>
            </tr>
        </table>
        <div id="divscroll" runat="server" style="width: 100%; height: 500px; overflow: auto">
            <table style="width: 100%">
                <tr style="height: 30px;">
                    <td colspan="6">
                    </td>
                </tr>
                <tr style="height: 30px;">
                    <td style="font-weight: bold; text-align: left; padding-left: 5%; width: 33%" nowrap="nowrap">
                        Tail # &nbsp&nbsp&nbsp
                        <asp:Label ID="lblAircraftType" Style="font-weight: normal;" runat="server"></asp:Label>
                    </td>
                    <td style="font-weight: bold; text-align: left; width: 33%; padding-left: 33px" nowrap="nowrap">
                        Start Time &nbsp&nbsp&nbsp
                        <asp:Label ID="lblStartTIme" Style="font-weight: normal;" runat="server"></asp:Label>
                    </td>
                    <td style="font-weight: bold; text-align: left; width: 33%" nowrap="nowrap">
                        Route &nbsp&nbsp&nbsp
                        <asp:Label ID="FromAirport" Style="font-weight: normal;" runat="server"></asp:Label>
                        <svg style='margin-left: 10px; margin-right: 10px; text-align: center' xmlns='http://www.w3.org/2000/svg'
                            xmlns:xlink='http://www.w3.org/1999/xlink' aria-hidden='true' focusable='false'
                            width='1.10em' height='0.80em' style='-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg);
                            transform: rotate(360deg);' preserveaspectratio='xMidYMid meet' viewbox='0 0 640 512'
                            class='iconify' data-icon='fa-solid:plane-departure' data-inline='false'><path d='M624 448H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h608c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zM80.55 341.27c6.28 6.84 15.1 10.72 24.33 10.71l130.54-.18a65.62 65.62 0 0 0 29.64-7.12l290.96-147.65c26.74-13.57 50.71-32.94 67.02-58.31c18.31-28.48 20.3-49.09 13.07-63.65c-7.21-14.57-24.74-25.27-58.25-27.45c-29.85-1.94-59.54 5.92-86.28 19.48l-98.51 49.99l-218.7-82.06a17.799 17.799 0 0 0-18-1.11L90.62 67.29c-10.67 5.41-13.25 19.65-5.17 28.53l156.22 98.1l-103.21 52.38l-72.35-36.47a17.804 17.804 0 0 0-16.07.02L9.91 230.22c-10.44 5.3-13.19 19.12-5.57 28.08l76.21 82.97z' fill='currentColor'></path></svg>
                        <asp:Label ID="lblToAirport" Style="font-weight: normal;" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="height: 30px;">
                    <td style="font-weight: bold; text-align: left; padding-left: 5%; width: 33%" nowrap="nowrap">
                        Trip # &nbsp&nbsp&nbsp
                        <asp:Label ID="lbltripno" Style="font-weight: normal;" runat="server"></asp:Label>
                    </td>
                    <td style="font-weight: bold; text-align: left; padding-left: 33px; width: 33%" nowrap="nowrap">
                        End Time &nbsp&nbsp&nbsp
                        <asp:Label ID="lblEndTime1" Style="font-weight: normal;" runat="server"></asp:Label>
                    </td>
                    <td style="font-weight: bold; text-align: left; width: 33%" nowrap="nowrap">
                        Time in the air &nbsp&nbsp&nbsp
                        <asp:Label ID="lblTimeintheair" Style="font-weight: normal;" runat="server"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px;">
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="7">
                        <asp:GridView ID="gvCharter" runat="server" PagerStyle-CssClass="pager" EmptyDataText="No Data Found"
                            AutoGenerateColumns="false" CssClass="table" HeaderStyle-CssClass="thead-dark"
                            TabIndex="7" Width="90%" OnRowDataBound="gvCharter_OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' Style="color: White;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="40%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcurrentdate" Font-Size="14px" runat="server" Text='<%# Eval("Current Date") %>'
                                            Style="color: White;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Time" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="40%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcurrenttime" Font-Size="14px" runat="server" Text='<%# Eval("Current time") %>'
                                            Style="color: White;"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Current Price" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                    HeaderStyle-Width="10%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                    <ItemTemplate>
                                        <table width="100%" style="border: 0px">
                                            <tr style="border: 0px">
                                                <td style="width: 70%; border: 0px; padding-right: 5px; color: White" nowrap="nowrap">
                                                    <asp:Label ID="lblpriceflag" Font-Size="14px" Style="text-align: center" runat="server"
                                                        Text='<%# Eval("Priceflag") %>'></asp:Label>
                                                    $
                                                </td>
                                                <td style="width: 30%; border: 0px; color: White" nowrap="nowrap">
                                                    <asp:Label ID="lblcurrentprice" Font-Size="14px" runat="server" Text='<%# Eval("Current Price")  %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; padding-bottom: 10px; padding-top: 10px" colspan="6">
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
