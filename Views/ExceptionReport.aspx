﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="ExceptionReport.aspx.cs" Inherits="Views_ExceptionReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body p-0">
                    <div class="row m-0">
                        <div class="col-lg-12 p-0">
                            <div class="form-group row">

                                <label class="col-2 col-form-label" style="max-width: 11%; text-align:center">
                                    Status
                                </label>
                                <div class="col-3">
                                    <asp:DropDownList ID="ddlstatus" OnSelectedIndexChanged="ddlstatus_onselectedChanged"
                                        Width="80%" CssClass="cusinputuser" runat="server" AutoPostBack="true">
                                        <asp:ListItem Selected="true" Value="Fail" Text="Declined Transaction"></asp:ListItem>
                                        <asp:ListItem Value="twise" Text="Duplicate Transaction"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 11%; text-align:center">
                                    Tail Number
                                </label>
                                <div class="col-2" style="max-width: 11%;">
                                    <asp:TextBox ID="txtTailFilter" class="cusinputuser" placeholder="Enter tail #" runat="server"
                                        Width="100%" AutoPostBack="true" OnTextChanged="textBox1_TextChanged"></asp:TextBox>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 11%;">
                                    Trip Number
                                </label>
                                <div class="col-2" style="max-width: 11%;">
                                    <asp:TextBox ID="txttripno" class="cusinputuser" placeholder="Enter trip #" runat="server"
                                        Width="100%" AutoPostBack="true" OnTextChanged="txttripno_TextChanged"></asp:TextBox>
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 10%;">
                                    From Date<span style="color: Red">*</span>
                                </label>
                                <div class="col-2" style="max-width: 12%;">
                                    <asp:TextBox ID="txtfromdate" class="cusinputuser" Placeholder="Date(MM-DD-YYYY)"
                                        onkeypress="return NumericsIphon(event)" runat="server" Width="98%" AutoPostBack="true"
                                        OnTextChanged="txtfromdate_changed"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtfromdate"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                </div>
                                <label class="col-1 col-form-label">
                                    To Date<span style="color: Red">*</span>
                                </label>
                                <div class="col-2" style="max-width: 12%;">
                                    <asp:TextBox ID="txttodate" class="cusinputuser" Placeholder="Date(MM-DD-YYYY)" onkeypress="return NumericsIphon(event)"
                                        runat="server" Width="98%" AutoPostBack="true" OnTextChanged="txtto_changed"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttodate"
                                        Format="MM-dd-yyyy" Enabled="True">
                                    </asp:CalendarExtender>
                                </div>
                                <div class="col-2" style="max-width: 10%; padding-top: 5px;">
                                    <asp:Button ID="Button2" runat="server" Text="Export To Excel" CssClass="btn btn-danger"
                                        OnClick="btnExport_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="dvGrid" runat="server">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body p-4" id="tblGrid" runat="server">
                    <div class="row m-0" id="dvheader" runat="server" visible="false">
                        <div class="col-xl-12 p-0">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="color: Black; font-size: 22px; background-color: gray;">
                                    JET EDGE
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0" id="dvheader1" runat="server" visible="false">
                        <div class="col-xl-12 p-0">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="color: Black; font-size: 22px; background-color: gray;">
                                    Exception Report
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 p-0">
                            <asp:GridView ID="gvTailDetails" runat="server" PagerStyle-CssClass="pager" AutoGenerateColumns="false"
                                PageSize="25" OnSelectedIndexChanged="gvTailDetails_SelectedIndexChanged" EmptyDataText="No trips found."
                                AllowPaging="True" Width="100%" OnPageIndexChanging="gvTailDetails_PageIndexChanging"
                                CssClass="table" HeaderStyle-CssClass="thead-dark" OnRowDataBound="gvpricing_price_OnRowDataBound"
                                OnRowCommand="ContactsGridView_RowCommandNew">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="textstyle"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-Width="2%"
                                        Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tail Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTailNo" Text='<%# Eval("TailNo") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trip Number" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltripno" Text='<%# Eval("Tripno") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Booked Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" Text='<%# Eval("book","{0:MMMM d, yyyy}") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trip Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" Text='<%# Eval("FromDate","{0:MMMM d, yyyy}") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Airport" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstartair" Text='<%# Eval("ActualStartAirport") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="End Airport" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblendair" Text='<%# Eval("ActualEndAirport") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Time" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstarttime" Text='<%# Eval("FromTime") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ToTime" HeaderText="End Time" ItemStyle-HorizontalAlign="left"
                                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                    <asp:TemplateField HeaderText="Invoice Number" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID='lnkbtn' runat="server" CssClass="lnkbtn" Text='<%# Eval("Invoicenumber") %>'
                                                Style="color: Black;" CommandName="Select"></asp:LinkButton>
                                            <%--<asp:Label ID="lblcreatedon" Text='<%# Eval("Invoicenumber") %>' runat="server"></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice Number" ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-HorizontalAlign="center" HeaderStyle-Width="8%" ItemStyle-Width="8%"
                                        HeaderStyle-Wrap="false" ItemStyle-Wrap="false" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblinvoice" Text='<%# Eval("Invoicenumber") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkstatus" CommandName="Status" runat="server" CommandArgument="<%# Container.DataItemIndex %>"
                                                CssClass="lnkbtn" Text='<%# Eval("PaymentStatus") %>' Style="color: Black;"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"
                                        Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lnkbtn2" Text='<%# Eval("PaymentStatus") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                        HeaderStyle-Width="8%" ItemStyle-Width="8%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltotal" Text='<%# Eval("TotalAmount","{0:c2}") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Left" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btntrans" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="mpetrans" runat="server" TargetControlID="btntrans" PopupControlID="pnltrans"
        BackgroundCssClass="modalBackground" Y="50" CancelControlID="btncancelpopup">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnltrans" runat="server" CssClass="modalPopup" BackColor="Black" Style="display: none;
        float: right; align: right; color: White; width: 30%; position: fixed">
        <table style="width: 100%; padding-left: 5%">
            <tr style="padding-bottom: 20px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td style="color: White; font-size: 16px; vertical-align: middle">
                    Booking Details
                </td>
                <td style="height: 30px; margin-right: 2%; padding-right: 3%; color: White; font-weight: bold;
                    border-color: none; padding-bottom: 10px; padding-top: 10px; font-size: larger;
                    vertical-align: middle" align="right" colspan="5">
                    <asp:LinkButton ID="btncancelpopup" runat="server" Style="color: rgb(255,255,255);
                        text-decoration: none" CssClass="btncommonclose" Text="Close" CausesValidation="false" />
                    <%-- <asp:ImageButton ID="btncancelpopup" runat="server" Height="20px" Width="20px" ImageUrl="assets/img/close.png" />--%>
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label7" Text="Booked For" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblbookedname" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label8" Text="Booked Date" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblbookeddate" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label3" Text="Transaction Status" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lbltransaction" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label9" Text="Transaction ID" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lbltransid" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label4" Text="Amount" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblamount" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
            <tr style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #b0b3b8">
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="Label10" Text="Mobile Number" runat="server"></asp:Label>
                    </h6>
                </td>
                <td nowrap="nowrap" colspan="2" style="padding-bottom: 10px; padding-top: 10px;">
                    <h6>
                        <asp:Label ID="lblmblno" runat="server"></asp:Label>
                    </h6>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
