﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class Views_NonZoomCustomer : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.NonZoomCustomerBLL objNonZoomCustomerBLL = new BusinessLayer.NonZoomCustomerBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here
            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Non Zoom Customer");
            }

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Non Zoom Customer";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;

            Bind_Country_Dropdown();
            Bind_PhoneCode_Dropdown("0");

            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
    }

    #endregion

    #region Button Events

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            DataTable dt = objNonZoomCustomerBLL.AlreadyExist(txtEmail.Text.Trim());
            if (dt.Rows.Count > 0)
            {
                txtEmail.Focus();
                lblalert.Text = txtEmail.Text + " is already exist";
                mpealert.Show();
                return;
            }
        }

        AddAdminUser();

        int retVal = objNonZoomCustomerBLL.Save(objNonZoomCustomerBLL);

        if (retVal > 0)
        {
            if (btnSave.Text == "Save")
            {
                lblalert.Text = "Customer Details has been saved successfully";
            }
            else
            {
                lblalert.Text = "Customer Details has been updated successfully";
            }


            mpealert.Show();
            btnAdd.Focus();
            btnViewOnClick(sender, e);
            Clear();
        }
        else
        {
            lblMessage.Value = "An error has occured while processing your request";
        }
    }

    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            Clear();
            ddlgender.Focus();
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            lblMan.Visible = true;
            btnSch.Visible = false;
            divReason.Visible = false;
            divUnSub.Visible = false;
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();

            if (gvUser.Rows.Count > 0)
            {
                tblForm.Visible = false;
                tblGrid.Visible = true;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnView.Visible = false;
                lblMan.Visible = false;
                btnSch.Visible = true;
            }
            else
            {
                tblForm.Visible = true;
                tblGrid.Visible = false;
                btnAdd.Visible = false;
                btnSave.Visible = true;
                btnView.Visible = true;
                lblMan.Visible = true;
                btnSch.Visible = false;
            }

            btnAdd.Focus();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }

    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            objNonZoomCustomerBLL.DeletUser(lblrowtodelete.Text.Trim());
            List();
            lblalert.Text = "User Details have been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvPricingDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvUser.SelectedRow.FindControl("lblrowid");

            DataTable dtPS = objNonZoomCustomerBLL.Edit(lblrowid.Text.Trim());

            if (dtPS != null && dtPS.Rows.Count > 0)
            {
                ddlgender.Focus();
                hdfPSRID.Value = lblrowid.Text;
                ddlgender.SelectedIndex = ddlgender.Items.IndexOf(ddlgender.Items.FindByValue(dtPS.Rows[0]["title"].ToString()));
                rblMustMoveMail.SelectedIndex = rblMustMoveMail.Items.IndexOf(rblMustMoveMail.Items.FindByValue(dtPS.Rows[0]["MustMoveFlag"].ToString()));
                if (rblMustMoveMail.SelectedIndex == -1)
                {
                    rblMustMoveMail.SelectedValue = "Y";
                }
                txtFname.Text = dtPS.Rows[0]["FirstName"].ToString();
                txtComName.Text = dtPS.Rows[0]["CompanyName"].ToString();
                txtLastName.Text = dtPS.Rows[0]["LastName"].ToString();
                txtEmail.Text = dtPS.Rows[0]["Email"].ToString();
                txtPhone.Text = dtPS.Rows[0]["Phone"].ToString();
                txtAddress2.Text = dtPS.Rows[0]["Address2"].ToString();
                txtAddress.Text = dtPS.Rows[0]["Address1"].ToString();
                //  txtCountry.Text = dtPS.Rows[0]["Country"].ToString();
                txtCity.Text = dtPS.Rows[0]["City"].ToString();
                txtState.Text = dtPS.Rows[0]["State"].ToString();
                txtZip.Text = dtPS.Rows[0]["ZipCode"].ToString();
                // txtPhoneCode.Text = dtPS.Rows[0]["PhoneCode"].ToString();
                ddlPhoneCode.SelectedIndex = ddlPhoneCode.Items.IndexOf(ddlPhoneCode.Items.FindByValue(dtPS.Rows[0]["PhoneCode"].ToString()));
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(dtPS.Rows[0]["CountryId"].ToString()));
                if (dtPS.Rows[0]["Active"].ToString() == "1")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }

                lblReason.Text = dtPS.Rows[0]["UnSubReason"].ToString();

                if (lblReason.Text.Length > 0)
                {
                    divReason.Visible = true;
                }
                else
                {
                    divReason.Visible = false;
                }

                string strUnSubBy = dtPS.Rows[0]["UnSubBy"].ToString();
                string strUnSubon = dtPS.Rows[0]["UnSubOn"].ToString();

                if (strUnSubBy.Length > 0 && strUnSubon.Length > 0)
                {
                    lblUnSubBy.Text = strUnSubBy + " ( on ) " + Convert.ToDateTime(strUnSubon).ToString("MMM dd, yyyy hh:mm tt");
                    divUnSub.Visible = true;
                }
                else
                {
                    divUnSub.Visible = false;
                }

                string strCreatedBy = dtPS.Rows[0]["created"].ToString();
                string trCreatedOn = dtPS.Rows[0]["Createdon"].ToString();
                if (strCreatedBy != "" && trCreatedOn != "")
                {
                    lblLMby.Text = strCreatedBy + " ( on  ) " + Convert.ToDateTime(trCreatedOn).ToString("MMM dd, yyyy hh:mm tt");
                    divLMBy.Visible = true;
                }
                else
                {
                    divLMBy.Visible = false;
                }


            }

            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            lblMan.Visible = true;
            btnSch.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }



    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            lblrowtodelete.Text = lblrowid.Text;
            mpeconfirm.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    #endregion

    #region  User Defined Function

    public void AddAdminUser()
    {
        string autoGenPwd = string.Empty;
        autoGenPwd = genclass.RandomString(genclass.RandomNumber(8, 15));

        if (btnSave.Text == "Save")
        {
            objNonZoomCustomerBLL.ManageType = "I";
        }
        else
        {
            objNonZoomCustomerBLL.ManageType = "U";
        }
        objNonZoomCustomerBLL.RowID = hdfPSRID.Value;
        objNonZoomCustomerBLL.Title = ddlgender.SelectedItem.Text.Trim();
        objNonZoomCustomerBLL.firstName = txtFname.Text.Trim();
        objNonZoomCustomerBLL.lastName = txtLastName.Text.Trim();
        objNonZoomCustomerBLL.Email = txtEmail.Text.Trim();

        string strPCode = GetPhoneCode(ddlCountry.SelectedValue.Trim());
        objNonZoomCustomerBLL.PhoneCode = strPCode.Trim();
        objNonZoomCustomerBLL.Phone = txtPhone.Text.Trim();
        objNonZoomCustomerBLL.Address1 = txtAddress.Text.Trim();
        objNonZoomCustomerBLL.Address2 = txtAddress2.Text.Trim();
        objNonZoomCustomerBLL.City = txtCity.Text.Trim();
        objNonZoomCustomerBLL.Country = ddlCountry.SelectedItem.Text.Trim();
        objNonZoomCustomerBLL.State = txtState.Text.Trim();
        objNonZoomCustomerBLL.ZipCode = txtZip.Text.Trim();
        objNonZoomCustomerBLL.CompanyName = txtComName.Text.Trim();
        objNonZoomCustomerBLL.MustMoveFlag = rblMustMoveMail.SelectedValue.Trim();
        objNonZoomCustomerBLL.CountryId = ddlCountry.SelectedValue.Trim();

        if (chkActive.Checked == true)
        {
            objNonZoomCustomerBLL.Active = "1";
        }
        else
        {
            objNonZoomCustomerBLL.Active = "0";
        }


        objNonZoomCustomerBLL.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"];
    }

    public void List()
    {
        DataTable dt = objNonZoomCustomerBLL.Select();
        if (dt.Rows.Count > 0)
        {
            gvUser.DataSource = dt;
            gvUser.DataBind();

            btnSch.Visible = true;
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();

            btnSch.Visible = false;
        }
    }
    public string GetPhoneCode(string strCountry)
    {
        string strPCode = string.Empty;
        DataTable dtPhoneCode = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select PhoneCode from JETEDGE_FUEL..Country where Row_ID='" + strCountry + "'").Tables[0];

        if (dtPhoneCode.Rows.Count > 0)
        {
            strPCode = dtPhoneCode.Rows[0]["PhoneCode"].ToString().Trim();
        }

        return strPCode;
    }

    public void Clear()
    {
        try
        {
            hdfPSRID.Value = "0";
            btnSave.Text = "Save";
            divLMBy.Visible = false;
            ddlgender.ClearSelection();
            txtFname.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtState.Text = string.Empty;
            ddlCountry.ClearSelection();
            txtZip.Text = string.Empty;
            ddlPhoneCode.ClearSelection();
            txtComName.Text = string.Empty;
            rblMustMoveMail.SelectedValue = "Y";

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void Bind_Country_Dropdown()
    {
        genclass.Bind_Country_Dropdown(ddlCountry);
    }
    private void Bind_PhoneCode_Dropdown(string Country)
    {
        genclass.Bind_PhoneCode_Dropdown(ddlPhoneCode, Country);
    }

    #endregion
}