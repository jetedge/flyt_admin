﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using BusinessLayer;
using System.Drawing;

public partial class Views_UserLogReport : System.Web.UI.Page
{
    public string strFrom = null;
    public string strTo = null;

    public int linenum = 0;
    public string MethodName = string.Empty;

    protected BookedReportBLL objMember = new BookedReportBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Reports", "Admin User Log");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Reports";
                lblSubHeader.InnerText = "Admin User Log";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                txtSchFromDate.Text = DateTime.Now.ToString("MMM d, yyyy");
                txtSchTODate.Text = DateTime.Now.ToString("MMM d, yyyy");

                bindemptyleg();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #region Admin User Log

    public void gvUser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //Default values
            string strPreviousDataKeyValue = "";
            Color RowColor = Color.LightCyan;
            foreach (GridViewRow row in gvUser.Rows)
            {
                if (!strPreviousDataKeyValue.Equals(gvUser.DataKeys[row.RowIndex].Value.ToString()))
                    RowColor = RowColor == Color.White ? Color.LightCyan : Color.White;
                row.BackColor = RowColor;
                strPreviousDataKeyValue = gvUser.DataKeys[row.RowIndex].Value.ToString();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void gvUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        btnSubmit_Click(sender, e);
        gvUser.PageIndex = e.NewPageIndex;
        gvUser.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlstatus.SelectedValue == "User")
            {
                bindemptyleg();

                gvUser.Visible = true;
                gvvisiter.Visible = false;
            }
            else
            {
                BindGuestlogin();

                gvUser.Visible = false;
                gvvisiter.Visible = true;
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    void bindemptyleg()
    {
        string strRepFromDate = null;
        string strRepToDate = null;
        string strUserName = null;

        strRepFromDate = Convert.ToDateTime(txtSchFromDate.Text).ToString("yyyy-MM-dd");
        strRepToDate = Convert.ToDateTime(txtSchTODate.Text).ToString("yyyy-MM-dd");
        strUserName = txtSearch.Text;

        DataTable dtAdminUserLog = objMember.AdminUser_Log_Report(strRepFromDate, strRepToDate, strUserName);
        if (dtAdminUserLog.Rows.Count > 0)
        {
            gvUser.DataSource = dtAdminUserLog;
            gvUser.DataBind();

            btnexport.Visible = true;
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();

            btnexport.Visible = false;
        }
    }

    #endregion

    public override void VerifyRenderingInServerForm(Control control)
    {

    }

    void funFileDownload(string Location)
    {
        try
        {
            FileInfo file = new FileInfo(Location);
            Session.Contents.Remove("FullFileName");
            if (file.Exists)
            {
                Response.Buffer = false;
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(Location));
                Response.TransmitFile(Location);
                Response.End();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string strName = string.Empty;
            strName = ddlstatus.SelectedItem.Text.ToString().Replace(" ", "_") + "_" + DateTime.Now.ToString("ddMMMyyyy");

            string[] arrlblPageHits = { "lblUserName", "lblIPAddress", "lblVisitedTime", "lblmodule", "lblScreenName" };

            objMember.Exportexcel(gvUser, strName, arrlblPageHits, DateTime.Now.ToString("MMM/dd/yyyy"), lblUser.Text, ddlstatus.SelectedItem.Text);
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }

    #region Visitor Log

    void BindGuestlogin()
    {
        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Logaudit_Guest where CONVERT(varchar,visitedtime,101) between CONVERT(varchar,'" + Convert.ToDateTime(txtSchFromDate.Text).ToString("MM/dd/yyyy") + "',101) AND CONVERT(varchar,'" + Convert.ToDateTime(txtSchTODate.Text).ToString("MM/dd/yyyy") + "',101)").Tables[0];

        gvvisiter.DataSource = dt;
        gvvisiter.DataBind();

        gvUser.Visible = false;
        gvvisiter.Visible = true;
    }

    protected void gvvisiter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label visitedtime = e.Row.FindControl("visitedtime") as Label;

            if (visitedtime.Text.Length > 0)
            {


                if (Convert.ToDateTime(visitedtime.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                {
                    visitedtime.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    visitedtime.Font.Bold = true;
                }
                else
                {
                    visitedtime.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    //gvinvoice.Rows[i].Cells[6].Font.Bold = true;
                }
            }
        }
    }

    public static string GetUserData(string strFrom, string strTo, string search, string UserType)
    {
        string sqlquery = string.Empty;
        string where = string.Empty;
        if (search.Length == 0)
            where = "loggedin >= '" + strFrom + "'  and loggedin <=  '" + strTo + "'";//' and Logaudit.CustomerID=" + CustomerID + "";
        else
            where += "loggedin >= '" + strFrom + "'  and loggedin <=  '" + strTo + "' and  UserMasterCreate.Email like '%" + search + "%'";// and Logaudit.CustomerID=" + CustomerID + "";


        sqlquery = @"select Logaudit.UserID 'Logged_User_Name', firstname +' '+ lastname as UserName_Create,
IPAddress ,loggedin 'Logged_In_Time',loggedout 'Logged_Out_Time', logoutReason 'Logged_Out_Reason', 
case when loggedout is not null then DATEDIFF(SS,loggedIn ,loggedout) else 0 end 'Logged_In_Sec' , Logaudit.rowid
from Logaudit 
INNER JOIN UserMasterCreate on Logaudit.UserID=UserMasterCreate.rowid  
where " + where + "  order by UserName_Create";
        return sqlquery;
    }

    #endregion
}