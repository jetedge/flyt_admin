﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="DiscountandCoupon.aspx.cs" Inherits="Views_DiscountandCoupon" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .table th, .table td
        {
            padding: 0.5rem;
            vertical-align: middle;
            border: 0;
            border-top: 1px solid #EBEDF3;
        }
        .table thead th, .table thead td
        {
            font-weight: 600;
            font-size: inherit !important;
            border-bottom-width: 1px;
            padding-top: 1rem;
            padding-bottom: 1rem;
        }
        #MainContent_chkuser
        {
            line-height: 22px;
            letter-spacing: 0.5px;
        }
        
        input[type="radio"], input[type="checkbox"]
        {
            margin-right: 6px;
        }
        #MainContent_gvCustomerMapping tbody tr td table tr td
        {
            padding: 0;
            border: 0;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function CheckDate(sender, args) {
            debugger;

            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;

            var today1 = sender._selectedDate;
            var date1 = today1.getFullYear() + '-' + (today1.getMonth() + 1) + '-' + today1.getDate();
            var time1 = today1.getHours() + ":" + today1.getMinutes() + ":" + today1.getSeconds();
            var dateTime1 = date1 + ' ' + time1;

            var dateOne = new Date(today.getFullYear(), (today.getMonth() + 1), today.getDate()); //Year, Month, Date    
            var dateTwo = new Date(today1.getFullYear(), (today1.getMonth() + 1), today1.getDate()); //Year, Month, Date    

            if (dateOne > dateTwo) {
                document.getElementById('lblalert').innerHTML = 'You cannot select past date!';
                $find("mpealert").show();
                //  alert("You cannot select past date!");
                sender._selectedDate = new Date();
                sender._textbox.set_Value('')
                return;
            }


            if (document.getElementById("txtstartdate").value != '' && document.getElementById("txtEDate").value != '') {
                var endDate = new Date(document.getElementById("txtEDate").value);
                var startDate = new Date(document.getElementById("txtstartdate").value);

                if (startDate > endDate) {
                    document.getElementById('lblalert').innerHTML = 'Please ensure that the End Date is greater than or equal to the Start Date.';
                    $find("mpealert").show();
                    //alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                    sender._textbox.set_Value('')
                    return false;
                }
            }
        }      
    </script>
    <script language="javascript" type="text/javascript">
        function txtusernameF_TextChangedf() {
            document.getElementById('MainContent_btnusernameF').click()
            return;
        }
        function txtEmailF_TextChangedf() {
            document.getElementById('MainContent_btnEmailF').click()
            return;
        }

        function checkAll(objRef) {
            var GridView = document.getElementById("MainContent_gvCustomerMapping");
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }


        function chkListSelectAll(cbControl) {
            var chkBoxList = document.getElementById(cbControl);
            if (chkBoxList != null) {
                if (cbControl.checked == true) {
                    var chkBoxCount = chkBoxList.getElementsByTagName("input");
                    for (var i = 0; i < chkBoxCount.length; i++) {
                        chkBoxCount[i].checked = true;
                    }
                } else {
                    var chkBoxCount = chkBoxList.getElementsByTagName("input");
                    for (var i = 0; i < chkBoxCount.length; i++) {
                        chkBoxCount[i].checked = false;
                    }
                }

            }
            return false;
        }

        function CheckBoxListSelect(cbControl, state) {

            var chkBoxList = document.getElementById(cbControl);
            if (chkBoxList != null) {
                var chkBoxCount = chkBoxList.getElementsByTagName("input");
                for (var i = 0; i < chkBoxCount.length; i++) {
                    chkBoxCount[i].checked = state;
                }
            }
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row" id="dvGrid" runat="server">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="row form-group" id="dvGridHeader" runat="server">
                        <div class="col-xl-4 text-right">
                        </div>
                        <div class="col-xl-4">
                            <div class="form-group row align-items-center" id="Div1" runat="server" visible="true">
                                <label class="col-4 col-form-label text-center">
                                    Status
                                </label>
                                <div class="col-8">
                                    <asp:DropDownList ID="ddlstatus" OnSelectedIndexChanged="ddlstatus_onselectedChanged"
                                        Width="80%" CssClass="cusinputuser" runat="server" AutoPostBack="true">
                                        <asp:ListItem Selected="true" Value="Active" Text="Active Coupons"></asp:ListItem>
                                        <asp:ListItem Value="Expire" Text="Expired Coupons"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 text-right" style="padding-right: 3%;">
                            <asp:Button ID="btnAdd" runat="server" Text="Add New" CssClass="btn btn-danger" OnClick="btnAddOnClick"
                                TabIndex="1" />
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 boxshadow table-responsive">
                            <asp:GridView ID="gvdiscount" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                OnSelectedIndexChanged="gvdiscount_SelectedIndexChanged" OnRowDataBound="gvTailDetails_OnRowDataBound"
                                AutoGenerateColumns="false" EmptyDataText="No Discount & Coupon found." PageSize="25"
                                AllowPaging="True" Width="100%">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrowid" Text='<%# Eval("rowid") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DiscountType" HeaderText="Discount Type" HeaderStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" ItemStyle-Wrap="false"
                                        HeaderStyle-Wrap="false" />
                                    <asp:BoundField DataField="CouponCode" HeaderText="Coupon Code" HeaderStyle-Width="13%"
                                        ItemStyle-Width="13%" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        ItemStyle-Wrap="false" HeaderStyle-Wrap="false" />
                                    <asp:BoundField DataField="CouponDescription" HeaderText="Registered User" Visible="false"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="left"
                                        HeaderStyle-HorizontalAlign="left" ItemStyle-Wrap="false" HeaderStyle-Wrap="false" />
                                    <asp:BoundField DataField="STDate" HeaderText="Start Date" HeaderStyle-Width="10%"
                                        ItemStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        ItemStyle-Wrap="false" HeaderStyle-Wrap="false" />
                                    <asp:BoundField DataField="ExpDate" HeaderText="End Date" HeaderStyle-Width="10%"
                                        ItemStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        ItemStyle-Wrap="false" HeaderStyle-Wrap="false" />
                                    <asp:BoundField DataField="CouponValue" HeaderText="Redeem($)" DataFormatString="{0:c2}"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="12%"
                                        ItemStyle-Width="12%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false" />
                                    <asp:BoundField DataField="CouponPercentage" HeaderText="Redeem(%)" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="12%" ItemStyle-Width="12%"
                                        ItemStyle-Wrap="false" HeaderStyle-Wrap="false" />
                                    <asp:TemplateField HeaderText="Updated By" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="14%" ItemStyle-Width="14%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedby" Text='<%# Eval("ModifiedByN") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated On" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                        HeaderStyle-Width="16%" ItemStyle-Width="16%" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcreatedon" Text='<%# Eval("ModifiedOnN") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbView1" runat="server" TabIndex="4" ImageUrl="~/Images/pencil_Edit.png"
                                                CommandName="Select" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="5%" ItemStyle-Width="5%" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" TabIndex="4" ImageUrl="~/Images/icon_delete.png"
                                                CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="dvForm" runat="server" visible="false">
        <div class="col-xl-12">
            <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                <div class="form-group row">
                    <div class="col-xl-12 text-right" style="padding-right: 3%;">
                        <label class="importantlabels" id="dvFormHeader" runat="server" visible="false">
                            <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                        </label>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" OnClick="btnSaveOnClick"
                            TabIndex="7" ValidationGroup="DC" />
                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="DC" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                        <asp:Button ID="btnView" runat="server" Text="Back" OnClick="btnViewOnClick" CssClass="btn btn-danger"
                            TabIndex="8" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-1">
                    </div>
                    <div class="col-xl-10">
                        <div class="form-group row kt-portlet kt-portlet__body m-0">
                            <div class="col-xl-12">
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">
                                        Discount Type
                                    </label>
                                    <div class="col-4 col-form-label">
                                        <asp:RadioButtonList ID="rblAutoFlag" runat="server" AutoPostBack="true" RepeatColumns="3"
                                            RepeatDirection="Horizontal" OnSelectedIndexChanged="rblAutoFlag_SelectedIndexChanged">
                                            <asp:ListItem Text="Personal" Value="P" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Auto Apply" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Jet Edge Retail" Value="J"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <label class="col-2 col-form-label">
                                        Start Date <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-4">
                                        <asp:TextBox class="cusinputuser" runat="server" Placeholder="MMM d, yyyy" onkeypress="return NumericsIphon(event)"
                                            ID="txtstartdate" TabIndex="3" MaxLength="20" OnTextChanged="txtEDate_TextChanged"
                                            Width="60%" AutoPostBack="true" />
                                        <asp:CalendarExtender ID="CalendarExtender1" OnClientDateSelectionChanged="CheckDate"
                                            CssClass="cal_Theme1" runat="server" TargetControlID="txtstartdate" Format="MMM d, yyyy"
                                            Enabled="True">
                                        </asp:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtstartdate"
                                            Display="None" ErrorMessage="Start Date is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-2 col-form-label">
                                        Coupon Code <span class="MandatoryField">* </span>
                                        <br />
                                        <label style="font-style: italic; color: darkgray; font-size: 12px; letter-spacing: 0.5px;">
                                            [Printed on Invoice File]
                                        </label>
                                    </label>
                                    <div class="col-4">
                                        <input class="cusinputuser" type="text" id="txtCouponCd" runat="server" maxlength="20"
                                            tabindex="1" />
                                        <asp:RequiredFieldValidator ID="rfvCCode" runat="server" ControlToValidate="txtCouponCd"
                                            Display="None" ErrorMessage="Coupon Code is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                    <label class="col-2 col-form-label">
                                        End Date <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-4">
                                        <asp:TextBox class="cusinputuser" runat="server" Placeholder="MMM d, yyyy" onkeypress="return NumericsIphon(event)"
                                            ID="txtEDate" TabIndex="3" MaxLength="20" OnTextChanged="txtEDate_TextChanged"
                                            Width="60%" AutoPostBack="true" />
                                        <asp:CalendarExtender ID="CalendarExtender2" OnClientDateSelectionChanged="CheckDate"
                                            CssClass="cal_Theme1" runat="server" TargetControlID="txtEDate" Format="MMM d, yyyy"
                                            Enabled="True">
                                        </asp:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="rfvEDate" runat="server" ControlToValidate="txtEDate"
                                            Display="None" ErrorMessage="Expiry Date is required." SetFocusOnError="True"
                                            ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">
                                        Coupon Desciption <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-4">
                                        <input class="cusinputuser" type="text" id="txtcouponDesc" runat="server" maxlength="200"
                                            tabindex="2" />
                                    </div>
                                    <label class="col-2 col-form-label">
                                        Redeem Option <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-4">
                                        <div class="row align-items-center" id="dvOtherRedeem" runat="server">
                                            <div class="col-3 pr-0">
                                                <asp:DropDownList ID="ddlDiscounttype" runat="server" class="cusinputuser" TabIndex="4">
                                                    <asp:ListItem Value="$" Text="$" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="%" Text="%"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-6">
                                                <input class="cusinputuser" id="txtRAmount" onkeypress="return Numericswithdot(event,this.id)"
                                                    onblur="isNumberKeyPer(event,this.id)" runat="server" maxlength="20" type="text"
                                                    style="width: 71%;" tabindex="6" />
                                                <asp:RequiredFieldValidator ID="rfvRA" runat="server" ControlToValidate="txtRAmount"
                                                    Display="None" ErrorMessage="Redeem ($) is required." SetFocusOnError="True"
                                                    ValidationGroup="DC">*</asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="row align-items-center" id="dvRetailRedeem" runat="server" visible="false">
                                            <div class="col-12 p-0 col-form-label">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 30%;" align="center">
                                                            ( Display Price - $
                                                        </td>
                                                        <td style="width: 18%;" align="center">
                                                            <input class="cusinputuser" id="txtRetailRedeemD" runat="server" maxlength="20" tabindex="6"
                                                                onkeypress="return Numericswithdot(event,this.id)" onblur="isNumberKeyPer(event,this.id)"
                                                                title="Enter numeric characters only and limited to 20 characters." type="text" />
                                                            <asp:RequiredFieldValidator ID="reqRetailRedeemD" runat="server" ControlToValidate="txtRetailRedeemD"
                                                                Display="None" ErrorMessage="Redeem D is required." SetFocusOnError="True" ValidationGroup="DC" />
                                                        </td>
                                                        <td style="width: 12%;" align="center">
                                                            ) / ( 1 +
                                                        </td>
                                                        <td style="width: 18%;" align="center">
                                                            <input class="cusinputuser" id="txtRetailRedeemP" runat="server" maxlength="20" tabindex="6"
                                                                onkeypress="return Numericswithdot(event,this.id)" onblur="isNumberKeyPer(event,this.id)"
                                                                title="Enter numeric characters only and limited to 20 characters." type="text" />
                                                            <asp:RequiredFieldValidator ID="reqRetailRedeemP" runat="server" ControlToValidate="txtRetailRedeemP"
                                                                Display="None" ErrorMessage="Redeem P is required." SetFocusOnError="True" ValidationGroup="DC" />
                                                        </td>
                                                        <td style="width: 5%;" align="center">
                                                            % )
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">
                                        Applicable For <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-6 col-form-label">
                                        <asp:CheckBoxList ID="chkApplicable" runat="server" RepeatDirection="Horizontal"
                                            RepeatColumns="5">
                                            <asp:ListItem Text="Customer" Value="C"></asp:ListItem>
                                            <asp:ListItem Text="Broker" Value="B"></asp:ListItem>
                                            <asp:ListItem Text="Jet Edge Team" Value="J"></asp:ListItem>
                                            <asp:ListItem Text="Guest User" Value="G"></asp:ListItem>
                                            <asp:ListItem Text="Jet Edge Retail" Value="R"></asp:ListItem>
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                                <div class="row" id="tbllast" runat="server" visible="false">
                                    <label class="col-12 col-form-label">
                                        <b>Last Modified by : </b>
                                        <asp:Label ID="lblupdatedby" class="cuslabeluser" CssClass="lastmodify" runat="server" />
                                        ( on )
                                        <asp:Label ID="lblupdatedon" class="cuslabeluser" CssClass="lastmodify" runat="server" />
                                    </label>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="form-group row kt-portlet kt-portlet__body m-0">
                            <div class="col-xl-12">
                                <div class="form-group row align-items-center">
                                    <div class="col-xl-3 col-form-label">
                                        <label style="color: #b72025; font-size: 1.2rem; font-weight: 500;">
                                            Registered User
                                        </label>
                                    </div>
                                    <div class="col-xl-3 text-right">
                                    </div>
                                    <div class="col-xl-3 text-right">
                                        <asp:TextBox class="cusinputuser" runat="server" placeholder="Enter User / Broker Name"
                                            OnTextChanged="txtusernameF_TextChanged" ID="txtusernameF" AutoPostBack="true" />
                                    </div>
                                    <div class="col-xl-3 text-right">
                                        <asp:TextBox class="cusinputuser" OnTextChanged="txtEmailF_TextChanged" Placeholder="Enter Email / Domain"
                                            runat="server" ID="txtEmailF" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xl-12">
                                        <asp:Panel ID="Panel1" runat="server" Style="background-color: whitesmoke; border-color: rgb(204, 204, 204);
                                            padding: 0rem; border-width: 1px; border-style: solid; height: 320px; width: 100%;
                                            overflow: auto;">
                                            <asp:GridView ID="gvCustomerMapping" runat="server" EmptyDataText="No Registered User found"
                                                AutoGenerateColumns="false" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                                OnRowDataBound="gvCustomerMapping_OnRowDataBound" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="2%" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' />
                                                            <asp:Label ID="lblRowID" Visible="false" Text='<%# Eval("RowID") %>' runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="User Name / Email" HeaderStyle-Width="28%" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRegisteredUser" Text='<%# Eval("RegisteredUser") %>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="User Role" HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblUserRole" Text='<%# Eval("UserRole") %>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Applicable Flag" HeaderStyle-Width="18%" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <div class="row p-0 m-0">
                                                                <div class="col-lg-12 p-0 m-0" style="align-self: center;">
                                                                    <asp:RadioButtonList ID="rblApplyFlag" RepeatColumns="3" RepeatDirection="Horizontal"
                                                                        OnSelectedIndexChanged="rblApplyFlag_SelectedIndexChanged" AutoPostBack="true"
                                                                        runat="server">
                                                                        <asp:ListItem Text="One Time" Value="OT" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="Period" Value="PE"></asp:ListItem>
                                                                        <asp:ListItem Text="All Bookings" Value="AB"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>
                                                            <div class="row p-0 m-0">
                                                                <div class="col-lg-6 m-0 pl-0">
                                                                    <asp:TextBox class="cusinputuser" runat="server" Placeholder="MMM d, yyyy" TabIndex="3"
                                                                        onkeypress="return false" ID="txtStartDate" Style="padding: 6px; font-size: 13px;"
                                                                        MaxLength="20" Visible="false" />
                                                                    <asp:CalendarExtender ID="ceStartDate" runat="server" TargetControlID="txtStartDate"
                                                                        Format="MMM d, yyyy" Enabled="True" CssClass="cal_Theme1">
                                                                    </asp:CalendarExtender>
                                                                </div>
                                                                <div class="col-lg-6 m-0 pl-0">
                                                                    <asp:TextBox class="cusinputuser" runat="server" Placeholder="MMM d, yyyy" TabIndex="3"
                                                                        onkeypress="return false" ID="txtEndDate" Style="padding: 6px; font-size: 13px;"
                                                                        MaxLength="20" Visible="false" />
                                                                    <asp:CalendarExtender ID="ceEndDate" runat="server" TargetControlID="txtEndDate"
                                                                        Format="MMM d, yyyy" Enabled="True" CssClass="cal_Theme1">
                                                                    </asp:CalendarExtender>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkuserAll" Width="100%" Text="Check All" runat="server" class="radiobuttonali"
                                                                TabIndex="6" onclick="checkAll(this);"></asp:CheckBox>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkuser" Width="100%" runat="server" class="radiobuttonali" TabIndex="6">
                                                            </asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr>
                <td style="color: rgb(255,255,255); letter-spacing: 0.5px; vertical-align: middle"
                    align="center">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    &nbsp;
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="lblalert" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdfCDRID" Value="0" runat="server" />
    <asp:HiddenField ID="lbluserflag" runat="server" />
    <asp:Button ID="btnusernameF" Style="display: none;" OnClick="txtusernameF_TextChanged"
        runat="server" Text="UserName" />
    <asp:Button ID="btnEmailF" Style="display: none;" OnClick="txtEmailF_TextChanged"
        runat="server" Text="UserName" />
    <asp:Label ID="lblRowId" runat="server" Visible="false" />
    <asp:HiddenField ID="lblMessage" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
</asp:Content>
