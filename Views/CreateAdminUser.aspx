﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="true" CodeFile="CreateAdminUser.aspx.cs" Inherits="General_Setup_CreateAdminUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12" style="padding-right: 4%; padding-left: 4%;">
                        <div class="kt-portlet">
                            <div class="form-group row">
                                <div class="col-xl-12 text-right">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" TabIndex="1" />
                                    <label id="lblMan" runat="server" class="importantlabels" visible="false">
                                        <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                            ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                                    </label>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                        ValidationGroup="SP" />
                                    &nbsp;
                                    <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                                        OnClick="btnViewOnClick" />
                                    <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                        runat="server" ShowSummary="false" />
                                </div>
                            </div>
                            <div class="row m-0" id="tblGrid" runat="server">
                                <div class="col-xl-12 p-0 table-responsive boxshadow">
                                    <asp:GridView ID="gvUser" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                        Width="100%" AutoGenerateColumns="false" OnSelectedIndexChanged="gvPricingDetails_SelectedIndexChanged"
                                        PageSize="50" AllowPaging="True">
                                        <Columns>
                                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="1%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                                    <asp:Label ID="lblrowid" Text='<%# Eval("RowID") %>' runat="server" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="First Name" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                                ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfirstName" Text='<%# Eval("firstName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Name" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="12%"
                                                ItemStyle-Width="12%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastName" Text='<%# Eval("lastname") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="22%"
                                                ItemStyle-Width="22%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" Text='<%# Eval("Email") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile Number" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="13%"
                                                ItemStyle-Width="13%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPhone" Text='<%# Eval("PhoneNumber") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%"
                                                ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreatedBy" Text='<%# Eval("CreatedName") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="19%"
                                                ItemStyle-Width="19%" HeaderStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCreatedOn" Text='<%# Eval("CreatedOn") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Active" HeaderStyle-Width="3%" ItemStyle-Width="3%"
                                                HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false"
                                                HeaderStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkactiveM" Enabled="false" TabIndex="7" runat="server" Checked='<%# Convert.ToBoolean(Eval("Activeuser")) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                                ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbView1" runat="server" TabIndex="2" ImageUrl="~/Images/pencil_Edit.png"
                                                        CommandName="Select" ToolTip="Click here to edit" OnClientClick="return showloader();" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%"
                                                ItemStyle-Width="5%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imbDelete" runat="server" TabIndex="3" ImageUrl="~/Images/icon_delete.png"
                                                        CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick"
                                                        ToolTip="Click here to delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row" id="tblForm" runat="server" visible="false">
                                <div class="col-xl-12">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Title <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:DropDownList ID="ddlgender" runat="server" TabIndex="4" class="form-control">
                                                <asp:ListItem>Mr</asp:ListItem>
                                                <asp:ListItem>Mrs</asp:ListItem>
                                                <asp:ListItem>Miss</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            Address 1 <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TabIndex="10"
                                                MaxLength="200" autocomplete="off" ToolTip="Enter all characters.Length is limited to 200"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtAddress"
                                                Display="None" ErrorMessage="Address 1 is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            First Name <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtFname" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                                                CssClass="form-control" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFname"
                                                Display="None" ErrorMessage="First Name is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            Address2
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control" TabIndex="11"
                                                MaxLength="100" autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Last Name <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" TabIndex="6"
                                                MaxLength="100" autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                                                Display="None" ErrorMessage="Last Name is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            City <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" TabIndex="12" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCity"
                                                Display="None" ErrorMessage="City is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Email <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtloginID" runat="server" CssClass="form-control" TabIndex="7"
                                                autocomplete="off" MaxLength="200" ToolTip="Enter all characters. Length is limited to 200"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revEmailValidation" runat="server" ControlToValidate="txtloginID"
                                                Display="None" ErrorMessage="Please enter valid email address." ValidationGroup="SP"
                                                SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtloginID"
                                                Display="None" ErrorMessage="Email is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            State <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtState" runat="server" CssClass="form-control" TabIndex="13" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100" MaxLength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtState"
                                                Display="None" ErrorMessage="State is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Mobile Number <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-2 p-l-0 " style="display: none;">
                                            <asp:DropDownList ID="ddlPhoneCode" runat="server" CssClass="form-control " TabIndex="8">
                                            </asp:DropDownList>
                                            <%--  <asp:TextBox ID="txtPhoneCode" runat="server" CssClass="form-control" TabIndex="8"
                                                MaxLength="3" autocomplete="off" ToolTip="Enter all characters. Length is limited to 3"></asp:TextBox>--%>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlPhoneCode"
                                                Display="None" ErrorMessage="Phone Code is required." SetFocusOnError="True"
                                                ValidationGroup="SP" InitialValue="0">*</asp:RequiredFieldValidator>--%>
                                        </div>
                                        <div class="col-4">
                                            <input class="cusinputuser" runat="server" id="txtPhone" onkeypress="return Numerics(event)"
                                                data-mask="999.999.9999" maxlength="15" type="text" />
                                            <%--  <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" TabIndex="8" autocomplete="off"
                                                MaxLength="15" Width="100%" ToolTip="Enter Valid Mobile Number. Length is limited to 15"></asp:TextBox>--%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPhone"
                                                Display="None" ErrorMessage="Mobile Number is required." SetFocusOnError="True"
                                                ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                        <label class="col-2 col-form-label">
                                            Country <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control " TabIndex="14">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlCountry"
                                                Display="None" ErrorMessage="Country is required." SetFocusOnError="True" ValidationGroup="SP"
                                                InitialValue="0">*</asp:RequiredFieldValidator>
                                            <%--  <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" TabIndex="14"
                                                MaxLength="100" autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>--%>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">
                                            Active
                                        </label>
                                        <div class="col-4" style="padding-top: 15px;">
                                            <asp:CheckBox ID="chkActive" Text="Check for Active" Checked="true" TabIndex="9"
                                                runat="server" />
                                        </div>
                                        <label class="col-2  col-form-label">
                                            Zip Code <span class="MandatoryField">* </span>
                                        </label>
                                        <div class="col-4">
                                            <asp:TextBox ID="txtZip" runat="server" CssClass="form-control" TabIndex="15" autocomplete="off"
                                                ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtZip"
                                                Display="None" ErrorMessage="Zip Code is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="form-group row">
                                                    <label class="col-12 col-form-label" style="font-size: 16px; text-align: left; font-weight: bold;
                                                        padding-top: 5px !important; padding-bottom: 5px; padding-top: 10px; width: 40%;
                                                        border-bottom: 1px solid #C0C0C0 !important;">
                                                        Email Configuration
                                                    </label>
                                                </div>
                                                <div class="col-8" style="padding-top: 10px; padding-left: 0px">
                                                    <asp:CheckBoxList ID="rblEmailConfig" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="Booking Confirmation" Value="BC"></asp:ListItem>
                                                        <asp:ListItem Text="Exception" Value="EXC"></asp:ListItem>
                                                        <asp:ListItem Text="Saved Routes" Value="SR"></asp:ListItem>
                                                        <asp:ListItem Text="Must Moves" Value="MM"></asp:ListItem>
                                                        <%--  <asp:ListItem Text="SignUp" Value="SU"></asp:ListItem>--%>
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                        <div style="float: left; padding-top: 10px; padding-left: 15px;">
                                            <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                                font-weight: 600;"></asp:Label>
                                            <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="lblMessage" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
