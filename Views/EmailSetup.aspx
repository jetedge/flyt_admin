﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="EmailSetup.aspx.cs" Inherits="Views_EmailSetup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-xl-1">
        </div>
        <div class="col-xl-10">
            <div class="kt-portlet">
                <div class="kt-portlet__body p-0" id="tblGrid" runat="server">
                    <div class="form-group row">
                        <div class="col-xl-12 text-right">
                            <asp:Button ID="btnAdd" runat="server" Text="Add External User" CssClass="btn btn-danger"
                                TabIndex="1" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                ValidationGroup="SP" />
                            <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                                runat="server" ShowSummary="false" />
                            <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="col-xl-12 p-0 table-responsive boxshadow">
                            <asp:GridView ID="gvListItems" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                AutoGenerateColumns="False" EmptyDataText="No Data Available" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="#" HeaderStyle-Width="1%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="First Name" HeaderStyle-Width="20%" ItemStyle-Width="20%"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblfirstname" runat="server" Text='<%# Bind("firstName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name" HeaderStyle-Width="15%" ItemStyle-Width="15%"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllasname" runat="server" Text='<%# Bind("Lastname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email ID" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Booking Confirmation" Visible="true" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="padding: 0px;">
                                                <asp:CheckBox ID="Cbtrip" runat="server" Text="" Style="margin-left: 0rem;" Checked='<%# Convert.ToBoolean(Eval("BookingConfirmation")) %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exception" Visible="true" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="padding: 0px;">
                                                <asp:CheckBox ID="cbexception" runat="server" Text="" Style="margin-left: 0rem;"
                                                    Checked='<%# Convert.ToBoolean(Eval("Exception")) %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Saved Routes" Visible="true" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="padding: 0px;">
                                                <asp:CheckBox ID="cbpreference" runat="server" Text="" Style="margin-left: 0rem;"
                                                    Checked='<%# Convert.ToBoolean(Eval("Preference")) %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Must Moves" Visible="true" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="padding: 0px;">
                                                <asp:CheckBox ID="cbmustmoves" runat="server" Text="" Style="margin-left: 0rem;"
                                                    Checked='<%# Convert.ToBoolean(Eval("Mustmoves")) %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SignUp" Visible="false" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"
                                        HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="padding: 0px;">
                                                <asp:CheckBox ID="cbsignup" runat="server" Text="" Style="margin-left: 0rem;" Checked='<%# Convert.ToBoolean(Eval("SignUp")) %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-1">
        </div>
        <asp:Button ID="btnShow" runat="server" Style="display: none" Text="Show Modal Popup" />
        <asp:ModalPopupExtender ID="mpetime" runat="server" TargetControlID="btnShow" PopupControlID="pnltime"
            CancelControlID="btncancelpopup" BackgroundCssClass="modalBackground" Y="100">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnltime" runat="server" Width="40%" ScrollBars="Auto" Style="display: none;
            background-color: rgb(17,24,32)">
            <table width="100%" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="middle" colspan="2" style="color: White; font-weight: bold; padding: 10px;"
                        align="left">
                        <h5 style="color: White; margin-left: 1%; font-size: 18px">
                            Add External User</h5>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="height: 8px">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 3%; padding-right: 3%">
                        <div class="cusfielduser-cuscontaineruser">
                            <label class="cuslabeluser" for="txtFname" style="text-align: left; color: rgb(255,255,255)">
                                First Name <span style="color: Red">*</span>
                            </label>
                            <input class="cusinputuserTextbox" type="text" id="txtfName" runat="server" maxlength="20"
                                tabindex="1" />
                            <asp:RequiredFieldValidator ID="rfvCCode" runat="server" ControlToValidate="txtfName"
                                Display="None" ErrorMessage="First Name is required." SetFocusOnError="True"
                                ValidationGroup="DC">*</asp:RequiredFieldValidator>
                        </div>
                    </td>
                    <td style="text-align: left; padding-left: 3%; padding-right: 3%">
                        <div class="cusfielduser-cuscontaineruser">
                            <label class="cuslabeluser" for="Phone" style="text-align: left; color: rgb(255,255,255)">
                                Last Name<span style="color: Red">*</span>
                            </label>
                            <input class="cusinputuserTextbox" runat="server" id="txtLastname" maxlength="20"
                                tabindex="2" type="text">
                            <asp:RequiredFieldValidator ID="rfvEDate" runat="server" ControlToValidate="txtLastname"
                                Display="None" ErrorMessage="Last Name is required." SetFocusOnError="True" ValidationGroup="DC">*</asp:RequiredFieldValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px;">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 3%; padding-right: 3%" colspan="2">
                        <div class="cusfielduser-cuscontaineruser">
                            <label class="cuslabeluser" for="ddlRegisteredUser" style="text-align: left; color: rgb(255,255,255)">
                                Email ID<span style="color: Red">*</span>
                            </label>
                            <input class="cusinputuserTextbox" type="text" id="txtEmail" runat="server" maxlength="200"
                                onchange="ValidateEmail(this)" tabindex="3" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                                Display="None" ErrorMessage="Email ID is required." SetFocusOnError="True" ValidationGroup="DC">*</asp:RequiredFieldValidator>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px;">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 0%; padding-right: 3%;">
                        <div class="cusformuser-cuscontaineruser" style="padding-left: 4%; padding-top: 2%">
                            <div class="cusfielduser-cuscontaineruser">
                                <label class="cuslabeluser" for="ddlRegisteredUser" style="text-align: left">
                                </label>
                                <br />
                                <asp:CheckBox ID="chkEmpty" TabIndex="4" runat="server" />
                                <label class="cuslabeluser" for="chkEmpty" style="color: rgb(255,255,255)">
                                    Booking Confirmation</label>
                            </div>
                        </div>
                    </td>
                    <td style="text-align: left; padding-left: 3%; padding-right: 3%">
                        <div class="cusfielduser-cuscontaineruser">
                            <label class="cuslabeluser" for="ddlRegisteredUser" style="text-align: left">
                            </label>
                            <br />
                            <asp:CheckBox ID="chkexception" TabIndex="4" runat="server" />
                            <label class="cuslabeluser" for="chkexception" style="color: rgb(255,255,255)">
                                Exception</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 0%; padding-right: 3%">
                        <div class="cusformuser-cuscontaineruser" style="padding-left: 4%; padding-top: 2%">
                            <div class="cusfielduser-cuscontaineruser">
                                <label class="cuslabeluser" for="ddlRegisteredUser" style="text-align: left">
                                </label>
                                <br />
                                <asp:CheckBox ID="chkpreference" TabIndex="4" runat="server" />
                                <label class="cuslabeluser" for="chkpreference" style="color: rgb(255,255,255)">
                                    Saved Routes</label>
                            </div>
                        </div>
                    </td>
                    <td style="text-align: left; padding-left: 3%; padding-right: 3%">
                        <div class="cusfielduser-cuscontaineruser">
                            <label class="cuslabeluser" for="ddlRegisteredUser" style="text-align: left">
                            </label>
                            <br />
                            <asp:CheckBox ID="chkmustmoves" TabIndex="4" runat="server" />
                            <label class="cuslabeluser" for="chkexception" style="color: rgb(255,255,255)">
                                Must Moves</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 0%; padding-right: 3%">
                        <div class="cusformuser-cuscontaineruser" style="padding-left: 4%; padding-top: 2%">
                            <div class="cusfielduser-cuscontaineruser">
                                <label class="cuslabeluser" for="ddlRegisteredUser" style="text-align: left">
                                </label>
                                <br />
                                <asp:CheckBox ID="chksignup" TabIndex="4" runat="server" />
                                <label class="cuslabeluser" for="chksignup" style="color: rgb(255,255,255)">
                                    SignUp</label>
                            </div>
                        </div>
                    </td>
                    <td style="text-align: left; padding-left: 0%; padding-right: 3%">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 2%; padding-right: 2%; padding-bottom: 20px;
                        padding-top: 20px; text-align: center" colspan="2">
                        <asp:Button ID="btnSaveExternal" runat="server" Text="Save" CssClass="btn btn-danger"
                            TabIndex="5" ValidationGroup="DC" />&nbsp;
                        <asp:LinkButton ID="btncancelpopup" runat="server" Style="color: rgb(255,255,255);
                            text-decoration: none" CssClass="btn btn-danger" Text="Cancel" CausesValidation="false" />
                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="DC" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
        <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
            BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
            padding: 10px">
            <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
                cellpadding="0" cellspacing="0">
                <tr style="border-right: 0px; border-left: 0px">
                    <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                        <asp:Label ID="lblalert" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 15px">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                            text-decoration: none" CausesValidation="false" Text="Ok" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hdfCDRID" runat="server" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
</asp:Content>
