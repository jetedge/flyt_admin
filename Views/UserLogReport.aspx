﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="UserLogReport.aspx.cs" Inherits="Views_UserLogReport" Debug="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:UpdatePanel ID="uplPageHits" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnexport" />
        </Triggers>
        <ContentTemplate>
            <div class="kt-portlet mt-2 mb-2">
                <div class="form-group row m-0" style="align-items: center;">
                    <label class="col-2 col-form-label text-center" style="max-width: 9%;">
                        From Date
                    </label>
                    <div class="col-2" style="max-width: 11%;">
                        <asp:TextBox ID="txtSchFromDate" runat="server" Placeholder="MMM d, yyyy" onkeypress="return NumericsIphon(event)"
                            TabIndex="1" Width="100%" onkeydown="return ReadOnly(event)" MaxLength="15"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtSchFromDate"
                            CssClass="cal_Theme1" Format="MMM d, yyyy" Enabled="True">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="valFrom" runat="server" ControlToValidate="txtSchFromDate"
                            Display="None" ErrorMessage="From Date is mandatory" ValidationGroup="a">*</asp:RequiredFieldValidator>
                    </div>
                    <label class="col-2 col-form-label text-center" style="max-width: 8%;">
                        To Date
                    </label>
                    <div class="col-2" style="max-width: 11%;">
                        <asp:TextBox ID="txtSchTODate" runat="server" Placeholder="MMM d, yyyy" onkeypress="return NumericsIphon(event)"
                            Width="100%" TabIndex="2" MaxLength="15" onkeydown="return ReadOnly(event)">
                        </asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtSchTODate"
                            CssClass="cal_Theme1" Format="MMM d, yyyy" Enabled="True">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSchTODate"
                            Display="None" ErrorMessage="From Date is mandatory" ValidationGroup="a">*</asp:RequiredFieldValidator>
                    </div>
                    <label class="col-1 col-form-label text-center" style="max-width: 5%;">
                        Type
                    </label>
                    <div class="col-2" style="max-width: 12%;">
                        <asp:DropDownList ID="ddlstatus" TabIndex="3" Width="100%" Css runat="server">
                            <asp:ListItem Selected="true" Value="User" Text="Admin User Log"></asp:ListItem>
                            <asp:ListItem Value="Visitor" Text="Visitor Log"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <label class="col-2 col-form-label text-center" style="max-width: 10%;">
                        User Name
                    </label>
                    <div class="col-3" style="max-width: 12%;">
                        <asp:TextBox ID="txtSearch" runat="server" TabIndex="4" Width="100%" Placeholder="User Name"
                            ToolTip="User Name" />
                    </div>
                    <div class="col-3 text-right p-0" style="max-width: 20%">
                        <asp:Button ID="btnsubmit" runat="server" ValidationGroup="a" TabIndex="5" class="btn btn-danger"
                            Style="float: left;" OnClick="btnSubmit_Click" Text="Submit" />
                        &nbsp;
                        <asp:Button ID="btnexport" runat="server" Text="Export To Excel" CssClass="btn btn-danger"
                            Visible="false" TabIndex="6" OnClick="btnExport_Click" />
                        <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="a" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                    </div>
                </div>
                <div class="form-group row m-0 pt-3">
                    <div class="col-xl-12 boxshadow table-responsive">
                        <asp:GridView ID="gvUser" runat="server" EmptyDataText="No data found" Width="100%"
                            CssClass="table" HeaderStyle-CssClass="thead-dark" OnPageIndexChanging="gvUser_PageIndexChanging"
                            PagerStyle-CssClass="pager" AutoGenerateColumns="false" PageSize="100" AllowPaging="True"
                            OnRowDataBound="gvUser_RowDataBound" DataKeyNames="LogauditId">
                            <Columns>
                                <asp:TemplateField HeaderText="User Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLogauditId" Visible="false" runat="server" Text='<%# Bind("LogauditId") %>' />
                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UserName") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IP Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIPAddress" runat="server" Text='<%# Bind("IPAddress") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Visited Time">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVisitedTime" runat="server" Text='<%# Bind("VisitedTime") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Module">
                                    <ItemTemplate>
                                        <asp:Label ID="lblmodule" runat="server" Text='<%# Bind("module") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Screen Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScreenName" runat="server" Text='<%# Bind("ScreenName") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:GridView ID="gvvisiter" runat="server" EmptyDataText="No data found" Width="100%"
                            CssClass="table" HeaderStyle-CssClass="thead-dark" OnPageIndexChanging="gvUser_PageIndexChanging"
                            OnRowDataBound="gvvisiter_RowDataBound" PagerStyle-CssClass="pager" AutoGenerateColumns="false"
                            PageSize="25" AllowPaging="True" Visible="False">
                            <Columns>
                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IP Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("IPAddress") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Visited Time">
                                    <ItemTemplate>
                                        <asp:Label ID="visitedtime" runat="server" Text='<%# Bind("visitedtime") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
</asp:Content>
