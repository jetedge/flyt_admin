﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="Unsubscribe.aspx.cs" Inherits="Views_Unsubscribe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .unsubscribeContent
        {
            border-radius: 10px;
            border: 1px solid #d6d4d4;
            background-color: #ffffff;
            padding: 45px 60px;
            width: 920px;
            box-sizing: border-box;
            text-align: center;
            margin: 25px auto;
        }
        .mailSubscribe
        {
            font-size: 18px;
            line-height: 31px;
            border-bottom: 1px solid #dbdedf;
            padding-bottom: 30px;
            margin-bottom: 30px;
        }
        .msgTitle
        {
            color: #ff3f12;
            font-size: 36px;
            display: inline-block;
            margin-bottom: 24px;
        }
        .selectReason
        {
            width: 430px;
            text-align: left;
            margin: 0 auto;
            margin-top: 35px;
        }
        .errorMsg
        {
            color: #e11414;
            font-size: 12px;
            line-height: 24px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    
</asp:Content>
