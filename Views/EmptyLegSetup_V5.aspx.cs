﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Views_EmptyLegSetup_V5 : System.Web.UI.Page
{
    protected BusinessLayer.EmptyLegSetupBLL objMember = new BusinessLayer.EmptyLegSetupBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Empty Leg Details");
            }


            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "Admin";
            lblSubHeader.InnerText = "Empty Leg Setup";

            BindEmptyLegs();

            hdfSCID.Value = "0";
            tblForm.Visible = false;
            tblGrid.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
            btnAdd.Focus();
        }
    }

    protected void btnSaveOnClick(object sender, EventArgs e)
    {
        string[] CategoryDetails = new string[70];
        string strMsg = string.Empty;
        string strActive = string.Empty;
        try
        {
            #region For Manual and Modiy Fos

            if (lblgroup.Text != "Group")
            {
                string strBusinessAlert = string.Empty;

                if (txtTailNo.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Tail number is required. <br/>";
                }
                if (txttrip.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Trip number is required. <br/>";
                }
                if (txtFlyHours1.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Flying Hours is required. <br/>";
                }
                if (txtAvaFrom.Text.ToString().Length == 0)
                {
                    strBusinessAlert += "- Available From is required. <br/>";
                }
                if (txtAvaTo.Text.ToString().Length == 0)
                {
                    strBusinessAlert += "- Available To is required. <br/>";
                }
                if (txtMPrice.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Price is required. <br/>";
                }

                if (txtStartTime.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- Start Time is required. <br/>";
                }

                if (txtEndTime.Value.ToString().Length == 0)
                {
                    strBusinessAlert += "- End Time is required. <br/>";
                }


                if (lblgroup.Text != "Modify")
                {
                    if (gvDepAirport.Rows.Count == 0)
                    {
                        strBusinessAlert += "- Departure Airport is required. <br/>";
                    }

                    if (gvArrivalAirport.Rows.Count == 0)
                    {
                        strBusinessAlert += "- Arrival Airport is required. <br/>";
                    }
                }

                if (gvDepAirport.Rows.Count > 0)
                {
                    if (txtDepDisLandPage.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Departure Landing Page input is required. <br/>";
                    }
                }
                if (gvArrivalAirport.Rows.Count > 0)
                {
                    if (txtArrDisLandPage.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Arrival Landing Page input is required. <br/>";
                    }
                }


                DataTable dtPrice = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select HourlyRate,pricingcompleted  from Tailsetup where tailno='" + txtTailNo.Value.Trim() + "'").Tables[0];

                if (dtPrice.Rows.Count > 0)
                {
                    if (dtPrice.Rows[0]["pricingcompleted"].ToString().Trim() == "0")
                    {
                        strBusinessAlert += "- Please complete the pricing setup for the tail [ " + txtTailNo.Value.Trim() + " ] <br/>";
                    }

                    if (txtMPrice.Value.Trim() != "")
                    {
                        string strHourlyRate = dtPrice.Rows[0]["HourlyRate"].ToString().Trim();

                        if (Convert.ToDecimal(txtMPrice.Value.Trim()) < Convert.ToDecimal(strHourlyRate.Trim()))
                        {
                            strBusinessAlert += "- Please enter price greater than $ " + strHourlyRate + " <br/>";
                        }
                    }
                }
                else
                {
                    strBusinessAlert += "- Tail not exist in pricing setup. <br/>";
                }


                if (strBusinessAlert.Length > 0)
                {
                    lblalert.Text = strBusinessAlert;
                    mpealert.Show();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                    return;
                }
                decimal daystoflight = 0;
                decimal L10_minPrice = 0;
                decimal M10_maxPrice = 0;
                decimal price = 0;

                decimal totalflyingHours = Convert.ToDecimal(txtFlyHours1.Value);

                var HourlyRate = Convert.ToDecimal("0.00"); ;
                var fixedCeiling = Convert.ToDecimal("0.00");
                var fixedFloor = Convert.ToDecimal("0.00");
                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                var MinFLHSellable = Convert.ToDecimal("0.00");
                var finalPricevalidation2 = (decimal)0;


                var dtFlyingDays = Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00";

                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtStartTime.Value + ":00").Subtract(DateTime.Now);

                var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                string stractive = string.Empty;

                if (chkEmpty.Checked)
                {
                    stractive = "1";
                }
                else
                {
                    stractive = "0";
                }

                if (hdfSCID.Value == "0")
                {
                    CategoryDetails[0] = "I";

                    DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
                    if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
                    {
                        gv_Child_Tail.DataSource = dttailtrip;
                        gv_Child_Tail.DataBind();
                        mpetrip.Show();
                        return;
                    }

                    DataTable dttail = new DataTable();

                    if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
                    {
                        lblalert.Text = "Tail No  already exists.";
                        mpeconfirm.Show();
                        return;
                        txtTailNo.Value = string.Empty;
                    }
                }
                else
                {
                    if (LBLTRIPNO.Text != txttrip.Value)
                    {
                        DataTable dttailtrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *  from Tailsetup_tripDetails where TripNo='" + txttrip.Value + "'").Tables[0];
                        if (dttailtrip.Rows.Count > 0 && lblgroup.Text != "Modify")
                        {
                            gv_Child_Tail.DataSource = dttailtrip;
                            gv_Child_Tail.DataBind();
                            mpetrip.Show();
                            return;
                        }
                        DataTable dttail = new DataTable();
                        if (dttail.Rows.Count > 0 && lblgroup.Text != "Modify")
                        {
                            lblalert.Text = "Tail No  already exists.";
                            mpeconfirm.Show();
                            return;
                        }
                    }

                    CategoryDetails[0] = "U";
                }
                string atctivetail = string.Empty;
                if (chkEmpty.Checked)
                {
                    atctivetail = "1";
                }
                else
                {
                    atctivetail = "0";
                }

                DataTable dtstatrt = new DataTable();
                DataTable dtend = new DataTable();

                DateTime fromDate = new DateTime();
                DateTime toDate = new DateTime();
                string dateTimeNow = string.Empty;


                DateTime DatefromTZ = new DateTime();
                DateTime DatetoTZ = new DateTime();
                DatefromTZ = Convert.ToDateTime(txtAvaFrom.Text + " " + txtStartTime.Value);
                if (dtstatrt.Rows.Count > 0)
                {
                    fromDate = Convert.ToDateTime(DatefromTZ).AddHours(-Convert.ToInt32(dtstatrt.Rows[0]["Timezone"].ToString()));
                }

                DatetoTZ = Convert.ToDateTime(txtAvaTo.Text + " " + txtEndTime.Value);
                if (dtend.Rows.Count > 0)
                {
                    toDate = Convert.ToDateTime(DatetoTZ).AddHours(-Convert.ToInt32(dtend.Rows[0]["Timezone"].ToString()));
                }


                string strstartdate = fromDate.ToString("yyyy-MM-dd");
                string strstarttime = fromDate.ToString("HH:mm");

                string strenddate = toDate.ToString("yyyy-MM-dd");
                string strendtime = toDate.ToString("HH:mm");
                string strstartcity = string.Empty;
                string strendcity = string.Empty;

                if (hdfSCID.Value == "")
                {
                    CategoryDetails[1] = "0";
                }
                else
                {
                    CategoryDetails[1] = hdfSCID.Value;
                }
                CategoryDetails[2] = txtTailNo.Value;
                CategoryDetails[3] = txtTailNo.Value;

                if (gvDepAirport.Rows.Count > 0)
                {
                    string strAirportCode = string.Empty;
                    string strAirportName = string.Empty;

                    string strStateCode = string.Empty;
                    string strStateName = string.Empty;

                    string strCountryCode = string.Empty;
                    string strCountryName = string.Empty;

                    for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                    {
                        Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                        Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                        if (lblSetupBy.Text.Trim() == "Airport")
                        {
                            strstartcity = lblAirport.Text.Replace(" - ", "-");
                            String[] strliststart = strstartcity.Split('-');

                            strAirportCode += strliststart[1] + "/";
                            strAirportName += strliststart[0] + "/";
                        }
                        if (lblSetupBy.Text.Trim() == "State")
                        {
                            string strState = lblAirport.Text;
                            if (strState.Contains(','))
                            {
                                string[] strSplit = strState.Split(',');

                                if (strSplit.Length > 1)
                                {
                                    strStateName += strSplit[0].ToString().Trim() + "/";

                                    string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                    if (strSTCode.Length > 1)
                                    {
                                        strStateCode += strSTCode[0].ToString().Trim() + "/";
                                    }
                                }
                            }
                        }
                        if (lblSetupBy.Text.Trim() == "Country")
                        {
                            string strCountry = lblAirport.Text;
                            if (strCountry.Contains(','))
                            {
                                string[] strSplit = strCountry.Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strCountryCode += strSplit[1].ToString().Trim() + "/";
                                    strCountryName += strSplit[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }


                    CategoryDetails[4] = (strCountryCode + strStateCode + strAirportCode).TrimEnd('/');
                    CategoryDetails[5] = (strCountryName + strStateName + strAirportName).TrimEnd('/');

                    CategoryDetails[52] = txtDepDisLandPage.Text.Trim();
                }
                else
                {
                    CategoryDetails[4] = lblStartData.Text;
                    CategoryDetails[5] = lblscitydata.Text;

                    CategoryDetails[52] = lblStartData.Text;
                }
                if (gvArrivalAirport.Rows.Count > 0)
                {
                    string strEndAirportCode = string.Empty;
                    string strEndAirportName = string.Empty;

                    string strEndStateCode = string.Empty;
                    string strEndStateName = string.Empty;

                    string strEndCountryCode = string.Empty;
                    string strEndCountryName = string.Empty;
                    for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                    {
                        Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                        Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                        if (lblSetupBy.Text.Trim() == "Airport")
                        {
                            strstartcity = lblAirport.Text.Replace(" - ", "-");
                            String[] strliststart = strstartcity.Split('-');

                            strEndAirportCode += strliststart[1] + "/";
                            strEndAirportName += strliststart[0] + "/";
                        }
                        if (lblSetupBy.Text.Trim() == "State")
                        {
                            string strState = lblAirport.Text;
                            if (strState.Contains(','))
                            {
                                string[] strSplit = strState.Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strEndStateName += strSplit[0].ToString().Trim() + "/";

                                    string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                    if (strSTCode.Length > 1)
                                    {
                                        strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                    }
                                }
                            }
                        }
                        if (lblSetupBy.Text.Trim() == "Country")
                        {
                            string strCountry = lblAirport.Text;
                            if (strCountry.Contains(','))
                            {
                                string[] strSplit = strCountry.Split(',');
                                if (strSplit.Length > 1)
                                {
                                    strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                    strEndCountryName += strSplit[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }

                    CategoryDetails[7] = (strEndCountryCode + strEndStateCode + strEndAirportCode).TrimEnd('/');
                    CategoryDetails[8] = (strEndCountryName + strEndStateName + strEndAirportName).TrimEnd('/');
                    CategoryDetails[53] = txtArrDisLandPage.Text.Trim();
                }
                else
                {
                    CategoryDetails[7] = lblendData.Text.Trim();
                    CategoryDetails[8] = lblEcityData.Text.Trim();
                    CategoryDetails[53] = lblendData.Text.Trim();
                }



                CategoryDetails[6] = txtStartTime.Value;
                CategoryDetails[9] = txtEndTime.Value;
                CategoryDetails[10] = "";
                CategoryDetails[11] = txtFlyHours1.Value;
                CategoryDetails[12] = Convert.ToDateTime(txtAvaFrom.Text).ToString("yyyy-MM-dd");
                CategoryDetails[13] = Convert.ToDateTime(txtAvaTo.Text).ToString("yyyy-MM-dd");

                CategoryDetails[15] = "DN";
                CategoryDetails[41] = strstartdate;
                CategoryDetails[42] = strenddate;
                CategoryDetails[43] = strstarttime;
                CategoryDetails[44] = strendtime;
                if (hdfSCID.Value == "0")
                {
                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' ").Tables[0];
                    if (dttailTable.Rows.Count > 0)
                    {
                    }
                    else
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                    }

                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *, isnull(noofseats,0) as noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value + "' and pricingcompleted='1'").Tables[0];
                    if (dttailTableEx.Rows.Count > 0)
                    {
                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                        CategoryDetails[35] = "1";
                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                    }
                    else
                    {
                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                        CategoryDetails[35] = "0";
                        CategoryDetails[33] = "1";
                        CategoryDetails[40] = "0";
                        CategoryDetails[14] = "0";
                    }
                }
                else
                {
                    if (lbloldtail.Text != txtTailNo.Value)
                    {
                        DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + lbloldtail.Text + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
                        if (dttail.Rows.Count > 0)
                        {
                            CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
                            CategoryDetails[35] = "1";
                            CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
                        }
                        else
                        {
                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                            CategoryDetails[35] = "0";
                            CategoryDetails[33] = "1";
                        }
                    }
                    else
                    {
                        DataTable dttail = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup_tripDetails  Where TailNo='" + txtTailNo.Value + "'  and TripNo ='" + LBLTRIPNO.Text + "' and pricingcompleted='1'").Tables[0];
                        if (dttail.Rows.Count > 0)
                        {
                            CategoryDetails[20] = Convert.ToDecimal(dttail.Rows[0]["MinBD"].ToString()).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(dttail.Rows[0]["MaxBD"].ToString()).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(dttail.Rows[0]["MinSellable"].ToString()).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(dttail.Rows[0]["HourlyRate"].ToString()).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(dttail.Rows[0]["FixedFloor"].ToString()).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(dttail.Rows[0]["FixedCeiling"].ToString()).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(dttail.Rows[0]["Amount"].ToString()).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(dttail.Rows[0]["Daystoflight"].ToString()).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(dttail.Rows[0]["MinPrice"].ToString()).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(dttail.Rows[0]["MaxPrice"].ToString()).ToString();
                            CategoryDetails[35] = "1";
                            CategoryDetails[33] = dttail.Rows[0]["applyTax"].ToString().ToString();
                        }
                        else
                        {
                            CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                            CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                            CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                            CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                            CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                            CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                            CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                            CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                            CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                            CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                            CategoryDetails[35] = "0";
                            CategoryDetails[33] = "1";
                        }
                    }
                }

                CategoryDetails[16] = "0.00";
                CategoryDetails[16] = "0.00";
                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                if (chkEmpty.Checked)
                {
                    CategoryDetails[19] = "1";
                }
                else
                {
                    CategoryDetails[19] = "0";
                }
                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                TimeSpan ts = TimeSpan.Parse(s);
                CategoryDetails[31] = ts.ToString();
                CategoryDetails[32] = DateTime.Now.Date.ToString();
                if (hdfSCID.Value == "0")
                {
                    CategoryDetails[34] = txttrip.Value;
                }
                else
                {
                    CategoryDetails[34] = txttrip.Value;
                }
                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                CategoryDetails[37] = DateTime.Now.Date.ToString();
                CategoryDetails[38] = LBLTRIPNO.Text;
                CategoryDetails[39] = lbloldtail.Text;

                CategoryDetails[48] = txtMPrice.Value.Trim();

                if (lblgroup.Text == "Modify")
                {
                    if (lblModifyFlag.Text == "NO")
                    {
                        CategoryDetails[49] = "MF";
                    }
                    else
                    {
                        CategoryDetails[49] = lblModifyFlag.Text;
                    }
                    CategoryDetails[0] = "CH";
                    CategoryDetails[54] = lbltripflagupdate.Text.Trim();
                    if (lbllegdisplay.Text.Trim() == "" || lbllegdisplay.Text.Trim() == "0")
                    {
                        CategoryDetails[51] = "1";
                    }
                    else
                    {
                        CategoryDetails[51] = lbllegdisplay.Text.Trim();
                    }

                    DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from Tailsetup_tripDetails").Tables[0];
                    int ModifyGroupId = 1;
                    if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
                    {
                        if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
                        {
                            ModifyGroupId = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
                        }
                        else
                        {
                            ModifyGroupId = 1;
                        }
                    }

                    CategoryDetails[50] = ModifyGroupId.ToString();
                }
                else
                {
                    CategoryDetails[49] = "ET";
                    CategoryDetails[51] = "1";
                    CategoryDetails[54] = lbltripflagupdate.Text.Trim();
                }

                string strReturnVal = objMember.saveCategory(CategoryDetails);

                if (strReturnVal != "0")
                {
                    lblalert.Text = "Empty leg details have been saved successfully";
                    mpealert.Show();
                    if (hdfSCID.Value == "0")
                    {
                        lblManEditGroupId.Text = strReturnVal;
                    }
                    else if (hdfSCID.Value != "0" && lblgroup.Text == "Modify")
                    {
                        lblManEditGroupId.Text = strReturnVal;
                    }

                    if (gvDepAirport.Rows.Count > 0)
                    {
                        string strModFlag = string.Empty;
                        if (lblgroup.Text == "Modify")
                        {
                            strModFlag = "MF";
                        }
                        else
                        {
                            strModFlag = "ET";
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");

                        for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                        {
                            Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                            Label lblAir = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");
                            Label lblCreatedBy = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedBy");
                            Label lblCreatedOn = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedOn");

                            Label lblStateCode = (Label)gvDepAirport.Rows[i].FindControl("lblStateCode");
                            Label lblCountryCode = (Label)gvDepAirport.Rows[i].FindControl("lblCountryCode");
                            Label lblAirportCode = (Label)gvDepAirport.Rows[i].FindControl("lblAirportCode");

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Dep', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text + "','" + lblCountryCode.Text + "','" + lblAirportCode.Text + "')");
                        }
                    }

                    if (gvArrivalAirport.Rows.Count > 0)
                    {
                        string strModFlag = string.Empty;
                        if (lblgroup.Text == "Modify")
                        {
                            strModFlag = "MF";
                        }
                        else
                        {
                            strModFlag = "ET";
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='" + strModFlag + "' and GroupId='" + lblManEditGroupId.Text.ToString().Trim() + "'");

                        for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                        {
                            Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                            Label lblAir = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");
                            Label lblCreatedBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedBy");
                            Label lblCreatedOn = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedOn");

                            Label lblStateCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateCode");
                            Label lblCountryCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblCountryCode");
                            Label lblAirportCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblAirportCode");

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblManEditGroupId.Text.ToString().Trim() + "','" + strModFlag + "','Arr', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text + "','" + lblCountryCode.Text + "','" + lblAirportCode.Text + "')");
                        }
                    }

                }
                if (hdfSCID.Value != "0")
                {
                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set Active='" + stractive + " ',Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "'  and tripno='" + LBLTRIPNO.Text + "'");
                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update bookeddetails set Tailno='" + txtTailNo.Value + "' Where TailNo='" + lbloldtail.Text + "' and tripno  ='" + LBLTRIPNO.Text + "'  ");
                    DataTable dtPScheck = new DataTable();
                    if (txtStartCity.Text.Contains("-") && txtEndCity.Text.Contains("-"))
                    {
                        string strstartcity1 = txtStartCity.Text.Replace(" - ", "-");
                        String[] strliststart = strstartcity1.Split('-');

                        string strendcity1 = txtEndCity.Text.Replace(" - ", "-");
                        String[] strlistend = strendcity1.Split('-');
                        dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "' and Flag='FINAL'").Tables[0];
                        if (dtPScheck.Rows.Count > 0)
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + strliststart[1] + "' and [End]='" + strlistend[1] + "'  and FLAG='FINAL'");
                        }
                    }

                    else
                    {
                        dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  LivePricingCaculation where  TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "' and Flag='FINAL'").Tables[0];

                        if (dtPScheck.Rows.Count > 0)
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update LivePricingCaculation set FLAG='OLD' Where TailNo='" + lbloldtail.Text + "' and tripno='" + LBLTRIPNO.Text + "' and Start='" + txtStartCity.Text.Trim() + "' and [End]='" + txtEndCity.Text.Trim() + "'  and FLAG='FINAL'");
                        }
                    }

                }
            }

            #endregion

            #region Split Insert

            else
            {
                string strBusinessAlert = string.Empty;

                if (rblBreak.SelectedValue.Trim() == "CO")
                {
                    if (txtcountry.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Start county is required. <br/>";
                    }
                    if (ddlLeg1EndCity.Text.ToString() == "")
                    {
                        strBusinessAlert += "- End city is required. <br/>";
                    }
                    if (txtCouStartTime.Value.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Start Time is required. <br/>";
                    }
                    if (txtSplitLeg1EndTime.Text.ToString().Length == 0)
                    {
                        strBusinessAlert += "- End Time is required. <br/>";
                    }
                    if (txtSpliFlyHour.Value.ToString().Length == 0)
                    {
                        strBusinessAlert += "- Flying Hour is required. <br/>";
                    }
                }
                else if (rblBreak.SelectedValue.Trim() == "CT")
                {
                    if (gvLegDetails.Rows.Count == 0)
                    {
                        strBusinessAlert = "Please add leg";
                    }
                }
                if (strBusinessAlert.Length > 0)
                {
                    lblalert.Text = strBusinessAlert;
                    mpealert.Show();
                    return;
                }


                #region Split New Insert

                if (btnSave.Text == "Save")
                {
                    int groupid = 1;
                    decimal daystoflight = 0; decimal L10_minPrice = 0; decimal M10_maxPrice = 0; decimal price = 0;

                    var HourlyRate = Convert.ToDecimal("0.00");
                    var fixedCeiling = Convert.ToDecimal("0.00");
                    var fixedFloor = Convert.ToDecimal("0.00");
                    var D4_MinTimeBD = Convert.ToDecimal("0.0");
                    var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                    var MinFLHSellable = Convert.ToDecimal("0.00");
                    var finalPricevalidation2 = (decimal)0;
                    var _totalDaysK101 = "0";

                    string stractive = string.Empty;
                    stractive = "1";

                    if (hdfSCID.Value == "0")
                    {
                        CategoryDetails[0] = "I";
                    }
                    else
                    {
                        CategoryDetails[0] = "U";
                    }
                    string atctivetail = string.Empty;

                    atctivetail = "1";

                    DataTable dtPScheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select max(groupid) as groupid from Tailsetup_tripDetails").Tables[0];

                    if (dtPScheck.Rows.Count > 0 && dtPScheck != null)
                    {
                        if (Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) > 0)
                        {
                            groupid = Convert.ToInt32(dtPScheck.Rows[0]["groupid"].ToString()) + 1;
                        }
                        else
                        {
                            groupid = 1;
                        }
                    }

                    if (rblBreak.SelectedValue.Trim() == "CO")
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CO',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");

                        if (lblbaseflag.Text.Trim() != "B0")
                        {
                            var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + txtCouStartTime.Value.Trim() + ":00");
                            var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                            var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                            var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value + ":00").Subtract(DateTime.Now);
                            _totalDaysK101 = K10_totalDays1.TotalDays.ToString("0.00");

                            string strstartcity = string.Empty;
                            strstartcity = ddlLeg1EndCity.Text.Replace(" - ", "-");
                            String[] strlistend = strstartcity.Split('-');
                            string startAirport = string.Empty;
                            string startcity = string.Empty;
                            if (strlistend.Length > 1)
                            {
                                startAirport = strlistend[1];
                                startcity = strlistend[0];
                            }

                            CategoryDetails[1] = hdfSCID.Value;
                            CategoryDetails[2] = txtTailNo.Value.Trim();
                            CategoryDetails[3] = txtTailNo.Value.Trim();
                            CategoryDetails[4] = txtcountry.Text;
                            CategoryDetails[5] = "";

                            CategoryDetails[6] = txtCouStartTime.Value.Trim();

                            CategoryDetails[7] = startAirport;
                            CategoryDetails[8] = startcity;
                            CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                            CategoryDetails[10] = "";
                            CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                            CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
                            CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
                            CategoryDetails[15] = "";
                            CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                            CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                            CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                            CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

                            if (hdfSCID.Value == "0")
                            {
                                DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                if (dttailTable.Rows.Count > 0)
                                {

                                }
                                else
                                {
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                }

                                DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                if (dttailTableEx.Rows.Count > 0)
                                {
                                    CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                    CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                    CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                    CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                    CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                    CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                    CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
                                    CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                    CategoryDetails[35] = "1";
                                    CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                    CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                    CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                }
                                else
                                {
                                    CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                    CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                    CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                    CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                    CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                    CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                    CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                    CategoryDetails[27] = Convert.ToDecimal(_totalDaysK101).ToString();
                                    CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                    CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                    CategoryDetails[35] = "0";
                                    CategoryDetails[33] = "1";
                                    CategoryDetails[40] = "0";
                                    CategoryDetails[14] = "0";
                                }
                            }
                            decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
                            CategoryDetails[16] = "0.00";
                            CategoryDetails[16] = "0.00";
                            CategoryDetails[30] = totalflyingHours.ToString("0.00");
                            if (cbSplitEmpty.Checked)
                            {
                                CategoryDetails[19] = "1";
                            }
                            else
                            {
                                CategoryDetails[19] = "0";
                            }
                            string s1 = DateTime.Now.ToString("HH:mm:ss.fff");

                            TimeSpan ts1 = TimeSpan.Parse(s1);
                            CategoryDetails[31] = ts1.ToString();
                            CategoryDetails[32] = DateTime.Now.Date.ToString();
                            if (hdfSCID.Value == "0")
                            {
                                CategoryDetails[34] = txttrip.Value.Trim();
                            }
                            else
                            {
                                CategoryDetails[34] = txttrip.Value.Trim();
                            }
                            CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                            CategoryDetails[37] = DateTime.Now.Date.ToString();
                            CategoryDetails[38] = txttrip.Value.Trim();
                            CategoryDetails[39] = txtTailNo.Value.Trim();
                            CategoryDetails[45] = "B1";
                            CategoryDetails[46] = groupid.ToString();
                            CategoryDetails[47] = lbllegdisplay.Text;
                            CategoryDetails[48] = rblBreak.SelectedValue.Trim();


                            string strReturnVal1 = objMember.saveCategoryGroup(CategoryDetails);
                        }
                        else
                        {
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");
                        }


                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

                        string strCountryCode = string.Empty;

                        if (txtcountry.Text.Contains("/"))
                        {
                            string[] strSplitCountry = txtcountry.Text.Trim().Split('/');

                            if (strSplitCountry.Length > 1)
                            {
                                for (int i = 0; i < strSplitCountry.Length; i++)
                                {
                                    DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + strSplitCountry[i].ToString().Trim() + "' ").Tables[0];

                                    if (detr.Rows.Count > 0)
                                    {
                                        strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                                    }
                                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Country', '" + strSplitCountry[i].ToString().Replace("'", "''") + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                                }
                            }
                        }
                        else
                        {
                            DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + txtcountry.Text.Trim() + "' ").Tables[0];

                            if (detr.Rows.Count > 0)
                            {
                                strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                            }
                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Country', '" + txtcountry.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

                        string strstartcity1 = string.Empty;
                        strstartcity1 = ddlLeg1EndCity.Text.Replace(" - ", "-");
                        String[] strlistend1 = strstartcity1.Split('-');
                        string startAirport1 = string.Empty;
                        string startcity1 = string.Empty;
                        if (strlistend1.Length > 1)
                        {
                            startAirport1 = strlistend1[1];
                            startcity1 = strlistend1[0];
                        }

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Arr', 'Airport', '" + ddlLeg1EndCity.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + startAirport1.Trim() + "')");


                    }
                    if (gvLegDetails.Rows.Count > 0 && rblBreak.SelectedValue.Trim() == "CT")
                    {
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + groupid + ",ModifyFlag='SP',SplitType='CT',CreatedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "',createdOn='" + DateTime.Now.ToString() + "' Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");

                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE ModifiedFlag='SP' and GroupId='" + groupid.ToString().Trim() + "'");

                        for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                        {
                            if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
                            {
                                string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString();
                                var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text + ' ' + strStartTime.Trim() + ":00");
                                var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                                var K10_totalDays1 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                K10_totalDays1.TotalDays.ToString("0.00");

                                #region Leg 2 Insert Breakdown by City

                                var K10_totalDays2 = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                var _totalDaysK102 = K10_totalDays2.TotalDays.ToString("0.00");

                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
                                CategoryDetails[12] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaFrom.Text)));
                                CategoryDetails[13] = (string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(txtSplitAvaTo.Text)));
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");

                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {

                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK102).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }

                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString()).ToString("0.00");
                                if (chkEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s2 = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts2 = TimeSpan.Parse(s2);
                                CategoryDetails[31] = ts2.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString();
                                CategoryDetails[46] = groupid.ToString();
                                CategoryDetails[47] = lbllegdisplay.Text;
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                string strReturnVal2 = objMember.saveCategoryGroup(CategoryDetails);

                                #endregion
                            }
                            if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
                            {
                                string strStartAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                string strStartCity = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;

                                string strEndAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                string strEndCity = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;

                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Dep', 'Airport', '" + strStartCity.Trim() + " - " + strStartAirport.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strStartAirport.Trim() + "')");

                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + groupid.ToString().Trim() + "','SP','Arr', 'Airport', '" + strEndCity.Trim() + " - " + strEndAirport.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strEndAirport.Trim() + "')");
                            }
                        }
                    }
                }

                #endregion

                #region  Update Split Trip

                else
                {
                    // DataTable dtOutput = (DataTable)ViewState["Tripgrid"];

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Update Tailsetup_tripDetails set Activeflag='INACTIVE' Where Groupid='" + lblgroupiddisplay.Text + "' and Tripflag<>'B0' ");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "update Tailsetup_tripDetails set groupid=" + lblgroupiddisplay.Text + " Where TailNo='" + lblTailData.Text + "' and tripno='" + lblTripData.Text + "' and AvailbleFrom='" + Convert.ToDateTime(lblDateData.Text) + "' and leg=" + lbllegdisplay.Text + "  ");

                    if (lblbaseflag.Text != "B0")
                    {
                        if (rblBreak.SelectedValue.Trim() == "CO")
                        {
                            #region Breakdown by Country

                            DataTable dtexist = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='B1' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

                            if (dtexist.Rows.Count > 0)
                            {
                                decimal daystoflight = 0;
                                decimal L10_minPrice = 0;
                                decimal M10_maxPrice = 0;
                                decimal price = 0;

                                decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());
                                var HourlyRate = Convert.ToDecimal("0.00"); ;
                                var fixedCeiling = Convert.ToDecimal("0.00");
                                var fixedFloor = Convert.ToDecimal("0.00");
                                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                var MinFLHSellable = Convert.ToDecimal("0.00");
                                var finalPricevalidation2 = (decimal)0;
                                var _totalDaysK10 = "0";

                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }
                                else
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }


                                string stractive = string.Empty;
                                stractive = "1";
                                CategoryDetails[0] = "U";
                                string atctivetail = string.Empty;
                                atctivetail = "1";

                                string strEndCity = ddlLeg1EndCity.Text.Replace(" - ", "-");
                                String[] strlistend = strEndCity.Split('-');
                                string endAirport = string.Empty;
                                string endCity = string.Empty;
                                if (strlistend.Length > 1)
                                {
                                    endAirport = strlistend[1];
                                    endCity = strlistend[0];
                                }
                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                if (rblBreak.SelectedValue.Trim() == "CT")
                                {
                                    string strstartcity1 = string.Empty;
                                    strstartcity1 = ddlStartCitySplit.Text.Replace(" - ", "-");
                                    String[] strliststart1 = strstartcity1.Split('-');
                                    string startAirport1 = string.Empty;
                                    string startcity1 = string.Empty;
                                    if (strliststart1.Length > 1)
                                    {
                                        startAirport1 = strliststart1[1];
                                        startcity1 = strliststart1[0];

                                        CategoryDetails[4] = startAirport1;
                                        CategoryDetails[5] = startcity1;
                                    }
                                    else
                                    {

                                        CategoryDetails[4] = ddlStartCitySplit.Text;
                                        CategoryDetails[5] = "";
                                    }


                                }
                                else
                                {
                                    CategoryDetails[4] = txtcountry.Text;
                                    CategoryDetails[5] = "";
                                }
                                if (rblBreak.SelectedValue == "CO")
                                {
                                    CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
                                }

                                // CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                CategoryDetails[7] = endAirport;
                                CategoryDetails[8] = endCity;
                                CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                                CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
                                CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
                                //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                if (cbSplitEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts = TimeSpan.Parse(s);
                                CategoryDetails[31] = ts.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = "B1";
                                CategoryDetails[46] = lblgroupiddisplay.Text;
                                CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();


                                string strReturnVal = objMember.saveCategoryGroup(CategoryDetails);



                            }
                            else
                            {
                                decimal daystoflight = 0;
                                decimal L10_minPrice = 0;
                                decimal M10_maxPrice = 0;
                                decimal price = 0;
                                decimal totalflyingHours = Convert.ToDecimal(txtSpliFlyHour.Value.Trim());

                                var HourlyRate = Convert.ToDecimal("0.00"); ;
                                var fixedCeiling = Convert.ToDecimal("0.00");
                                var fixedFloor = Convert.ToDecimal("0.00");
                                var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                var MinFLHSellable = Convert.ToDecimal("0.00");
                                var finalPricevalidation2 = (decimal)0;
                                var _totalDaysK10 = "0";
                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtCouStartTime.Value.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }
                                else
                                {
                                    var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00";
                                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + txtSplitLeg1StartTime.Text.Trim() + ":00").Subtract(DateTime.Now);
                                    _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                                }


                                string stractive = string.Empty;

                                stractive = "1";

                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[0] = "I";
                                }
                                else
                                {
                                    CategoryDetails[0] = "U";
                                }
                                string atctivetail = string.Empty;

                                atctivetail = "1";
                                string strEndCity = ddlLeg1EndCity.Text.Replace(" - ", "-");
                                String[] strlistend = strEndCity.Split('-');
                                string endAirport = string.Empty;
                                string endCity = string.Empty;
                                if (strlistend.Length > 1)
                                {
                                    endAirport = strlistend[1];
                                    endCity = strlistend[0];
                                }

                                CategoryDetails[1] = hdfSCID.Value;
                                CategoryDetails[2] = txtTailNo.Value.Trim();
                                CategoryDetails[3] = txtTailNo.Value.Trim();
                                CategoryDetails[4] = txtcountry.Text.Trim();
                                CategoryDetails[5] = "";
                                if (rblBreak.SelectedValue.Trim() == "CO")
                                {
                                    CategoryDetails[6] = txtCouStartTime.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[6] = txtSplitLeg1StartTime.Text.Trim();
                                }
                                CategoryDetails[7] = endAirport.Trim();
                                CategoryDetails[8] = endCity.Trim();
                                CategoryDetails[9] = txtSplitLeg1EndTime.Text.Trim();
                                CategoryDetails[10] = "";
                                CategoryDetails[11] = txtSpliFlyHour.Value.Trim();
                                CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
                                CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
                                //CategoryDetails[14] = dtOutput.Rows[i]["Noofseats"].ToString();
                                CategoryDetails[15] = "";
                                CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                if (hdfSCID.Value == "0")
                                {
                                    DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                    if (dttailTable.Rows.Count > 0)
                                    {
                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                    }

                                    DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                    if (dttailTableEx.Rows.Count > 0)
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                        CategoryDetails[35] = "1";
                                        CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                        CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                        CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                    }
                                    else
                                    {
                                        CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                        CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                        CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                        CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                        CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                        CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                        CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                        CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                        CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                        CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                        CategoryDetails[35] = "0";
                                        CategoryDetails[33] = "1";
                                        CategoryDetails[40] = "0";
                                        CategoryDetails[14] = "0";
                                    }
                                }

                                CategoryDetails[16] = "0.00";
                                CategoryDetails[16] = "0.00";
                                CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                if (cbSplitEmpty.Checked)
                                {
                                    CategoryDetails[19] = "1";
                                }
                                else
                                {
                                    CategoryDetails[19] = "0";
                                }
                                string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                TimeSpan ts = TimeSpan.Parse(s);
                                CategoryDetails[31] = ts.ToString();
                                CategoryDetails[32] = DateTime.Now.Date.ToString();
                                if (hdfSCID.Value == "0")
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                else
                                {
                                    CategoryDetails[34] = txttrip.Value.Trim();
                                }
                                CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                CategoryDetails[37] = DateTime.Now.Date.ToString();
                                CategoryDetails[38] = txttrip.Value.Trim();
                                CategoryDetails[39] = txtTailNo.Value.Trim();
                                CategoryDetails[45] = "B1";
                                CategoryDetails[46] = lblgroupiddisplay.Text;
                                CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                string strReturnVal = objMember.saveCategoryGroup(CategoryDetails);
                            }

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Dep' and ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.ToString().Trim() + "'");

                            string strCountryCode = string.Empty;

                            if (txtcountry.Text.Contains("/"))
                            {
                                string[] strSplitCountry = txtcountry.Text.Trim().Split('/');

                                if (strSplitCountry.Length > 1)
                                {

                                    for (int i = 0; i < strSplitCountry.Length; i++)
                                    {
                                        DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + strSplitCountry[i].ToString().Trim() + "' ").Tables[0];

                                        if (detr.Rows.Count > 0)
                                        {
                                            strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                                        }
                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Dep', 'Country', '" + strSplitCountry[i].ToString().Replace("'", "''") + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                                    }
                                }
                            }
                            else
                            {
                                DataTable detr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select Country_Code from JETEDGE_FUEL..Country WHere Country_Name='" + txtcountry.Text.Trim() + "' ").Tables[0];

                                if (detr.Rows.Count > 0)
                                {
                                    strCountryCode = detr.Rows[0]["Country_Code"].ToString().Trim();
                                }
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Dep', 'Country', '" + txtcountry.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','" + strCountryCode.Trim() + "','')");
                            }

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE AirportFor='Arr' and ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.ToString().Trim() + "'");

                            string strstartcity2 = string.Empty;
                            strstartcity2 = ddlLeg1EndCity.Text.Replace(" - ", "-");
                            String[] strlistend2 = strstartcity2.Split('-');
                            string startAirport2 = string.Empty;
                            string startcity2 = string.Empty;
                            if (strlistend2.Length > 1)
                            {
                                startAirport2 = strlistend2[1];
                                startcity2 = strlistend2[0];
                            }

                            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.ToString().Trim() + "','SP','Arr', 'Airport', '" + ddlLeg1EndCity.Text.Trim() + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + startAirport2.Trim() + "')");

                            #endregion
                        }

                        #region Breakdown by City

                        else if (rblBreak.SelectedValue.Trim() == "CT")
                        {
                            if (gvLegDetails.Rows.Count > 0)
                            {
                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete From EmptyLegAirport_Child WHERE  ModifiedFlag='SP' and GroupId='" + lblgroupiddisplay.Text.Trim() + "'");

                                for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                                {
                                    string strTriplag = gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim();

                                    DataTable dtexist1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup_tripDetails Where TailNo ='" + txtTailNo.Value.Trim() + "' and tripflag='" + strTriplag + "' and groupid='" + lblgroupiddisplay.Text + "' ").Tables[0];

                                    #region Leg2

                                    if (dtexist1.Rows.Count > 0)
                                    {
                                        decimal daystoflight = 0;
                                        decimal L10_minPrice = 0;
                                        decimal M10_maxPrice = 0;
                                        decimal price = 0;

                                        decimal totalflyingHours = Convert.ToDecimal(gvLegDetails.Rows[i].FindControl("lblFlyHours").ToString().Trim());
                                        var HourlyRate = Convert.ToDecimal("0.00"); ;
                                        var fixedCeiling = Convert.ToDecimal("0.00");
                                        var fixedFloor = Convert.ToDecimal("0.00");
                                        var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                        var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                        var MinFLHSellable = Convert.ToDecimal("0.00");
                                        var finalPricevalidation2 = (decimal)0;
                                        string strStartTime = gvLegDetails.Rows[i].FindControl("lblstarttime").ToString().Trim();
                                        var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
                                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");

                                        string stractive = string.Empty;
                                        stractive = "1";
                                        CategoryDetails[0] = "U";
                                        string atctivetail = string.Empty;
                                        atctivetail = "1";

                                        CategoryDetails[1] = hdfSCID.Value;
                                        CategoryDetails[2] = txtTailNo.Value.Trim();
                                        CategoryDetails[3] = txtTailNo.Value.Trim();
                                        CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                        CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                        CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                        CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                        CategoryDetails[10] = "";
                                        CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text;
                                        CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text).ToString("yyyy-MM-dd");
                                        CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text).ToString("yyyy-MM-dd");
                                        CategoryDetails[15] = "";
                                        CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                        CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                        if (hdfSCID.Value == "0")
                                        {
                                            DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                            if (dttailTable.Rows.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                            }

                                            DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT   FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                            if (dttailTableEx.Rows.Count > 0)
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[35] = "1";
                                                CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                                CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                                CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                            }
                                            else
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                                CategoryDetails[35] = "0";
                                                CategoryDetails[33] = "1";
                                                CategoryDetails[40] = "0";
                                                CategoryDetails[14] = "0";
                                            }
                                        }
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                        if (cbSplitEmpty.Checked)
                                        {
                                            CategoryDetails[19] = "1";
                                        }
                                        else
                                        {
                                            CategoryDetails[19] = "0";
                                        }
                                        string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                        TimeSpan ts = TimeSpan.Parse(s);
                                        CategoryDetails[31] = ts.ToString();
                                        CategoryDetails[32] = DateTime.Now.Date.ToString();
                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                        CategoryDetails[37] = DateTime.Now.Date.ToString();
                                        CategoryDetails[38] = txttrip.Value.Trim();
                                        CategoryDetails[39] = txtTailNo.Value.Trim();
                                        CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
                                        CategoryDetails[46] = lblgroupiddisplay.Text;
                                        CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                        CategoryDetails[48] = rblBreak.SelectedValue.Trim();

                                        string strReturnVal = objMember.saveCategoryGroup(CategoryDetails);
                                    }
                                    else
                                    {
                                        decimal daystoflight = 0;
                                        decimal L10_minPrice = 0;
                                        decimal M10_maxPrice = 0;
                                        decimal price = 0;
                                        decimal totalflyingHours = Convert.ToDecimal(((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim());

                                        var HourlyRate = Convert.ToDecimal("0.00"); ;
                                        var fixedCeiling = Convert.ToDecimal("0.00");
                                        var fixedFloor = Convert.ToDecimal("0.00");
                                        var D4_MinTimeBD = Convert.ToDecimal("0.0");
                                        var D5_MaxTimeBD = Convert.ToDecimal("0.00");
                                        var MinFLHSellable = Convert.ToDecimal("0.00");
                                        var finalPricevalidation2 = (decimal)0;
                                        string strStartTime = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text.ToString().Trim();

                                        var dtFlyingDays = Convert.ToDateTime(txtSplitAvaFrom.Text).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00";
                                        var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).Date.ToString("yyyy-MM-dd") + ' ' + strStartTime.Trim() + ":00").Subtract(DateTime.Now);
                                        var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");


                                        string stractive = string.Empty;

                                        stractive = "1";

                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[0] = "I";
                                        }
                                        else
                                        {
                                            CategoryDetails[0] = "U";
                                        }
                                        string atctivetail = string.Empty;

                                        atctivetail = "1";
                                        CategoryDetails[1] = hdfSCID.Value;
                                        CategoryDetails[2] = txtTailNo.Value.Trim();
                                        CategoryDetails[3] = txtTailNo.Value.Trim();
                                        CategoryDetails[4] = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        CategoryDetails[5] = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;
                                        CategoryDetails[6] = ((Label)gvLegDetails.Rows[i].FindControl("lblstarttime")).Text;
                                        CategoryDetails[7] = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        CategoryDetails[8] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;
                                        CategoryDetails[9] = ((Label)gvLegDetails.Rows[i].FindControl("lblEndTime")).Text;
                                        CategoryDetails[10] = "";
                                        CategoryDetails[11] = ((Label)gvLegDetails.Rows[i].FindControl("lblFlyHours")).Text.ToString().Trim();
                                        CategoryDetails[12] = Convert.ToDateTime(txtSplitAvaFrom.Text.Trim()).ToString("yyyy-MM-dd");
                                        CategoryDetails[13] = Convert.ToDateTime(txtSplitAvaTo.Text.Trim()).ToString("yyyy-MM-dd");
                                        CategoryDetails[15] = "";
                                        CategoryDetails[41] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[42] = DateTime.Now.ToString("yyyy-MM-dd");
                                        CategoryDetails[43] = DateTime.Now.ToString("HH:mm");
                                        CategoryDetails[44] = DateTime.Now.ToString("HH:mm");
                                        if (hdfSCID.Value == "0")
                                        {
                                            DataTable dttailTable = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' ").Tables[0];
                                            if (dttailTable.Rows.Count > 0)
                                            {
                                            }
                                            else
                                            {
                                                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Insert Into  Tailsetup values ('" + txtTailNo.Value.Trim() + "','" + "1" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 1 + "','EMPTY','" + Request.Cookies["JETRIPSADMIN"]["UserId"].ToString() + "','" + DateTime.Now.ToString() + "','0','None','0','0','','0') ");
                                            }

                                            DataTable dttailTableEx = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *,isnull( noofseats,0) noofseatsT  FROM  Tailsetup  Where TailNo='" + txtTailNo.Value.Trim() + "' and pricingcompleted='1'").Tables[0];
                                            if (dttailTableEx.Rows.Count > 0)
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinBD"].ToString()).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(dttailTableEx.Rows[0]["MaxBD"].ToString()).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(dttailTableEx.Rows[0]["MinSellable"].ToString()).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(dttailTableEx.Rows[0]["HourlyRate"].ToString()).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedFloor"].ToString()).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(dttailTableEx.Rows[0]["FixedCeiling"].ToString()).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(0).ToString();
                                                CategoryDetails[35] = "1";
                                                CategoryDetails[33] = dttailTableEx.Rows[0]["applyTax"].ToString().ToString();
                                                CategoryDetails[40] = dttailTableEx.Rows[0]["length"].ToString().ToString();
                                                CategoryDetails[14] = dttailTableEx.Rows[0]["noofseatsT"].ToString().ToString();
                                            }
                                            else
                                            {
                                                CategoryDetails[20] = Convert.ToDecimal(D4_MinTimeBD).ToString();
                                                CategoryDetails[21] = Convert.ToDecimal(D5_MaxTimeBD).ToString();
                                                CategoryDetails[22] = Convert.ToDecimal(MinFLHSellable).ToString();
                                                CategoryDetails[23] = Convert.ToDecimal(HourlyRate).ToString();
                                                CategoryDetails[24] = Convert.ToDecimal(fixedFloor).ToString();
                                                CategoryDetails[25] = Convert.ToDecimal(fixedCeiling).ToString();
                                                CategoryDetails[26] = Convert.ToDecimal(finalPricevalidation2).ToString();
                                                CategoryDetails[27] = Convert.ToDecimal(_totalDaysK10).ToString();
                                                CategoryDetails[28] = Convert.ToDecimal(L10_minPrice).ToString();
                                                CategoryDetails[29] = Convert.ToDecimal(M10_maxPrice).ToString();
                                                CategoryDetails[35] = "0";
                                                CategoryDetails[33] = "1";
                                                CategoryDetails[40] = "0";
                                                CategoryDetails[14] = "0";
                                            }
                                        }

                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[16] = "0.00";
                                        CategoryDetails[30] = totalflyingHours.ToString("0.00");
                                        if (chkEmpty.Checked)
                                        {
                                            CategoryDetails[19] = "1";
                                        }
                                        else
                                        {
                                            CategoryDetails[19] = "0";
                                        }
                                        string s = DateTime.Now.ToString("HH:mm:ss.fff");

                                        TimeSpan ts = TimeSpan.Parse(s);
                                        CategoryDetails[31] = ts.ToString();
                                        CategoryDetails[32] = DateTime.Now.Date.ToString();
                                        if (hdfSCID.Value == "0")
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        else
                                        {
                                            CategoryDetails[34] = txttrip.Value.Trim();
                                        }
                                        CategoryDetails[36] = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
                                        CategoryDetails[37] = DateTime.Now.Date.ToString();
                                        CategoryDetails[38] = txttrip.Value.Trim();
                                        CategoryDetails[39] = txtTailNo.Value.Trim();
                                        CategoryDetails[45] = ((Label)gvLegDetails.Rows[i].FindControl("lblTripFlag")).Text.ToString().Trim();
                                        CategoryDetails[46] = lblgroupiddisplay.Text;
                                        CategoryDetails[47] = lbllegdisplay.Text.Trim();
                                        CategoryDetails[48] = rblBreak.SelectedValue.Trim();
                                        string strReturnVal = objMember.saveCategoryGroup(CategoryDetails);
                                    }

                                    #endregion


                                    if (gvLegDetails.Rows[i].FindControl("lblTripFlag").ToString().Trim() != "B0")
                                    {
                                        string strStartAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblstartair")).Text;
                                        string strStartCity = ((Label)gvLegDetails.Rows[i].FindControl("lblStartCity")).Text;

                                        string strEndAirport = ((Label)gvLegDetails.Rows[i].FindControl("lblendair")).Text;
                                        string strEndCity = ((Label)gvLegDetails.Rows[i].FindControl("lblEndCity")).Text;



                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.Trim() + "','SP','Dep', 'Airport', '" + strStartCity + " - " + strStartAirport + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strStartAirport.Trim() + "')");

                                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO EmptyLegAirport_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode) VALUES ('" + lblgroupiddisplay.Text.Trim() + "','SP','Arr', 'Airport', '" + strEndCity + " - " + strEndAirport + "','" + lblUser.Text + "','" + DateTime.Now.ToString() + "','','','" + strEndAirport.Trim() + "')");

                                    }
                                }
                            }
                        }

                        #endregion
                    }
                }

                #endregion
            }

            #endregion

            CalculateLivePrice();

            lblalert.Text = "Empty leg details have been saved successfully";
            mpealert.Show();
            hdfSCID.Value = "0";
            BindEmptyLegs();
            tblForm.Visible = false;
            tblGrid.Visible = true;
            rblStatus.Visible = true;
            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvTailDetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ViewState["dtLegDetails"] = null;
            gvLegDetails.DataSource = null;
            gvLegDetails.DataBind();

            ViewState["dtDepAirport"] = null;
            gvDepAirport.DataSource = null;
            gvDepAirport.DataBind();
            txtDepDisLandPage.Text = string.Empty;
            divDepLan.Visible = false;
            ddlDepSetupBy.ClearSelection();


            ViewState["dtArrAirport"] = null;
            gvArrivalAirport.DataSource = null;
            gvArrivalAirport.DataBind();
            txtArrDisLandPage.Text = string.Empty;
            divArrLan.Visible = false;
            ddlArrSetupBy.ClearSelection();

            trSplit.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            btnAdd.Visible = false;
            rblStatus.Visible = false;
            trManualandModiy.Visible = false;
            divModify.Visible = true;
            divManualAirport.Visible = true;
            Label TailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
            Label lbltrip = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
            Label lblType = (Label)gvTailDetails.SelectedRow.FindControl("lblType");
            Label lblGroupId = (Label)gvTailDetails.SelectedRow.FindControl("lblGroupId");
            Label lblMFlag = (Label)gvTailDetails.SelectedRow.FindControl("lblModFlag");
            lblModifyFlag.Text = lblMFlag.Text.Trim();
            lbltripflagupdate.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblTripFlag")).Text.Trim();
            Loadcountry();

            #region

            if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text == "0")
            {
                tbldata.Visible = true;
                divPriceModRight.Visible = true;
                divPricModLeft.Visible = true;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                lblSubScren.Text = "Modify a FOS Trip";
                trManualandModiy.Visible = true;
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

                LBLTRIPNO.Text = lbltripnoG.Text.Trim();

                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                lbllegdisplay.Text = lblleg.Text;

                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lblTripData.Text;
                txtFlyHours1.Value = lblFlyingHours.Text;
                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "").Replace(".00", "");

                txtStartTime.Value = lblStarttime.Text;
                txtEndTime.Value = lblEndTime.Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                //                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:GetDepAirport();", true);
                txtMod_DepCity.Visible = true;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = false;
                txtstartsplit.Value = lblstartcity.Text + " - " + lblstart.Text;

                txtEndCity.Text = lblEndcity.Text + " - " + lblend.Text;

                txtStartCity.Text = lblstartcity.Text + " - " + lblstart.Text;

                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                txtAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");

                //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
                //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

                //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
                //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

                if (lblMFlag.Text.Trim() == "ET" && lblGroupId.Text.Trim() == "0")
                {
                    DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid, New_StartAirport,New_EndAirport  FROM  OneWayTail_Setup  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "'").Tables[0];

                    if (dtOneWayTSel.Rows.Count > 0)
                    {
                        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and ModifiedFlag='ET' and AirportFor='Dep'").Tables[0];

                        if (dt.Rows.Count > 0)
                        {
                            gvDepAirport.DataSource = dt;
                            gvDepAirport.DataBind();
                            ViewState["dtDepAirport"] = dt;
                            divDepLan.Visible = true;
                            txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
                        }
                        else
                        {
                            divDepLan.Visible = false;
                        }

                        DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + dtOneWayTSel.Rows[0]["rowid"].ToString().Trim() + "' and ModifiedFlag='ET' and AirportFor='Arr'").Tables[0];

                        if (dtArr.Rows.Count > 0)
                        {
                            gvArrivalAirport.DataSource = dtArr;
                            gvArrivalAirport.DataBind();
                            ViewState["dtArrAirport"] = dtArr;
                            txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
                            divArrLan.Visible = true;
                        }
                        else
                        {
                            divArrLan.Visible = false;
                        }
                    }

                }


                lblgroup.Text = "Modify";
                txtTailNo.Disabled = true;
                txttrip.Disabled = true;
                tbldata.Visible = true;
                divstartcity.Visible = true;
                divstartcityLeft.Visible = true;
                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplit.Visible = false;
                divstartsplitLeft.Attributes.Add("style", "display:none");
                txtstartsplit.Disabled = true;
                divendcity.Visible = true;
                btnSave.Visible = true;
                tblForm.Visible = true;
                tblGrid.Visible = false;
            }

            #endregion

            #region Edited Split

            else if (lblType.Text.Trim() == "Split" && lblGroupId.Text != "0")
            {
                lblSubScren.Text = "Add a Manual Leg";
                divPriceModRight.Visible = false;
                divPricModLeft.Visible = false;
                trManualandModiy.Visible = false;
                DataTable dtPS = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag<>'B0' Order by groupid").Tables[0];

                lblgroupiddisplay.Text = lblGroupId.Text.Trim();

                trSplit.Visible = true;
                tbldata.Visible = true;
                divstartcounty.Attributes.Add("style", "display:''");
                divstartsplitLeft.Attributes.Add("style", "display:none");
                trLeg1.Attributes.Add("style", "display:none");
                trLeg2.Attributes.Add("style", "display:none");
                if (dtPS != null && dtPS.Rows.Count > 0)
                {
                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");


                    //  lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;

                    //lblupdatedon.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedon")).Text;


                    txtSpliFlyHour.Value = lblFlyingHours.Text;
                    txtNoSeats.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                    txtSpliFlyHour.Value = lblFlyingHours.Text;
                    txtMPrice.Value = dtPS.Rows[0]["CurrentPrice"].ToString();

                    lblTailData.Text = lblTailNo.Text;
                    lblTripData.Text = lbltripnoG.Text;
                    lblStartData.Text = lblstart.Text;
                    lblendData.Text = lblend.Text;
                    lblscitydata.Text = lblstartcity.Text;
                    lblEcityData.Text = lblEndcity.Text;
                    lblStimedata.Text = lblStarttime.Text;
                    lblEtimeData.Text = lblEndTime.Text;
                    lblDateData.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MMM dd, yyyy").ToUpper();
                    lblstartdate.Text = Convert.ToDateTime(dtPS.Rows[0]["DDAte"].ToString()).ToString("MM-dd-yyyy");
                    lblenddate.Text = Convert.ToDateTime(dtPS.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
                    lbllegdisplay.Text = lblleg.Text;

                    txtTailNo.Value = lblTailNo.Text;
                    txttrip.Value = lbltripnoG.Text;
                    LBLTRIPNO.Text = txttrip.Value.Trim();

                    txtcountry.Text = string.Empty;
                    DataTable dtbase2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
                    if (dtbase2.Rows.Count > 0)
                    {
                        if (dtbase2.Rows[0]["tripflag"].ToString() == "B1")
                        {

                            DataTable dtPS2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from LivePricingCaculation where Groupid is not null and Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL' and Tripflag='B1' Order by groupid").Tables[0];
                            // txtStartCity.Text = dtPS2.Rows[0]["Endcity"].ToString() + " - " + dtPS2.Rows[0]["End"].ToString();
                            string strSpeed = string.Empty;
                            DataTable dtPS1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from Tailsetup Where TailNo='" + txtTailNo.Value + "'").Tables[0];
                            if (dtPS1.Rows.Count > 0)
                            {
                                strSpeed = dtPS1.Rows[0]["AvgSpeed"].ToString();
                            }
                            DataTable dtResul = new DataTable();
                            try
                            {

                                dtResul = AirRouteCalculation(dtPS2.Rows[0]["End"].ToString(), lblendData.Text, strSpeed);
                            }
                            catch (Exception ex)
                            {

                            }

                            if (dtResul.Rows.Count > 0)
                            {
                                txtSpliFlyHour.Value = (Math.Round(Convert.ToDecimal(dtResul.Rows[0]["flight_time_min"].ToString()) / 60, 2)).ToString();
                            }
                        }
                    }

                    DataTable dtpreference = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select TailNo as TailName,   TailNo, convert(varchar(12),DDate,109) DDate ,convert(varchar(12),ADate,109) ADate ,DDate [DateFormat],  CONVERT(VARCHAR(5), StartTime, 108) DTime,
		                                                CONVERT(VARCHAR(5), EndTime, 108)  ATime,Start+'-->'+[End] [routes], Start, StartCity, [End], EndCity, isnull(NoOFSeats,0) NoOFSeats,
		                                                FlyingHoursFOS, CalcualtedFlyingHrs FLTTIMEMINS,TripNo,
		                                                DaysToFlight,tripflag,Groupid,splittype from LivePricingCaculation where Groupid is not null and 
                                                        Groupid='" + lblGroupId.Text.Trim() + "' and   flag='FINAL'  Order by groupid").Tables[0];

                    if (dtpreference.Rows.Count > 0)
                    {
                        dtpreference.Columns.Add("leg");

                        for (int i = 0; i < dtpreference.Rows.Count; i++)
                        {
                            DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + dtpreference.Rows[i]["Tailno"].ToString() + @"' and tripno='" + dtpreference.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(dtpreference.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(dtpreference.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(dtpreference.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + lblstart.Text + @"'
                and EndAirport='" + lblend.Text + @"'").Tables[0];
                            if (dtleg.Rows.Count > 0)
                            {
                                dtpreference.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                            }
                        }

                        for (int i = 0; i < dtpreference.Rows.Count; i++)
                        {
                            if (i > 0)
                            {
                                dtpreference.Rows[i]["leg"] = dtpreference.Rows[0]["leg"].ToString();
                            }
                        }

                        ViewState["Tripgrid"] = dtpreference;

                        DataTable dt = new DataTable();
                        dtpreference.DefaultView.Sort = "Tripflag asc";
                        dt = dtpreference.DefaultView.ToTable();
                        rblBreak.SelectedValue = "CO";
                        DataRow[] result1 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag='B1' and splittype='CO'");

                        if (result1.Length > 0)
                        {
                            rblBreak.SelectedValue = "CO";
                            DataTable dttemp1 = result1.CopyToDataTable();
                            hdfSCID.Value = "0";
                            LBLTRIPNO.Text = dttemp1.Rows[0]["tripno"].ToString();
                            lbloldtail.Text = dttemp1.Rows[0]["tailno"].ToString();
                            txttrip.Value = dttemp1.Rows[0]["tripno"].ToString();
                            txtTailNo.Value = dttemp1.Rows[0]["TailNo"].ToString();
                            txtCouStartTime.Value = dttemp1.Rows[0]["DTime"].ToString();
                            txtSplitLeg1EndTime.Text = dttemp1.Rows[0]["ATime"].ToString();
                            txtSpliFlyHour.Value = dttemp1.Rows[0]["FlyinghoursFOS"].ToString().Replace(":", ".");

                            DataTable dtTake = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select Mod_AvailableTo,Mod_AvilableFrom,Mod_EndAirport,Mod_Endcity,Mod_StartAirport,Mod_Startcity from Tailsetup_tripDetails where Groupid='" + lblGroupId.Text.Trim() + "' and TripFlag='B1' and  ModifyFlag='SP'  and Mod_AvilableFrom!='1900-01-01 00:00:00.000'").Tables[0];

                            if (dtTake.Rows.Count > 0)
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dtTake.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");
                                ddlLeg1EndCity.Text = dtTake.Rows[0]["Mod_Endcity"].ToString() + " - " + dtTake.Rows[0]["Mod_EndAirport"].ToString();
                            }
                            else
                            {
                                txtSplitAvaFrom.Text = Convert.ToDateTime(dttemp1.Rows[0]["DDate"].ToString()).ToString("MM-dd-yyyy");
                                txtSplitAvaTo.Text = Convert.ToDateTime(dttemp1.Rows[0]["ADate"].ToString()).ToString("MM-dd-yyyy");
                            }

                            txtNoSeats.Value = dttemp1.Rows[0]["NoOFSeats"].ToString();
                            txtcountry.Text = dttemp1.Rows[0]["Start"].ToString();
                            divstartcounty.Attributes.Add("style", "display:''");

                        }
                        DataRow[] result2 = dt.Select("TAilno='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and Tripflag in ('B1','B2','B3') and splittype='CT' ");

                        if (result2.Length > 0)
                        {
                            divstartcounty.Attributes.Add("style", "display:none");
                            divstartsplitLeft.Attributes.Add("style", "display:''");
                            trLeg1.Attributes.Add("style", "display:''");
                            trLeg2.Attributes.Add("style", "display:''");
                            rblBreak.SelectedValue = "CT";


                            ddlStartCitySplit.Text = string.Empty;
                            ddlLeg1EndCity.Text = string.Empty;
                            txtSpliFlyHour.Value = string.Empty;
                            txtSplitLeg1StartTime.Text = string.Empty;
                            txtSplitLeg1EndTime.Text = string.Empty;

                            DataTable dttemp1 = result2.CopyToDataTable();
                            ViewState["dtLegDetails"] = dttemp1;
                            gvLegDetails.DataSource = dttemp1;
                            gvLegDetails.DataBind();
                            if (gvLegDetails.Rows.Count == 3)
                            {
                                btnAddLeg.Visible = false;
                                divSplitEnd.Visible = false;
                                divSplitFlyHour.Visible = false;
                                divstartsplitLeft.Attributes.Add("style", "display:none;");
                            }
                            else
                            {
                                btnAddLeg.Visible = true;
                                divSplitEnd.Visible = true;
                                divSplitFlyHour.Visible = true;
                                divstartsplitLeft.Attributes.Add("style", "display:'';");
                            }
                            hdfSCID.Value = "0";
                        }
                        else
                        {
                            ViewState["dtLegDetails"] = null;
                            gvLegDetails.DataSource = null;
                            gvLegDetails.DataBind();
                        }
                    }
                }
                DataTable dtbase = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select max(Tripflag) as tripflag from LivePricingCaculation where groupid='" + lblGroupId.Text.Trim() + "'").Tables[0];
                if (dtbase.Rows.Count > 0)
                {
                    if (dtbase.Rows[0]["tripflag"].ToString() == "B0")
                    {
                        lblbaseflag.Text = "B1";
                    }
                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B1")
                    {
                        lblbaseflag.Text = "B2";
                    }
                    else if (dtbase.Rows[0]["tripflag"].ToString() == "B2")
                    {
                        lblbaseflag.Text = "B3";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Split();", true);
                btnSave.Text = "Update";
                tblForm.Visible = true;
                tblGrid.Visible = false;
                lblgroup.Text = "Group";
            }

            #endregion

            #region New Split

            else if ((lblType.Text.Trim() == "Split" && lblGroupId.Text == "0"))
            {
                trSplit.Visible = true;
                divPriceModRight.Visible = false;
                divPricModLeft.Visible = false;
                lblSubScren.Text = "Add a Manual Leg";
                trManualandModiy.Visible = false;

                txtcountry.Text = string.Empty;
                rblBreak.SelectedValue = "CT";
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");

                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");

                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");

                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");

                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");

                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");


                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");

                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");

                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                LBLTRIPNO.Text = lbltripnoG.Text.Trim();
                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                lbllegdisplay.Text = lblleg.Text;

                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lbltripnoG.Text;
                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                hdnstarttime.Value = lblStarttime.Text;
                hdnendtime.Value = lblEndTime.Text;
                ddlStartCitySplit.Text = lblstartcity.Text + " - " + lblstart.Text;
                ddlStartCitySplit.Enabled = false;
                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                tbldata.Visible = true;


                btnSave.Visible = true;

                divstartcounty.Attributes.Add("style", "display:none");
                divstartsplitLeft.Attributes.Add("style", "display:''");
                trLeg1.Attributes.Add("style", "display:''");
                trLeg2.Attributes.Add("style", "display:''");

                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
                saveCategoryParam[0].Value = 1;//MANAGETYPE
                saveCategoryParam[1].Value = 25;//Rowid
                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
                saveCategoryParam[3].Value = strEndDate;//Rowid
                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];

                DataTable dtAdvSearch = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM Airportmaster").Tables[0];
                if (dtOneWayTrip.Rows.Count > 0)
                {
                    for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtAdvSearch.Rows.Count; j++)
                        {
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["Start"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["End"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["End"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtOneWayTrip.Rows[i]["End"].ToString())
                            {
                                string strk = dtOneWayTrip.Rows[i]["routes"].ToString().Replace("-->", @"@");
                                String[] strlist = strk.Split(new[] { '@' });
                                dtOneWayTrip.Rows[i]["Start"] = strlist[0];
                                dtOneWayTrip.Rows[i]["End"] = strlist[1];
                            }
                        }
                    }
                }



                DataView dv = dtOneWayTrip.DefaultView;

                DataTable sortedDT = dv.ToTable();
                sortedDT.Columns.Add("leg");
                for (int i = 0; i < sortedDT.Rows.Count; i++)
                {
                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
                    String[] strlist = routes.Split(new[] { ',' });
                    string startD = strlist[0];
                    string EndD = strlist[1];
                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
                and EndAirport='" + EndD + @"'").Tables[0];
                    if (dtleg.Rows.Count > 0)
                    {
                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                    }
                }

                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
                DataTable dt = new DataTable();
                if (rowsFilteredConditionAND.Length > 0)
                {
                    dt = rowsFilteredConditionAND.CopyToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    ViewState["tripBase"] = dt;

                    lblbaseflag.Text = "B1";

                    lblgroup.Text = "Group";
                }

                tblForm.Visible = true;
                tblGrid.Visible = false;
                ddlLeg1EndCity.Text = string.Empty;
                txtSpliFlyHour.Value = string.Empty;
            }

            #endregion

            #region Modified Split

            else if (lblType.Text == "MSplit" && lblGroupId.Text != "0")
            {
                trSplit.Visible = true;
                divPriceModRight.Visible = false;
                divPricModLeft.Visible = false;
                lblSubScren.Text = "Add a Manual Leg";
                trManualandModiy.Visible = false;

                txtcountry.Text = string.Empty;
                rblBreak.SelectedValue = "CO";
                Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("Label1");
                Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label2");
                Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("Label1");

                lblTailData.Text = lblTailNo.Text;
                lblTripData.Text = lbltripnoG.Text;
                lblStartData.Text = lblstart.Text;
                lblendData.Text = lblend.Text;
                lblscitydata.Text = lblstartcity.Text;
                lblEcityData.Text = lblEndcity.Text;
                lblStimedata.Text = lblStarttime.Text;
                lblEtimeData.Text = lblEndTime.Text;
                lblDateData.Text = Convert.ToDateTime(lblavilfrom.Text).ToString("MMM dd, yyyy").ToUpper();
                if (lblleg.Text == "" || lblleg.Text == "0")
                {
                    lbllegdisplay.Text = "1";
                }
                else
                {
                    lbllegdisplay.Text = lblleg.Text;
                }


                txtTailNo.Value = lblTailNo.Text;
                txttrip.Value = lbltripnoG.Text;
                txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "");
                LBLTRIPNO.Text = txttrip.Value.Trim();

                lblstartdate.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                lblenddate.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaFrom.Text = Convert.ToDateTime(lblstartdate1.Text).ToString("MM-dd-yyyy");
                txtSplitAvaTo.Text = Convert.ToDateTime(lblenddate1.Text).ToString("MM-dd-yyyy");
                tbldata.Visible = true;


                btnSave.Visible = true;

                divstartcounty.Attributes.Add("style", "display:''");
                divstartsplitLeft.Attributes.Add("style", "display:none");
                trLeg1.Attributes.Add("style", "display:none");
                trLeg2.Attributes.Add("style", "display:none");

                string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

                SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", false);
                saveCategoryParam[0].Value = 1;//MANAGETYPE
                saveCategoryParam[1].Value = 25;//Rowid
                saveCategoryParam[2].Value = strStartDate;//MANAGETYPE
                saveCategoryParam[3].Value = strEndDate;//Rowid
                DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "[GetEmptytripsDatatable]", saveCategoryParam).Tables[0];

                DataTable dtAdvSearch = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM Airportmaster").Tables[0];
                if (dtOneWayTrip.Rows.Count > 0)
                {
                    for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
                    {
                        for (int j = 0; j < dtAdvSearch.Rows.Count; j++)
                        {
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["Start"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["End"].ToString() == dtAdvSearch.Rows[j]["ICAO"].ToString())
                            {
                                dtOneWayTrip.Rows[i]["End"] = dtAdvSearch.Rows[j]["Country"].ToString();
                            }
                            if (dtOneWayTrip.Rows[i]["Start"].ToString() == dtOneWayTrip.Rows[i]["End"].ToString())
                            {
                                string strk = dtOneWayTrip.Rows[i]["routes"].ToString().Replace("-->", @"@");
                                String[] strlist = strk.Split(new[] { '@' });
                                dtOneWayTrip.Rows[i]["Start"] = strlist[0];
                                dtOneWayTrip.Rows[i]["End"] = strlist[1];
                            }
                        }
                    }
                }



                DataView dv = dtOneWayTrip.DefaultView;

                DataTable sortedDT = dv.ToTable();
                sortedDT.Columns.Add("leg");
                for (int i = 0; i < sortedDT.Rows.Count; i++)
                {
                    string routes = sortedDT.Rows[i]["routes"].ToString().Replace("-->", ",");
                    String[] strlist = routes.Split(new[] { ',' });
                    string startD = strlist[0];
                    string EndD = strlist[1];
                    DataTable dtleg = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, @"select * from Tailsetup_tripDetails
                Where TailNo='" + sortedDT.Rows[i]["Tailno"].ToString() + @"' and tripno='" + sortedDT.Rows[i]["tripno"].ToString() + @"' and AvailbleFrom='" + Convert.ToDateTime(sortedDT.Rows[i]["DDAte"].ToString()).ToString("yyyy-MM-dd") + @"' and StartTime='" + Convert.ToDateTime(sortedDT.Rows[i]["DTime"].ToString()).ToString("HH:mm") + @"' and EndTime='" + Convert.ToDateTime(sortedDT.Rows[i]["ATime"].ToString()).ToString("HH:mm") + @"' and StartAirport='" + startD + @"'
                and EndAirport='" + EndD + @"'").Tables[0];
                    if (dtleg.Rows.Count > 0)
                    {
                        sortedDT.Rows[i]["leg"] = dtleg.Rows[0]["leg"].ToString();
                    }
                }

                DataRow[] rowsFilteredConditionAND = sortedDT.Select("TailNo='" + lblTailNo.Text + "' and Tripno='" + lbltripnoG.Text + "' and leg='" + lbllegdisplay.Text + "' and Dateformat='" + Convert.ToDateTime(lblDateData.Text) + "' ", "Dtime asc");
                DataTable dt = new DataTable();
                if (rowsFilteredConditionAND.Length > 0)
                {
                    dt = rowsFilteredConditionAND.CopyToDataTable();
                }

                if (dt.Rows.Count > 0)
                {
                    ViewState["tripBase"] = dt;


                }
                lblbaseflag.Text = "B1";

                lblgroup.Text = "Group";
                tblForm.Visible = true;
                tblGrid.Visible = false;
            }

            #endregion

            #region Modiied FOS
            else if (lblType.Text.Trim() == "FOS DH" && lblGroupId.Text != "0")
            {
                lblgroup.Text = "Modify";
                lblSubScren.Text = "Modify a FOS Trip";
                trManualandModiy.Visible = true;
                trSplit.Visible = false;
                tbldata.Visible = true;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  TailNO,TripNo,NoOfseats, Mod_AvilableFrom, Mod_AvailableTo, Mod_StartAirport, Mod_Startcity,Mod_EndAirport,Mod_Endcity,Mod_Starttime,Mod_Endtime,Mod_FlyingHours,Mod_Price,CreatedOn,Active,New_StartAirport,New_EndAirport  FROM  Tailsetup_tripDetails  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "' and GroupId='" + lblGroupId.Text.Trim() + "' and TripFlag='" + lbltripflagupdate.Text + "' and Activeflag='ACTIVE'").Tables[0];
                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
                {
                    Label lblTailNo = (Label)gvTailDetails.SelectedRow.FindControl("lblTailNo");
                    Label lblStarttime = (Label)gvTailDetails.SelectedRow.FindControl("lblstarttime");
                    Label lblEndTime = (Label)gvTailDetails.SelectedRow.FindControl("lblEndTime");
                    Label lblstart = (Label)gvTailDetails.SelectedRow.FindControl("lblstartair");
                    Label lblend = (Label)gvTailDetails.SelectedRow.FindControl("lblendair");
                    Label lblstartcity = (Label)gvTailDetails.SelectedRow.FindControl("lblstartcity");
                    Label lblEndcity = (Label)gvTailDetails.SelectedRow.FindControl("lblendcity");
                    Label lblavilfrom = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                    Label lblpricing = (Label)gvTailDetails.SelectedRow.FindControl("lblPrice");
                    Label lblFlyingHours = (Label)gvTailDetails.SelectedRow.FindControl("lblFlyHours");
                    Label lblleg = (Label)gvTailDetails.SelectedRow.FindControl("lblleg");
                    Label lbltripnoG = (Label)gvTailDetails.SelectedRow.FindControl("lbltrip");
                    Label lblenddate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate1");
                    Label lblstartdate1 = (Label)gvTailDetails.SelectedRow.FindControl("lbldate");
                    lbllegdisplay.Text = lblleg.Text;
                    string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                    string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");
                    DataTable dtGetData = objMember.GET_LIVEDATA("", strStartDate, strEndDate, lblTailNo.Text, lbltrip.Text, lbltripflagupdate.Text.Trim());

                    if (dtGetData.Rows.Count > 0)
                    {
                        lblTailData.Text = dtGetData.Rows[0]["TailNo"].ToString();
                        lblTripData.Text = dtGetData.Rows[0]["TripNo"].ToString();
                        lblStartData.Text = dtGetData.Rows[0]["StartAirport"].ToString();
                        lblendData.Text = dtGetData.Rows[0]["EndAirport"].ToString();
                        lblscitydata.Text = dtGetData.Rows[0]["StartCity"].ToString();
                        lblEcityData.Text = dtGetData.Rows[0]["EndCity"].ToString();
                        lblStimedata.Text = dtGetData.Rows[0]["StartTime"].ToString();
                        lblEtimeData.Text = dtGetData.Rows[0]["Endtime"].ToString();
                        lblDateData.Text = Convert.ToDateTime(dtGetData.Rows[0]["AvailbleFrom"].ToString()).ToString("MMM dd, yyyy").ToUpper();
                    }

                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Dep'").Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        gvDepAirport.DataSource = dt;
                        gvDepAirport.DataBind();
                        ViewState["dtDepAirport"] = dt;
                        divDepLan.Visible = true;
                        txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
                    }
                    else
                    {
                        divDepLan.Visible = false;
                    }

                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='MF' and AirportFor='Arr'").Tables[0];

                    if (dtArr.Rows.Count > 0)
                    {
                        gvArrivalAirport.DataSource = dtArr;
                        gvArrivalAirport.DataBind();
                        ViewState["dtArrAirport"] = dtArr;
                        txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
                        divArrLan.Visible = true;
                    }
                    else
                    {
                        divArrLan.Visible = false;
                    }

                    txtTailNo.Value = lblTailNo.Text;
                    txttrip.Value = lbltripnoG.Text;
                    txtFlyHours1.Value = lblFlyingHours.Text;
                    txtMPrice.Value = lblpricing.Text.Replace("$", "").Replace(",", "").Replace(".00", "");
                    txtFlyHours1.Value = lblFlyingHours.Text;

                    txtStartTime.Value = lblStarttime.Text;
                    txtEndTime.Value = lblEndTime.Text;

                    hdfSCID.Value = "1";
                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["TripNo"].ToString();
                    lbloldtail.Text = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    txttrip.Value = dtOneWayTSel.Rows[0]["TripNo"].ToString();
                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();

                    if (dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() != "")
                    {
                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    }
                    else
                    {
                        txtStartCity.Text = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    }
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
                    if (dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() != "")
                    {
                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString() + " - " + dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    }
                    else
                    {
                        txtEndCity.Text = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    }

                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Mod_FlyingHours"].ToString().Replace(":", ".");
                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString()).ToString("MM-dd-yyyy");
                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["Mod_AvailableTo"].ToString()).ToString("MM-dd-yyyy");

                    //CalendarExtender2.StartDate = Convert.ToDateTime(txtAvaFrom.Text);
                    //CalendarExtender1.StartDate = Convert.ToDateTime(txtAvaFrom.Text);

                    //CalendarExtender2.EndDate = Convert.ToDateTime(txtAvaTo.Text);
                    //CalendarExtender1.EndDate = Convert.ToDateTime(txtAvaTo.Text);

                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOfseats"].ToString();

                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    hdnDDate.Value = dtOneWayTSel.Rows[0]["Mod_AvilableFrom"].ToString();
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["Mod_Starttime"].ToString();
                    txtEndTime.Value = dtOneWayTSel.Rows[0]["Mod_Endtime"].ToString();
                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["Mod_Startcity"].ToString();
                    hdnendcity.Value = dtOneWayTSel.Rows[0]["Mod_Endcity"].ToString();
                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["Mod_StartAirport"].ToString();
                    hdnendairport.Value = dtOneWayTSel.Rows[0]["Mod_EndAirport"].ToString();
                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();
                    txtMPrice.Value = dtOneWayTSel.Rows[0]["Mod_Price"].ToString();
                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
                    {
                        chkEmpty.Checked = true;
                    }
                    else
                    {
                        chkEmpty.Checked = false;
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                }
                tbldata.Visible = true;
                tblForm.Visible = true;
                tblGrid.Visible = false;
            }

            #endregion

            #region Manual Edit

            else
            {
                trManualandModiy.Visible = true;
                trSplit.Visible = false;
                divManualAirport.Visible = false;
                divModify.Visible = true;
                lblSubScren.Text = "Add a Manual Trip";
                DataTable dtOneWayTSel = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours,  AvailbleFrom,AvilbleTo,NoOFSeats,DecrementType,DecrementAmount,EndAirport, EndCity,Active,TRIPNO,createdby,createdon,MPrice,New_StartAirport,New_EndAirport  FROM  OneWayTail_Setup  where Tailno ='" + TailNo.Text + "' and tripno='" + lbltrip.Text + "'").Tables[0];
                if ((dtOneWayTSel.Rows.Count > 0) && (dtOneWayTSel != null))
                {
                    hdfSCID.Value = "1";
                    lblManEditGroupId.Text = dtOneWayTSel.Rows[0]["rowid"].ToString();
                    LBLTRIPNO.Text = dtOneWayTSel.Rows[0]["tripno"].ToString();
                    lbloldtail.Text = dtOneWayTSel.Rows[0]["tailno"].ToString();
                    txttrip.Value = dtOneWayTSel.Rows[0]["tripno"].ToString();
                    txtTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    txtStartCity.Text = dtOneWayTSel.Rows[0]["StartCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["StartAirport"].ToString();
                    txtStartTime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();

                    txtEndCity.Text = dtOneWayTSel.Rows[0]["EndCity"].ToString() + " - " + dtOneWayTSel.Rows[0]["EndAirport"].ToString();
                    txtEndTime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
                    txtFlyHours1.Value = dtOneWayTSel.Rows[0]["Flyinghours"].ToString().Replace(":", ".");
                    txtAvaFrom.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString()).ToString("MM-dd-yyyy");
                    txtAvaTo.Text = Convert.ToDateTime(dtOneWayTSel.Rows[0]["AvilbleTo"].ToString()).ToString("MM-dd-yyyy");
                    txtNoSeats.Value = dtOneWayTSel.Rows[0]["NoOFSeats"].ToString();
                    hdnTailNo.Value = dtOneWayTSel.Rows[0]["TailNo"].ToString();
                    hdnDDate.Value = dtOneWayTSel.Rows[0]["AvailbleFrom"].ToString();
                    hdnstarttime.Value = dtOneWayTSel.Rows[0]["StartTime"].ToString();
                    hdnendtime.Value = dtOneWayTSel.Rows[0]["EndTime"].ToString();
                    hdnstartcity.Value = dtOneWayTSel.Rows[0]["StartCity"].ToString();
                    hdnendcity.Value = dtOneWayTSel.Rows[0]["EndCity"].ToString();
                    hdnstartAirport.Value = dtOneWayTSel.Rows[0]["StartAirport"].ToString();
                    hdnendairport.Value = dtOneWayTSel.Rows[0]["EndAirport"].ToString();
                    //lblupdatedby.Text = ((Label)gvTailDetails.SelectedRow.FindControl("lblcreatedby")).Text;
                    //lblupdatedon.Text = dtOneWayTSel.Rows[0]["createdon"].ToString();
                    txtMPrice.Value = dtOneWayTSel.Rows[0]["MPrice"].ToString().Replace(".00", "");
                    if (dtOneWayTSel.Rows[0]["Active"].ToString() == "1")
                    {
                        chkEmpty.Checked = true;
                    }
                    else
                    {
                        chkEmpty.Checked = false;
                    }


                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Dep'").Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        gvDepAirport.DataSource = dt;
                        gvDepAirport.DataBind();
                        ViewState["dtDepAirport"] = dt;
                        divDepLan.Visible = true;
                        txtDepDisLandPage.Text = dtOneWayTSel.Rows[0]["New_StartAirport"].ToString();
                    }
                    else
                    {
                        divDepLan.Visible = false;
                    }

                    DataTable dtArr = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  *  FROM  EmptyLegAirport_Child  where  GroupId='" + lblGroupId.Text.Trim() + "' and ModifiedFlag='ET' and AirportFor='Arr'").Tables[0];

                    if (dtArr.Rows.Count > 0)
                    {
                        gvArrivalAirport.DataSource = dtArr;
                        gvArrivalAirport.DataBind();
                        ViewState["dtArrAirport"] = dtArr;
                        txtArrDisLandPage.Text = dtOneWayTSel.Rows[0]["New_EndAirport"].ToString();
                        divArrLan.Visible = true;
                    }
                    else
                    {
                        divArrLan.Visible = false;
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

                }

                tblForm.Visible = true;
                tblGrid.Visible = false;
            }
            #endregion

            if (gvArrivalAirport.Rows.Count > 1)
            {
                btnArrGroup.Visible = true;
            }
            else
            {
                btnArrGroup.Visible = false;
            }

            if (gvDepAirport.Rows.Count > 1)
            {
                btnCreateGroup.Visible = true;
            }
            else
            {
                btnCreateGroup.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void ddlstatus_onselectedChanged(object sender, EventArgs e)
    {
        BindEmptyLegs();
        gvTailDetails.PageIndex = 0;
        gvTailDetails.DataBind();
    }

    protected void btnconfirmdelsplitdelete_click(object sender, EventArgs e)
    {
        try
        {
            if (lblDeletModFlag.Text.Trim() == "ET" && lblgroupiddelete.Text.Trim() == "0")
            {
                DataTable dtOneWayTSel = objMember.LivePricing_SEL(lblDeleteTailNo.Text, lblDeleteTripNo.Text, "", "", "0", "").Tables[0];

                if (dtOneWayTSel.Rows.Count > 0)
                {
                    objMember.SplitDelete(dtOneWayTSel.Rows[0]["RowId"].ToString());
                }
            }
            else
            {
                objMember.SplitDelete(lblgroupiddelete.Text.Trim());
            }

            CalculateLivePrice();

            tblForm.Visible = false;
            tblGrid.Visible = true;
            BindEmptyLegs();
            hdfSCID.Value = "0";
            lblalert.Text = "Tail setup details has been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void imbtoDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            ImageButton lb = (ImageButton)sender;
            //ImageButton imbExport = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            // Label lblrowid = (Label)gvrow.FindControl("lblrowid");
            Label lblTailNo = (Label)gvrow.FindControl("lblTailNo");
            Label lbltrip = (Label)gvrow.FindControl("lbltrip");
            Label lbldate = (Label)gvrow.FindControl("lbldate");
            Label lblstarttime = (Label)gvrow.FindControl("lblstarttime");
            Label lblstartair = (Label)gvrow.FindControl("lblstartair");
            Label lblendair = (Label)gvrow.FindControl("lblendair");
            Label lblGroupId = (Label)gvrow.FindControl("lblGroupId");
            Label lblType = (Label)gvrow.FindControl("lblType");

            lblDeleteTailNo.Text = lblTailNo.Text;
            lblDeleteTripNo.Text = lbltrip.Text;

            lblDeletModFlag.Text = ((Label)gvrow.FindControl("lblModFlag")).Text;

            if (lblType.Text.Trim() == "Manual DH")
            {
                lbltailconfirm.Text = lblTailNo.Text;
                lbltripconfirm.Text = lbltrip.Text;
                lblDateconfirm.Text = lbldate.Text;
                lblstarttimeconfirm.Text = lblstarttime.Text;
                lblstartairconfirm.Text = lblstartair.Text;
                lblendairconfirm.Text = lblendair.Text;

                mpeconfirm.Show();
            }
            else
            {


                Label lblgroupid = (Label)gvrow.FindControl("lblgroupid");


                lblgroupiddelete.Text = lblgroupid.Text;

                mpesplit.Show();
                //lbllegnoP.Text = lblName.Text;
            }




        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            if (lblgroupiddelete.Text.Trim() == "")
            {
                objMember.ManualDelete("0", lbltailconfirm.Text.Trim(), lbltripconfirm.Text, lblDateconfirm.Text, lblstarttimeconfirm.Text, lblstartairconfirm.Text, lblendairconfirm.Text);
            }
            else
            {
                objMember.ManualDelete(lblgroupiddelete.Text.Trim(), lbltailconfirm.Text.Trim(), lbltripconfirm.Text, lblDateconfirm.Text, lblstarttimeconfirm.Text, lblstartairconfirm.Text, lblendairconfirm.Text);
            }

            CalculateLivePrice();

            tblForm.Visible = false;
            tblGrid.Visible = true;
            BindEmptyLegs();
            hdfSCID.Value = "0";
            lblalert.Text = "Tail setup details has been deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnAddOnClick(object sender, EventArgs e)
    {
        lblgroup.Text = string.Empty;
        divPricModLeft.Visible = true;
        divPriceModRight.Visible = true;
        tblForm.Visible = true;
        tblGrid.Visible = false;
        txtTailNo.Disabled = false;
        txttrip.Disabled = false;
        divstartcity.Visible = true;
        divModify.Visible = true;
        divManualAirport.Visible = false;
        divstartcityLeft.Visible = true;
        divstartcounty.Attributes.Add("style", "display:none");
        btnSave.Text = "Save";
        lblSubScren.Text = "Add a Manual Trip";
        clearform();
        txtTailNo.Focus();
        trSplit.Visible = false;
        rblStatus.Visible = false;
        rblStatus.Visible = true;
        btnSave.Visible = true;
        btnView.Visible = true;
        tbldata.Visible = false;
        trManualandModiy.Visible = true;
        ViewState["dtLegDetails"] = null;
        gvLegDetails.DataSource = null;
        gvLegDetails.DataBind();


        ViewState["dtDepAirport"] = null;
        gvDepAirport.DataSource = null;
        gvDepAirport.DataBind();
        txtDepDisLandPage.Text = string.Empty;
        divDepLan.Visible = false;
        ddlDepSetupBy.ClearSelection();


        ViewState["dtArrAirport"] = null;
        gvArrivalAirport.DataSource = null;
        gvArrivalAirport.DataBind();
        txtArrDisLandPage.Text = string.Empty;
        divArrLan.Visible = false;
        ddlArrSetupBy.ClearSelection();
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        BindEmptyLegs();
        tblForm.Visible = false;
        tblGrid.Visible = true;
        tbldata.Visible = false;
        rblStatus.Visible = true;
    }

    protected void gvTailDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindEmptyLegs();
        gvTailDetails.PageIndex = e.NewPageIndex;
        gvTailDetails.DataBind();
    }

    protected void gvTailDetails_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblcreatedon = e.Row.FindControl("lblcreatedon") as Label;

                Label lblGroupID = e.Row.FindControl("lblGroupID") as Label;

                Label lblType = e.Row.FindControl("lblType") as Label;

                Label lblMod = e.Row.FindControl("lblModFlag") as Label;

                Label lblTailFlag = e.Row.FindControl("lblTailFlag") as Label;

                Label lblTripFlag = e.Row.FindControl("lblTripFlag") as Label;

                ImageButton imgDelete = e.Row.FindControl("imbDelete") as ImageButton;

                if (lblType.Text.Trim() == "FOS DH")
                {
                    lblType.ForeColor = System.Drawing.ColorTranslator.FromHtml("#1B5E20");
                    lblType.Font.Bold = true;
                }
                else if (lblType.Text.Trim() == "Manual DH")
                {
                    lblType.ForeColor = System.Drawing.ColorTranslator.FromHtml("#d32f2f");
                    lblType.Font.Bold = true;
                }
                if (lblGroupID.Text.Trim() == "0" && lblType.Text.Trim() == "FOS DH")
                {
                    imgDelete.Visible = false;
                }
                else if (lblGroupID.Text.Trim() == "0" && lblType.Text.Trim() == "Split")
                {
                    imgDelete.Visible = false;
                }
                else if (lblGroupID.Text.Trim() != "0" && lblType.Text.Trim() == "Split")
                {
                    imgDelete.Visible = true;
                }
                else
                {
                    imgDelete.Visible = true;
                }

                if (lblcreatedon.Text.Trim() == "1/1/1900 12:00:00 AM")
                {
                    lblcreatedon.Text = null;
                }

                if (lblcreatedon.Text.Length > 0)
                {
                    if (Convert.ToDateTime(lblcreatedon.Text.ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        lblcreatedon.Font.Bold = true;
                    }
                    else
                    {
                        lblcreatedon.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
                if (lblMod.Text.Trim() == "SP" && lblTailFlag.Text.Trim() == "AUTO" && lblTripFlag.Text.Trim() == "B0" && lblType.Text.Trim() == "FOS DH")
                {
                    e.Row.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            //  lblMsg.Text = ex.Message;
        }
    }

    protected void Buttonyes_Click(object sender, EventArgs e)
    {
        mpetrip.Hide();
        tblForm.Visible = true;
        tblGrid.Visible = false;
    }

    protected void btnGroupCloseclick(object sender, EventArgs e)
    {
        try
        {
            mpeGroup.Hide();
            ddlArrSetupBy.ClearSelection();
            ddlDepSetupBy.ClearSelection();
            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnAddLegOnClick(object sender, EventArgs e)
    {
        trLeg2.Attributes.Add("style", "display:block");
        trLeg1.Attributes.Add("style", "display:block");
        divstartsplitLeft.Attributes.Add("style", "display:''");
        divstartcounty.Attributes.Add("style", "display:none");
        DataRow drMappingDetails;
        DataTable dtMapping = new DataTable();
        try
        {
            if (ViewState["dtLegDetails"] != null)
            {
                dtMapping = (DataTable)ViewState["dtLegDetails"];
            }
            else
            {
                dtMapping.Columns.Clear();
                dtMapping.Columns.Add("TripFlag");
                dtMapping.Columns.Add("TailNo");
                dtMapping.Columns.Add("tripno");
                dtMapping.Columns.Add("Start");
                dtMapping.Columns.Add("startCity");
                dtMapping.Columns.Add("End");
                dtMapping.Columns.Add("EndCity");
                dtMapping.Columns.Add("DTime");
                dtMapping.Columns.Add("ATime");
                dtMapping.Columns.Add("FlyinghoursFOS");
            }


            drMappingDetails = dtMapping.NewRow();
            if (gvLegDetails.Rows.Count == 0)
            {
                drMappingDetails["TripFlag"] = "B1";
            }
            else if (gvLegDetails.Rows.Count == 1)
            {
                drMappingDetails["TripFlag"] = "B2";
            }
            else if (gvLegDetails.Rows.Count == 2)
            {
                drMappingDetails["TripFlag"] = "B3";
            }

            drMappingDetails["TailNo"] = lblTailData.Text;
            drMappingDetails["tripno"] = lblTripData.Text;

            string[] strStartCitySplit = ddlStartCitySplit.Text.Split('-');

            if (strStartCitySplit.Length >= 1)
            {
                drMappingDetails["start"] = strStartCitySplit[1].ToString();
                drMappingDetails["startCity"] = strStartCitySplit[0].ToString();
            }

            string[] strEndCitySplit = ddlLeg1EndCity.Text.Split('-');
            if (strEndCitySplit.Length >= 1)
            {
                drMappingDetails["End"] = strEndCitySplit[1].ToString();
                drMappingDetails["EndCity"] = strEndCitySplit[0].ToString();
            }

            drMappingDetails["DTime"] = txtSplitLeg1StartTime.Text;
            drMappingDetails["ATime"] = txtSplitLeg1EndTime.Text;
            drMappingDetails["FlyinghoursFOS"] = txtSpliFlyHour.Value.Trim();
            dtMapping.Rows.Add(drMappingDetails);


            ViewState["dtLegDetails"] = dtMapping;
            gvLegDetails.DataSource = dtMapping;
            gvLegDetails.DataBind();

            ddlStartCitySplit.Text = ddlLeg1EndCity.Text.Trim();
            //ddlStartCitySplit.Text = string.Empty;
            ddlLeg1EndCity.Text = string.Empty;
            txtSpliFlyHour.Value = "";

            if (gvLegDetails.Rows.Count == 3)
            {
                btnAddLeg.Visible = false;
                divSplitEnd.Visible = false;
                divSplitFlyHour.Visible = false;
                divstartsplitLeft.Attributes.Add("style", "display:none;");
            }
            else
            {
                btnAddLeg.Visible = true;
                divSplitEnd.Visible = true;
                divSplitFlyHour.Visible = true;
                divstartsplitLeft.Attributes.Add("style", "display:'';");
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvLegDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtLegDetails"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtLegDetails"] = dt;
            gvLegDetails.EditIndex = -1;
            gvLegDetails.DataSource = ViewState["dtLegDetails"];
            gvLegDetails.DataBind();

            if (gvLegDetails.Rows.Count == 3)
            {
                btnAddLeg.Visible = false;
                divSplitEnd.Visible = false;
                divSplitFlyHour.Visible = false;
                divstartsplitLeft.Attributes.Add("style", "display:'none';");
            }
            else
            {
                btnAddLeg.Visible = true;
                divSplitEnd.Visible = true;
                divSplitFlyHour.Visible = true;
                divstartsplitLeft.Attributes.Add("style", "display:'';");
            }

            if (gvLegDetails.Rows.Count > 0)
            {
                for (int i = 0; i < gvLegDetails.Rows.Count; i++)
                {
                    Label lblEndCity = (Label)gvLegDetails.Rows[i].FindControl("lblEndCity");

                    Label lblendair = (Label)gvLegDetails.Rows[i].FindControl("lblendair");


                    ddlStartCitySplit.Text = lblEndCity.Text + " - " + lblendair.Text;
                }
            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvArrivalAirport_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtArrAirport"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtArrAirport"] = dt;
            gvArrivalAirport.EditIndex = -1;
            gvArrivalAirport.DataSource = ViewState["dtArrAirport"];
            gvArrivalAirport.DataBind();

            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }
            if (gvArrivalAirport.Rows.Count > 0)
            {
                string strEndAirportCode = string.Empty;
                string strEndAirportName = string.Empty;

                string strEndStateCode = string.Empty;
                string strEndStateName = string.Empty;

                string strEndCountryCode = string.Empty;
                string strEndCountryName = string.Empty;
                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strEndAirportName += lblAirport.Text.Trim() + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                strEndCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }
                divArrLan.Visible = true;
                txtArrDisLandPage.Text = (strEndCountryName + strEndStateCode + strEndAirportName).TrimEnd('/');
            }
            else
            {
                divArrLan.Visible = false;
                txtArrDisLandPage.Text = string.Empty;
            }
            if (gvArrivalAirport.Rows.Count > 1)
            {
                btnArrGroup.Visible = true;
            }
            else
            {
                btnArrGroup.Visible = false;
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvDepAirport_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtDepAirport"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtDepAirport"] = dt;
            gvDepAirport.EditIndex = -1;
            gvDepAirport.DataSource = ViewState["dtDepAirport"];
            gvDepAirport.DataBind();

            if (txtStartTime.Value != "" && txtEndTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }

            if (gvDepAirport.Rows.Count > 0)
            {
                string strAirportCode = string.Empty;
                string strAirportName = string.Empty;

                string strStateCode = string.Empty;
                string strStateName = string.Empty;

                string strCountryCode = string.Empty;
                string strCountryName = string.Empty;

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strAirportName += lblAirport.Text + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Split(',');

                            if (strSplit.Length > 1)
                            {
                                strStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strCountryCode += strSplit[1].ToString().Trim() + "/";
                                strCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }

                divDepLan.Visible = true;

                txtDepDisLandPage.Text = (strCountryName + strStateCode + strAirportName).TrimEnd('/');
            }
            else
            {
                divDepLan.Visible = false;
                txtDepDisLandPage.Text = string.Empty;
            }
            if (gvDepAirport.Rows.Count > 1)
            {
                btnCreateGroup.Visible = true;
            }
            else
            {
                btnCreateGroup.Visible = false;
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void imbGroupDepDeleteOnclick(object sender, EventArgs e)
    {
        try
        {
            if (txtStartTime.Value != "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            }

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblAirportId");

            objMember.DeleteAirport(lblrowid.Text.Trim());
            objMember.DeleteAirportChild(lblrowid.Text.Trim());


            BindGroup();
            mpeGroup.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnRouteAddDep_Click(object sender, EventArgs e)
    {
        string strAirport = string.Empty;
        string strCode = string.Empty;
        DataRow drDepAirport;
        DataTable dtDepAIrport = new DataTable();
        try
        {
            if (ViewState["dtDepAirport"] != null)
            {
                dtDepAIrport = (DataTable)ViewState["dtDepAirport"];
            }
            else
            {
                dtDepAIrport.Columns.Clear();
                dtDepAIrport.Columns.Add("SetupBy");
                dtDepAIrport.Columns.Add("Airport");
                dtDepAIrport.Columns.Add("StateCode");
                dtDepAIrport.Columns.Add("CountryCode");
                dtDepAIrport.Columns.Add("AirportCode");
                dtDepAIrport.Columns.Add("CreatedBy");
                dtDepAIrport.Columns.Add("CreatedOn");
            }

            drDepAirport = dtDepAIrport.NewRow();

            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                drDepAirport["SetupBy"] = "Airport";
                drDepAirport["Airport"] = txtMod_DepCity.Text.Trim();

                if (txtMod_DepCity.Text.Contains("-"))
                {
                    string strstartcity2 = txtMod_DepCity.Text.Replace(" - ", "-");
                    String[] strliststart = strstartcity2.Split('-');
                    strCode = strliststart[1].ToString();
                }

                drDepAirport["AirportCode"] = strCode.Trim();

            }
            else if (ddlDepSetupBy.SelectedValue == "CO")
            {
                drDepAirport["SetupBy"] = "Country";
                drDepAirport["Airport"] = txtModDepCountry.Text.Trim();

                if (txtModDepCountry.Text.Contains(','))
                {
                    string[] strSplit = txtModDepCountry.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        strCode = strSplit[1].ToString().Trim();
                    }
                }

                drDepAirport["CountryCode"] = strCode.Trim();
            }
            else if (ddlDepSetupBy.SelectedValue == "ST")
            {
                drDepAirport["SetupBy"] = "State";
                drDepAirport["Airport"] = txtModDepState.Text.Trim();

                if (txtModDepState.Text.Length > 0)
                {
                    string[] strSplit = txtModDepState.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                        if (strSTCode.Length > 1)
                        {
                            strCode = strSTCode[0].ToString().Trim();
                        }
                    }
                }

                drDepAirport["StateCode"] = strCode.Trim();

            }

            drDepAirport["CreatedBy"] = lblUser.Text;
            drDepAirport["CreatedOn"] = DateTime.Now.ToString();
            dtDepAIrport.Rows.Add(drDepAirport);

            ViewState["dtDepAirport"] = dtDepAIrport;
            gvDepAirport.DataSource = dtDepAIrport;
            gvDepAirport.DataBind();

            if (gvDepAirport.Rows.Count > 0)
            {
                string strAirportCode = string.Empty;
                string strAirportName = string.Empty;

                string strStateCode = string.Empty;
                string strStateName = string.Empty;

                string strCountryCode = string.Empty;
                string strCountryName = string.Empty;

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strAirportName += lblAirport.Text + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Split(',');

                            if (strSplit.Length > 1)
                            {
                                strStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strCountryCode += strSplit[1].ToString().Trim() + "/";
                                strCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }



                divDepLan.Visible = true;

                txtDepDisLandPage.Text = (strCountryName + strStateCode + strAirportName).TrimEnd('/');
            }
            else
            {
                divDepLan.Visible = false;
            }
            txtMod_DepCity.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;
            txtModDepState.Text = string.Empty;
            //  ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            ddlDepSetupBy_SelectedIndexChanged(sender, e);

            if (gvDepAirport.Rows.Count > 1)
            {
                btnCreateGroup.Visible = true;
            }
            else
            {
                btnCreateGroup.Visible = false;
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnRouteAddArr_Click(object sender, EventArgs e)
    {
        string strAirport = string.Empty;
        string strCode = string.Empty;
        DataRow drArrAirport;
        DataTable dtArrAIrport = new DataTable();
        try
        {
            if (ViewState["dtArrAirport"] != null)
            {
                dtArrAIrport = (DataTable)ViewState["dtArrAirport"];
            }
            else
            {
                dtArrAIrport.Columns.Clear();
                dtArrAIrport.Columns.Add("SetupBy");
                dtArrAIrport.Columns.Add("Airport");
                dtArrAIrport.Columns.Add("StateCode");
                dtArrAIrport.Columns.Add("CountryCode");
                dtArrAIrport.Columns.Add("AirportCode");
                dtArrAIrport.Columns.Add("CreatedBy");
                dtArrAIrport.Columns.Add("CreatedOn");

            }
            drArrAirport = dtArrAIrport.NewRow();

            if (ddlArrSetupBy.SelectedValue == "CT")
            {
                drArrAirport["SetupBy"] = "Airport";
                drArrAirport["Airport"] = txtArrAirport.Text;
                if (txtArrAirport.Text.Contains("-"))
                {
                    string strstartcity2 = txtArrAirport.Text.Replace(" - ", "-");
                    String[] strliststart = strstartcity2.Split('-');
                    strCode = strliststart[1];
                }

                drArrAirport["AirportCode"] = strCode.Trim();
            }
            else if (ddlArrSetupBy.SelectedValue == "CO")
            {
                drArrAirport["SetupBy"] = "Country";
                drArrAirport["Airport"] = txtArrCountry.Text;
                if (txtArrCountry.Text.Contains(','))
                {
                    string[] strSplit = txtArrCountry.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        strCode = strSplit[1].ToString().Trim();
                    }
                }

                drArrAirport["CountryCode"] = strCode.Trim();
            }
            else if (ddlArrSetupBy.SelectedValue == "ST")
            {
                drArrAirport["SetupBy"] = "State";
                drArrAirport["Airport"] = txtArrState.Text;
                if (txtArrState.Text.Length > 0)
                {
                    string[] strSplit = txtArrState.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                        if (strSTCode.Length > 1)
                        {
                            strCode = strSTCode[0].ToString().Trim();
                        }
                    }
                }
                drArrAirport["StateCode"] = strCode.Trim();
            }

            drArrAirport["CreatedBy"] = lblUser.Text;
            drArrAirport["CreatedOn"] = DateTime.Now.ToString();
            dtArrAIrport.Rows.Add(drArrAirport);

            ViewState["dtArrAirport"] = dtArrAIrport;
            gvArrivalAirport.DataSource = dtArrAIrport;
            gvArrivalAirport.DataBind();

            if (gvArrivalAirport.Rows.Count > 0)
            {
                string strEndAirportCode = string.Empty;
                string strEndAirportName = string.Empty;

                string strEndStateCode = string.Empty;
                string strEndStateName = string.Empty;

                string strEndCountryCode = string.Empty;
                string strEndCountryName = string.Empty;
                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAirport = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");

                    if (lblSetupBy.Text.Trim() == "Airport")
                    {
                        strEndAirportName += lblAirport.Text.Trim() + "/";
                    }
                    if (lblSetupBy.Text.Trim() == "State")
                    {
                        string strState = lblAirport.Text;
                        if (strState.Contains(','))
                        {
                            string[] strSplit = strState.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndStateName += strSplit[0].ToString().Trim() + "/";

                                string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                                if (strSTCode.Length > 1)
                                {
                                    strEndStateCode += strSTCode[0].ToString().Trim() + "/";
                                }
                            }
                        }
                    }
                    if (lblSetupBy.Text.Trim() == "Country")
                    {
                        string strCountry = lblAirport.Text;
                        if (strCountry.Contains(','))
                        {
                            string[] strSplit = strCountry.Split(',');
                            if (strSplit.Length > 1)
                            {
                                strEndCountryCode += strSplit[1].ToString().Trim() + "/";
                                strEndCountryName += strSplit[0].ToString().Trim() + "/";
                            }
                        }
                    }
                }
                divArrLan.Visible = true;
                txtArrDisLandPage.Text = (strEndCountryName + strEndStateCode + strEndAirportName).TrimEnd('/');
            }
            else
            {
                divArrLan.Visible = false;
            }

            txtArrAirport.Text = string.Empty;
            txtArrCountry.Text = string.Empty;
            txtArrState.Text = string.Empty;

            // ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
            ddlArrSetupBy_SelectedIndexChanged(sender, e);

            if (gvArrivalAirport.Rows.Count > 1)
            {
                btnArrGroup.Visible = true;
            }
            else
            {
                btnArrGroup.Visible = false;
            }




        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void ddlDepSetupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                txtMod_DepCity.Visible = true;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = false;
                btnCreateGroup.Visible = true;
                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "ST")
            {
                txtMod_DepCity.Visible = false;
                txtModDepState.Visible = true;
                txtModDepCountry.Visible = false;
                btnCreateGroup.Visible = true;
                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "CO")
            {
                txtMod_DepCity.Visible = false;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = true;
                btnCreateGroup.Visible = true;
                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "GP")
            {
                BindGroup();
                mpeGroup.Show();
                btnCreateGroup.Visible = false;
                divDepart.Visible = true;
                divArrival.Visible = false;
                btnAddDeparture.Visible = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void ddlArrSetupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlArrSetupBy.SelectedValue == "CT")
            {
                txtArrAirport.Visible = true;
                txtArrState.Visible = false;
                txtArrCountry.Visible = false;
                btnArrGroup.Visible = true;
                btnAddArrival.Visible = true;
            }
            else if (ddlArrSetupBy.SelectedValue == "ST")
            {
                txtArrAirport.Visible = false;
                txtArrState.Visible = true;
                txtArrCountry.Visible = false;
                btnArrGroup.Visible = true;
                btnAddArrival.Visible = true;
            }
            else if (ddlArrSetupBy.SelectedValue == "CO")
            {
                txtArrAirport.Visible = false;
                txtArrState.Visible = false;
                txtArrCountry.Visible = true;
                btnArrGroup.Visible = true;
                btnAddArrival.Visible = true;
            }
            else if (ddlArrSetupBy.SelectedValue == "GP")
            {
                BindGroup();
                mpeGroup.Show();
                divDepart.Visible = false;
                divArrival.Visible = true;
                btnArrGroup.Visible = false;
                btnAddArrival.Visible = false;

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnDepCreateGroup(object sender, EventArgs e)
    {
        try
        {
            DataTable dtDuplicateCheck = objMember.GroupExist(txtDepDisLandPage.Text.Trim());

            if (dtDuplicateCheck.Rows.Count > 0)
            {
                lblalert.Text = "Group Name [ " + txtDepDisLandPage.Text.Trim() + " ] already exist";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                return;
            }


            string strRtVal = objMember.SaveDepartureGroup("I", "0", txtDepDisLandPage.Text, "Dep", Request.Cookies["JETRIPSADMIN"]["UserId"].ToString());

            if (strRtVal.ToString() != "")
            {
                string strModFlag = string.Empty;
                if (lblgroup.Text == "Modify")
                {
                    strModFlag = "MF";
                }
                else
                {
                    strModFlag = "ET";
                }

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAir = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");
                    Label lblCreatedBy = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedBy");
                    Label lblCreatedOn = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedOn");
                    Label lblStateCode = (Label)gvDepAirport.Rows[i].FindControl("lblStateCode");
                    Label lblCountryCode = (Label)gvDepAirport.Rows[i].FindControl("lblCountryCode");
                    Label lblAirportCode = (Label)gvDepAirport.Rows[i].FindControl("lblAirportCode");

                    objMember.SaveAirportChild("I", strRtVal.ToString().Trim(), strModFlag, "Dep", lblSetupBy.Text, lblAir.Text.Replace("'", "''"), lblCreatedBy.Text,
                        lblStateCode.Text, lblCountryCode.Text, lblAirportCode.Text);

                }
            }
            lblalert.Text = "Group [ " + txtDepDisLandPage.Text.Trim() + " ] created successfully";
            mpealert.Show();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void btnArrCreateGroup(object sender, EventArgs e)
    {
        try
        {
            DataTable dtDuplicateCheck = objMember.GroupExist(txtArrDisLandPage.Text.Trim());

            if (dtDuplicateCheck.Rows.Count > 0)
            {
                lblalert.Text = "Group Name [ " + txtArrDisLandPage.Text.Trim() + " ] already exist";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);
                return;
            }


            string strRtVal = objMember.SaveDepartureGroup("I", "0", txtDepDisLandPage.Text, "Arr", Request.Cookies["JETRIPSADMIN"]["UserId"].ToString());

            if (strRtVal.ToString() != "")
            {
                string strModFlag = string.Empty;
                if (lblgroup.Text == "Modify")
                {
                    strModFlag = "MF";
                }
                else
                {
                    strModFlag = "ET";
                }

                for (int i = 0; i < gvArrivalAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAir = (Label)gvArrivalAirport.Rows[i].FindControl("lblStartCity");
                    Label lblCreatedBy = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedBy");
                    Label lblCreatedOn = (Label)gvArrivalAirport.Rows[i].FindControl("lblCreatedOn");

                    Label lblStateCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblStateCode");
                    Label lblCountryCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblCountryCode");
                    Label lblAirportCode = (Label)gvArrivalAirport.Rows[i].FindControl("lblAirportCode");

                    objMember.SaveAirportChild("I", strRtVal.ToString().Trim(), strModFlag, "Arr", lblSetupBy.Text, lblAir.Text.Replace("'", "''"), lblCreatedBy.Text,
                       lblStateCode.Text, lblCountryCode.Text, lblAirportCode.Text);
                }

                lblalert.Text = "Group [ " + txtDepDisLandPage.Text.Trim() + " ] created successfully";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:Manual();", true);

            }
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvGroup.SelectedRow.FindControl("lblAirportId");

            Label lblGroupName = (Label)gvGroup.SelectedRow.FindControl("lblGroupName");

            DataTable dtGetGroupAirport = objMember.BindGroupChildData(lblrowid.Text);

            if (dtGetGroupAirport.Rows.Count > 0)
            {
                gvDepAirport.DataSource = dtGetGroupAirport;
                gvDepAirport.DataBind();
                ViewState["dtDepAirport"] = dtGetGroupAirport;
                divDepLan.Visible = true;
            }
            else
            {
                gvDepAirport.DataSource = null;
                gvDepAirport.DataBind();
                divDepLan.Visible = false;
            }

            txtDepDisLandPage.Text = lblGroupName.Text;
            ddlDepSetupBy.ClearSelection();
            ddlDepSetupBy_SelectedIndexChanged(sender, e);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void gvGroupArrival_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label lblrowid = (Label)gvGroupArrival.SelectedRow.FindControl("lblAirportId");

            Label lblGroupName = (Label)gvGroupArrival.SelectedRow.FindControl("lblGroupName");

            DataTable dtGetGroupAirport = objMember.BindGroupChildData(lblrowid.Text);

            if (dtGetGroupAirport.Rows.Count > 0)
            {
                gvArrivalAirport.DataSource = dtGetGroupAirport;
                gvArrivalAirport.DataBind();
                ViewState["dtArrAirport"] = dtGetGroupAirport;
                divArrLan.Visible = true;
            }
            else
            {
                gvArrivalAirport.DataSource = null;
                gvArrivalAirport.DataBind();
                divArrLan.Visible = false;
            }


            txtArrDisLandPage.Text = lblGroupName.Text;
            ddlArrSetupBy.ClearSelection();
            ddlArrSetupBy_SelectedIndexChanged(sender, e);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    public void CalculateLivePrice()
    {
        DataTable dtOneWayTrip = objMember.Get_FutureData();
        if (dtOneWayTrip.Rows.Count > 0)
        {
            for (int i = 0; i < dtOneWayTrip.Rows.Count; i++)
            {
                decimal daystoflight = 0;
                decimal L10_minPrice = 0;
                decimal M10_maxPrice = 0;
                decimal price = 0;

                string tailNo = dtOneWayTrip.Rows[i]["TailNo"].ToString();
                string noOfSeats = dtOneWayTrip.Rows[i]["NoOFSeats"].ToString();
                var flyingDate = dtOneWayTrip.Rows[i]["DDate"].ToString();
                var flyingtoDate = dtOneWayTrip.Rows[i]["ADate"].ToString();
                var strtTime = dtOneWayTrip.Rows[i]["DTime"].ToString();

                var endTime = dtOneWayTrip.Rows[i]["ATime"].ToString();
                var flyingHours = dtOneWayTrip.Rows[i]["FH"].ToString();
                string startCity = dtOneWayTrip.Rows[i]["StartCity"].ToString();
                string endCity = dtOneWayTrip.Rows[i]["EndCity"].ToString();
                string startAirport = dtOneWayTrip.Rows[i]["Start"].ToString();
                string endAirport = dtOneWayTrip.Rows[i]["End"].ToString();

                var HourlyRate = dtOneWayTrip.Rows[i]["HourlyRate"].ToString();
                var fixedCeiling = dtOneWayTrip.Rows[i]["FixedCeiling"].ToString();
                var fixedFloor = dtOneWayTrip.Rows[i]["FixedFloor"].ToString();
                var D4_MinTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MinTimeBD"].ToString());
                var D5_MaxTimeBD = Convert.ToDecimal(dtOneWayTrip.Rows[i]["MaxTimeBD"].ToString());
                var MinFLHSellable = dtOneWayTrip.Rows[i]["MinFLHSellable"].ToString();
                var applytax = dtOneWayTrip.Rows[i]["applytax"].ToString();
                var tripno = dtOneWayTrip.Rows[i]["tripno"].ToString();
                var enabletrip = dtOneWayTrip.Rows[i]["enabletrip"].ToString();
                var showtrip = dtOneWayTrip.Rows[i]["Active"].ToString();
                var TailFlag = dtOneWayTrip.Rows[i]["TailFlag"].ToString();
                var createdby = dtOneWayTrip.Rows[i]["createdby"].ToString();
                var createdon = dtOneWayTrip.Rows[i]["createdon"].ToString();
                var tripflag = dtOneWayTrip.Rows[i]["tripflag"].ToString();
                var groupid = dtOneWayTrip.Rows[i]["groupid"].ToString();
                var FosModifiedFlag = dtOneWayTrip.Rows[i]["ModifyFlag"].ToString();
                var leg = dtOneWayTrip.Rows[i]["leg"].ToString();
                var splitype = dtOneWayTrip.Rows[i]["Splittype"].ToString();
                var NewStartAirport = dtOneWayTrip.Rows[i]["NewStart"].ToString();
                var NewEndAirport = dtOneWayTrip.Rows[i]["NewEnd"].ToString();

                if (dtOneWayTrip.Rows[i]["HourlyRate"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedCeiling"].ToString() != "0.00" && dtOneWayTrip.Rows[i]["FixedFloor"].ToString() != "0.00")
                {
                    var dtFlyingDays = Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00";

                    var K10_totalDays = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00").Subtract(DateTime.Now);

                    DateTime startTime = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + strtTime + ":00");
                    DateTime endtime1 = Convert.ToDateTime(Convert.ToDateTime(flyingDate).Date.ToString("yyyy-MM-dd") + ' ' + endTime + ":00");
                    var duration = endtime1.Subtract(startTime);
                    var _totalDaysK10 = K10_totalDays.TotalDays.ToString("0.00");
                    decimal totalhrs = 0;
                    if (flyingHours != "")
                    {
                        totalhrs = Convert.ToDecimal(flyingHours);
                    }

                    long durationTicks = Math.Abs(duration.Ticks / TimeSpan.TicksPerMillisecond);
                    long hours = durationTicks / (1000 * 60 * 60);
                    long minutes = (durationTicks - (hours * 60 * 60 * 1000)) / (1000 * 60);

                    string CBPF = string.Empty;
                    string exactFlyinghrs = hours.ToString("00") + ":" + minutes.ToString("00");

                    L10_minPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs));

                    if (L10_minPrice < Convert.ToDecimal(fixedFloor))
                    {
                        L10_minPrice = Convert.ToDecimal(fixedFloor);
                        CBPF = "FIXEDCEILING";
                    }

                    M10_maxPrice = (Convert.ToDecimal(HourlyRate) * Convert.ToDecimal(totalhrs)) + Convert.ToDecimal(fixedCeiling);


                    var finalPricevalidation2 = (decimal)0;

                    var finalPrice = Convert.ToInt32(((1 - (D5_MaxTimeBD - Convert.ToDecimal(_totalDaysK10)) / (D5_MaxTimeBD - Convert.ToDecimal(D4_MinTimeBD))) * (M10_maxPrice - L10_minPrice) + L10_minPrice) / 25) * 25;

                    int mul1 = 0;
                    if (Convert.ToDecimal(_totalDaysK10) < D4_MinTimeBD)
                    {
                        mul1 = 0;
                    }
                    else
                        mul1 = 1;

                    string mul2 = "0";
                    if (Convert.ToDecimal(Convert.ToDecimal(totalhrs)) >= Convert.ToDecimal(MinFLHSellable))
                    {
                        mul2 = "1";
                    }
                    else
                        mul2 = "ERROR";



                    if (mul2 != "ERROR")
                    {
                        finalPricevalidation2 = Convert.ToDecimal(finalPrice) * Convert.ToDecimal(mul2) * Convert.ToDecimal(mul1);
                    }
                    string Specialflag = string.Empty;
                    DataTable dtspecialpricing = objMember.LivePricing_SEL(tailNo, tripno, startAirport, endAirport, groupid, flyingDate).Tables[1];
                    DataRow[] drspecial = dtspecialpricing.Select("#" + flyingDate + "# >= StartDate AND EndDate >= #" + flyingDate + "#" + "and startAirport='" + startAirport + "' and endairport ='" + endAirport + "'");
                    DataTable dtselection = new DataTable();
                    if (drspecial.Length > 0)
                    {
                        dtselection = drspecial.CopyToDataTable();
                        if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "H")
                        {
                            finalPricevalidation2 = Convert.ToDecimal(flyingHours) * Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                            Specialflag = "(Special Price)";
                        }
                        else if (dtselection.Rows[0]["SpecialPriceDescription"].ToString() == "D")
                        {
                            finalPricevalidation2 = Convert.ToDecimal(dtselection.Rows[0]["pricedetails"].ToString());
                            Specialflag = "(Special Price)";
                        }
                    }

                    if (FosModifiedFlag.ToString() == "ET")
                    {
                        DataTable dtManualPrice = objMember.LivePricing_SEL(tailNo, tripno, startAirport, endAirport, groupid, flyingDate).Tables[2];

                        if (dtManualPrice.Rows.Count > 0)
                        {
                            if (Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString()) > 3500)
                            {
                                finalPricevalidation2 = Convert.ToDecimal(dtManualPrice.Rows[0]["Mprice"].ToString());
                            }

                        }
                    }
                    if (FosModifiedFlag.ToString() == "MF")
                    {
                        DataTable dtEditPrice = objMember.LivePricing_SEL(tailNo, tripno, startAirport, endAirport, groupid, flyingDate).Tables[3];

                        if (dtEditPrice.Rows.Count > 0)
                        {
                            if (Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString()) > 3500)
                            {
                                finalPricevalidation2 = Convert.ToDecimal(dtEditPrice.Rows[0]["Mod_Price"].ToString());
                            }
                        }
                    }

                    string s = DateTime.Now.ToString("HH:mm:ss.fff");
                    TimeSpan ts = TimeSpan.Parse(s);
                    string time = ts.ToString();
                    string Date = DateTime.Now.Date.ToString();
                    if (Specialflag == "(Special Price)")
                    {
                        DataTable dtspecialCheck = objMember.LivePricing_SEL(tailNo, tripno, startAirport, endAirport, groupid, flyingDate).Tables[4];

                        if (dtspecialCheck.Rows.Count > 0)
                        {

                        }
                        else
                        {
                            objMember.TailNo = tailNo;
                            objMember.FlyingDate = flyingDate;
                            objMember.FlyingToDate = flyingtoDate;
                            objMember.StartTime = strtTime;
                            objMember.EndTime = endTime;
                            objMember.StartAirport = startAirport;
                            objMember.StartCity = startCity.Replace("'", "''");
                            objMember.EndAirport = endAirport;
                            objMember.EndCity = endCity.Replace("'", "''");
                            objMember.NoOfSeats = noOfSeats;
                            objMember.totalHoours = totalhrs.ToString();
                            objMember.CalculatelyHours = totalhrs.ToString();
                            objMember.HourlyRate = HourlyRate;
                            objMember.FixedFloor = fixedFloor;
                            objMember.FixedCeeling = fixedCeiling;
                            objMember.MinTime = D4_MinTimeBD;
                            objMember.MaxTime = D5_MaxTimeBD;
                            objMember.MinSeallable = MinFLHSellable;
                            objMember.TotalDays = _totalDaysK10;
                            objMember.LMinPrice = L10_minPrice;
                            objMember.LMaxPrice = M10_maxPrice;
                            objMember.FinalPrice = finalPricevalidation2;
                            objMember.Time = time;
                            objMember.Date = Date;
                            objMember.FLAG = "FINAL";
                            objMember.CBPFFlag = CBPF;
                            objMember.BookingFlag = "0";
                            objMember.ApplyTax = applytax;
                            objMember.TripNo = tripno;
                            objMember.EnableTrip = enabletrip;
                            objMember.ACtive = showtrip;
                            objMember.PriceFlag = Specialflag;
                            objMember.TailFlag = TailFlag;
                            objMember.CreatedBy = createdby;
                            objMember.CreatedOn = createdon;
                            objMember.TripFlag = tripflag;
                            objMember.GroupId = groupid;
                            objMember.Mod_Flag = FosModifiedFlag;
                            objMember.Leg = leg;
                            objMember.SplitType = splitype;
                            objMember.NewStartAirport = NewStartAirport;
                            objMember.NewEndAirport = NewEndAirport;

                            string retVal = objMember.InsertLivePricingCalculation(objMember);
                        }
                    }
                    else
                    {
                        objMember.TailNo = tailNo;
                        objMember.FlyingDate = flyingDate;
                        objMember.FlyingToDate = flyingtoDate;
                        objMember.StartTime = strtTime;
                        objMember.EndTime = endTime;
                        objMember.StartAirport = startAirport;
                        objMember.StartCity = startCity.Replace("'", "''");
                        objMember.EndAirport = endAirport;
                        objMember.EndCity = endCity.Replace("'", "''");
                        objMember.NoOfSeats = noOfSeats;
                        objMember.totalHoours = totalhrs.ToString();
                        objMember.CalculatelyHours = totalhrs.ToString();
                        objMember.HourlyRate = HourlyRate;
                        objMember.FixedFloor = fixedFloor;
                        objMember.FixedCeeling = fixedCeiling;
                        objMember.MinTime = D4_MinTimeBD;
                        objMember.MaxTime = D5_MaxTimeBD;
                        objMember.MinSeallable = MinFLHSellable;
                        objMember.TotalDays = _totalDaysK10;
                        objMember.LMinPrice = L10_minPrice;
                        objMember.LMaxPrice = M10_maxPrice;
                        objMember.FinalPrice = finalPricevalidation2;
                        objMember.Time = time;
                        objMember.Date = Date;
                        objMember.FLAG = "FINAL";
                        objMember.CBPFFlag = CBPF;
                        objMember.BookingFlag = "0";
                        objMember.ApplyTax = applytax;
                        objMember.TripNo = tripno;
                        objMember.EnableTrip = enabletrip;
                        objMember.ACtive = showtrip;
                        objMember.PriceFlag = Specialflag;
                        objMember.TailFlag = TailFlag;
                        objMember.CreatedBy = createdby;
                        objMember.CreatedOn = createdon;
                        objMember.TripFlag = tripflag;
                        objMember.GroupId = groupid;
                        objMember.Mod_Flag = FosModifiedFlag;
                        objMember.Leg = leg;
                        objMember.SplitType = splitype;
                        objMember.NewStartAirport = NewStartAirport;
                        objMember.NewEndAirport = NewEndAirport;

                        string retVal = objMember.InsertLivePricingCalculation(objMember);
                    }
                }
            }

        }
    }

    public void BindEmptyLegs()
    {
        string strMangeType = string.Empty;
        string strStartDate = DateTime.Now.ToString("yyyy-MM-dd");
        string strEndDate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd");

        if (rblStatus.SelectedValue.Trim() == "Active")
        {
            strMangeType = "Active";
            lblSubScren.Text = "Add a Manual Trip";
            btnAdd.Visible = true;
        }
        else if (rblStatus.SelectedValue.Trim() == "Booked Trips")
        {
            strMangeType = "Booked";
            lblSubScren.Text = "Modify a FOS Trip";
            btnAdd.Visible = false;
        }
        else if (rblStatus.SelectedValue.Trim() == "Group")
        {
            strMangeType = "Split";
            lblSubScren.Text = "Add a Manual Leg";
            btnAdd.Visible = false;
        }

        DataTable dtOneWayTrip = objMember.BindEmptyLegs_SP(strMangeType, strStartDate, strEndDate, "1", "25");
        dtOneWayTrip.Columns.Add("leg");


        if (dtOneWayTrip.Rows.Count > 0)
        {
            gvTailDetails.DataSource = dtOneWayTrip;
            gvTailDetails.DataBind();
        }
        else
        {
            gvTailDetails.DataSource = null;
            gvTailDetails.DataBind();
        }
        gvTailDetails.Visible = true;
    }

    private void Loadcountry()
    {

        DataTable dt = objMember.CountryList();
        cblcountry.DataSource = dt;
        cblcountry.DataTextField = "Country_Name";
        cblcountry.DataValueField = "Country_Name";
        cblcountry.DataBind();

    }

    private void BindGroup()
    {
        DataTable dtAirport = objMember.BindGroupData();
        if (dtAirport.Rows.Count > 0)
        {
            gvGroup.DataSource = dtAirport;
            gvGroup.DataBind();

            for (int i = 0; i < gvGroup.Rows.Count; i++)
            {
                Label lblRowId = (Label)gvGroup.Rows[i].FindControl("lblAirportId");

                DataList dsScreen = (DataList)gvGroup.Rows[i].FindControl("rptAirports");

                DataTable dtChild = objMember.BindGroupChildData(lblRowId.Text);

                if (dtChild.Rows.Count > 0)
                {
                    dsScreen.DataSource = dtChild;
                    dsScreen.DataBind();
                }
                else
                {
                    dsScreen.DataSource = null;
                    dsScreen.DataBind();
                }

            }

            gvGroupArrival.DataSource = dtAirport;
            gvGroupArrival.DataBind();

            for (int i = 0; i < gvGroupArrival.Rows.Count; i++)
            {
                Label lblRowId = (Label)gvGroupArrival.Rows[i].FindControl("lblAirportId");

                DataList dsScreen = (DataList)gvGroupArrival.Rows[i].FindControl("rptAirports");

                DataTable dtChild = objMember.BindGroupChildData(lblRowId.Text);

                if (dtChild.Rows.Count > 0)
                {
                    dsScreen.DataSource = dtChild;
                    dsScreen.DataBind();
                }
                else
                {
                    dsScreen.DataSource = null;
                    dsScreen.DataBind();
                }

            }
        }
        else
        {
            gvGroup.DataSource = null;
            gvGroup.DataBind();

            gvGroupArrival.DataSource = null;
            gvGroupArrival.DataBind();
        }
    }

    void clearform()
    {
        hdfSCID.Value = "0";
        txtTailNo.Value = "";
        txtStartCity.Text = "";
        txtStartTime.Value = "";
        txtEndCity.Text = "";
        txtEndTime.Value = "";
        txtFlyHours1.Value = "";
        txtAvaFrom.Text = "";
        txtAvaTo.Text = "";
        txtNoSeats.Value = "";
        txttrip.Value = "";
        txtMPrice.Value = "";
    }

    public DataTable AirRouteCalculation(string StartAirport, string strEndAirport, string strSpeed)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
        string strRoot = StartAirport + "-" + strEndAirport;

        string WEBSERVICE_URL = "https://greatcirclemapper.p.rapidapi.com/airports/route/";
        WEBSERVICE_URL = WEBSERVICE_URL + strRoot + @"/" + strSpeed;

        var jsonResponse = "";

        var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
        if (webRequest != null)
        {
            webRequest.Method = "GET";
            webRequest.Timeout = 20000;
            webRequest.ContentType = "text/html;charset=UTF-8"; //"application/json";

            webRequest.Headers.Add("X-RapidAPI-Host", "greatcirclemapper.p.rapidapi.com");
            webRequest.Headers.Add("X-RapidAPI-Key", SecretsBLL.RapidAPIKey.ToString());
            webRequest.Headers.Add("vary", "Accept-Encoding");

            //webRequest.Headers.Add("content-type", "text/html;charset=UTF-8");

            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                {
                    jsonResponse = sr.ReadToEnd();
                }
            }
        }
        LegDetails8 leg = new LegDetails8();
        List<LegDetails8> RouteListLeg = new List<LegDetails8>();

        JavaScriptSerializer js = new JavaScriptSerializer();
        dynamic d = js.Deserialize<dynamic>(jsonResponse.ToString());

        dynamic[] arrlegs = d["legs"];

        for (int i = 0; i < arrlegs.Length; i++)
        {
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(arrlegs[i]);

            dynamic dInnerLeg = js.Deserialize<dynamic>(json);

            leg = new LegDetails8();

            leg.legNo = Convert.ToString(i + 1);
            leg.startICAO = dInnerLeg["origin"]["ident"];
            leg.endICAO = dInnerLeg["destination"]["ident"];
            leg.distance_km = Convert.ToString(arrlegs[i]["distance_km"]);
            leg.distance_nm = Convert.ToString(arrlegs[i]["distance_nm"]);
            leg.flight_time_min = Convert.ToString(arrlegs[i]["flight_time_min"]);
            leg.heading_deg = Convert.ToString(arrlegs[i]["heading_deg"]);
            leg.heading_compass = Convert.ToString(arrlegs[i]["heading_compass"]);

            RouteListLeg.Add(leg);
        }

        DataTable dtResul = ToDataTable(RouteListLeg);

        return dtResul;

    }
    public DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }

        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }

        //put a breakpoint here and check datatable
        return dataTable;

    }
    public DataTable RapidAPIDatatable()
    {
        DataTable dtRapid = new DataTable();
        dtRapid.Columns.Add("legNo");
        dtRapid.Columns.Add("startICAO");
        dtRapid.Columns.Add("endICAO");
        dtRapid.Columns.Add("distance_km");
        dtRapid.Columns.Add("distance_nm");
        dtRapid.Columns.Add("flight_time_min");
        dtRapid.Columns.Add("heading_deg");
        dtRapid.Columns.Add("heading_compass");

        DataRow row = dtRapid.NewRow();
        row["legNo"] = 1;
        row["startICAO"] = "KVNY";
        row["endICAO"] = "KSNA";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        row = dtRapid.NewRow();
        row["legNo"] = 2;
        row["startICAO"] = "KSNA";
        row["endICAO"] = "KVNY";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        return dtRapid;


    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText)
    {
        return AutoFillProducts(prefixText);

    }

    private static List<string> AutoFillProducts(string prefixText)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {
                com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;


            }

        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCity(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepState(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select S.State_Name+' , '+S.State_Code+ ' - '+C.Country_Name as State from JETEDGE_FUEL..State S Left Join JETEDGE_FUEL..Country C on C.Row_ID=S.Country_ID where " + "State_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["State"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCountry(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct Country_Name+ ' , '+Country_Code as Country from JETEDGE_FUEL..Country where " + "Country_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["Country"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    public class LegDetails8
    {
        public string legNo { get; set; }
        public string startICAO { get; set; }
        public string endICAO { get; set; }
        public string distance_km { get; set; }
        public string distance_nm { get; set; }
        public string flight_time_min { get; set; }
        public string heading_deg { get; set; }
        public string heading_compass { get; set; }
    }
}

