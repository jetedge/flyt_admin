﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Views_AirportGroup : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.CreateAdminBLL objCreateAdminBLL = new BusinessLayer.CreateAdminBLL();

    string ICAO = string.Empty;
    string StateCode = string.Empty;
    string CountryCode = string.Empty;
    string City = string.Empty;
    string IATA = string.Empty;
    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Airport Group");
            }


            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Airport Group";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;

            BindGroup();
        }
    }

    #endregion

    protected void gvGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ViewState["dtDepAirport"] = null;
            gvDepAirport.DataSource = null;
            gvDepAirport.DataBind();
            txtGname.Text = string.Empty;
            txtMod_DepCity.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;
            txtModDepState.Text = string.Empty;
            ddlDepSetupBy.ClearSelection();

            Label lblrowid = (Label)gvGroupArrival.SelectedRow.FindControl("lblAirportId");

            hdfPSRID.Value = lblrowid.Text;

            Label lblGroupName = (Label)gvGroupArrival.SelectedRow.FindControl("lblGroupName");

            Label lblActiveFlag = (Label)gvGroupArrival.SelectedRow.FindControl("lblActive");

            rblActive.SelectedIndex = rblActive.Items.IndexOf(rblActive.Items.FindByText(lblActiveFlag.Text.Trim()));

            Label lblCreatedBy = (Label)gvGroupArrival.SelectedRow.FindControl("lblCreatedBy");

            Label lblCreatedOn = (Label)gvGroupArrival.SelectedRow.FindControl("lblCreatedOn");

            if (lblCreatedBy.Text != "" && lblCreatedOn.Text != "")
            {
                lblLMby.Text = lblCreatedBy.Text + " ( on ) " + lblCreatedOn.Text;
                divLMBy.Visible = true;
            }
            else
            {
                divLMBy.Visible = false;
            }



            DataTable dtGetGroupAirport = BindGroupChildData(lblrowid.Text);

            if (dtGetGroupAirport.Rows.Count > 0)
            {
                gvDepAirport.DataSource = dtGetGroupAirport;
                gvDepAirport.DataBind();
                ViewState["dtDepAirport"] = dtGetGroupAirport;

            }
            else
            {
                gvDepAirport.DataSource = null;
                gvDepAirport.DataBind();

            }
            txtGname.Text = lblGroupName.Text;
            ddlDepSetupBy.ClearSelection();
            ddlDepSetupBy_SelectedIndexChanged(sender, e);
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            btnSave.Text = "Update";
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void btnAddonClick(object sender, EventArgs e)
    {
        try
        {
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnAdd.Visible = false;
            btnView.Visible = true;
            btnSave.Visible = true;
            btnSave.Text = "Save";
            ViewState["dtDepAirport"] = null;
            gvDepAirport.DataSource = null;
            gvDepAirport.DataBind();
            txtGname.Text = string.Empty;
            txtMod_DepCity.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;
            txtModDepState.Text = string.Empty;
            ddlDepSetupBy.ClearSelection();
            rblActive.SelectedValue = "Y";
            hdfPSRID.Value = "0";
            divLMBy.Visible = false;
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void btnViewonClick(object sender, EventArgs e)
    {
        try
        {
            tblForm.Visible = false;
            tblGrid.Visible = true;
            btnAdd.Visible = true;
            btnView.Visible = false;
            btnSave.Visible = false;
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void btnDepCreateGroup(object sender, EventArgs e)
    {
        string strManageType = string.Empty;

        try
        {
            if (btnSave.Text == "Save")
            {
                strManageType = "I";

                hdfPSRID.Value = "0";

                DataTable dtDuplicateCheck = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT * FROM  AirportGroup  where GroupName ='" + txtGname.Text.Trim() + "'").Tables[0];

                if (dtDuplicateCheck.Rows.Count > 0)
                {
                    lblalert.Text = "Group Name [ " + txtGname.Text.Trim() + " ] already exist";
                    mpealert.Show();
                    return;
                }

                lblalert.Text = "Group [ " + txtGname.Text.Trim() + " ] saved successfully";
            }
            else
            {
                strManageType = "U";

                lblalert.Text = "Group [ " + txtGname.Text.Trim() + " ] updated successfully";

            }

            string strRtVal = SaveDepartureGroup(strManageType, hdfPSRID.Value, txtGname.Text, "Dep", Request.Cookies["JETRIPSADMIN"]["UserId"].ToString(),
                rblActive.SelectedValue.Trim());


            if (strRtVal.ToString() != "")
            {
                string strModFlag = string.Empty;

                strModFlag = "ET";

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "DELETE FROM AirportGroup_Child WHERE GroupId='" + strRtVal.ToString().Trim() + "'");

                for (int i = 0; i < gvDepAirport.Rows.Count; i++)
                {
                    Label lblSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblSetupBy");
                    Label lblAir = (Label)gvDepAirport.Rows[i].FindControl("lblStartCity");
                    Label lblCreatedBy = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedBy");
                    Label lblCreatedOn = (Label)gvDepAirport.Rows[i].FindControl("lblCreatedOn");
                    Label lblStateCode = (Label)gvDepAirport.Rows[i].FindControl("lblStateCode");
                    Label lblCountryCode = (Label)gvDepAirport.Rows[i].FindControl("lblCountryCode");
                    Label lblAirportCode = (Label)gvDepAirport.Rows[i].FindControl("lblAirportCode");
                    Label lblStateID = (Label)gvDepAirport.Rows[i].FindControl("lblStateid");
                    Label lblDepSetupBy = (Label)gvDepAirport.Rows[i].FindControl("lblDepSetupBy");

                    SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "INSERT INTO AirportGroup_Child (GroupId,ModifiedFlag,AirportFor,SetupBy,Airport,CreatedBy,CreatedOn,StateCode,CountryCode,AirportCode,StateId) VALUES ('" + strRtVal.ToString().Trim() + "','" + strModFlag + "','" + lblDepSetupBy.Text + "', '" + lblSetupBy.Text + "', '" + lblAir.Text.Replace("'", "''") + "','" + lblCreatedBy.Text + "','" + lblCreatedOn.Text + "','" + lblStateCode.Text + "','" + lblCountryCode.Text + "','" + lblAirportCode.Text + "','" + lblStateID.Text.Trim() + "')");
                }
            }

            mpealert.Show();
            BindGroup();
            btnViewonClick(sender, e);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void ddlDepSetupBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                txtMod_DepCity.Visible = true;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = false;

                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "ST")
            {
                txtMod_DepCity.Visible = false;
                txtModDepState.Visible = true;
                txtModDepCountry.Visible = false;

                btnAddDeparture.Visible = true;
            }
            else if (ddlDepSetupBy.SelectedValue == "CO")
            {
                txtMod_DepCity.Visible = false;
                txtModDepState.Visible = false;
                txtModDepCountry.Visible = true;
                btnAddDeparture.Visible = true;
            }

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void btnRouteAddDep_Click(object sender, EventArgs e)
    {
        string strAirport = string.Empty;
        string strCode = string.Empty;
        DataRow drDepAirport;
        DataTable dtDepAIrport = new DataTable();
        try
        {
            if (ViewState["dtDepAirport"] != null)
            {
                dtDepAIrport = (DataTable)ViewState["dtDepAirport"];
            }
            else
            {
                dtDepAIrport.Columns.Clear();
                dtDepAIrport.Columns.Add("SetupBy");
                dtDepAIrport.Columns.Add("Airport");
                dtDepAIrport.Columns.Add("StateCode");
                dtDepAIrport.Columns.Add("StateID");
                dtDepAIrport.Columns.Add("CountryCode");
                dtDepAIrport.Columns.Add("AirportCode");
                dtDepAIrport.Columns.Add("CreatedBy");
                dtDepAIrport.Columns.Add("CreatedOn");

                dtDepAIrport.Columns.Add("DepSetupBy");
                dtDepAIrport.Columns.Add("DepSetupByText");
            }

            drDepAirport = dtDepAIrport.NewRow();

            if (ddlDepSetupBy.SelectedValue == "CT")
            {
                AirportClass.AirportSplit(txtMod_DepCity.Text, out ICAO, out StateCode, out  CountryCode, out City, out IATA);


                drDepAirport["SetupBy"] = "Airport";
                drDepAirport["Airport"] = txtMod_DepCity.Text.Trim();
                strCode = ICAO;

                drDepAirport["AirportCode"] = strCode.Trim();

                //drDepAirport["SetupBy"] = "Airport";
                //drDepAirport["Airport"] = txtMod_DepCity.Text.Trim();

                //if (txtMod_DepCity.Text.Contains("-"))
                //{
                //    string strstartcity2 = txtMod_DepCity.Text.Replace(",", "-").Replace(" ", "");
                //    String[] strliststart = strstartcity2.Split('-');
                //    strCode = strliststart[0].ToString();
                //}

                //drDepAirport["AirportCode"] = strCode.Trim();

            }
            else if (ddlDepSetupBy.SelectedValue == "CO")
            {
                drDepAirport["SetupBy"] = "Country";
                drDepAirport["Airport"] = txtModDepCountry.Text.Trim();

                if (txtModDepCountry.Text.Contains(','))
                {
                    string[] strSplit = txtModDepCountry.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        strCode = strSplit[1].ToString().Trim();
                    }
                }

                drDepAirport["CountryCode"] = strCode.Trim();
            }
            else if (ddlDepSetupBy.SelectedValue == "ST")
            {
                drDepAirport["SetupBy"] = "State";
                drDepAirport["Airport"] = txtModDepState.Text.Trim();

                if (txtModDepState.Text.Length > 0)
                {
                    string[] strSplit = txtModDepState.Text.Split(',');

                    if (strSplit.Length > 1)
                    {
                        string[] strSTCode = strSplit[1].ToString().Trim().Replace(" ", "").Split('-');

                        if (strSTCode.Length > 1)
                        {
                            strCode = strSTCode[0].ToString().Trim();
                        }
                        DataTable dtStateID = genclass.GetDataTable("select Row_ID from JETEDGE_FUEL..State WHERE State_Name='" + strSplit[0].ToString().Trim() + "'");
                        if (dtStateID.Rows.Count > 0)
                        {
                            drDepAirport["StateID"] = dtStateID.Rows[0]["Row_ID"].ToString();
                        }
                    }
                }

                drDepAirport["StateCode"] = strCode.Trim();

            }

            drDepAirport["CreatedBy"] = lblUser.Text;
            drDepAirport["CreatedOn"] = DateTime.Now.ToString("MMM dd, yyyy hh:mm tt").ToUpper();

            drDepAirport["DepSetupBy"] = rblSetupBy.SelectedValue;
            drDepAirport["DepSetupByText"] = rblSetupBy.SelectedItem.Text;


            dtDepAIrport.Rows.Add(drDepAirport);

            ViewState["dtDepAirport"] = dtDepAIrport;
            gvDepAirport.DataSource = dtDepAIrport;
            gvDepAirport.DataBind();


            txtMod_DepCity.Text = string.Empty;
            txtModDepCountry.Text = string.Empty;
            txtModDepState.Text = string.Empty;

            ddlDepSetupBy_SelectedIndexChanged(sender, e);
        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }
    protected void gvDepAirport_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DataTable dt = ViewState["dtDepAirport"] as DataTable;
            dt.Rows[e.RowIndex].Delete();
            dt.AcceptChanges();

            ViewState["dtDepAirport"] = dt;
            gvDepAirport.EditIndex = -1;
            gvDepAirport.DataSource = ViewState["dtDepAirport"];
            gvDepAirport.DataBind();

        }
        catch (Exception ex)
        {
            int linenum = 0;
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
        }
    }

    protected void imbGroupDepDeleteOnclick(object sender, EventArgs e)
    {
        try
        {

            ImageButton lb = (ImageButton)sender;
            GridViewRow gvrow = (GridViewRow)lb.Parent.Parent;//.Parent.Parent;
            Label lblrowid = (Label)gvrow.FindControl("lblAirportId");
            lblrowDeleteId.Value = lblrowid.Text;

            mpeconfirm.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnConfirmDelete(object sender, EventArgs e)
    {
        try
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete from AirportGroup where Rowid='" + lblrowDeleteId.Value.Trim() + "'");
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "Delete from AirportGroup_Child where Groupid='" + lblrowDeleteId.Value.Trim() + "'");

            BindGroup();

            lblalert.Text = "Group deleted successfully";
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    private void BindGroup()
    {
        DataTable dtAirport = BindGroupData();
        if (dtAirport.Rows.Count > 0)
        {

            gvGroupArrival.DataSource = dtAirport;
            gvGroupArrival.DataBind();

            for (int i = 0; i < gvGroupArrival.Rows.Count; i++)
            {
                Label lblRowId = (Label)gvGroupArrival.Rows[i].FindControl("lblAirportId");

                DataList dsScreen = (DataList)gvGroupArrival.Rows[i].FindControl("rptAirports");
                DataTable dtChild = BindGroupChildData(lblRowId.Text);

                if (dtChild.Rows.Count > 0)
                {
                    dsScreen.DataSource = dtChild;
                    dsScreen.DataBind();
                }
                else
                {
                    dsScreen.DataSource = null;
                    dsScreen.DataBind();
                }

            }
        }
        else
        {
            gvGroupArrival.DataSource = null;
            gvGroupArrival.DataBind();
        }
    }
    public string SaveDepartureGroup(string strManageType, string strRowId, string strGroupName, string strFlag, string strCreatedBy,
        string strActive)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_INS]", false);

        saveCategoryParam[0].Value = strManageType;
        saveCategoryParam[1].Value = strRowId;
        saveCategoryParam[2].Value = strGroupName;
        saveCategoryParam[3].Value = strFlag;
        saveCategoryParam[4].Value = strCreatedBy;
        saveCategoryParam[6].Value = strActive;

        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_AirportGroup_INS]", saveCategoryParam).ToString();
        return obj.ToString();
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCity(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where  ZoomFlag='Yes' AND " + "city like  @Search + '%' or " + "icao like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    public DataTable BindGroupData()
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", false);
        GetLegDetails[0].Value = "S";
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", GetLegDetails).Tables[0];
    }

    public DataTable BindGroupChildData(string strGroupId)
    {
        SqlParameter[] GetLegDetails = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", false);
        GetLegDetails[0].Value = "GC";
        GetLegDetails[5].Value = strGroupId;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, "SP_AirportGroup_INS", GetLegDetails).Tables[0];
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText)
    {
        return AutoFillProducts(prefixText);

    }

    private static List<string> AutoFillProducts(string prefixText)
    {
        DataTable dtNew = new DataTable();
        DataTable dt = new DataTable();
        DataRow[] drNew = null;
        if (HttpContext.Current.Session["ICAOC"] == null)
        {
            dt = genclass.GetZoomAirports("YES");

            HttpContext.Current.Session["ICAOC"] = dt;
        }

        dt = (DataTable)HttpContext.Current.Session["ICAOC"];

        if (dt.Rows.Count > 0)
        {
            drNew = dt.Select("ICAONAME like '%' + '" + prefixText + "' + '%'");



            //if (prefixText.Length < 5)
            //{
            //    drNew = dt.Select("ICAO like '%' + '" + prefixText + "' + '%'");
            //}
            //else
            //{
            //    drNew = dt.Select("StateName like  '%' + '" + prefixText + "' + '%' OR City like  '%' +  '" + prefixText + "' + '%'");
            //}

            if (drNew.Length > 0)
            {
                dtNew = drNew.CopyToDataTable();
            }
        }
        List<string> sEmail = new List<string>();
        for (int i = 0; i < drNew.Length; i++)
        {
            sEmail.Add(drNew[i]["ICAOName"].ToString());
        }

        //foreach (DataRow row in dtNew.Rows)
        //{
        //    sEmail.Add(row[0].ToString());
        //}
        return sEmail;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepState(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select S.State_Name+' , '+S.State_Code+ ' - '+C.Country_Name as State from JETEDGE_FUEL..State S Left Join JETEDGE_FUEL..Country C on C.Row_ID=S.Country_ID where " + "State_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["State"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> DepCountry(string prefixText, int count)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;
            using (SqlCommand com = new SqlCommand())
            {

                com.CommandText = "select distinct Country_Name+ ' , '+Country_Code as Country from JETEDGE_FUEL..Country where " + "Country_Name like  @Search + '%'";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["Country"].ToString());
                    }
                }
                con.Close();
                return countryNames;
            }
        }
    }
}