﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;

public partial class Views_UserPreferenceReport : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.UserPreferenceBLL objMember = new BusinessLayer.UserPreferenceBLL();
    public int linenum = 0;
    public string MethodName = string.Empty;

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                // Put user code to initialize the page here
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Reports", "User Preferences Report");
                }

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Reports";
                lblSubHeader.InnerText = "User Preferences Report";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                List();
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.txtusername.TextChanged += new System.EventHandler(this.txtusername_changed);
        this.Load += new System.EventHandler(this.Page_Load);
    }

    #endregion

    #region Button Events

    public void txtusername_changed(object sender, EventArgs e)
    {
        try
        {
            List();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    public void List()
    {
        string FiletrDate = txtFilterDate.Text.Length > 0 ? Convert.ToDateTime(txtFilterDate.Text).ToString("yyyy-MM-dd") : "";

        DataTable dt = objMember.Preference_Report("REP", txtusername.Text, txtairport.Text, FiletrDate);
        if (dt.Rows.Count > 0)
        {
            gvUser.DataSource = dt;
            gvUser.DataBind();

            btnExport.Visible = true;
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();

            btnExport.Visible = false;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string strName = string.Empty;
            strName = "User_Preferences_Report_" + DateTime.Now.ToString("ddMMMyyyy");

            string[] arrlblPageHits = { "lblSNo", "lblName", "lblUserType", "lblStartICAO", "lblEndICAO", "lblAvailableFromDate", "lblAvailabletodate", 
                                        "lblTimeflag", "lblEmailFlag", "lblEmail", "lblSMSFlag", "lblPhoneNumber" };

            objMember.Exportexcel(gvUser, strName, arrlblPageHits, DateTime.Now.ToString("MMM/dd/yyyy"), lblUser.Text, "User Preferences Report");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }

    #endregion

    #region  User Defined Function



    #endregion
}