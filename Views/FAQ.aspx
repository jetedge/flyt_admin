﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="FAQ.aspx.cs" Inherits="Views_FAQ" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        #wpsm_accordion_4219
        {
            margin-bottom: 20px;
            overflow: hidden;
            float: none;
            width: 100%;
            display: block;
        }
        .entry-content
        {
            font-size: 1em;
            padding: 0;
            line-height: 1.9em;
            text-align: justify;
        }
        #wpsm_accordion_4219 .wpsm_panel-default
        {
            border: 1px solid #111 !important;
            border-radius: 0 !important;
        }
        #wpsm_accordion_4219 .wpsm_panel
        {
            overflow: hidden;
            box-shadow: 0 0 0 rgba(0,0,0,.05);
        }
        .wpsm_panel
        {
            margin-bottom: 20px;
            background-color: #fff;
        }
        #wpsm_accordion_4219 .wpsm_panel-default > .wpsm_panel-heading
        {
            color: #ffffff !important;
            background-color: #111111 !important;
            border-color: #111111 !important;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        #wpsm_accordion_4219 .wpsm_panel-title
        {
            margin: 0px !important;
            text-transform: none !important;
            line-height: 1 !important;
        }
        #wpsm_accordion_4219 .wpsm_panel-title a
        {
            font-family: "Exo" !important;
        }
        #wpsm_accordion_4219 .wpsm_panel-title a
        {
            text-decoration: none;
            overflow: hidden;
            display: block;
            padding: 0;
            font-size: 18px !important;
            font-family: Open Sans !important;
            color: #ffffff !important;
            border-bottom: 0px !important;
        }
        #wpsm_accordion_4219 .ac_open_cl_icon
        {
            background-color: #111111 !important;
            color: #ffffff !important;
            float: right !important;
            padding-top: 12px !important;
            padding-bottom: 12px !important;
            line-height: 1.0 !important;
            padding-left: 15px !important;
            padding-right: 15px !important;
            display: inline-block !important;
        }
        
        #wpsm_accordion_4219 .wpsm_panel-body
        {
            font-family: "Exo" !important;
        }
        #wpsm_accordion_4219 .wpsm_panel-body
        {
            background-color: #000000 !important;
            color: #ffffff !important;
            border-top-color: #111111 !important;
            font-size: 16px !important;
            font-family: Open Sans !important;
            overflow: hidden;
            border: 2px solid #111111 !important;
        }
        #wpsm_accordion_4219 .ac_title_class
        {
            display: block;
            padding-top: 12px;
            padding-bottom: 12px;
            padding-left: 15px;
            padding-right: 15px;
        }
        .fa-minus:before
        {
            content: "\f068";
        }
        .fa-plus:before
        {
            content: "\f067";
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="kt-portlet" id="tblGrid" runat="server">
        <div class="kt-portlet__body p-3">
            <div class="row form-group">
                <div class="col-lg-12 text-right">
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-danger" TabIndex="1" />
                </div>
            </div>
            <div class="row form-group">
                <div class="col-xl-12 boxshadow table-responsive">
                    <asp:GridView ID="gvFAQ" runat="server" CssClass="table" HeaderStyle-CssClass="thead-dark"
                        OnPageIndexChanging="gvFAQ_PageIndexChanging" Width="100%" AutoGenerateColumns="false"
                        PageSize="25" AllowPaging="True" OnSelectedIndexChanged="gvFAQ_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Rowid" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblrowid" Text='<%# Eval("RowID") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <asp:Label ID="lblRowNumber" Text='<%# Eval("DisplaySequence") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Topic" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="13%"
                                HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblTopic" Text='<%# Eval("Topic") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Question" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="13%"
                                HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestion" Text='<%# Eval("Question") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Answer" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="28%"
                                HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblAnswer" Text='<%# Eval("ANSWER") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Active" HeaderStyle-Width="5%" ItemStyle-Width="5%"
                                HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" Text='<%# Eval("ActiveFlag") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Modified By/On" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" HeaderStyle-HorizontalAlign="left" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblcreatedby" Text='<%# Eval("CreatedBy") %>' runat="server"></asp:Label><br />
                                    <asp:Label ID="lblcreatedon" Text='<%# Eval("createdOn") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="4%"
                                ItemStyle-Width="4%" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbView1" runat="server" TabIndex="4" ImageUrl="~/Images/pencil_Edit.png"
                                        CommandName="Select" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="4%"
                                ItemStyle-Width="4%" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" TabIndex="4" ImageUrl="~/Images/icon_delete.png"
                                        CausesValidation="false" AlternateText="Delete Tail details" OnClick="imbtoDeleteOnclick" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                
            </div>
        </div>
    </div>
    <div class="row" id="tblForm" runat="server" visible="false">
        <div class="col-xl-1">
        </div>
        <div class="col-xl-10">
            <div class="kt-portlet" style="margin-bottom: 5px; padding-top: 5px !important;">
                <div class="form-group row">
                    <div class="col-xl-12 text-right">
                        <label class="importantlabels" id="dvMand" runat="server" visible="false">
                            <span style="color: Red">*</span>&nbsp&nbsp<asp:Label runat="server" class="cuslabeluser"
                                ID="lblmandatory" Text="Mandatory fields"></asp:Label>
                        </label>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                            ValidationGroup="SP" />
                        <asp:Button ID="btnView" runat="server" Text="Back" CssClass="btn btn-danger" TabIndex="1"
                            OnClick="btnViewOnClick" />
                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                            runat="server" ShowSummary="false" />
                        <asp:Label ID="lblrowtodelete" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="kt-portlet__body p-4">
                    <div class="row" style="width: 100%;">
                        <div class="col-xl-12">
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 15%; padding-right: 0px;">
                                    Topic <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-4">
                                    <asp:DropDownList ID="ddlTopic" runat="server">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTopic"
                                        InitialValue="0" Display="None" ErrorMessage="Topic is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                                <div class="col-1 text-left" style="padding: 10px;">
                                    <asp:ImageButton ID="imbEditTopic" runat="server" TabIndex="4" ImageUrl="~/Images/Add2.png"
                                        OnClick="imbAddTopic_Click " />
                                    <asp:ImageButton ID="imbAddTopic" runat="server" TabIndex="4" ImageUrl="~/Images/icon-edit-new.png"
                                        OnClick="imbEditTopic_Click" Height="18px" Width="18px" />
                                </div>
                                <label class="col-2 col-form-label" style="max-width: 17%; padding-right: 0px;">
                                    Display Sequence <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-2">
                                    <asp:TextBox ID="txtSeq" runat="server" CssClass="form-control" TabIndex="11" autocomplete="off"
                                        ToolTip="Enter all characters. Length is limited to 100" Width="100%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSeq"
                                        Display="None" ErrorMessage="Display sequence is required." SetFocusOnError="True"
                                        ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" style="max-width: 15%;">
                                    Question <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-9">
                                    <asp:TextBox ID="txtQuestion" runat="server" CssClass="form-control" TabIndex="11"
                                        autocomplete="off" ToolTip="Enter all characters. Length is limited to 100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtQuestion"
                                        Display="None" ErrorMessage="Question is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3 col-form-label" style="max-width: 15%;">
                                    Answer <span class="MandatoryField">* </span>
                                </label>
                                <div class="col-9">
                                    <cc1:Editor ID="EditorMessage" runat="server" Height="300px" TabIndex="13" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="EditorMessage"
                                        Display="None" ErrorMessage="Answer is required." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label" style="max-width: 15%;">
                                    Active
                                </label>
                                <div class="col-2" style="padding-top: 9px;">
                                    <asp:RadioButtonList ID="rblActive" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group row" id="divLMBy" runat="server" visible="false">
                                <div style="float: left; padding-top: 10px; padding-left: 12px">
                                    <asp:Label ID="lblMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                        font-weight: 600;"></asp:Label>
                                    <asp:Label ID="lblLMby" runat="server" Visible="true" Style="font-weight: 600;"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnShow" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpetime" runat="server" TargetControlID="btnShow" PopupControlID="pnltime"
        CancelControlID="btncancelpopup1" BackgroundCssClass="modalBackground" Y="100">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnltime" runat="server" Width="40%" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <div class="form-group row" style="margin-bottom: 0rem;">
            <label class="col-lg-6 col-form-label" style="color: White;">
                <asp:Label ID="Label3" runat="server" Text="Add / Edit Topic"></asp:Label>
            </label>
            <div class="col-6 text-right">
                <asp:Button ID="btncancelpopup1" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="padding-top: 3px; padding-bottom: 3px" />&nbsp;&nbsp;
            </div>
        </div>
        <div class="form-group row">
            <label class="col-4 col-form-label" style="max-width: 15%; color: White;">
                Topic <span class="MandatoryField">* </span>
            </label>
            <div class="col-8">
                <asp:TextBox ID="txtTopic" runat="server" TabIndex="5" MaxLength="100" autocomplete="off"
                    ToolTip="Enter all characters. Length is limited to 100" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCCode" runat="server" ControlToValidate="txtTopic"
                    Display="None" ErrorMessage="Topic is required." SetFocusOnError="True" ValidationGroup="DC">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="row">
            <label class="col-4 col-form-label" style="max-width: 15%; color: White">
                Active <span class="MandatoryField">* </span>
            </label>
            <div class="col-8" style="padding-top: 9px;">
                <asp:RadioButtonList ID="rblTActive" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                    Style="color: White;">
                    <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 text-center">
                <asp:Button ID="btnSaveExternal" runat="server" Text="Save" CssClass="btn btn-danger"
                    TabIndex="5" ValidationGroup="DC" />
                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="DC" ShowMessageBox="true"
                    runat="server" ShowSummary="false" />
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnconfirm" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpeconfirm" runat="server" TargetControlID="btnconfirm"
        PopupControlID="pnlconfirm" BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelconfirm">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlconfirm" runat="server" ScrollBars="Auto" Style="display: none;
        width: 25%; background-color: rgb(17,24,32); padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="left">
                    <asp:Label ID="Label1" Text="Are you sure to delete?" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnClick="btnconfirmdelete_click"
                        Style="color: rgb(255,255,255); text-decoration: none" CausesValidation="false"
                        Text="Ok" />
                    <asp:LinkButton ID="btncancelconfirm" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false"></asp:Label>
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="hdTopId" runat="server" />
</asp:Content>
