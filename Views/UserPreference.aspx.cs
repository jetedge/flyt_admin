﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Security.Cryptography;


public partial class Views_UserPreference : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.UserPreferenceBLL objMember = new BusinessLayer.UserPreferenceBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here
            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "User Preferences");
            }
            
            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "User Preferences";


            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            btnSave.Visible = false;
            btnView.Visible = false;
            hdfPSRID.Value = "0";
            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnView.Click += new System.EventHandler(this.btnViewOnClick);
        this.rblflytime1.TextChanged += new System.EventHandler(this.rblflytime1_OnSelectedIndexChanged);
        this.txtusername.TextChanged += new System.EventHandler(this.txtusername_changed);
        this.txtairport.TextChanged += new System.EventHandler(this.txtairport_changed);
        this.txtDate.TextChanged += new System.EventHandler(this.txtdate_changed);
        this.Load += new System.EventHandler(this.Page_Load);
    }

    #endregion

    #region Button Events

    public void txtusername_changed(object sender, EventArgs e)
    {
        DataTable dtDiscountCoupon = objMember.UserNameFilter(txtusername.Text);

        if (dtDiscountCoupon != null && dtDiscountCoupon.Rows.Count > 0)
        {
            gvUser.DataSource = dtDiscountCoupon;
            gvUser.DataBind();
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();

        }
    }

    public void txtairport_changed(object sender, EventArgs e)
    {
        DataTable dtDiscountCoupon = objMember.AirportFilter(txtairport.Text);

        if (dtDiscountCoupon != null && dtDiscountCoupon.Rows.Count > 0)
        {
            gvUser.DataSource = dtDiscountCoupon;
            gvUser.DataBind();
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();

        }

    }

    public void txtdate_changed(object sender, EventArgs e)
    {
        DataTable dtDiscountCoupon = objMember.DateFilter(Convert.ToDateTime(txtDate.Text).ToString("yyyy-MM-dd"));
        if (dtDiscountCoupon != null && dtDiscountCoupon.Rows.Count > 0)
        {
            gvUser.DataSource = dtDiscountCoupon;
            gvUser.DataBind();
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();
        }
    }

    public void rblflytime1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblflytime1.SelectedIndex == 0)
        {
            trtime.Visible = false;
        }
        else
        {
            trtime.Visible = true;
        }
    }

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {
            string strstartcity = string.Empty;
            strstartcity = txtScity1.Text.Replace(" , ", "-");
            String[] strliststart = strstartcity.Split('-');

            string strendcity = string.Empty;
            strendcity = txtEcity1.Text.Replace(" , ", "-");
            String[] strlistend = strendcity.Split('-');
            string emailflag = string.Empty;


            if (rblEmailFlag.SelectedIndex == 0)
            {
                emailflag = "Yes";
            }
            else
            {
                emailflag = "No";
            }


            string SMSFlag = string.Empty;
            if (rblSMSFlag.SelectedIndex == 0)
            {
                SMSFlag = "Yes";
            }
            else
            {
                SMSFlag = "No";
            }

            string timeflag = string.Empty;
            if (rblflytime1.SelectedIndex == 0)
            {
                timeflag = "Anytime";
            }
            else
            {
                timeflag = "Specific Time";
            }

            objMember.RowID = hdfPSRID.Value;
            objMember.StartAirport = strliststart[0].Trim();
            objMember.EndAirport = strlistend[0].Trim();
            objMember.StatCity = strliststart[1].Trim();
            objMember.EndCity = strlistend[1].Trim();
            if (txtDate1.Text == "")
            {
                objMember.AvailableDate = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else
            {
                objMember.AvailableDate = Convert.ToDateTime(txtDate1.Text).ToString("yyyy-MM-dd");
            }

            if (txtToDate1.Text == "")
            {
                objMember.AvailableToDate = null;
            }
            else
            {
                objMember.AvailableToDate = Convert.ToDateTime(txtToDate1.Text).ToString("yyyy-MM-dd");
            }

            objMember.AvailableTime = txtTime1.Text;
            objMember.EmailFlag = emailflag;
            objMember.TimeFlag = timeflag;
            objMember.SMSFlag = SMSFlag;

            int retVal = objMember.Update(objMember);

            if (retVal > 0)
            {

                btnSave.Visible = false;
                btnView.Visible = false;
                tblGrid.Visible = true;
                tblForm.Visible = false;
                dvFilter.Visible = true;

                lblalert.Text = "User Preferences have been saved successfully";
            }
            else
            {
                lblalert.Text = "An error has occured while processing your request";
            }
            List();
            mpealert.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void gvuser_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (this.gvUser.Rows.Count > 0)
            {
                gvUser.UseAccessibleHeader = true;
                gvUser.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbluserid = (Label)e.Row.FindControl("lbluserid");
                GridView gv_Child = (GridView)e.Row.FindControl("gv_Child");
                int userid = Convert.ToInt32(lbluserid.Text);

                e.Row.Cells[2].Attributes.Add("style", "word-break:break-all;word-wrap:break-word;");

                DataTable dtpreference = objMember.Child(userid.ToString());

                if (dtpreference.Rows.Count != null && dtpreference.Rows.Count > 0)
                {
                    gv_Child.DataSource = dtpreference;
                    gv_Child.DataBind();
                    for (int i = 0; i < gv_Child.Rows.Count; i++)
                    {
                        ((Label)gv_Child.Rows[i].FindControl("lblstartdate")).Text = Convert.ToDateTime(dtpreference.Rows[0]["Availabledate"].ToString()).ToString("MM-dd-yyyy");
                        ((Label)gv_Child.Rows[i].FindControl("lblenddate")).Text = Convert.ToDateTime(dtpreference.Rows[0]["Availabletodate"].ToString()).ToString("MM-dd-yyyy"); ;
                    }
                }
                else
                {
                    gv_Child.DataSource = dtpreference;
                    gv_Child.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void ContactsGridView_RowCommand(Object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int index = Convert.ToInt32(e.CommandArgument);

            GridView childGrid = (GridView)sender;

            object str1 = childGrid.DataKeys[index].Value;
            int SUID = Convert.ToInt32(str1);
            DataTable dtpreference = objMember.Edit(SUID.ToString());

            if (dtpreference != null && dtpreference.Rows.Count > 0)
            {
                hdfPSRID.Value = dtpreference.Rows[0]["rowid"].ToString();
                lbluserT.Text = dtpreference.Rows[0]["username"].ToString();
               // txtScity1.Text = dtpreference.Rows[0]["Startcity"].ToString();
               // txtEcity1.Text = dtpreference.Rows[0]["endcity"].ToString();

                DataTable dtSPstart = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select city,StateName from JETEDGE_FUEL..st_airport Where icao='" + dtpreference.Rows[0]["StartAirport"].ToString() + "'").Tables[0];


                txtScity1.Text = dtpreference.Rows[0]["StartAirport"].ToString() + " , " + dtSPstart.Rows[0]["city"].ToString() + " - " + dtSPstart.Rows[0]["StateName"].ToString();

                DataTable dtSPend = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select city from JETEDGE_FUEL..st_airport Where icao='" + dtpreference.Rows[0]["EndAirPort"].ToString() + "'").Tables[0];

                txtEcity1.Text = dtpreference.Rows[0]["EndAirPort"].ToString() + " , " + dtSPstart.Rows[0]["city"].ToString() + " - " + dtSPstart.Rows[0]["StateName"].ToString();

                txtDate1.Text = Convert.ToDateTime(dtpreference.Rows[0]["Availabledate"].ToString()).ToString("MM-dd-yyyy");
                txtToDate1.Text = Convert.ToDateTime(dtpreference.Rows[0]["Availabletodate"].ToString()).ToString("MM-dd-yyyy");


                rblEmailFlag.SelectedIndex = rblEmailFlag.Items.IndexOf(rblEmailFlag.Items.FindByValue(dtpreference.Rows[0]["EmailFlag"].ToString()));
                rblflytime1.SelectedIndex = rblflytime1.Items.IndexOf(rblflytime1.Items.FindByValue(dtpreference.Rows[0]["Timeflag"].ToString()));


                if (rblflytime1.SelectedIndex == 0)
                {
                    txtTime1.Text = dtpreference.Rows[0]["AvailableTime"].ToString();

                    trtime.Visible = false;

                }
                else
                {
                    txtTime1.Text = dtpreference.Rows[0]["AvailableTime"].ToString();

                    trtime.Visible = true;
                }


            }
            btnSave.Text = "Update";
            tblForm.Visible = true;
            tblGrid.Visible = false;
            btnSave.Visible = true;
            btnView.Visible = true;
            dvFilter.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnViewOnClick(object sender, EventArgs e)
    {
        try
        {
            List();
            dvFilter.Visible = true;
            tblForm.Visible = false;
            tblGrid.Visible = true;
            btnSave.Visible = false;
            btnView.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    #endregion

    #region  User Defined Function

    public void List()
    {
        DataTable dt = objMember.Select();

        if (dt.Rows.Count > 0)
        {
            gvUser.DataSource = dt;
            gvUser.DataBind();
        }
        else
        {
            gvUser.DataSource = null;
            gvUser.DataBind();
        }
    }
    public void Clear()
    {
        hdfPSRID.Value = "0";
        txtScity1.Text = string.Empty;
        txtEcity1.Text = string.Empty;
        txtDate1.Text = string.Empty;
        txtToDate1.Text = string.Empty;
        txtTime1.Text = string.Empty;
        rblEmailFlag.SelectedIndex = 0;
        rblflytime1.SelectedIndex = 0;
    }

    void SendEmail(string strEmail, string strPassword, string strName)
    {
        StringBuilder sbEmailBody = new StringBuilder();


        string subject = "Welcome to Jet Edge !";

        string strURL = "https://www.flyjetedge.net/Zoom/";

        sbEmailBody.Append("Dear " + strName + ",<br><br>");
        sbEmailBody.Append("We would like to inform you that Jet Edge has created an user account for you in Jet Edge Zoom.<br><br>");
        sbEmailBody.Append("Your account details are as follows:<br><br>");
        sbEmailBody.Append("URL : <a href='" + strURL + "'>" + strURL + " </a><br><br>");
        sbEmailBody.Append("Login ID : " + strEmail + "<br><br>");
        sbEmailBody.Append("Password : " + strPassword + " <br><br>");

        sbEmailBody.Append("To log into Jet Edge Zoom, please use the above URL. Please save this in browser favorites to access at any point of time.<br><br>");
        sbEmailBody.Append("If you have questions, please do not hesitate to contact Jet Edge support<br><br><br>");
        sbEmailBody.Append("(818) 442-0096 | zoom@flyjetedge.com<br><br>");

        sbEmailBody.Append("Thank you for your co-operation,<br><br>Jet Edge IT Team<br>");
        sbEmailBody.Append("<h3><u>NOTE: Replies to this email will not be accepted. Please contact the Jet Edge support team directly.</u></h3><br>");


        try
        {
            string strRes = genclass.SendEmail_login(strEmail, subject, sbEmailBody.ToString());

            if (strRes.ToLower().Trim() != "yes")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Email sending failed.');", true);
            }
            else
            {
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Email sending failed.');", true);
        }

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetCompletionList(string prefixText, int count)
    {
        return AutoFillProducts(prefixText);

    }

    private static List<string> AutoFillProducts(string prefixText)
    {
        using (SqlConnection con = new SqlConnection())
        {
            con.ConnectionString = SqlHelper.FLYTConnectionString;

            using (SqlCommand com = new SqlCommand())
            {
               // com.CommandText = "select distinct City +' - ' +icao as city from JETEDGE_FUEL..st_airport where " + "city like  @Search + '%' or " + "icao like  @Search + '%'";
                com.CommandText = "select distinct icao+' , '+ City +' - ' +StateName as city from JETEDGE_FUEL..st_airport where " + "icao like  @Search + '%' AND Access=1";

                com.Parameters.AddWithValue("@Search", prefixText);
                com.Connection = con;
                con.Open();
                List<string> countryNames = new List<string>();
                using (SqlDataReader sdr = com.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        countryNames.Add(sdr["city"].ToString());
                    }
                }
                con.Close();
                return countryNames;


            }

        }
    }
    #endregion
}