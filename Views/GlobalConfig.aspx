﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/MainMaster.master" AutoEventWireup="true"
    CodeFile="GlobalConfig.aspx.cs" Inherits="Views_TaxSetup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .flightticket
        {
            border-radius: 5px;
            padding: 10px;
            box-shadow: 0 0 7px 3px rgba(0, 0, 0, 0.1);
            -webkit-font-smoothing: antialiased;
            background: white;
            color: rgb(17,24,32);
        }
    </style>
    <script type="text/javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function FFSetupDelete() {
            return confirm("Are you sure you want to delete the selected Fore Flight Tail Setup?");
        }

        function checkAll(objRef) {
            var GridView = document.getElementById("MainContent_gvFleet");
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function txtCounStartTimefun(object) {
            var txtCounStartTime = document.getElementById('txtStartTime1');
            document.getElementById('MainContent_txtStartTime').value = txtCounStartTime.value;
        }
        function Manual() {
            document.getElementById('txtStartTime1').value = document.getElementById('MainContent_txtStartTime').value;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="tblForm" runat="server">
        <div class="kt-portlet__body p-4">
            <div class="row">
                <div class="col-xl-15">
                </div>
                <div class="col-xl-45 pr-1">
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2  fab fa-cc-visa"></i>Payment (Authorize .Net)</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-6 col-form-label" style="line-height: 30px;">
                                        Do you want to deduct payment</br>when booking?
                                    </label>
                                    <div class="col-6  col-form-label" style="line-height: 30px;">
                                        <asp:RadioButtonList ID="rblPaymentgateway" runat="server" TabIndex="20" RepeatDirection="Horizontal"
                                            RepeatColumns="1">
                                            <asp:ListItem Text="Enabled (Live Account)" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Disabled (Sandbox Account)" Value="0"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12" style="text-align: center">
                                        <asp:Button ID="btnPGSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                            OnClick="btnPaymentGatewaySave_Click" />
                                    </div>
                                </div>
                                <div class="row" id="divPGLMBy" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="lblPGMB" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="lblPGLMby" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2  fas fa-sms"></i>Enable SMS </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-lg-7 col-form-label" style="line-height: 30px;">
                                        Do you want enable SMS when booking ?
                                    </label>
                                    <div class="col-lg-5 col-form-label" style="line-height: 30px;">
                                        <asp:RadioButtonList ID="rblEnableSMS" runat="server" TabIndex="20" RepeatDirection="Horizontal"
                                            RepeatColumns="2">
                                            <asp:ListItem Text="Yes (Twilio)" Value="Y" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12" style="text-align: center">
                                        <asp:Button ID="btnSMSSave" runat="server" Text="Save" CssClass="btn btn-danger"
                                            TabIndex="1" OnClick="btnSMSSave_Click" />
                                    </div>
                                </div>
                                <div class="row" id="divSMSLMBy" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="Label2" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="lblSMSLMby" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2  fa fa-file-signature"></i>Docu Sign </span>
                                        <asp:LinkButton ID="btnDocSignHistory" runat="server" Text="History" Style="color: Blue;
                                            font-size: 15px; font-weight: bold; float: right; text-decoration: underline;"
                                            TabIndex="1" OnClick="btnDocSignHistory_Click" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">
                                        Docu Sign Expiry <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-4">
                                        <asp:TextBox ID="txtDocExp" runat="server" TabIndex="5" MaxLength="5" autocomplete="off"
                                            ToolTip="Enter numeric characters only. Length is limited to 5" CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FTV" runat="server" TargetControlID="txtDocExp"
                                            ValidChars="0123456789">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDocExp"
                                            Display="None" ErrorMessage="Docu Sign Expiry is required." SetFocusOnError="True"
                                            ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                    </div>
                                    <label class="col-4 col-form-label">
                                        ( in Minutes )
                                    </label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">
                                        Effective From <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-4">
                                        <asp:TextBox ID="txtEffFrom" runat="server" TabIndex="5" MaxLength="10" autocomplete="off"
                                            onkeypress="return false;" ToolTip="Enter numeric characters only. Length is limited to 100"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="cal_Theme1"
                                            Format="MMM dd, yyyy" TargetControlID="txtEffFrom">
                                        </asp:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtEffFrom"
                                            Display="None" ErrorMessage="Effective From is required." SetFocusOnError="True"
                                            ValidationGroup="SP">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12" style="text-align: center">
                                        <asp:Button ID="btnDocSignSave" runat="server" Text="Save" CssClass="btn btn-danger"
                                            TabIndex="1" OnClick="btnDocSignSave_Click" ValidationGroup="SP" />
                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="SP" ShowMessageBox="true"
                                            runat="server" ShowSummary="false" />
                                    </div>
                                </div>
                                <div class="row" id="divDOC" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="Label5" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="lblDocUp" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket">
                                <div class="form-group row">
                                    <div class="col-lg-7">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2 fa fa-users"></i>External Users </span>
                                    </div>
                                    <div class="col-lg-5 text-right">
                                        <asp:LinkButton ID="LinkButton1" CommandName="EXTUSER" runat="server" ForeColor="Blue"
                                            Font-Size="15px" Font-Bold="true" Text="Add New" OnClick="lnkAddNew_Click" Style="float: right;
                                            text-decoration: underline" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xl-12">
                                        <asp:GridView ID="gvExtUser" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                                            EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                            OnSelectedIndexChanged="gvextUser_SelectedIndexChanged" OnRowDeleting="gvextUser_RowDeleting"
                                            DataKeyNames="ExtId" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%#Bind("UserName")%>' />
                                                        <asp:Label ID="lblExtUserID" runat="server" Visible="false" Text='<%#Bind("ExtId")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="22%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmailID" runat="server" Text='<%#Bind("EmailId")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Wrap="true" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User Role">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserType" runat="server" Text='<%#Bind("UserRole")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="22%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbEdit" runat="server" CommandName="Select" ImageUrl="~/Images/pencil_Edit.png"
                                                            CausesValidation="false" Height="22px" AlternateText="Edit" ToolTip="Click here to edit"
                                                            CommandArgument="<%# Container.DataItemIndex + 1 %>" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle Width="4%" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbDel" runat="server" CommandName="Delete" ImageUrl="~/Images/icon_delete.png"
                                                            CausesValidation="false" AlternateText="Delete" ToolTip="Click here to Delete"
                                                            OnClientClick="return confirm('Are you sure to delete?');" CommandArgument="<%# Container.DataItemIndex + 1 %>" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row" id="div2" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="Label7" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="Label8" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-45">
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2 fab fa-gg-circle"></i>Custom FLYT </span>
                                        <asp:LinkButton ID="lnkCustomHistory" runat="server" Text="History" Style="color: Blue;
                                            font-size: 15px; font-weight: bold; float: right; text-decoration: underline;"
                                            TabIndex="1" OnClick="lnkCustomHistory_Click" />
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-lg-12">
                                        <table width="100%">
                                            <tr>
                                                <td valign="middle">
                                                    Price Cap = Custom_FLYT_Hours x (
                                                </td>
                                                <td style="width: 11%;">
                                                    <asp:TextBox ID="txtPriceCap1" runat="server" TabIndex="5" MaxLength="10" autocomplete="off"
                                                        ToolTip="Enter numeric characters only. Length is limited to 10" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtPriceCap1"
                                                        Display="None" ErrorMessage="Price Cap is required." SetFocusOnError="True" ValidationGroup="CUSTOM" />
                                                </td>
                                                <td valign="middle" align="center">
                                                    x Custom_FLYT_Hours +
                                                </td>
                                                <td style="width: 11%;">
                                                    <asp:TextBox ID="txtPriceCap2" runat="server" TabIndex="5" MaxLength="10" autocomplete="off"
                                                        ToolTip="Enter numeric characters only. Length is limited to 10" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtPriceCap2"
                                                        Display="None" ErrorMessage="Price Cap is required." SetFocusOnError="True" ValidationGroup="CUSTOM" />
                                                </td>
                                                <td valign="middle" align="center">
                                                    )
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-5 col-form-label">
                                        Incremental Stops Price <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-7">
                                        <asp:TextBox ID="txtIncrementalStops" runat="server" TabIndex="5" MaxLength="15"
                                            autocomplete="off" Width="50%" ToolTip="Enter numeric characters only. Length is limited to 15"
                                            CssClass="form-control" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtIncrementalStops"
                                            ValidChars="0123456789">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtIncrementalStops"
                                            Display="None" ErrorMessage="Incremental Stops Price is required." SetFocusOnError="True"
                                            ValidationGroup="CUSTOM" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-5 col-form-label">
                                        Effective From <span class="MandatoryField">* </span>
                                    </label>
                                    <div class="col-lg-7">
                                        <asp:TextBox ID="txtCustomEffec" runat="server" TabIndex="5" MaxLength="10" autocomplete="off"
                                            onkeypress="return false;" ToolTip="Enter numeric characters only. Length is limited to 100"
                                            CssClass="form-control" Width="50%"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="MMM dd, yyyy"
                                            CssClass="cal_Theme1" TargetControlID="txtCustomEffec">
                                        </asp:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtCustomEffec"
                                            Display="None" ErrorMessage="Effective From is required." SetFocusOnError="True"
                                            ValidationGroup="CUSTOM">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12 text-center">
                                        <asp:Button ID="btnCustomSave" runat="server" Text="Save" CssClass="btn btn-danger"
                                            TabIndex="1" OnClick="btnCustomSave_Click" ValidationGroup="CUSTOM" />
                                        <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="CUSTOM" ShowMessageBox="true"
                                            runat="server" ShowSummary="false" />
                                    </div>
                                </div>
                                <div class="row" id="dvCFLYtModified" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="Label6" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="lblCFLYtModifiedBy" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket">
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2 fa fa-users"></i>Crew / Day Duty Filter</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">
                                        BLK Hours / Day
                                    </label>
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtBLKHours" runat="server" TabIndex="5" MaxLength="10" autocomplete="off"
                                            ToolTip="Enter numeric characters only. Length is limited to 10" CssClass="form-control" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtBLKHours"
                                            ValidChars="0123456789.">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtBLKHours"
                                            Display="None" ErrorMessage="BLK Hours / Day is required." SetFocusOnError="True"
                                            ValidationGroup="CREWDUTY" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4 col-form-label">
                                        Crew Duty Hours / Day
                                    </label>
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtCrewHours" runat="server" TabIndex="5" MaxLength="10" autocomplete="off"
                                            ToolTip="Enter numeric characters only. Length is limited to 10" CssClass="form-control" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtCrewHours"
                                            ValidChars="0123456789.">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtCrewHours"
                                            Display="None" ErrorMessage="Crew Duty Hours / Day is required." SetFocusOnError="True"
                                            ValidationGroup="CREWDUTY" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-7 col-form-label">
                                        Do you want apply this filter in Custom FLYT ?
                                    </label>
                                    <div class="col-lg-5 col-form-label">
                                        <asp:RadioButtonList ID="rblCrewDutyFilter" runat="server" TabIndex="20" RepeatDirection="Horizontal"
                                            RepeatColumns="2">
                                            <asp:ListItem Text="No" Value="N"> </asp:ListItem>
                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="form-group row" style="font-style: italic;">
                                    <label class="col-2 col-form-label text-center" style="color: red;">
                                        Notes :
                                    </label>
                                    <div class="col-lg-10">
                                        <label>
                                            * One Hour before the first leg State Time.
                                        </label>
                                        <br />
                                        <label>
                                            * One Hour after the last leg Emd Time.
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12 text-center">
                                        <asp:Button ID="btnCrewDutySave" runat="server" Text="Save" CssClass="btn btn-danger"
                                            TabIndex="1" OnClick="btnCrewDutySave_Click" ValidationGroup="CREWDUTY" />
                                        <asp:ValidationSummary ID="ValidationSummary5" ValidationGroup="CREWDUTY" ShowMessageBox="true"
                                            runat="server" ShowSummary="false" />
                                    </div>
                                </div>
                                <div class="row" id="div3" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="Label9" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="Label10" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket">
                                <div class="form-group row">
                                    <div class="col-lg-7">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2  fa fa-clock"></i>Flight Hours </span>
                                    </div>
                                    <div class="col-lg-5 text-right">
                                        <asp:LinkButton ID="Button2" runat="server" ForeColor="Blue" Font-Size="15px" Font-Bold="true"
                                            Text="Comparision" OnClick="btnCalc_Click" Style="float: right; text-decoration: underline" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-5 col-form-label">
                                        Do you want calculate from ?
                                    </label>
                                    <div class="col-7  col-form-label" style="line-height: 24px; padding-right: 0px;
                                        padding-left: 0px;">
                                        <asp:RadioButtonList ID="rblFlightHours" runat="server" TabIndex="20" RepeatDirection="Horizontal"
                                            RepeatColumns="1" AutoPostBack="true" OnSelectedIndexChanged="rblFlightHours_SelectedIndexChanged">
                                            <asp:ListItem Text="Fore Flight API" Value="FF" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Great Circle Mapper API" Value="GC"></asp:ListItem>
                                            <asp:ListItem Text="Distance Formula" Value="MN"></asp:ListItem>
                                            <asp:ListItem Text="Great Circle Formula" Value="GF"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                    </div>
                                    <div class="col-lg-4" style="text-align: center">
                                        <asp:Button ID="btnFHSave" runat="server" Text="Save" CssClass="btn btn-danger" TabIndex="1"
                                            OnClick="btnFHSave_Click" />
                                    </div>
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                                <div class="row" id="divFHLMBy" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="Label3" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="lblFHLMby" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xl-12">
                            <div class="flightticket" id="divForFlight" runat="server">
                                <div class="form-group row">
                                    <div class="col-lg-7">
                                        <span style="font-size: 16px; font-weight: 600; letter-spacing: 0.5px;" class="svg-icon svg-icon-md svg-icon-danger">
                                            <i class="icon-lg pr-2  fa fa-cubes"></i>Fore Flight Tail Setup </span>
                                    </div>
                                    <div class="col-lg-5 text-right">
                                        <asp:LinkButton ID="lnkAddNew" runat="server" ForeColor="Blue" Font-Size="15px" Font-Bold="true"
                                            Text="Add New" OnClick="lnkAddNew_Click" CommandName="FOREFLYT" Style="float: right;
                                            text-decoration: underline" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:GridView ID="gvForeFlight" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                                            EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                            Width="100%" OnSelectedIndexChanged="gvForeFlight_SelectedIndexChanged" OnRowDeleting="gvForeFlight_OnRowDeleting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                        <asp:Label ID="lblForeFlightID" runat="server" Visible="false" Text='<%#Bind("RowID")%>' />
                                                        <asp:Label ID="lblCruiseProfiles" runat="server" Visible="false" Text='<%#Bind("CruiseProfiles")%>' />
                                                        <asp:Label ID="lblwindModel" runat="server" Visible="false" Text='<%#Bind("windModel")%>' />
                                                        <asp:Label ID="lblfixedWindComponent" runat="server" Visible="false" Text='<%#Bind("fixedWindComponent")%>' />
                                                        <asp:Label ID="lblfixedIsaDeviation" runat="server" Visible="false" Text='<%#Bind("fixedIsaDeviation")%>' />
                                                        <asp:Label ID="lblhistoricalProbability" runat="server" Visible="false" Text='<%#Bind("historicalProbability")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Aircraft Registration </br>(in FF)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%#Bind("TailID")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="25%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Setup By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSetupBy" runat="server" Text='<%#Bind("SetuupBy")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Modified By/On">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<%#Bind("CreatedByName")%>' />
                                                        <br />
                                                        <asp:Label ID="lblCreatedOn" runat="server" Text='<%#Bind("CreatedOn")%>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="18%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbEdit" runat="server" CommandName="Select" ImageUrl="~/Images/pencil_Edit.png"
                                                            CausesValidation="false" Height="22px" AlternateText="Edit" ToolTip="Click here to edit"
                                                            CommandArgument="<%# Container.DataItemIndex + 1 %>" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle Width="4%" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbDelete" runat="server" CommandName="Delete" ImageUrl="~/Images/icon_delete.png"
                                                            CausesValidation="false" Height="22px" AlternateText="Edit" ToolTip="Click here to edit"
                                                            CommandArgument="<%# Container.DataItemIndex + 1 %>" OnClientClick="if ( ! FFSetupDelete()) return false;" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <HeaderStyle Width="4%" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row" id="div1" runat="server" visible="false">
                                    <div class="col-lg-12">
                                        <asp:Label ID="Label1" runat="server" Text="Last Modified By : " Style="color: #e4685d;
                                            font-weight: 600;" />
                                        <asp:Label ID="Label4" runat="server" Visible="true" Style="font-weight: 600;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-15">
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnHist" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeCalculation" runat="server" PopupControlID="pnlHistory"
        TargetControlID="btnHist" Y="10" CancelControlID="btnHistClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlHistory" runat="server" CssClass="modalPopupnew" Style="display: none;
        background-color: White; width: 90%; height: 95%; padding: 10px">
        <div class="form-group row">
            <label class="col-9" style="color: White;">
                <h5 style="color: Black; margin-left: 0%; font-size: 18px">
                    Comparision</h5>
            </label>
            <div class="col-lg-3 text-right">
                <asp:Button ID="btnHistClose" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="padding: 3px 12px;" ToolTip="Close Popup"></asp:Button>
            </div>
        </div>
        <div class="row ml-3">
            <label class="col-2 col-form-label">
                Start Aiport <span class="MandatoryField">* </span>
                <br />
                <asp:TextBox runat="server" ID="txtStartAirport" MaxLength="20" Style="text-transform: uppercase;"
                    Width="100%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtStartAirport"
                    Display="None" ErrorMessage="Start Airport is required." SetFocusOnError="True"
                    ValidationGroup="S">*</asp:RequiredFieldValidator>
            </label>
            <label class="col-2 col-form-label">
                End Aiport <span class="MandatoryField">* </span>
                <br />
                <asp:TextBox runat="server" ID="txtEndAirport" MaxLength="20" Style="text-transform: uppercase;"
                    Width="100%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndAirport"
                    Display="None" ErrorMessage="End Airport is required." SetFocusOnError="True"
                    ValidationGroup="S">*</asp:RequiredFieldValidator>
            </label>
            <label class="col-2 col-form-label" style="max-width: 15%;">
                Tail <span class="MandatoryField">* </span>
                <br />
                <asp:DropDownList runat="server" ID="ddlTail" MaxLength="20" Width="100%">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAvgSpeed"
                    Display="None" ErrorMessage="Avg. Speed is required." SetFocusOnError="True"
                    ValidationGroup="S">*</asp:RequiredFieldValidator>
            </label>
            <label class="col-2 col-form-label" style="max-width: 10%;">
                Avg. Speed <span class="MandatoryField">* </span>
                <br />
                <asp:TextBox runat="server" ID="txtAvgSpeed" MaxLength="20" Width="100%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAvgSpeed"
                    Display="None" ErrorMessage="Avg. Speed is required." SetFocusOnError="True"
                    ValidationGroup="S">*</asp:RequiredFieldValidator>
            </label>
            <label class="col-2 col-form-label" style="max-width: 13%;">
                Depature Date<span class="MandatoryField">* </span>
                <br />
                <asp:TextBox runat="server" ID="txtDepartDate" Placeholder="Date(MMM dd, yyyy)" onkeypress="return NumericsIphon(event)"
                    TabIndex="4" MaxLength="20" Width="100%"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtDepartDate"
                    CssClass="cal_Theme1" Format="MMM dd, yyyy" Enabled="True">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDepartDate"
                    Display="None" ErrorMessage="Departure  Date is required." SetFocusOnError="True"
                    ValidationGroup="S">*</asp:RequiredFieldValidator>
            </label>
            <label class="col-2 col-form-label" style="max-width: 13%;">
                Depature Time<span class="MandatoryField">* </span>
                <br />
                <input style="display: none;" runat="server" id="txtStartTime" maxlength="20" type="text" />
                <input class="form-control" id="txtStartTime1" tabindex="10" onblur="txtCounStartTimefun(this);"
                    type="time" style="width: 80%;" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtStartTime"
                    Display="None" ErrorMessage="Departure Time is required." SetFocusOnError="True"
                    ValidationGroup="S">*</asp:RequiredFieldValidator>
            </label>
            <div class="col-1 col-form-label">
                <br />
                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-danger" Text="Calculate"
                    OnClick="btnSearch_Click" ValidationGroup="S" ToolTip="Calculate"></asp:Button>
                <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="S" ShowMessageBox="true"
                    runat="server" ShowSummary="false" />
            </div>
        </div>
        <div class="form-group row ml-3 mt-4" id="dvSearch" runat="server">
            <div class="col-3">
                <div class="flightticket" style="height: 425px;">
                    <div class="row">
                        <div class="col-lg-12" style="font-size: 1.2rem; font-weight: 600; letter-spacing: 0.5px;">
                            Fore Flight API
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Departure Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblFFDepartureTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Arrival Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblFFArrivalTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group
    row">
                        <label class="col-6 col-form-label">
                            Distance (nm)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblFFDistance" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6
    col-form-label">
                            Flight Time (HH:mm)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblFFFlightTimehour" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Flight Time (MIN)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblFFFlightTimeMin" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-form-label" style="text-align: center">
                            <asp:Button ID="btnFFApply" runat="server" CssClass="btn btn-danger" Text="Apply"
                                OnClick="btnFFApply_Click" ToolTip="Apply"></asp:Button>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="flightticket" style="height: 425px;">
                    <div class="row">
                        <div class="col-lg-12" style="font-size: 1.2rem; font-weight: 600; letter-spacing: 0.5px;">
                            Great Circle API
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Departure Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblGCDepartureTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6
    col-form-label">
                            Arrival Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblGCArrivalTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Distance (nm)
                        </label>
                        <label class="col-5
    col-form-label">
                            <asp:Label ID="lblGCDistance" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group
    row">
                        <label class="col-6 col-form-label">
                            Flight Time (HH:mm)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblGCFlightTimehour" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6
    col-form-label">
                            Flight Time (MIN)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblGCFlightTimeMin" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-form-label" style="text-align: center">
                            <asp:Button ID="btnGCApply" runat="server" CssClass="btn
    btn-danger" Text="Apply" OnClick="btnGCApply_Click" ToolTip="Apply"></asp:Button>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="flightticket" style="height: 425px;">
                    <div class="row">
                        <div class="col-lg-12" style="font-size: 1.2rem; font-weight: 600; letter-spacing: 0.5px;">
                            Great Circle Mapper Formula
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Departure Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblDepartureTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group
    row">
                        <label class="col-6 col-form-label">
                            Arrival Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblArrivalTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6
    col-form-label">
                            Distance (nm)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblDistance" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Flight Time (HH:mm)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblFlightTimehour" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Flight Time (MIN)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblFlightTimeMin" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group
    row">
                        <label class="col-12 col-form-label" style="text-align: center">
                            <asp:Button ID="btnMNApply" runat="server" CssClass="btn btn-danger" Text="Apply"
                                OnClick="btnGFApply_Click" ToolTip="Apply"></asp:Button>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="flightticket" style="height: 425px;">
                    <div class="row">
                        <div class="col-lg-12" style="font-size: 1.2rem; font-weight: 600; letter-spacing: 0.5px;">
                            Distance Formula
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Departure Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblDFDepartureTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group
    row">
                        <label class="col-6 col-form-label">
                            Arrival Time
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblDFArrivalTime" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6
    col-form-label">
                            Distance (nm)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblDFDistance" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Flight Time (HH:mm)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblDFFlightTimehour" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-6 col-form-label">
                            Flight Time (MIN)
                        </label>
                        <label class="col-6 col-form-label">
                            <asp:Label ID="lblDFFlightTimeMin" runat="server" Style="font-weight: 500" />
                        </label>
                    </div>
                    <div class="form-group
    row">
                        <label class="col-12 col-form-label" style="text-align: center">
                            <asp:Button ID="Button3" runat="server" CssClass="btn btn-danger" Text="Apply" OnClick="btnMNApply_Click"
                                ToolTip="Apply"></asp:Button>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
        </div>
    </asp:Panel>
    <asp:Button ID="btnFore" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeForeFlight" runat="server" PopupControlID="pnlFOreFlight"
        TargetControlID="btnFore" Y="10" CancelControlID="btnCloeFore" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlFOreFlight" runat="server" CssClass="modalPopupnew p-0" Style="display: none;
        background-color: White; width: 70%; height: 90%; padding: 10px; overflow-y: auto;
        overflow-x: hidden;">
        <div class="modal-content" style="background-color: white;">
            <div class="modal-header" style="border-bottom: 1px solid #ccc;">
                <h5 class="modal-title" style="color: black; margin: 0; color: Black; font-weight: 500;
                    font-size: 1.2rem;">
                    Fore Flight Setup</h5>
                <asp:Button ID="btnCloeFore" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="padding: 3px 12px; float: right;" ToolTip="Close Popup"></asp:Button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-10">
                        <div class="form-group row">
                            <label class="col-lg-3" style="align-self: center;">
                                Aircraft Registration<br />
                                <span style="font-size: 0.9rem;">(in Fore Flight) </span><span class="MandatoryField">
                                    * </span>
                            </label>
                            <div class="col-lg-3" style="align-self: center;">
                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlForeFlightTail" AutoPostBack="true"
                                    TabIndex="1" OnSelectedIndexChanged="ddlForeFlightTail_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlForeFlightTail"
                                    InitialValue="0" Display="None" ErrorMessage="Tail in Fore Flight is required."
                                    SetFocusOnError="True" ValidationGroup="ER" />
                            </div>
                            <label class="col-lg-3" style="align-self: center;">
                                Fixed Wind Component <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-lg-3" style="align-self: center;">
                                <asp:TextBox runat="server" ID="txtfixedWindComponent" onkeypress="return isNumberKey(event)"
                                    MaxLength="50" TabIndex="5"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtfixedWindComponent"
                                    Display="None" ErrorMessage="Fixed Wind Component is required." SetFocusOnError="True"
                                    ValidationGroup="ER" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3" style="align-self: center;">
                                Cruise Profiles
                                <br />
                                <span style="font-size: 0.9rem;">(in Fore Flight) </span><span class="MandatoryField">
                                    * </span>
                            </label>
                            <div class="col-lg-3" style="align-self: center;">
                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlProfiles" MaxLength="50"
                                    TabIndex="2">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlProfiles"
                                    InitialValue="0" Display="None" ErrorMessage="Cruise Profiles is required." SetFocusOnError="True"
                                    ValidationGroup="ER" />
                            </div>
                            <label class="col-lg-3" style="align-self: center;">
                                Fixed Is a Deviation <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-lg-3" style="align-self: center;">
                                <asp:TextBox runat="server" ID="txtfixedIsaDeviation" onkeypress="return isNumberKey(event)"
                                    MaxLength="50" TabIndex="6"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtfixedIsaDeviation"
                                    Display="None" ErrorMessage="Fixed Is a Deviation is required." SetFocusOnError="True"
                                    ValidationGroup="ER" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3" style="align-self: center;">
                                Wind Model <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-lg-3" style="align-self: center;">
                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlwindModel" MaxLength="50"
                                    TabIndex="3">
                                    <asp:ListItem Value="0" Text="Please Select"></asp:ListItem>
                                    <asp:ListItem Value="Forecasted" Text="Forecasted"></asp:ListItem>
                                    <asp:ListItem Value="Fixed" Text="Fixed"></asp:ListItem>
                                    <asp:ListItem Value="Historical" Text="Historical"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:TextBox runat="server" ID="txtwindModel" MaxLength="50" TabIndex="3"></asp:TextBox>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlwindModel"
                                    Display="None" ErrorMessage="Wind Model is required." SetFocusOnError="True"
                                    ValidationGroup="ER" />
                            </div>
                            <label class="col-lg-3" style="align-self: center;">
                                Historical Probability <span class="MandatoryField">* </span>
                            </label>
                            <div class="col-lg-3" style="align-self: center;">
                                <asp:TextBox runat="server" ID="txthistoricalProbability" onkeypress="return isNumberKey(event)"
                                    MaxLength="50" TabIndex="7"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txthistoricalProbability"
                                    Display="None" ErrorMessage="Historical Probability is required." SetFocusOnError="True"
                                    ValidationGroup="ER" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3" style="align-self: center;">
                                Setup By
                            </label>
                            <div class="col-lg-4" style="align-self: center;">
                                <asp:RadioButtonList ID="rblSetupBy" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                    TabIndex="4" AutoPostBack="true" OnSelectedIndexChanged="rblSetupBy_SelectedIndexChanged">
                                    <asp:ListItem Text="All Fleet" Value="All" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Specific Fleet" Value="SPE"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group row" id="dvFleet" runat="server" visible="false">
                            <div class="col-lg-12">
                                <asp:GridView ID="gvFleet" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                                    EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex +1 %>
                                                <asp:Label ID="lblFleetID" runat="server" Visible="false" Text='<%#Bind("RowID")%>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fleet Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" Text='<%#Bind("FleetName")%>' />
                                            </ItemTemplate>
                                            <HeaderStyle Width="20%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="checkAll" Text="Select All" Font-Bold="true" runat="server" onclick="checkAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkStatus" runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12 text-center">
                                <asp:Button ID="btnSaveFleet" runat="server" CssClass="btn btn-danger" Text="Save"
                                    ToolTip="click here to save" ValidationGroup="ER" OnClick="btnSaveFleet_Click">
                                </asp:Button>
                                &nbsp;&nbsp;
                                <asp:Button ID="btnClear" runat="server" CssClass="btn btn-danger" Text="Clear" ToolTip="click here to clear"
                                    OnClick="btnClear_Click"></asp:Button>
                                <asp:ValidationSummary ID="vldSumCountry" runat="server" ShowMessageBox="True" ShowSummary="False"
                                    ValidationGroup="ER" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnDHistory" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeDHistoru" runat="server" PopupControlID="pnlDocHistory"
        TargetControlID="btnDHistory" Y="10" CancelControlID="btnDocClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlDocHistory" runat="server" CssClass="modalPopupnew" Style="display: none;
        background-color: White; width: 70%; height: 80%; padding: 10px">
        <div class="form-group row">
            <label class="col-9" style="color: White;">
                <h5 style="color: Black; margin-left: 0%; font-size: 18px">
                    History of Docu Sign</h5>
            </label>
            <div class="col-lg-3 text-right">
                <asp:Button ID="btnDocClose" runat="server" CssClass="btn btn-danger" Text="Close"
                    Style="padding: 3px 12px;" ToolTip="Close Popup"></asp:Button>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="gvDocHistory" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                    EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex +1 %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Effective From">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Bind("DocSignEffFrom")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Docu Sign Expiry ">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%#Bind("DocSignExpiry")%>' />
                                ( in Minutes )
                            </ItemTemplate>
                            <HeaderStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Updated By">
                            <ItemTemplate>
                                <asp:Label ID="lblUpdatedBy" runat="server" Text='<%#Bind("DocUpdatedBy")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Updated On">
                            <ItemTemplate>
                                <asp:Label ID="lblUpdatedON" runat="server" Text='<%#Bind("DocSignUpdatedOn")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="20%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="Button4" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeCFLYTHistory" runat="server" PopupControlID="pnlCFLYTHistory"
        TargetControlID="Button4" Y="10" CancelControlID="Button5" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlCFLYTHistory" runat="server" CssClass="modalPopupnew" Style="display: none;
        background-color: White; width: 70%; height: 80%; padding: 10px">
        <div class="form-group row">
            <label class="col-lg-12" style="color: White;">
                <h5 style="color: Black; margin-left: 0%; font-size: 18px">
                    Custom FLYT History Details
                    <asp:Button ID="Button5" runat="server" CssClass="btn btn-danger" Text="Close" Style="padding: 3px 12px;
                        float: right;" ToolTip="Close Popup" /></h5>
            </label>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <asp:GridView ID="gvCustomFLYT" runat="server" ShowFooter="false" AutoGenerateColumns="False"
                    EmptyDataText="No Data Found" CssClass="table" HeaderStyle-CssClass="thead-dark"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex +1 %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Price Cap">
                            <ItemTemplate>
                                Custom_FLYT_Hours x (
                                <asp:Label ID="lblPriceCap1" runat="server" Font-Bold="true" Text='<%#Bind("PriceCap1")%>' />
                                x Custom_FLYT_Hours +
                                <asp:Label ID="lblPriceCap2" runat="server" Font-Bold="true" Text='<%#Bind("PriceCap2")%>' />
                                )
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Incremental Stops Price">
                            <ItemTemplate>
                                <asp:Label ID="lblIncStopsPrice" runat="server" Text='<%#Bind("IncStopsPrice")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="19%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Modified By">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" runat="server" Text='<%#Bind("UserName")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="14%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Modified On">
                            <ItemTemplate>
                                <asp:Label ID="lblModifiedOn" runat="server" Text='<%#Bind("ModifiedOn")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="18%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="btnalert" runat="server" Style="display: none" Text="Show Modal Popup" />
    <asp:ModalPopupExtender ID="mpealert" runat="server" TargetControlID="btnalert" PopupControlID="pnlalert"
        BackgroundCssClass="modalBackground" Y="80" CancelControlID="btncancelalert">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlalert" runat="server" ScrollBars="Auto" Style="display: none; background-color: rgb(17,24,32);
        padding: 10px">
        <table style="width: 100%; height: 100%; padding-left: 0px; border-right: 0px; padding-right: 0px"
            cellpadding="0" cellspacing="0">
            <tr style="border-right: 0px; border-left: 0px">
                <td style="color: rgb(255,255,255); font-size: 13px; vertical-align: middle" align="center">
                    <table style="width: 100%; text-align: left">
                        <tr>
                            <td>
                                <asp:Label ID="lblalert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:LinkButton ID="btncancelalert" runat="server" CssClass="btn btn-danger" Style="color: rgb(255,255,255);
                        text-decoration: none" CausesValidation="false" Text="Ok" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Button ID="btnAdEditExtUser" runat="server" Text="Button" Style="display: none" />
    <asp:ModalPopupExtender ID="mpeExtUser" runat="server" PopupControlID="pnlExtUser"
        TargetControlID="btnAdEditExtUser" Y="70" CancelControlID="btnCloseExtUser" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlExtUser" runat="server" CssClass="modalPopupnew" Style="display: none;
        background-color: rgb(17,24,32); width: 40%; padding: 10px">
        <div class="form-group row">
            <label class="col-6 col-form-label" style="color: White;">
                <h5 style="color: White; margin-left: 0%; font-size: 18px">
                    Add / Edit External User</h5>
            </label>
        </div>
        <div class="form-group row">
            <label class="col-3 col-form-label" style="color: White;">
                Name <span class="MandatoryField">* </span>
            </label>
            <div class="col-lg-9">
                <asp:TextBox ID="txtName" runat="server" MaxLength="100" ToolTip="Enter Alpha numeric characters and length is minited to 100."
                    TabIndex="1" Style="margin: 0; background-color: rgb(153,153,154); color: rgb(255,255,255);
                    border: 1px solid #ccc; padding: 10px; font-family: inherit; font-size: 16px;"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Rfv" runat="server" ControlToValidate="txtName" Display="None"
                    ErrorMessage="Name is required." SetFocusOnError="True" ValidationGroup="External">*</asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-3 col-form-label" style="color: White;">
                Email Id <span class="MandatoryField">* </span>
            </label>
            <div class="col-lg-9">
                <asp:TextBox ID="txtEmailId" runat="server" Style="margin: 0; background-color: rgb(153,153,154);
                    color: rgb(255,255,255); border: 1px solid #ccc; padding: 10px; font-family: inherit;
                    font-size: 16px;" MaxLength="100" TabIndex="2" ToolTip="Enter a valis Email Id and length is minited to 100."></asp:TextBox>
                <asp:RequiredFieldValidator ID="Rfv3" runat="server" ControlToValidate="txtEmailId"
                    Display="None" ErrorMessage="EmailId is required." SetFocusOnError="True" ValidationGroup="External">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailId"
                    Display="None" ErrorMessage="Please enter valid email address." ValidationGroup="External"
                    SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-3 col-form-label" style="color: White;">
                User Role
            </label>
            <div class="col-lg-9">
                <asp:TextBox ID="txtUserRole" runat="server" Style="margin: 0; background-color: rgb(153,153,154);
                    color: rgb(255,255,255); border: 1px solid #ccc; padding: 10px; font-family: inherit;
                    font-size: 16px;" MaxLength="100" TabIndex="3" ToolTip="Enter Alpha numeric characters and length is minited to 100."></asp:TextBox>
            </div>
        </div>
        <br />
        <div class="form-group row">
            <div class="col-lg-12 text-center">
                <asp:Button ID="btnSaveExtUser" runat="server" CssClass="btn btn-danger" Text="Save"
                    TabIndex="4" CausesValidation="true" ToolTip="click here to save external user"
                    OnClick="btnSaveExtUser_Click" ValidationGroup="External" />
                <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowMessageBox="True"
                    ShowSummary="False" ValidationGroup="External" />
                <asp:Button ID="btnClearExtUser" runat="server" CssClass="btn btn-danger" Text="Clear"
                    TabIndex="5" ToolTip="click here to clear" OnClick="btnClearExtUser_Click" />
                <asp:Button ID="btnCloseExtUser" runat="server" CssClass="btn btn-danger" TabIndex="6"
                    Text="Close" ToolTip="Click here to close popup." CausesValidation="false" />
            </div>
        </div>
    </asp:Panel>
    <asp:Button ID="Button1" runat="server" Text="FH Calculation" CssClass="btn btn-primary"
        TabIndex="1" OnClick="btnCalc_Click" Visible="false" />
    <asp:HiddenField ID="hdfPSRID" runat="server" />
    <asp:HiddenField ID="hdfCDRID" runat="server" />
    <asp:HiddenField ID="hdfTaxFlag" runat="server" />
    <asp:HiddenField ID="lblMessage" runat="server" />
    <asp:Label ID="lblrowtodelete" runat="server" Visible="false" />
    <asp:Label ID="lblUser" Text="UserName" runat="server" Visible="false" />
    <asp:Label ID="lblForeFLightID" Text="UserName" runat="server" Visible="false" />
    <asp:Label ID="lblcontact" Text="Contact" runat="server" Visible="false" />
    <asp:Label ID="lblUserEmail" Text="Email" runat="server" Visible="false" />
    <asp:Label ID="lblExtUserId" runat="server" Visible="false" />
    <asp:Label ID="lblCrewDayId" runat="server" Visible="false" Text="0" />
</asp:Content>
