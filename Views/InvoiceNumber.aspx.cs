﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class Views_InvoiceNumber : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.InvoiceNumberBLL objMember = new BusinessLayer.InvoiceNumberBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here
            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Invoice # Generation");
            }
           

            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

            lblMainHeader.InnerText = "General Setup";
            lblSubHeader.InnerText = "Invoice Number Generation";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            DataTable dttail1 = objMember.InvoiceNumber();
            if (dttail1.Rows.Count > 0)
            {
                lblinvoice.Text = dttail1.Rows[0]["invoicenumber"].ToString();
            }
            else
            {
                lblinvoice.Text = "";
            }

            List();
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
        this.Load += new System.EventHandler(this.Page_Load);
    }

    #endregion

    #region Button Events

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        int retVal = 0;
        int max = 0;
        string strActive = string.Empty;
        string strenabletrip = string.Empty;
        string strapplytax = string.Empty;
        string invoicenumber = txtTailFilter.Text + '-' + txtprocess.Text + '-' + txtcategory.Text + '-' + txtNumber.Text;
        string invoicenumber2 = txtTailFilter.Text + '-' + txtprocess.Text + '-' + txtcategory.Text;

        DataTable dttail2 = objMember.BookingInNumberExist(invoicenumber);
        if (dttail2.Rows.Count > 0)
        {
            lblalert.Text = "Invoice Number  already exist";
            mpealert.Show();
            return;
        }

        DataTable dttail3 = objMember.BookingDetails(invoicenumber);
        if (dttail3.Rows.Count >= 2)
        {
            for (int i = 0; i < dttail3.Rows.Count - 1; i++)
            {
                string maxinvoice1 = dttail3.Rows[i]["InvoiceNumber"].ToString();
                string maxinvoice2 = dttail3.Rows[i + 1]["InvoiceNumber"].ToString();
                String[] strlist1 = maxinvoice1.Split('-');
                String[] strlist2 = maxinvoice1.Split('-');
                int max1 = Convert.ToInt32(strlist1[3]);
                int max2 = Convert.ToInt32(strlist2[3]);

                if (max2 > max)
                {
                    max = max1;
                }
            }
            if (max > Convert.ToInt32(txtNumber.Text))
            {
                lblalert.Text = "Invoice Number  should not be less than already existed invoice.";
                mpealert.Show();
                return;
            }
        }
        else if (dttail3.Rows.Count == 1)
        {
            string maxinvoice1 = dttail3.Rows[0]["InvoiceNumber"].ToString();
            String[] strlist1 = maxinvoice1.Split('-');
            int max1 = Convert.ToInt32(strlist1[3]);
            if (max1 > Convert.ToInt32(txtNumber.Text))
            {
                lblalert.Text = "Invoice Number  should not be less than already existed invoice.";
                mpealert.Show();
                return;
            }
        }

        DataTable dttail = objMember.InvoiceExist(txtNumber.Text, txtTailFilter.Text, txtprocess.Text, txtcategory.Text);
        if (dttail.Rows.Count > 0)
        {
            txtTailFilter.Text = string.Empty;
            lblalert.Text = "Invoice Number  already exist";
            mpealert.Show();
            return;
        }
        objMember.ManageType = "I";
        objMember.InvoiceNumbert = txtNumber.Text;
        objMember.ProjectShortCode = txtTailFilter.Text;
        objMember.ProcessShortCode = txtprocess.Text.Trim();
        objMember.Category = txtcategory.Text.Trim();
        objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserID"].ToString();
        objMember.AutoIncrement = txtNumber.Text;
        objMember.Save(objMember);

        if (retVal > 0)
        {
            lblalert.Text = "Invoice Number Details have been saved successfully";
            mpealert.Show();
        }
        else
        {
            lblalert.Text = "Invoice Number Details have been saved successfully";
            mpealert.Show();
        }

        DataTable dttail1 = objMember.InvoiceNumber();
        if (dttail1.Rows.Count > 0)
        {
            lblinvoice.Text = dttail1.Rows[0]["invoicenumber"].ToString();
        }
        else
        {
            lblinvoice.Text = "";
        }

        List();

        Clear();
    }

    private void btnClear_Click(object sender, System.EventArgs e)
    {
        Clear();
    }

    #endregion

    #region  User Defined Function

    public void List()
    {
        DataTable dt = objMember.Select();
        if (dt.Rows.Count > 0)
        {
            gvinvoice.DataSource = dt;
            gvinvoice.DataBind();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["createdon"].ToString().Length > 0)
                {
                    if (Convert.ToDateTime(dt.Rows[i]["createdon"].ToString()).ToString("yyyy-MM-dd") == Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd"))
                    {
                        gvinvoice.Rows[i].Cells[6].ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                        gvinvoice.Rows[i].Cells[6].Font.Bold = true;
                    }
                    else
                    {
                        gvinvoice.Rows[i].Cells[6].ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                }
            }
        }
        else
        {
            gvinvoice.DataSource = null;
            gvinvoice.DataBind();
        }
    }

    public void Clear()
    {
        spinvoice.Text = string.Empty;
        txtTailFilter.Text = string.Empty;
        txtprocess.Text = string.Empty;
        txtcategory.Text = string.Empty;
        txtNumber.Text = string.Empty;
    }

    #endregion
}