﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;
using System.Configuration;
using System.Reflection;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Services;

public partial class Views_Payment : System.Web.UI.Page
{
    public int linenum = 0;
    public string MethodName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Reports", "Booking Report");
                }


                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "Home";
                lblSubHeader.InnerText = "Payment";

                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();


                BindGrid();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    public void BindGrid()
    {
        DataTable dt = GetTripDetails("S", ddlPayStatus.SelectedValue.Trim(), txtTripRefNumber.Text);

        if (dt.Rows.Count > 0)
        {
            gvTripDetail.DataSource = dt;
            gvTripDetail.DataBind();

            for (int i = 0; i < gvTripDetail.Rows.Count; i++)
            {
                Label lblStatu = (Label)gvTripDetail.Rows[i].FindControl("lblPaymentStatus");

                if (lblStatu.Text == "Pending")
                {
                    lblStatu.ForeColor = System.Drawing.Color.Red;
                }
                else if (lblStatu.Text == "Approval Pending")
                {
                    lblStatu.ForeColor = System.Drawing.Color.Orange;
                }
                else if (lblStatu.Text == "Paid")
                {
                    lblStatu.ForeColor = System.Drawing.Color.Green;
                }
            }
        }
        else
        {
            gvTripDetail.DataSource = null;
            gvTripDetail.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void gvTripDetail_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblTripRefNumber.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblrowid")).Text;

            lblEAmount.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lbltime")).Text;

            lblEBookedFor.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblBookedFor")).Text;

            lblBokkPlaced.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblBookedDate")).Text;

            lblDeparture.Text = ((Label)gvTripDetail.SelectedRow.FindControl("Label3")).Text + " ( " + ((Label)gvTripDetail.SelectedRow.FindControl("Label2")).Text + " ) ";

            lblArrival.Text = ((Label)gvTripDetail.SelectedRow.FindControl("Label3w")).Text + " ( " + ((Label)gvTripDetail.SelectedRow.FindControl("lblArrival")).Text + " ) ";

            lblCustNotes.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblPaidNotes")).Text;

            lblEditValueId.Value = ((Label)gvTripDetail.SelectedRow.FindControl("lblMainId")).Text;

            txtTransactionId.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblTransactionId")).Text;

            txtAmount.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblPaidAmount")).Text;

            txtTransactionDate.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblTransactinDate")).Text;

            txtNotes.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblAdminNotes")).Text;


            if (((Label)gvTripDetail.SelectedRow.FindControl("lblApprovedBy")).Text.Length > 2 && ((Label)gvTripDetail.SelectedRow.FindControl("lblAppon")).Text.Length > 2)
            {
                divLMBy.Visible = true;

                lblLMby.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblApprovedBy")).Text + " ( on ) " + ((Label)gvTripDetail.SelectedRow.FindControl("lblAppon")).Text;
            }
            else
            {
                divLMBy.Visible = false;
            }

            if (((Label)gvTripDetail.SelectedRow.FindControl("lblCustUpdatedBy")).Text.Length > 2 && ((Label)gvTripDetail.SelectedRow.FindControl("lblCustUpdatedOn")).Text.Length > 2)
            {
                dvCusUpdatedBy.Visible = true;

                lblCLMB.Text = ((Label)gvTripDetail.SelectedRow.FindControl("lblCustUpdatedBy")).Text + " ( on ) " + ((Label)gvTripDetail.SelectedRow.FindControl("lblCustUpdatedOn")).Text;
            }
            else
            {
                dvCusUpdatedBy.Visible = false;
            }

            mpePayStatus.Show();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void btnUpdate_Status(object sender, EventArgs e)
    {
        try
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "UPDATE BookedDetails SET TransId='" + txtTransactionId.Text + "',PaidTransactionDate='" + Convert.ToDateTime(txtTransactionDate.Text) + "',PaidTransactionAmount='" + txtAmount.Text + "',PaidStatus='" + rblPayRece.SelectedValue.Trim() + "',AdminNotes='" + txtNotes.Text + "',PaymentApprovedBy='" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "',PaymentApprovedOn='" + DateTime.Now.ToString() + "' WHERE ROWID='" + lblEditValueId.Value.Trim() + "'");

            lblalert.Text = "Payment Details have been updated successfully.";

            mpealert.Show();

            BindGrid();
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public static DataTable GetTripDetails(string strManageType, string strStatus, string strTripRefNo)
    {
        SqlParameter[] getAllTripParm = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_PAYMENT_SEL]", false);
        getAllTripParm[0].Value = strManageType;
        getAllTripParm[1].Value = strStatus;
        getAllTripParm[2].Value = strTripRefNo;
        return SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.StoredProcedure, "[SP_PAYMENT_SEL]", getAllTripParm).Tables[0];
    }
}