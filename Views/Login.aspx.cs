﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.Data;
using System.Xml;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Configuration;
using System.Dynamic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Collections.Specialized;
using System.Globalization;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Security.Cryptography;
//using iTextSharp.text.pdf;
//using iTextSharp.text.html.simpleparser;
//using iTextSharp.text;
using System.Net.Mail;
using System.Web;
using System.Globalization;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Net.Mime;
using BusinessLayer;

public partial class Views_Login : System.Web.UI.Page
{
    //HttpCookie ETuserCookie = new HttpCookie("ETUser");
    //HttpCookie ETJETJETTransactionidCookie = new HttpCookie("ETJETTransactionid");
    HttpCookie EMPTYTRIPADMINTRANSID = new HttpCookie("EMPTYTRIPADMINTRANSID");
    CreateAdminBLL objCreateAdminBLL = new CreateAdminBLL();
    //HttpCookie JETIDFORALERT = new HttpCookie("JETIDFORALERT");
    //HttpCookie JETRIPSPAY = new HttpCookie("JETRIPSPAY");
    //HttpCookie JETRIPSPAYTIME = new HttpCookie("JETRIPSPAYTIME");
    //HttpCookie JETRIPSPAYAir = new HttpCookie("JETRIPSPAYAir");

    string userInfo = string.Empty;
    string autoGen = string.Empty;

    string strUserID = string.Empty;
    string userRole = string.Empty;

    public int linenum = 0;
    public string MethodName = string.Empty;

    public enum UserInfo { Incorrect, Invalid, LoggedIn, Locked, LogOutProblem }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if ((Request.QueryString["UName"] != null && Request.QueryString["UName"] != "" && Request.QueryString["UName"] != string.Empty))
            {
                RedirecttoAdmin(Request.QueryString["UName"], "L", "", "");
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
    private void RedirecttoAdmin(string strUserName, string strFrom, string strTo, string TransId)
    {
        try
        {
            DataTable dt = objCreateAdminBLL.Select_User_Login(strUserName, "SEL"); 
            HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
            Response.Cookies.Remove("JETRIPSADMIN");
            Response.Cookies.Add(JETRIPSADMIN);
            JETRIPSADMIN.Values.Add("Username", dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
            JETRIPSADMIN.Values.Add("FirstName", dt.Rows[0]["FirstName"].ToString());
            JETRIPSADMIN.Values.Add("LastName", dt.Rows[0]["LastName"].ToString());
            JETRIPSADMIN.Values.Add("Title", dt.Rows[0]["Title"].ToString());
            JETRIPSADMIN.Values.Add("Email", dt.Rows[0]["Email"].ToString());
            JETRIPSADMIN.Values.Add("PhoneNumber", dt.Rows[0]["PhoneNumber"].ToString());
            JETRIPSADMIN.Values.Add("Phonecode", dt.Rows[0]["Code_Phone"].ToString());
            JETRIPSADMIN.Values.Add("Address", dt.Rows[0]["Address"].ToString());
            JETRIPSADMIN.Values.Add("Country", dt.Rows[0]["Country"].ToString());
            JETRIPSADMIN.Values.Add("City", dt.Rows[0]["City"].ToString());
            JETRIPSADMIN.Values.Add("ZipCode", dt.Rows[0]["ZipCode"].ToString());
            JETRIPSADMIN.Values.Add("Role", dt.Rows[0]["Role"].ToString());
            JETRIPSADMIN.Values.Add("UserId", dt.Rows[0]["RowId"].ToString());
            JETRIPSADMIN.Values.Add("brokerflag", dt.Rows[0]["UserbrokerfLAG"].ToString());

            Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);

            //changed by gomathy
            NameValueCollection ipAddress = Request.ServerVariables;
            EMPTYTRIPADMINTRANSID.Values.Add("TrasId", UserLoggedIn(dt.Rows[0]["RowId"].ToString(), ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value));
            Response.Cookies.Add(EMPTYTRIPADMINTRANSID);

            FormsAuthentication.RedirectFromLoginPage(txtLoginEmail.Value, false);
            Response.Redirect("PriceCharter.aspx", false);
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void OpenLogin(object sender, EventArgs e)
    {
        // mpelogout.Show();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
        if (txtLoginEmail.Value.Length > 0 && txtPassword.Value.Length > 0)
        {
            btnLogin.Focus();
        }
        else
        {
            txtLoginEmail.Focus();
        }
    }

    protected void logged_Click(object sender, EventArgs e)
    {

        LoginDirect();

    }
    protected void btncloseLogin(object sender, EventArgs e)
    {
        if (BookedFlag.Text == "B")
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Update   Booking_Log set Bookflag='0'  where  tailno='" + Request.Cookies["JETRIPSPAY"]["tailno"].ToString() + "' and tripno='" + Request.Cookies["JETRIPSPAY"]["Tripno"].ToString() + "' and [Date]='" + Convert.ToDateTime(Request.Cookies["JETRIPSPAY"]["BDate"].ToString()) + "' and Starttime='" + Request.Cookies["JETRIPSPAY"]["starttime"].ToString() + "' and Endtime='" + Request.Cookies["JETRIPSPAY"]["Endtime"].ToString() + "' and [Start]='" + Request.Cookies["JETRIPSPAY"]["start"].ToString() + "' and [End]='" + Request.Cookies["JETRIPSPAY"]["End"].ToString() + "' and AlerId ='" + Request.Cookies["JETIDFORALERT"]["AlertId"].ToString() + "' ");

        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Close();", true);
    }

    protected void ShowCreateUserPopup(object sender, EventArgs e)
    {
        mpesignup.Show();
    }
    protected void lnkcookesshow_click(object sender, EventArgs e)
    {
        //framecookies.Attributes.Add("src", "Cookies.aspx");
        mpefrmaecookie.Show();
        mpecookie.Show();
    }
    protected void btnOTP_click(object sender, EventArgs e)
    {
        string strBusinessAlert = string.Empty;
        if (txtotp.Value.ToString().Length == 0)
        {
            strBusinessAlert += "- Token is required. <br/>";

        }

        if (strBusinessAlert.Length > 0)
        {

            lblalert.Text = strBusinessAlert;
            mpealert.Show();
            mpeotp.Show();
            //  ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
            return;
        }
        if (txtotp.Value.Length > 0)
        {
            DataTable dtuser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];
            if (dtuser.Rows.Count > 0)
            {
                string OTP = dtuser.Rows[0]["OTP"].ToString();
                DateTime date1 = Convert.ToDateTime(dtuser.Rows[0]["OTPTime"].ToString());
                DateTime date2 = DateTime.Now;
                TimeSpan ts = date2 - date1;
                if (txtotp.Value == OTP)
                {
                    if (Convert.ToInt32(ts.TotalMinutes) > 30)
                    {
                        lblOTPalert.Visible = true;
                        lblOTPalert.Text = "Your token has  expired.Please click Resend Token to get new one";
                        alertOTp.Visible = true;
                        mpeotp.Show();
                    }
                    else
                    {
                        string Query = "Update UserMasterCreate Set OTPflag='1'  where  Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'";
                        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, Query);
                        mpecookie.Show();
                    }
                }
                else
                {
                    //   ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Please enter valid token has send to your email')", true);
                    lblalert.Text = "Token is invalid – please try again.";
                    txtotp.Value = string.Empty;
                    mpealert.Show();

                    mpeotp.Show();
                }
            }
        }
    }
    protected void lnkReject_Onclick(object sender, EventArgs e)
    {
        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Usecookies='0' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");
        //  Response.Redirect("simplesearchBooking.aspx");
        // ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
        mpecookie.Hide();
        if (loggedflag.Text == "L" && BookedFlag.Text == "B")
        {

            mpetime.Show();
            ddlTimeFrom.Focus();
            //tdHeader.Visible = true;
            //tdHeader1.Visible = true;
            //tdHeader2.Visible = true;
            //tdHeader4.Visible = true;
            //tdHeader3.Visible = false;
        }
        else
        {

            Response.Redirect("SimpleSearchBooking.aspx?Flag=L");

        }
    }
    protected void lnkAllow_Onclick(object sender, EventArgs e)
    {
        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Usecookies='1' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
        mpecookie.Hide();
        if (loggedflag.Text == "L" && BookedFlag.Text == "B")
        {

            mpetime.Show();
            ddlTimeFrom.Focus();
            //tdHeader.Visible = true;
            //tdHeader1.Visible = true;
            //tdHeader2.Visible = true;
            //tdHeader4.Visible = true;
            //tdHeader3.Visible = false;
        }
        else
        {

            Response.Redirect("SimpleSearchBooking.aspx?Flag=L");

        }
        // LoginDirectSecond();

        //Response.Redirect("simplesearchBooking.aspx");
    }
    void LoginDirect()
    {
        try
        {
            string strBusinessAlert = string.Empty;
            if (txtLoginEmail.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- E-mail <br/>";
            }
            if (txtPassword.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- Password <br/>";
            }

            if (strBusinessAlert.Length > 0)
            {
                lblalert.Text = "- The below required items are missing:<br/><br/>" + strBusinessAlert;
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
                return;
            }
            userInfo = AuthenticateUser(txtLoginEmail.Value, txtPassword.Value);
            if (userInfo == UserInfo.Incorrect.ToString()) // Login failed...
            {
                lblalert.Text = "Incorrect Password";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
                return;
            }
            else if (userInfo == UserInfo.Invalid.ToString()) // Login failed...
            {
                lblalert.Text = "Invalid username or password.Please try again.";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
                return;
            }
            else if (userInfo == "ResetOTP")
            {
                lblforgot.Text = "Entered token has been expired.Please click 'Forgot Password?' to get new token";
                lblforgot.Attributes.Add("style", "display:'block'");
                lblforgot.ForeColor = System.Drawing.ColorTranslator.FromHtml("	#FFFFFF");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
                return;
            }


            else // Login success...
            {
                string strErr = "";
                string strval = string.Empty;

                DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * ,isnull(BrokerFlag,'N') as UserbrokerfLAG  from UserMasterCreate where Email='" + txtLoginEmail.Value + "'").Tables[0];
                HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
                Response.Cookies.Remove("JETRIPSADMIN");
                Response.Cookies.Add(JETRIPSADMIN);
                JETRIPSADMIN.Values.Add("Username", dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
                JETRIPSADMIN.Values.Add("FirstName", dt.Rows[0]["FirstName"].ToString());
                JETRIPSADMIN.Values.Add("LastName", dt.Rows[0]["LastName"].ToString());
                JETRIPSADMIN.Values.Add("Title", dt.Rows[0]["Title"].ToString());
                JETRIPSADMIN.Values.Add("Email", dt.Rows[0]["Email"].ToString());
                JETRIPSADMIN.Values.Add("PhoneNumber", dt.Rows[0]["PhoneNumber"].ToString());
                JETRIPSADMIN.Values.Add("Phonecode", dt.Rows[0]["Code_Phone"].ToString());
                JETRIPSADMIN.Values.Add("Address", dt.Rows[0]["Address"].ToString());
                JETRIPSADMIN.Values.Add("Country", dt.Rows[0]["Country"].ToString());
                JETRIPSADMIN.Values.Add("City", dt.Rows[0]["City"].ToString());
                JETRIPSADMIN.Values.Add("ZipCode", dt.Rows[0]["ZipCode"].ToString());
                JETRIPSADMIN.Values.Add("Role", dt.Rows[0]["Role"].ToString());
                JETRIPSADMIN.Values.Add("UserId", dt.Rows[0]["RowId"].ToString());
                JETRIPSADMIN.Values.Add("brokerflag", dt.Rows[0]["UserbrokerfLAG"].ToString());

                Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);

                //changed by gomathy
                NameValueCollection ipAddress = Request.ServerVariables;
                //JETJETTransactionidCookie.Values.Add("TrasId", UserLoggedIn(txtUserName.Value.Replace("'", "''").Trim(), ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value, dtUser.Rows[0]["CustomerID"].ToString()));
                EMPTYTRIPADMINTRANSID.Values.Add("TrasId", UserLoggedIn(dt.Rows[0]["RowId"].ToString(), ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value));
                Response.Cookies.Add(EMPTYTRIPADMINTRANSID);

                //changed by gomathy

            }

            if (txtLoginEmail.Value.Length > 0)
            {
                if (txtLoginEmail.Value != "guest@demo.com")
                {
                    DataTable dtUserRole = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtLoginEmail.Value + "'").Tables[0];
                    if (dtUserRole.Rows.Count > 0)
                    {
                        if (dtUserRole.Rows[0]["role"].ToString().ToLower() == "admin")
                        {
                            if (dtUserRole.Rows[0]["MustChangePassword"].ToString() != "1")
                            {
                                Response.Redirect("Pricecharter.aspx");
                            }
                            else
                            {
                                mpechange.Show();
                            }
                        }
                        else
                        {
                            DataTable dtUserotp = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select OTPflag,email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtLoginEmail.Value + "'").Tables[0];
                            if (dtUserotp.Rows[0]["OTPflag"].ToString() == "0")
                            {
                                mpeotp.Show();
                                return;
                            }

                            DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtLoginEmail.Value + "'").Tables[0];
                            if (dtUser.Rows[0]["MustChangePassword"].ToString() != "1")
                            {
                                if (dtUser.Rows.Count > 0)
                                {
                                    if (dtUser.Rows[0]["role"].ToString().ToLower() == "admin")
                                    {
                                        Response.Redirect("Pricecharter.aspx");
                                    }
                                    else
                                    {
                                        loggedflag.Text = "L";
                                        //lblLoggedin.Text = "Logged in as " + Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                                        if (dtUser.Rows[0]["Usecookies"].ToString() == "1")
                                        {
                                            if (loggedflag.Text == "L" && BookedFlag.Text == "B")
                                            {
                                                mpetime.Show();
                                                ddlTimeFrom.Focus();
                                                //tdHeader.Visible = true;
                                                //tdHeader1.Visible = true;
                                                //tdHeader2.Visible = true;
                                                //tdHeader4.Visible = true;
                                                //tdHeader3.Visible = false;
                                            }
                                            else
                                            {
                                                Response.Redirect("SimpleSearchBooking.aspx?Flag=L");
                                            }
                                        }
                                        else
                                        {
                                            mpecookie.Show();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                mpechange.Show();
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public string AuthenticateUser(string userName, string password)
    {
        string retMessage = string.Empty;
        DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + userName.Trim() + "'").Tables[0];
        if (dtUser.Rows.Count > 0)
        {
            string pass1 = genclass.DecryptData(dtUser.Rows[0]["Password"].ToString());

            if (pass1 == password)
            {
                if (dtUser.Rows[0]["MustChangePassword"].ToString() == "1")
                {
                    if (dtUser.Rows[0]["role"].ToString().ToLower() != "admin")
                    {
                        if (dtUser.Rows[0]["userflag"].ToString().ToUpper() != "ADMINUSER")
                        {
                            DateTime date1 = Convert.ToDateTime(dtUser.Rows[0]["ForgotPassTime"].ToString());
                            DateTime date2 = DateTime.Now;
                            TimeSpan ts = date2 - date1;

                            if (Convert.ToInt32(ts.TotalMinutes) > 30)
                            {

                                // return= string.Empty;
                                retMessage = "ResetOTP";
                            }
                            else
                            {
                                retMessage = string.Empty;

                            }

                        }
                        else
                        {
                            if (dtUser.Rows[0]["ForgotPassTime"].ToString().Length > 0)
                            {
                                DateTime date1 = Convert.ToDateTime(dtUser.Rows[0]["ForgotPassTime"].ToString());
                                DateTime date2 = DateTime.Now;
                                TimeSpan ts = date2 - date1;

                                if (Convert.ToInt32(ts.TotalMinutes) > 30)
                                {

                                    // return= string.Empty;
                                    retMessage = "ResetOTP";
                                }
                                else
                                {
                                    retMessage = string.Empty;

                                }

                            }
                            else
                            {
                                retMessage = "Changepassword";
                            }
                        }

                    }
                    else
                    {
                        retMessage = string.Empty;

                    }
                }
                else
                {
                    retMessage = string.Empty;
                }

            }
            else
            {
                retMessage = UserInfo.Incorrect.ToString();
            }



        }
        else
            retMessage = UserInfo.Invalid.ToString();

        //}

        return retMessage;
    }
    public static string UserLoggedIn(string userName, string ipAddress, string BName, string BVersion, string userInfo, string PBXType)
    {
        string TRansID = string.Empty;

        string CurrentDate = string.Empty;
        CurrentDate = System.DateTime.Now.ToString();

        string Query = "Update UserMasterCreate Set LastLoginIP='" + ipAddress + "'  where  Rowid='" + userName + "'";
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, Query);

        string strQuery = "insert into Logaudit(Userid,loggedin,loggedout,IPAddress,LoginDate,[Loggedout(sec)],logoutReason,[browserName],[browserVersion]) values('" + userName + "', GETDATE(), NULL, '" + ipAddress + "','" + DateTime.Now.Date + "','0',NULL, '" + BName + "', '" + BVersion + "'); SELECT SCOPE_IDENTITY() ;";
        TRansID = Convert.ToString(SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, strQuery));

        DataTable dtInfo = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select top 1 rowid,loggedout from Logaudit where Userid='" + userName + "' " +
                           " and CONVERT(char,loggedin,101)=CONVERT(char,GETDATE(),101) and isnull(loggedout,'')<>'' and rowid <'" + TRansID + "' order by loggedin desc ").Tables[0];
        if (dtInfo.Rows.Count > 0)
        {
            if (dtInfo.Rows[0]["rowid"].ToString() != TRansID)
            {
                strQuery = "Update Logaudit Set [Loggedout(sec)]= DATEDIFF(ss,'" + dtInfo.Rows[0]["loggedout"].ToString() + "','" + CurrentDate + "') where  Logaudit.Userid='" + userName + "' and Logaudit.rowid='" + dtInfo.Rows[0]["rowid"].ToString() + "'";
                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, strQuery);
            }
        }
        return TRansID;
    }
    protected void btnforgetPassOnClick(object sender, EventArgs e)
    {
        try
        {
            string autoGenPwd = string.Empty;
            autoGenPwd = genclass.RandomStringNumbers(6);
            DataTable dt1 = new DataTable();
            string strName = string.Empty;
            if (txtLoginEmail.Value.Length > 0)
            {
                dt1 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtLoginEmail.Value + "'").Tables[0];
                if (dt1.Rows.Count > 0)
                {
                    strName = dt1.Rows[0]["firstName"].ToString() + " " + dt1.Rows[0]["LastName"].ToString();

                    SendEmailForget(txtLoginEmail.Value, autoGenPwd, strName);

                    SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Password ='" + genclass.Encrypt(autoGenPwd) + "',MustChangePassword='1',ForgotPassTime='" + DateTime.Now + "' Where Email='" + txtLoginEmail.Value + "'");

                    DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtLoginEmail.Value + "'").Tables[0];
                    HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
                    Response.Cookies.Remove("JETRIPSADMIN");
                    Response.Cookies.Add(JETRIPSADMIN);
                    JETRIPSADMIN.Values.Add("Username", dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
                    JETRIPSADMIN.Values.Add("FirstName", dt.Rows[0]["FirstName"].ToString());
                    JETRIPSADMIN.Values.Add("LastName", dt.Rows[0]["LastName"].ToString());
                    JETRIPSADMIN.Values.Add("Title", dt.Rows[0]["Title"].ToString());
                    JETRIPSADMIN.Values.Add("Email", dt.Rows[0]["Email"].ToString());
                    JETRIPSADMIN.Values.Add("PhoneNumber", dt.Rows[0]["PhoneNumber"].ToString());
                    JETRIPSADMIN.Values.Add("Address", dt.Rows[0]["Address"].ToString());
                    JETRIPSADMIN.Values.Add("Country", dt.Rows[0]["Country"].ToString());
                    JETRIPSADMIN.Values.Add("City", dt.Rows[0]["City"].ToString());
                    JETRIPSADMIN.Values.Add("ZipCode", dt.Rows[0]["ZipCode"].ToString());
                    JETRIPSADMIN.Values.Add("Role", dt.Rows[0]["Role"].ToString());
                    JETRIPSADMIN.Values.Add("UserId", dt.Rows[0]["RowId"].ToString());

                    Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);
                    mpethankForget.Show();
                    return;
                }
                else
                {
                    lblforgot.Text = "Not a registered user!";
                    lblforgot.Visible = true;
                }
            }
            else
            {
                lblalert.Text = "- The below required items are missing:<br/><br/>" + "- Email";
                mpealert.Show();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Openforgot();", true);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "')", true);
        }
    }
    protected void btnOkThankForget_onclick(object sender, EventArgs e)
    {
        if (loggedflag.Text == "L" && BookedFlag.Text == "B")
        {
            mpeotpforget.Show();
        }
        else
        {
            mpeotpforget.Show();
        }
    }
    protected void btnforgetPassOnClick_newuser(object sender, EventArgs e)
    {
        try
        {
            string autoGenPwd = string.Empty;
            autoGenPwd = genclass.RandomString(genclass.RandomNumber(8, 15));
            DataTable dt = new DataTable();
            string strName = string.Empty;
            dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtemail1.Value + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                strName = dt.Rows[0]["firstName"].ToString();
                SendEmailForgetNew(txtemail1.Value, autoGenPwd, strName);
                SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Password ='" + genclass.Encrypt(autoGenPwd) + "',MustChangePassword='1' Where Email='" + txtemail1.Value + "'");
                lblforgotnew.Text = "We will send a token to your email to reset your password";

                lblforgotnew.Visible = true;
                mpelogin.Show();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "')", true);
        }
    }
    protected void btnresendotpForget_Click(object sender, EventArgs e)
    {
        try
        {
            string autoGenPwd = string.Empty;
            autoGenPwd = genclass.RandomStringNumbers(6);
            DataTable dt = new DataTable();
            string strName = string.Empty;
            if (txtLoginEmail.Value.Length > 0)
            {


                dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtLoginEmail.Value + "'").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    strName = dt.Rows[0]["firstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                    //  SendEmail_forgotPassword(txtLoginEmail.Value, strName, autoGenPwd);
                    SendEmailForget(txtLoginEmail.Value, autoGenPwd, strName);
                    SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Password ='" + genclass.Encrypt(autoGenPwd) + "',MustChangePassword='1',ForgotPassTime='" + DateTime.Now + "' Where Email='" + txtLoginEmail.Value + "'");
                    //lblforgot.Text = "We will send a token to your email <br/> to reset your password";
                    //lblforgot.Visible = true;
                    lblalert.Text = "Token has been resent to your registered email address.";
                    mpealert.Show();
                    mpeotpforget.Show();
                    return;
                }
                else
                {
                    lblforgot.Text = "Not a registered user!";
                    lblforgot.Visible = true;
                }
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Openforgot();", true);
            }


        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "')", true);
        }
    }
    protected void btnlogin2_click(object sender, EventArgs e)
    {
        LoginDirectSecond();
    }
    protected void btnresendotp_Click(object sender, EventArgs e)
    {

        string autoGenPwd = string.Empty;
        autoGenPwd = genclass.RandomStringNumbers(6);

        DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];

        if (dtUser.Rows.Count > 0)
        {
            string username = dtUser.Rows[0]["firstname"].ToString() + " " + dtUser.Rows[0]["lastname"].ToString();
            string email = dtUser.Rows[0]["email"].ToString();
            string Query = "Update UserMasterCreate Set OTP='" + autoGenPwd + "',OTPflag='0',OTPTime='" + DateTime.Now + "'  where  email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'";
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, Query);

            //  SendEmail_OTPNew(email, username, autoGenPwd);
            SendEmail_logincreate(email, username, autoGenPwd);
            mpeotp.Show();
            lblOTPalert.Text = "Token has been resent to your registered email address.";
            alertOTp.Visible = true;
            //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Token has been resent to your registered Email Address.');", true);

        }


    }
    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        mpesignup.Hide();


        // ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "CloseSignup();", true);
    }
    protected void OpenTermsandConditions2(object sender, EventArgs e)
    {

        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from PDF_Master").Tables[0];

        string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"500px\">";
        embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        embed += "</object>";
        //  ltEmbed.Text = string.Format(embed, ResolveUrl("Uploads/Generate_Quote/Terms.pdf"));
        // ltterms2.Text = string.Format(embed, "https://www.flyjetedge.net/Zoom/Uploads/Generate_Quote/Terms.pdf");
        //iframeterms.Attributes.Add("src", "https://www.flyjetedge.net/Zoom/Uploads/Generate_Quote/Termsandcondition/Terms.pdf");
        mpeterms2.Show();


    }
    protected void btnCreateLoginOnClick(object sender, EventArgs e)
    {
        string[] CategoryDetails = new string[40];
        string strMsg = string.Empty;
        string strActive = string.Empty;
        string autoGenPwd = string.Empty;
        autoGenPwd = genclass.RandomStringNumbers(6);


        try
        {
            string strBusinessAlert = string.Empty;


            if (txtFirstName.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>- First Name <br/>";


            }
            if (txtLastName.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>- Last Name <br/>";


            }

            if (txtemail.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>- Email <br/>";
            }


            if (txtpassword1C.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>-  Password  <br/>";


            }
            if (txtpassword2C.Value.ToString().Length == 0)
            {
                strBusinessAlert += "</t>-  Confirm Password <br/>";


            }
            if (strBusinessAlert.Length > 0)
            {
                //  ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + strBusinessAlert + "')", true);
                lblalert.Text = "- The below required items are missing:<br/><br/>" + strBusinessAlert;
                mpealert.Show();
                mpesignup.Show();
                return;

            }
            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtemail.Value + "'").Tables[0];
            if (dt.Rows.Count > 0)
            {
                mpesignup.Show();
                lblalert.Text = txtemail.Value + " is already exist";
                mpealert.Show();
                //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + txtemail.Value + " is already exist');", true);
                //  ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "OpenSignUp();", true);
                return;
            }
            CategoryDetails[0] = "I";
            CategoryDetails[1] = "0";
            CategoryDetails[2] = "";
            CategoryDetails[3] = txtemail.Value;
            CategoryDetails[4] = txtphone.Value;
            CategoryDetails[5] = txtAddress.Value;
            CategoryDetails[6] = txtCity.Value;
            CategoryDetails[7] = txtZipcode.Value;
            CategoryDetails[8] = "User";
            CategoryDetails[9] = (genclass.Encrypt(txtpassword1C.Value));
            CategoryDetails[10] = txtFirstName.Value;
            CategoryDetails[11] = txtLastName.Value;
            CategoryDetails[12] = txtAddress2.Value;
            CategoryDetails[13] = txtstate.Value;
            CategoryDetails[14] = txtcountry.Value;
            CategoryDetails[15] = autoGenPwd;
            CategoryDetails[16] = "0";
            CategoryDetails[17] = txtPhoneCode.Value;


            //objUserBll.password = (genClass.Encrypt(autoGenPwd));


            string strReturnVal = saveCategory(CategoryDetails);
            DataTable dtCD = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT *  FROM  CouponDiscount ").Tables[0];
            if (dtCD.Rows.Count > 0)
            {
                for (int i = 0; i < dtCD.Rows.Count; i++)
                {
                    if (dtCD.Rows[i]["UserFlag"].ToString() == "A")
                    {
                        string couponcode = dtCD.Rows[i]["CouponCode"].ToString();
                        string users = dtCD.Rows[i]["CouponDescription"].ToString();
                        if (users.Length > 0)
                        {
                            users = users + "," + strReturnVal;

                        }
                        else
                        {
                            users = strReturnVal;

                        }
                        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update CouponDiscount set CouponDescription ='" + users + "' Where CouponCode='" + couponcode + "'");
                    }
                }
            }

            SendEmail_logincreate(txtemail.Value, txtFirstName.Value + " " + txtLastName.Value, autoGenPwd);

            //SendEmail_OTPNew(txtemail.Value, txtFirstName.Value + " " + txtLastName.Value, autoGenPwd);
            //  SendEmail(txtemail.Value, txtFirstName.Value + " " + txtLastName.Value);

            HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
            Response.Cookies.Remove("JETRIPSADMIN");
            Response.Cookies.Add(JETRIPSADMIN);
            JETRIPSADMIN.Values.Add("Username", txtFirstName.Value + " " + txtLastName.Value);
            JETRIPSADMIN.Values.Add("FirstName", txtFirstName.Value);
            JETRIPSADMIN.Values.Add("LastName", txtLastName.Value);
            JETRIPSADMIN.Values.Add("Email", txtemail.Value);
            JETRIPSADMIN.Values.Add("PhoneNumber", txtphone.Value);
            JETRIPSADMIN.Values.Add("Phonecode", txtPhoneCode.Value);
            JETRIPSADMIN.Values.Add("Address", txtAddress.Value);
            JETRIPSADMIN.Values.Add("Address2", txtAddress2.Value);
            JETRIPSADMIN.Values.Add("City", txtCity.Value);
            JETRIPSADMIN.Values.Add("ZipCode", txtZipcode.Value);
            JETRIPSADMIN.Values.Add("Role", "User");
            JETRIPSADMIN.Values.Add("UserId", strReturnVal);
            JETRIPSADMIN.Values.Add("Brokerflag", "N");
            // JECA2019.Values.Add("Phone", dt.Rows[0]["Phone"].ToString());
            //SqlHelper.ExecuteScalar(SqlHelper.CACConnectionString, CommandType.Text, "UPDATE UserAccountLog SET WrongAttempts = 0 WHERE UserName='" + exampleInputEmail1.Value + "'");
            // string transID = UserReportBLL.LogAuditInsertCA(exampleInputEmail1.Value.Replace("'", "''").Trim(), ipAddress[32].ToString(), System.DateTime.Now.ToString(), dt.Rows[0]["CustomerID"].ToString()).ToString();
            //JETJETTransactionidCookie.Values.Add("TrasId", transID);
            //Response.Cookies.Add(JETJETTransactionidCookie);
            Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);
            // lblLoggedin.Text = "Logged in as " + txtFirstName.Value + " " + txtLastName.Value;
            NameValueCollection ipAddress = Request.ServerVariables;
            //JETJETTransactionidCookie.Values.Add("TrasId", UserLoggedIn(txtUserName.Value.Replace("'", "''").Trim(), ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value, dtUser.Rows[0]["CustomerID"].ToString()));
            EMPTYTRIPADMINTRANSID.Values.Add("TrasId", UserLoggedIn(strReturnVal, ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value));
            Response.Cookies.Add(EMPTYTRIPADMINTRANSID);
            loggedflag.Text = "L";
            if (strReturnVal != "0")
            {
                mpesignup.Hide();
                mpethank.Show();
                // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('User details have been saved successfully ');", true);
                //  ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
            }

            //  Response.Redirect("SimpleSearchBooking.aspx?Flag=L");




        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    public string saveCategory(string[] arrCategory)
    {
        SqlParameter[] saveCategoryParam = SqlHelperParameterCache.GetSpParameterSet(SqlHelper.FLYTConnectionString, "[SP_UserMaster_INS]", false);
        // DataTable dtOneWayTrip = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "SELECT  OneWayTail_Setup.rowid,  OneWayTail_Setup.TailNo, TailName, StartAirport, StartCity, StartTime,EndTime,Flyinghours, EndAirport, EndCity  FROM  OneWayTail_Setup Inner join TailPricing_Configuration on TailPricing_Configuration.TailNo=OneWayTail_Setup.TailNo ").Tables[0];

        saveCategoryParam[0].Value = arrCategory[0];//MANAGETYPE
        saveCategoryParam[1].Value = arrCategory[1];//Rowid
        saveCategoryParam[2].Value = arrCategory[2];//Username
        saveCategoryParam[3].Value = arrCategory[10];//Role
        saveCategoryParam[4].Value = arrCategory[11];//Role
        saveCategoryParam[6].Value = arrCategory[3];//Email
        saveCategoryParam[7].Value = arrCategory[4];//Phone Number
        saveCategoryParam[8].Value = arrCategory[5];//Address
        saveCategoryParam[10].Value = arrCategory[6];//Country
        saveCategoryParam[11].Value = arrCategory[7];//Zipcode
        saveCategoryParam[12].Value = arrCategory[8];//Role
        saveCategoryParam[13].Value = arrCategory[9];//Role
        saveCategoryParam[16].Value = "0";//Role
        saveCategoryParam[17].Value = arrCategory[12];//Address2
        saveCategoryParam[15].Value = arrCategory[13];//Address2
        saveCategoryParam[9].Value = arrCategory[14];//Address2
        saveCategoryParam[18].Value = "0";//Address2
        saveCategoryParam[20].Value = arrCategory[15];//Address2
        saveCategoryParam[21].Value = arrCategory[16];//Address2
        saveCategoryParam[23].Value = arrCategory[17];//Address2



        object obj = SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, "[SP_UserMaster_INS]", saveCategoryParam).ToString();
        return obj.ToString();
    }
    protected void btnOTP_clickForget(object sender, EventArgs e)
    {
        string strBusinessAlert = string.Empty;
        if (txtotpForget.Value.ToString().Length == 0)
        {
            strBusinessAlert += "- Token is required. <br/>";
        }

        if (strBusinessAlert.Length > 0)
        {
            lblalert.Text = strBusinessAlert;
            mpealert.Show();
            mpeotpforget.Show();
            return;
        }
        userInfo = AuthenticateUser(txtLoginEmail.Value, txtotpForget.Value);
        if (userInfo == UserInfo.Incorrect.ToString()) // Login failed...
        {
            lblalert.Text = "Token is invalid – please try again.";
            mpeotpforget.Show();
            mpealert.Show();
            return;
        }
        else if (userInfo == UserInfo.Invalid.ToString()) // Login failed...
        {
            lblalert.Text = "Invalid username or password";
            mpeotpforget.Show();
            mpealert.Show();
            return;
        }
        else if (userInfo == "ResetOTP")
        {
            lblalert.Text = "Entered token has been expired.Please click 'Forgot Password?' to get new token";
            mpeotpforget.Show();
            mpealert.Show();
            return;
        }

        else // Login success...
        {
            DataTable dtUserRole = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtLoginEmail.Value + "'").Tables[0];
            if (dtUserRole.Rows.Count > 0)
            {
                if (dtUserRole.Rows[0]["MustChangePassword"].ToString() == "1")
                {
                    mpechange.Show();
                }
            }
        }
    }
    protected void btnok_click(object sender, EventArgs e)
    {
        if (BookedFlag.Text == "B")
        {
            SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Update   Booking_Log set Bookflag='0'  where  tailno='" + Request.Cookies["JETRIPSPAY"]["tailno"].ToString() + "' and tripno='" + Request.Cookies["JETRIPSPAY"]["Tripno"].ToString() + "' and [Date]='" + Convert.ToDateTime(Request.Cookies["JETRIPSPAY"]["BDate"].ToString()) + "' and Starttime='" + Request.Cookies["JETRIPSPAY"]["starttime"].ToString() + "' and Endtime='" + Request.Cookies["JETRIPSPAY"]["Endtime"].ToString() + "' and [Start]='" + Request.Cookies["JETRIPSPAY"]["start"].ToString() + "' and [End]='" + Request.Cookies["JETRIPSPAY"]["End"].ToString() + "' and AlerId ='" + Request.Cookies["JETIDFORALERT"]["AlertId"].ToString() + "' ");

        }
        mpealertfortime.Hide();
        Response.Redirect("SimpleSearchBooking.aspx?Flag=L");

        // ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "CloseSignup();", true);
    }
    protected void btnOkThank_onclick(object sender, EventArgs e)
    {
        if (loggedflag.Text == "L" && BookedFlag.Text == "B")
        {

            mpeotp.Show();
            // Response.Redirect("signup.aspx");
            //mpetime.Show();
            //tdHeader.Visible = true;
            //tdHeader1.Visible = true;
            //tdHeader2.Visible = true;
            //tdHeader4.Visible = true;
            //tdHeader3.Visible = false;
        }
        else
        {
            mpeotp.Show();
            //  Response.Redirect("signup.aspx");

        }
    }
    protected void Closempepopup(object sender, EventArgs e)
    {


        mpeterms2.Hide();
        mpesignup.Show();


    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
        try
        {
            string strBusinessAlert = string.Empty;
            //if (txtOldPassword.Value.ToString().Length == 0)
            //{
            //    strBusinessAlert += "- Old Password is required. <br/>";

            //}
            if (txtNewPassword.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- New Password <br/>";

            }
            if (txtConformPass.Value.ToString().Length == 0)
            {
                strBusinessAlert += "- Confirm Password <br/>";

            }

            if (strBusinessAlert.Length > 0)
            {

                lblalert.Text = "The below required items are missing:<br><br>" + strBusinessAlert;
                mpealert.Show();
                mpechange.Show();
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
                return;
            }
            //string userInfo = UserClass.AuthenticateUser(User.Identity.Name, txtOldPassword.Text);
            //string userInfo = AuthenticateUser(Request.Cookies["JETRIPSADMIN"]["Email"].ToString(), txtOldPassword.Value);
            string result = string.Empty;
            string result1 = string.Empty;

            //if (userInfo == UserInfo.Invalid.ToString() || userInfo == UserInfo.Incorrect.ToString()) // Login failed...
            //{
            //    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Incorrect Old Password.');", true);
            //    lblalert.Text = "Incorrect Old Password.";
            //    mpealert.Show();
            //    mpechange.Show();
            //    return;
            //}
            //else
            //{



            SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set Password ='" + genclass.Encrypt(txtNewPassword.Value.Trim()) + "',MustChangePassword='0' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");


            string strErr = "";
            string strval = string.Empty;

            DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select *, isnull(BrokerFlag,'N') as UserbrokerfLAG from UserMasterCreate where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];
            HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
            Response.Cookies.Remove("JETRIPSADMIN");
            Response.Cookies.Add(JETRIPSADMIN);
            JETRIPSADMIN.Values.Add("Username", dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
            JETRIPSADMIN.Values.Add("FirstName", dt.Rows[0]["FirstName"].ToString());
            JETRIPSADMIN.Values.Add("LastName", dt.Rows[0]["LastName"].ToString());
            JETRIPSADMIN.Values.Add("Title", dt.Rows[0]["Title"].ToString());
            JETRIPSADMIN.Values.Add("Email", dt.Rows[0]["Email"].ToString());
            JETRIPSADMIN.Values.Add("PhoneNumber", dt.Rows[0]["PhoneNumber"].ToString());
            JETRIPSADMIN.Values.Add("Phonecode", dt.Rows[0]["Code_Phone"].ToString());
            JETRIPSADMIN.Values.Add("Address", dt.Rows[0]["Address"].ToString());
            JETRIPSADMIN.Values.Add("Country", dt.Rows[0]["Country"].ToString());
            JETRIPSADMIN.Values.Add("City", dt.Rows[0]["City"].ToString());
            JETRIPSADMIN.Values.Add("ZipCode", dt.Rows[0]["ZipCode"].ToString());
            JETRIPSADMIN.Values.Add("Role", dt.Rows[0]["Role"].ToString());
            JETRIPSADMIN.Values.Add("UserId", dt.Rows[0]["RowId"].ToString());
            JETRIPSADMIN.Values.Add("Brokerflag", dt.Rows[0]["UserbrokerfLAG"].ToString());

            Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);

            if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString().Length > 0)
            {
                if (Request.Cookies["JETRIPSADMIN"]["Email"].ToString() != "guest@demo.com")
                {


                    DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'").Tables[0];

                    if (dtUser.Rows[0]["MustChangePassword"].ToString() != "1")
                    {
                        if (dtUser.Rows.Count > 0)
                        {
                            if (dtUser.Rows[0]["role"].ToString().ToLower() == "admin")
                            {
                                //  Response.Redirect("Adminhome.aspx?user=" + dtUser.Rows[0][2].ToString() + "");
                                Response.Redirect("Pricecharter.aspx");
                            }
                            else
                            {

                                loggedflag.Text = "L";
                                //lblLoggedin.Text = "Logged in as " + Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                                if (loggedflag.Text == "L" && BookedFlag.Text == "B")
                                {

                                    mpetime.Show();
                                    ddlTimeFrom.Focus();
                                    //tdHeader.Visible = true;
                                    //tdHeader1.Visible = true;
                                    //tdHeader2.Visible = true;
                                    //tdHeader4.Visible = true;
                                    //tdHeader3.Visible = false;
                                }
                                else
                                {
                                    if (dt.Rows[0]["userflag"].ToString().ToUpper() != "ADMINUSER")
                                    {

                                        Response.Redirect("SimpleSearchBooking.aspx?Flag=L");
                                    }
                                    else
                                    {
                                        Response.Redirect("SimpleSearchBooking.aspx");
                                    }

                                }



                            }
                        }
                    }


                }



            }

            //}
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void lnkalertprice_Click(object sender, EventArgs e)
    {
        Response.Redirect("Bookingcart.aspx?Flag=Available");
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("simplesearchBooking.aspx");
    }
    protected void Buttonsubmit_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }

    }
    protected void btncancelpopup_Click(object sender, EventArgs e)
    {
        SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, @"Update   Booking_Log set Bookflag='0'  where  tailno='" + Request.Cookies["JETRIPSPAY"]["tailno"].ToString() + "' and tripno='" + Request.Cookies["JETRIPSPAY"]["Tripno"].ToString() + "' and [Date]='" + Convert.ToDateTime(Request.Cookies["JETRIPSPAY"]["BDate"].ToString()) + "' and Starttime='" + Request.Cookies["JETRIPSPAY"]["starttime"].ToString() + "' and Endtime='" + Request.Cookies["JETRIPSPAY"]["Endtime"].ToString() + "' and [Start]='" + Request.Cookies["JETRIPSPAY"]["start"].ToString() + "' and [End]='" + Request.Cookies["JETRIPSPAY"]["End"].ToString() + "' and AlerId ='" + Request.Cookies["JETIDFORALERT"]["AlertId"].ToString() + "' ");
        mpetime.Hide();
    }

    protected void lnkalertBook_Click(object sender, EventArgs e)
    {
        if (loggedflag.Text == "L" && BookedFlag.Text == "B")
        {
            mpetime.Show();
        }
        else if (BookedFlag.Text == "B")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);

        }
    }
    public DataTable AirRouteCalculation(string StartAirport, string strEndAirport, string strSpeed)
    {
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
        string strRoot = StartAirport + "-" + strEndAirport;

        string WEBSERVICE_URL = "https://greatcirclemapper.p.rapidapi.com/airports/route/";
        WEBSERVICE_URL = WEBSERVICE_URL + strRoot + @"/" + strSpeed;

        var jsonResponse = "";

        var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
        if (webRequest != null)
        {
            webRequest.Method = "GET";
            webRequest.Timeout = 20000;
            webRequest.ContentType = "text/html;charset=UTF-8"; //"application/json";

            webRequest.Headers.Add("X-RapidAPI-Host", "greatcirclemapper.p.rapidapi.com");
            webRequest.Headers.Add("X-RapidAPI-Key", SecretsBLL.RapidAPIKey.ToString());
            webRequest.Headers.Add("vary", "Accept-Encoding");

            //webRequest.Headers.Add("content-type", "text/html;charset=UTF-8");

            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                {
                    jsonResponse = sr.ReadToEnd();
                }
            }
        }
        LegDetails12 leg = new LegDetails12();
        List<LegDetails12> RouteListLeg = new List<LegDetails12>();

        JavaScriptSerializer js = new JavaScriptSerializer();
        dynamic d = js.Deserialize<dynamic>(jsonResponse.ToString());

        dynamic[] arrlegs = d["legs"];

        for (int i = 0; i < arrlegs.Length; i++)
        {
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(arrlegs[i]);

            dynamic dInnerLeg = js.Deserialize<dynamic>(json);

            leg = new LegDetails12();

            leg.legNo = Convert.ToString(i + 1);
            leg.startICAO = dInnerLeg["origin"]["ident"];
            leg.endICAO = dInnerLeg["destination"]["ident"];
            leg.distance_km = Convert.ToString(arrlegs[i]["distance_km"]);
            leg.distance_nm = Convert.ToString(arrlegs[i]["distance_nm"]);
            leg.flight_time_min = Convert.ToString(arrlegs[i]["flight_time_min"]);
            leg.heading_deg = Convert.ToString(arrlegs[i]["heading_deg"]);
            leg.heading_compass = Convert.ToString(arrlegs[i]["heading_compass"]);

            RouteListLeg.Add(leg);
        }

        DataTable dtResul = ToDataTable(RouteListLeg);

        return dtResul;

    }
    public DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name);
        }

        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }

        //put a breakpoint here and check datatable
        return dataTable;

    }
    public DataTable RapidAPIDatatable()
    {
        DataTable dtRapid = new DataTable();
        dtRapid.Columns.Add("legNo");
        dtRapid.Columns.Add("startICAO");
        dtRapid.Columns.Add("endICAO");
        dtRapid.Columns.Add("distance_km");
        dtRapid.Columns.Add("distance_nm");
        dtRapid.Columns.Add("flight_time_min");
        dtRapid.Columns.Add("heading_deg");
        dtRapid.Columns.Add("heading_compass");

        DataRow row = dtRapid.NewRow();
        row["legNo"] = 1;
        row["startICAO"] = "KVNY";
        row["endICAO"] = "KSNA";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        row = dtRapid.NewRow();
        row["legNo"] = 2;
        row["startICAO"] = "KSNA";
        row["endICAO"] = "KVNY";
        row["distance_km"] = "2344";
        row["distance_nm"] = "12345";
        row["flight_time_min"] = "50.7";
        row["heading_deg"] = 1;
        row["heading_compass"] = 1;
        dtRapid.Rows.Add(row);

        return dtRapid;


    }
    void LoginDirectSecond()
    {
        try
        {
            userInfo = AuthenticateUser(txtemail1.Value, txtpassword1.Value);
            if (userInfo == UserInfo.Incorrect.ToString()) // Login failed...
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Incorrect Password')", true);
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
                mpelogin.Show();
                return;
                //lblInvalidLogin.InnerHtml = "Incorrect Password";
            }
            else if (userInfo == UserInfo.Invalid.ToString()) // Login failed...
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Invalid username or password')", true);
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "modalpopup", "Open();", true);
                mpelogin.Show();
                return;

                //lblInvalidLogin.InnerHtml = "Invalid username or password";
            }

            else // Login success...
            {
                string strErr = "";
                string strval = string.Empty;

                DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtemail1.Value + "'").Tables[0];
                HttpCookie JETRIPSADMIN = new HttpCookie("JETRIPSADMIN");
                Response.Cookies.Remove("JETRIPSADMIN");
                Response.Cookies.Add(JETRIPSADMIN);
                JETRIPSADMIN.Values.Add("Username", dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString());
                JETRIPSADMIN.Values.Add("FirstName", dt.Rows[0]["FirstName"].ToString());
                JETRIPSADMIN.Values.Add("LastName", dt.Rows[0]["LastName"].ToString());
                JETRIPSADMIN.Values.Add("Title", dt.Rows[0]["Title"].ToString());
                JETRIPSADMIN.Values.Add("Email", dt.Rows[0]["Email"].ToString());
                JETRIPSADMIN.Values.Add("PhoneNumber", dt.Rows[0]["PhoneNumber"].ToString());
                JETRIPSADMIN.Values.Add("Address", dt.Rows[0]["Address"].ToString());
                JETRIPSADMIN.Values.Add("Country", dt.Rows[0]["Country"].ToString());
                JETRIPSADMIN.Values.Add("City", dt.Rows[0]["City"].ToString());
                JETRIPSADMIN.Values.Add("ZipCode", dt.Rows[0]["ZipCode"].ToString());
                JETRIPSADMIN.Values.Add("Role", dt.Rows[0]["Role"].ToString());
                JETRIPSADMIN.Values.Add("UserId", dt.Rows[0]["RowId"].ToString());

                Response.Cookies["authUser"].Expires = DateTime.Now.AddDays(2);
                //changed by gomathy
                NameValueCollection ipAddress = Request.ServerVariables;
                EMPTYTRIPADMINTRANSID.Values.Add("TrasId", UserLoggedIn(dt.Rows[0]["RowId"].ToString(), ipAddress[32].ToString(), Request.Browser.Browser, Request.Browser.Version, userInfo, txtTelephony.Value));
                Response.Cookies.Add(EMPTYTRIPADMINTRANSID);
                //changed by gomathy

            }

            if (txtemail1.Value.Length > 0)
            {
                if (txtemail1.Value != "guest@demo.com")
                {
                    DataTable dtUserRole = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtemail1.Value + "'").Tables[0];
                    if (dtUserRole.Rows.Count > 0)
                    {
                        if (dtUserRole.Rows[0]["role"].ToString().ToLower() == "admin")
                        {
                            if (dtUserRole.Rows[0]["MustChangePassword"].ToString() != "1")
                            {
                                Response.Redirect("Pricecharter.aspx");
                            }
                            else
                            {
                                mpechange.Show();
                            }
                        }
                        else
                        {
                            string path = Request.Url.PathAndQuery;
                            if (path.Contains("verified=y"))
                            {
                                if (Request.QueryString["verified"].ToString() != null)
                                {
                                    if (Request.QueryString["verified"].ToString() == "y")
                                    {
                                        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set MustChangePassword='0' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");
                                    }
                                    else
                                    {
                                        SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set MustChangePassword='1' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");
                                    }
                                }
                            }
                            else
                            {
                                DataTable dtUser2 = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtemail1.Value + "'").Tables[0];
                                if (dtUser2.Rows[0]["MustChangePassword"].ToString() != "1")
                                {

                                }
                                else
                                {
                                    SqlHelper.ExecuteReader(SqlHelper.FLYTConnectionString, CommandType.Text, "Update UserMasterCreate set MustChangePassword='1' Where Email='" + Request.Cookies["JETRIPSADMIN"]["Email"].ToString() + "'");
                                }

                            }
                            DataTable dtUser = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select email,role,FirstName+''+LastName as Name,* from UserMasterCreate where email='" + txtemail1.Value + "'").Tables[0];
                            if (dtUser.Rows[0]["MustChangePassword"].ToString() != "1")
                            {
                                if (dtUser.Rows.Count > 0)
                                {
                                    if (dtUser.Rows[0]["role"].ToString().ToLower() == "admin")
                                    {
                                        Response.Redirect("Pricecharter.aspx");
                                    }
                                    else
                                    {
                                        loggedflag.Text = "L";
                                        if (dtUser.Rows[0]["Usecookies"].ToString() == "1")
                                        {
                                            if (loggedflag.Text == "L" && BookedFlag.Text == "B")
                                            {

                                                mpetime.Show();
                                                ddlTimeFrom.Focus();
                                                //tdHeader.Visible = true;
                                                //tdHeader1.Visible = true;
                                                //tdHeader2.Visible = true;
                                                //tdHeader4.Visible = true;
                                                //tdHeader3.Visible = false;
                                            }
                                            else
                                            {
                                                Response.Redirect("SimpleSearchBooking.aspx?Flag=L");
                                            }
                                        }
                                        else
                                        {
                                            mpecookie.Show();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('The Email Id is not verified by you.please verify the email and then sign in to application.')", true);
                            }

                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    void SendEmailForget(string strEmail, string strPassword, string strName)
    {
        string ZendeskTicketId = string.Empty;
        string strEditorMessage = string.Empty;
        string subject = string.Empty;
        string emailcc = string.Empty;
        subject = "Forgot Password - Jet Edge Zoom";

        StringBuilder sbEmailBody = new StringBuilder();

        
        string strURL = "https://www.flyjetedge.net/Zoom/simplesearchBooking.aspx";
        //Mail Body
        sbEmailBody.Append("Dear " + strName + ",<br><br>");
        sbEmailBody.Append("We recently received a request to reset the password for your Jet Edge Zoom account. To reset your password, please return to your browser and enter the following token");

        //sbEmailBody.Append("Url : <a href='" + strURL + "'>" + strURL + " </a><br><br>");
        //sbEmailBody.Append("Login ID : " + strEmail + "<br><br>");
        sbEmailBody.Append("<br/>");
        sbEmailBody.Append("<br/>");

        sbEmailBody.Append("<div style='width:25%;padding:10px;background-color:rgb(178,30,40);text-align:center;color:rgb(255,255,255);font-size:22px'>" + strPassword + "</div>");
        sbEmailBody.Append("<br/>");
        sbEmailBody.Append("<br/>");

        sbEmailBody.Append("The token will expire in 30 minutes. If you did not submit this request, disregard this e-mail.");
        sbEmailBody.Append("<br/>");

        sbEmailBody.Append("<br/>");
        sbEmailBody.Append("<br/>");
        sbEmailBody.Append("Thank you.<br>Jet Edge Zoom Team<br>");

        try
        {
            string strRes = genclass.SendEmail_login(strEmail, subject, sbEmailBody.ToString());
            if (strRes.ToLower().Trim() != "yes")
            {
                lblalert.Text = "Email sending failed.";
                mpealert.Show();
            }
            else
            {
                lblalert.Text = "We will send a token to your email to reset your password";
            }
        }
        catch (Exception ex)
        {
            lblalert.Text = "Email sending failed.";
            mpealert.Show();
        }
    }

    void SendEmailForgetNew(string strEmail, string strPassword, string strName)
    {
        StringBuilder sbEmailBody = new StringBuilder();

        // Subject of the mail
        string subject = "Welcome to Jet Edge !";
        
        string strURL = "https://www.flyjetedge.net/Zoom/simplesearchBooking.aspx";
        //Mail Body
        sbEmailBody.Append("Dear " + strName + ",<br><br>");
        sbEmailBody.Append("We have reset your password <br><br>");

        sbEmailBody.Append("Url : <a href='" + strURL + "'>" + strURL + " </a><br><br>");
        sbEmailBody.Append("Login ID : " + strEmail + "<br><br>");
        sbEmailBody.Append("Password : " + strPassword + " <br><br>");

        sbEmailBody.Append("Thank you for your co-operation,<br><br>Jet Edge.<br>");
        sbEmailBody.Append("<h3><u>NOTE: Reply to this Email will NOT be accepted or responded.</u></h3><br>");
        try
        {
            string strRes = genclass.SendEmail_login(strEmail, subject, sbEmailBody.ToString());

            if (strRes.ToLower().Trim() != "yes")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Email sending failed.');", true);
            }
            else
            {
                mpelogin.Show();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('Email sending failed.');", true);
        }

    }

    protected void SendEmail_logincreate(string email, string username, string OTP)
    {
        string ZendeskTicketId = string.Empty;
        string strEditorMessage = string.Empty;
        string subject = string.Empty;
        string emailcc = string.Empty;

        try
        {
            MailMessage message = new MailMessage();
            message.To.Add(email);// Email-ID of Receiver  
            message.Bcc.Add("vignesh.murugan@sankarasoftware.com");// Email-ID of Receiver  
            message.Subject = "WELCOME TO JET EDGE ZOOM";// Subject of Email  
            message.From = new
            System.Net.Mail.MailAddress("zoom@flyjetedge.com");// Email-ID of Sender  
            message.IsBodyHtml = true;
            message.AlternateViews.Add(Mail_Body(OTP, username));
            SmtpClient SmtpMail = new SmtpClient();
            SmtpMail.Host = "localhost";//name or IP-Address of Host used for SMTP transactions  
            SmtpMail.Port = 25;//Port for sending the mail  
            SmtpMail.Credentials = new
            System.Net.NetworkCredential("", "");//username/password of network, if apply  
            SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpMail.EnableSsl = false;
            SmtpMail.ServicePoint.MaxIdleTime = 0;
            SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);
            message.BodyEncoding = Encoding.Default;
            message.Priority = MailPriority.High;
            SmtpMail.Send(message); //Smtpclient to send the mail message  
        }
        catch (Exception ex)
        { }
    }

    private AlternateView Mail_Body(string OTP, string username)
    {
        DataTable dt = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select * from UserMasterCreate where Email='" + txtemail.Value + "'").Tables[0];
        string id = string.Empty;
        if (dt.Rows.Count > 0)
        {
            id = dt.Rows[0]["Rowid"].ToString();
        }

        string strURL = "https://www.flyjetedge.net/Zoom/Simplesearchbooking.aspx?verified=y&id=" + id + "";
        string path = Server.MapPath(@"assets/img/jet-edge-logo-Email.png");
        LinkedResource Img = new LinkedResource(path, MediaTypeNames.Image.Jpeg);
        Img.ContentId = "MyImage";
        string str = @" <div>
         <center>
        <div style='border: 1px solid #a3a29e;width:500px'>
        <div style='text-align: center;'>
        <table width='100%' style='text-align:center'>
<tr>
<td style='text-align:right;width:45%'>
 <img src=cid:MyImage  id='img1' alt='' width='55px' height='55px'/> 
</td>
<td style='text-align:left;width:55%;padding-left:9px;vertical-align:middle'>
  JET EDGE

</tr>
</table>
        </div>
        <div style='text-align: center;padding-bottom:10px'>
          WELCOME TO JET EDGE ZOOM
        </div>
        <div style='border-top:1px solid #a3a29e;padding-top:10px;padding-bottom:10px;text-align:left;padding-left:5%'>
          Thank you for creating a Jet Edge Zoom account.  Your token is below.  Please click on it within the next 30 minutes to verify your registration.  If you did not create an account, please disregard this e-mail.
        </div>
         <div style='padding-top:20px;padding-bottom:20px'>
           <a style='background-color:#b72025;padding-right:30%;padding-left:30%;padding-top:10px;text-decoration:none;font-size:25px;padding-bottom:10px;width:100%;color:white' >" + OTP + @"</a>
        </div>
          <div style='padding-top:6px;font-size:11px;padding-bottom:6px;display:none'>
           Your token will expire in 30 minutes
        </div>
         <div style='padding-top:10px;border-top:1px solid #a3a29e;text-align:left;padding-left:5%'>
          If you have any difficulty verifying your account, please contact us at zoom@flyjetedge.com.  Replies to this e-mail will not be accepted.  
        </div>
         
         <div style='padding-top:20px;padding-bottom:10px;text-align:left;padding-left:5%'>
         We hope you enjoy Zoom!
<br/>
<br/>
Jet Edge Zoom Team
        </div>
     
     </div>

       </center>
        </div>";
        AlternateView AV =
        AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
        AV.LinkedResources.Add(Img);
        return AV;
    }
}

public class LegDetails12
{
    public string legNo { get; set; }
    public string startICAO { get; set; }
    public string endICAO { get; set; }
    public string distance_km { get; set; }
    public string distance_nm { get; set; }
    public string flight_time_min { get; set; }
    public string heading_deg { get; set; }
    public string heading_compass { get; set; }
}