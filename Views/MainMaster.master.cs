﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Data;
using BusinessLayer;

public partial class Pages_AdminMasterPage : System.Web.UI.MasterPage
{
    public int linenum = 0;
    public string MethodName = string.Empty;
    CreateAdminBLL objCreateAdminBLL = new CreateAdminBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DataTable dtRunTime = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select TOP 1 FORMAT(CurrentDate,'yyyy-MM-dd') CurrentDate,  CurrentTime FROM LivePricingCaculation order by CurrentDate DESC, CurrentTime DESC").Tables[0];
            if (dtRunTime.Rows.Count > 0)
            {
                string LastRunTime = dtRunTime.Rows[0]["CurrentDate"].ToString() + " " + dtRunTime.Rows[0]["CurrentTime"].ToString();

                lblLastRunTime.Text = genclass.Long_Date_Time(Convert.ToDateTime(LastRunTime)) + " PST";
                lblNextRunTime.Text = genclass.Long_Date_Time(Convert.ToDateTime(LastRunTime).AddMinutes(15)) + " PST";
            }

            if (!IsPostBack)
            {
                NameValueCollection ipAddress2 = Request.ServerVariables;
                lblLastLogin.Text = ipAddress2[32];

                DataTable dtuser = objCreateAdminBLL.Edit(Request.Cookies["JETRIPSADMIN"]["UserId"].ToString().Trim());
                if (dtuser.Rows.Count > 0)
                {
                    lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                    lblUserInitial.Text = Request.Cookies["JETRIPSADMIN"]["FirstName"].ToString().Substring(0, 1) + Request.Cookies["JETRIPSADMIN"]["LastName"].ToString().Substring(0, 1);
                    lblUserInitial1.Text = Request.Cookies["JETRIPSADMIN"]["FirstName"].ToString().Substring(0, 1) + Request.Cookies["JETRIPSADMIN"]["LastName"].ToString().Substring(0, 1);
                    lblcontact.Text = dtuser.Rows[0]["Code_Phone"].ToString().Replace("+", "") + "." + genclass.Format_Phone(dtuser.Rows[0]["PhoneNumber"].ToString().Trim().Replace(".", ""));
                    lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();
                }

                DataTable dtpayment = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select top(1) * from Enablepayment order by rowid desc").Tables[0];
                if (dtpayment != null && dtpayment.Rows.Count > 0)
                {
                    if (dtpayment.Rows[0]["EnablePayment"].ToString() == "1")
                    {
                        chkpayment.Checked = true;
                        lblpayment.Text = "[Enabled]";
                    }
                    else
                    {
                        chkpayment.Checked = false;
                        lblpayment.Text = "[Disabled]";

                    }
                }

                DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
                if (dtGlobalConfig.Rows.Count > 0)
                {
                    if (dtGlobalConfig.Rows[0]["MailFlag"].ToString().Length > 0)
                    {
                        rblMailConfig.SelectedValue = dtGlobalConfig.Rows[0]["MailFlag"].ToString();
                        lblMailConfig.Text = rblMailConfig.SelectedItem.Text;
                    }

                    if (dtGlobalConfig.Rows[0]["SmsFlag"].ToString().Length > 0)
                    {
                        rblEnableSMS.SelectedValue = dtGlobalConfig.Rows[0]["SmsFlag"].ToString();
                        lblEnableSMS.Text = rblEnableSMS.SelectedItem.Text;

                    }
                    if (dtGlobalConfig.Rows[0]["FlightHourFlag"].ToString().Length > 0)
                    {
                        rblFlightHours.SelectedValue = dtGlobalConfig.Rows[0]["FlightHourFlag"].ToString();
                        lblFilghtHourFlag.Text = rblFlightHours.SelectedItem.Text;
                    }
                }

                DataTable dtPaymentCount = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "Select * FROM BookedDetails where PayMethod = 'WT' and PaidStatus='P'").Tables[0];
                if (dtPaymentCount.Rows.Count > 0)
                {
                    lblPaymentCount.Text = dtPaymentCount.Rows.Count.ToString();
                }
                else
                {
                    lblPaymentCount.Text = "0";
                }

                DataTable dtDiscountCoupon = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select tailno,isnull(Minbd,0.00) as Minbd,isnull(MaxBd,0.00) as MaxBd,isnull(Minsellable,0.00) as Minsellable,isnull(HourlyRate,0.00) as HourlyRate,isnull(FixedFloor,0.00) as FixedFloor,isnull(FixedCeiling	,0.00) as FixedCeiling	,pricingcompleted	,applytax,	CASE WHEN Tailsetup.active='1'  THEN 'TRUE' WHEN Tailsetup.active = '0' THEN 'FALSE' ELSE 'FALSE' END AS Show  from Tailsetup where pricingcompleted='0'").Tables[0];
                if (dtDiscountCoupon.Rows.Count > 0)
                {
                    gvtail.DataSource = dtDiscountCoupon;
                    gvtail.DataBind();
                    lblNotCount.Text = dtDiscountCoupon.Rows.Count.ToString();
                    notcountde.Text = dtDiscountCoupon.Rows.Count.ToString();
                    notcountde1.Text = dtDiscountCoupon.Rows.Count.ToString();
                }
                else
                {
                    gvtail.DataSource = null;
                    gvtail.DataBind();
                    lblNotCount.Text = "0";
                }
            }
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    public string GetPhoneCode(string strCountry)
    {
        string strPCode = string.Empty;
        DataTable dtPhoneCode = SqlHelper.ExecuteDataset(SqlHelper.FLYTConnectionString, CommandType.Text, "select PhoneCode from JETEDGE_FUEL..Country where Row_ID='" + strCountry + "'").Tables[0];

        if (dtPhoneCode.Rows.Count > 0)
        {
            strPCode = dtPhoneCode.Rows[0]["PhoneCode"].ToString().Trim();
        }

        return strPCode;
    }

    protected void chkpayment_checkedchange(object sender, EventArgs e)
    {
        try
        {
            string strpay = string.Empty;
            if (chkpayment.Checked)
            {
                strpay = "1";
                lblpayment.Text = "[Enabled]";
            }
            else
            {
                strpay = "0";
                lblpayment.Text = "[Disabled]";
            }

            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into Enablepayment ([EnablePayment] , [Updatedby] , [Updatedon]  ) values ('" + strpay + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #region Added By Venkatesh

    protected void rblMailConfig_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;

            FlightHours = rblFlightHours.SelectedValue;
            MailFlag = rblMailConfig.SelectedValue;
            SmsFlag = rblEnableSMS.SelectedValue;


            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [MailFlag] = '" + MailFlag + "' , [SmsFlag] = '" + SmsFlag + "',[FlightHourFlag]='" + FlightHours + "', [UpdatedBy]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [UpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [UpdatedBy], [UpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = rblMailConfig.SelectedValue == "S" ? "Mail Config : Zendesk to SMTP" : "Mail Config : SMTP to Zendesk";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void rblEnableSMS_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;

            FlightHours = rblFlightHours.SelectedValue;
            MailFlag = rblMailConfig.SelectedValue;
            SmsFlag = rblEnableSMS.SelectedValue;


            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [MailFlag] = '" + MailFlag + "' , [SmsFlag] = '" + SmsFlag + "',[FlightHourFlag]='" + FlightHours + "', [UpdatedBy]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [UpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [UpdatedBy], [UpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = rblEnableSMS.SelectedValue == "Y" ? "Enable SMS : No to Yes" : "Enable SMS : Yes to No";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    protected void rblFlightHours_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string MailFlag = string.Empty;
            string SmsFlag = string.Empty;
            string FlightHours = string.Empty;

            FlightHours = rblFlightHours.SelectedValue;
            MailFlag = rblMailConfig.SelectedValue;
            SmsFlag = rblEnableSMS.SelectedValue;


            DataTable dtGlobalConfig = genclass.GetDataTable_Mgnt("Select * FROM GlobalConfig");
            if (dtGlobalConfig.Rows.Count > 0)
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "update GlobalConfig SET  [MailFlag] = '" + MailFlag + "' , [SmsFlag] = '" + SmsFlag + "',[FlightHourFlag]='" + FlightHours + "', [UpdatedBy]= '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "', [UpdatedOn]= '" + DateTime.Now + "'");
            }
            else
            {
                SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig ( [MailFlag] , [SmsFlag] ,[FlightHourFlag], [UpdatedBy], [UpdatedOn]  ) values ('" + MailFlag + "','" + SmsFlag + "','" + FlightHours + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
            }

            string EventName = string.Empty;
            EventName = rblFlightHours.SelectedValue == "Y" ? "Flight Hours changed" : "Flight Hours changed";
            SqlHelper.ExecuteScalar(SqlHelper.FLYTConnectionString, CommandType.Text, "insert into GlobalConfig_Log ( [EventName] , [UpdatedBy] , [UpdatedOn]  ) values ('" + EventName + "' , '" + Request.Cookies["JETRIPSADMIN"]["UserId"] + "' , '" + DateTime.Now + "' ) ");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }

    #endregion

    protected void loggedout_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Cookies["JETRIPSADMIN"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Remove("JETRIPSADMIN");
            Session.Abandon();

            Response.Redirect("https://www.flyjetedge.net/FLYT");
        }
        catch (Exception ex)
        {
            linenum = Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
            MethodName = System.Reflection.MethodBase.GetCurrentMethod().Name.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + genclass.LogException_New(ex, linenum, MethodName) + "')", true);
        }
    }
}
