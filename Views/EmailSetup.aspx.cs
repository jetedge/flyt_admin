﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class Views_EmailSetup : System.Web.UI.Page
{
    #region Global Declaration

    protected BusinessLayer.EmailSetupBLL objMember = new BusinessLayer.EmailSetupBLL();

    #endregion

    #region Pageload

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (Request.Cookies["JETRIPSADMIN"] == null)
            {
                Response.Redirect("SessionExpired.aspx", false);
            }

            if (!IsPostBack)
            {
                // Put user code to initialize the page here
                if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
                {
                    genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "General Setup", "Email Setup");
                }
               

                MasterPage mp = this.Master;
                HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
                HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");

                lblMainHeader.InnerText = "General Setup";
                lblSubHeader.InnerText = "Email Setup";
                lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
                lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
                lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

                btnAdd.Visible = true;
                btnSave.Visible = true;
                List();
            }
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    #endregion

    #region Web Form Designer generated code

    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    /// 
    private void InitializeComponent()
    {
        this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        this.btnAdd.Click += new System.EventHandler(this.btnNew_Click);
        this.btnSaveExternal.Click += new System.EventHandler(this.btnSaveExternalOnClick);
        this.Load += new System.EventHandler(this.Page_Load);

    }

    #endregion

    #region Button Events

    private void btnSave_Click(object sender, System.EventArgs e)
    {
        try
        {


            int retVal = 0;

            if (gvListItems.Rows.Count > 0)
            {
                for (int i = 0; i < gvListItems.Rows.Count; i++)
                {

                    if (((CheckBox)gvListItems.Rows[i].FindControl("Cbtrip")).Checked == true)
                    {
                        objMember.ChkTrip = "1";
                    }
                    else
                    {
                        objMember.ChkTrip = "0";
                    }
                    if (((CheckBox)gvListItems.Rows[i].FindControl("cbexception")).Checked == true)
                    {
                        objMember.ChkException = "1";
                    }
                    else
                    {
                        objMember.ChkException = "0";
                    }
                    if (((CheckBox)gvListItems.Rows[i].FindControl("cbpreference")).Checked == true)
                    {
                        objMember.ChkPreference = "1";
                    }
                    else
                    {
                        objMember.ChkPreference = "0";
                    }
                    if (((CheckBox)gvListItems.Rows[i].FindControl("cbmustmoves")).Checked == true)
                    {
                        objMember.ChkMustMove = "1";
                    }
                    else
                    {
                        objMember.ChkMustMove = "0";
                    }
                    if (((CheckBox)gvListItems.Rows[i].FindControl("cbsignup")).Checked == true)
                    {
                        objMember.ChkSignUp = "1";
                    }
                    else
                    {
                        objMember.ChkSignUp = "0";
                    }

                    objMember.CreatedBy = "10";
                    objMember.Email = ((Label)gvListItems.Rows[i].FindControl("lblEmail")).Text;
                    objMember.FirstName = ((Label)gvListItems.Rows[i].FindControl("lblfirstname")).Text;
                    objMember.LastName = ((Label)gvListItems.Rows[i].FindControl("lbllasname")).Text;

                    retVal = objMember.Save(objMember);
                }
            }


            if (retVal > 0)
            {
                lblalert.Text = "Email Setup Details have been saved successfully";
                mpealert.Show();
                btnAdd.Focus();
            }
            else
            {
                lblalert.Text = "Email Setup Details have been saved successfully";
                mpealert.Show();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    protected void btnSaveExternalOnClick(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = objMember.DuplicateUserMasterEmail(txtEmail.Value.Trim());

            if (dt.Rows.Count > 0)
            {
                txtEmail.Focus();
                string str = txtEmail.Value + " already exists";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('" + str + "')", true);
                mpetime.Show();
                return;
            }
            DataTable dt1 = objMember.DuplicateEmailSetupEmail(txtEmail.Value.Trim());


            if (dt1.Rows.Count > 0)
            {
                txtEmail.Focus();

                string str = txtEmail.Value + " already exist";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('" + str + "')", true);
                mpetime.Show();
                return;
            }


            if (chkEmpty.Checked)
            {
                objMember.ChkTrip = "1";
            }
            else
            {
                objMember.ChkTrip = "0";
            }
            if (chkexception.Checked)
            {
                objMember.ChkException = "1";
            }
            else
            {
                objMember.ChkException = "0";
            }
            if (chkpreference.Checked)
            {
                objMember.ChkPreference = "1";
            }
            else
            {
                objMember.ChkPreference = "0";
            }
            if (chkmustmoves.Checked)
            {
                objMember.ChkMustMove = "1";
            }
            else
            {
                objMember.ChkMustMove = "0";
            }
            if (chksignup.Checked)
            {
                objMember.ChkSignUp = "1";
            }
            else
            {
                objMember.ChkSignUp = "0";
            }
            objMember.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString();
            objMember.Email = txtEmail.Value;
            objMember.FirstName = txtfName.Value;
            objMember.LastName = txtLastname.Value;

            int strReturnVal = objMember.Save(objMember);

            if (strReturnVal > 0)
            {
                lblalert.Text = "Email setup details has been saved successfully";
                mpealert.Show();
            }
            List();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    private void btnNew_Click(object sender, System.EventArgs e)
    {
        try
        {
            mpetime.Show();
            txtfName.Focus();
            txtfName.Value = string.Empty;
            txtEmail.Value = string.Empty;
            txtLastname.Value = string.Empty;
            chkEmpty.Checked = false;
            chkexception.Checked = false;
            chkmustmoves.Checked = false;
            chkpreference.Checked = false;
            chksignup.Checked = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + ex.Message + "')", true);
        }
    }

    #endregion

    #region  User Defined Function


    public void List()
    {
        DataTable dt = objMember.Select();
        if (dt.Rows.Count > 0)
        {
            gvListItems.DataSource = dt;
            gvListItems.DataBind();
        }
        else
        {
            gvListItems.DataSource = null;
            gvListItems.DataBind();
        }
    }

    #endregion
}