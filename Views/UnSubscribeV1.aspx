﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnSubscribeV1.aspx.cs" Inherits="Views_UnSubscribeV1"
    Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        .unsubscribeContent
        {
            border-radius: 10px;
            border: 1px solid #d6d4d4;
            background-color: #ffffff;
            padding: 40px 20px;
            width: 600px;
            box-sizing: border-box;
            text-align: center;
            margin: 25px auto;
        }
        .mailSubscribe
        {
            font-size: 18px;
            line-height: 31px;
            border-bottom: 1px solid #dbdedf;
            padding-bottom: 30px;
            margin-bottom: 30px;
        }
        .msgTitle
        {
            color: #ff3f12;
            font-size: 36px;
            display: inline-block;
            margin-bottom: 24px;
        }
        .selectReason
        {
            width: 430px;
            text-align: left;
            margin: 0 auto;
            margin-top: 35px;
        }
        .errorMsg
        {
            color: #e11414;
            font-size: 12px;
            line-height: 24px;
        }
        input[type="text"], select
        {
            margin-top: 3px;
            padding: 8px; /* font-size: 1.15vw; */
            font-size: 14px;
            width: 75%;
            border-radius: 0px;
            border: 1px solid rgb(153,154,154);
            font-weight: 400;
        }
        .primaryBtn
        {
            background-color: #b21e28;
            font-size: 18px;
            color: #ffffff;
            padding: 10px 24px;
            text-align: center;
            border-radius: 5px;
            display: inline-block;
            text-decoration: none;
            border: none;
        }
        input, textarea
        {
            font-family: Gotham, "Helvetica Neue" , Helvetica, Arial, sans-serif;
            box-sizing: border-box;
        }
        .inputType
        {
            border: 1px solid #b3baba;
            padding: 10px 7px;
            width: 100%;
            font-size: 15px;
            border-radius: 6px;
            overflow: hidden;
        }
        .subsInnerContent
        {
            font-size: 16px;
            line-height: 25px;
        }
    </style>
</head>
<body style="font-family: Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif;
    color: White;">
    <form id="form1" runat="server">
    <div class="unsubscribeContent" id="divBefore" runat="server" style="background-color: Black;">
        <div style="text-align: center;">
            <img height="77" width="162" src="https://ecp.yusercontent.com/mail?url=https%3A%2F%2Fimage.s4.sfmc-content.com%2Flib%2Ffe94157073660d7970%2Fm%2F1%2F322c38c7-0297-4bec-ad8f-bc107557e1ca.png&amp;t=1600074859&amp;ymreqid=895ad81f-0726-2db0-1c62-750057018300&amp;sig=iJ9t.OFmBQiC9JSoEwMK7Q--~D">
        </div>
        <div class="mailSubscribe">
            <strong>
                <asp:TextBox Style="margin-top: 22px;" ID="txtEmailId" runat="server" CssClass="hideTextarea inputType"
                    Rows="4" placeholder="Please enter your email here..." autocomplete="off"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmailId"
                    Display="None" ErrorMessage="Please Enter Email." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator></strong>
            <%--is subscribed to our mailing list(s).--%>
        </div>
        <br />
        <div class="subsInnerContent">
            <strong class="msgTitle" style="color: White; font-size:25px">Unsubscribe from our mailing list</strong><br>
            To help us improve our services, we would be grateful if you could tell us why:<br>
            <div class="selectReason" style="margin-top: 35px; min-width: 250px;">
                <asp:DropDownList CssClass="inputType" ID="selectUnsubscribeReason" runat="server"
                    AutoPostBack="true" OnSelectedIndexChanged="selectUnsubscribeReason_SelectedIndexChanged">
                    <asp:ListItem Text="Please Select Reason" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Your emails are not relevant to me" Value="Your emails are not relevant to me"></asp:ListItem>
                    <asp:ListItem Text="Your emails are too frequent" Value="Your emails are too frequent"></asp:ListItem>
                    <asp:ListItem Text="I don't remember signing up for this" Value="I don't remember signing up for this"></asp:ListItem>
                    <asp:ListItem Text="I no longer want to receive these emails" Value="I no longer want to receive these emails"></asp:ListItem>
                    <asp:ListItem Text="The emails are spam and should be reported" Value="The emails are spam and should be reported"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="others"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="selectUnsubscribeReason"
                    Display="None" ErrorMessage="Reason is required." SetFocusOnError="True" ValidationGroup="SP"
                    InitialValue="0">*</asp:RequiredFieldValidator>
                <br>
                <asp:TextBox Style="margin-top: 22px;" ID="unsubscribereason" runat="server" Visible="false"
                    CssClass="hideTextarea inputType" Rows="4" placeholder="Provide your reason here..."></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="selectUnsubscribeReason"
                    Display="None" ErrorMessage="Please Enter Reason." SetFocusOnError="True" ValidationGroup="SP">*</asp:RequiredFieldValidator>
                <span style="display: none" class="errorMsg">Please Enter Reason</span>
            </div>
        </div>
        <br>
        <br>
        <div style="text-align: center">
            <asp:Button ID="btnUnsub" runat="server" Text="Unsubscribe" class="primaryBtn" OnClick="btnUnsub_Click"
                ValidationGroup="SP" />
            <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="SP" ShowMessageBox="true"
                runat="server" ShowSummary="false" />
        </div>
    </div>
    <div class="unsubscribeContent" id="divAfter" runat="server" visible="false" style="background-color: Black;">
        <div style="text-align: center;">
            <img height="77" width="162" style="float: left;" src="https://ecp.yusercontent.com/mail?url=https%3A%2F%2Fimage.s4.sfmc-content.com%2Flib%2Ffe94157073660d7970%2Fm%2F1%2F322c38c7-0297-4bec-ad8f-bc107557e1ca.png&amp;t=1600074859&amp;ymreqid=895ad81f-0726-2db0-1c62-750057018300&amp;sig=iJ9t.OFmBQiC9JSoEwMK7Q--~D">
        </div>
        <div class="mailSubscribe" style="border-bottom: 0px;">
        </div>
        <div class="subsInnerContent">
            <div>
                <strong class="msgTitle" style="padding-right: 23%;">Done!</strong><br>
                <hr>
                <span class="msgTxt"><strong>
                    <asp:Label ID="lblAMailId" runat="server"></asp:Label></strong><br>
                    has been successfully unsubscribed from our mailing list(s)</span>
            </div>
        </div>
    </div>
    </form>
    <asp:Label ID="lblFromFlag" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblFromId" runat="server" Visible="false"></asp:Label>
</body>
</html>
