﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI.HtmlControls;

public partial class Views_CustomerScheduler : System.Web.UI.Page
{

    protected BusinessLayer.SchedulerBLL objScheduler = new BusinessLayer.SchedulerBLL();

    string[] Scheduler = new string[30];
    string[] Status = new string[10];

    string status = string.Empty;
    string operation = string.Empty;
    string strMsg = string.Empty;
    string result = string.Empty;
    string ToAddress = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["JETRIPSADMIN"] == null)
        {
            Response.Redirect("SessionExpired.aspx", false);
        }

        if (!IsPostBack)
        {
            // Put user code to initialize the page here

            if (Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"] != null && Request.Cookies["JETRIPSADMIN"]["UserId"] != null)
            {
                genclass.fun_Get_ScreenName(Request.Cookies["EMPTYTRIPADMINTRANSID"]["TrasId"].ToString(), "Admin", "Sign Up Scheduler Setup");
            }


            MasterPage mp = this.Master;
            HtmlGenericControl lblMainHeader = (HtmlGenericControl)mp.FindControl("lblMainHeader");
            HtmlGenericControl lblSubHeader = (HtmlGenericControl)mp.FindControl("lblSubHeader");
            lblMainHeader.InnerText = "Admin";
            lblSubHeader.InnerText = "Sign Up Scheduler Setup";

            lblUser.Text = Request.Cookies["JETRIPSADMIN"]["Username"].ToString();
            lblcontact.Text = Request.Cookies["JETRIPSADMIN"]["PhoneNumber"].ToString();
            lblUserEmail.Text = Request.Cookies["JETRIPSADMIN"]["Email"].ToString();

            BindSchedule();
        }

    }

     protected void btnAddScheduler_Click(object sender, EventArgs e)
    {
        try
        {
            clear();



            gvToAddress.DataSource = null;
            gvToAddress.DataBind();

            tdSignature.Visible = true;

            dvGridView.Visible = false;
            dvFormView.Visible = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }

    }

    protected void gvScheduler_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblSchId.Text = ((Label)gvScheduler.SelectedRow.FindControl("lblScheID")).Text;
            Label lblRunFlag = ((Label)gvScheduler.SelectedRow.FindControl("lblRunFlag"));

            Bind_SelectedIndexChanged(lblRunFlag.Text);
            lblDeleteFor.Text = string.Empty;
            lblRowIndexId.Text = "0";
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    private void Bind_SelectedIndexChanged(string RunFlag)
    {
        //clear();

        if (RunFlag == "R")
        {
            btnStopScheduler.Visible = true;
            btnRunScheduler.Visible = false;
        }
        else
        {
            btnRunScheduler.Visible = true;
            btnStopScheduler.Visible = false;
        }

        EditSchedule(lblSchId.Text);

        dvGridView.Visible = false;
        dvFormView.Visible = true;
    }

    public void EditSchedule(string RowID)
    {
        trModify.Visible = true;

        DataTable dtEmail = objScheduler.BindEmail(RowID);
        gvToAddress.DataSource = dtEmail;
        gvToAddress.DataBind();
        if (dtEmail.Rows.Count > 0)
        {
            CommanClass.bindDropdown(dtEmail, ddlSignature, "EmailID", "UserID");
            tdSignature.Visible = true;
        }
        else
        {
            tdSignature.Visible = false;
        }

        ViewState["dtUser"] = dtEmail;

        DataTable dtExtUser = objScheduler.BindExternalUser(RowID);
        gvExtUser.DataSource = dtExtUser;
        gvExtUser.DataBind();
        ViewState["dtExtUser"] = dtExtUser;

        DataTable dt = objScheduler.BindSchedulePerticular(RowID);
        if (dt.Rows.Count > 0)
        {
            lblSchId.Text = dt.Rows[0]["ScheID"].ToString();
            ButSubmit.Text = "Update";

            BindSchedulerStatus(lblSchId.Text);
            string strDays = dt.Rows[0]["SchDays"].ToString();
            string[] strSplit = strDays.Split(',');
            foreach (ListItem item in cblDays.Items)
            {
                if (strDays.Contains(item.Value))
                {
                    item.Selected = true;
                }
            }

            //string strMove = dt.Rows[0]["Move"].ToString();
            //string[] strMSplit = strMove.Split(',');
            //foreach (ListItem item in cbMustMove.Items)
            //{
            //    if (strMove.Contains(item.Value))
            //    {
            //        item.Selected = true;
            //    }
            //}

            ddlSignature.SelectedIndex = ddlSignature.Items.IndexOf(ddlSignature.Items.FindByValue(dt.Rows[0]["Signature"].ToString()));
            lblModify.Text = dt.Rows[0]["Createdby"].ToString() + " (on) " + dt.Rows[0]["Createdon"].ToString();


            string strEndTime = dt.Rows[0]["EffTime"].ToString();
            if (strEndTime.Length > 0)
            {
                string[] strTimeSplit = strEndTime.Replace(" ", ":").Split(':');
                if (strTimeSplit.Length > 2)
                {
                    ddlHour.SelectedIndex = ddlHour.Items.IndexOf(ddlHour.Items.FindByValue(strTimeSplit[0].ToString().Trim()));
                    ddlMin.SelectedIndex = ddlMin.Items.IndexOf(ddlMin.Items.FindByValue(strTimeSplit[1].ToString().Trim()));
                    ddlAMPM.SelectedIndex = ddlAMPM.Items.IndexOf(ddlAMPM.Items.FindByValue(strTimeSplit[2].ToString().Trim()));
                }
            }

            // 
            txtEffFrom.Text = Convert.ToDateTime(dt.Rows[0]["NextPostingTime"].ToString()).ToString("MMM d, yyyy");
            lblScheduleFrom.Text = Convert.ToDateTime(dt.Rows[0]["EffDate"].ToString()).ToString("MMM d, yyyy");

            //rblRepFor.SelectedIndex = rblRepFor.Items.IndexOf(rblRepFor.Items.FindByValue(dt.Rows[0]["ReportFor"].ToString()));
            //rblMarMessage.SelectedIndex = rblMarMessage.Items.IndexOf(rblMarMessage.Items.FindByValue(dt.Rows[0]["MessageFlag"].ToString().Trim()));

            //txtMessage.Text = dt.Rows[0]["Message"].ToString();
            //txtMessageId.Text = dt.Rows[0]["MessageId"].ToString();
            //if (dt.Rows[0]["Startdate"].ToString() != "")
            //{
            //    txtStartDate.Text = Convert.ToDateTime(dt.Rows[0]["Startdate"].ToString()).ToString("MMM d, yyyy");
            //}
            //if (dt.Rows[0]["EndDate"].ToString() != "")
            //{
            //    txtExpiryDate.Text = Convert.ToDateTime(dt.Rows[0]["EndDate"].ToString()).ToString("MMM d, yyyy");
            //}
            //txtStartTime.Text = dt.Rows[0]["startTime"].ToString();
            //txtExpTime.Text = dt.Rows[0]["EndTime"].ToString();
            //if (txtExpTime.Text != "")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "javascript:datetimeupdate();", true);
            //}
        }
    }

    private void BindSchedulerStatus(string strSchedId)
    {
        DataTable dt = objScheduler.BindSchedulerStatus(strSchedId);

        if (dt.Rows.Count > 0)
        {
            tdStatus.Visible = true;

            tdStatus.Visible = true;
            lblCurrentStatus.Text = dt.Rows[0]["Status"].ToString();
            lblStatusBy.Text = dt.Rows[0]["ModifiedBy"].ToString() + " (on) " + Convert.ToDateTime(dt.Rows[0]["ModifiedOn"]).ToString("MMM dd, yyyy hh:mm tt").ToUpper();
        }

        if (lblCurrentStatus.Text == "Stopped")
        {
            imgTick.ImageUrl = "~/Components/images/tick_no.png";
            imgTick.ToolTip = "Stopped";
        }
        else
        {
            imgTick.ImageUrl = "~/Components/images/tick_yes.png";
            imgTick.ToolTip = "Running";
        }
    }

    protected void btnRunScheduler_Click(object sender, EventArgs e)
    {
        try
        {

            objScheduler.UpdateRunFlag(lblSchId.Text, "R");
            objScheduler.Save_ScheduleStatus("IS", lblSchId.Text.Trim(), "R", lblUserId.Text);

            btnStopScheduler.Visible = true;
            btnRunScheduler.Visible = false;

            BindSchedulerStatus(lblSchId.Text);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnStopScheduler_Click(object sender, EventArgs e)
    {
        try
        {

            objScheduler.UpdateRunFlag(lblSchId.Text, "S");

            objScheduler.Save_ScheduleStatus("IS", lblSchId.Text, "S", lblUserId.Text.Trim());

            btnStopScheduler.Visible = false;
            btnRunScheduler.Visible = true;

            BindSchedulerStatus(lblSchId.Text);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnAddAddress_Click(object sender, EventArgs e)
    {
        try
        {
            gvpAddress.DataSource = objScheduler.Get_ReceiveEmailAlerts("CUST");
            gvpAddress.DataBind();

            DataTable dtUser = (DataTable)ViewState["dtUser"];
            if (dtUser != null)
            {
                foreach (GridViewRow row in gvpAddress.Rows)
                {
                    for (int i = 0; i < dtUser.Rows.Count; i++)
                    {
                        if (dtUser.Rows[i]["UserID"].ToString() == ((Label)row.FindControl("lblUserID")).Text)
                        {
                            ((CheckBox)row.FindControl("chkStatus")).Checked = true;
                        }
                    }
                }
            }

            mpeATC.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvToAddress_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            string strtype = string.Empty;
            string strUserID = ((Label)gvToAddress.SelectedRow.FindControl("lblUserID")).Text;
            gvToAddress.DataSource = objScheduler.getToAddress(strtype);
            gvToAddress.DataBind();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {

            if (ViewState["dtUser"] == null)
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[4] {
                    new DataColumn("UserID"),
                    new DataColumn("Name"), 
                    new DataColumn("EmailID"),
                     new DataColumn("RoleName")
                });
                ViewState["dtUser"] = dt;
            }
            DataTable dtUser = (DataTable)ViewState["dtUser"];

            dtUser.Rows.Clear();

            foreach (GridViewRow row in gvpAddress.Rows)
            {
                if (((CheckBox)row.FindControl("chkStatus")).Checked == true)
                {
                    DataRow newrow = dtUser.NewRow();
                    newrow[0] = ((Label)row.FindControl("lblUserID")).Text;
                    newrow[1] = ((Label)row.FindControl("lblName")).Text;
                    newrow[2] = ((Label)row.FindControl("lblEmailID")).Text; ;
                    newrow[3] = ((Label)row.FindControl("lblUserType")).Text;
                    dtUser.Rows.Add(newrow);
                }
            }

            gvToAddress.DataSource = dtUser;
            gvToAddress.DataBind();

            if (dtUser.Rows.Count > 0)
            {
                CommanClass.bindDropdown(dtUser, ddlSignature, "EmailID", "UserID");
                tdSignature.Visible = true;
            }
            else
            {
                tdSignature.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }

    }

    protected void btnconfirmdelete_click(object sender, EventArgs e)
    {
        try
        {
            if (lblDeleteFor.Text.Trim() == "RE")
            {
                DataTable dt = ViewState["dtUser"] as DataTable;
                dt.Rows[Convert.ToInt32(lblRowIndexId.Text)].Delete();
                dt.AcceptChanges();
                ViewState["dtUser"] = dt;
                gvToAddress.EditIndex = -1;
                gvToAddress.DataSource = dt;
                gvToAddress.DataBind();
                gvToAddress.Focus();
                lblalert.Text = "Email deleted successfully";
                mpealert.Show();
            }
            else if (lblDeleteFor.Text.Trim() == "EU")
            {
                DataTable dt = ViewState["dtExtUser"] as DataTable;
                dt.Rows[Convert.ToInt32(lblRowIndexId.Text)].Delete();
                dt.AcceptChanges();

                ViewState["dtExtUser"] = dt;

                gvExtUser.EditIndex = -1;
                gvExtUser.DataSource = dt;
                gvExtUser.DataBind();
                lblalert.Text = "Email deleted successfully";
                mpealert.Show();
            }

            lblDeleteFor.Text = string.Empty;
            lblRowIndexId.Text = "0";

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void gvToAddress_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {
            lblDeleteFor.Text = "RE";
            lblRowIndexId.Text = e.RowIndex.ToString();
            mpeconfirm.Show();

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void ButSubmit_Click(object sender, EventArgs e)
    {
        string strRunFlag = string.Empty;
        try
        {
            if (gvToAddress.Rows.Count == 0)
            {
                mpealert.Show();
                lblalert.Text = "Receive Email Alerts is required";

                return;
            }


            ToAddress = string.Empty;
            foreach (GridViewRow row in gvToAddress.Rows)
            {
                ToAddress = ToAddress + ((Label)row.FindControl("lblEmailID")).Text + ",";
            }

            string CDays = string.Empty;
            foreach (ListItem item in cblDays.Items)
            {
                if (item.Selected)
                {
                    CDays += item.Value + ",";
                }
            }

            if (lblSchId.Text == "0")
            {
                objScheduler.ManageType = "I";
                strMsg = "Scheduled";
                lblalert.Text = "Scheduler details has been saved successfully";
            }
            else
            {
                objScheduler.ManageType = "U";
                strMsg = "Rescheduled";
                lblalert.Text = "Scheduler details has been updated successfully";
            }

            objScheduler.RowID = lblSchId.Text;   //@ScheID
            objScheduler.ToAddress = ToAddress.Trim(','); //@ToAddress
            objScheduler.ScheduleFRom = lblScheduleFrom.Text == string.Empty ? Convert.ToDateTime(txtEffFrom.Text).ToString("yyyy-MM-dd") : Convert.ToDateTime(lblScheduleFrom.Text).ToString("yyyy-MM-dd"); //@EffDate
            objScheduler.Time = ddlHour.SelectedValue.Trim() + ":" + ddlMin.SelectedValue.Trim() + " " + ddlAMPM.SelectedValue.Trim(); //@EffTime
            objScheduler.Signature = ddlSignature.SelectedValue; //@Signature
            objScheduler.SchDays = CDays.TrimEnd(',');
            objScheduler.PostingTime = Convert.ToDateTime(txtEffFrom.Text).ToString("yyyy-MM-dd") + " " + ddlHour.SelectedValue.Trim() + ":" + ddlMin.SelectedValue.Trim() + " " + ddlAMPM.SelectedValue.Trim(); //@NextPostingTime
            objScheduler.CreatedBy = Request.Cookies["JETRIPSADMIN"]["UserId"].ToString().Trim(); //@Createdby
            objScheduler.ReportFor = "CU";

            if (tdStatus.Visible == true)
            {
                if (btnStopScheduler.Visible == true)
                {
                    objScheduler.RunFlag = "R"; //@RunFlag
                    strRunFlag = "R";
                }
                else
                {
                    objScheduler.RunFlag = "S"; //@RunFlag
                    strRunFlag = "S";
                }
            }
            else
            {
                objScheduler.RunFlag = "R"; //@RunFlag
                strRunFlag = "R";
            }


            int retVal = objScheduler.Save(objScheduler);

            if (retVal > 0)
            {
                objScheduler.DeleteEMAIL(retVal.ToString());
                foreach (GridViewRow row in gvToAddress.Rows)
                {
                    Label lblUserID = ((Label)row.FindControl("lblUserID"));
                    objScheduler.SaveEmail(retVal.ToString(), lblUserID.Text);
                }

                SqlHelper.ExecuteNonQuery(SqlHelper.FLYTConnectionString, CommandType.Text, "DELETE FROM ZOOM_External_User WHERE SchId='" + retVal + "'");
                foreach (GridViewRow row in gvExtUser.Rows)
                {
                    Label lblName = ((Label)row.FindControl("lblName"));
                    Label lblEmailid = ((Label)row.FindControl("lblEmailID"));
                    Label lblUserRole = ((Label)row.FindControl("lblUserType"));

                    objScheduler.SaveExternalUser(retVal.ToString(), lblName.Text, lblEmailid.Text, lblUserRole.Text, Request.Cookies["JETRIPSADMIN"]["UserId"].ToString().Trim());
                }

                objScheduler.Save_ScheduleStatus("IS", retVal.ToString(), strRunFlag.Trim(), Request.Cookies["JETRIPSADMIN"]["UserId"].ToString().Trim());

                BindSchedule();

                mpealert.Show();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            clear();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    public void clear()
    {
        ButSubmit.Text = "Save";
        txtEffFrom.Text = string.Empty;
        lblSchId.Text = "0";
        trModify.Visible = false;
        lblScheduleFrom.Text = string.Empty;
        tdSignature.Visible = false;
        tdStatus.Visible = false;
        imgTick.ImageUrl = "~/Images/Tick_No.png";

        gvToAddress.DataSource = null;
        gvToAddress.DataBind();

        ViewState["dtUser"] = null;
        ViewState["dtExtUser"] = null;
        //rblMarMessage.SelectedValue = "N";
        //divMarketingMssage.Visible = false;
        //txtMessageId.Text = string.Empty;
        //txtMessage.Text = string.Empty;
        //txtStartDate.Text = string.Empty;
        //txtExpiryDate.Text = string.Empty;
        //txtStartTime.Text = string.Empty;
        //txtExpTime.Text = string.Empty;
        ddlHour.ClearSelection();
        ddlMin.ClearSelection();
        ddlAMPM.ClearSelection();
        lblDeleteFor.Text = string.Empty;
        lblRowIndexId.Text = "0";

    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        try
        {
            BindSchedule();
            dvGridView.Visible = true;
            dvFormView.Visible = false;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }


    protected void gvextUser_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        try
        {

            lblDeleteFor.Text = "EU";
            lblRowIndexId.Text = e.RowIndex.ToString();
            mpeconfirm.Show();



        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnAddExtUser_Click(object sender, EventArgs e)
    {
        try
        {

            mpeExtUser.Show();
            ClearExternalUser();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    protected void btnSaveExtUser_Click(object sender, EventArgs e)
    {
        string Active = string.Empty;
        DataRow drExtUser;
        DataTable dtExtUser = new DataTable();

        try
        {

            if (ViewState["dtExtUser"] != null)
            {
                dtExtUser = (DataTable)ViewState["dtExtUser"];
            }
            else
            {
                dtExtUser.Columns.Clear();
                dtExtUser.Columns.Add("ExtUserID");
                dtExtUser.Columns.Add("Name");
                dtExtUser.Columns.Add("EmailId");
                dtExtUser.Columns.Add("UserRole");
            }

            if (btnSaveExtUser.Text == "Update")
            {
                dtExtUser.Rows[Convert.ToInt32(lblExtUserId.Text)]["ExtUserID"] = lblExtUserId.Text;
                dtExtUser.Rows[Convert.ToInt32(lblExtUserId.Text)]["Name"] = txtName.Text.Trim();
                dtExtUser.Rows[Convert.ToInt32(lblExtUserId.Text)]["EmailId"] = txtEmailId.Text.Trim();
                dtExtUser.Rows[Convert.ToInt32(lblExtUserId.Text)]["UserRole"] = txtUserRole.Text.Trim();
            }
            else
            {
                drExtUser = dtExtUser.NewRow();
                drExtUser["ExtUserID"] = lblExtUserId.Text;
                drExtUser["Name"] = txtName.Text.Trim();
                drExtUser["EmailId"] = txtEmailId.Text.Trim();
                drExtUser["UserRole"] = txtUserRole.Text.Trim();
                dtExtUser.Rows.Add(drExtUser);
            }

            ViewState["dtExtUser"] = dtExtUser;

            gvExtUser.DataSource = dtExtUser;
            gvExtUser.DataBind();

            ClearExternalUser();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }
    protected void btnClearExtUser_Click(object sender, EventArgs e)
    {
        try
        {

            ClearExternalUser();
            mpeExtUser.Show();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alert", "alert('" + genclass.LogException(ex) + "');", true);
        }
    }

    void ClearExternalUser()
    {
        btnSaveExtUser.Text = "Save";
        lblExtUserId.Text = "0";

        txtName.Text = string.Empty;
        txtEmailId.Text = string.Empty;
        txtUserRole.Text = string.Empty;
    }
    private void BindSchedule()
    {
        clear();

        DataTable dtSch = objScheduler.Select_CustomerSignup_Sch();

        gvScheduler.DataSource = dtSch;
        gvScheduler.DataBind();

        if (gvScheduler.Rows.Count > 0)
        {
            for (int i = 0; i < gvScheduler.Rows.Count; i++)
            {
                Label lblSchId = (Label)gvScheduler.Rows[i].FindControl("lblScheID");
                Label lblStatus = (Label)gvScheduler.Rows[i].FindControl("lblStatus");

                DataTable dt = objScheduler.BindSchedulerStatus(lblSchId.Text);

                if (dt.Rows.Count > 0)
                {
                    lblStatus.Text = dt.Rows[0]["Status"].ToString();
                }
                else
                {
                    lblStatus.Text = "Stopped";
                }

                if (lblStatus.Text == "Stopped")
                {
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
                else if (lblStatus.Text == "Running")
                {
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
            }

            dvFormView.Visible = false;
            dvGridView.Visible = true;
        }
        else
        {
            dvFormView.Visible = true;
            dvGridView.Visible = false;
        }
    }
}